<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lowcosttrip-wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wUx+u]Zl9U+HL.400_XlODHRH*J^hD}F1|l /b{C~FhoghZM~tv/`6]-BU>wywWs');
define('SECURE_AUTH_KEY',  'lCDQfQ[Scbm6cI7A+A#P9}HPt cBTT(v#{.Eo$6<re9G^OksJlejbFr?8n!`T{bP');
define('LOGGED_IN_KEY',    'N3teX`%o1Gbkp0k6@_tP)TUIP69nes%dNyOeI1-uTuRJSc`u/EN0 ~V,y|?!uPf9');
define('NONCE_KEY',        '-E[uQ[(yWZ_5u8`i}x=Mn>o ,R!@/@d/ctSu5`3(u,9qqI{GLn_>K5uz.mbdM~(C');
define('AUTH_SALT',        ';H*ZM],<V.MNtG.fL@[Fo-L60=g{Y0&d;!Li%okwITQ-ji)/8]yq4JY9w0)RTix[');
define('SECURE_AUTH_SALT', 'Z`,6HZy4HmQ23sFZDp,_;w=n.ez;&hL]@3i}xZPc>`e1$([w#dnJ2nEvJ9t>f**o');
define('LOGGED_IN_SALT',   '(NJw_l*?~h.cGiZ{2k&Z`5{n728f+wZcb$Y[hZzdT7Pk|al|k4#f(rm&Gr^,6;0~');
define('NONCE_SALT',       'kECk>.EL$e]fY;Xf(*JSJO3KIjAu%nZ#/CBTQmbT4rFb<-_#g1@_1<7-BTlz8uh+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
