<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


<!-- CSS -->
<link href="<?php echo get_template_directory_uri(); ?>/style.css" rel = "stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/css/css/bootstrap.css" rel = "stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/css/css/bootstrap.min.css" rel = "stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/css/css/bootstrap-theme.css" rel = "stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/css/css/bootstrap-theme.min.css" rel = "stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/css/css/media-queries.css" rel = "stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
<header class="hed-bg">
    <div class="hidden-xs">
        <div class="main-nav">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="logo">
                            <?php if ( get_header_image() ) : ?>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand"><img src="<?php header_image(); ?>" class="img-responsive" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" /></a>
							<?php endif; ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'nav nav-tabs navbar-nav' ) ); ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div id="myTabContent" class="tab-content">
                <!-- Holiday Block Begins -->
                <div class="tab-pane fade in active" id="holiday-block">
                    <!-- Search Block Begins -->
                    <div class="search-block hotel-search">
                        <div class="main-sub-nav">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 col-lg-offset-3">
                                        <nav>
                                           <?php wp_nav_menu( array( 

	                                           	'theme_location' => 'primary', 
	                                           	'menu'            => '',
							                    'container'       => '',
							                    'container_class' => '',
							                    'container_id'    => '',
							                    'menu_class'      => 'nav nav-tabs navbar-nav',
							                    'menu_id'         => '',
							                    'echo'            => true,
							                    'fallback_cb'     => 'wp_page_menu',
							                    'before'          => '',
							                    'after'           => '',
							                    'link_before'     => '',
							                    'link_after'      => '',
							                    'items_wrap'      => '<ul class="ul-reset">%3$s</ul>',
							                    'depth'           => 0

                                           	) ); ?>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="holiday-bottom">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-lg-offset-1">

                                        <form method="GET" id="tour_filter_form" name="tour_filter_form" action="{{ route('tour_filter') }}">
                                            <div class="row">
                                                <div>
                                                    <input type="text" placeholder="Search Your Destination" name="search" class=" search-dest">
                                                </div>
                                                <div>
                                                    <button type="submit" class=" btn-search ">Search</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>
                                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                        <div class="text-center">
                                            <br>
                                            <span class="search-span">or</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
                                        <div class="row">
                                            <div class="search-cust">

                                                <a href='{{env("APP_URL")}}home/customise-book' class="btn btn-default btn-customize">Build your package</a>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div> -->
                    </div>
                    <!-- Search Block Ends -->
                </div>
                <div class=" mobile-nav hidden-sm hidden-md hidden-lg">
        		<nav class="navbar navbar-default">
	            <div class="container-fluid">
	                <div class="navbar-header">
	                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">

	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                    </button>

	                   <?php if ( get_header_image() ) : ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php header_image(); ?>" class="header-image" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" /></a>
						<?php endif; ?>
	                </div>
	                <div class="collapse navbar-collapse" id="myNavbar">
	                    <ul class="nav navbar-nav">
	                        <li class="active dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Holidays</a>
	                            <ul class="dropdown-menu">
	                                <li><a href="#">Domestic Tours</a></li>
	                                <li><a href="#">International Tours</a></li>
	                                <li><a href="#">Group Tours</a></li>
	                                <li><a href="#">Tours By Price</a></li>
	                                <li><a href="#">Deals</a></li>
	                            </ul>

	                        </li>
	                        <li><a href="#">Hotels</a></li>
	                        <li><a href="#">Flights</a></li>
	                        <li><a href="#">Visa</a></li>
	                        <li><a href="#">Login</a></li>
	                    </ul>

	                </div>
            	</div>
        	</nav>
        </div>
    </div>
</header>
<div id="main" class="wrapper">