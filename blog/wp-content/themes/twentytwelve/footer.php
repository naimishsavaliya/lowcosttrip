<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</div><!-- #main .wrapper -->

    <div class="footer-top">
        <div class="container">
            <div class="footer-top-blocks">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" id="footer-sidebar-1">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(2) ) : ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" id="footer-sidebar-2">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(3) ) : ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" id="footer-sidebar-3">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(4) ) : ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="footer-copyright">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <p>&copy; <a href="<?php echo esc_url( __( '', 'twentytwelve' ) ); ?>">All Rights Reserved</a></p>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                                <ul class="footer-payment">
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/payment/comodo.jpg"/></a></li>
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/payment/visa.jpg"/></a></li>
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/payment/master-card.jpg"/></a></li>
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/payment/ae.jpg"/></a></li>
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/payment/paypal.jpg"/></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- #page -->

<?php wp_footer(); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js/bootstrap.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</body>
</html>