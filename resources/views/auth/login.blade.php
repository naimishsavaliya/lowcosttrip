@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
    select.select-style{
        background-size: 6px 5px, 5px 5px, 3em 3.5em;
    }
    .or-seperator {
        margin: 20px 0 10px;
        text-align: center;
        border-top: 1px solid #ccc;
    }
    .or-seperator i {
        padding: 0 10px;
        background: #f7f7f7;
        position: relative;
        top: -11px;
        z-index: 1;
    }
    .social-login i {
       background:  none;
    }
    .social-login a {
        width: 48px;
        margin-left: 25px;
    }
</style>
<script type="text/javascript">
    cityData='';
</script>
<div class="container">
<br/>
@if(session()->has('message'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('log_error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session()->get('log_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row">
        <div class="col-md-6">
            <div class="card"> 

                <div class="card-header">{{ __('LOG IN') }}</div>
                <div class="card-body">
                    <form method="POST" id="loginform" name="loginform" action="{{ route('client_login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Email" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group marb0">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <a class="btn btn-link" href="{{ route('forgot_password') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                <button type="submit" class="btn btn-primary submit-btn">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="or-seperator"><i>Or login with social accounts</i></div>
            <div class="text-center social-login">
                <a href="{{ url('login/redirect/google/0') }}" class="btn btn-danger"> 
                    <i class="fa fa-google-plus" aria-hidden="true" ></i>
                </a>

                <a href="{{ url('login/redirect/facebook/0') }}"  class="btn btn-primary"> 
                    <i class="fa fa-facebook" aria-hidden="true"  ></i>
                </a>

                <a href="{{ url('login/redirect/twitter/0') }}" class="btn btn-info"> 
                    <i class="fa fa-twitter" aria-hidden="true"  ></i>
                </a>
            </div>

        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('SIGN UP') }}</div>
                <div class="card-body">
                    <form method="POST" id="registration_frm" name="registration_frm" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="col-md-3 nopadding ">
                            <select class="form-control select-style errow-size"  name="country_code" id="country_code">
                                <option value="" selected>Country Code</option>
                                @if(isset($countrys))
                                @foreach($countrys as $country)
                                <option value="{{ $country->phonecode }}" {{ (isset($users[0]->country_code) && $users[0]->country_code == $country->country_id) ? 'selected' : ($country->country_id == 101 ?  'selected'  : '') }}>{{ $country->sortname.' ('.$country->phonecode.')'}}</option>
                                @endforeach
                                @endif
                            </select>
                            <small class="text-danger">{{ $errors->first('country_code') }}</small>
                        </div>
                        <div class="col-md-9" style="padding-right: 0px;">
                            <div class="form-group">
                                <input type="text" name="phone_no" id="phone_no" class="form-control" placeholder="Phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="form-control select-style errow-size"  name="country_id" id="country_id">
                                <option value="" selected>Choose Country</option>
                                @if(isset($countrys))
                                @foreach($countrys as $country)
                                <option value="{{ $country->country_id }}" {{ (isset($users[0]->country_id) && $users[0]->country_id == $country->country_id) ? 'selected' : ($country->country_id == 101 ?  'selected'  : '') }}>{{ $country->country_name}}</option>
                                @endforeach
                                @endif
                            </select>
                            <small class="text-danger">{{ $errors->first('country_id') }}</small>
                        </div>
                        <div class="form-group">
                            <select class="form-control select-style errow-size"  name="state_id" id="state_id">
                                <option value="" selected>Choose State</option>
                                @if(isset($states))
                                @foreach($states as $state)
                                <option value="{{ $state->state_id }}" {{ (isset($users[0]->state_id) && $users[0]->state_id == $state->state_id) ? 'selected' : (($state->state_id == 22) ? 'selected' : '') }}>{{ $state->state_name}}</option>
                                @endforeach
                                @endif
                            </select>
                            <small class="text-danger">{{ $errors->first('country_id') }}</small>
                        </div>
                        <div class="form-group">
                            <select class="form-control select-style errow-size"  name="city_id" id="city_id">
                                <option value="" selected>Choose City</option>
                                @if(isset($cities))
                                @foreach($cities as $city)
                                <option value="{{ $city->city_id }}" {{ (isset($users[0]->city_id) && $users[0]->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                @endforeach
                                @endif
                            </select>
                            <small class="text-danger">{{ $errors->first('country_id') }}</small>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="reg_password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password">
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary submit-btn gr2">
                                    {{ __('CREATE AN ACCOUNT') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>

<script type="text/javascript">
$(function () {
    $("#registration_frm").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            email: {
                required: true,
                email: true,
                remote: {
                    url: '{{ route("duplicate_email") }}',
                    type: "POST",
                    data: {is_agent:0},
                    headers: {'X-CSRF-TOKEN': Laravel.csrfToken}
                }
            },
            phone_no: {
                required: true,
                remote: {
                    url: '{{ route("duplicate_phone_no") }}',
                    type: "POST",
                    data: {is_agent:0},
                    headers: {'X-CSRF-TOKEN': Laravel.csrfToken}
                }
            },
            country_id: "required",
            state_id: "required",
            city_id: "required",
            password: {
                required: true,
                minlength: 6
            },
            password_confirmation: {
                required: true,
                equalTo: "#reg_password"
            }
        },
        messages: {
            first_name: "Please enter firstname",
            last_name: "Please enter lastname",
            email: {
                    required: "Please enter an email",
                    remote:"Email already exists."
                },
            phone_no: {
                    required: "Please enter phone number",
                    remote:"Phone number already exists."
                },
            country_id: "Please select country id",
            state_id: "Please select state",
            city_id: "Please select city",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            password_confirmation: {
                required: "Please provide a confirm password",
                equalTo: "Enter Confirm Password Same as Password"
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("#loginform").validate({
        rules: {
            email: "required",
            password: "required"
        },
        messages: {
            email: "Please enter email",
            password: "Please enter Password"
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#country_id').change(function () {
        var country_id = $(this).val();
        var url = '{{ route("state_by_country_front", ":country_id") }}';
        url = url.replace(':country_id', country_id);
        $.ajax({
            url: url,
            type: 'GET',
            data: {country_id:country_id},
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#state_id')
                        .find('option')
                        .remove();
                $('#state_id').append('<option value="">Choose State</option>');
                $.map(data, function (item) {
                    $('#state_id').append('<option value="' + item.state_id + '">' + item.state_name + '</option>');
                });
                $("#state_id").trigger("chosen:updated");
                $("#state_id").trigger("liszt:updated");
            }
        });
        
    });
    $('#state_id').change(function () {
        var state_id = $(this).val();
        var url = '{{ route("city_by_state_front", ":state_id") }}';
        url = url.replace(':state_id', state_id);
        $.ajax({
            url: url,
            type: 'GET',
            data: {state_id:state_id},
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#city_id')
                        .find('option')
                        .remove();
                $('#city_id').append('<option value="">Choose City</option>');
                $.map(data, function (item) {
                    $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
                });
                $("#city_id").trigger("chosen:updated");
                $("#city_id").trigger("liszt:updated");
            }
        });
    });
});
</script>
@endsection
