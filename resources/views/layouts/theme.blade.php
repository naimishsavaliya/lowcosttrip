<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="{{ app()->getLocale() }}">> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="{{ app()->getLocale() }}"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>

        <!--<link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">-->

        <title>LCT - Admin Panel</title>

        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
        <!-- uikit -->
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/select2/dist/css/select2.min.css">
        <!-- flag icons -->
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/icons/flags/flags.min.css" media="all">
        <!-- style switcher -->
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/css/style_switcher.min.css" media="all">
        <!-- altair admin -->
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/css/main.css" media="all">
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/css/custom.css" media="all">
        <!-- themes -->
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/css/themes/themes_combined.min.css" media="all">

        <!-- matchMedia polyfill for testing media queries in JS -->
        <!--[if lte IE 9]>
            <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/matchMedia/matchMedia.js"></script>
            <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/matchMedia/matchMedia.addListener.js"></script>
            <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/css/ie.css" media="all">
        <![endif]-->
        
        <script src="{{ env('APP_PUBLIC_URL')}}admin/js/jquery-1.8.3.min.js"></script>
        @yield('style')
        <script type="text/javascript">
                    var ADMIN_URL = "{{ env('ADMIN_URL')}}";
        </script>

    </head>
    <body class="disable_transitions top_menu">


        @include('admin.includes.topmenu')
        <div id="page_content">
            @yield('content')
        </div>
        <!-- google web fonts -->
        <script>
            WebFontConfig = {
                google: {
                    families: [
                        'Source+Code+Pro:400,700:latin',
                        'Roboto:400,300,500,700,400italic:latin'
                    ]
        }
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
        </script>

        <!-- common functions -->
        <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/common.min.js"></script>
        <!-- uikit functions -->
        <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/uikit_custom.min.js"></script>
        <!-- altair common functions/helpers -->
        <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/altair_admin_common.js"></script>

        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/pdfmake/build/pdfmake.min.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/pdfmake/build/vfs_fonts.js"></script>

        <!-- page specific plugins -->
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/select2/dist/js/select2.min.js"></script>

        <!-- datatables -->
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/datatables/buttons.uikit.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/datatables-buttons/js/buttons.colVis.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/datatables-buttons/js/buttons.html5.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/datatables-buttons/js/buttons.print.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/datatables/datatables.uikit.min.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/plugins_datatables.min.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom.js"></script>


        <!-- dropify code start-->
        <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.dropify').dropify({
                    messages: {
                        'default': '',
                        'replace': '',
                        'remove':  'Remove',
                        'error':   ''
                    }
                });
            });
        </script>
        <!-- dropify code over-->



        @yield('javascript')
    </body>
</html>