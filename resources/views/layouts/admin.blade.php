<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>LCT - Admin Panel</title>
        <link rel="shortcut icon" href="{{ env('APP_PUBLIC_URL')}}admin/img/favicon.ico" type="image/x-icon">

        <link href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
        <link href="{{ env('APP_PUBLIC_URL')}}admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="{{ env('APP_PUBLIC_URL')}}admin/css/style.css" rel="stylesheet" />
        <link href="{{ env('APP_PUBLIC_URL')}}admin/css/style_responsive.css" rel="stylesheet" />
        <link href="{{ env('APP_PUBLIC_URL')}}admin/css/style_default.css" rel="stylesheet" id="style_color" />

        <link href="{{ env('APP_PUBLIC_URL')}}admin/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/uniform/css/uniform.default.css" />
        <link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/chosen-bootstrap/chosen/chosen.css" />

        <script src="{{ env('APP_PUBLIC_URL')}}admin/js/jquery-1.8.3.min.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}admin/js/jquery.blockui.js"></script>
        <script type="text/javascript">
            var ADMIN_URL = "{{ env('ADMIN_URL')}}";
        </script>
        <?php if (file_exists(public_path() . '/admin/js/' . $controller . '.js')) { ?>
            <script src="{{ env('APP_PUBLIC_URL')}}admin/js/{{$controller}}.js"></script>
        <?php } ?>
    </head>
    <body class="fixed-top">
        <!-- BEGIN HEADER -->

        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        @include('admin.includes.topnavigation')
        <div id="container" class="row-fluid">
            @include('admin.includes.left')
            @yield('content')
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <!--        <div id="footer">
                                2013 &copy; TDX.
                                <div class="span pull-right">
                                        <span class="go-top"><i class="icon-arrow-up"></i></span>
                                </div>
                        </div>-->

        <!-- ie8 fixes -->
        <!--[if lt IE 9]>
        <script src="{{ env('APP_PUBLIC_URL')}}admin/js/excanvas.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}admin/js/respond.js"></script>
        <![endif]-->
        <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
        <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/uniform/jquery.uniform.min.js"></script>
        <script src="{{ env('APP_PUBLIC_URL')}}admin/js/scripts.js"></script>
        @yield('javascript');
        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                App.init();
    });
        </script>
    </body>
</html>