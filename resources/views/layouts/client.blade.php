<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head> 

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Low Cost Trip</title>
         
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}client/fonts/fa/css/font-awesome.min.css">
       <link rel="icon" href="{{ env('APP_PUBLIC_URL')}}favicon.ico" type="image/x-icon">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}new-theme/fonts/fa/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}new-theme/fonts/soap/soap-icon.css">
        <!--Bootstrap Css -->
        <link href="{{ env('APP_PUBLIC_URL')}}new-theme/css/bootstrap.min.css" rel="stylesheet">
        <!-- External Css -->
        <link rel='stylesheet prefetch' href="{{ env('APP_PUBLIC_URL')}}new-theme/assets/owl/css/owl.carousel.css">
        <!--    <link rel='stylesheet' href="{{ env('APP_PUBLIC_URL')}}new-theme/assets/aos/aos.css" type="text/css"> -->
        <link href="{{ env('APP_PUBLIC_URL')}}new-theme/style.css" rel="stylesheet" type="text/css">
        
		<link href="{{ env('APP_PUBLIC_URL')}}new-theme/css/media-queries.css" rel="stylesheet" type="text/css">
		<link href="{{ env('APP_PUBLIC_URL')}}new-theme/css/responseve.css" rel="stylesheet" type="text/css">

        <link href="{{ env('APP_PUBLIC_URL')}}client/css/custom.css" rel="stylesheet" type="text/css">
		<link href=" https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <link href="{{ env('APP_PUBLIC_URL')}}new-theme/css/fullcalendar.css" rel='stylesheet' />
        <link href="{{ env('APP_PUBLIC_URL')}}new-theme/css/fullcalendar.print.min.css" rel='stylesheet' media='print' />
		<!--<link rel="stylesheet" href="/resources/demos/style.css">-->
		<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->

        <!--[if lt IE 9]>
             <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
             <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>;
			var APP_URL = "<?= env('APP_URL') ?>";
        </script>
		
    </head>
    <body>

        @include('client.includes.header')

        @yield('content')

        @include('client.includes.footer')
        @yield('javascript') 
    </body>
</html>