<html>
<head></head>
  <body>
    <div style="background:#ecf0f1;width: 500px;font-family: Arial, Helvetica, sans-serif;">
      <div style="width: 500px; background-color:#fefefe;font-family: Arial, Helvetica, sans-serif;">
        <div >
          <h4>Dear {{$username}}</h4>
          <p>This is to inform you that you have set up below follow up reminder.</p>
          <p>Request to do the needful on above reminder.</p>
          <?php 
$date=isset($reminder_datetime) ? date('d F Y H:i a', strtotime($reminder_datetime)) : '';
          ?>
            <table border="1" cellpadding="1" cellspacing="1" style="width:400px;">
              <tbody>
                <tr>
                  <td>Lead ID:</td>
                  <td>#{{$lead_id}}</td>
                </tr>
                <tr>
                  <td>Lead Title:</td>
                  <td>{{$lead_title}}</td>
                </tr>
                <tr>
                  <td>Customer Name:</td>
                  <td>{{$username}}</td>
                </tr>
                <tr>
                  <td>Reminder Date &amp; Time:</td>
                  <td>{{$date}}</td>
                </tr>
                <tr>
                  <td>Reminder Note:</td>
                  <td>{{$reminder_note}}</td>
                </tr>
              </tbody>
            </table>
          <p>Request to do the needful on above reminder.</p>
          <p>Regards</p>
          <p>LCT Team</p>
        </div>
      </div>
    </div>
  </body>
</html>



