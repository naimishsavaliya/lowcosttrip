@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        @can('add_role')
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('role.add')}}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
        @endcan
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')
            <table class="uk-table" id="role_list">
                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Role Name</th>
                        <th>Created Date</th>
                        <!--<th class="hidden-phone">No. Of Users</th>-->
                        <th >Action</th>
                    </tr>
                </thead>
                <!-- <tfoot>
                    <tr>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="ID" name="" colPos="0" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Role Name" name="" colPos="1" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot> -->
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(function () {
        var roleTable = $('#role_list').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            searching: true,
            order: [[ 0, "desc" ]],
            "ajax": {
                "url": "{{ route('role.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"},
                beforeSend: function () {

                    if (typeof roleTable != 'undefined' && roleTable.hasOwnProperty('settings')) {
                        roleTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {data: "id", name: "id"},
                {data: "name", name: "name"},
                {data: "created_at", name: "created_at"},
                {data: "action", 'sortable': false, "render": function (data, type, row) {
                        var action = "";
                                @can('edit_role')
                                action += '<a href="' + ADMIN_URL + 'role/edit/' + row.id + '" title="Edit Role" class="btn btn-mini btn-primary"><i class="icon-pencil icon-white"></i></a>';
                                @endcan
                                @can('delete_role')
                                action += ' <a href="' + ADMIN_URL + 'role/delete/' + row.id + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete Role" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>';
                                @endcan
                                action += ' <a href="' + ADMIN_URL + 'role/role-permission/' + row.id + '" title="Role Permission" class=""><i class="md-icon material-icons">visibility</i></a></td>';
                        return action;
                    },
                },
            ]
        });

        $(roleTable.table().container()).on('keyup', 'tfoot input', function () {
            roleTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(roleTable.table().container()).on('change', 'tfoot select', function () {
            roleTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        jQuery('#role_list_wrapper .dataTables_filter input').addClass("input-medium"); // modify table search input
        jQuery('#role_list_wrapper .dataTables_length select').addClass("input-mini"); // modify table per page dropdown
    });
</script>
@endsection