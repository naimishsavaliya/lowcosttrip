@extends('layouts.theme')
@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('destination.save') }}" name="destination_frm" id="destination_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($destinations->desId) ? $destinations->desId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Destination Name</label>
                                        <input type="text" class="md-input" name="name" value="{{ isset($destinations->name) ? $destinations->name : '' }}" />
                                        @if($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="sector_id" class="md-input" name="sector_id">
                                            <option value="">Choose Sector</option>
                                            @foreach($sectors as $sector)
                                            <option value="{{ $sector->id }}" {{ (isset($destinations->sector_id) && $destinations->sector_id == $sector->id) ? 'selected' : '' }}>{{ $sector->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('sector_id'))<small class="text-danger">{{ $errors->first('sector_id') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Description</label>
                                        <textarea cols="30" rows="4" class="md-input" name="description">{{ isset($destinations->description) ? $destinations->description : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Weather Information</label>
                                        <textarea cols="30" rows="4" class="md-input" name="weather_description" >{{ isset($destinations->weather_description) ? $destinations->weather_description : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3 p-r-50">
                                        <select class="md-input" name="country_id" id="country_id">
                                            <option value="" selected>Choose Country</option>
                                            @if(isset($countrys))
                                            @foreach($countrys as $country)
                                            <option value="{{ $country->country_id }}" {{ (isset($destinations->country_id) && $destinations->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('country_id'))<small class="text-danger">{{ $errors->first('country_id') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-3 p-l-50">
                                        <select class="md-input" name="state_id" id="state_id">
                                            <option value="" selected>Choose State</option>
                                            @if(isset($states))
                                            @foreach($states as $state)
                                            <option value="{{ $state->state_id }}" {{ (isset($destinations->state_id) && $destinations->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('state_id'))<small class="text-danger">{{ $errors->first('state_id') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-3 p-l-50">
                                        <select class="md-input" name="city_id" id="city_id">
                                            <option value="">Choose City</option>
                                            @if(isset($cities))
                                            @foreach($cities as $city)
                                            <option value="{{ $city->city_id }}" {{ (isset($destinations->city_id) && $destinations->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('city_id'))<small class="text-danger">{{ $errors->first('city_id') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($destinations->status) && $destinations->status == 1) ? 'checked' : ''  }} {{ (!isset($destinations)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($destinations->status) && $destinations->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('destination.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

@endsection


@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>
<script type="text/javascript">
$(function () {

    // It has the name attribute "activity_type_frm"
    $("form#destination_frm").validate({
// Specify validation rules
        rules: {
            name: "required",
            sector_id: "required",
            country_id: "required",
            state_id: "required",
            city_id: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            name: "Please enter destination name",
            sector_id: "Please select sector",
            country_id: "Please select country",
            state_id: "Please select state",
            city_id: "Please select city",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });


    $('#country_id').change(function () {
        var country_id = $(this).val();
        var url = '{{ route("state_by_country", ":country_id") }}';
        url = url.replace(':country_id', country_id);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#state_id')
                        .find('option')
                        .remove();
                $('#state_id').append('<option value="">Choose State</option>');
                $.map(data, function (item) {
                    $('#state_id').append('<option value="' + item.state_id + '">' + item.state_name + '</option>');
                });
                $("#state_id").trigger("chosen:updated");
                $("#state_id").trigger("liszt:updated");
            }
        });
    });

    $('#state_id').change(function () {
        var state_id = $(this).val();
        var url = '{{ route("city_by_state", ":state_id") }}';
        url = url.replace(':state_id', state_id);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#city_id')
                        .find('option')
                        .remove();
                $('#city_id').append('<option value="">Choose City</option>');
                $.map(data, function (item) {
                    $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
                });
                $("#city_id").trigger("chosen:updated");
                $("#city_id").trigger("liszt:updated");
            }
        });
    });

});
</script>

@endsection
