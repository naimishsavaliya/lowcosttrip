@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Mailbox</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>


        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN MAILBOX PORTLET-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-envelope"></i> Mailbox</h4>
                        <div class="tools pull-right mtop7 mail-btn">
                            <div class="btn-group">
                                <a class="btn btn-small element" data-original-title="New" href="{{ env('ADMIN_URL')}}user/compose" data-toggle="tooltip" data-placement="top">
                                    <i class=" icon-plus"></i>
                                </a>
                                <a class="btn btn-small element" data-original-title="Share" href="#" data-toggle="tooltip" data-placement="top">
                                    <i class="icon-share-alt"></i>
                                </a>

                                <a class="btn btn-small element" data-original-title="Report" href="#" data-toggle="tooltip" data-placement="top">
                                    <i class="icon-exclamation-sign">
                                    </i>
                                </a>
                                <a class="btn btn-small element" data-original-title="Delete" href="#" data-toggle="tooltip" data-placement="top">
                                    <i class="icon-trash">
                                    </i>
                                </a>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-small element" data-original-title="Move to" href="#" data-toggle="tooltip" data-placement="top">
                                    <i class="icon-folder-close">
                                    </i>
                                </a>
                                <a class="btn btn-small element" data-original-title="Tag" href="#" data-toggle="tooltip" data-placement="top">
                                    <i class="icon-tag">
                                    </i>
                                </a>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-small element" data-original-title="Prev" href="#" data-toggle="tooltip" data-placement="top">
                                    <i class="icon-chevron-left">
                                    </i>
                                </a>
                                <a class="btn btn-small element" data-original-title="Next" href="#" data-toggle="tooltip" data-placement="top">
                                    <i class="icon-chevron-right">
                                    </i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body">
                        <table class="table table-condensed table-striped table-hover no-margin">
                            <thead>
                                <tr>
                                    <th style="width:3%">
                                        <input type="checkbox" class="no-margin">
                                    </th>
                                    <th style="width:17%">
                                        Sent by
                                    </th>
                                    <th class="hidden-phone" style="width:55%">
                                        Subject
                                    </th>
                                    <th class="right-align-text hidden-phone" style="width:12%">
                                        Labels
                                    </th>
                                    <th class="right-align-text hidden-phone" style="width:12%">
                                        Date
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="no-margin">
                                    </td>
                                    <td>
                                        Dulal khan
                                    </td>
                                    <td class="hidden-phone">
                                        <strong>
                                            Senior Creative Designer
                                        </strong>
                                        <small class="info-fade">
                                            Vector Lab
                                        </small>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        <span class="label label label-info">
                                            Read
                                        </span>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        Yesterday
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="no-margin">
                                    </td>
                                    <td>
                                        Mosaddek Hossain
                                    </td>
                                    <td class="hidden-phone">
                                        <strong>
                                            Senior UI Engineer
                                        </strong>
                                        <small class="info-fade">
                                            Vector Lab International
                                        </small>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        <span class="label label label-success">
                                            New
                                        </span>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        Today
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="no-margin">
                                    </td>
                                    <td>
                                        Sumon Ahmed
                                    </td>
                                    <td class="hidden-phone">
                                        <strong>
                                            Manager
                                        </strong>

                                        <small class="info-fade">
                                            ABC International
                                        </small>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        <span class="label label">
                                            Imp
                                        </span>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        Yesterday
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="no-margin">
                                    </td>
                                    <td>
                                        Rafiqul Islam
                                    </td>
                                    <td class="hidden-phone">
                                        <strong>
                                            Verify your email
                                        </strong>

                                        <small class="info-fade">
                                            lorem ipsum dolor imit
                                        </small>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        <span class="label label label-info">
                                            Read
                                        </span>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        18-04-2013
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="no-margin">
                                    </td>
                                    <td>
                                        Dkmosa
                                    </td>
                                    <td class="hidden-phone">
                                        <strong>
                                            Statement for January 2012
                                        </strong>
                                        <small class="info-fade">
                                            Director
                                        </small>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        <span class="label label label-success">
                                            New
                                        </span>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        10-02-2013
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="no-margin">
                                    </td>
                                    <td>
                                        Mosaddek
                                    </td>
                                    <td class="hidden-phone">
                                        <strong>
                                            You're In!
                                        </strong>
                                        <small class="info-fade">
                                            Frontend developer
                                        </small>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        <span class="label label">
                                            Imp
                                        </span>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        21-01-2013
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="no-margin">
                                    </td>
                                    <td>
                                        Dulal khan
                                    </td>
                                    <td class="hidden-phone">
                                        <strong>
                                            Support
                                        </strong>
                                        <small class="info-fade">
                                            XYZ Interactive
                                        </small>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        <span class="label label label-info">
                                            New
                                        </span>
                                    </td>
                                    <td class="right-align-text hidden-phone">
                                        19-01-2013
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END MAILBOX PORTLET-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection