@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
<style type="text/css">
    .user_heading {
        padding: 26px 26px 0 26px !important;
    }
    .uk-sticky-placeholder .uk-tab{background: #1976D2;}
    .user_heading{ padding: 24px 24px 0px 24px;}
    .select2{ width: 100% !important;}
    .uk-tab > li.uk-active > a{color: #FFF;}
    .uk-tab > li.uk-active > a{border-bottom-color: #00071f;}
    .uk-dropdown{width: 200px !important;}
    .select2-container--open{z-index: 9999;}
    .uk-dropdown-shown{min-width: 260px !important;}

</style>
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('agent.index') }}">Lead Name</a></li>
            <li><span>Details</span></li>
        </ul>
    </div>
    <div id="page_content">
        <div id="page_content_inner p-0">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-1-1">
                    <div class="">
                        <div class="user_heading">
                            <div class="user_heading_menu hidden-print">
                                <div class="uk-display-inline-block" data-uk-dropdown="{pos:'left-top'}">
                                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="javascript:;" data-uk-modal="{target:'#update_status'}" ><i class="material-icons ">add</i> Update Status</a></li>
                                            <li><a href="{{ route('agent.notes.add', '1') }}"><i class="material-icons ">add</i> Followup</a></li>
                                            <li><a href="{{ route('agent.document.add', '1') }}"><i class="material-icons ">add</i> Document</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="user_heading">
                                <div class="user_heading_avatar">
                                    <div class="thumbnail">
                                        <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/avatars/user@2x.png" alt="user avatar"/>
                                    </div>
                                </div>
                                <div class="user_heading_content" style="display:inline-flex;">
                                    <h2 class="heading_b uk-margin-bottom">
                                        <span class="uk-text-truncate">Agent Name</span>
                                        <span class="sub-heading">Address Here</span>
                                        <span class="md-btn md-btn-primary">Pending Status</span>
                                    </h2>
                                    <ul class="user_stats" style="margin-left:40px;padding: 5px;">
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Leads</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Quotations</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Confirm Leads</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Tours</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Customers</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Reviews</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Revenue</span></h4>
                                        </li>
                                    </ul>
                                </div>
                                <!--                                <a class="md-fab md-fab-small md-fab-accent hidden-print" href="javascript:,;">
                                                                    <i class="material-icons">&#xE150;</i>
                                                                </a>-->
                            </div>
                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">Agent Info</a></li>
                                <li class="followup"><a href="#">Follow-up</a></li>
                                <li class="documents"><a href="#">Documents</a></li>
                                <li class="subscription"><a href="#">Subscription</a></li>
                                <li class="invoice"><a href="#">Invoice</a></li>
                                <li class="ledger"><a href="#">Ledger</a></li>
                                <!--<li><a href="#">History</a></li>-->
                            </ul>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <div class="p-t-30">
                                        <form action="{{ route('agent.index') }}" class="" method="get">
                                            <div class="uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <select id="select_demo_3" class="md-input">
                                                                    <option value="" selected>Choose Title</option>
                                                                    <option>Mr.</option>
                                                                    <option>Mrs.</option>
                                                                    <option>Ms.</option>
                                                                    <option>Dr.</option>
                                                                </select>
                                                            </div>
                                                            <div class="uk-width-medium-1-4 p-r-50">
                                                                <label>First Name <span class="required-lbl">*</span></label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-4 p-r-50">
                                                                <label>Last Name <span class="required-lbl">*</span></label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label>E-mail <span class="required-lbl">*</span></label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-4 p-r-50">
                                                                <label>Password <span class="required-lbl">*</span></label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-4 p-r-50">
                                                                <label>Confirm Password <span class="required-lbl">*</span></label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label>Address</label>
                                                                <textarea cols="30" rows="4" class="md-input"></textarea>
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-0">
                                                                <div class="uk-width-medium-1-12 display-inline-flex">
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <select id="select_demo_3" class="md-input">
                                                                            <option value="" selected>Choose Country</option>
                                                                            <option value="1">India</option>
                                                                            <option value="2">US</option>
                                                                            <option value="3">UK</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                                        <select id="select_demo_3" class="md-input">
                                                                            <option value="" selected>Choose State</option>
                                                                            <option value="1">India</option>
                                                                            <option value="2">US</option>
                                                                            <option value="3">UK</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="uk-width-medium-1-12 p-t-20 display-inline-flex">
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <select id="select_demo_3" class="md-input">
                                                                            <option value="" selected>Choose City</option>
                                                                            <option value="1">Mumbai</option>
                                                                            <option value="2">Pune</option>
                                                                            <option value="3">Thane</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                                        <label>Pincode</label>
                                                                        <input type="text" class="md-input" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <select id="select_demo_3" class="md-input">
                                                                    <option value="" selected>Choose Timezone</option>
                                                                    <?php
                                                                    $timezones = array('(GMT-11:00) Midway Island' => 'Pacific/Midway', '(GMT-11:00) Samoa' => 'Pacific/Samoa', '(GMT-10:00) Hawaii' => 'Pacific/Honolulu', '(GMT-09:00) Alaska' => 'US/Alaska', '(GMT-08:00) Pacific Time (US &amp; Canada)' => 'America/Los_Angeles', '(GMT-08:00) Tijuana' => 'America/Tijuana', '(GMT-07:00) Arizona' => 'US/Arizona', '(GMT-07:00) Chihuahua' => 'America/Chihuahua', '(GMT-07:00) La Paz' => 'America/Chihuahua', '(GMT-07:00) Mazatlan' => 'America/Mazatlan', '(GMT-07:00) Mountain Time (US &amp; Canada)' => 'US/Mountain', '(GMT-06:00) Central America' => 'America/Managua', '(GMT-06:00) Central Time (US &amp; Canada)' => 'US/Central', '(GMT-06:00) Guadalajara' => 'America/Mexico_City', '(GMT-06:00) Mexico City' => 'America/Mexico_City', '(GMT-06:00) Monterrey' => 'America/Monterrey', '(GMT-06:00) Saskatchewan' => 'Canada/Saskatchewan', '(GMT-05:00) Bogota' => 'America/Bogota', '(GMT-05:00) Eastern Time (US &amp; Canada)' => 'US/Eastern', '(GMT-05:00) Indiana (East)' => 'US/East-Indiana', '(GMT-05:00) Lima' => 'America/Lima', '(GMT-05:00) Quito' => 'America/Bogota', '(GMT-04:00) Atlantic Time (Canada)' => 'Canada/Atlantic', '(GMT-04:30) Caracas' => 'America/Caracas', '(GMT-04:00) La Paz' => 'America/La_Paz', '(GMT-04:00) Santiago' => 'America/Santiago', '(GMT-03:30) Newfoundland' => 'Canada/Newfoundland', '(GMT-03:00) Brasilia' => 'America/Sao_Paulo', '(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires', '(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires', '(GMT-03:00) Greenland' => 'America/Godthab', '(GMT-02:00) Mid-Atlantic' => 'America/Noronha', '(GMT-01:00) Azores' => 'Atlantic/Azores', '(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde', '(GMT+00:00) Casablanca' => 'Africa/Casablanca', '(GMT+00:00) Edinburgh' => 'Europe/London', '(GMT+00:00) Greenwich Mean Time : Dublin' => 'Etc/Greenwich', '(GMT+00:00) Lisbon' => 'Europe/Lisbon', '(GMT+00:00) London' => 'Europe/London', '(GMT+00:00) Monrovia' => 'Africa/Monrovia', '(GMT+00:00) UTC' => 'UTC', '(GMT+01:00) Amsterdam' => 'Europe/Amsterdam', '(GMT+01:00) Belgrade' => 'Europe/Belgrade', '(GMT+01:00) Berlin' => 'Europe/Berlin', '(GMT+01:00) Bern' => 'Europe/Berlin', '(GMT+01:00) Bratislava' => 'Europe/Bratislava', '(GMT+01:00) Brussels' => 'Europe/Brussels', '(GMT+01:00) Budapest' => 'Europe/Budapest', '(GMT+01:00) Copenhagen' => 'Europe/Copenhagen', '(GMT+01:00) Ljubljana' => 'Europe/Ljubljana', '(GMT+01:00) Madrid' => 'Europe/Madrid', '(GMT+01:00) Paris' => 'Europe/Paris', '(GMT+01:00) Prague' => 'Europe/Prague', '(GMT+01:00) Rome' => 'Europe/Rome', '(GMT+01:00) Sarajevo' => 'Europe/Sarajevo', '(GMT+01:00) Skopje' => 'Europe/Skopje', '(GMT+01:00) Stockholm' => 'Europe/Stockholm', '(GMT+01:00) Vienna' => 'Europe/Vienna', '(GMT+01:00) Warsaw' => 'Europe/Warsaw', '(GMT+01:00) West Central Africa' => 'Africa/Lagos', '(GMT+01:00) Zagreb' => 'Europe/Zagreb', '(GMT+02:00) Athens' => 'Europe/Athens', '(GMT+02:00) Bucharest' => 'Europe/Bucharest', '(GMT+02:00) Cairo' => 'Africa/Cairo', '(GMT+02:00) Harare' => 'Africa/Harare', '(GMT+02:00) Helsinki' => 'Europe/Helsinki', '(GMT+02:00) Istanbul' => 'Europe/Istanbul', '(GMT+02:00) Jerusalem' => 'Asia/Jerusalem', '(GMT+02:00) Kyiv' => 'Europe/Helsinki', '(GMT+02:00) Pretoria' => 'Africa/Johannesburg', '(GMT+02:00) Riga' => 'Europe/Riga', '(GMT+02:00) Sofia' => 'Europe/Sofia', '(GMT+02:00) Tallinn' => 'Europe/Tallinn', '(GMT+02:00) Vilnius' => 'Europe/Vilnius', '(GMT+03:00) Baghdad' => 'Asia/Baghdad', '(GMT+03:00) Kuwait' => 'Asia/Kuwait', '(GMT+03:00) Minsk' => 'Europe/Minsk', '(GMT+03:00) Nairobi' => 'Africa/Nairobi', '(GMT+03:00) Riyadh' => 'Asia/Riyadh', '(GMT+03:00) Volgograd' => 'Europe/Volgograd', '(GMT+03:30) Tehran' => 'Asia/Tehran', '(GMT+04:00) Abu Dhabi' => 'Asia/Muscat', '(GMT+04:00) Baku' => 'Asia/Baku', '(GMT+04:00) Moscow' => 'Europe/Moscow', '(GMT+04:00) Muscat' => 'Asia/Muscat', '(GMT+04:00) St. Petersburg' => 'Europe/Moscow', '(GMT+04:00) Tbilisi' => 'Asia/Tbilisi', '(GMT+04:00) Yerevan' => 'Asia/Yerevan', '(GMT+04:30) Kabul' => 'Asia/Kabul', '(GMT+05:00) Islamabad' => 'Asia/Karachi', '(GMT+05:00) Karachi' => 'Asia/Karachi', '(GMT+05:00) Tashkent' => 'Asia/Tashkent', '(GMT+05:30) Chennai' => 'Asia/Calcutta', '(GMT+05:30) Kolkata' => 'Asia/Kolkata', '(GMT+05:30) Mumbai' => 'Asia/Calcutta', '(GMT+05:30) New Delhi' => 'Asia/Calcutta', '(GMT+05:30) Sri Jayawardenepura' => 'Asia/Calcutta', '(GMT+05:45) Kathmandu' => 'Asia/Katmandu', '(GMT+06:00) Almaty' => 'Asia/Almaty', '(GMT+06:00) Astana' => 'Asia/Dhaka', '(GMT+06:00) Dhaka' => 'Asia/Dhaka', '(GMT+06:00) Ekaterinburg' => 'Asia/Yekaterinburg', '(GMT+06:30) Rangoon' => 'Asia/Rangoon', '(GMT+07:00) Bangkok' => 'Asia/Bangkok', '(GMT+07:00) Hanoi' => 'Asia/Bangkok', '(GMT+07:00) Jakarta' => 'Asia/Jakarta', '(GMT+07:00) Novosibirsk' => 'Asia/Novosibirsk', '(GMT+08:00) Beijing' => 'Asia/Hong_Kong', '(GMT+08:00) Chongqing' => 'Asia/Chongqing', '(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong', '(GMT+08:00) Krasnoyarsk' => 'Asia/Krasnoyarsk', '(GMT+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur', '(GMT+08:00) Perth' => 'Australia/Perth', '(GMT+08:00) Singapore' => 'Asia/Singapore', '(GMT+08:00) Taipei' => 'Asia/Taipei', '(GMT+08:00) Ulaan Bataar' => 'Asia/Ulan_Bator', '(GMT+08:00) Urumqi' => 'Asia/Urumqi', '(GMT+09:00) Irkutsk' => 'Asia/Irkutsk', '(GMT+09:00) Osaka' => 'Asia/Tokyo', '(GMT+09:00) Sapporo' => 'Asia/Tokyo', '(GMT+09:00) Seoul' => 'Asia/Seoul', '(GMT+09:00) Tokyo' => 'Asia/Tokyo', '(GMT+09:30) Adelaide' => 'Australia/Adelaide', '(GMT+09:30) Darwin' => 'Australia/Darwin', '(GMT+10:00) Brisbane' => 'Australia/Brisbane', '(GMT+10:00) Canberra' => 'Australia/Canberra', '(GMT+10:00) Guam' => 'Pacific/Guam', '(GMT+10:00) Hobart' => 'Australia/Hobart', '(GMT+10:00) Melbourne' => 'Australia/Melbourne', '(GMT+10:00) Port Moresby' => 'Pacific/Port_Moresby', '(GMT+10:00) Sydney' => 'Australia/Sydney', '(GMT+10:00) Yakutsk' => 'Asia/Yakutsk', '(GMT+11:00) Vladivostok' => 'Asia/Vladivostok', '(GMT+12:00) Auckland' => 'Pacific/Auckland', '(GMT+12:00) Fiji' => 'Pacific/Fiji', '(GMT+12:00) International Date Line West' => 'Pacific/Kwajalein', '(GMT+12:00) Kamchatka' => 'Asia/Kamchatka', '(GMT+12:00) Magadan' => 'Asia/Magadan', '(GMT+12:00) Marshall Is.' => 'Pacific/Fiji', '(GMT+12:00) New Caledonia' => 'Asia/Magadan', '(GMT+12:00) Solomon Is.' => 'Asia/Magadan', '(GMT+12:00) Wellington' => 'Pacific/Auckland', '(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu');
                                                                    ?>
                                                                    @foreach($timezones as $t=>$zone)
                                                                    <option value = "1">{{$t}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <label>Mobile Number</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label>Landline Number</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <label>Age</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label class="uk-form-label">Gender</label>
                                                                <div>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio"  id="male" data-md-icheck name="gender" value="M" />
                                                                        <label for="mal" class="inline-label">Male</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" id="female" data-md-icheck name="gender" value="F" />
                                                                        <label for="female" class="inline-label">Female</label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="uk-width-medium-1-2 uk-row-first">
                                                                <select id="select_demo_3" class="md-input">
                                                                    <option value="" selected>Choose Agent Subscription</option>
                                                                    <option value="1">Group 1</option>
                                                                    <option value="2">Group 2</option>
                                                                    <option value="3">Group 3</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Brief Description</label>
                                                                <textarea cols="30" rows="4" class="md-input"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-2-6">
                                                                <h3 class="heading_a uk-margin-small-bottom">Logo</h3>
                                                                <input type="file" id="input-file-a" class="dropify" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <div>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" name="settings_cache_type" id="settings_cache_file" data-md-icheck checked />
                                                                        <label for="settings_cache_file" class="inline-label">Active</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" name="settings_cache_type" id="settings_cache_mysql" data-md-icheck />
                                                                        <label for="settings_cache_mysql" class="inline-label">In Active</label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="form_hr">
                                            <div class="uk-grid uk-margin-medium-top uk-text-right">
                                                <div class="uk-width-1-1">
                                                    <a href="{{ route('agent.index') }}" class="md-btn md-btn-danger">Cancel</a>
                                                    <button type="submit" class="md-btn md-btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </li> 

                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Date Added</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Reminder</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>17/03/2019</td>
                                                    <td>non, bibendum sed, est. Nunc laoreet</td>
                                                    <td>eleifend nec, malesuada ut, sem.</td>
                                                    <td><i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>  1 Jan 2019 10 AM</td>
                                                    <td >
                                                        <a href="{{ route('agent.notes.edit', array('1','1')) }}" title="Edit notes" class=""><i class="md-icon material-icons">&#xE254;</i></a> 
                                                        <a href="{{ route('agent.notes.delete', array('1','1')) }}" onclick="return confirm('Are you sure to delete?');" title="Delete notes" class=""><i class="md-icon material-icons">delete</i></a> 
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Date Added</th>
                                                <th>Title</th>
                                                <th>Attachment</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>17/03/2019</td>
                                                    <td>PAN Card</td>
                                                    <td><img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image16.jpg" alt="" style="height: 50px;"></td>
                                                    <td>ID Card</td>
                                                    <td >
                                                        <a href="{{ route('agent.document.edit', array('1','1')) }}" title="Edit Document" class=""><i class="md-icon material-icons">edit</i></a> 
                                                        <a href="javascript:;" onclick="return confirm('Are you sure to delete?');" title="Delete Document" class=""><i class="md-icon material-icons">delete</i></a> 
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <div class="pricing_table_v2 pricing_table_v2_b uk-grid uk-grid-small uk-grid-width-medium-1-2 uk-grid-width-large-1-3 uk-margin-large-bottom" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                        <div>
                                            <div class="md-card">
                                                <div class="md-card-content padding-reset" style="min-height: 479px;">
                                                    <div class="pricing_table_plan">Active Subscription Plan</div>
                                                    <div class="pricing_table_price">
                                                        <!--<span class="currency">$</span>-->
                                                        Free
                                                        <!--<span class="period">monthly</span>-->
                                                    </div>
                                                    <!--                                                    <ul class="pricing_table_features">
                                                                                                            <li><strong>512MB</strong> Memory</li>
                                                                                                            <li><strong>1 Core</strong> Processor</li>
                                                                                                            <li><strong>20GB</strong> SSD Disk</li>
                                                                                                            <li><strong>1TB</strong> Transfer</li>
                                                                                                        </ul>-->
                                                    <!--                                                    <div class="pricing_table_select">
                                                                                                            <a href="#" class="md-btn md-btn-large">Get it now</a>
                                                                                                        </div>-->
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="uk-width-medium-1-2">
                                                <h2 class="heading_b uk-margin-bottom" style="display:inline-flex;color: #727272;">
                                                    Expire On: 
                                                    <span class="sub-heading" style="padding:4px">00-00-0000</span>
                                                </h2                                                        >
                                                <a href="javascript:,;" class="md-btn md-btn-large">Upgrade</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Invoice ID</th>
                                                <th>DateTime</th>
                                                <th>Particular</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>12345687</td>
                                                    <td>11/Jun/2018 12:23:10</td>
                                                    <td>Particular <?= $i ?></td>
                                                    <td><?= $i ?>,256</td>
                                                    <td><span class="label label-success">Active</span></td>
                                                    <td>
                                                        <a href="javascript:;" title="Print" class=""><i class="md-icon material-icons">print</i></a> 
                                                        <a href="javascript:;" title="E-mail" class=""><i class="md-icon material-icons">email</i></a> 
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Transaction ID</th>
                                                <th>DateTime</th>
                                                <th>Particular</th>
                                                <th>Debit</th>
                                                <th>Credit</th>
                                                <th>Balance</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>12345687</td>
                                                    <td>11/Jun/2018 12:23:10</td>
                                                    <td>Particular <?= $i ?></td>
                                                    <td>
                                                        <?php
                                                        if ($i % 2 == 0) {
                                                            echo $i . ",256";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($i % 2 != 0) {
                                                            echo $i . ",256";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?= $i ?>,256</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>                                                                                

            <div class="uk-modal" id="assign_group_tour">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Assign Group Tour</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Tour</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Gujarat Tour</option>
                                <option value="1">Kerala Tour</option>
                                <option value="2"                                                                                        >Kashmir Tour</option>
                                <option value="2">Rajasthan Tour</option>
                                <option value="2">Punjab Tour</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Departure Date</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">01 Jan 2019</option>
                                <option value="2">01 Feb 2019</option>
                                <option value="2">01 Mar 2019</option>
                                <option value="2">01 Jun 2019</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-modal-footer">
                        <div class="uk-text-left uk-width-medium-1-2 uk-float-left">
                            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-flat">Create New Group Tour</a>
                        </div>
                        <div class="uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" class="md-b                                                                                                                                                                                                        tn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="assign_customised_tour">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Assign Customised Tour</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Tour</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Gujarat Tour</option>
                                <option value="1">Kerala Tour</option>
                                <option value="2">Kashmir Tour</option>
                                <option value="2">Rajasthan Tour</option>
                                <option value="2">Punjab Tour</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                        <div class="uk-width-medium-1-1 ">
                            <div class="md-input-wrapper"><label for="invoice_dp">Departure Date</label><input class="md-input uk-width-1-1 " type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}"><span class="md-input-bar "></span></div>

                        </div>
                        <div class="clear clearfix"></div>
                    </div>
                    <div class="uk-modal-footer">
                        <div class="uk-text-left uk-width-medium-1-2 uk-float-left">
                            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-flat">Create New Customised Tour</a>
                        </div>
                        <div class="uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="update_status">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Update Status</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Status</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Status 1</option>
                                <option value="2">Status 2</option>
                                <option value="2">Status 3</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                    </div>
                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="update_status" class="uk-modal uk-hide uk-fade" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999;">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Update Status</h3>
        </div>
        <select id="selec_adv_s2_1" name="selec_adv_s2_1" class="select_no_search uk-width-large-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
            <option value="1">Status 1</option>
            <option value="2">Status 2</option>
            <option value="2">Status 3</option>
        </select>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button> <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Update</button>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script type="text/javascript">
    $(function () {
        if (location.hash && location.hash.length) {
            var hash = decodeURIComponent(location.hash.substr(1));
            setTimeout(
                    function ()
                    {
                        $("ul#user_profile_tabs li." + hash).trigger("click");
//                        alert(hash);
                    }, 1000);
        }
    });
</script>

@endsection