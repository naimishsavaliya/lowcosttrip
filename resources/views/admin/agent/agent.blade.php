@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        @can('add_agent')
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('agent.add')}}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
        @endcan
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table" id="agent_tbl">
                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Name</th>
                        <th>Email ID</th>
                        <th class="hidden-phone">Mobile No.</th>
                        <!--<th class="hidden-phone">Gender</th>-->
                        <th class="hidden-phone">City</th>
                        <th class="hidden-phone">Subscription</th>
                        <th class="hidden-phone">Follow-Up Note</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td class="pd0"><input class="md-input" type="text"  placeholder="ID" name="" colPos="0"></td>
                        <td><input class="md-input" type="text"  placeholder="Name" name="" colPos="1"></td>
                        <td><input class="md-input" type="text"  placeholder="Email ID" name="" colPos="2"></td>
                        <td><input class="md-input" type="text"  placeholder="Mobile No" name="" colPos="3"></td>
                        <td><input class="md-input" type="text"  placeholder="City" name="" colPos="4"></td>
                        <td>
                            <select class="md-input" data-placeholder="Choose a Status" tabindex="1" colPos="5">
                                <option value="">Choose Package</option>
                                @if($subscription_pkg)
                                @foreach($subscription_pkg as $package)
                                <option value="{{ $package->packId }}">{{ $package->name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </td>
                        <td></td>
                        <td>
                            <select class="md-input" data-placeholder="Choose a Status" tabindex="1" colPos="7">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="0">InActive</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                </tfoot>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="changestatus" class="uk-modal" >
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Status</h3>
        </div>
        <div class="">
            <div class="control-group">
                <input type="hidden" value="" id="change-status-row-id" />
                <label class="control-label">Status</label>
                <div class="controls">
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="1" id="status_dialog_active" data-md-icheck />
                        <label for="status_dialog_active" class="inline-label">Active</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="0" id="status_dialog_inactive" data-md-icheck />
                        <label for="status_dialog_inactive" class="inline-label">InActive</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close" id="status-dlg-btn">Submit</button> 
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type = "text/javascript" >
$(function () {

    var userTable = $('#agent_tbl').DataTable({
        processing: true,
        serverSide: true,
        pageLength: 20,
        lengthMenu: [ 10, 20, 50, 75, 100 ],
        destroy: true,
        searching: true,
//            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
//            "pagingType": "full_numbers", //full,full_numbers
        "ajax": {
            "url": "{{ route('agent.data') }}",
            "dataType": "json",
            "type": "POST",
            "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
            beforeSend: function () {

                if (typeof userTable != 'undefined' && userTable.hasOwnProperty('settings')) {
                    userTable.settings()[0].jqXHR.abort();
                }
            }
        },
        "columns": [
            {"data": "id", "name": "users.id"},
            {"data": "full_name", "name": "full_name"},
            {"data": "email", "name": "users.email"},
            {"data": "phone_no", "name": "users_profile.phone_no"},
            {"data": "city_name", "name": "city.city_name"},
            {"data": "name", "name": "subscription_package.packId"},
            {"data": "note", "name": "note"},
            {"data": "status", "name": "status", "render": function (data, type, row) {
                    if (data == 1) {
                        return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.id + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a>';
                    } else {
                        return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.id + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-danger">InActive</span></a>';
                    }
                },
            },
            {"data": "action", 'sortable': false, "render": function (data, type, row) {
                    var edit_url = "{{ route('agent.edit', ':id') }}";
                    var dele_url = "{{ route('agent.delete', ':id') }}";
                    var change_role_url = "{{ route('user.change.role', ':id') }}";
                    edit_url = edit_url.replace(':id', row.id);
                    dele_url = dele_url.replace(':id', row.id);
                    change_role_url = change_role_url.replace(':id', row.id);
                    var linkHtml ='';
                    @can('update_agent')
                    linkHtml += '<a href="' + edit_url + '" title="Edit User" class=""><i class="md-icon material-icons">&#xE254;</i></a> ';
                    @endcan
                    @can('delete_agent')
                    linkHtml += '<a href="' + dele_url + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete User" class=""><i class="md-icon material-icons">delete</i></a> ';
                    @endcan
                    return  linkHtml;
                },
            },
        ]
    });

    $(userTable.table().container()).on('keyup', 'tfoot input', function () {
        userTable
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

    $(userTable.table().container()).on('change', 'tfoot select', function () {
        userTable
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

    $(document.body).on('click', '.change-status-btn', function () {
        var $obj = $(this);
        var row_id = $obj.attr('row-id');
        var status = $obj.attr('data-id');
        $('input#change-status-row-id').val(row_id);
        if (status == "1") {
            $('#status_dialog_inactive').removeAttr('checked');
            $('#status_dialog_active').iCheck('check');
        } else {
            $('#status_dialog_active').removeAttr('checked');
            $('#status_dialog_inactive').iCheck('check');
        }
    });

    $(document.body).on('click', '#status-dlg-btn', function () {
        var $obj = $(this);
        var user_id = $('#change-status-row-id').val();
        var status = $('input.user-status-change:checked').val();
        $.ajax({
            type: 'get',
            url: '{{ route("change_status")}}',
            data: {
                "_token": "{{ csrf_token() }}",
                "table": "users",
                "id": user_id,
                "column_name": "id",
                "status": status
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    userTable.draw();
                }
                showNotify(data.msg_status, data.message);
            }
        });
    });

});
</script>
@endsection