@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('agent.index') }}">Agent</i></a></li>
            <li><span>Manage Follow Up</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('agent.notes.add', '1') }}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')
            <table class="uk-table dt_default">
                <thead>
                    <tr>
                        <th>Lead ID</th>
                        <th>Date Added</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Reminder</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>17/03/2019</td>
                            <td>non, bibendum sed, est. Nunc laoreet</td>
                            <td>eleifend nec, malesuada ut, sem.</td>
                            <td><i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>  1 Jan 2019 10 AM</td>
                            <td >
                                <a href="{{ route('agent.notes.edit', array('1','1')) }}" title="Edit notes" class=""><i class="md-icon material-icons">&#xE254;</i></a> 
                                <a href="{{ route('agent.notes.delete', array('1','1')) }}" onclick="return confirm('Are you sure to delete?');" title="Delete notes" class=""><i class="md-icon material-icons">delete</i></a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


