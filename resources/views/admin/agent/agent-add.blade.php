@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ route('agent.store') }}" name="agent_frm" id="agent_frm" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($users[0]->user_id) ? $users[0]->user_id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="title" class="md-input" name="title">
                                            <option value="">Choose Title</option>
                                            <option value="Mr." {{ (isset($users[0]->title) && $users[0]->title == "Mr.") ? 'selected' : '' }}>Mr.</option>
                                            <option value="Mrs." {{ (isset($users[0]->title) && $users[0]->title == "Mrs.") ? 'selected' : '' }}>Mrs.</option>
                                            <option value="Ms." {{ (isset($users[0]->title) && $users[0]->title == "Ms.") ? 'selected' : '' }}>Ms.</option>
                                            <option value="Dr." {{ (isset($users[0]->title) && $users[0]->title == "Dr.") ? 'selected' : '' }}>Dr.</option>
                                        </select>
                                        @if($errors->has('title'))<small class="text-danger">{{ $errors->first('title') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>First Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="first_name" value="{{ isset($users[0]->first_name) ? $users[0]->first_name : old('first_name') }}" />
                                        @if($errors->has('first_name'))<small class="text-danger">{{ $errors->first('first_name') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>Last Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="last_name" value="{{ isset($users[0]->last_name) ? $users[0]->last_name : old('last_name') }}" />
                                        @if($errors->has('last_name'))<small class="text-danger">{{ $errors->first('last_name') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>E-mail <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="email" value="{{ isset($users[0]->email) ? $users[0]->email : old('email') }}" />
                                        @if($errors->has('email'))<small class="text-danger">{{ $errors->first('email') }}</small>@endif
                                    </div>
                                    @if(isset($users[0]->id) == false)
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>Password <span class="required-lbl">*</span></label>
                                        <input type="password" class="md-input" name="password" id="password" />
                                        @if($errors->has('password'))<small class="text-danger">{{ $errors->first('password') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>Confirm Password <span class="required-lbl">*</span></label>
                                        <input type="password" class="md-input" name="confirm_password" id="confirm_password" />
                                        @if($errors->has('confirm_password'))<small class="text-danger">{{ $errors->first('confirm_password') }}</small>@endif
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Company Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="company_name" value="{{ isset($users[0]->company_name) ? $users[0]->company_name : old('company_name') }}" />
                                        @if($errors->has('company_name'))<small class="text-danger">{{ $errors->first('company_name') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>Website</label>
                                        <input type="text" class="md-input" name="website" id="website" value="{{ isset($users[0]->website) ? $users[0]->website : old('website') }}" />
                                        @if($errors->has('website'))<small class="text-danger">{{ $errors->first('website') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>Skype ID</label>
                                        <input type="text" class="md-input" name="skype_id" id="skype_id" value="{{ isset($users[0]->skype_id) ? $users[0]->skype_id : old('skype_id') }}" />
                                        @if($errors->has('skype_id'))<small class="text-danger">{{ $errors->first('skype_id') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Designation <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="designation" value="{{ isset($users[0]->designation) ? $users[0]->designation : old('designation') }}" />
                                        @if($errors->has('designation'))<small class="text-danger">{{ $errors->first('company_name') }}</small>@endif
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="time_zone" class="md-input" name="time_zone">
                                            <option value="">Choose Timezone</option>
                                            @if(isset($timezones))
                                            @foreach($timezones as $t=>$zone)
                                            <option value="{{ $t }}" {{ (isset($users[0]->time_zone) && $users[0]->time_zone == $t) ? 'selected' : '' }}>{{ $zone }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('time_zone'))<small class="text-danger">{{ $errors->first('time_zone') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2">
                                        <label>Mobile Number</label>
                                        <input type="text" class="md-input" name="phone_no" value="{{ isset($users[0]->phone_no) ? $users[0]->phone_no : old('phone_no') }}" />
                                        @if($errors->has('phone_no'))<small class="text-danger">{{ $errors->first('phone_no') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Landline Number</label>
                                        <input type="text" class="md-input" name="landline_no" value="{{ isset($users[0]->landline_no) ? $users[0]->landline_no : old('landline_no') }}" />
                                        @if($errors->has('landline_no'))<small class="text-danger">{{ $errors->first('landline_no') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2">
                                        <label>Age</label>
                                        <input type="text" class="md-input" name="age" value="{{ isset($users[0]->age) ? $users[0]->age : old('age') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="select_demo_3" class="md-input" name="gender">
                                            <option value="" selected>Choose Gender</option>
                                            <option value="M" {{ (isset($users[0]->gender) && $users[0]->gender == "M") ? 'selected' : '' }}>Male</option>
                                            <option value="F" {{ (isset($users[0]->gender) && $users[0]->gender == "F") ? 'selected' : '' }}>Female</option>
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-1-2 uk-row-first">
                                        <select id="subscription_id" class="md-input" name="subscription_id">
                                            <option value="">Choose Agent Subscription</option>
                                            @if(isset($subscription_pkg))
                                            @foreach($subscription_pkg as $package)
                                            <option value="{{ $package->packId }}" {{ (isset($users[0]->subscription_id) && $users[0]->subscription_id == $package->packId) ? 'selected' : '' }}>{{ $package->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('subscription_id'))<small class="text-danger">{{ $errors->first('subscription_id') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="manger_id" class="md-input" name="manger_id">
                                            <option value="">Choose Relationship Manager...</option>
                                            @if(isset($managers))
                                            @foreach($managers as $manager)
                                            <option value="{{ $manager->id }}" {{ (isset($users[0]->manager_id) && $users[0]->manager_id == $manager->id) ? 'selected' : '' }}>{{ $manager->first_name .' '. $manager->last_name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('time_zone'))<small class="text-danger">{{ $errors->first('time_zone') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1 p-r-50">
                                        <label>Brief Description</label>
                                        <textarea cols="30" rows="4" class="md-input" name="information">{{ isset($users[0]->information) ? $users[0]->information : old('information') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="heading_a p-t-20">Contact Detail</h3>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-6 p-r-50">
                                        <label>Name<span class="required-lbl">*</span></label>
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <div class="md-input-wrapper md-input-filled">
                                            <label>Accounts:</label>
                                            <input type="text" class="md-input label-fixed" name="contact_name_1" value="{{ isset($users[0]->contact_name_1) ? $users[0]->contact_name_1 : old('contact_name_1') }}" />
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <div class="md-input-wrapper md-input-filled">
                                            <label>Reservation/Operations:</label>
                                            <input type="text" class="md-input label-fixed" name="contact_name_2" value="{{ isset($users[0]->contact_name_2) ? $users[0]->contact_name_2 : old('contact_name_2') }}" />
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <div class="md-input-wrapper md-input-filled">
                                            <label>Management:</label>
                                            <input type="text" class="md-input label-fixed" name="contact_name_3" value="{{ isset($users[0]->contact_name_3) ? $users[0]->contact_name_3 : old('contact_name_3') }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-6 p-r-50">
                                        <label>Email:<span class="required-lbl">*</span></label>
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <input type="text" class="md-input" name="contact_email_1" value="{{ isset($users[0]->contact_email_1) ? $users[0]->contact_email_1 : old('contact_email_1') }}" />
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <input type="text" class="md-input" name="contact_email_2" value="{{ isset($users[0]->contact_email_2) ? $users[0]->contact_email_2 : old('contact_email_2') }}" />
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <input type="text" class="md-input" name="contact_email_3" value="{{ isset($users[0]->contact_email_3) ? $users[0]->contact_email_3 : old('contact_email_3') }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-6 p-r-50">
                                        <label>Number:<span class="required-lbl">*</span></label>
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <input type="text" class="md-input" name="contact_phone_1" value="{{ isset($users[0]->contact_phone_1) ? $users[0]->contact_phone_1 : old('contact_phone_1') }}" />
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <input type="text" class="md-input" name="contact_phone_2" value="{{ isset($users[0]->contact_phone_2) ? $users[0]->contact_phone_2 : old('contact_phone_2') }}" />
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <input type="text" class="md-input" name="contact_phone_3" value="{{ isset($users[0]->contact_phone_3) ? $users[0]->contact_phone_3 : old('contact_phone_3') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>


                        <h3 class="heading_a p-t-20">Company Profile</h3>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="agent_type" class="md-input" name="agent_type">
                                            <option value="">Choose Agent Type...</option>
                                            @if(isset($agent_types))
                                            @foreach($agent_types as $ag_key => $ag_value)
                                            <option value="{{ $ag_key }}" {{ (isset($users[0]->agent_type) && $users[0]->agent_type == $ag_key) ? 'selected' : '' }}>{{ $ag_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('agent_type'))<small class="text-danger">{{ $errors->first('agent_type') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>how old is your agency(In Year)</label>
                                        <input type="text" class="md-input" name="how_old_agency" value="{{ isset($users[0]->how_old_agency) ? $users[0]->how_old_agency : old('how_old_agency') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="booking_per_month" class="md-input" name="booking_per_month">
                                            <option value="">Choose Bookings per month...</option>
                                            @if(isset($booking_per_months))
                                            @foreach($booking_per_months as  $per_key => $per_value)
                                            <option value="{{ $per_key }}" {{ (isset($users[0]->booking_per_month) && $users[0]->booking_per_month == $per_key) ? 'selected' : '' }}>{{ $per_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('booking_per_month'))<small class="text-danger">{{ $errors->first('booking_per_month') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <select id="no_of_employee" class="md-input" name="no_of_employee">
                                            <option value="">Choose No of Employees...</option>
                                            @if(isset($no_of_employees))
                                            @foreach($no_of_employees as  $emp_key => $emp_value)
                                            <option value="{{ $emp_key }}" {{ (isset($users[0]->no_of_employee) && $users[0]->no_of_employee == $emp_key) ? 'selected' : '' }}>{{ $emp_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <select id="year_of_registration" class="md-input" name="year_of_registration">
                                            <option value="">Choose Year of Registration....</option>
                                            @if(isset($year_of_registation))
                                            @foreach($year_of_registation as  $year_key => $year_value)
                                            <option value="{{ $year_key }}" {{ (isset($users[0]->year_of_registration) && $users[0]->year_of_registration == $year_key) ? 'selected' : '' }}>{{ $year_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Address</label>
                                        <textarea cols="30" rows="4" class="md-input" name="address">{{ isset($users[0]->address) ? $users[0]->address : old('address') }}</textarea>
                                        @if($errors->has('address'))<small class="text-danger">{{ $errors->first('address') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2">
                                        <div class="uk-width-medium-1-12 display-inline-flex">
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <select class="md-input" name="country_id" id="country_id">
                                                    <option value="" selected>Choose Country</option>
                                                    @if(isset($countrys))
                                                    @foreach($countrys as $country)
                                                    <option value="{{ $country->country_id }}" {{ (isset($users[0]->country_id) && $users[0]->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @if($errors->has('country_id'))<small class="text-danger">{{ $errors->first('country_id') }}</small>@endif
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                <select class="md-input" name="state_id" id="state_id">
                                                    <option value="" selected>Choose State</option>
                                                    @if(isset($states))
                                                    @foreach($states as $state)
                                                    <option value="{{ $state->state_id }}" {{ (isset($users[0]->state_id) && $users[0]->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @if($errors->has('state_id'))<small class="text-danger">{{ $errors->first('state_id') }}</small>@endif
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-12 p-t-20 display-inline-flex">
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <select class="md-input" name="city_id" id="city_id">
                                                    <option value="" selected>Choose City</option>
                                                    @if(isset($cities))
                                                    @foreach($cities as $city)
                                                    <option value="{{ $city->city_id }}" {{ (isset($users[0]->city_id) && $users[0]->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @if($errors->has('city_id'))<small class="text-danger">{{ $errors->first('city_id') }}</small>@endif
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                <label>Pincode</label>
                                                <input type="text" class="md-input" name="pin_code" value="{{ isset($users[0]->pin_code) ? $users[0]->pin_code : old('pin_code') }}" />
                                                @if($errors->has('pin_code'))<small class="text-danger">{{ $errors->first('pin_code') }}</small>@endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="compnay_type" class="md-input" name="compnay_type">
                                            <option value="">Choose Company Type...</option>
                                            @if(isset($company_types))
                                            @foreach($company_types as  $comp_key => $comp_value)
                                            <option value="{{ $comp_key }}" {{ (isset($users[0]->compnay_type) && $users[0]->compnay_type == $comp_key) ? 'selected' : '' }}>{{ $comp_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('booking_per_month'))<small class="text-danger">{{ $errors->first('booking_per_month') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="where_hear_about" class="md-input" name="where_hear_about">
                                            <option value="">Choose  Where did you hear about us...</option>
                                            @if(isset($hear_about_us))
                                            @foreach($hear_about_us as  $hear_key => $hear_value)
                                            <option value="{{ $hear_key }}" {{ (isset($users[0]->where_hear_about) && $users[0]->where_hear_about == $hear_key) ? 'selected' : '' }}>{{ $hear_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Are you registered with any Travel Website?</label>
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="use_any_travel_web" value="1" id="use_any_travel_web_1" data-md-icheck {{ (isset($users[0]->use_any_travel_web) && $users[0]->use_any_travel_web == 1) ? 'checked' : ''  }} {{ (!isset($users)) ? 'checked' : '' }} />
                                                       <label for="use_any_travel_web_1" class="inline-label">Yes</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="use_any_travel_web" value="0" id="use_any_travel_web_0" data-md-icheck {{ (isset($users[0]->use_any_travel_web) && $users[0]->use_any_travel_web == 0) ? 'checked' : ''  }} />
                                                       <label for="use_any_travel_web_0" class="inline-label">No</label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="heading_a p-t-20">Specialisation</h3>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid m-t-10" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <h3 class="heading_a"><span class="sub-heading">Destinations you sell the most?</span></h3>
                                        <select id="destination_ids" name="destination_ids[]" class="uk-width-1-1" multiple data-md-select2>
                                            @if(isset($destinations))

                                            @php
                                            if(isset($users[0]->destination_ids))
                                            $design_arr = explode(',',$users[0]->destination_ids);
                                            @endphp

                                            @foreach($destinations as  $des_key => $des_value)
                                            <option value="{{ $des_value->desId }}" {{ (isset($users[0]->destination_ids) && in_array($des_value->desId,$design_arr)) ? 'selected' : '' }}>{{ $des_value->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <!--<div class="uk-grid m-t-10" data-uk-grid-margin>-->
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <h3 class="heading_a"><span class="sub-heading">Choose  Products Services</span></h3>
                                        <select id="products_services" name="products_services[]" class="uk-width-1-1" multiple data-md-select2>
                                            @if(isset($product_services))

                                            @php
                                            if(isset($users[0]->products_services))
                                            $serv_arr = explode(',',$users[0]->products_services);
                                            @endphp

                                            @foreach($product_services as  $ser_key => $ser_value)
                                            <option value="{{ $ser_key }}" {{ (isset($users[0]->products_services) && in_array($ser_key,$serv_arr)) ? 'selected' : '' }}>{{ $ser_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <!--</div>-->
                                    </div>

                                </div>
                                <h3 class="heading_a"><span class="sub-heading">Your current travellers are from which region ?</span></h3>
                                <div class="uk-grid m-t-10" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select class="md-input" name="current_travellers_regions" id="current_travellers_regions">
                                            <option value="" selected>Choose Country</option>
                                            @if(isset($countrys))
                                            @foreach($countrys as $country)
                                            <option value="{{ $country->country_id }}" {{ (isset($users[0]->current_travellers_regions) && $users[0]->current_travellers_regions == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select class="md-input" name="affiliations_id" id="affiliations_id">
                                            <option value="" selected>Choose Affiliations</option>
                                            @if(isset($affiliations))
                                            @foreach($affiliations as  $affi_key => $affi_value)
                                            <option value="{{ $affi_key }}" {{ (isset($users[0]->affiliations_id) && $users[0]->affiliations_id == $affi_key) ? 'selected' : '' }}>{{ $affi_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6">
                                        <h3 class="heading_a uk-margin-small-bottom">Profile</h3>
                                        <input type="file" id="input-file-a" name="image" class="dropify" data-default-file="{{  isset($users[0]->image) ?  env('APP_PUBLIC_URL').'uploads/user_profile/'.$users[0]->image : '' }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    @for ($doc_counter = 1; $doc_counter <= 6; $doc_counter++)
                                    <input type="hidden" name="document_hidden[]" value="{{ isset($documents[$doc_counter - 1]->docId) ? $documents[$doc_counter - 1]->docId : ''}}" />
                                    <div class="uk-width-medium-2-6">
                                        <h3 class="heading_a uk-margin-small-bottom">Document {{ $doc_counter }}</h3>
                                        <input type="file" id="document_{{ $doc_counter }}" name="document_{{ $doc_counter }}" class="dropify" data-default-file="{{ isset($documents[$doc_counter - 1]->doc_file_name) ?  env('APP_PUBLIC_URL').'uploads/documents/'.$agent_id.'/'.$documents[$doc_counter - 1]->doc_file_name : '' }}" />
                                    </div>
                                    @endfor
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="status_1" data-md-icheck {{ (isset($users[0]->status) && $users[0]->status == 1) ? 'checked' : ''  }} {{ (!isset($users)) ? 'checked' : '' }} />
                                                       <label for="status_1" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="status_0" data-md-icheck {{ (isset($users[0]->status) && $users[0]->status == 0) ? 'checked' : ''  }} />
                                                       <label for="status_0" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('agent.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                </div>
                        </div>-->

        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
// It has the name attribute "agent_frm"
    $("form#agent_frm").validate({
// Specify validation rules
        rules: {
            title: "required",
            first_name: "required",
            last_name: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                required: true,
                equalTo: "#password"
            },
            phone_no: "required",
            landline_no: "required",
            time_zone: "required",
            address: "required",
            country_id: "required",
            state_id: "required",
            city_id: "required",
            pin_code: "required",
            subscription_id: "required",
            manger_id: "required",
            contact_name_1: "required",
            contact_email_1: "required",
            contact_phone_1: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            title: "Please select title",
            first_name: "Please enter firstname",
            last_name: "Please enter lastname",
            email: "Please enter a valid email address",
            password: {
                required: "Please enter a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please enter a confirm password",
                equalTo: "Enter Confirm Password Same as Password"
            },
            phone_no: "Please enter phone number",
            landline_no: "Please enter landline number",
            time_zone: "Please selet timezone",
            address: "Please enter address",
            country_id: "Please select country id",
            state_id: "Please select state",
            city_id: "Please select city",
            pin_code: "Please enter pin code",
            subscription_id: "Please select subscription package",
            manger_id: "Please select relationship manager",
            contact_name_1: "Please enter name",
            contact_email_1: "Please enter email",
            contact_phone_1: "Please enter phone number",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    $('#country_id').change(function () {
        var country_id = $(this).val();
        var url = '{{ route("state_by_country", ":country_id") }}';
        url = url.replace(':country_id', country_id);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#state_id')
                        .find('option')
                        .remove();
                $('#state_id').append('<option value="">Choose State</option>');
                $.map(data, function (item) {
                    $('#state_id').append('<option value="' + item.state_id + '">' + item.state_name + '</option>');
                });
                $("#state_id").trigger("chosen:updated");
                $("#state_id").trigger("liszt:updated");
            }
        });
    });

    $('#state_id').change(function () {
        var state_id = $(this).val();
        var url = '{{ route("city_by_state", ":state_id") }}';
        url = url.replace(':state_id', state_id);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#city_id')
                        .find('option')
                        .remove();
                $('#city_id').append('<option value="">Choose City</option>');
                $.map(data, function (item) {
                    $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
                });
                $("#city_id").trigger("chosen:updated");
                $("#city_id").trigger("liszt:updated");
            }
        });
    });
});

</script>
@endsection