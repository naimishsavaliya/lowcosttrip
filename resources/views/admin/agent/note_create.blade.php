@extends('layouts.theme')
@section('content')

<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('agent.view', '1') }}">Travel Agent</i></a></li>
            <li><span>Add / Edit Follow Up Notes</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ route('agent.view', '1') }}" class="" method="get">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-50-10">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label>Title</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label>Description</label>
                                        <textarea class="md-input autosized" row="2" ></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="uk-width-medium-1-1">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label for="uk_dp_1">Follow Up Date</label>
                                        <input class="md-input" type="text" data-uk-datepicker="{format:'DD.MM.YYYY'}">
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label for="uk_dp_1">Follow Up Time</label>
                                        <input class="md-input" type="text" id="uk_tp_1" data-uk-timepicker>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('agent.view', '1') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                </div>
                        </div>-->

        </div>
    </div>

</div>

@endsection

