@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Sector</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('sector.add') }}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')
            <table id="sector_list" class="uk-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Sector Name</th>
                        <th>Category</th>
                        <th class="hidden-phone">Country</th>
                        <th class="hidden-phone">State</th>
                        <th class="hidden-phone">City</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td class="pd0"><div class="md-input-wrapper"><input placeholder="ID" name="" colPos="0" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td ><div class="md-input-wrapper"><input placeholder="Sector Name" name="" colPos="1" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td ><div class="md-input-wrapper"><input placeholder="Category" name="" colPos="2" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td ><div class="md-input-wrapper"><input placeholder="Country" name="" colPos="3" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td ><div class="md-input-wrapper"><input placeholder="State" name="" colPos="4" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td ><div class="md-input-wrapper"><input placeholder="City" name="" colPos="5" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td ><select  class="md-input uk-form-width-small" data-placeholder="Choose a Status" tabindex="1" colPos="6">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="2">InActive</option>
                            </select></td>
                        <td></td>
                    </tr>
                </tfoot>
                <tbody>

                </tbody>
            </table>
            <!--</div>-->
        </div>
    </div>
</div>


<script type="text/javascript">
//    $(function () {
    $(document).ready(function () {

        var categoryTable = $('#sector_list').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
            "ajax": {
                "url": "{{ url('admin/sector/data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof categoryTable != 'undefined' && categoryTable.hasOwnProperty('settings')) {
                        categoryTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "id", "name": "id"},
                {"data": "name", "name": "name"},
                {"data": "category_id", "name": "category_id", "render": function (data, type, row) {
                        if (data == 1) {
                            return 'Domestic';
                        } else {
                            return 'International';
                        }
                    },
                },
                {"data": "country_id", "name": "country_id"},
                {"data": "state_id", "name": "state_id"},
                {"data": "city_id", "name": "city_id"},
//                {"data": "created_by", "name": "created_by"},
//                {"data": "created_at", "name": "created_at"},
                {"data": "status", "name": "status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<span class="uk-badge uk-badge-success">Active</span>';
                        } else {
                            return '<span class="uk-badge uk-badge-danger">InActive</span>';
                        }
                    },
                },
                {"data": "action",  "render": function (data, type, row) {
                        return '<a href="' + ADMIN_URL + 'sector/edit/' + row.id + '" title="Edit Sector" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + ADMIN_URL + 'sector/delete/' + row.id + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete Sector" class=""><i class="md-icon material-icons">delete</i></a>';

                    },
                },
            ]
        });

        $(categoryTable.table().container()).on('keyup', 'tfoot input', function () {
            categoryTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(categoryTable.table().container()).on('change', 'tfoot select', function () {
            categoryTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

    });
</script>
@endsection