@extends('layouts.admin')
@section('content')


<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
                    </li>
                    <li><a href="#">User 1 Profile</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>


        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-user"></i> Profile</h4>
                        <span class="tools">
                        </span>                    
                    </div>
                    <div class="widget-body">
                        <div class="span3">
                            <div class="text-center profile-pic">
                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/profile-pic.jpg" alt="">
                            </div>
                            <!--                            <ul class="nav nav-tabs nav-stacked">
                                                            <li><a href="#courseDetail" data-toggle="modal" title="View Courses"><i class="icon-globe"></i> Courses</a></li>
                                                            <li><a href="#betchDetail" data-toggle="modal" title="View Batches"><i class="icon-bar-chart"></i> Batches</a></li>
                                                            <li><a href="#paymentDetail" data-toggle="modal" title="View Payments"><i class="icon-money"></i> Payments</a></li>
                                                            <li><a href="#assignments" data-toggle="modal" title="View Assignments"><i class="icon-copy"></i> Assignments</a></li>
                                                            <li><a href="#feedback" data-toggle="modal" title="View Feedback"><i class="icon-comments"></i> Feedback</a></li>
                                                            <li><a href="javascript:void(0)"><i class="icon-star"></i> Ranking or Rating</a></li>
                                                            <li><a href="javascript:void(0)"><i class="icon-github"></i> Github</a></li>
                                                        </ul>-->
                        </div>
                        <div class="span7">
                            <h4>User 1 <br/><small>Admin</small></h4>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td class="span2">First Name :</td>
                                        <td>
                                            Student
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">Last Name :</td>
                                        <td>
                                            Kher
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2"> Email :</td>
                                        <td>
                                            abc@abc.com
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2"> Mobile :</td>
                                        <td>
                                            12345677
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2"> Landline Number :</td>
                                        <td>
                                            12345677
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">Address :</td>
                                        <td>
                                            38001 SG Highway
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">City :</td>
                                        <td>
                                            Ahmedabad, Gujarat, India 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">Birthday :</td>
                                        <td>
                                            13 july 1983 (35 Years)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">Gender :</td>
                                        <td>
                                            Male
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">Timezone :</td>
                                        <td>
                                            (GMT+05:30) Mumbai
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="clear clearfix"></div><br/>
                            <h4>Brief Description</h4>
                            <hr>

                            <p class="push">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor. Vestibulum ullamcorper, odio sed rhoncus imperdiet, enim elit sollicitudin orci, eget dictum leo mi nec lectus. Nam commodo turpis id lectus scelerisque vulputate. Integer sed dolor erat. Fusce erat ipsum, varius vel euismod sed, tristique et lectus? Etiam egestas fringilla enim, id convallis lectus laoreet at. Fusce purus nisi, gravida sed consectetur ut, interdum quis nisi. Quisque egestas nisl id lectus facilisis scelerisque? Proin rhoncus dui at ligula vestibulum ut facilisis ante sodales! Suspendisse potenti. Aliquam tincidunt sollicitudin sem nec ultrices. Sed at mi velit. Ut egestas tempor est, in cursus enim venenatis eget! Nulla quis ligula ipsum. Donec vitae ultrices dolor?</p>


                            <div class="clear clearfix"></div><br/>





                        </div>

                        <div class="space5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection