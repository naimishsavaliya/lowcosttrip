@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Teachers Payment Due</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>


        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Teachers Payment Due</h4>
                    </div>
                    <div class="widget-body">
                        <table class="table table-striped table-bordered" id="sample_1">
                            <thead>
                                <tr>
                                    <th style="width: 25px;">ID</th>
                                    <th>Teacher Name</th>
                                    <th class="hidden-phone">Mobile No.</th>
                                    <th class="hidden-phone">Course</th>
                                    <th class="hidden-phone">Batch</th>
                                    <th class="hidden-phone">Student</th>
                                    <th class="hidden-phone">Date</th>
                                    <th class="hidden-phone">Amount</th>
                                    <th style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Teacher Name" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Mobile No." name="" colPos="1"></td>
                                    <td ><select class="chosen" data-placeholder="Choose a Course" tabindex="1" colPos="6">
                                            <option value="">Course</option>
                                            <option value="1">Course 1</option>
                                            <option value="2">Course 2</option>
                                            <option value="2">Course 3</option>
                                        </select></td>
                                    <td ><select class="chosen" data-placeholder="Choose a Batch" tabindex="1" colPos="6">
                                            <option value="">Batch</option>
                                            <option value="1">Batch 1</option>
                                            <option value="2">Batch 2</option>
                                            <option value="2">Batch 3</option>
                                        </select></td>
                                    <td ><input type="text"  placeholder="Students" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Date" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Amount" name="" colPos="1"></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td>Teacher <?php echo $i; ?></td>
                                        <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                        <td class="hidden-phone">Course <?php echo rand(1, 5); ?></td>
                                        <td class="hidden-phone">Batch <?php echo rand(1, 5); ?></td>
                                        <td class="hidden-phone"><?php echo rand(3, 7); ?></td>
                                        <td class="hidden-phone">12 Jan To 15 Apr 2018</td>
                                        <td class="hidden-phone"><?php echo rand(20000, 30000); ?></td>
                                        <td >
                                            <a href="{{ env('ADMIN_URL')}}teacher/paymentadd/1" title="Pay Amount" class="btn btn-mini btn-info"><i class="icon-money icon-white"></i></a> 
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
            </div>
        </div> 
    </div>
</div>
<div id="history" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel1">Student 1 History</h3>

    </div>
    <div class="modal-body">

        <ul class="chats normal-chat">
            <li class="in">
                <img class="avatar" alt="" src="{{ env('APP_PUBLIC_URL')}}admin/img/avatar1.jpg" />
                <div class="message ">
                    <span class="arrow"></span>
                    <a href="#" class="name">Mosaddek</a>
                    <span class="datetime">at Apr 14, 2013 3:23</span>
                    <span class="body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. dolore magna aliquam erat volutpat. dolore magna aliquam erat volutpat.
                    </span>
                </div>
            </li>
            <li class="out">
                <img class="avatar" alt="" src="{{ env('APP_PUBLIC_URL')}}admin/img/avatar2.jpg" />
                <div class="message">
                    <span class="arrow"></span>
                    <a href="#" class="name">Dulal Khan</a>
                    <span class="datetime">at Apr 14, 2013 3:29</span>
                    <span class="body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    </span>
                </div>
            </li>
            <li class="in">
                <img class="avatar" alt="" src="{{ env('APP_PUBLIC_URL')}}admin/img/avatar3.jpg" />
                <div class="message">
                    <span class="arrow"></span>
                    <a href="#" class="name">Munshi Kamal</a>
                    <span class="datetime">at Apr 14, 2013 3:31</span>
                    <span class="body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.													</span>
                </div>
            </li>
        </ul>
        <div class="chat-form">
            <div class="input-cont">
                <input type="text" placeholder="Type a message here..." />
            </div>
            <div class="btn-cont">
                <a href="javascript:;" class="btn btn-primary">Send</a>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
<div id="changestatus" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel1">Student 1</h3>

    </div>
    <div class="modal-body">

        <div class="control-group">
            <label class="control-label">Status</label>
            <div class="controls">
                <label class="radio">
                    <input type="radio" name="optionsRadios1" value="option1" checked />
                    Active
                </label>
                <label class="radio">
                    <input type="radio" name="optionsRadios1" value="option1" />
                    InActive
                </label>
                <label class="radio">
                    <input type="radio" name="optionsRadios1" value="option1" />
                    Remove From Batch
                </label>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Remark</label>
            <div class="controls">
                <textarea class="span12 " rows="3"></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="button" data-dismiss="modal"  class="btn btn-primary">Submit</button> 
    </div>
</div>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection