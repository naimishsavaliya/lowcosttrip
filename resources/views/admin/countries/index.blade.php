@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content_inner">

        <div class="md-card uk-margin-medium-bottom" style="box-shadow: none;">
            <div class="md-card-content">
                @include('admin.includes.alert')
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <ul class="uk-tab" data-uk-tab="{connect:'#country_state_city_content'}" id="country_state_city">
                            <li class="uk-active loaded"><a href="#">Country</a></li>
                            <li class=""><a href="#">State</a></li>
                            <li class="named_tab"><a href="#">City</a></li>
                        </ul>
                        <ul id="country_state_city_content" class="uk-switcher uk-margin">
                            <li>
                                <div class="uk-grid uk-margin-medium-top uk-text-right">
                                    <div class="uk-width-1-1">
                                        <a href="{{ route('country.add') }}" class="md-btn md-btn-primary">Add New</a>
                                    </div>
                                </div>
                                <table class="uk-table" id="country_table">
                                    <thead>
                                        <tr>
                                            <th style="width: 25px;">ID</th>
                                            <th>Name</th>
                                            <th>Country Code</th>
                                            <th>Phone Code</th>
                                            <th class="hidden-phone" style="width: 10%;">Status</th>
                                            <th style="width: 15%;">Action</th>
                                        </tr>
                                    </thead>
                                    <!-- <tfoot>
                                        <tr>
                                            <td>
                                                <div class="md-input-wrapper"><input placeholder="ID" type="text" class="md-input" colPos="0"><span class="md-input-bar "></span></div>
                                            </td>
                                            <td>
                                                <div class="md-input-wrapper"><input placeholder="Name" type="text" class="md-input" colPos="1"><span class="md-input-bar "></span></div>
                                            </td>
                                            <td>
                                                <div class="md-input-wrapper"><input placeholder="Country Code" type="text" class="md-input" colPos="2"><span class="md-input-bar "></span></div>
                                            </td>
                                            <td>
                                                <div class="md-input-wrapper"><input placeholder="Phone Code" type="text" class="md-input" colPos="3"><span class="md-input-bar "></span></div>
                                            </td>
                                            <td >
                                                <select id="select_demo_1" class="md-input uk-form-width-small" colPos="4">
                                                    <option value="1">Active</option>
                                                    <option value="2">InActive</option>
                                                </select>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                                    </tbody>
                                </table>
                            </li>
                            <li>
                                <div class="uk-grid uk-margin-medium-top uk-text-right">
                                    <div class="uk-width-1-1">
                                        <a href="{{ route('state.add') }}" class="md-btn md-btn-primary">Add New</a>
                                    </div>
                                </div>
                                <div class="clear clearfix"> </div>
                                <table class="uk-table" id="state_table" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 25px;">ID</th>
                                            <th>State Name</th>
                                            <th>Country Name</th>
                                            <th>Status</th>
                                            <th >Action</th>
                                        </tr>
                                    </thead>
                                    <!-- <tfoot>
                                        <tr>
                                            <td><div class="md-input-wrapper"><input placeholder="ID" type="text" class="md-input" colPos="0"><span class="md-input-bar "></span></div></td>
                                            <td><div class="md-input-wrapper"><input placeholder="State Name" type="text" class="md-input" colPos="1"><span class="md-input-bar "></span></div></td>
                                            <td><div class="md-input-wrapper"><input placeholder="Country Name" type="text" class="md-input" colPos="2"><span class="md-input-bar "></span></div></td>
                                            <td >
                                                <select id="select_demo_1" class="md-input uk-form-width-small" colPos="3">
                                                    <option value="1">Active</option>
                                                    <option value="2">InActive</option>
                                                </select>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>

                                    </tbody>
                                </table>
                            </li>
                            <li>
                                <div class="uk-grid uk-margin-medium-top uk-text-right">
                                    <div class="uk-width-1-1">
                                        <a href="{{ route('city.add') }}" class="md-btn md-btn-primary">Add New</a>
                                    </div>
                                </div>
                                <div class="clear clearfix"> </div>
                                <table class="uk-table" id="city_table" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 25px;">ID</th>
                                            <th>City Name</th>
                                            <th>State Name</th>
                                            <th>Country Name</th>
                                            <th class="hidden-phone" style="width: 10%;">Status</th>
                                            <th style="width: 15%;">Action</th>
                                        </tr>
                                    </thead>
                                    <!-- <tfoot>
                                        <tr>
                                            <td><div class="md-input-wrapper"><input placeholder="ID" type="text" class="md-input" colPos="0"><span class="md-input-bar "></span></div></td>
                                            <td><div class="md-input-wrapper"><input placeholder="City Name" type="text" class="md-input" colPos="1"><span class="md-input-bar "></span></div></td>
                                            <td><div class="md-input-wrapper"><input placeholder="State Name" type="text" class="md-input" colPos="2"><span class="md-input-bar "></span></div></td>
                                            <td><div class="md-input-wrapper"><input placeholder="Country Name" type="text" class="md-input" colPos="3"><span class="md-input-bar "></span></div></td>
                                            <td >
                                                <select id="select_demo_1" class="md-input uk-form-width-small" colPos="4">
                                                    <option value="1">Active</option>
                                                    <option value="2">InActive</option>
                                                </select>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                                        <?php for ($i = 1; $i <= 0; $i++) { ?>
                                            <tr class="odd gradeX">
                                                <td><?php echo $i; ?></td>
                                                <td>City <?php echo $i; ?></td>
                                                <td><?php echo $i; ?></td>
                                                <td>State <?php echo $i; ?></td>
                                                <td>Country <?php echo $i; ?></td>
                                                <td ><a class="" data-uk-modal="{target:'#changestatus'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a></td>
                                                <td>
                                                    <a href="{{ route('city.edit', '1') }}" title="Edit Country" class=""><i class="md-icon material-icons">edit</i></a> 
                                                    <a href="{{ route('city.delete', '1') }}" onclick="return confirm('Are you sure to delete?');" title="Delete Country" class=""><i class="md-icon material-icons">delete</i></a> 
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@section('javascript')
<script type="text/javascript">
    $(function () {


        var countryTable = $('#country_table').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
            searching: true,
            order: [[ 0, "desc" ]],
            "ajax": {
                "url": "{{ route('country.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof countryTable != 'undefined' && countryTable.hasOwnProperty('settings')) {
                        countryTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "country_id", "name": "country_id"},
                {"data": "country_name", "name": "country_name"},
                {"data": "sortname", "name": "sortname"},
                {"data": "phonecode", "name": "phonecode"},
                {"data": "status", "name": "status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<span class="uk-badge uk-badge-success">Active</span>';
                        } else {
                            return '<span class="uk-badge uk-badge-danger">InActive</span>';
                        }
                    },
                },
                {"data": "action", "render": function (data, type, row) {
                        var edit_url = "{{ route('country.edit', ':id') }}";
                        var dele_url = "{{ route('country.delete', ':id') }}";
                        edit_url = edit_url.replace(':id', row.country_id);
                        dele_url = dele_url.replace(':id', row.country_id);
                        return '<a href="' + edit_url + '" title="Edit Country" class=""><i class="md-icon material-icons">edit</i></a> <a href="' + dele_url + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete Country" class=""><i class="md-icon material-icons">delete</i></a>';
                    },
                },
            ]
        });

        $(countryTable.table().container()).on('keyup', 'tfoot input', function () {
            countryTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(countryTable.table().container()).on('change', 'tfoot select', function () {
            countryTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

//////////////////// state /////////////////////////

        var stateTable = $('#state_table').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
            searching: true,
            order: [[ 0, "desc" ]],
            "ajax": {
                "url": "{{ route('state.data')}}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {
                    if (typeof stateTable != 'undefined' && stateTable.hasOwnProperty('settings')) {
                        stateTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "state_id", "name": "state.state_id"},
                {"data": "state_name", "name": "state.state_name"},
                {"data": "country_name", "name": "country.country_name"},
                {"data": "status", "name": "state.status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<span class="uk-badge uk-badge-success">Active</span>';
                        } else {
                            return '<span class="uk-badge uk-badge-danger">InActive</span>';
                        }
                    },
                },
                {"data": "action", "render": function (data, type, row) {
                        var edit_url = "{{ route('state.edit', ':id') }}";
                        var dele_url = "{{ route('state.delete', ':id') }}";
                        edit_url = edit_url.replace(':id', row.state_id);
                        dele_url = dele_url.replace(':id', row.state_id);
                        return '<a href="' + edit_url + '" title="Edit State" class=""><i class="md-icon material-icons">edit</i></a> <a href="' + dele_url + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete State" class=""><i class="md-icon material-icons">delete</i></a>';

                    },
                },
            ]
        });

        $(stateTable.table().container()).on('keyup', 'tfoot input', function () {
            stateTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(stateTable.table().container()).on('change', 'tfoot select', function () {
            stateTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
//////////////////// city /////////////////////////

        var cityTable = $('#city_table').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
            searching: true,
            order: [[ 0, "desc" ]],
            "ajax": {
                "url": "{{ route('city.data')}}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof cityTable != 'undefined' && cityTable.hasOwnProperty('settings')) {
                        cityTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "city_id", "name": "city.city_id"},
                {"data": "city_name", "name": "city.city_name"},
                {"data": "state_name", "name": "state.state_name"},
                {"data": "country_name", "name": "country.country_name"},
                {"data": "status", "name": "city.status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<span class="uk-badge uk-badge-success">Active</span>';
                        } else {
                            return '<span class="uk-badge uk-badge-danger">InActive</span>';
                        }
                    },
                },
                {"data": "action", "render": function (data, type, row) {
                        return '<a href="' + ADMIN_URL + 'city/edit/' + row.city_id + '" title="Edit City" class=""><i class="md-icon material-icons">edit</i></a> <a href="' + ADMIN_URL + 'city/delete/' + row.city_id + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete City" class=""><i class="md-icon material-icons">delete</i></a>';

                    },
                },
            ]
        });


        $(cityTable.table().container()).on('keyup', 'tfoot input', function () {
            cityTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(cityTable.table().container()).on('change', 'tfoot select', function () {
            cityTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });




//        $("ul#country_state_city li").click(function () {
//
//            var tabNo = $(this).index();
//            if ($(this).hasClass('loaded')) {
//            } else {
//                $(this).addClass('loaded');
//                if (tabNo == 1) {
//
//                }
//                if (tabNo == 2) {
//
//                }
//            }
//        });
    });
</script>
@endsection