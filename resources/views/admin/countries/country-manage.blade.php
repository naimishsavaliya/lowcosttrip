@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('country.save') }}" id="country_form" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($country->country_id) ? $country->country_id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Country Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="country_name" value="{{ isset($country->country_name) ? $country->country_name : old('country_name') }}" />
                                        <small class="text-danger">{{ $errors->first('country_name') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Country Code <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="sortname" value="{{ isset($country->sortname) ? $country->sortname : old('sortname') }}" />
                                        <small class="text-danger">{{ $errors->first('sortname') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Phone Code <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="phonecode" value="{{ isset($country->phonecode) ? $country->phonecode : old('phonecode') }}" />
                                        <small class="text-danger">{{ $errors->first('phonecode') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="status-column">
                                        <span class="icheck-inline">
                                            <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($country->status) && $country->status == 1) ? 'checked' : ''  }} {{ (!isset($country)) ? 'checked' : '' }} />
                                                   <label for="active" class="inline-label">Active</label>
                                        </span>
                                        <span class="icheck-inline">
                                            <input type="radio" name="status" value="2" id="inactive" data-md-icheck {{ (isset($country->status) && $country->status == 2) ? 'checked' : ''  }} />
                                                   <label for="inactive" class="inline-label">In Active</label>
                                        </span>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('status') }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('country.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

@endsection
@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>
<script type="text/javascript">
    $(function () {
// It has the name attribute "category_frm"
        $("form#country_form").validate({
// Specify validation rules
            rules: {
                country_name: "required",
                sortname: "required",
                phonecode: "required",
                status: "required",
    },
    // Specify validation error messages
    messages: {
        country_name: "Please enter country name",
        sortname: "Please enter country code",
        phonecode: "Please enter country phone code",
        status: "Please select status",
    },
    errorPlacement: function (error, element) {
        if (element.attr("type") == "radio") {
            error.insertAfter($(element).parents('div.status-column'));
        } else {
            error.insertAfter($(element).parents('div.md-input-wrapper'));
        }
    },
    // in the "action" attribute of the form when valid
    submitHandler: function (form) {
        form.submit();
    }
});

});

</script>
@endsection
