@extends('layouts.theme')

@section('style')
<!-- kendo UI -->
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/kendo-ui/styles/kendo.common-material.min.css"/>
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/kendo-ui/styles/kendo.material.min.css" id="kendoCSS"/>
<style>
    .heading_a{
        font-weight: bold;
    }
    ul#horizontal-list {
        min-width: 696px;
        list-style: none;
        padding-top: 20px;
        padding-left: 0px !important;
    }
    ul#horizontal-list li {
        display: inline;
        font-size: 20px;
        padding: 5px;
    }
</style>
@endsection

@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Activity Details</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}hotel/add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Activity
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <div class="uk-grid blog_article">
                <div class="uk-width-medium-1-12">
                    <h1 class="uk-article-title">
                        Sailing & Snorkeling
                    </h1>
                    <div class="uk-article-meta">
                        <ul id="horizontal-list">
                            <li style="color:red;"><span>Overview</span></li>
                            <li><span>Inclusion</span></li>
                            <li><span>Schedule</span></li>
                            <li><span>Policies</span></li>
                        </ul>
                    </div>	
                </div>
                <div class="uk-width-medium-7-10">
                    <div>
                        <div class="large-padding">
                            <div class="uk-article">
                                <div class="uk-slidenav-position" data-uk-slideshow="{animation:'scale'}">
                                    <ul class="uk-slideshow">
                                        <li><img src="{{ env('APP_PUBLIC_URL')}}/uploads/mo-freitas1.jpeg" alt=""></li>
                                        <li><img src="{{ env('APP_PUBLIC_URL')}}/uploads/panama-city-beach-jet-ski-rental-624x376.jpg" alt=""></li>
                                        <li><img src="{{ env('APP_PUBLIC_URL')}}/uploads/ebbfcb0eb00a10a19a843ecc570f13c2--trek-bikes-watch-video.jpg" alt=""></li>
                                        <li><img src="{{ env('APP_PUBLIC_URL')}}/uploads/Buggie-ride-624x376.jpg" alt=""></li>
                                    </ul>
                                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                                    <ul class="hotel-img-slide uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
                                        <li data-uk-slideshow-item="0"><a href=""></a></li>
                                        <li data-uk-slideshow-item="1"><a href=""></a></li>
                                        <li data-uk-slideshow-item="2"><a href=""></a></li>
                                        <li data-uk-slideshow-item="3"><a href=""></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-3-10 p-l-0 bg-pink">
                    <div class="md-card md-box bg-pink brd-radius-0" style="padding-bottom: 2px;">
                        <div class="md-card-content large-padding">
                            <div style="padding:30px 0 20px 0;">
                                <div class="pricing_table_price position-relative">
                                    <h2 class="heading_a p-10" style="padding: 10px 0;color:#FFF;">Highlights</h2>
                                    <div class="period text-white" style="margin-left: 10px;">Enjoy Flyboarding in Goa under Instructrors</div>
                                    <div class="period text-white" style="margin-left: 10px;">Get a change to enjoy catamaran cruise ride and kayaking as well</div>
                                </div>

                                <div class="pricing_table_price position-relative">
                                    <h2 class="heading_a p-10" style="padding: 10px 0;color:#FFF;">Meeting Point</h2>
                                    <div class="period text-white" style="margin-left: 10px;">South Goa</div>
                                </div>

                                <div class="pricing_table_price position-relative" style="margin-top: 15px;">
                                    <span class="currency text-white"><i class="icon-rupee"></i>
                                        <span style="left: calc(50% - 20px); -webkit-text-decoration-line: line-through; text-decoration-line: line-through;" class="period crossed-line old-price text-white">
                                            <i class="icon-rupee"></i> RS. 8,650
                                        </span>
                                        <br />
                                        <span style="left: calc(50% - 20px); font-size: 24px;" class="period crossed-line old-price text-white">
                                            <i class="icon-rupee"></i> RS. 5,560
                                        </span>
                                        <br />
                                        <span>Per person</span>
                                    </span>
                                </div>

                                <div class="line p-t-16" style="text-align:center;">
                                    <a class="md-btn btn-book-room md-btn-large" href="javascript:;" style="padding: 2px 30px;">Inquire Now</a>
                                    <a class="md-btn md-btn-default btn-book-room md-btn-large md-btn-wave-light" href="javascript:;" style="padding: 2px 30px;">Book Room</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-12">
                <div>
                    <div class="large-padding p-t-10">
                        <h3 class="heading_a uk-margin-bottom">Description</h3>
                        <p class="line-height-2 text-justify">
                            Illo voluptatum et blanditiis quasi ea provident id saepe amet aut et iste commodi accusantium in repellat aut laudantium ex est ullam et quaerat eos omnis accusantium ad omnis quia est sint ullam molestiae eum  est nobis voluptas recusandae fugit quaerat ut neque ipsam ipsa omnis velit magni placeat maxime atque sit dicta illo dolores quos perferendis porro perspiciatis explicabo facilis sequi nihil neque fugit magnam ut magni eum eos voluptates quasi accusantium inventore sed soluta voluptatum alias repellat 
                        </p>
                    </div>
                </div>
            </div>

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin="">
                <div class="uk-width-large-1-4">
                    <h3 class="heading_a uk-margin-bottom">Inclusion & Exclusion</h3>
                    <div>
                        <h4 class="heading_a uk-margin-bottom">Inclusion</h4>
                        <ul class="md-list md-list-addon gmap_list" id="map_users_list">
                            <li data-gmap-lat="37.406267"  data-gmap-lon="-122.06742" data-gmap-user="Weldon Krajcik Sr." data-gmap-user-company="Treutel Group">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Speed Boat Ride </span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.379267"  data-gmap-lon="-122.02148" data-gmap-user="Liam O'Connell" data-gmap-user-company="Smith, Torphy and D'Amore">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Life jackets</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.410267"  data-gmap-lon="-122.11048" data-gmap-user="Arne Jacobs" data-gmap-user-company="Padberg-Cruickshank">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Equipment Rental</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.397267"  data-gmap-lon="-122.084417" data-gmap-user="Ashlee Vandervort" data-gmap-user-company="Rippin-Glover">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Certified instructors</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.372267"  data-gmap-lon="-122.090417" data-gmap-user="Dolly Kshlerin" data-gmap-user-company="Gerhold Inc">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Photos/Videos</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="uk-width-large-1-4">
                    <h3 class="heading_a uk-margin-bottom">&nbsp;</h3>
                    <div>
                        <h4 class="heading_a uk-margin-bottom">Exclusion</h4>
                        <ul class="md-list md-list-addon gmap_list" id="map_users_list">
                            <li data-gmap-lat="37.406267"  data-gmap-lon="-122.06742" data-gmap-user="Weldon Krajcik Sr." data-gmap-user-company="Treutel Group">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Food & Beverages </span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.379267"  data-gmap-lon="-122.02148" data-gmap-user="Liam O'Connell" data-gmap-user-company="Smith, Torphy and D'Amore">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Pick up/drop</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin="">
                <div class="uk-width-large-1-2">
                    <!--<h3 class="heading_a uk-margin-bottom">Schedule</h3>-->
                    <div class="md-card">
                        <div id="clndr_events" class="clndr-wrapper">
                            <div class="md-card-toolbar">
                                <div class="md-card-toolbar-actions">
                                    <i class="md-icon clndr_add_event material-icons" data-uk-modal="{target:'#modal_clndr_new_event'}">&#xE145;</i>
                                </div>
                                <h3 class="md-card-toolbar-heading-text">
                                    Schedule
                                </h3>
                            </div>
                            <div class="clndr_days p-20 text-center">
                                <div id="kUI_calendar"></div>
                            </div>
                        </div>
                        <div class="uk-modal" id="modal_clndr_new_event">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">New Event</h3>
                                </div>
                                <div class="uk-margin-bottom">
                                    <label for="clndr_event_title_control">Event Title</label>
                                    <input type="text" class="md-input" id="clndr_event_title_control" />
                                </div>
                                <div class="uk-margin-medium-bottom">
                                    <label for="clndr_event_link_control">Event Link</label>
                                    <input type="text" class="md-input" id="clndr_event_link_control" />
                                </div>
                                <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-large-bottom" data-uk-grid-margin>
                                    <div>
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                            <label for="clndr_event_date_control">Event Date</label>
                                            <input class="md-input" type="text" id="clndr_event_date_control" data-uk-datepicker="{format:'YYYY-MM-DD', minDate: '2018-01-16' }">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                            <label for="clndr_event_start_control">Event Start</label>
                                            <input class="md-input" type="text" id="clndr_event_start_control" data-uk-timepicker>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                            <label for="clndr_event_end_control">Event End</label>
                                            <input class="md-input" type="text" id="clndr_event_end_control" data-uk-timepicker>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-modal-footer uk-text-right">
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="button" class="md-btn md-btn-flat md-btn-flat-primary" id="clndr_new_event_submit">Add Event</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="uk-width-large-1-2">
                    <h3 class="heading_a uk-margin-bottom">&nbsp;</h3>
                    <div>
                        <ul class="md-list md-list-addon gmap_list" id="map_users_list">
                            <li data-gmap-lat="37.406267"  data-gmap-lon="-122.06742" data-gmap-user="Weldon Krajcik Sr." data-gmap-user-company="Treutel Group">
                                <div class="md-list-content">
                                    <span class="md-list-heading">morning 10am to12noon</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.379267"  data-gmap-lon="-122.02148" data-gmap-user="Liam O'Connell" data-gmap-user-company="Smith, Torphy and D'Amore">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Noon 2pm to 4pm</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.410267"  data-gmap-lon="-122.11048" data-gmap-user="Arne Jacobs" data-gmap-user-company="Padberg-Cruickshank">
                                <div class="md-list-content">
                                    <span class="md-list-heading">Evening 6pm to 8pm</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="uk-width-medium-1-12">
                <div>
                    <div class="large-padding p-t-10">
                        <h3 class="heading_a uk-margin-bottom">Booking Policy</h3>
                        <p class="line-height-2 text-justify">
                            Illo voluptatum et blanditiis quasi ea provident id saepe amet aut et iste commodi accusantium in repellat aut laudantium ex est ullam et quaerat eos omnis accusantium ad omnis quia est sint ullam molestiae eum  est nobis voluptas recusandae fugit quaerat ut neque ipsam ipsa omnis velit magni placeat maxime atque sit dicta illo dolores quos perferendis porro perspiciatis explicabo facilis sequi nihil neque fugit magnam ut magni eum eos voluptates quasi accusantium inventore sed soluta voluptatum alias repellat 
                        </p>
                    </div>
                </div>
            </div>

            <div class="uk-width-medium-1-12">
                <div>
                    <div class="large-padding p-t-10">
                        <h3 class="heading_a uk-margin-bottom">Cancellation Policy</h3>
                        <p class="line-height-2 text-justify">
                            Illo voluptatum et blanditiis quasi ea provident id saepe amet aut et iste commodi accusantium in repellat aut laudantium ex est ullam et quaerat eos omnis accusantium ad omnis quia est sint ullam molestiae eum  est nobis voluptas recusandae fugit quaerat ut neque ipsam ipsa omnis velit magni placeat maxime atque sit dicta illo dolores quos perferendis porro perspiciatis explicabo facilis sequi nihil neque fugit magnam ut magni eum eos voluptates quasi accusantium inventore sed soluta voluptatum alias repellat 
                        </p>
                    </div>
                </div>
            </div>

            <div class="uk-width-medium-1-12">
                <div>
                    <div class="large-padding p-t-10">
                        <h3 class="heading_a uk-margin-bottom">Term & Condition</h3>
                        <p class="line-height-2 text-justify">
                            Illo voluptatum et blanditiis quasi ea provident id saepe amet aut et iste commodi accusantium in repellat aut laudantium ex est ullam et quaerat eos omnis accusantium ad omnis quia est sint ullam molestiae eum  est nobis voluptas recusandae fugit quaerat ut neque ipsam ipsa omnis velit magni placeat maxime atque sit dicta illo dolores quos perferendis porro perspiciatis explicabo facilis sequi nihil neque fugit magnam ut magni eum eos voluptates quasi accusantium inventore sed soluta voluptatum alias repellat 
                        </p>
                    </div>
                </div>
            </div>

            <div class="uk-grid uk-margin-medium-top">
                <div class="uk-width-1-1">
                    <a href="{{ route('activities.type') }}" class="md-btn md-btn-danger">Inquire Now</a>
                    <a href="{{ route('activities.type') }}" class="md-btn md-btn-primary">Book Now</a>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
@endsection

@section('javascript')
<!-- kendo UI -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/kendoui_custom.min.js"></script>
<!--  kendoui functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/kendoui.min.js"></script>
@endsection
