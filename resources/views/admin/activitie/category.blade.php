@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('activities.type.add') }}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dataTable" id="activity_type_tbl">
                <thead>
                    <tr>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 20%;">Type</th>
                        <th style="width: 40%;" class="hidden-phone">Description</th>
                        <th style="width: 10%;" class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <!-- <tfoot>
                    <tr>
                        <td><div class="md-input-wrapper"><input placeholder="ID" colPos="0" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td><div class="md-input-wrapper"><input placeholder="Name" colPos="1" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td><div class="md-input-wrapper"><input placeholder="Description" colPos="2" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                        <td>
                            <select class="md-input uk-form-width-small" data-placeholder="Choose a Status" tabindex="1" colPos="3">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="0">InActive</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                </tfoot> -->
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE widget-->
</div>
<div id="changestatus" class="uk-modal" >
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Status</h3>
        </div>
        <div class="">
            <div class="control-group">
                <input type="hidden" value="" id="change-status-row-id" />
                <label class="control-label">Status</label>
                <div class="controls">
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="1" id="status_dialog_active" data-md-icheck />
                        <label for="status_dialog_active" class="inline-label">Active</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="0" id="status_dialog_inactive" data-md-icheck />
                        <label for="status_dialog_inactive" class="inline-label">InActive</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close" id="status-dlg-btn">Submit</button> 
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function () {

        var actitytypeTable = $('#activity_type_tbl').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
            searching: true,
            order: [[ 0, "desc" ]],
//            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
//            "pagingType": "full_numbers", //full,full_numbers
            "ajax": {
                "url": "{{ route('activities.type_data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof actitytypeTable != 'undefined' && actitytypeTable.hasOwnProperty('settings')) {
                        actitytypeTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "typeId", "name": "typeId"},
                {"data": "name", "name": "name"},
                {"data": "description", "name": "description"},
                {"data": "status", "name": "status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.typeId + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a>';
                        } else {
                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.typeId + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-danger">InActive</span></a>';
                        }
                    },
                },
                {"data": "action", 'sortable': false, "render": function (data, type, row) {
                        var edit_url = "{{ route('activities.type.edit', ':id') }}";
                        var dele_url = "{{ route('activities.type.delete', ':id') }}";
                        edit_url = edit_url.replace(':id', row.typeId);
                        dele_url = dele_url.replace(':id', row.typeId);
                        return '<a href="' + edit_url + '" title="Edit Activity type" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + dele_url + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete Activity type" class=""><i class="md-icon material-icons">delete</i></a>';
                    },
                },
            ]
        });


        $(actitytypeTable.table().container()).on('keyup', 'tfoot input', function () {
            actitytypeTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(actitytypeTable.table().container()).on('change', 'tfoot select', function () {
            actitytypeTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });


        $(document.body).on('click', '.change-status-btn', function () {
            var $obj = $(this);
            var row_id = $obj.attr('row-id');
            var status = $obj.attr('data-id');
            $('input#change-status-row-id').val(row_id);
            if (status == "1") {
                $('#status_dialog_inactive').removeAttr('checked');
                $('#status_dialog_active').iCheck('check');
            } else {
                $('#status_dialog_active').removeAttr('checked');
                $('#status_dialog_inactive').iCheck('check');
            }

        });

        $(document.body).on('click', '#status-dlg-btn', function () {
            var $obj = $(this);
            var row_id = $('#change-status-row-id').val();
            var status = $('input.user-status-change:checked').val();
            $.ajax({
                type: 'get',
                url: '{{ route("change_status")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table": "master_activity_type",
                    "id": row_id,
                    "column_name": "typeId",
                    "status": status
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        actitytypeTable.draw();
                    }
                    showNotify(data.msg_status, data.message);
                }
            });
        });


    });
</script>
@endsection
