@extends('layouts.theme')
@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection
@section('content')
<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('activities.index') }}">Activities</i></a></li>
            <li><span>Add / Edit Activities</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner" style="padding-top:0;">
            <div>
                <div class="p-t-30">
                    <form action="{{ route('activities.index') }}" class="" method="get">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Title <span class="required-lbl">*</span></label>
                                            <input type="text" class="md-input" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select id="select_demo_3" class="md-input">
                                                <option value="" selected>Choose Type</option>
                                                <option value="1">Sightseeing</option>
                                                <option value="2">Ground Transport</option>
                                                <option value="3">Guide</option>
                                                <option value="4">Show</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
<!--                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Price</label>
                                            <input type="text" class="md-input" />
                                        </div>-->
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Require Age</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_3" class="md-input">
                                                <option value="" selected>Choose Star</option>
                                                <option value="">--Please choose--</option>
                                                <option value="1">* (1)</option>
                                                <option value="2">** (2)</option>
                                                <option value="3">*** (3)</option>
                                                <option value="4">**** (4)</option>
                                                <option value="5">***** (5)</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Google map link</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label for="Group / FIT(Customized)" class="uk-form-label">Inventory Type</label>
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="settings_cache_type" id="settings_cache_file" data-md-icheck checked />
                                                    <label for="settings_cache_file" class="inline-label">Day Wise</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="settings_cache_type" id="settings_cache_mysql" data-md-icheck />
                                                    <label for="settings_cache_mysql" class="inline-label">Bulk</label>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Select Date</label>
                                            <input type="text" class="md-input" id="uk_dp_1" data-uk-datepicker="{format:'DD/MM/YYYY'}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Quantity</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                        <div class="uk-width-medium-1-4 p-l-50">
                                            <label>To Date</label>
                                            <input type="text" class="md-input" id="uk_dp_start" autocomplete="off" />
                                        </div>
                                        <div class="uk-width-medium-1-4 p-l-50">
                                            <label>From Date</label>
                                            <input type="text" class="md-input" id="uk_dp_end" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Duration</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Description</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_3" class="md-input" name="country_id" id="country_id">
                                                <option value="" selected>Choose a Country</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select id="select_demo_3" class="md-input" name="state_id" id="state_id">
                                                <option value="" selected>Choose a State</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_3" class="md-input" name="city_id" id="city_id">
                                                <option value="" selected>Choose a City</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Pincode</label>
                                            <input type="text" class="md-input" name="pin_code" value="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Keywords / Tags for search</label>
                                            <input type="text" class="md-input" />
                                        </div>
<!--                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Price</label>
                                            <input type="text" class="md-input" />
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
<!--                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Sell Price</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Duration</label>
                                            <input type="text" class="md-input" />
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-4 p-r-50">
                                            <label>Open Time</label>
                                            <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                        </div>
                                        <div class="uk-width-medium-1-4 p-l-50">
                                            <label>Close Time</label>
                                            <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label for="meal_includes" class="uk-form-label">Pickup drop Included</label>
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="checkbox" name="settings_cache_type" id="meal_include" data-md-icheck />
                                                    <label for="meal_include" class="inline-label">Yes</label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Tags</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <table class="span12 table" style="width:100%;">
                                                <tr>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="sunday_1" id="sunday_1" data-md-icheck />
                                                                <label for="sunday_1" class="inline-label">Sunday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="monday_1" id="monday_1" data-md-icheck />
                                                                <label for="monday_1" class="inline-label">Monday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="tuesday_1" id="tuesday_1" data-md-icheck />
                                                                <label for="tuesday_1" class="inline-label">Tuesday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="wednesdy_1" id="wednesdy_1" data-md-icheck />
                                                                <label for="wednesdy_1" class="inline-label">Wednesday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="thursday_1" id="thursday_1" data-md-icheck />
                                                                <label for="thursday_1" class="inline-label">Thursday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="friday_1" id="friday_1" data-md-icheck />
                                                                <label for="friday_1" class="inline-label">Friday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="saturday_1" id="saturday_1" data-md-icheck />
                                                                <label for="saturday_1" class="inline-label">Saturday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Transport Details</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Things to Carry</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Advisory/precausion</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Cancellation Policy</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Refund Policy</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Conformation Policy</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-2-6">
                                            <h3 class="heading_a uk-margin-small-bottom">Main Image</h3>
                                            <input type="file" id="input-file-a" class="dropify" />
                                        </div>
                                        <div class="uk-width-medium-2-3">
                                            <h3 class="heading_a uk-margin-small-bottom">Multiple Image for banner</h3>
                                            <div id="file_upload-drop" class="uk-file-upload">
                                                <p class="uk-text">Drop file to upload</p>
                                                <p class="uk-text-muted uk-text-small uk-margin-small-bottom">or</p>
                                                <a class="uk-form-file md-btn">choose file<input id="file_upload-select" type="file"></a>
                                            </div>
                                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                                <div class="uk-progress-bar" style="width:0">0%</div>
                                            </div>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-6">
                                            <input type="radio" name="status" value="1" id="status_1" data-md-icheck />
                                            <label for="status_1" class="inline-label">Active</label>
                                        </div>
                                        <div class="uk-width-medium-1-6">
                                            <input type="radio" name="status" value="0" id="status_0" data-md-icheck />
                                            <label for="status_0" class="inline-label">In Active</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="form_hr">
                        <div class="uk-grid uk-margin-medium-top uk-text-right">
                            <div class="uk-width-1-1">
                                <a href="{{ route('activities.index') }}" class="md-btn md-btn-danger">Cancel</a>
                                <button type="submit" class="md-btn md-btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>

@endsection


@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_advanced.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
@endsection
