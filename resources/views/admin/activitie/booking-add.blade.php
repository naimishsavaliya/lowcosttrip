@extends('layouts.theme')
@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection
@section('content')
<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('activities.mange.booking.index') }}">Activity Booking</i></a></li>
            <li><span>Add / Edit Booking</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner" style="padding-top:0;">
            <div>
                <div class="p-t-30">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-6-10">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <select class="md-input">
                                            <option>Choose Activity</option>
                                            <option>Activity 1</option>
                                            <option>Activity 2</option>
                                            <option>Activity 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label>Lead ID</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-1-2">
                                        <label>Email</label>
                                        <input type="text" class="md-input"  />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-10">
                                        <label>Title</label>
                                        <select id="product_search_status" class="md-input">
                                            <option value=""></option>
                                            <option value="1">Ms.</option>
                                            <option value="2">Mrs.</option>
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-4-10">
                                        <label>First Name</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-4-10">
                                        <label>Last Name</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-10">
                                        <label>Time Slot</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-4-10">
                                        <label>Adult</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-4-10">
                                        <label>Child</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-5-10">
                                        <label>Phone No</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                    <div class="uk-width-medium-5-10">
                                        <label>Add Discount Code</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                </div>
                            </div>
                            <hr class="form_hr">
                            <div class="uk-grid uk-margin-medium-top uk-text-right">
                                <div class="uk-width-1-1">
                                    <a href="{{ route('activities.mange.booking.index') }}" class="md-btn md-btn-danger">Cancel</a>
                                    <a href="{{ route('activities.mange.booking.index') }}" class="md-btn md-btn-primary">Save</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

@endsection

