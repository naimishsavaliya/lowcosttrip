@extends('layouts.theme')
@section('content')

<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('activities.inventory') }}">Activities Inventory</i></a></li>
            <li><span>Add / Edit Activities Inventory</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ route('destination.save') }}" name="destination_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($destinations->desId) ? $destinations->desId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3 p-r-50">
                                        <select class="md-input" name="activitie_id" id="country_id">
                                            <option value="" selected>Choose Activitie</option>
                                            <option value="1">Activities 1</option>
                                            <option value="2">Activities 2</option>
                                            <option value="3">Activities 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-4 uk-width-1-1 p-r-50">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_start">Start Date</label>
                                        <input class="md-input" type="text" id="uk_dp_start" name="start_date" autocomplete="off" value="">
                                    </div>
                                </div>
                                <div class="uk-width-large-1-4 uk-width-medium-1-1">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_end">End Date</label>
                                        <input class="md-input" type="text" id="uk_dp_end" name="end_date" autocomplete="off" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Sell Price</label>
                                        <input type="text" class="md-input" name="name" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck checked="" />
                                                <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck />
                                                <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('activities.inventory') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                </div>
                        </div>-->

        </div>
    </div>

</div>

@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_advanced.min.js"></script>

@endsection
