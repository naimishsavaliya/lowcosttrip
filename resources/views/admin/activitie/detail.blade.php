@extends('layouts.theme')


@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/fullcalendar/dist/fullcalendar.min.css">
<style>
    .fc-toolbar h2{line-height: 32px;}
</style>
@endsection

@section('content')

<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('activities.index') }}">Activity Name</a></li>
            <li><span>Details</span></li>
        </ul>
    </div>
    <style>
        .uk-sticky-placeholder .uk-tab{background: #1976D2;}
        .user_heading{ padding: 24px 24px 0px 24px;}
        .select2{ width: 100% !important;}
        .uk-tab > li.uk-active > a{color: #FFF;}
        .uk-tab > li.uk-active > a{border-bottom-color: #00071f;}
        .uk-dropdown{width: 200px !important;}
        .select2-container--open{z-index: 9999;}
        .uk-dropdown-shown{min-width: 260px !important;}
    </style>
    <div id="page_content">
        <div id="page_content_inner" class="p-0">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-1-1">
                    <div class="">
                        <div class="user_heading">
                            <div class="user_heading_menu hidden-print">
                                <div class="uk-display-inline-block" data-uk-dropdown="{pos:'left-top'}">
                                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="javascript:;" data-uk-modal="{target:'#booking_form_model'}"><i class="material-icons ">add</i> Booking</a></li>
                                            <li><a href="javascript:;" data-uk-modal="{target:'#discount_form_model'}"><i class="material-icons ">add</i> Discount</a></li>
                                            <!--<li><a href="javascript:;" data-uk-modal="{target:'#payment_form_model'}"><i class="material-icons ">add</i> Payment</a></li>-->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="user_heading_content" style="display:inline-flex;">
                                <h2 class="heading_b uk-margin-bottom">
                                    <span class="uk-text-truncate">Activity Name</span>
                                    <!--<span class="sub-heading">By Supplier Name (Group Tour)-->
                                    <!--</span>-->
                                </h2>
                            </div>

                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">Activity</a></li>
                                <li class="bookinglist"><a href="#" >Booking List</a></li>
                                <li class="bookingcalander"><a href="#" >Booking Calender</a></li>
                                <li class="discount"><a href="#" >Discount</a></li>
                                <li class="rates"><a href="#" >Rates</a></li>
                            </ul>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <form class="" method="get">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Title <span class="required-lbl">*</span></label>
                                            <input type="text" class="md-input" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select id="select_demo_3" class="md-input">
                                                <option value="" selected>Choose Type</option>
                                                <option value="1">Sightseeing</option>
                                                <option value="2">Ground Transport</option>
                                                <option value="3">Guide</option>
                                                <option value="4">Show</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
<!--                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Price</label>
                                            <input type="text" class="md-input" />
                                        </div>-->
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Require Age</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_3" class="md-input">
                                                <option value="" selected>Choose Star</option>
                                                <option value="">--Please choose--</option>
                                                <option value="1">* (1)</option>
                                                <option value="2">** (2)</option>
                                                <option value="3">*** (3)</option>
                                                <option value="4">**** (4)</option>
                                                <option value="5">***** (5)</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Google map link</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label for="Group / FIT(Customized)" class="uk-form-label">Inventory Type</label>
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="settings_cache_type" id="settings_cache_file" data-md-icheck checked />
                                                    <label for="settings_cache_file" class="inline-label">Day Wise</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="settings_cache_type" id="settings_cache_mysql" data-md-icheck />
                                                    <label for="settings_cache_mysql" class="inline-label">Bulk</label>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Select Date</label>
                                            <input type="text" class="md-input" id="uk_dp_1" data-uk-datepicker="{format:'DD/MM/YYYY'}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Quantity</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                        <div class="uk-width-medium-1-4 p-l-50">
                                            <label>To Date</label>
                                            <input type="text" class="md-input" id="uk_dp_start" autocomplete="off" />
                                        </div>
                                        <div class="uk-width-medium-1-4 p-l-50">
                                            <label>From Date</label>
                                            <input type="text" class="md-input" id="uk_dp_end" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Duration</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Description</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_3" class="md-input" name="country_id" id="country_id">
                                                <option value="" selected>Choose a Country</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select id="select_demo_3" class="md-input" name="state_id" id="state_id">
                                                <option value="" selected>Choose a State</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_3" class="md-input" name="city_id" id="city_id">
                                                <option value="" selected>Choose a City</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Pincode</label>
                                            <input type="text" class="md-input" name="pin_code" value="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Keywords / Tags for search</label>
                                            <input type="text" class="md-input" />
                                        </div>
<!--                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Price</label>
                                            <input type="text" class="md-input" />
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
<!--                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Sell Price</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Duration</label>
                                            <input type="text" class="md-input" />
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-4 p-r-50">
                                            <label>Open Time</label>
                                            <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                        </div>
                                        <div class="uk-width-medium-1-4 p-l-50">
                                            <label>Close Time</label>
                                            <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label for="meal_includes" class="uk-form-label">Pickup drop Included</label>
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="checkbox" name="settings_cache_type" id="meal_include" data-md-icheck />
                                                    <label for="meal_include" class="inline-label">Yes</label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Tags</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <table class="span12 table" style="width:100%;">
                                                <tr>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="sunday_1" id="sunday_1" data-md-icheck />
                                                                <label for="sunday_1" class="inline-label">Sunday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="monday_1" id="monday_1" data-md-icheck />
                                                                <label for="monday_1" class="inline-label">Monday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="tuesday_1" id="tuesday_1" data-md-icheck />
                                                                <label for="tuesday_1" class="inline-label">Tuesday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="wednesdy_1" id="wednesdy_1" data-md-icheck />
                                                                <label for="wednesdy_1" class="inline-label">Wednesday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="thursday_1" id="thursday_1" data-md-icheck />
                                                                <label for="thursday_1" class="inline-label">Thursday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="friday_1" id="friday_1" data-md-icheck />
                                                                <label for="friday_1" class="inline-label">Friday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="width:14.28%;">
                                                        <div>
                                                            <span class="icheck-inline">
                                                                <input type="checkbox" name="saturday_1" id="saturday_1" data-md-icheck />
                                                                <label for="saturday_1" class="inline-label">Saturday</label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Open Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Close Time</label>
                                                                <input type="text" class="md-input" id="uk_tp_1" data-uk-timepicker autocomplete="off" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Transport Details</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Things to Carry</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Advisory/precausion</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Cancellation Policy</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Refund Policy</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Conformation Policy</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-2-6">
                                            <h3 class="heading_a uk-margin-small-bottom">Main Image</h3>
                                            <input type="file" id="input-file-a" class="dropify" />
                                        </div>
                                        <div class="uk-width-medium-2-3">
                                            <h3 class="heading_a uk-margin-small-bottom">Multiple Image for banner</h3>
                                            <div id="file_upload-drop" class="uk-file-upload">
                                                <p class="uk-text">Drop file to upload</p>
                                                <p class="uk-text-muted uk-text-small uk-margin-small-bottom">or</p>
                                                <a class="uk-form-file md-btn">choose file<input id="file_upload-select" type="file"></a>
                                            </div>
                                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                                <div class="uk-progress-bar" style="width:0">0%</div>
                                            </div>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-6">
                                            <input type="radio" name="status" value="1" id="status_1" data-md-icheck />
                                            <label for="status_1" class="inline-label">Active</label>
                                        </div>
                                        <div class="uk-width-medium-1-6">
                                            <input type="radio" name="status" value="0" id="status_0" data-md-icheck />
                                            <label for="status_0" class="inline-label">In Active</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="form_hr">
                        <div class="uk-grid uk-margin-medium-top uk-text-right">
                            <div class="uk-width-1-1">
                                <a href="javascript:;" class="md-btn md-btn-danger">Cancel</a>
                                <button type="submit" class="md-btn md-btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                                </li>
                                <li>
                                    <table class="u dt_defaultk>
                                        <thead>
                                            <tr>
                                                <th>Lead ID</th>
                                                <th>Customer</th>
                                                <th>Lead Title</th>
                                                <th>Booking Date</th>
                                                <th>Activity Date</th>
                                                <th>Activity type</th>
                                                <th>No of Booking</th>
                                                <th>Adult/Child/Infant</th>
                                                <th>Supplier</th>
                                                <th>Agent</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Lead Id" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Customer" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Lead Title" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Booking Date" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Activity Date" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Activity type" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="No of Booking Activity" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Adult/Child/Infant" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Supplier" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Agent" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>LCT<?php echo rand(1111, 9999); ?></td>
                                                    <td>Customer Name</td>
                                                    <td>Kerala Tour</td>
                                                    <td>Mar 5, 2018</td>
                                                    <td>Nov 10, 2018</td>
                                                    <td>Water</td>
                                                    <td>1</td>
                                                    <td><?php echo rand(2, 3); ?>/<?php echo rand(0, 2); ?>/<?php echo rand(0, 1); ?></td>
                                                    <td>Supplier 1</td>
                                                    <td>Agent 1</td>
                                                    <td>
                                                        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                            <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                                            <div class="uk-dropdown">
                                                                <ul class="uk-nav uk-nav-dropdown">
                                                                    <li><a href="javascript:;"> Cancel Booking</a></li>
                                                                    <li><a href="javascript:;"> Print Voucher</a></li>
                                                                    <li><a href="javascript:;"> Email Voucher</a></li>
                                                                    <li><a href="javascript:;"> Edit Booking</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <!--View booking calender-->
                                    <div class="p-t-30">
                                        <div id="calendar_selectable"></div>
                                    </div>
                                </li>
                                <li>
                                    <!--discount tab-->
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Discount Code</th>
                                                <th>Activity Type</th>
                                                <!--<th>Activity Option</th>-->
                                                <th>Price</th>
                                                <th>Discount %</th>
                                                <th>Discoun Amount</th>
                                                <th>Date Period</th>
                                                <th>Travel Agent</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="ID" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Discount Code" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Activity Type" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
<!--                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Activity Option" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>-->
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Price" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Discount %" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Discounted Price" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Date Period" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Travel Agent" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td><?php echo $i; ?></td>
                                                    <td>DIS<?php echo rand(111, 999); ?></td>
                                                    <td>Water</td>
                                                    <!--<td>Pick up Included</td>-->
                                                    <td>1500</td>
                                                    <td>10</td>
                                                    <td>1350</td>
                                                    <td>01 Jan 19 To 05 Feb 19</td>
                                                    <td>Travel Agent</td>
                                                    <td>
                                                        <a href="javascript:;" data-uk-modal="{target:'#discount_form_model'}" title="Edit Discount" class=""><i class="md-icon material-icons">&#xE254;</i></a> 
                                                        <a href="javascript:;" onclick="return confirm('Are you sure to delete?');" title="Delete Discount" class=""><i class="md-icon material-icons">delete</i></a> 
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <!--rates tab-->
                                    <form class="uk-form-stacked" id="wizard_advanced_form">
                                        <div data-dynamic-fields="rates_field_wizard" class="uk-grid" data-uk-grid-margin></div>
                                    </form>
                                </li>


                            </ul>
                        </div>
                    </div>
                </div>

            </div>



        </div>

    </div>
</div>

<!--add Booking model-->
    <div class="uk-modal uk-modal-card-fullscreen" id="booking_form_model">
        <div class="uk-modal-dialog uk-modal-dialog-blank">
            <div class="md-card uk-height-viewport">
                <div class="md-card-toolbar">
                    <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                    <h3 class="md-card-toolbar-heading-text">
                        Add Booking
                    </h3>
                </div>
                <div class="md-card-content">
                    <div class="p-t-30">
                        <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-6-10">
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label>Lead ID</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label>Email</label>
                                <input type="text" class="md-input"  />
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-2-10">
                                <label>Title</label>
                                <select id="product_search_status" class="md-input">
                                    <option value=""></option>
                                    <option value="1">Ms.</option>
                                    <option value="2">Mrs.</option>
                                </select>
                            </div>
                            <div class="uk-width-medium-4-10">
                                <label>First Name</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-4-10">
                                <label>Last Name</label>
                                <input type="text" class="md-input" />
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-2-10">
                                <label>Time Slot</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-4-10">
                                <label>Adult</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-4-10">
                                <label>Child</label>
                                <input type="text" class="md-input" />
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-5-10">
                                <label>Phone No</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-5-10">
                                <label>Add Discount Code</label>
                                <input type="text" class="md-input" />
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <a href="javascript:;" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Submit</a>
                    </div>
                </div>
                <div class="uk-width-medium-4-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Sailing & Snorkeling
<!--                                <div class="pricing_table_plan fac-star-wrapper display-inline-block p-l-10">
                                    <i class="material-icons icon-star">&#xE838;</i>
                                    <i class="material-icons icon-star">&#xE838;</i>
                                    <i class="material-icons icon-star">&#xE838;</i>
                                    <i class="material-icons icon-star">&#xE838;</i>
                                    <i class="material-icons icon-star">&#xE838;</i>
                                </div>-->
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <!--<b class="uk-text-small uk-display-block">Areal</b>-->
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-12">
                                    <ul class="md-list hotel-detail-list ">
                                        <li class="uk-width-medium-1-2 uk-float-left">
                                            <div class="md-list-content">
                                                <span class="md-list-heading uk-text-medium">Min Age: </span>
                                                <span class="uk-text-small uk-text-muted uk-display-block" style="padding:2px;">12</span>
                                            </div>
                                        </li>
                                        <li class="uk-width-medium-1-2 uk-float-left">
                                            <div class="md-list-content">
                                                <span class="md-list-heading uk-text-medium">Duration</span>
                                                <span class="uk-text-small uk-text-muted uk-display-block" style="padding:2px;">1 Day</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-medium-1-1">
                                    <ul class="md-list hotel-detail-list ">
                                        <li>
                                            <div class="md-list-content">
                                                <span class="md-list-heading uk-text-medium">Pickup Drop Included</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-medium-1-1">
                                    <ul class="md-list hotel-detail-list  ">
                                        <li class="uk-width-medium-7-10 uk-float-left">
                                            <span class="uk-text-small uk-display-block">Price (1 room x 5 Nights)</span>
                                        </li>
                                        <li class="uk-width-medium-3-10 text-right uk-float-left">
                                            <span class="md-list-heading uk-text-large"><i class="icon-rupee"></i> 2000</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-medium-1-12">
                                    <ul class="md-list hotel-detail-list  ">
                                        <li class="uk-width-medium-7-10 uk-float-left">
                                            <span class="uk-text-small uk-display-block">VAT / GST</span>
                                        </li>
                                        <li class="uk-width-medium-3-10 text-right uk-float-left">
                                            <span class="md-list-heading uk-text-large"><i class="icon-rupee"></i> 2000</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-medium-1-12">
                                    <hr class="uk-article-divider">
                                    <ul class="md-list hotel-detail-list ">
                                        <li class="uk-width-medium-7-10 uk-float-left">
                                            <div class="md-list-content">
                                                <span class="md-list-heading uk-text-medium">Total</span>
                                            </div>
                                        </li>
                                        <li class="uk-width-medium-3-10 text-right uk-float-left">
                                            <span class="md-list-heading uk-text-large uk-text-success"><i class="icon-rupee"></i> 4000</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2 text-center">
                            <a href="javascript:;" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" >Make Payment</a>
                        </div>
                        <div class="uk-width-medium-1-2 text-center">
                            <a href="javascript:;" class="md-btn md-btn-default md-btn-wave-light waves-effect waves-button waves-light">Payment Offline</a>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--Discount model content-->
<div class="uk-modal uk-modal-card-fullscreen" id="discount_form_model">
    <div class="uk-modal-dialog uk-modal-dialog-blank">
        <div class="md-card uk-height-viewport">
            <div class="md-card-toolbar">
                <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                <h3 class="md-card-toolbar-heading-text">
                    Add /Edit Discount
                </h3>
            </div>
            <div class="md-card-content">
                <div class="p-t-30">
                    <form class="uk-form-stacked" id="wizard_advanced_form">
                        <div data-dynamic-fields="discount_field_wizard" class="uk-grid" data-uk-grid-margin>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script id="discount_field_wizard" type="text/x-handlebars-template">
    <div class="uk-width-medium-1-1 parsley-row form_section">
    <div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-medium-1-1">
    <div class="uk-form-row">


    <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-4">

    <select id="select_demo_3" class="md-input">
    <option value="" selected>Room Type</option>
    <option value="1">All Rooms</option>
    <option value="1">Deluxe</option>
    <option value="2">Semi Deluxe</option>
    <option value="3">Garden</option>
    </select>
    </div>
    <div class="uk-width-medium-1-5">
    <div class="uk-input-group">
    <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
    <label for="uk_dp_start">Start Date</label>
    <input class="md-input" type="text" id="uk_dp_start">
    </div>
    </div>
    <div class="uk-width-medium-1-5">
    <div class="uk-input-group">
    <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
    <label for="uk_dp_end">End Date</label>
    <input class="md-input" type="text" id="uk_dp_end">
    </div>
    </div>
    <div class="uk-width-medium-2-10">
    <div class="uk-input-group">
    <div class="md-input-wrapper"><label>Discount Percentage %</label><input type="text" class="md-input"><span class="md-input-bar "></span></div>

    </div>
    </div>
    <div class="uk-width-medium-1-10">
    <div class="uk-input-group">
    <a href="#" class="btnSectionClone"><i class="material-icons md-36"></i></a>

    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</script>
<script id="rates_field_wizard" type="text/x-handlebars-template">
    <div class="uk-width-medium-1-1 parsley-row form_section">
    <div class="uk-input-group uk-grid" style="width:100%">
    <div class="uk-width-1-1">
    <div class="">
    <div class="uk-grid" data-uk-grid-margin="">
    <div class="uk-width-medium-1-4 uk-width-1-1 uk-row-first">
    <label for="invoice_dp">Date From</label>
    <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="invoice_dp">Date To</label>
    <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="discount">Customer Discount %</label>
    <input class="md-input" type="text" id="discount" value="">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="discount">Agent Discount %</label>
    <input class="md-input" type="text" id="discount" value="">
    </div>
    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div>
    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div> <br/>


    <div class="uk-width-medium-1-1">
    <table class="uk-table ">
    <tbody>
    <tr>
    <td style="width: 15%;vertical-align:bottom;">
    <div class="md-input-wrapper">
    <span class="icheck-inline">
    <input type="checkbox" name="rates[]" id="sunday_1" data-md-icheck />
    <label for="sunday_1" class="inline-label">Sunday</label>
    </span>
    </div>
    <div class="md-input-wrapper">
    <label> </label>
    <select class="md-input" id="select_demo_5" data-md-selectize data-md-selectize-bottom >
    <option value="1">Adult Rate</option>
    <option value="3">Senior citizen</option>
    <option value="4">Children</option>
    <option value="5">Infant</option>
    <option value="6">Government officials</option>
    <option value="7">Student</option>
    <option value="8">Armed forces</option>
    <option value="9">Disable</option>
    </select>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <span class="icheck-inline">
    <input type="checkbox" name="rates[]" id="monday_1" data-md-icheck />
    <label for="monday_1" class="inline-label">Monday</label>
    </span>
    </div>
    <div class="md-input-wrapper">
    <label>Standard</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <span class="icheck-inline">
    <input type="checkbox" name="rates[]" id="tuesday_1" data-md-icheck />
    <label for="tuesday_1" class="inline-label">Tuesday</label>
    </span>
    </div>
    <div class="md-input-wrapper">
    <label>1px</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <span class="icheck-inline">
    <input type="checkbox" name="rates[]" id="wednesday_1" data-md-icheck />
    <label for="wednesday_1" class="inline-label">Wednesday</label>
    </span>
    </div>
    <div class="md-input-wrapper">
    <label>2px</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <span class="icheck-inline">
    <input type="checkbox" name="rates[]" id="thursday_1" data-md-icheck />
    <label for="thursday_1" class="inline-label">Thursday</label>
    </span>
    </div>
    <div class="md-input-wrapper">
    <label>3px</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <span class="icheck-inline">
    <input type="checkbox" name="rates[]" id="friday_1" data-md-icheck />
    <label for="friday_1" class="inline-label">Friday</label>
    </span>
    </div>
    <div class="md-input-wrapper">
    <label>4px</label>
    <input type="text" class="md-input"><span class="md-input-bar"></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <span class="icheck-inline">
    <input type="checkbox" name="rates[]" id="saturday_1" data-md-icheck />
    <label for="saturday_1" class="inline-label">Saturday</label>
    </span>
    </div>
    <div class="md-input-wrapper">
    <label>5px</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <label>6px</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <label>7px</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <label>7px to 12px</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td style="vertical-align:bottom;">
    <div class="md-input-wrapper">
    <label>12+px</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    </tr>
<tr>
            <td style="width: 15%;vertical-align:bottom;">
                <select class="md-input" id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                    <option value="1">Child Rate</option>
                    <option value="3">Senior citizen</option>
                    <option value="4">Children</option>
                    <option value="5">Infant</option>
                    <option value="6">Government officials</option>
                    <option value="7">Student</option>
                    <option value="8">Armed forces</option>
                    <option value="9">Disable</option>
                </select>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>Standard</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>1px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>2px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>3px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>4px</label>
                    <input type="text" class="md-input"><span class="md-input-bar"></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>5px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>6px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px to 12px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>12+px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
        </tr>
        
        <tr>
            <td style="width: 15%;vertical-align:bottom;">
                <select class="md-input" id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                    <option value="1">Infant Rate</option>
                    <option value="3">Senior citizen</option>
                    <option value="4">Children</option>
                    <option value="5">Infant</option>
                    <option value="6">Government officials</option>
                    <option value="7">Student</option>
                    <option value="8">Armed forces</option>
                    <option value="9">Disable</option>
                </select>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>Standard</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>1px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>2px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>3px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>4px</label>
                    <input type="text" class="md-input"><span class="md-input-bar"></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>5px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>6px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px to 12px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>12+px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
        </tr>
        
        <tr>
            <td style="width: 15%;vertical-align:bottom;">
                <select class="md-input" id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                    <option value="1">Government officials</option>
                    <option value="3">Senior citizen</option>
                    <option value="4">Children</option>
                    <option value="5">Infant</option>
                    <option value="7">Student</option>
                    <option value="8">Armed forces</option>
                    <option value="9">Disable</option>
                </select>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>Standard</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>1px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>2px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>3px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>4px</label>
                    <input type="text" class="md-input"><span class="md-input-bar"></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>5px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>6px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px to 12px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>12+px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
        </tr>
        
        <tr>
            <td style="width: 15%;vertical-align:bottom;">
                <select class="md-input" id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                    <option value="1">Student</option>
                    <option value="3">Senior citizen</option>
                    <option value="4">Children</option>
                    <option value="5">Infant</option>
                    <option value="7">Student</option>
                    <option value="8">Armed forces</option>
                    <option value="9">Disable</option>
                </select>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>Standard</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>1px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>2px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>3px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>4px</label>
                    <input type="text" class="md-input"><span class="md-input-bar"></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>5px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>6px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px to 12px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>12+px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
        </tr>
        
        <tr>
            <td style="width: 15%;vertical-align:bottom;">
                <select class="md-input" id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                    <option value="1">Armed forces</option>
                    <option value="3">Senior citizen</option>
                    <option value="4">Children</option>
                    <option value="5">Infant</option>
                    <option value="7">Student</option>
                    <option value="8">Armed forces</option>
                    <option value="9">Disable</option>
                </select>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>Standard</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>1px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>2px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>3px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>4px</label>
                    <input type="text" class="md-input"><span class="md-input-bar"></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>5px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>6px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px to 12px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>12+px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
        </tr>
        
        <tr>
            <td style="width: 15%;vertical-align:bottom;">
                <select class="md-input" id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                    <option value="1">Armed forces</option>
                    <option value="3">Senior citizen</option>
                    <option value="4">Children</option>
                    <option value="5">Infant</option>
                    <option value="7">Student</option>
                    <option value="8">Armed forces</option>
                </select>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>Standard</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>1px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>2px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>3px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>4px</label>
                    <input type="text" class="md-input"><span class="md-input-bar"></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>5px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>6px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>7px to 12px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
            <td style="vertical-align:bottom;">
                <div class="md-input-wrapper">
                    <label>12+px</label>
                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                </div>
            </td>
        </tr>
    </tbody>
    </table>
    </div>

    <hr/>


    <div class="uk-width-medium-1-10">
    <span class="uk-input-group-addon">
    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
    </span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</script>
@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script><!-- ckeditor -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ckeditor/ckeditor.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ckeditor/adapters/jquery.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_wysiwyg.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/plugins_fullcalendar.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>
<script type="text/javascript">

                                                            $(function () {

                                                                if (location.hash && location.hash.length) {
                                                                    var hash = decodeURIComponent(location.hash.substr(1));
                                                                    setTimeout(function () {
                                                                        $("ul#user_profile_tabs li." + hash).trigger("click");
                                                                    }, 1000);
                                                                }
                                                            });
</script>

@endsection
