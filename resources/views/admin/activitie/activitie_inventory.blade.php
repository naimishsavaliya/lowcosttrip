@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Activities Inventory</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8" style="padding:8px 0;">
            <a href="{{ env('ADMIN_URL')}}activities/inventory/add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Activities Inventory
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Activitie Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Published At</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
<!--                <tfoot>
                    <tr>
                        <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Hotel Name" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Start Date" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="End Date" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Published At" name="" colPos="1"></td>
                        <td ><select class="chosen" data-placeholder="Choose a Status" tabindex="1" colPos="6">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="2">InActive</option>
                            </select></td>
                        <td></td>
                    </tr>
                </tfoot>-->
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>Hotel <?php echo $i; ?></td>
                            <td>2018-05-01</td>
                            <td>2018-05-01</td>
                            <td>2018-05-01</td>
                            <td><span class="label label-success">Active</span></td>
                            <td>
                                <a href="{{ env('ADMIN_URL')}}activities/inventory/edit/1" title="Edit Activities Inventory" class=""><i class="md-icon material-icons">&#xE254;</i></a> 
                                <a href="{{ env('ADMIN_URL')}}activities/inventory/delete/1" onclick="return confirm('Are you sure to delete?');" title="Delete Activities Inventory" class=""><i class="md-icon material-icons">delete</i></a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

