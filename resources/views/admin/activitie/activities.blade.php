@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Activities</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('activities.add')}}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')


            <table class="uk-table dt_default" >
                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Activities Name</th>
                        <th class="hidden-phone">Location</th>
                        <th class="hidden-phone">Supplier</th>
                        <th class="hidden-phone">Duration</th>
                        <th class="hidden-phone">Total Booking</th>
                        <th class="hidden-phone">Total Inventory</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>Activities <?php echo $i; ?></td>
                            <td class="hidden-phone">City,State<br />Country</td>
                            <td class="hidden-phone">Supplier</td>
                            <td class="hidden-phone"><?php echo $i; ?> Day</td>
                            <td class="hidden-phone">20</td>
                            <td class="hidden-phone">15</td>
                            <td><span class="label label-success">Active</span></td>
                            <td>
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                    <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav uk-nav-dropdown">
                                            <li><a href="{{ route('activity.view','1') }}">View Activity</a></li>
                                            <li><a href="{{ route('activities.detail','1') }}">Edit Activity</a></li>
                                            <li><a href="{{ route('activity.inventory','1') }}">View/Edit Inventory</a></li>
                                            <li><a href="{{ route('activities.detail','1') }}#bookinglist">View Booking List</a></li>
                                            <li><a href="{{ route('activities.detail','1') }}#bookingcalander">View Booking Calander</a></li>
                                            <!--<li><a href="{{ route('activities.detail','1') }}#newbooking">Add sNew Booking</a></li>-->
                                            <li><a href="{{ route('activities.detail','1') }}#discount">Manage Discount</a></li>
                                            <li><a href="{{ route('activities.detail','1') }}#rates">Manage Rates</a></li>
                                            <li><a href="javascript:;" onclick="return confirm('Are you sure to delete?');">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE widget-->
</div>

@endsection