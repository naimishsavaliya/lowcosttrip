@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ route('book-activities.store') }}" class="" id="tour-activities" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($activities->id) ? $activities->id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select  data-placeholder="Select country..." class="md-input" name="country_id" id="country_id">
                                            <option value="" selected>Select Country</option>
                                            @if(isset($countrys))
                                            @foreach($countrys as $country)
                                            <option value="{{ $country->country_id }}" {{ (isset($activities->country_id) && $activities->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>

                        
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Title<span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="title" value="{{ isset($activities->title) ? $activities->title : old('title') }}" />                                        
                                        <small class="text-danger">{{ $errors->first('title') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Description<span class="required-lbl">*</span></label>
                                        <textarea name="description" class="md-input autosized"  cols="30" rows="4" > {{ isset($activities->description) ? $activities->description : old('description') }}</textarea>                                      
                                        <small class="text-danger">{{ $errors->first('description') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="status-column">
                                        <span class="icheck-inline">
                                            <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($activities->status) && $activities->status == 1) ? 'checked' : ''  }} {{ (!isset($activities)) ? 'checked' : '' }} />
                                                   <label for="active" class="inline-label">Active</label>
                                        </span>
                                        <span class="icheck-inline">
                                            <input type="radio" name="status" value="2" id="inactive" data-md-icheck {{ (isset($activities->status) && $activities->status == 2) ? 'checked' : ''  }} />
                                                   <label for="inactive" class="inline-label">In Active</label>
                                        </span>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('status') }}</small>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('country.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection
@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>
<script type="text/javascript">
    $(function () {
// It has the name attribute "category_frm"
        $("form#tour-activities").validate({
// Specify validation rules
            rules: {
                country_id: "required",
                title: "required",
                description: "required",
                status: "required",
    },
    // Specify validation error messages
    messages: {
        country_id: "Please select country name",
        title: "Please enter title",
        description: "Please enter description",
        status: "Please select status",
    },
    errorPlacement: function (error, element) {
        if (element.attr("type") == "radio") {
            error.insertAfter($(element).parents('div.status-column'));
        } else {
            error.insertAfter($(element).parents('div.md-input-wrapper'));
        }
    },
    // in the "action" attribute of the form when valid
    submitHandler: function (form) {
        form.submit();
    }
});

});

</script>
@endsection

