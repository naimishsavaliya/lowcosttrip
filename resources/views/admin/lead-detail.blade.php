@extends('layouts.admin')
@section('content')
<link href="{{ env('APP_PUBLIC_URL')}}front/css/style.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/css/bootstrap.min.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/css/animate.min.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/css/updates.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/css/responsive.css" rel="stylesheet" />

<script src="{{ env('APP_PUBLIC_URL')}}front/js/js/jquery-1.11.1.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}front/js/jquery-ui.1.10.4.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}front/js/bootstrap.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}front/js/theme-scripts.js"></script>
<div id="main-content">
    <div class="container-fluid">


        <div class="row">
            <section id="content1">
                <div class="container">
                    <div class="row">
                        <div id="main" class="col-md-9">

                            <div id="cruise-features" class="tab-container">
                                <ul class="tabs">
                                    <li class="active"><a href="#cruise-description" data-toggle="tab">Description</a></li>
                                    <li><a href="#quotation" data-toggle="tab">Quotation</a></li>
                                    <li><a href="#cruise-availability" data-toggle="tab">Hotel</a></li>
                                    <li><a href="#cruise-food-dinning" data-toggle="tab">Notes</a></li>
                                    <li><a href="#reminder-dinning" data-toggle="tab">Reminder</a></li>
                                    <li><a href="#cruise-flight" data-toggle="tab">Flights</a></li>
                                    <li><a href="#cruise-reviews" data-toggle="tab">Activities</a></li>
                                    <!--                                    <li><a href="#cruise-write-review" data-toggle="tab">Write a Review</a></li>-->
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="cruise-description">
                                        <div class="listing-style3 cruise">
                                            <article class="box">
                                                <figure class="col-sm-2">
                                                    <a title="" href="ajax/cruise-slideshow-popup.html" class="hover-effect popup-gallery"><img alt="" src="http://placehold.it/270x160" width="270" height="160"></a>
                                                </figure>
                                                <div class="details col-sm-10">
                                                    <div class="clearfix">
                                                        <h4 class="box-title pull-left">
                                                            Lead Name<small>Booking ID: 1231568</small>
                                                        </h4>
<!--                                                        <span class="price pull-left"><small>from</small>$239</span>-->
                                                    </div>
                                                    <div class="character clearfix">
                                                        <div class="col-xs-3 date">
                                                            <i class="soap-icon-clock yellow-color"></i>
                                                            <div>
                                                                <span class="skin-color">Created Date</span><br>Jan 26, 2014
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 date">
                                                            <i class="soap-icon-clock yellow-color"></i>
                                                            <div>
                                                                <span class="skin-color">Modify Date</span><br>Jan 26, 2014
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 departure">
                                                            <i class="soap-icon-user yellow-color"></i>
                                                            <div>
                                                                <span class="skin-color">Created By</span><br>User Name
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 departure">
                                                            <i class="soap-icon-user yellow-color"></i>
                                                            <div>
                                                                <span class="skin-color">Modify By</span><br>User Name
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix">
                                                        <!--<a href="cruise-detailed.html" class="button btn-small pull-right">select cruise</a>-->
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="intro table-wrapper full-width hidden-table-sms">
                                            <div class="col-sm-9 col-lg-7 features table-cell">
                                                <ul>
                                                    <li><label style="width: auto;">Customer name:</label>Fname Lname</li>
                                                    <li><label style="width: auto;">Status:</label>Standard</li>
                                                    <li><label style="width: auto;">Departure Date:</label>17-01-2019</li>
                                                    <li><label style="width: auto;">Interested Destination:</label>Dummy Text</li>
                                                    <li><label style="width: auto;">Adult:</label>2</li>
                                                    <li><label style="width: auto;">Child:</label>2</li>
                                                    <li><label style="width: auto;">Hotel Preferences:</label>Not Offered</li>
                                                    <li><label style="width: auto;">Duration:</label>5 Day</li>
                                                    <li><label style="width: auto;">Flights:</label>Domestic</li>
                                                    <li><label style="width: auto;">Internal Transportation:</label>No</li>
                                                    <li><label style="width: auto;">Budget:</label>32,500</li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-5 col-lg-5 table-cell cruise-itinerary">
                                                <div class="travelo-box">
                                                    <h4 class="box-title">Contact Details</h4>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Email</th>
                                                                <th>Phone</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>contact@email.com</td>
                                                                <td>9565456578</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>contact@email.com</td>
                                                                <td>9565456578</td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td>contact@email.com</td>
                                                                <td>9565456578</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="long-description">
                                            <h2>About Carnival Cruise</h2>
                                            <p>
                                                Sed aliquam nunc eget velit imperdiet, in rutrum mauris malesuada. Quisque ullamcorper vulputate nisi, et fringilla ante convallis quis. Nullam vel tellus non elit suscipit volutpat. Integer id felis et nibh rutrum dignissim ut non risus. In tincidunt urna quis sem luctus, sed accumsan magna pellentesque. Donec et iaculis tellus. Vestibulum ut iaculis justo, auctor sodales lectus. Donec et tellus tempus, dignissim maurornare, consequat lacus. Integer dui neque, scelerisque nec sollicitudin sit amet, sodales a erat. Duis vitae condimentum ligula. Integer eu mi nisl. Donec massa dui, commodo id arcu quis, venenatis scelerisque velit.
                                                <br /><br />
                                                Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla condimentum, semper dolor non, faucibus dolor. Vivamus adipiscing eros quis orci fringilla, sed pretium lectus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec nec velit non odio aliquam suscipit. Sed non neque faucibus, condimentum lectus at, accumsan enim. Fusce pretium egestas cursus. Etiam consectetur, orci vel rutrum volutpat, odio odio pretium nisiodo tellus libero et urna. Sed commodo ipsum ligula, id volutpat risus vehicula in. Pellentesque non massa eu nibh posuere bibendum non sed enim. Maecenas lobortis nulla sem, vel egestas dui ullamcorper ac.
                                                <br /><br />
                                                Sed scelerisque lectus sit amet faucibus sodales. Proin ut risus tortor. Etiam fermentum tellus auctor, fringilla sapien et, congue quam. In a luctus tortor. Suspendisse eget tempor libero, ut sollicitudin ligula. Nulla vulputate tincidunt est non congue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus at est imperdiet, dapibus ipsum vel, lacinia nulla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus id interdum lectus, ut elementum elit. Nullam a molestie magna. Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla condimentum, semper dolor non, faucibus dolor. Vivamus adipiscing eros quis orci fringilla, sed pretium lectus viverra.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="cruise-availability">
                                        <!--                                        <div class="border-box travelo-box clearfix">
                                                                                    <h4 class="title col-xs-12">Select a Stateroom Category</h4>
                                                                                    <form class="update-search-form">
                                                                                        <div class="form-group col-xs-6 col-md-3">
                                                                                            <label>Date</label>
                                                                                            <div class="datepicker-wrap">
                                                                                                <input type="text" class="input-text full-width" placeholder="mon, Jan 26, 2014">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-xs-6 col-md-3">
                                                                                            <label>Room category</label>
                                                                                            <div class="selector">
                                                                                                <select>
                                                                                                    <option value="">Suite</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-xs-8 col-md-4">
                                                                                            <label>Price</label>
                                                                                            <div class="selector">
                                                                                                <select>
                                                                                                    <option value="">Up to $300</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-xs-4 col-md-2">
                                                                                            <label>&nbsp;</label>
                                                                                            <button type="submit" class="icon-check full-width">PROCEED</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>-->
                                        <h2>Hotels </h2>
                                        <div class="room-list listing-style3 hotel">
                                            <article class="box">
                                                <figure class="col-sm-4 col-md-3">
                                                    <a class="hover-effect popup-gallery" href="#"><img src="http://placehold.it/230x160" alt=""></a>
                                                </figure>
                                                <div class="details col-xs-12 col-sm-8 col-md-9">
                                                    <div>
                                                        <div>
                                                            <div class="box-title">
                                                                <h4 class="title">Grand Suite</h4>
                                                                <dl class="description">
                                                                    <dt>Deck:</dt>
                                                                    <dd>Upper</dd>
                                                                    <dt>Size:</dt>
                                                                    <dd>330 sq ft Balcony: 70 sq ft.</dd>
                                                                </dl>
                                                            </div>
                                                            <div class="amenities">
                                                                <i class="soap-icon-wifi circle"></i>
                                                                <i class="soap-icon-fitnessfacility circle"></i>
                                                                <i class="soap-icon-fork circle"></i>
                                                                <i class="soap-icon-television circle"></i>
                                                            </div>
                                                        </div>
                                                        <div class="price-section">
                                                            <span class="price"><small>PER/NIGHT</small>$121</span>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                                        <div class="action-section">
                                                            <a href="#" class="button btn-small full-width text-center">BOOK NOW</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="box">
                                                <figure class="col-sm-4 col-md-3">
                                                    <a class="hover-effect popup-gallery" href="#"><img src="http://placehold.it/230x160" alt=""></a>
                                                </figure>
                                                <div class="details col-xs-12 col-sm-8 col-md-9">
                                                    <div>
                                                        <div>
                                                            <div class="box-title">
                                                                <h4 class="title">Junior Suite</h4>
                                                                <dl class="description">
                                                                    <dt>Deck:</dt>
                                                                    <dd>Verandah</dd>
                                                                    <dt>Size:</dt>
                                                                    <dd>220 sq ft Balcony: 30 sq ft.</dd>
                                                                </dl>
                                                            </div>
                                                            <div class="amenities">
                                                                <i class="soap-icon-wifi circle"></i>
                                                                <i class="soap-icon-fitnessfacility circle"></i>
                                                                <i class="soap-icon-fork circle"></i>
                                                                <i class="soap-icon-television circle"></i>
                                                            </div>
                                                        </div>
                                                        <div class="price-section">
                                                            <span class="price"><small>PER/NIGHT</small>$325</span>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                                        <div class="action-section">
                                                            <a href="#" class="button btn-small full-width text-center">BOOK NOW</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="box">
                                                <figure class="col-sm-4 col-md-3">
                                                    <a class="hover-effect popup-gallery" href="#"><img src="http://placehold.it/230x160" alt=""></a>
                                                </figure>
                                                <div class="details col-xs-12 col-sm-8 col-md-9">
                                                    <div>
                                                        <div>
                                                            <div class="box-title">
                                                                <h4 class="title">Junior Suite (obstructed view)</h4>
                                                                <dl class="description">
                                                                    <dt>Deck:</dt>
                                                                    <dd>Upper</dd>
                                                                    <dt>Size:</dt>
                                                                    <dd>220 sq ft Balcony: 30 sq ft.</dd>
                                                                </dl>
                                                            </div>
                                                            <div class="amenities">
                                                                <i class="soap-icon-wifi circle"></i>
                                                                <i class="soap-icon-fitnessfacility circle"></i>
                                                                <i class="soap-icon-fork circle"></i>
                                                                <i class="soap-icon-television circle"></i>
                                                            </div>
                                                        </div>
                                                        <div class="price-section">
                                                            <span class="price"><small>PER/NIGHT</small>$248</span>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                                        <div class="action-section">
                                                            <a href="#" class="button btn-small full-width text-center">BOOK NOW</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="quotation" style="background: #f5f5f5;">
                                        <span class="price">
                                            <a class="button btn-small yellow-bg white-color" href="{{ env('ADMIN_URL')}}quotation/add">Add New</a>
                                        </span>
                                        <h2 class="box-title">Lead Name Quotations</h2>
                                        <div class="cruise-list listing-style3 cruise">
                                            <article class="box">
                                                <div class="details col-sm-12">
                                                    <div class="clearfix">
                                                        <h4 class="box-title pull-left">Quotation 1<small>Status</small></h4>
                                                        <!--<span class="price pull-right">$239</span>-->
                                                    </div>
                                                    <div class="character clearfix">
                                                        <div class="col-xs-3 cruise-logo">
                                                            <i class="soap-icon-clock yellow-color"></i> 
                                                            <div>
                                                                <span class="skin-color"> &nbsp; Quote Date</span><br>mon, Jan 26, 2014
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4 date">
                                                            <i class="soap-icon-clock yellow-color"></i>
                                                            <div>
                                                                <span class="skin-color"> Valid Till</span><br>mon, Jan 26, 2014
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5 departure">
                                                            <i class="soap-icon-departure yellow-color"></i>
                                                            <div>
                                                                <span class="skin-color">Departure</span><br>Los Angeles, miami, Florida
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                                    <div class="clearfix">
                                                                                                            <div class="review pull-left">
                                                                                                                <div class="five-stars-container">
                                                                                                                    <span class="five-stars" style="width: 60%;"></span>
                                                                                                                </div>
                                                                                                                <span>270 reviews</span>
                                                                                                            </div>
                                                                                                            <a href="cruise-detailed.html" class="button btn-small pull-right">select cruise</a>
                                                                                                        </div>-->
                                                </div>
                                            </article>
                                            <article class="box">
                                                <div class="details col-sm-12">
                                                    <div class="clearfix">
                                                        <h4 class="box-title pull-left">Quotation 2<small>Status</small></h4>
                                                        <!--<span class="price pull-right">$239</span>-->
                                                    </div>
                                                    <div class="character clearfix">
                                                        <div class="col-xs-3 cruise-logo">
                                                            <i class="soap-icon-clock yellow-color"></i> 
                                                            <div>
                                                                <span class="skin-color"> &nbsp; Quote Date</span><br>mon, Jan 26, 2014
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4 date">
                                                            <i class="soap-icon-clock yellow-color"></i>
                                                            <div>
                                                                <span class="skin-color"> Valid Till</span><br>mon, Jan 26, 2014
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5 departure">
                                                            <i class="soap-icon-departure yellow-color"></i>
                                                            <div>
                                                                <span class="skin-color">Departure</span><br>Los Angeles, miami, Florida
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                                    <div class="clearfix">
                                                                                                            <div class="review pull-left">
                                                                                                                <div class="five-stars-container">
                                                                                                                    <span class="five-stars" style="width: 60%;"></span>
                                                                                                                </div>
                                                                                                                <span>270 reviews</span>
                                                                                                            </div>
                                                                                                            <a href="cruise-detailed.html" class="button btn-small pull-right">select cruise</a>
                                                                                                        </div>-->
                                                </div>
                                            </article>
                                            <div class="booking-information travelo-box">
                                                <h2>Quotation Information</h2>
                                                <dl class="term-description col-sm-6">
                                                    <dt>Quotation Owner:</dt><dd>Fname Lname</dd>
                                                    <dt>Contact name:</dt><dd>Lucy Williams</dd>
                                                    <dt>Company:</dt><dd>Brown</dd>
                                                </dl>
                                                <dl class="term-description col-sm-6">
                                                    <dt>Trip Name:</dt><dd>Lucy - kerela trip</dd>
                                                    <dt>Subject:</dt><dd>Lucy William kerela - 3 Night/2 Day</dd>
                                                    <dt>Quote Date:</dt><dd>12-Apr-2018</dd>
                                                    <dt>Quote Stage:</dt><dd>Approved</dd>
                                                </dl>

                                                <h2>Address Information</h2>
                                                <dl class="term-description col-sm-6">
                                                    <dt>Biling Street:</dt><dd>14 Park Street</dd>
                                                    <dt>Billing City:</dt><dd>Kolkata</dd>
                                                    <dt>Billing State:</dt><dd>West Bengal</dd>
                                                    <dt>Billing Code:</dt><dd>700064</dd>
                                                    <dt>Billing Country:</dt><dd>India</dd>
                                                </dl>
                                                <dl class="term-description col-sm-6">
                                                    <dt>Shipping Street:</dt><dd>14 Park Street</dd>
                                                    <dt>Shipping City:</dt><dd>Kolkata</dd>
                                                    <dt>Shipping State:</dt><dd>West Bengal</dd>
                                                    <dt>Shipping Code:</dt><dd>700064</dd>
                                                    <dt>Shipping Country:</dt><dd>India</dd>
                                                </dl>

                                                <h2>Product Detail</h2>
                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><span class="hidden-phone">Product Detail</span></th>
                                                            <th><span class="hidden-phone">List Price(Rs.)</span></th>
                                                            <th><span class="hidden-phone">Quantity</span></th>
                                                            <th><span class="hidden-phone">Amount(Rs.)</span></th>
                                                            <th><span class="hidden-phone">Discount(Rs.)</span></th>
                                                            <th><span class="hidden-phone">Tax(Rs.)</span></th>
                                                            <th><span class="hidden-phone">Total(Rs.)</span></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Karela - 3 Nights/2 Days</td>
                                                            <td>55,000</td>
                                                            <td>3</td>
                                                            <td>20,000</td>
                                                            <td>75,000</td>
                                                            <td>3,000</td>
                                                            <td>78,000</td>
                                                            <td>
                                                                <a href="#" class="icon-remove hidden-phone hidden-tablet"></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Karela - 3 Nights/2 Days</td>
                                                            <td>55,000</td>
                                                            <td>3</td>
                                                            <td>20,000</td>
                                                            <td>75,000</td>
                                                            <td>3,000</td>
                                                            <td>78,000</td>
                                                            <td>
                                                                <a href="#" class="icon-remove hidden-phone hidden-tablet"></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <dl class="term-description col-sm-6 col-sm-offset-6">
                                                    <dt>Sub Total:</dt><dd>1,10,000</dd>
                                                    <dt>Discount:</dt><dd>6,000</dd>
                                                    <dt>Tax:</dt><dd>7,000</dd>
                                                    <dt>Adjustment:</dt><dd>0</dd>
                                                    <dt>Grand Total:</dt><dd>2,45,000</dd>
                                                </dl>

                                                <hr>
                                                <h2>Term & Conditions</h2>
                                                <p>Praesent dolor lectus, rutrum sit amet risus vitae, imperdiet cursus neque. Nulla tempor nec lorem eu suscipit. Donec dignissim lectus a nunc molestie consectetur. Nulla eu urna in nisi adipiscing placerat. Nam vel scelerisque magna. Donec justo urna, posuere ut dictum quis.</p>
                                                <br>
                                                <!--<p class="red-color">Payment is made by Credit Card Via Paypal.</p>-->
                                                <hr>
                                                <h2>Description Information</h2>
                                                <p>Praesent dolor lectus, rutrum sit amet risus vitae, imperdiet cursus neque. Nulla tempor nec lorem eu suscipit. Donec dignissim lectus a nunc molestie consectetur. Nulla eu urna in nisi adipiscing placerat. Nam vel scelerisque magna. Donec justo urna, posuere ut dictum quis.</p>
                                                <!--<br>-->
                                                <!--<a href="#" class="red-color underline view-link">https://www.travelo.com/booking-details/?=f4acb19f-9542-4a5c-b8ee</a>-->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="cruise-flight">
                                        <div class="flight-list listing-style3 flight">
                                            <article class="box">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span><img alt="" src="http://placehold.it/270x160"></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div>
                                                                <h4 class="box-title">Indianapolis to Paris<small>Oneway flight</small></h4>
                                                                <a class="button btn-mini stop">1 STOP</a>
                                                                <div class="amenities">
                                                                    <i class="soap-icon-wifi circle"></i>
                                                                    <i class="soap-icon-entertainment circle"></i>
                                                                    <i class="soap-icon-fork circle"></i>
                                                                    <i class="soap-icon-suitcase circle"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <span class="price"><small>AVG/PERSON</small>$620</span>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time">
                                                                <div class="take-off col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">Take off</span><br>Wed Nov 13, 2013 7:50 Am
                                                                    </div>
                                                                </div>
                                                                <div class="landing col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">landing</span><br>Wed Nov 13, 2013 9:20 am
                                                                    </div>
                                                                </div>
                                                                <div class="total-time col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">total time</span><br>13 Hour, 40 minutes
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action">
                                                                <a href="flight-detailed.html" class="button btn-small full-width">SELECT NOW</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="box">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span><img alt="" src="http://placehold.it/270x160"></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div>
                                                                <h4 class="box-title">Indianapolis to Paris<small>Oneway flight</small></h4>
                                                                <a class="button btn-mini stop">1 STOP</a>
                                                                <div class="amenities">
                                                                    <i class="soap-icon-wifi circle"></i>
                                                                    <i class="soap-icon-entertainment circle"></i>
                                                                    <i class="soap-icon-fork circle"></i>
                                                                    <i class="soap-icon-suitcase circle"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <span class="price"><small>AVG/PERSON</small>$170</span>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time">
                                                                <div class="take-off col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">Take off</span><br>Wed Nov 13, 2013 7:50 Am
                                                                    </div>
                                                                </div>
                                                                <div class="landing col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">landing</span><br>Wed Nov 13, 2013 9:20 am
                                                                    </div>
                                                                </div>
                                                                <div class="total-time col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">total time</span><br>13 Hour, 40 minutes
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action">
                                                                <a href="flight-detailed.html" class="button btn-small full-width">SELECT NOW</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="box">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span><img alt="" src="http://placehold.it/270x160"></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div>
                                                                <h4 class="box-title">Indianapolis to Paris<small>Oneway flight</small></h4>
                                                                <a class="button btn-mini stop">1 STOP</a>
                                                                <div class="amenities">
                                                                    <i class="soap-icon-wifi circle"></i>
                                                                    <i class="soap-icon-entertainment circle"></i>
                                                                    <i class="soap-icon-fork circle"></i>
                                                                    <i class="soap-icon-suitcase circle"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <span class="price"><small>AVG/PERSON</small>$320</span>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time">
                                                                <div class="take-off col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">Take off</span><br>Wed Nov 13, 2013 7:50 Am
                                                                    </div>
                                                                </div>
                                                                <div class="landing col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">landing</span><br>Wed Nov 13, 2013 9:20 am
                                                                    </div>
                                                                </div>
                                                                <div class="total-time col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">total time</span><br>13 Hour, 40 minutes
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action">
                                                                <a href="flight-detailed.html" class="button btn-small full-width">SELECT NOW</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="box">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span><img alt="" src="http://placehold.it/270x160"></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div>
                                                                <h4 class="box-title">Indianapolis to Paris<small>Oneway flight</small></h4>
                                                                <a class="button btn-mini stop">1 STOP</a>
                                                                <div class="amenities">
                                                                    <i class="soap-icon-wifi circle"></i>
                                                                    <i class="soap-icon-entertainment circle"></i>
                                                                    <i class="soap-icon-fork circle"></i>
                                                                    <i class="soap-icon-suitcase circle"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <span class="price"><small>AVG/PERSON</small>$620</span>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time">
                                                                <div class="take-off col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">Take off</span><br>Wed Nov 13, 2013 7:50 Am
                                                                    </div>
                                                                </div>
                                                                <div class="landing col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">landing</span><br>Wed Nov 13, 2013 9:20 am
                                                                    </div>
                                                                </div>
                                                                <div class="total-time col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">total time</span><br>13 Hour, 40 minutes
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action">
                                                                <a href="flight-detailed.html" class="button btn-small full-width">SELECT NOW</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="box">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span><img alt="" src="http://placehold.it/270x160"></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div>
                                                                <h4 class="box-title">Indianapolis to Paris<small>Oneway flight</small></h4>
                                                                <a class="button btn-mini stop">1 STOP</a>
                                                                <div class="amenities">
                                                                    <i class="soap-icon-wifi circle"></i>
                                                                    <i class="soap-icon-entertainment circle"></i>
                                                                    <i class="soap-icon-fork circle"></i>
                                                                    <i class="soap-icon-suitcase circle"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <span class="price"><small>AVG/PERSON</small>$170</span>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time">
                                                                <div class="take-off col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">Take off</span><br>Wed Nov 13, 2013 7:50 Am
                                                                    </div>
                                                                </div>
                                                                <div class="landing col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">landing</span><br>Wed Nov 13, 2013 9:20 am
                                                                    </div>
                                                                </div>
                                                                <div class="total-time col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">total time</span><br>13 Hour, 40 minutes
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action">
                                                                <a href="flight-detailed.html" class="button btn-small full-width">SELECT NOW</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="box">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span><img alt="" src="http://placehold.it/270x160"></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div>
                                                                <h4 class="box-title">Indianapolis to Paris<small>Oneway flight</small></h4>
                                                                <a class="button btn-mini stop">1 STOP</a>
                                                                <div class="amenities">
                                                                    <i class="soap-icon-wifi circle"></i>
                                                                    <i class="soap-icon-entertainment circle"></i>
                                                                    <i class="soap-icon-fork circle"></i>
                                                                    <i class="soap-icon-suitcase circle"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <span class="price"><small>AVG/PERSON</small>$320</span>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time">
                                                                <div class="take-off col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">Take off</span><br>Wed Nov 13, 2013 7:50 Am
                                                                    </div>
                                                                </div>
                                                                <div class="landing col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">landing</span><br>Wed Nov 13, 2013 9:20 am
                                                                    </div>
                                                                </div>
                                                                <div class="total-time col-sm-4">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>
                                                                        <span class="skin-color">total time</span><br>13 Hour, 40 minutes
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action">
                                                                <a href="flight-detailed.html" class="button btn-small full-width">SELECT NOW</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>  
                                    <div class="tab-pane fade" id="cruise-food-dinning">
                                        <div class="box">
                                            <span class="price">
                                                <a class="button btn-small yellow-bg white-color" href="{{ env('ADMIN_URL')}}lead/notes/add">Add New</a>
                                            </span>
                                            <h2 class="box-title">Notes</h2>
                                        </div>
                                        <div class="food-dinning-list image-box style2">
                                            <div class="box">
                                                <!--                                                                                                <figure>
                                                                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Sushi Bar
                                                            <small><i class="soap-icon-calendar yellow-color"></i> 18-05-2018</small>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <!--                                                <figure>
                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Sushi Bar
                                                            <small><i class="soap-icon-calendar yellow-color"></i> 18-05-2018</small>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <!--                                                <figure>
                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Sushi Bar
                                                            <small><i class="soap-icon-calendar yellow-color"></i> 18-05-2018</small>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <!--                                                <figure>
                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Sushi Bar
                                                            <small><i class="soap-icon-calendar yellow-color"></i> 18-05-2018</small>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <!--                                                <figure>
                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Sushi Bar
                                                            <small><i class="soap-icon-calendar yellow-color"></i> 18-05-2018</small>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="reminder-dinning">
                                        <div class="box">
<!--                                            <span class="price">
                                                <a class="button btn-small yellow-bg white-color" href="{{ env('ADMIN_URL')}}lead/notes/add">Add New</a>
                                            </span>-->
                                            <h2 class="box-title">Reminders</h2>
                                        </div>
                                        <div class="food-dinning-list image-box style2">
                                            <div class="box">
                                                <!--                                                                                                <figure>
                                                                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="titl">
                                                            Reminder 1
                                                            <div class="col-sm-12 m-l-0 p-l-0">
                                                                <small class="col-sm-2 m-l-0 p-l-0"><i class="soap-icon-calendar yellow-color"></i><strong>Sent: </strong> 18-05-2018</small>
                                                                <small class="col-sm-2"><i class="soap-icon-letter yellow-color"></i><strong>Sent Via: </strong> Email</small>
                                                            </div>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <!--                                                <figure>
                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Reminder 2
                                                            <div class="col-sm-12 m-l-0 p-l-0">
                                                                <small class="col-sm-2 m-l-0 p-l-0"><i class="soap-icon-calendar yellow-color"></i><strong>Sent: </strong> 18-05-2018</small>
                                                                <small class="col-sm-2"><i class="soap-icon-letter yellow-color"></i><strong>Sent Via: </strong> Email</small>
                                                            </div>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <!--                                                <figure>
                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Reminder 3
                                                            <div class="col-sm-12 m-l-0 p-l-0">
                                                                <small class="col-sm-2 m-l-0 p-l-0"><i class="soap-icon-calendar yellow-color"></i><strong>Sent: </strong> 18-05-2018</small>
                                                                <small class="col-sm-2"><i class="soap-icon-letter yellow-color"></i><strong>Sent Via: </strong> Email</small>
                                                            </div>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <!--                                                <figure>
                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Reminder 4
                                                            <div class="col-sm-12 m-l-0 p-l-0">
                                                                <small class="col-sm-2 m-l-0 p-l-0"><i class="soap-icon-calendar yellow-color"></i><strong>Sent: </strong> 18-05-2018</small>
                                                                <small class="col-sm-2"><i class="soap-icon-letter yellow-color"></i><strong>Sent Via: </strong> Email</small>
                                                            </div>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <!--                                                <figure>
                                                                                                    <a href="#"><img src="http://placehold.it/250x160" alt=""></a>
                                                                                                </figure>-->
                                                <div class="details">
                                                    <div class="review-score">
                                                        <!--                                                        <div class="five-stars-container" data-placement="bottom" data-toggle="tooltip" title="4 stars"><div class="five-stars" style="width: 80%;"></div></div>-->
                                                                                                                <!--<span class="review">27 reviews</span>-->
                                                    </div>
                                                    <div class="box-title">
                                                        <h4 class="title">
                                                            Reminder 15
                                                            <div class="col-sm-12 m-l-0 p-l-0">
                                                                <small class="col-sm-2 m-l-0 p-l-0"><i class="soap-icon-calendar yellow-color"></i><strong>Sent: </strong> 18-05-2018</small>
                                                                <small class="col-sm-2"><i class="soap-icon-letter yellow-color"></i><strong>Sent Via: </strong> Email</small>
                                                            </div>
                                                        </h4>
                                                        <!--                                                        <dl>
                                                                                                                    <dt>Seating Times:</dt>
                                                                                                                    <dd>Open nightly prior to dinner.</dd>
                                                                                                                </dl>-->
                                                    </div>
                                                    <hr class="hidden-xs">
                                                    <p>Watch the sushi chef prepare fresh Nigiri salmon, California rolls, Nigiri Toro, and other delicious Japanese delicacies for your enjoyment. Then grab some chopsticks and sample these colorful creations.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="tab-pane fade" id="cruise-reviews">
                                        <div class="guest-reviews">
                                            <span class="price">
                                                <a class="button btn-small yellow-bg white-color" href="{{ env('ADMIN_URL')}}activities/add">Add New</a>
                                            </span>
                                            <h2 class="box-title">
                                                Activities
                                            </h2>
                                            <div class="guest-review table-wrapper">
                                                <div class="col-xs-3 col-md-2 author table-cell">
                                                    <a href="#"><img src="http://placehold.it/270x263" alt="" width="270" height="263" /></a>
                                                    <p class="name">Jessica Brown</p>
                                                    <p class="date">NOV, 12, 2013</p>
                                                </div>
                                                <div class="col-xs-9 col-md-10 table-cell comment-container">
                                                    <div class="comment-header clearfix">
                                                        <h4 class="comment-title">Pretty good for a weekend</h4>
                                                        <!--                                                        <div class="review-score">
                                                                                                                    <div class="five-stars-container"><div class="five-stars" style="width: 80%;"></div></div>
                                                                                                                    <span class="score">4.0/5.0</span>
                                                                                                                </div>-->
                                                    </div>
                                                    <div class="comment-content">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="guest-review table-wrapper">
                                                <div class="col-xs-3 col-md-2 author table-cell">
                                                    <a href="#"><img src="http://placehold.it/270x263" alt="" width="270" height="263" /></a>
                                                    <p class="name">David Jhon</p>
                                                    <p class="date">NOV, 12, 2013</p>
                                                </div>
                                                <div class="col-xs-9 col-md-10 table-cell comment-container">
                                                    <div class="comment-header clearfix">
                                                        <h4 class="comment-title">First time cruise - FANTASTIC</h4>
                                                        <!--                                                        <div class="review-score">
                                                                                                                    <div class="five-stars-container"><div class="five-stars" style="width: 64%;"></div></div>
                                                                                                                    <span class="score">3.2/5.0</span>
                                                                                                                </div>-->
                                                    </div>
                                                    <div class="comment-content">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="guest-review table-wrapper">
                                                <div class="col-xs-3 col-md-2 author table-cell">
                                                    <a href="#"><img src="http://placehold.it/270x263" alt="" width="270" height="263" /></a>
                                                    <p class="name">Kyle Martin</p>
                                                    <p class="date">NOV, 12, 2013</p>
                                                </div>
                                                <div class="col-xs-9 col-md-10 table-cell comment-container">
                                                    <div class="comment-header clearfix">
                                                        <h4 class="comment-title">5-day Carnival Inspiration out of Tampa</h4>
                                                        <!--                                                        <div class="review-score">
                                                                                                                    <div class="five-stars-container"><div class="five-stars" style="width: 76%;"></div></div>
                                                                                                                    <span class="score">3.8/5.0</span>
                                                                                                                </div>-->
                                                    </div>
                                                    <div class="comment-content">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                    <div class="tab-pane fade" id="cruise-write-review">
                                                                            <div class="main-rating table-wrapper full-width hidden-table-sms intro">
                                                                                <article class="image-box box cruise listing-style1 photo table-cell col-sm-4">
                                                                                    <figure>
                                                                                        <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="http://placehold.it/270x160"></a>
                                                                                    </figure>
                                                                                    <div class="details">
                                                                                        <h4 class="box-title">Carnival Cruise Lines<small>Carnival inspiration</small></h4>
                                                                                        <div class="feedback">
                                                                                            <div title="4 stars" class="five-stars-container" data-toggle="tooltip" data-placement="bottom"><span class="five-stars" style="width: 80%;"></span></div>
                                                                                            <span class="review">270 reviews</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </article>
                                                                                <div class="table-cell col-sm-8">
                                                                                    <div class="overall-rating">
                                                                                        <h4>Your overall Rating of this property</h4>
                                                                                        <div class="star-rating clearfix">
                                                                                            <div class="five-stars-container"><div class="five-stars" style="width: 80%;"></div></div>
                                                                                            <span class="status">VERY GOOD</span>
                                                                                        </div>
                                                                                        <div class="detailed-rating">
                                                                                            <ul class="clearfix">
                                                                                                <li class="col-md-6"><div class="each-rating"><label>Ship Quality</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                                                                <li class="col-md-6"><div class="each-rating"><label>Ship Staff Quality</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                                                                <li class="col-md-6"><div class="each-rating"><label>Dining/Food</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                                                                <li class="col-md-6"><div class="each-rating"><label>Activities</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                                                                <li class="col-md-6"><div class="each-rating"><label>rooms Quality</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                                                                <li class="col-md-6"><div class="each-rating"><label>Play areas</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                                                                <li class="col-md-6"><div class="each-rating"><label>Cleanliness</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                                                                <li class="col-md-6"><div class="each-rating"><label>fitness facility</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <form class="review-form">
                                                                                <div class="form-group col-md-5 no-float no-padding">
                                                                                    <h4 class="title">Title of your review</h4>
                                                                                    <input type="text" name="review-title" class="input-text full-width" value="" placeholder="enter a review title" />
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <h4 class="title">Your review</h4>
                                                                                    <textarea class="input-text full-width" placeholder="enter your review (minimum 200 characters)" rows="5"></textarea>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <h4 class="title">What sort of Trip was this?</h4>
                                                                                    <ul class="sort-trip clearfix">
                                                                                        <li><a href="#"><i class="soap-icon-businessbag circle"></i></a><span>Business</span></li>
                                                                                        <li><a href="#"><i class="soap-icon-couples circle"></i></a><span>Couples</span></li>
                                                                                        <li><a href="#"><i class="soap-icon-family circle"></i></a><span>Family</span></li>
                                                                                        <li><a href="#"><i class="soap-icon-friends circle"></i></a><span>Friends</span></li>
                                                                                        <li><a href="#"><i class="soap-icon-user circle"></i></a><span>Solo</span></li>
                                                                                    </ul>
                                                                                </div>
                                                                                <div class="form-group col-md-5 no-float no-padding">
                                                                                    <h4 class="title">When did you travel?</h4>
                                                                                    <div class="selector">
                                                                                        <select class="full-width">
                                                                                            <option value="2014-6">June 2014</option>
                                                                                            <option value="2014-7">July 2014</option>
                                                                                            <option value="2014-8">August 2014</option>
                                                                                            <option value="2014-9">September 2014</option>
                                                                                            <option value="2014-10">October 2014</option>
                                                                                            <option value="2014-11">November 2014</option>
                                                                                            <option value="2014-12">December 2014</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <h4 class="title">Add a tip to help travelers choose a good ship</h4>
                                                                                    <textarea class="input-text full-width" rows="3" placeholder="write something here"></textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-5 no-float no-padding">
                                                                                    <h4 class="title">Do you have photos to share? <small>(Optional)</small> </h4>
                                                                                    <div class="fileinput full-width">
                                                                                        <input type="file" class="input-text" data-placeholder="select image/s" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <h4 class="title">Share with friends <small>(Optional)</small></h4>
                                                                                    <p>Share your review with your friends on different social media networks.</p>
                                                                                    <ul class="social-icons icon-circle clearfix">
                                                                                        <li class="twitter"><a title="Twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                                                                        <li class="facebook"><a title="Facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                                                                        <li class="googleplus"><a title="GooglePlus" href="#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                                                                        <li class="pinterest"><a title="Pinterest" href="#" data-toggle="tooltip"><i class="soap-icon-pinterest"></i></a></li>
                                                                                    </ul>
                                                                                </div>
                                                                                <div class="form-group col-md-5 no-float no-padding no-margin">
                                                                                    <button type="submit" class="btn-large full-width">SUBMIT REVIEW</button>
                                                                                </div>
                                                                            </form>
                                    
                                                                        </div>-->
                                </div>

                            </div>
                        </div>
                        <div class="sidebar col-md-3">
                            <article class="detailed-logo">
                                <figure>
                                    <img width="320" height="80" src="http://placehold.it/320x80" alt="">
                                </figure>
                                <div class="details">
                                    <h2 class="box-title">4 Night Baja Mexico<small>mon, Jan 26, 2014</small></h2>
                                    <span class="price clearfix">
                                        <small class="pull-left">from</small>
                                        <span class="pull-right">$239</span>
                                    </span>
                                    <div class="feedback clearfix">
                                        <div title="4 stars" class="five-stars-container" data-toggle="tooltip" data-placement="bottom"><span class="five-stars" style="width: 80%;"></span></div>
                                        <span class="review pull-right">270 reviews</span>
                                    </div>
                                    <p class="description">Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                    <a class="button yellow full-width uppercase btn-small">add to wishlist</a>
                                </div>
                            </article>
                            <div class="travelo-box contact-box">
                                <h4>Need Travelo Help?</h4>
                                <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                                <address class="contact-details">
                                    <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                                    <br>
                                    <a class="contact-email" href="#">help@travelo.com</a>
                                </address>
                            </div>
                            <div class="travelo-box book-with-us-box">
                                <h4>Why Book with us?</h4>
                                <ul>
                                    <li>
                                        <i class="soap-icon-hotel-1 circle"></i>
                                        <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                                        <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                    </li>
                                    <li>
                                        <i class="soap-icon-savings circle"></i>
                                        <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                                        <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                    </li>
                                    <li>
                                        <i class="soap-icon-support circle"></i>
                                        <h5 class="title"><a href="#">Excellent Support</a></h5>
                                        <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>

        
        <div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h3 id="myModalLabel1">Modal Header</h3>
            </div>
            <div class="modal-body">
                <div class="booking-section travelo-box">
                            <form class="cruise-booking-form">
                                <div class="person-information">
                                    <h2>Your Personal Information</h2>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>first name</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>last name</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>email address</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Verify E-mail Address</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Country of Citizenship</label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option>United Kingdom</option>
                                                    <option>United States</option>
                                                </select><span class="custom-select full-width">United Kingdom</span>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6 col-md-5">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label>Date of Birth</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" class="input-text full-width hasDatepicker" placeholder="mm/dd/yy" data-min-date="01/01/1900" id="dp1528961472440"><img class="ui-datepicker-trigger" src="images/icon/blank.png" alt="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label>Gender</label>
                                                    <div>
                                                        <label class="radio radio-inline radio-square checked">
                                                            <input type="radio" name="gender" checked="checked">Male
                                                        </label>
                                                        <label class="radio radio-inline radio-square">
                                                            <input type="radio" name="gender">Female
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Country code</label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option>United Kingdom (+44)</option>
                                                    <option>United States (+1)</option>
                                                </select><span class="custom-select full-width">United Kingdom (+44)</span>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Phone number</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Home Address</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                        <div class="form-group col-sm-6 col-md-5">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label>State</label>
                                                    <div class="selector full-width">
                                                        <select>
                                                            <option value="uk">UK</option>
                                                        </select><span class="custom-select">UK</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label>Zipcode</label>
                                                    <input type="text" class="input-text full-width">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> I want to receive <span class="skin-color">Travelo</span> promotional offers in the future
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="card-information">
                                    <h2>Your Card Information</h2>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Credit Card Type</label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option>select a card</option>
                                                </select><span class="custom-select full-width">select a card</span>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Card holder name</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Card number</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Card identification number</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-5">
                                            <label>Expiration Date</label>
                                            <div class="constant-column-2">
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option>month</option>
                                                    </select><span class="custom-select full-width">month</span>
                                                </div>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option>year</option>
                                                    </select><span class="custom-select full-width">year</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-2">
                                            <label>billing zip code</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> By continuing, you agree to the <a href="#"><span class="skin-color">Terms and Conditions</span></a>.
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-5">
                                        <button type="submit" class="full-width btn-large">CONFIRM BOOKING</button>
                                    </div>
                                </div>
                            </form>
                        </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection