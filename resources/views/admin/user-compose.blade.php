@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">New Message</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>


        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->   
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i>New Message</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{ env('ADMIN_URL')}}user/message" class="" method="get">
                            <div class="control-group">
                                <label class="control-label">To</label>
                                <div class="controls">
                                    <input type="text" class="span10 " />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Subject</label>
                                <div class="controls">
                                    <input type="text" class="span10 " />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Message</label>
                                <div class="controls ">
                                    <textarea class="span10 wysihtml5 " rows="8"></textarea>
                                </div>
                            </div>


                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <button type="button" class="btn">Cancel</button>
                            </div>
                        </form>
                        <!-- END FORM-->           
                    </div>
                </div>
                <!-- END SAMPLE FORM widget-->
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
@endsection