@extends('layouts.admin')
@section('content')


<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
                    </li>
                    <li>
                        <a href="#">{{env('BREAD_CRUMBS')}}</a> <span class="divider">&nbsp;</span>
                    </li>
                    <li><a href="#">Batch Manager Permission</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->   
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Role Permission</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{ env('ADMIN_URL')}}user/role" class="" method="get">

                            <div class="control-group">
                                <label class="control-label">Manage Book</label>
                                <div class="controls">
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 1
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 2
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 3
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 4
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 5
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 6
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 7
                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Batch</label>
                                <div class="controls">
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 1
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 2
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 3
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 4
                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Teachers</label>
                                <div class="controls">
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 1
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 2
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 3
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 4
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 5
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 6
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 7
                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Students</label>
                                <div class="controls">
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 1
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 2
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 3
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 4
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 5
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 6
                                    </label>
                                    <label class="checkbox span3">
                                        <input type="checkbox" value="" /> Page 7
                                    </label>
                                </div>
                            </div>


                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <button type="button" class="btn">Cancel</button>
                            </div>
                        </form>
                        <!-- END FORM-->           
                    </div>
                </div>
                <!-- END SAMPLE FORM widget-->
            </div>
        </div>
    </div>
</div>
@endsection