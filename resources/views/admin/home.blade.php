@extends('layouts.theme')

@section('style')
<!-- metrics graphics (charts) -->
<!--<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/metrics-graphics/dist/metricsgraphics.css">-->
<!-- chartist -->
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/chartist/dist/chartist.min.css">

<style>
    .ct-series-a .ct-bar, .ct-series-a .ct-line, .ct-series-a .ct-point, .ct-series-a .ct-slice-donut {
        stroke: #1f77b4;
    }
    .ct-series-b .ct-bar, .ct-series-b .ct-line, .ct-series-b .ct-point, .ct-series-b .ct-slice-donut {
        stroke: #1f77b4;
    }
    body {
        font-family: Raleway,"Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.428571429;
        color: #6a6c6f;
        background: #edf0f5;
    }
</style>
@endsection
@section('content') 
<?php $colors = array(1 => 'primary', 2 => 'danger', 3 => 'success', 4 => 'warning', 5 => 'info'); ?>
<div id="page_content">
    <div id="page_content_inner">

        <div class="uk-grid" data-uk-grid-margin="">
            <div class="uk-width-medium-1-12">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2 p-r-50">
                            <h2 style="color: #ff6541;padding-top: 18px;">Lead Status</h2>
                        </div>
                        <div class="uk-width-medium-1-6 p-l-50">
                            <label for="start_date">Start Date</label>
                            <input type="text" class="md-input" name="start_date" id="start_date" data-uk-datepicker="{format:'DD MMMM YYYY'}" value="{{ isset($_GET['start_date']) ? date('d F Y',strtotime($_GET['start_date'])) : '' }}" autocomplete="off" />
                            <small class="text-danger" style="display: none;" id="start_date_error">Please select start date</small>
                        </div>
                        <div class="uk-width-medium-1-6 p-l-50">
                            <label for="end_date">End Date</label>
                            <input type="text" class="md-input" name="end_date" id="end_date" data-uk-datepicker="{format:'DD MMMM YYYY'}" value="{{ isset($_GET['end_date']) ? date('d F Y',strtotime($_GET['end_date'])) : '' }}" autocomplete="off" />
                            <small class="text-danger" style="display: none;" id="end_date_error">Please select end date</small>
                        </div>
                        <div class="uk-width-medium-1-6 p-l-50">
                            <a href="#" id="Filter" class="md-btn md-btn-primary md-btn-large md-btn-wave-light waves-effect waves-button waves-light">Filter</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-grid uk-grid-width-large-1-5 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>

            <?php $i = 1; foreach ($intrested_ins as $ins_id => $intrested) { ?>
                <div>
                    <div class="md-card">
                        <div class="widget stats-widget">
                            <div class="widget-body clearfix">
                                <div class="pull-left">
                                    <h3 class="widget-title uk-text-{{ ((isset($colors[$i]) ) ? $colors[$i] : $colors[rand(1,8)]) }}">
                                        <span class="counter" data-plugin="counterUp"><strong>{{$intrested}}</strong></span>
                                    </h3>
                                    <?php 
                                    $total_Rec = 0;
                                    foreach ($status as $status_id => $status_name) { 
                                        $total_Rec += $rec = (isset($leads[$ins_id][$status_id]) ? count($leads[$ins_id][$status_id]) : 0);
                                    ?>
                                        <small class="text-color list-1">{{$status_name}}</small> 
                                        <small class="text-color list-2">{{$rec}}</small>
                                    <?php } ?>
                                </div>
                                <span class="pull-right big-icon watermark">
                                    <i class="fa fa-paperclip"></i>
                                </span>
                            </div>
                            <footer class="widget-footer bg-{{ ( (isset($colors[$i]) ) ? $colors[$i] : $colors[rand(1,8)] )}}">
                                <span class="list-1"><strong>Total {{$intrested}} leads</strong></span>
                                <span class="list-3"><strong>{{$total_Rec}}</strong></span>
                            </footer>
                        </div>
                    </div>
                </div>
            <?php $i++; } ?>
        </div>
        <?php 
            $month = array();
            for ($i = -12; $i <= 0; $i++){
                $month[date('m-Y', strtotime("$i month"))] = date('F - Y', strtotime("$i month"));
            }
        ?>

        <div  style="background: #FFF;border: 1px solid #D1D1D1">
            <div class="uk-width-medium-1-12">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2 p-r-50">
                            <h2 style="color: #ff6541;padding-top: 18px;margin-left: 16px;">Monthly Status</h2>
                        </div>
                        <div class="uk-width-medium-1-2 p-l-50">
                            <div class="uk-form-row" style="margin-right: 12px;">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3">
                                    <select class="md-input chart_filter" id="status" name="status" data-uk-tooltip="{pos:'bottom'}" title="Select Intrested In...">
                                        <option value="" selected >Select Status</option>
                                        @if(isset($status) && count($status) > 0)
                                        @foreach ($status as $status_id => $status_name) { 
                                        <option value="{{ $status_id }}" >{{ $status_name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                    <select class="md-input chart_filter" id="intrested_in" name="intrested_in" data-uk-tooltip="{pos:'bottom'}" title="Select Intrested In...">
                                        <option value="" selected >Select Interested In...</option>
                                        @if(isset($intrested_ins))
                                        @foreach($intrested_ins as $in_key => $intrested_in)
                                        <option value="{{ $in_key }}">{{ $intrested_in }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                    <select class="md-input chart_filter" id="month" name="month" data-uk-tooltip="{pos:'bottom'}" title="Select Intrested In...">
                                        <option value="">Select Month</option>
                                        @if(isset($month))
                                            <?php $j=0; ?>
                                            @foreach(array_reverse($month) as $m_k => $m)
                                                <option  {{(($j==0)? 'selected' : '')}} value="{{ $m_k }}">{{ $m }}</option>
                                                <?php $j++; ?>
                                            @endforeach
                                        @endif
                                    </select>
                                    </div>
                                    <!-- <div class="uk-width-medium-1-3" style="margin-top: 14px;">
                                        <div  class="dataTables_processing">Loading</div>
                                    </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="chart"> </div>
            </div>
        </div>
</div>

@endsection

@section('javascript')

<!-- chartist (charts) -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/chartist/dist/chartist.min.js"></script>
<!-- peity (small charts) -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/peity/jquery.peity.min.js"></script>
<!-- countUp -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/countUp.js/dist/countUp.min.js"></script>

<!--Chart JS  -->
<!-- <script src="{{ env('APP_PUBLIC_URL')}}new-theme/js/jquery.canvasjs.min.js"></script> -->
<script src="https://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>

<script>
window.onload = function () {
    create_chart();
    $('.chart_filter').change(function() {
        create_chart();
    });
}
function create_chart() {
    if($('#month').val() == ''){
        return false;
    }
     $.ajax({
        type: 'POST',
        url: "{{ route('admin.chart') }}",
        async: false,
        data: {_token: "{{csrf_token()}}", status: $('#status').val(), intrested_in: $('#intrested_in').val(), month: $('#month').val()},
        dataType: 'json',
        beforeSend: function () {
             $('#chart').empty();
            $('.dataTables_processing').css('visibility','visible');
        },
        success: function (returnData) {
            $('#chart').html('<canvas id="canvas" style="height:350px;"></canvas>');
            if(returnData.success == true){
                var config = {
                    type: 'bar',
                    data: {
                        labels: returnData.month,
                        datasets: returnData.chart_data
                    },
                    options: {
                        maintainAspectRatio: false,
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Lead Information'
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Days'
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,

                                    min: 0, // it is for ignoring negative step.
                                    beginAtZero: true,
                                    callback: function(value, index, values) {
                                        if (Math.floor(value) === value) {
                                            return value;
                                        }
                                    }

                                    
                                    // suggestedMin: 50,
                                    // suggestedMax: 100,
                                    // max: 5,
                                    // min: 0,

                                     // suggestedMin: 5,
                                     // stepSizeMin: 1
                                     // suggestedMax: auto
                                },

                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Number Of Leads'
                                }
                            }]
                        }
                    }

                };
                
                var ctx = document.getElementById('canvas').getContext('2d');
                window.myLine = new Chart(ctx, config);
            }else{
                
            }
        },
        error: function (xhr, textStatus, errorThrown) {
           
        },
        complete: function () {
            $('.dataTables_processing').css('visibility','hidden');
        }
    });
}
</script>

<script type="text/javascript">
    $('#Filter').click(function(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var url = "{{ Request::url() }}";
        if(start_date != '' && end_date !=''){
            $('#start_date_error').hide();
            $('#end_date_error').hide();
            window.location.href = url + '?start_date=' + start_date + '&end_date=' + end_date;
        }else if(start_date == '' && end_date !=''){
            $('#start_date_error').show();
            $('#end_date_error').hide();
        }else if (start_date != '' && end_date ==''){
            $('#start_date_error').hide();
            $('#end_date_error').show();
        }else{
            $('#start_date_error').show();
            $('#end_date_error').show();
        }
});
</script>


@endsection