@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('video.category.add')}}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table" id="albumcat_tbl">

                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th style="width: 20%;">Image</th>
                        <th>Category</th>
                        <th class="hidden-phone" style="width: 10%;">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td class="pd0"><input class="md-input" type="text"  placeholder="ID" colPos="0"></td>
                        <td></td>
                        <td ><input class="md-input" type="text"  placeholder="Title" name="" colPos="2"></td>
                        <td ><select class="md-input" data-placeholder="Choose a Status" tabindex="1" colPos="3">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="0">InActive</option>
                            </select></td>
                        <td></td>
                    </tr>
                </tfoot>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="changestatus" class="uk-modal" >
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Status</h3>
        </div>
        <div class="">
            <div class="control-group">
                <input type="hidden" value="" id="change-status-row-id" />
                <label class="control-label">Status</label>
                <div class="controls">
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="1" id="status_dialog_active" data-md-icheck />
                        <label for="status_dialog_active" class="inline-label">Active</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="0" id="status_dialog_inactive" data-md-icheck />
                        <label for="status_dialog_inactive" class="inline-label">InActive</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close" id="status-dlg-btn">Submit</button> 
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function () {

        var albumcategoryTable = $('#albumcat_tbl').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
//            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
//            "pagingType": "full_numbers", //full,full_numbers
            "ajax": {
                "url": "{{ route('video.category.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof albumcategoryTable != 'undefined' && albumcategoryTable.hasOwnProperty('settings')) {
                        albumcategoryTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "catId", "name": "catId"},
                {"data": "image", "render": function (data, type, row) {
                        return '<img src="' + row.image + '" style="height: 50px;">';
                    }
                },
                {"data": "title", "name": "title"},
                {"data": "status", "name": "status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.catId + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a>';
                        } else {
                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.catId + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-danger">InActive</span></a>';
                        }
                    },
                },
                {"data": "action", 'sortable': false, "render": function (data, type, row) {
                        var edit_url = "{{ route('video.category.edit', ':id') }}";
                        var dele_url = "{{ route('video.category.delete', ':id') }}";
                        edit_url = edit_url.replace(':id', row.catId);
                        dele_url = dele_url.replace(':id', row.catId);
                        return '<a href="' + edit_url + '" title="Edit Video Category" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + dele_url + '" data-id="' + row.catId + '" title="Delete Video Category" class="delete-category"><i class="md-icon material-icons">delete</i></a>';
                    },
                },
            ]
        });


        $(albumcategoryTable.table().container()).on('keyup', 'tfoot input', function () {
            albumcategoryTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(albumcategoryTable.table().container()).on('change', 'tfoot select', function () {
            albumcategoryTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(document.body).on('click', '.change-status-btn', function () {
            var $obj = $(this);
            var row_id = $obj.attr('row-id');
            var status = $obj.attr('data-id');
            $('input#change-status-row-id').val(row_id);
            if (status == "1") {
                $('#status_dialog_inactive').removeAttr('checked');
                $('#status_dialog_active').iCheck('check');
            } else {
                $('#status_dialog_active').removeAttr('checked');
                $('#status_dialog_inactive').iCheck('check');
            }
        });

        $(document.body).on('click', '#status-dlg-btn', function () {
            var $obj = $(this);
            var row_id = $('#change-status-row-id').val();
            var status = $('input.user-status-change:checked').val();
            $.ajax({
                type: 'get',
                url: '{{ route("change_status")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table": "category_video",
                    "id": row_id,
                    "column_name": "catId",
                    "status": status
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        albumcategoryTable.draw();
                    }
                    showNotify(data.msg_status, data.message);
                }
            });
        });

        $(document.body).on('click', '.delete-category', function () {
            var crm = confirm("Are you sure to delete?");
            if (crm) {
                var $obj = $(this);
                var lead_id = $obj.attr('data-id');
                var delete_url = $obj.attr('href');
                $.ajax({
                    type: 'get',
                    url: delete_url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {

                    },
                    success: function (data, textStatus, jqXHR) {
                        if (data.success) {
                            albumcategoryTable.draw();
                        }
                        showNotify(data.msg_status, data.message);
                    }
                });
            }
            return false;
        });

    });
</script>
@endsection
