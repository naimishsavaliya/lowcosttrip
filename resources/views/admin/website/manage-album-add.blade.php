@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
<link href="{{ env('APP_PUBLIC_URL')}}css/dropzone.css" type="text/css" rel="stylesheet" />
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('website.manage.album.store')}}" name="albums_frm" id="albums_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($album->albId) ? $album->albId : '' }}" />
                    <input type="hidden" name="temp_id" id="tour_temp_id" value="{{ str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT) }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Title</label>
                                        <input type="text" class="md-input" name="title" value="{{ isset($album->title) ? $album->title : old('title') }}" />
                                        @if($errors->has('title'))<small class="text-danger">{{ $errors->first('title') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <h3 class="heading_a"><span class="sub-heading">Album Category</span></h3>
                                <div class="uk-grid m-t-10 md-input-wrapper" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="category_id" name="category_id[]" class="uk-width-1-1" multiple data-md-select2 data-allow-clear="true" data-placeholder="Select Categoty...">
                                            <option value="">Choose Category</option>
                                            @if(isset($album_category))
                                            @foreach($album_category as $a_key => $album_cat)
                                            <option value="{{ $album_cat->albId }}" {{ (isset($album_cat_arr) && in_array($album_cat->albId, $album_cat_arr) ? "selected" : "") }}>{{ $album_cat->title }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('category_id'))<small class="text-danger">{{ $errors->first('category_id') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                            <label for="uk_dp_1">Select date</label>
                                            <input class="md-input" type="text" name="album_date" id="uk_dp_1" value="{{ isset($album->album_date) ? date('d F Y', strtotime($album->album_date)) : old('album_date') }}" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Customer Name</label>
                                        <input type="text" class="md-input" name="customer_name" value="{{ isset($album->customer_name) ? $album->customer_name : old('customer_name') }}" />
                                        @if($errors->has('customer_name'))<small class="text-danger">{{ $errors->first('customer_name') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <div class="dropzone" id="dropzoneFileUpload">
                                        </div>
                                        <!--                        <div class="fallback">
                                                                    <input type="file" name="file" multiple>
                                                                </div>-->
                                    </div>
                                </div>
                            </div>

                            <!--Dropzone Preview Template-->
                            <div id="preview" style="display: none;">

                                <div class="dz-preview dz-file-preview">
                                    <div class="dz-image"><img data-dz-thumbnail /></div>

                                    <div class="dz-details">
                                        <div class="dz-size"><span data-dz-size></span></div>
                                        <div class="dz-filename"><span data-dz-name></span></div>
                                    </div>
                                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                    <div class="dz-error-message"><span data-dz-errormessage></span></div>



                                    <div class="dz-success-mark">

                                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                        <title>Check</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                        </g>
                                        </svg>

                                    </div>
                                    <div class="dz-error-mark">

                                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                        <title>error</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                                        <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                                        </g>
                                        </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($album->status) && $album->status == 1) ? 'checked' : ''  }} {{ (!isset($album)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($album->status) && $album->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('website.manage.album') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  forms_file_upload functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_upload.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/dropzone.js"></script>
<script type="text/javascript">
$(function () {

    $("form#albums_frm").validate({
        rules: {
            title: "required",
            "category_id[]": "required",
            customer_name: "required",
            status: "required",
        },
        messages: {
            title: "Please enter title name",
            "category_id[]": "Please select category",
            customer_name: "Please enter customer name",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else if (element.is('select')) {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});

</script>
<script type="text/javascript">
    var total_photos_counter = 0;
    var name = "";
    var baseUrl = "{{ route('album.images.store') }}";
    var token = "{{ csrf_token() }}";
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#dropzoneFileUpload", {
        url: baseUrl,
        params: {
            _token: token,
            tour_temp_id: $('#tour_temp_id').val()
        },
        uploadMultiple: true,
        parallelUploads: 2,
        maxFilesize: 16,
        previewTemplate: document.querySelector('#preview').innerHTML,
        addRemoveLinks: true,
        dictRemoveFile: 'Remove file',
        dictFileTooBig: 'Image is larger than 16MB',
        timeout: 10000,
        renameFile: function (file) {
            name = new Date().getTime() + Math.floor((Math.random() * 100) + 1) + '_' + file.name;
            return name;
        },
        init: function () {
            var myDropzone = this;
             @if (Request::route('id'))
                $.get("{{ route('album.images.fetch', Request::route('id')) }}", function (data) {
                    $.each(data.images, function (key, value) {
                        var file = {name: value.original, size: value.size};
                        myDropzone.options.addedfile.call(myDropzone, file);
                        myDropzone.options.thumbnail.call(myDropzone, file, value.server);
                        myDropzone.emit("complete", file);
                        total_photos_counter++;
                        $("#counter").text("(" + total_photos_counter + ")");
                    });
                });
            @endif
            this.on("removedfile", function (file) {
                $.post({
                    url: "{{ route('album.images.destroy')}}",
                    data: {
                        _token: token,
                        id: file.customName,
                        tour_temp_id: $('#tour_temp_id').val()
                    },
                    dataType: 'json',
                    success: function (data) {
                        total_photos_counter--;
                        $("#counter").text("# " + total_photos_counter);
                    }
                });
            });
        },
        success: function (file, done) {
            total_photos_counter++;
            $("#counter").text("#" + total_photos_counter);
            file["customName"] = name;
        }
    });


</script>
@endsection