@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('website.popup.store')}}" name="popup_frm" id="popup_frm" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($popup->id) ? $popup->id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Title</label>
                                        <input type="text" class="md-input" name="name" value="{{ isset($popup->name) ? $popup->name : old('name') }}" />
                                        @if($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Link</label>
                                        <input type="text" class="md-input" name="redirect_link" value="{{ isset($popup->redirect_link) ? $popup->redirect_link : old('redirect_link') }}" />
                                        @if($errors->has('redirect_link'))<small class="text-danger">{{ $errors->first('redirect_link') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-4 uk-width-1-1 p-r-50">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_start">Start Date</label>
                                        <input class="md-input" type="text" id="uk_dp_start" name="start_date" autocomplete="off" value="{{ isset($popup->start_date) ? date('d M Y', strtotime($popup->start_date)) : '' }}">
                                    </div>
                                </div>
                                <div class="uk-width-large-1-4 uk-width-medium-1-1 p-r-50 p-l-0">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_end">End Date</label>
                                        <input class="md-input" type="text" id="uk_dp_end" name="end_date" autocomplete="off" value="{{ isset($popup->end_date) ? date('d M Y', strtotime($popup->end_date)) : '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6">
                                        <h3 class="heading_a uk-margin-small-bottom">Image</h3>
                                        <input type="file" name="image" id="input-file-a" class="dropify" data-default-file="{{  isset($popup->image) ? \App\Helpers\S3url::s3_get_image($popup->image) : '' }}" />
                                        @if($errors->has('image'))<small class="text-danger">{{ $errors->first('image') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($popup->status) && $popup->status == 1) ? 'checked' : ''  }} {{ (!isset($popup)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($popup->status) && $popup->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('website.popup') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_advanced.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>

<script type="text/javascript">
$(function () {
// It has the name attribute "popup_frm"
    $("form#popup_frm").validate({
// Specify validation rules
        rules: {
            name: "required",
            redirect_link: "required",
            image: {
                accept: "jpg,png,jpeg,gif"
            },
            status: "required",
        },
        // Specify validation error messages
        messages: {
            name: "Please enter  name",
            redirect_link: "Please enter redirect url",
            image: {
                accept: "Only image type jpg/png/jpeg/gif is allowed"
            },
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    @if (isset($popup) == false)
    var settings = $("form#popup_frm").validate().settings;
    $.extend(true, settings, {
        rules: {
            image: {
                required: true,
//                accept: "jpg,png,jpeg,gif"
            }
        },
        messages: {
            image: {
                required: "Please upload certificate image",
//                accept: "Only image type jpg/png/jpeg/gif is allowed"
            }
        }
    });
    @endif

});

</script>
@endsection
