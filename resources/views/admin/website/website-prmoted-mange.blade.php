@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('website.hotel.promoted.store') }}" name="hotelpromoted_frm" id="hotelpromoted_frm" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($hotelpromote->id) ? $hotelpromote->id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="hotel_id" name="hotel_id" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Hotel...">
                                            <option value="">Choose Hotel</option>
                                            <?php
                                            if (count($hotels) > 0) {
                                                foreach ($hotels as $hotel) {
                                                    ?>
                                                    <option value="{{ $hotel->id }}" {{ (isset($hotelpromote->hotel_id) && $hotelpromote->hotel_id == $hotel->id) ? 'selected' : '' }} >{{ $hotel->name }}</option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        @if($errors->has('hotel_id'))<small class="text-danger">{{ $errors->first('hotel_id') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Title</label>
                                        <input type="text" class="md-input" name="name" value="{{ isset($hotelpromote->name) ? $hotelpromote->name : '' }}" />
                                        @if($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Sequence</label>
                                        <input type="text" class="md-input" name="sequence" value="{{ isset($hotelpromote->sequence) ? $hotelpromote->sequence : '' }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Link</label>
                                        <input type="text" class="md-input" name="link" value="{{ isset($hotelpromote->link) ? $hotelpromote->link : '' }}" />
                                        @if($errors->has('link'))<small class="text-danger">{{ $errors->first('link') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-4 uk-width-1-1 p-r-50">
                                    <div class="uk-input-group">                        
                                        <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_start">Start Date</label>
                                        <input class="md-input" type="text" id="uk_dp_start" name="start_date" autocomplete="off" value="{{ isset($hotelpromote->start_date) ? date('d M Y', strtotime($hotelpromote->start_date)) : '' }}">
                                    </div>
                                </div>
                                <div class="uk-width-large-1-4 uk-width-medium-1-1 p-r-50 p-l-0">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_end">End Date</label>
                                        <input class="md-input" type="text" id="uk_dp_end" name="end_date" autocomplete="off" value="{{ isset($hotelpromote->end_date) ? date('d M Y', strtotime($hotelpromote->end_date)) : '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6">
                                        <h3 class="heading_a uk-margin-small-bottom">Logo</h3>
                                        <input type="file" id="input-file-a" class="dropify" name="image" data-default-file="{{  isset($hotelpromote->image) ? \App\Helpers\S3url::s3_get_image($hotelpromote->image) : '' }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($hotelpromote->status) && $hotelpromote->status == 1) ? 'checked' : ''  }} {{ (!isset($hotelpromote)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($hotelpromote->status) && $hotelpromote->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('website.hotel.promoted') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_advanced.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
// It has the name attribute "currency_frm"
    $("form#hotelpromoted_frm").validate({
// Specify validation rules
        rules: {
            hotel_id: "required",
            name: "required",
            link: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            hotel_id: "Please select hotel",
            name: "Please enter name",
            link: "Please enter url",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

});

</script>
@endsection