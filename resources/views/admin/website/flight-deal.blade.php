@extends('layouts.theme')

@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home')}}"><i class="material-icons">home</i></a></li>
            <li><span>Hotel Flight Deal</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('website.flight.deal.add') }}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th style="width: 20%;">Title</th>
                        <th>Flight Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th class="hidden-phone" style="width: 10%;">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>Title <?php echo $i; ?></td>
                            <td>Flight Name <?php echo $i; ?></td>
                            <td><?php echo $i; ?>/Jun/2018</td>
                            <td><?php echo $i; ?>/Jun/2018</td>
                            <td ><a class="" data-uk-modal="{target:'#changestatus'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a></td>
                            <td>
                                <a href="{{ route('website.flight.deal.edit', '1') }}" title="Edit Flight Deal" class=""><i class="md-icon material-icons">edit</i></a> 
                                <a href="{{ route('website.flight.deal.delete', '1') }}" onclick="return confirm('Are you sure to delete?');" title="Delete Flight Deal" class=""><i class="md-icon material-icons">delete</i></a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="changestatus" class="uk-modal" >
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Status</h3>
        </div>
        <div class="">
            <div class="control-group">
                <label class="control-label">Status</label>
                <div class="controls">
                    <span class="icheck-inline">
                        <input type="radio" name="status" id="Active" data-md-icheck />
                        <label for="Active" class="inline-label">Active</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="status" id="InActive" data-md-icheck />
                        <label for="InActive" class="inline-label">InActive</label>
                    </span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Remark</label>
                <div class="controls">
                    <textarea class="md-input no_autosize" rows="2"></textarea>
                </div>
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button"  class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Submit</button> 
        </div>
    </div>
</div>
@endsection

