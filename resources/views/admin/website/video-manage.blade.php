@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection


@section('content')

<div id="page_content">

    <div id="top_bar">
       @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('video.category.store')}}" name="category_frm" id="category_frm" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($album_category->catId) ? $album_category->catId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Category Name</label>
                                        <input type="text" class="md-input" name="title" value="{{ isset($album_category->title) ? $album_category->title : old('title') }}" />
                                        @if($errors->has('title'))<small class="text-danger">{{ $errors->first('title') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6">
                                        <h3 class="heading_a uk-margin-small-bottom">Image</h3>
                                        <input type="file" name="image" id="input-file-a" class="dropify" data-default-file="{{  isset($album_category->image) ?  env('APP_PUBLIC_URL').'uploads/video_category/'.$album_category->image : '' }}" />
                                    </div>
                                </div>
                                @if($errors->has('image'))<small class="text-danger">{{ $errors->first('image') }}</small>@endif
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($album_category->status) && $album_category->status == 1) ? 'checked' : ''  }} {{ (!isset($album_category)) ? 'checked' : '' }} />
                                                <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($album_category->status) && $album_category->status == 0) ? 'checked' : ''  }} />
                                                <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('video.category.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>

<script type="text/javascript">
$(function () {
    // It has the name attribute "banner_frm"
    $("form#category_frm").validate({
        // Specify validation rules
        rules: {
            title: "required",
            image: {
                accept: "jpg,png,jpeg,gif"
            },
            status: "required",
        },
        // Specify validation error messages
        messages: {
            title: "Please enter video category title",
            image: {
                accept: "Only image type jpg/png/jpeg/gif is allowed"
            },
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    @if (isset($album_category) == false)
    var settings = $("form#category_frm").validate().settings;
    $.extend(true, settings, {
        rules: {
            image: {
                required: true,
//                accept: "jpg,png,jpeg,gif"
            }
        },
        messages: {
            image: {
                required: "Please upload video category image",
//                accept: "Only image type jpg/png/jpeg/gif is allowed"
            }
        }
    });
    @endif
});
</script>
@endsection

