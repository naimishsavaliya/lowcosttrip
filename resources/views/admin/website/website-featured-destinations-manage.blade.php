@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('website.feature.destination') }}">Featured Destination</i></a></li>
            <li><span>Add / Edit Featured Destination</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ env('ADMIN_URL')}}website/featured-destinations" class="" method="get">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Name</label>
                                        <input type="text" class="md-input" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="select_demo_3" class="md-input">
                                            <option value="" selected>Choose Sector</option>
                                            <option value="1">Sector 1</option>
                                            <option value="2">Sector 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="select_demo_3" class="md-input">
                                            <option value="" selected>Choose Destination</option>
                                            <option value="1">Destination 1</option>
                                            <option value="1">Destination 2</option>
                                            <option value="1">Destination 3</option>
                                            <option value="1">Destination 4</option>
                                            <option value="1">Destination 5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="settings_cache_type" id="settings_cache_file" data-md-icheck checked />
                                                <label for="settings_cache_file" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="settings_cache_type" id="settings_cache_mysql" data-md-icheck />
                                                <label for="settings_cache_mysql" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('website.feature.destination') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                </div>
                        </div>-->

        </div>
    </div>

</div>

@endsection
