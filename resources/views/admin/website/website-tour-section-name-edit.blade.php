@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>
    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('website.tour.section.name.update') }}" name="hometourselection_frm_edit" id="hometourselection_frm_edit" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id" value="{{ isset($hometoursection_data->id) ? $hometoursection_data->id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Primary Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="primary_name" id="primary_name" value="{{ isset($hometoursection_data->primary_name) ? $hometoursection_data->primary_name : '' }}" readonly/>
                                        <small class="text-danger">{{ $errors->first('primary_name') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Current Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="current_name" id="current_name" value="{{ isset($hometoursection_data->current_name) ? $hometoursection_data->current_name : '' }}"/>
                                        <small class="text-danger">{{ $errors->first('current_name') }}</small>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Url <span class="required-lbl">*</span></label>
                                        <input type="url" class="md-input" name="url" id="url" value="{{ isset($hometoursection_data->url) ? $hometoursection_data->url : '#' }}" />
                                        <small class="text-danger">{{ $errors->first('url') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="status_1" data-md-icheck {{ (isset($hometoursection_data->status) && $hometoursection_data->status == 1) ? 'checked' : ''  }}  />
                                                       <label for="status_1" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="status_0" data-md-icheck {{ (isset($hometoursection_data->status) && $hometoursection_data->status == 0) ? 'checked' : ''  }} />
                                                       <label for="status_0" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        <small class="text-danger">{{ $errors->first('status') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{route('website.tour.section.name.list')}}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
    $("form#hometourselection_frm_edit").validate({
        rules: {
            primary_name: "required",
            current_name: "required",
            status: "required",
            url: {
                required: true,
                url: true
            }
        },
        messages: {
            primary_name: "Please enter primary name",
            current_name: "Please enter current name",
            status: "Please select status"
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});
</script>
@endsection