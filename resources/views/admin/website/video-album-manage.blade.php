@extends('layouts.theme')
@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('video.album.store')}}" name="video_album_frm" id="video_album_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($album_video->vidId) ? $album_video->vidId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Title</label>
                                        <input type="text" class="md-input" name="title" value="{{ isset($album_video->title) ? $album_video->title : '' }}" />
                                        @if($errors->has('title'))<small class="text-danger">{{ $errors->first('title') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Video Url</label>
                                        <input type="text" class="md-input" name="video_url" value="{{ isset($album_video->video_url) ? $album_video->video_url : '' }}" />
                                        @if($errors->has('video_url'))<small class="text-danger">{{ $errors->first('video_url') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <h3 class="heading_a"><span class="sub-heading">Album Category</span></h3>
                                <div class="uk-grid m-t-10 md-input-wrapper" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="category_ids" name="category_ids[]" class="uk-width-1-1" multiple data-md-select2 multiple="">
                                            @if(count($video_category) > 0)
                                            @php
                                                $cat_ids_arr = array();
                                                if(isset($album_video->cat_ids)){
                                                    $cat_ids_arr = explode(',',$album_video->cat_ids);
                                                }
                                            @endphp
                                            @foreach($video_category as $vid_cat)
                                            <option value="{{ $vid_cat->catId }}" {{ (isset($cat_ids_arr) && in_array($vid_cat->catId, $cat_ids_arr) ? "selected" : "") }}>{{ $vid_cat->title }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('category_ids'))<small class="text-danger">{{ $errors->first('category_ids') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                            <label for="uk_dp_1">Select date</label>
                                            <input class="md-input" type="text" name="album_date" id="uk_dp_1" value="{{ isset($album_video->album_date) ? date('d F Y', strtotime($album_video->album_date)) : old('album_date') }}" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Customer Name</label>
                                        <input type="text" class="md-input" name="customer_name" value="{{ isset($album_video->customer_name) ? $album_video->customer_name : '' }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($album_video->status) && $album_video->status == 1) ? 'checked' : ''  }} {{ (!isset($album_video)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($album_video->status) && $album_video->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('video.album.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>


@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>

<script type="text/javascript">
$(function () {
    // It has the name attribute "banner_frm"
    $("form#video_album_frm").validate({
        // Specify validation rules
        rules: {
            title: "required",
            video_url: "required",
            "category_ids[]": "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            title: "Please enter title",
            video_url: "Please enter video url",
            "category_ids[]": "Please select category",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else if (element.is('select')) {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

});
</script>
@endsection

