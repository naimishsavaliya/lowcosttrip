@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        <div class="uk-navbar-flip p-t-8">
            <?php /*
            <a href="{{ env('ADMIN_URL')}}website/homebanner-add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
            */ ?>
        </div>
    </div>
    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')
            <table class="uk-table" id="hometoursection_tbl">
                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Primary Name</th>
                        <th>Current Name</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="changestatus" class="uk-modal" >
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Status</h3>
        </div>
        <div class="">
            <div class="control-group">
                <input type="hidden" value="" id="change-status-row-id" />
                <label class="control-label">Status</label>
                <div class="controls">
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="1" id="status_dialog_active" data-md-icheck />
                        <label for="status_dialog_active" class="inline-label">Active</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="0" id="status_dialog_inactive" data-md-icheck />
                        <label for="status_dialog_inactive" class="inline-label">InActive</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close" id="status-dlg-btn">Submit</button> 
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function () {

        var hometoursectionTable = $('#hometoursection_tbl').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            searching: true,
            order: [[ 0, "desc" ]],
            "ajax": {
                "url": "{{ route('website.tour.section.name.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof hometoursectionTable != 'undefined' && hometoursectionTable.hasOwnProperty('settings')) {
                        hometoursectionTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {data: "id", name: "id"},
                {data: "primary_name", name: "primary_name"},
                {data: "current_name", name: "current_name"},
                {data: "status", name: "status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.id + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a>';
                        } else {
                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.id + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-danger">InActive</span></a>';
                        }
                    },
                },
                {data: "action", 'sortable': false, "render": function (data, type, row) {
                        var edit_url = "{{ route('website.tour.section.name.edit', ':id') }}";
                        edit_url = edit_url.replace(':id', row.id);
                        return '<a href="' + edit_url + '" title="Edit Home Page Section Name" class=""><i class="md-icon material-icons">&#xE254;</i></a>';
                    },
                },
            ]
        });

        $(hometoursectionTable.table().container()).on('keyup', 'tfoot input', function () {
            hometoursectionTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(hometoursectionTable.table().container()).on('change', 'tfoot select', function () {
            hometoursectionTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(document.body).on('click', '.change-status-btn', function () {
            var $obj = $(this);
            var row_id = $obj.attr('row-id');
            var status = $obj.attr('data-id');
            $('input#change-status-row-id').val(row_id);
            if (status == "1") {
                $('#status_dialog_inactive').removeAttr('checked');
                $('#status_dialog_active').iCheck('check');
            } else {
                $('#status_dialog_active').removeAttr('checked');
                $('#status_dialog_inactive').iCheck('check');
            }
        });

        $(document.body).on('click', '#status-dlg-btn', function () {
            var $obj = $(this);
            var row_id = $('#change-status-row-id').val();
            var status = $('input.user-status-change:checked').val();
            $.ajax({
                type: 'get',
                url: '{{ route("change_status")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table": "home_tour_sections",
                    "id": row_id,
                    "column_name": "id",
                    "status": status
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        hometoursectionTable.draw();
                    }
                    showNotify(data.msg_status, data.message);
                }
            });
        });

    });
</script>
@endsection