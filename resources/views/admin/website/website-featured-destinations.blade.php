@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Featured Destinations</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}website/featured-destinations-add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Destination Name</th>
                        <th>Sector Name</th>
                        <th>Category</th>
                        <th class="hidden-phone">Sort Order</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
<!--                <tfoot>
                    <tr>
                        <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Sector Name" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Destination Name" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Category" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Sort Order" name="" colPos="1"></td>
                        <td ><select class="chosen" data-placeholder="Choose a Status" tabindex="1" colPos="6">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="2">InActive</option>
                            </select></td>
                        <td></td>
                    </tr>
                </tfoot>-->
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>Destination <?php echo $i; ?></td>
                            <td>Sector <?php echo $i; ?></td>
                            <td>Domestic</td>
                            <td><?php echo $i; ?></td>
                            <td ><span class="uk-badge uk-badge-success">Active</span></td>
                            <td >
                                <a href="{{ env('ADMIN_URL')}}website/featured-destinations-edit/1" title="Edit Destination" class=""><i class="md-icon material-icons">&#xE254;</i></a> 
                                <a href="{{ env('ADMIN_URL')}}website/featured-destinations-delete/1" onclick="return confirm('Are you sure to delete?');" title="Delete Destination" class=""><i class="md-icon material-icons">delete</i></a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection