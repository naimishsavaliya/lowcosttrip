@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('website.banner.store') }}" name="banner_frm" id="banner_frm" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($home_banner->banId) ? $home_banner->banId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>title <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="name" value="{{ isset($home_banner->title) ? $home_banner->title : old('title') }}" />
                                        @if($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Tooltip</label>
                                        <input type="text" class="md-input" name="tooltip" value="{{ isset($home_banner->tooltip) ? $home_banner->tooltip : old('tooltip') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="banner_option" class="md-input" name="banner_option">
                                            <option value="">Choose Banner Option</option>
                                            @if(isset($banner_option))
                                            @foreach($banner_option as $t => $opt)
                                            <option value="{{ $t }}" {{ (isset($home_banner->banner_opt) && $home_banner->banner_opt == $t) ? 'selected' : '' }}>{{ $opt }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('banner_option'))<small class="text-danger">{{ $errors->first('banner_option') }}</small>@endif
                                        <div class="error-msg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="banner_category" class="md-input" name="banner_category">
                                            <option value="">Choose Category</option>
                                            @if(isset($banner_category))
                                            @foreach($banner_category as $ct => $cat_opt)
                                            <option value="{{ $ct }}" {{ (isset($home_banner->banner_cat) && $home_banner->banner_cat == $ct) ? 'selected' : '' }}>{{ $cat_opt }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('banner_category'))<small class="text-danger">{{ $errors->first('banner_category') }}</small>@endif
                                        <div class="error-msg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Redirect To</label>
                                        <input type="text" class="md-input" name="redirect_to" value="{{ isset($home_banner->redirect_to) ? $home_banner->redirect_to : old('redirect_to') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Sort Order</label>
                                        <input type="text" class="md-input" name="sort_ord" value="{{ isset($home_banner->sort_ord) ? $home_banner->sort_ord : old('sort_ord') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Description</label>
                                        <textarea cols="30" rows="4" class="md-input" name="description">{{ isset($home_banner->description) ? $home_banner->description : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6" id="img_opt_1" style="display: {{ (isset($home_banner->banner_opt) && $home_banner->banner_opt >= 1) ? 'block' : 'none' }};">
                                        <h3 class="heading_a uk-margin-small-bottom">Banner Image 1</h3>
                                        <input type="file" name="image_1" id="image_1" class="dropify" data-default-file="{{  isset($home_banner->image_1) ?  env('APP_PUBLIC_URL').'uploads/home_banner/'.$home_banner->image_1 : '' }}" />
                                        @if($errors->has('image'))<small class="text-danger">{{ $errors->first('image') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-2-6" id="img_opt_2" style="display: {{ (isset($home_banner->banner_opt) && $home_banner->banner_opt >= 2) ? 'block' : 'none' }};">
                                        <h3 class="heading_a uk-margin-small-bottom">Banner Image 2</h3>
                                        <input type="file" name="image_2" id="image_2" class="dropify" data-default-file="{{  isset($home_banner->image_2) ?  env('APP_PUBLIC_URL').'uploads/home_banner/'.$home_banner->image_2 : '' }}" />
                                        @if($errors->has('image'))<small class="text-danger">{{ $errors->first('image') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-2-6" id="img_opt_3" style="display: {{ (isset($home_banner->banner_opt) && $home_banner->banner_opt >= 3) ? 'block' : 'none' }};">
                                        <h3 class="heading_a uk-margin-small-bottom">Banner Image 3</h3>
                                        <input type="file" name="image_3" id="image_3" class="dropify" data-default-file="{{  isset($home_banner->image_3) ?  env('APP_PUBLIC_URL').'uploads/home_banner/'.$home_banner->image_3 : '' }}" />
                                        @if($errors->has('image'))<small class="text-danger">{{ $errors->first('image') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($home_banner->status) && $home_banner->status == 1) ? 'checked' : ''  }} {{ (!isset($home_banner)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($home_banner->status) && $home_banner->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('website.banner.list') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--dropify--> 
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--form file input functions activity-->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>

<script type="text/javascript">
$(function () {
// It has the name attribute "banner_frm"
    $("form#banner_frm").validate({
// Specify validation rules
        rules: {
            name: "required",
            banner_option: "required",
            banner_category: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            name: "Please enter certificate name",
            banner_option: "Please select banner option",
            banner_category: "Please select banner category",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else if (element.is('select')) {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    $(document.body).on('change', '#banner_option', function () {
        var $obj = $(this);
        var opt_val = $obj.val();
        if (opt_val == 1) {
            $('#img_opt_1').show();
            $('#img_opt_2').hide();
            $('#img_opt_3').hide();
        }
        if (opt_val == 2) {
            $('#img_opt_1').show();
            $('#img_opt_2').show();
            $('#img_opt_3').hide();
        }
        if (opt_val == 3) {
            $('#img_opt_1').show();
            $('#img_opt_2').show();
            $('#img_opt_3').show();
        }
    });
}
);

</script>
@endsection