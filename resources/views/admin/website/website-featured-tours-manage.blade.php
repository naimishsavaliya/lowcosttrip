@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('website.featured.tour.store') }}" name="featured_tour" id="featured_tour" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($featured_tour->id) ? $featured_tour->id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="type_id" name="type_id" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Tour Type...">
                                            <option value="">Select Tour Type</option>
                                            <?php
                                            if (count($tour_type) > 0) {
                                                foreach ($tour_type as $type) {
                                                    ?>
                                                    <option value="{{ $type->typeId }}" {{ (isset($featured_tour->type_id) && $featured_tour->type_id == $type->typeId) ? "selected" : "" }}>{{ $type->name }}</option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        @if($errors->has('type_id'))<small class="text-danger">{{ $errors->first('type_id') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="tour_id" name="tour_id" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Tour...">
                                            <option value="">Select Tour</option>
                                            <?php
                                            if (count($tours) > 0) {
                                                foreach ($tours as $tour) {
                                                    ?>
                                                    <option value="{{ $tour->id }}" {{ (isset($featured_tour->tour_id) && $featured_tour->tour_id == $tour->id) ? "selected" : "" }}>{{ $tour->tour_name }}</option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        @if($errors->has('tour_id'))<small class="text-danger">{{ $errors->first('tour_id') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="status_1" data-md-icheck {{ (isset($featured_tour->status) && $featured_tour->status == 1) ? 'checked' : ''  }} {{ (!isset($featured_tour)) ? 'checked' : '' }} />
                                                       <label for="status_1" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="status_0" data-md-icheck {{ (isset($featured_tour->status) && $featured_tour->status == 0) ? 'checked' : ''  }} />
                                                       <label for="status_0" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('website.featured.tour') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
// It has the name attribute "lead_frm"
    $("form#featured_tour").validate({
// Specify validation rules
        rules: {
            type_id: "required",
            tour_id: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            type_id: "Please select tour type",
            tour_id: "Please select tour",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});

</script>
@endsection

