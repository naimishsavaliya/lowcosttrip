@extends('layouts.theme')
@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('tourinfo.save') }}" name="tour-info-form" id="tour-info-form" class="" method="post"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($tourinfo->id) ? $tourinfo->id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Title</label>
                                        <input type="text" class="md-input" name="title" value="{{ isset($tourinfo->title) ? $tourinfo->title : '' }}" />
                                        @if($errors->has('title'))<small class="text-danger">{{ $errors->first('title') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Inclusions</label>
                                        <textarea cols="30" rows="4" class="md-input" name="inclusions">{{ isset($tourinfo->inclusions) ? $tourinfo->inclusions : '' }}</textarea>
                                        @if($errors->has('inclusions'))<small class="text-danger">{{ $errors->first('inclusions') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Exclusions</label>
                                        <textarea cols="30" rows="4" class="md-input" name="exclusions">{{ isset($tourinfo->exclusions) ? $tourinfo->exclusions : '' }}</textarea>
                                        @if($errors->has('exclusions'))<small class="text-danger">{{ $errors->first('exclusions') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Terms Conditions</label>
                                        <textarea cols="30" rows="4" class="md-input" name="terms_conditions">{{ isset($tourinfo->terms_conditions) ? $tourinfo->terms_conditions : '' }}</textarea>
                                        @if($errors->has('terms_conditions'))<small class="text-danger">{{ $errors->first('terms_conditions') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($tourinfo->status) && $tourinfo->status == 1) ? 'checked' : ''  }} {{ (!isset($tourinfo)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($tourinfo->status) && $tourinfo->status == 0) ? 'checked' : '' }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                    </div>
                                    @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('sector.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection


@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>
<script type="text/javascript">
$(function () {
    $("form#tour-info-form").validate({
        rules: {
            title: "required",
            inclusions: "required",
            exclusions: "required",
            terms_conditions: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            title: "Please enter title",
            inclusions: "Please enter inclusions",
            exclusions: "Please enter exclusions",
            terms_conditions: "Please enter terms conditions",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});


</script>

@endsection

