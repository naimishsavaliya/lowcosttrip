@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><span>Add / Edit Payment</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ route('lead.payment') }}" class="" method="get">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2 uk-row-first">
                            <div class="md-input-wrapper"><label>Lead ID</label>
                                <input type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="md-input-wrapper"><label>Amount</label>
                                <input type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <select id="select_demo_5" class="md-input">
                                <option value="" selected>Payment Mode</option>
                                <option value="1">Cash</option>
                                <option value="2">Check</option>
                                <option value="2">IMPS</option>
                                <option value="2">RTGS</option>
                            </select>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="uk_dp_1">Payment Date & Time</label>
                            <div class="cler clearfix"> </div> <br/>
                            <input id="kUI_datetimepicker_basic"  class="uk-width-medium-1-1"/>
                        </div>
                        <div class="uk-width-medium-1-1">
                            <div class="md-input-wrapper"><label>Description</label>
                                <textarea  rows="1" class="md-input autosized"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('lead.payment') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

@endsection
@section('javascript')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/kendo-ui/styles/kendo.common-material.min.css"/>
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/kendo-ui/styles/kendo.material.min.css" id="kendoCSS"/>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/kendoui_custom.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/kendoui.min.js"></script>
@endsection

