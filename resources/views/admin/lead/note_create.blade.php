@extends('layouts.theme')
@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ route('lead.notes.store') }}" name="lead_note_frm" id="lead_note_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="lead_id" value="{{ Request::route('lead_id') }}" />
                    <input type="hidden" name="id" value="{{ isset($lead_note->notId) ? $lead_note->notId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-50-10">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label>Title</label>
                                        <input type="text" class="md-input" name="title" value="{{ isset($lead_note->title) ? $lead_note->title : old('title') }}" />
                                        @if($errors->has('title'))<small class="text-danger">{{ $errors->first('title') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label>Description</label>
                                        <textarea class="md-input autosized" row="2" name="description">{{ isset($lead_note->description) ? $lead_note->description : old('description') }}</textarea>
                                        @if($errors->has('description'))<small class="text-danger">{{ $errors->first('description') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-1">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label for="uk_dp_1">Follow Up Date</label>
                                        <div class="cler clearfix"> </div> <br/>
                                        <input id="followup_date"  class="uk-width-medium-1-1" name="followup_date" value="{{ isset($lead_note->followup_date) ? date('d F Y H:i a', strtotime($lead_note->followup_date)) : '' }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($lead_note->status) && $lead_note->status == 1) ? 'checked' : ''  }} {{ (!isset($lead_note)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($lead_note->status) && $lead_note->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('lead.notes.list') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                </div>
                        </div>-->

        </div>
    </div>

</div>

@endsection

@section('javascript')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/kendo-ui/styles/kendo.common-material.min.css"/>
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/kendo-ui/styles/kendo.material.min.css" id="kendoCSS"/>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/kendoui_custom.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/kendoui.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
// It has the name attribute "banner_frm"
    $("#followup_date").kendoDateTimePicker({
        format: "dd MMMM yyyy hh:mm tt"
    });
    $("#followup_date").val(kendo.toString(kendo.parseDate(new Date()),'dd MMMM yyyy hh:mm tt')).trigger('change');
    
    $("form#lead_note_frm").validate({
// Specify validation rules
        rules: {
            title: "required",
            description: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            title: "Please enter note title",
            description: "Please enter description",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

   
    
});

</script>
@endsection

