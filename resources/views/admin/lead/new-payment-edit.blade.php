@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>
    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('lead_payment.update') }}" name="payment_frm_edit" id="payment_frm_edit" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id" value="{{ isset($payment_info->id) ? $payment_info->id : '' }}" />
                    <input type="hidden" name="type" id="type" value="{{ isset($payment_info->type) ? $payment_info->type : '' }}" />
                    <input type="hidden" name="lead_id" id="lead_id" value="{{ isset($payment_info->lead_id) ? $payment_info->lead_id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <select class="md-input" name="type_of_payment" id="type_of_payment">
                                            <option value="">Choose Type Of Payment</option>
                                            @if(isset($type_of_payment))
                                            @foreach($type_of_payment as $type_of_payment_key=>$type_of_payment_val)
                                            <option value="{{ $type_of_payment_key }}" {{ (isset($payment_info->type_of_payment) && $payment_info->type_of_payment == $type_of_payment_key) ? 'selected' : '' }}>{{ $type_of_payment_val }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <small class="text-danger">{{ $errors->first('type_of_payment') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <select class="md-input" name="payment_type" id="payment_type">
                                            <option value="">Choose Payment Type</option>
                                            @if(isset($paymentType))
                                            @foreach($paymentType as $paymentType_key=>$paymentType_val)
                                            <option value="{{ $paymentType_key }}" {{ (isset($payment_info->payment_type) && $payment_info->payment_type == $paymentType_key) ? 'selected' : '' }}>{{ $paymentType_val }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <small class="text-danger">{{ $errors->first('payment_type') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <select class="md-input" name="payment_mode" id="payment_mode">
                                            <option value="">Choose Payment Detail</option>
                                            @if(isset($paymentMode))
                                            @foreach($paymentMode as $paymentMode_key=>$paymentMode_val)
                                            <option value="{{ $paymentMode_key }}" {{ (isset($payment_info->payment_mode) && $payment_info->payment_mode == $paymentMode_key) ? 'selected' : '' }}>{{ $paymentMode_val }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <small class="text-danger">{{ $errors->first('payment_mode') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Amount <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="amount" id="amount" value="{{ isset($payment_info->paid_amount) ? $payment_info->paid_amount : '' }}" />
                                        <small class="text-danger">{{ $errors->first('amount') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Note <span class="required-lbl">*</span></label>
                                        <textarea cols="30" rows="4" class="md-input" name="payment_note" id="payment_note">{{ isset($payment_info->payment_note) ? $payment_info->payment_note : '' }}</textarea>
                                        <small class="text-danger">{{ $errors->first('payment_note') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Receipt Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="receipt_name" id="receipt_name" value="{{ isset($payment_info->receipt_name) ? $payment_info->receipt_name : '' }}"/>
                                        <small class="text-danger">{{ $errors->first('receipt_name') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{route('lead.lead_detail', $payment_info->lead_id)}}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
    $(".onlynumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("form#payment_frm_edit").validate({
        rules: {
            type_of_payment: "required",
            payment_mode: "required",
            payment_type:"required",
            amount:"required",
            receipt_name: "required",
            payment_note: "required"
        },
        messages: {
            type_of_payment: "Please select type of payment.",
            payment_mode: "Please select payment detail.",
            payment_type:"Please select payment type.",
            amount:"Amount is required.",
            receipt_name: "Receipt name is required.",
            payment_note: "Note is required."
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});

</script>
@endsection