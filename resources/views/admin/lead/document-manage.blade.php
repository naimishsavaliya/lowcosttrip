@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection


@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('lead.document.store') }}" name="document_frm" id="document_frm" class="" method="post" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <input type="hidden" name="lead_id" value="{{ Request::route('lead_id') }}" />
                    <input type="hidden" name="id" value="{{ isset($document->docId) ? $document->docId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Title</label>
                                        <input type="text" class="md-input" name="name" value="{{ isset($document->name) ? $document->name : old('name') }}" />
                                        @if($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6">
                                        <h3 class="heading_a uk-margin-small-bottom">Document</h3>
                                        <input type="file" id="input-file-a" class="dropify" name="document" data-default-file="{{  isset($document->document) ?  env('APP_PUBLIC_URL').'uploads/lead_document/'.Request::route('lead_id').'/'.$document->document : '' }}" />
                                    </div>
                                </div>
                                @if($errors->has('document'))<small class="text-danger">{{ $errors->first('document') }}</small>@endif
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Description</label>
                                        <textarea cols="30" rows="4" class="md-input" name="description">{{ isset($document->description) ? $document->description : old('description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($document->status) && $document->status == 1) ? 'checked' : ''  }} {{ (!isset($document)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($document->status) && $document->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('lead.lead_detail',Request::route('lead_id').'#documents') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>

<script type="text/javascript">
$(function () {
// It has the name attribute "currency_frm"
    $("form#document_frm").validate({
// Specify validation rules
        rules: {
            name: "required",
            document: {
                accept: "jpg,png,jpeg,gif"
            },
            status: "required",
        },
        // Specify validation error messages
        messages: {
            name: "Please enter document name",
            document: {
                accept: "Only document type jpg/png/jpeg/gif is allowed"
            },
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    @if (isset($document) == false)
    var settings = $("form#document_frm").validate().settings;
    $.extend(true, settings, {
        rules: {
            document: {
                required: true,
//                accept: "jpg,png,jpeg,gif"
            }
        },
        messages: {
            document: {
                required: "Please upload document",
//                accept: "Only image type jpg/png/jpeg/gif is allowed"
            }
        }
    });
    @endif

});

</script>
@endsection
