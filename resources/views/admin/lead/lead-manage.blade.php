@extends('layouts.theme')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
    var cityData = <?= $cities_for_holidays ?>;
</script>
<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner" >
            @include('admin.lead.lead_form')
        </div>
    </div>

</div>

@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/get_regions.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- <script type="text/javascript">
$(function () {
    $("form#lead_frm").validate({
        rules: {
            lead_name: "required",
            email_id: "required",
            phone_no: "required",
            intrested_in: "required",
            no_adults: {
                required : true,
                number: true
            },
        },
        messages: {
            lead_name: "Please enter lead name",
            email_id: "Please enter email address",
            phone_no: "Please enter phone number",
            intrested_in: "Please select interested in",
            no_adults:{
                required : "Please enter no. of adults",
                number: "Pleae enter only number"
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});

</script> -->
<script type="text/javascript">
$(function () {
    $("form#lead_frm").validate({
        rules: {
            lead_name: "required",
            email_id: "required",
            phone_no: "required",
            intrested_in: "required"
        },
        messages: {
            lead_name: "Please enter lead name",
            email_id: "Please enter email address",
            phone_no: "Please enter phone number",
            intrested_in: "Please select interested in"
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".destination_add_btn").click(function () {
            var i = $(".destination_row").length;
            console.log(i);
            var html = '<tr class="destination_row">'
                    + '<td>'
                    + '<input type="text" name="destination['+i+'][city_name]" class="md-input autocomplete" placeholder="Search by city name">'
                    + '<input type="hidden" name="destination['+i+'][city_id]">'
                    + '</td>'
                    + '<td>'
                    + '<input type="number" name="destination['+i+'][number_of_nights]" class="md-input onlynumber" value="1" min="1" max="10">'
                    + '</td>'
                    + '<td>'
                    + '<a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus" data-field="quant[1]">delete</i></a>'
                    + '</td>'
                    + '</tr>';
            $(html).insertBefore(".add_btn_tr");
            $(".autocomplete").autocomplete({
                source: cityData,
                minLength: 3,
                focus: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(this).next().val(ui.item.value);
                }
            });
            $(".onlynumber").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
        $("body").on("click", ".destination_delete_btn", function () {
            $(this).closest("tr").remove();
        });
        $(".autocomplete").autocomplete({
            source: cityData,
            minLength: 3,
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(this).next().val(ui.item.value);
            }
        });
        $(".onlynumber").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });       
    });
    $(document).ready(function () {
        if($('#intrested_in').val() == '7'){
            $('.holidays-information').removeAttr("style");
        } 

        $(document).on('change', '#intrested_in', function() {
            if($(this).val() == '7'){
                $('.holidays-information').removeAttr("style");
            }else{
                $(".holidays-information").css("display", "none");
            }
        });

        $(document).on("click", ".add_tour_activites", function (event) {
            $.ajax({
                type: 'GET',
                url: "{{ route('lead.get-activiti-html') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    $('.append_new_row').before( data.html );
                }
            });
        });

        $(document).on("change", ".country_id_activity", function (event) {
            $th = $(this);
            $.ajax({
                type: 'POST',
                url: "{{ route('lead.get-activiti-by-city') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id" :  $th.val(),
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Please wait..</option>');
                },
                success: function (data, textStatus, jqXHR) {
                    $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Choose Activity</option>');

                    $.map(data.activities, function (item) {
                        $th.closest('tr').find('td:eq(1) .tour_activity').append('<option value="' + item.id + '">' + item.title + '</option>');
                    });
                }
            });
        });

        $( ".edit_country_id" ).each(function() {
            $th = $(this);
            $c_id = $th.attr("data-id");
            var activity_class = '.edit_tour_activity_'+$c_id;
            var activity_id = '.activity_id_'+$c_id;
            $.ajax({
                type: 'POST',
                url: "{{ route('lead.get-activiti-by-city') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id" :  $th.val(),
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $(activity_class).html('<option value="" selected>Please wait..</option>');
                },
                success: function (data, textStatus, jqXHR) {
                    $(activity_class).html('<option value="" selected>Choose Activity</option>');
                    $.map(data.activities, function (item) {
                        var selected = ''
                        if($(activity_id).val() == item.id){
                            selected = 'selected';
                        }
                        $(activity_class).append('<option '+selected+' value="' + item.id + '">' + item.title + '</option>');
                    });
                }
            });
        }); 
    }); 
</script>
@endsection