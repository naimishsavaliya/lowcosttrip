<tr>
    <td> 
	    <select id="country_id_activity" class="md-input country_id_activity" name="activiti_details['city'][]">
	        <option value="" selected>Choose Country</option>
	        @if(isset($countrys))
	        @foreach($countrys as $country)
	        <option value="{{ $country->country_id }}" {{ (isset($city->country_id) && $city->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
	        @endforeach
	        @endif
	    </select>
    </td>
    <td>
    	<select class="md-input tour_activity" name="activiti_details['activiti'][]">
	        <option value="" selected>Choose Activity</option>
	    </select>
    </td>
    <td>
        <a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus" data-field="quant[1]">delete</i></a>  
    </td>
</tr>
