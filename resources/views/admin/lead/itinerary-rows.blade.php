@if(isset($itinerarys) && count($itinerarys) > 0)
@foreach($itinerarys as $iti_key => $itninarary)
<div class="uk-width-medium-1-1 parsley-row form_section itinarary-row">
    <div class="uk-input-group uk-grid" style="width:100%">
        <div class="uk-width-medium-1-3 p-t-8">
            <div class="uk-form-row parsley-row">
                <label for="invoice_dp">Day {{ $itninarary->tour_day }}</label>
                <input type="text" name="itinerary[@{{$iti_key}}][day]" id="itinerary_day" class="md-input itinerary_day" value="{{ $itninarary->tour_day }}" required="">
            </div>
            <div class="uk-form-row parsley-row">
                <label for="inv_service">Title</label>
                <input type="text" name="itinerary[@{{$iti_key}}][title]" id="itinerary_title" class="md-input itinerary_title" value="{{ $itninarary->title }}" required="" />
            </div>
        </div>
        <div class="uk-width-medium-1-2 parsley-row">
            <label for="inv_service_rate">Description</label>
            <textarea cols="30" name="itinerary[@{{$iti_key}}][description]" id="itinerary_desciption" rows="4" class="md-input itinerary_desciption" required="">{{ $itninarary->description }}</textarea>
        </div>
        <div class="uk-width-medium-1-6 image-row">
            <img class="previewHolder" src="{{ env('APP_PUBLIC_URL')}}/uploads/itinerary/{{$itninarary->image}}" alt="Preview Holder" width="150px" height="150px" style="max-width: 69%;padding: 5px;"/>
            <div class="uk-form-file md-btn md-btn-primary">
                Select image
                <input type="file" name="itinerary[@{{$iti_key}}][image]" class="itinerary_image" id="form-file">
            </div>
            <span class="uk-input-group-addon" style="padding: 0;float: right;display: inline-block;">
                <a href="#" class="{{ ((count($itinerarys) - 1) == $iti_key) ? "btnSectionClone" : "btnSectionRemove" }} "><i class="material-icons md-24">{{ ((count($itinerarys) - 1) == $iti_key) ? "add_box" : "delete" }}</i></a>
            </span>
        </div>
    </div>
</div>
<hr class="form_hr width-100">
@endforeach
@endif
