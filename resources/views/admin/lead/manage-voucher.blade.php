@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
<link href="{{ env('APP_PUBLIC_URL')}}new-theme/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
<link href="{{ env('APP_PUBLIC_URL')}}new-theme/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
@endsection


@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('voucher.store') }}" name="voucher_frm" id="voucher_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($lead_package->id) ? $lead_package->id : '' }}" />
                    <input type="hidden" name="lead_id" value="{{ isset($lead->id) ? $lead->id : '' }}" />
                    <h3>Voucher Details:-</h3><hr>
                    <div class="uk-grid" data-uk-grid-margin>
                         <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                       <label>Guest Name</label>
                                        <input type="text" class="md-input" name="guest_name" value="{{ isset($lead_package->guest_name) ? $lead_package->guest_name : ((isset($lead->name) ) ? $lead->name :'') }}" />
                                        @if($errors->has('guest_name'))<small class="text-danger">{{ $errors->first('guest_name') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <?php /*
                                        <label>Package</label>
                                        <input type="text" class="md-input" name="package_id" value="{{ isset($lead_package->package_id) ? $lead_package->package_id : '' }}" />
                                        @if($errors->has('package_id'))<small class="text-danger">{{ $errors->first('package_id') }}</small>@endif */ ?>
                                        <select class="md-input" id="package_id" name="package_id" data-uk-tooltip="{pos:'bottom'}" title="Select Customer Type...">
                                            <option value="">Select Package Type</option>
                                            @if(isset($package_type))
                                            @foreach($package_type as $p_type)
                                            <option value="{{ $p_type->typeId }}" {{ (isset($lead_package->package_id) && $lead_package->package_id == $p_type->typeId) ? 'selected' : '' }}>{{ $p_type->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('package_id'))<small class="text-danger">{{ $errors->first('package_id') }}</small>@endif


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid" data-uk-grid-margin>
                         <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Number Of Guest</label>
                                        <input type="text" class="md-input" name="no_of_guests" value="{{ isset($lead_package->no_of_guests) ? $lead_package->no_of_guests : '' }}" />
                                        @if($errors->has('no_of_guests'))<small class="text-danger">{{ $errors->first('no_of_guests') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Meal Plan</label>
                                        <input type="text" class="md-input" name="meal_plan" value="{{ isset($lead_package->meal_plan) ? $lead_package->meal_plan : '' }}" />
                                        @if($errors->has('meal_plan'))<small class="text-danger">{{ $errors->first('meal_plan') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid" data-uk-grid-margin>
                         <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                       <label>Book Through</label>
                                        <input type="text" class="md-input" name="book_through" value="{{ isset($lead_package->book_through) ? $lead_package->book_through : '' }}" />
                                        @if($errors->has('book_through'))<small class="text-danger">{{ $errors->first('book_through') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Issueed By</label>
                                        <input type="text" class="md-input" name="issued_by" value="{{ isset($lead_package->issued_by) ? $lead_package->issued_by : '' }}" />
                                        @if($errors->has('issued_by'))<small class="text-danger">{{ $errors->first('issued_by') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid" data-uk-grid-margin>
                         <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                       <label>Voucher Number</label>
                                        <input type="text" class="md-input" name="voucher_number" value="{{ isset($lead_package->voucher_number) ? $lead_package->voucher_number : '' }}" />
                                        @if($errors->has('voucher_number'))<small class="text-danger">{{ $errors->first('voucher_number') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Issue Date</label>
                                        <input class="md-input" type="text" name="issue_date" value="{{ isset($lead_package->issue_date) ? date('d F Y', strtotime($lead_package->issue_date)) : '' }}"autocomplete="off"  data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                        @if($errors->has('issue_date'))<small class="text-danger">{{ $errors->first('issue_date') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <h3>Hotels:-</h3><hr>

                    <div class="uk-grid uk-margin-medium-top" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <div class="uk-width-1-1">

                                            <?php /* ?>
                                            <table class="uk-table table-border hotel-table">
                                                <thead>
                                                    <tr>
                                                        <td width="17%"><strong>Hotel Name</strong></td>
                                                        <td width="13%"><strong>Rooming details</strong></td>
                                                        <td width="17%"><strong>Location</strong></td>
                                                        <td width="13%"><strong>Check In</strong></td>
                                                        <td width="13%"><strong>Check Out</strong></td>
                                                        <td width="8%"><strong>Number Of Nights</strong></td>
                                                        <td width="5%"><strong>Action</strong></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if( isset($lead_package_meta) && count($lead_package_meta)>0)
                                                    @foreach($lead_package_meta as $key => $hotels)
                                                        <tr class="add_btn_tr">
                                                            <td>
                                                                <input type="text" class="md-input" placeholder="Hotel Name" value="{{$hotels->hotel_name}}" name="hotel_name[]">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input" placeholder="Rooming details" name="rooming_details[]">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input" placeholder="Location" value="{{$hotels->location}}" name="location[]">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input date-time-picker" placeholder="Check In" value="{{ date('d F Y H:i', strtotime($hotels->check_in)) }}" name="check_in[]" autocomplete="off">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input date-time-picker" placeholder="Check Out" value="{{ date('d F Y H:i', strtotime($hotels->check_out)) }}" name="check_out[]" autocomplete="off">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input" placeholder="Number Of Nights" value="{{$hotels->number_of_nights}}" name="number_of_nights[]">
                                                            </td>
                                                            <td>
                                                                <a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus">delete</i></a>

                                                                <a href="javaScript:void(0);" class="btnSectionClone destination_add_btn" data-type="plus"><i class="material-icons md-24">add_box</i></a> 
                                                            </td>
                                                    @endforeach
                                                    @else
                                                        <tr class="add_btn_tr">
                                                            <td>
                                                                <input type="text" class="md-input" placeholder="Hotel Name" name="hotel_name[]">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input" placeholder="Rooming details" name="rooming_details[]">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input" placeholder="Location" name="location[]">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input date-time-picker" placeholder="Check In" name="check_in[]" autocomplete="off" >
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input date-time-picker" placeholder="Check Out" name="check_out[]" autocomplete="off" >
                                                            </td>
                                                            <td>
                                                                <input type="text" class="md-input" placeholder="Number Of Nights" name="number_of_nights[]">
                                                            </td>
                                                            <td>
                                                                <a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus">delete</i></a>

                                                                <a href="javaScript:void(0);" class="btnSectionClone destination_add_btn" data-type="plus"><i class="material-icons md-24">add_box</i></a> 
                                                            </td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table> */ ?>

                                            @if( isset($lead_package_meta) && count($lead_package_meta)>0)
                                                    @foreach($lead_package_meta as $key => $hotels)
                                                    <div class="hotel-details">
                                                        <div class="uk-width-medium-1-1">
                                                           <div class="uk-input-group uk-grid" style="width:100%">

                                                              <div class="uk-width-medium-1-6 p-t-8">
                                                                 <div class="uk-form-row parsley-row">
                                                                    <div class="md-input-wrapper md-input-filled">
                                                                        <label for="invoice_dp">Hotel Name</label>
                                                                        <input type="text" class="md-input" value="{{$hotels->hotel_name}}"  name="hotel_name[]">
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                              <div class="uk-width-medium-1-6 p-t-8">
                                                                 <div class="uk-form-row parsley-row">
                                                                    <div class="md-input-wrapper md-input-filled">
                                                                        <label for="invoice_dp">Rooming details</label>
                                                                        <input type="text" class="md-input" value="{{$hotels->rooming_details}}" name="rooming_details[]">
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                              <div class="uk-width-medium-1-6 p-t-8">
                                                                 <div class="uk-form-row parsley-row">
                                                                    <div class="md-input-wrapper md-input-filled">
                                                                        <label for="invoice_dp">Location</label>
                                                                        <input type="text" class="md-input" value="{{$hotels->location}}" name="location[]">
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                              <div class="uk-width-medium-1-6 p-t-8">
                                                                 <div class="uk-form-row parsley-row">
                                                                    <div class="md-input-wrapper md-input-filled">
                                                                        <label for="invoice_dp">Check In</label>
                                                                        <input type="text" class="md-input date-time-picker" value="{{$hotels->check_in}}" name="check_in[]" autocomplete="off" >
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                              <div class="uk-width-medium-1-6 p-t-8">
                                                                 <div class="uk-form-row parsley-row">
                                                                    <div class="md-input-wrapper md-input-filled">
                                                                        <label for="invoice_dp">Check Out</label>
                                                                        <input type="text" class="md-input date-time-picker" value="{{$hotels->check_out}}" name="check_out[]" autocomplete="off" >
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                              <div class="uk-width-medium-1-6 p-t-8" style="float: left">
                                                                <div class="uk-width-medium-1-2" style="float: left">
                                                                     <div class="uk-form-row parsley-row">
                                                                        <div class="md-input-wrapper md-input-filled">
                                                                            <label for="invoice_dp">No. Of Nights</label>
                                                                            <input type="text" class="md-input" value="{{$hotels->number_of_nights}}" name="number_of_nights[]">

                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="uk-width-medium-1-2" style="float: left;margin-top: 12px; text-align: center;"> <div class="uk-form-row parsley-row">
                                                                        <a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus">delete</i></a>

                                                                        <a href="javaScript:void(0);" class="btnSectionClone destination_add_btn" data-type="plus"><i class="material-icons md-24">add_box</i></a> 
                                                                     </div>
                                                                  </div>
                                                              </div>
                                                           </div>
                                                           <div class="uk-clearfix"></div>

                                                           <div class="uk-input-group uk-grid" style="width:100%">
                                                                <div class="uk-width-medium-1-2 p-t-8">
                                                                    <label for="inv_service_rate">Inclusions</label>
                                                                    <textarea cols="30" name="inclusions[]" rows="2" class="md-input">{{$hotels->inclusions}}</textarea>
                                                                </div>
                                                                <div class="uk-width-medium-1-3 p-t-8">
                                                                    <label for="inv_service_rate">Exclusions</label>
                                                                    <textarea cols="30" name="exclusions[]" rows="2" class="md-input">{{$hotels->exclusions}}</textarea>
                                                                </div>
                                                           </div>
                                                           <div class="uk-clearfix"></div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                            @else
                                                <div class="hotel-details">
                                                    <div class="uk-width-medium-1-1">
                                                       <div class="uk-input-group uk-grid" style="width:100%">

                                                          <div class="uk-width-medium-1-6 p-t-8">
                                                             <div class="uk-form-row parsley-row">
                                                                <div class="md-input-wrapper">
                                                                    <label for="invoice_dp">Hotel Name</label>
                                                                    <input type="text" class="md-input"  name="hotel_name[]">

                                                                </div>
                                                             </div>
                                                          </div>
                                                          <div class="uk-width-medium-1-6 p-t-8">
                                                             <div class="uk-form-row parsley-row">
                                                                <div class="md-input-wrapper">
                                                                    <label for="invoice_dp">Rooming details</label>
                                                                    <input type="text" class="md-input" name="rooming_details[]">

                                                                </div>
                                                             </div>
                                                          </div>
                                                          <div class="uk-width-medium-1-6 p-t-8">
                                                             <div class="uk-form-row parsley-row">
                                                                <div class="md-input-wrapper">
                                                                    <label for="invoice_dp">Location</label>
                                                                    <input type="text" class="md-input" name="location[]">

                                                                </div>
                                                             </div>
                                                          </div>
                                                          <div class="uk-width-medium-1-6 p-t-8">
                                                             <div class="uk-form-row parsley-row">
                                                                <div class="md-input-wrapper">
                                                                    <label for="invoice_dp">Check In</label>
                                                                    <input type="text" class="md-input date-time-picker"name="check_in[]" autocomplete="off" >

                                                                </div>
                                                             </div>
                                                          </div>
                                                          <div class="uk-width-medium-1-6 p-t-8">
                                                             <div class="uk-form-row parsley-row">
                                                                <div class="md-input-wrapper">
                                                                    <label for="invoice_dp">Check Out</label>
                                                                    <input type="text" class="md-input date-time-picker" name="check_out[]" autocomplete="off" >

                                                                </div>
                                                             </div>
                                                          </div>
                                                          <div class="uk-width-medium-1-6 p-t-8" style="float: left">
                                                            <div class="uk-width-medium-1-2" style="float: left">
                                                                 <div class="uk-form-row parsley-row">
                                                                    <div class="md-input-wrapper">
                                                                        <label for="invoice_dp">No. Of Nights</label>
                                                                        <input type="text" class="md-input" name="number_of_nights[]">

                                                                    </div>
                                                                 </div>
                                                              </div>
                                                              <div class="uk-width-medium-1-2" style="float: left;margin-top: 12px; text-align: center;"> <div class="uk-form-row parsley-row">
                                                                    <a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus">delete</i></a>

                                                                    <a href="javaScript:void(0);" class="btnSectionClone destination_add_btn" data-type="plus"><i class="material-icons md-24">add_box</i></a> 
                                                                 </div>
                                                              </div>
                                                          </div>
                                                       </div>
                                                       <div class="uk-clearfix"></div>

                                                       <div class="uk-input-group uk-grid" style="width:100%">
                                                            <div class="uk-width-medium-1-2 p-t-8">
                                                                <label for="inv_service_rate">Inclusions</label>
                                                                <textarea cols="30" name="inclusions[]" rows="2" class="md-input"></textarea>
                                                            </div>
                                                            <div class="uk-width-medium-1-3 p-t-8">
                                                                <label for="inv_service_rate">Exclusions</label>
                                                                <textarea cols="30" name="exclusions[]" rows="2" class="md-input"></textarea>
                                                            </div>
                                                       </div>
                                                       <div class="uk-clearfix"></div>
                                                    </div>
                                                </div>
                                            @endif


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Quotation Number For Daywise Itinerary Plan:</label>
                                        <input type="text" class="md-input" name="quotation_id" value="{{ (isset($quote_info) && isset($quote_info->quatation_id) ? $quote_info->quatation_id : '') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <h3>Emergency Contacts:-</h3><hr>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                       <label>Contact Person Name 1:</label>
                                        <input type="text" class="md-input" name="em_name_1" value="{{ isset($lead_package->em_name_1) ? $lead_package->em_name_1 : '' }}" />
                                        @if($errors->has('em_name_1'))<small class="text-danger">{{ $errors->first('em_name_1') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Contact Person Number 1:</label>
                                        <input type="text" class="md-input" name="em_number_1" value="{{ isset($lead_package->em_number_1) ? $lead_package->em_number_1 : '' }}" />
                                        @if($errors->has('em_number_1'))<small class="text-danger">{{ $errors->first('em_number_1') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                       <label>Contact Person Name 2:</label>
                                        <input type="text" class="md-input" name="em_name_2" value="{{ isset($lead_package->em_name_2) ? $lead_package->em_name_2 : '' }}" />
                                        @if($errors->has('em_name_2'))<small class="text-danger">{{ $errors->first('em_name_2') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Contact Person Number 2:</label>
                                        <input type="text" class="md-input" name="em_number_2" value="{{ isset($lead_package->em_number_2) ? $lead_package->em_number_2 : '' }}" />
                                        @if($errors->has('em_number_2'))<small class="text-danger">{{ $errors->first('em_number_2') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                       <label>Contact Person Name 3:</label>
                                        <input type="text" class="md-input" name="em_name_3" value="{{ isset($lead_package->em_name_3) ? $lead_package->em_name_3 : '' }}" />
                                        @if($errors->has('em_name_3'))<small class="text-danger">{{ $errors->first('em_name_3') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Contact Person Number 3:</label>
                                        <input type="text" class="md-input" name="em_number_3" value="{{ isset($lead_package->em_number_3) ? $lead_package->em_number_3 : '' }}" />
                                        @if($errors->has('em_number_3'))<small class="text-danger">{{ $errors->first('em_number_3') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                       <label>Note</label>
                                       <textarea cols="30" name="note" rows="4" class="md-input">{{ isset($lead_package->note) ? $lead_package->note : '' }}</textarea>
                                        @if($errors->has('note'))<small class="text-danger">{{ $errors->first('note') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('lead.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
 
@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}new-theme/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
 
$(function () {
    $('.date-time-picker').datetimepicker({
        format: 'd F Y H:i',
    });
    //Add / Remove hotel

    $('div').on('click','.destination_add_btn',function(e){
        $('.date-time-picker').datetimepicker('destroy');
        var clone = $(".hotel-details:first").clone().find('input').val('').end().find('textarea').val('').end().insertAfter(".hotel-details:last");
        $('.date-time-picker').datetimepicker({
            format: 'd F Y H:i',
        });
        return false;
    });
    $('div').on('click','.destination_delete_btn',function(e){
      e.preventDefault();
      var length = $('.hotel-details').length;
      if(length == 1){
        return false;
      }
      $(this).closest('.hotel-details').remove();
    });
    //Add / Remove hotel




    //Velidation
    $("form#voucher_frm").validate({
        rules: {
            "guest_name" : "required",
            "package_id" : "required",
            "no_of_guests" : "required",
            "meal_plan" : "required",
            "book_through" : "required",
            "issued_by" : "required",
            "voucher_number" : "required",
            "issue_date" : "required",
            "hotel_name[]" : "required",
            "location[]" : "required",
            "check_in[]" : "required",
            "check_out[]" : "required",
            "number_of_nights[]" : "required",
        },
        // Specify validation error messages
        messages: {
            "name" : "Please enter document name",
            "status" : "Please select status",
            "guest_name" : "Please enter guest name",
            "package_id" : "Please select package type",
            "no_of_guests" : "Please enter no of guests",
            "meal_plan" : "Please enter meal plan",
            "book_through" : "Please enter book through",
            "issued_by" : "Please enter issued by",
            "voucher_number" : "Please enter voucher number",
            "issue_date" : "Please enter issue date",
            "hotel_name[]" : "Please enter hotel name",
            "location[]" : "Please enter location",
            "check_in[]" : "Please enter check in",
            "check_out[]" : "Please enter check out",
            "number_of_nights[]" : "Please enter number of nights",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
}); 
</script>
@endsection
