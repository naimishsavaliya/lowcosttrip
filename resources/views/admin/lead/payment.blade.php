@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Manage Payment</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('lead.payment.manage') }}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dt_default">
                <thead>
                    <tr>
                        <th>Lead ID</th>
                        <th>Customer Name</th>
                        <th class="hidden-phone">contact</th>
                        <th class="hidden-phone">Particular</th>
                        <!--<th class="hidden-phone">Title</th>-->
                        <th class="hidden-phone">Payment Date</th>
                        <th class="hidden-phone">Amount</th>
                        <th class="hidden-phone">Payment Mode</th>
                        <th class="hidden-phone">Supplier</th>
                        <th class="hidden-phone">Agent</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?= $i; ?>254</td>
                            <td>Customer <?= $i; ?></td>
                            <td>payment@lead.com <br /><?php echo "982". rand(111111, 999999); ?></td>
                            <td>Hotel/Tour/Flight/Activity</td>
                            <!--<td>Title <?= $i; ?></td>-->
                            <td><?= $i; ?> Jul 18 20:35</td>
                            <td><?= $i; ?>,244</td>
                            <td>Check</td>
                            <td>Supplier <?= $i; ?></td>
                            <td>Agent <?= $i; ?></td>
                            <td ><span class="uk-badge uk-badge-success">Active</span></td>
                            <td>
                                <a href="javascript:;" title="View Payment" class=""><i class="md-icon material-icons">visibility</i></a> 
                                <a href="javascript:;" title="Edit Payment" class=""><i class="md-icon material-icons">edit</i></a> 
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="update_status" class="uk-modal uk-hide uk-fade" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999;">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Update Status</h3>
        </div>
        <select id="selec_adv_s2_1" name="selec_adv_s2_1" class="select_no_search uk-width-large-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
            <option value="1">Status 1</option>
            <option value="2">Status 2</option>
            <option value="2">Status 3</option>
        </select>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button> <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Update</button>
        </div>
    </div>
</div>
@endsection
