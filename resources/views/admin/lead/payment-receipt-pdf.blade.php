
    <style>
    .invoice-box {
        /*max-width: 800px;*/
        /*margin: auto;*/
        padding: 10px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 15px;
        line-height: 18px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #000000;
    }
    
    .invoice-box table {
        width: 100%;
        /*line-height: inherit;*/
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 10px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    .txt-c {
        text-align: center;
    }
    .invoice-text{
        font-size:25px;
        text-align: center;
    }
    .address{
        font-size:10px;
        padding: 7px;
        text-align: left !important;
    }
    .date{
        font-size:10px;
        padding: 7px;
        text-align: left;
    }
    .mode{
        font-size:10px;
        padding: 7px;
        text-align: left;
    }
    .br-1{
        border:1px solid #000; 
        padding:0px; 
        font-size:10px;
        border-spacing: 0;
    }
    .user-info{
        line-height: 8px;
    }
    .product-list td{
        border:1px solid #000; 
        padding:0px; 
        font-size:10px;
        border-spacing: 0;
    }
    .total-amount{
        text-align: right; line-height: 8px; border-bottom: none !important; border-top: none !important;
    }
    .total-amt{
        text-align: right; line-height: 8px;
    }
    .declaration{
        border:1px solid #000; 
        padding:10px 0px; 
        font-size:10px;
        border-spacing: 0;
    }
    </style>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title> 
</head>
<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title" width="55%">
                                <img src="{{env('APP_PUBLIC_URL')}}client/images/logo.png" style="width:300px; max-width:300px;">
                            </td>
                            <td class="address"  width="30%">
                                <strong>Low Cost Trip</strong><br>
                                A-510, Khwaja Apartment, C.G. Road<br>
                                Rajkot,Gujarat<br>
                                India
                            </td>
                            <td class="address"  width="15%">
                                9632587410<br>
                                lowcost@lowcost.in
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid #DDD; padding: 25px;">
                    <table>
                        <tr> 
                            <td class="invoice-text">
                                Tax Invoice No.: #RNTINVINV38
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="date">
                                <strong>Invoice Date:</strong><br>
                                05 Aug 2017
                            </td>
                            
                            <td class="mode">
                                <strong>Payment Mode:</strong><br>
                                Cheque/NEFT/RTGS/Payment Gateway
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td width="60%"> 
                    <table class="user-info" cellspacing="0">
                        <tr>
                            <td class="br-1"><strong>Kishan Patel</strong></td>
                            <td class="br-1">State Code:</td>
                            <td class="br-1">24</td>
                        </tr>
                        <tr>
                            <td class="br-1">Ahmedabada</td>
                            <td class="br-1">PAN No.:</td>
                            <td class="br-1">24</td>
                        </tr>
                        <tr>
                            <td class="br-1">Ahmedabad - 123456</td>
                            <td class="br-1">GSTIN:</td>
                            <td class="br-1">24</td>
                        </tr>
                        <tr>
                            <td class="br-1">Gujarat, India</td>
                            <td class="br-1">Supply At:</td>
                            <td class="br-1">Gujarat</td>
                        </tr>
                    </table>
                </td>
                <td width="40%">
                    <table class="user-info"  cellspacing="0">
                        <tr>
                            <td class="br-1 txt-c" colspan="2"><strong>Seller GST Details</strong></td>
                        </tr>
                        <tr>
                            <td class="br-1">GSTIN:</td>
                            <td class="br-1">GSTIN</td>
                        </tr>
                        <tr>
                            <td class="br-1">CIN:</td>
                            <td class="br-1">CIN</td>
                        </tr>
                        <tr>
                            <td class="br-1">PAN No.:</td>
                            <td class="br-1">PAN No</td>
                        </tr>
                    </table> 
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <table class="product-list"  cellspacing="0">
                        <tr>
                            <td width="8%"><strong>Sr. No. </strong></td>
                            <td width="32%" style="text-align: left"><strong>Particulars </strong></td>
                            <td width="10%"><strong>Rate (INR) </strong></td>
                            <td width="10%"><strong>Qty </strong></td>
                            <td width="10%"><strong>Discount </strong></td>
                            <td width="10%"><strong>GST(%) </strong></td>
                            <td width="10%"><strong>HSN/SAC </strong></td>
                            <td width="10%"><strong>Amount(INR) </strong></td>
                        </tr>


                        <tr>
                            <td>1</td>
                            <td style="text-align: left">Particulars</td>
                            <td>5000</td>
                            <td>1</td>
                            <td>0.00</td>
                            <td>5.00</td>
                            <td>0.00</td>
                            <td>1000</td>
                        </tr>

                        <tr>
                            <td>1</td>
                            <td style="text-align: left">Particulars</td>
                            <td>5000</td>
                            <td>1</td>
                            <td>0.00</td>
                            <td>5.00</td>
                            <td>0.00</td>
                            <td>1000</td>
                        </tr>

                        <tr>
                            <td>1</td>
                            <td style="text-align: left">Particulars</td>
                            <td>5000</td>
                            <td>1</td>
                            <td>0.00</td>
                            <td>5.00</td>
                            <td>0.00</td>
                            <td>1000</td>
                        </tr>

                        <tr>
                            <td>1</td>
                            <td style="text-align: left">Particulars</td>
                            <td>5000</td>
                            <td>1</td>
                            <td>0.00</td>
                            <td>5.00</td>
                            <td>0.00</td>
                            <td>1000</td>
                        </tr>

                        <tr>
                            <td style="text-align: right; line-height: 8px; border-bottom: none; "  colspan="7">Sub Total:</td>
                            <td class="total-amt">54994.50</td>
                        </tr>
                        <tr>
                            <td class="total-amount"  colspan="7">CGST (+):</td>
                            <td  class="total-amt">2749.73</td>
                        </tr>
                        <tr>
                            <td class="total-amount"  colspan="7">SGST (+):</td>
                            <td  class="total-amt">2749.73</td>
                        </tr>
                        <tr>
                            <td class="total-amount"   colspan="7">IGST (+):</td>
                            <td   class="total-amt">0.00</td>
                        </tr>
                        <tr>
                            <td class="total-amount"   colspan="7">Total (INR):</td>
                            <td  class="total-amt">60493.95</td>
                        </tr>
                        <tr>
                            <td colspan="8" >Amount (in words): Sixty Thousands Four Hundred And Ninety Three Rupees .Nine Five Paise</td>
                        </tr>
                    </table>
                </td>
            </tr>


           <tr>
                <td colspan="2" > 
                    <table class="declaration" cellspacing="0">
                        <tr>
                            <td class="br-1"><strong>Declaration</strong></td>
                        </tr>
                        <tr>
                            <td class="br-1">we declare that 
                                this invoice shows the actual price 
                                of the goods or service described and that 
                                all particulars are true and correct.
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


           <tr>
                <td colspan="2" > 
                    <table class="declaration" cellspacing="0">
                        <tr>
                            <td class="br-1"><strong>Notes</strong></td>
                        </tr>
                        <tr>
                            <td class="br-1">simply dummy text of the printing and typesetting industry<br>
                                simply dummy text of the printing and typesetting industry
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

             
    </div>
</body>
</html>