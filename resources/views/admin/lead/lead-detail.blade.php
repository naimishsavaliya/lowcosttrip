@extends('layouts.theme')

@section('style')
<style type="text/css">
    .uk-sticky-placeholder .uk-tab{background: #1976D2;}
    .user_heading{ padding: 24px 24px 0px 24px;}
    .select2{ width: 100% !important;}
    .uk-tab > li.uk-active > a{color: #FFF;}
    .uk-tab > li.uk-active > a{border-bottom-color: #00071f;}
    .uk-dropdown{width: 200px !important;}
    .select2-container--open{z-index: 9999;}
    .uk-dropdown-shown{min-width: 260px !important;}
</style>
@endsection

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
    var cityData = <?= $cities_for_holidays ?>;
</script>
<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>
    <div id="page_content">
        <div id="page_content_inner" class="p-0">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-1-1">
                    <div class="">
                        <div class="user_heading" style="padding-top: 0px;position: fixed;width: 100%;z-index: 1000;padding-right: 0px">
                            <?php /*
                            <div class="user_heading_menu hidden-print">
                                <div class="uk-display-inline-block" data-uk-dropdown="{pos:'left-top'}">
                                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="{{ route('quotation.add', Request::route('id'))}}"><i class="material-icons ">add</i> Quotation</a></li>
                                            <!--<li><a href="{{ env('ADMIN_URL')}}hotel/lead_room_buy/1"><i class="material-icons ">add</i> Hotel Room</a></li>-->
                                            <!--<li><a href="#"><i class="material-icons ">add</i> Activity</a></li>-->
                                            <!--<li><a href="#C"><i class="material-icons ">add</i> Flight</a></li>-->
                                            <!--<li><a href="javascript:;" data-uk-modal="{target:'#assign_group_tour'}" ><i class="material-icons ">add</i> Assign Group Tour</a></li>-->
                                            <!--<li><a href="javascript:;" data-uk-modal="{target:'#assign_customised_tour'}" ><i class="material-icons ">add</i> Assign Customised Tour</a></li>-->
                                            <!--<li><a href="javascript:;" data-uk-modal="{target:'#update_status'}" ><i class="material-icons ">add</i> Update Status</a></li>-->
                                            <li><a href="{{ route('lead.note.add', Request::route('id'))}}"><i class="material-icons ">add</i> Followup</a></li>
                                            <li><a href="{{ route('lead.document.add',Request::route('id'))}}"><i class="material-icons ">add</i> Document</a></li>
                                            <li><a href="{{ route('lead.payment.view',Request::route('id'))}}"><i class="material-icons ">add</i> Payment</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate">{{ $lead->name }} #{{ $lead->lead_id }}</span>
                                    <span class="sub-heading">For {{ ucfirst($lead->first_name) .' '. ucfirst($lead->last_name) }} In <span class="md-btn md-btn-primary">{{ $lead->status }}</span></span></h2>
                            </div> */ ?>
                            <div class="uk-grid uk-width-medium-1-1">
                                <div class="uk-width-medium-1-2">
                                <!-- data-uk-sticky="{ top: 48, media: 960 } -->
                                    <!-- <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" "> -->
                                    <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content'}" ">
                                        <li class="uk-active"><a href="#">Lead Info</a></li>
                                        <li class="quotation"><a href="#" >Quotation</a></li>
                                        <!--<li class="hotel"><a href="#" >Hotel Rooms</a></li>-->
                                        <!--<li class="activity"><a href="#" >Activity</a></li>-->
                                        <!--<li class="flight"><a href="#" >Flight</a></li>-->
                                        <!--<li class="tour"><a href="#" >Tour</a></li>-->
                                        <li class="followup"><a href="#" >Followup</a></li>
                                        <li class="documents"><a href="#" >Document</a></li>
                                        <!--<li class="history"><a href="#" >History</a></li>-->
                                        <li class="payment"><a href="#" >Payment</a></li>
                                        <?php if($lead->interested_in == '3' ||$lead->interested_in == '4' ||$lead->interested_in == '7'){ ?>
                                            <li class="voucher"><a href="#" >Voucher</a></li>
                                        <?php } ?>
                                    </ul>
                                </div> 
                                <div class="uk-width-medium-1-2" style="text-align: right">
                                    <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}" aria-haspopup="true" aria-expanded="false">

                                        <div class="uk-navbar-flip p-t-8">
                                            <a href="javascript:void(0)" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                                                <i class="uk-icon-plus f-s-13"></i> Add New
                                            </a>
                                        </div>
                                        <div class="uk-dropdown uk-dropdown-small" style="text-align: left">
                                            <ul class="uk-nav">
                                                <li><a href="{{ route('quotation.add', Request::route('id'))}}"><i class="material-icons ">add</i> Quotation</a></li>
                                                <!--<li><a href="{{ env('ADMIN_URL')}}hotel/lead_room_buy/1"><i class="material-icons ">add</i> Hotel Room</a></li>-->
                                                <!--<li><a href="#"><i class="material-icons ">add</i> Activity</a></li>-->
                                                <!--<li><a href="#C"><i class="material-icons ">add</i> Flight</a></li>-->
                                                <!--<li><a href="javascript:;" data-uk-modal="{target:'#assign_group_tour'}" ><i class="material-icons ">add</i> Assign Group Tour</a></li>-->
                                                <!--<li><a href="javascript:;" data-uk-modal="{target:'#assign_customised_tour'}" ><i class="material-icons ">add</i> Assign Customised Tour</a></li>-->
                                                <!--<li><a href="javascript:;" data-uk-modal="{target:'#update_status'}" ><i class="material-icons ">add</i> Update Status</a></li>-->
                                                <li><a href="{{ route('lead.note.add', Request::route('id'))}}"><i class="material-icons ">add</i> Followup</a></li>
                                                <li><a href="{{ route('lead.document.add',Request::route('id'))}}"><i class="material-icons ">add</i> Document</a></li>
                                                <li><a href="{{ route('lead.payment.add_new_payment',Request::route('id'))}}"><i class="material-icons ">add</i> Payment</a></li>
                                                <?php if($lead->interested_in == '3' ||$lead->interested_in == '4' ||$lead->interested_in == '7'){ ?>
                                                    <li><a href="{{ route('lead.voucher.add',Request::route('id'))}}"><i class="material-icons ">add</i> Voucher</a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="user_content" style="padding-top: 60px">
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    @include('admin.lead.lead_form')
                                </li>
                                <li>
                                    <table class="uk-table" id="quotation_tbl" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Quotation Id</th>
                                                <th>Created Date</th>
                                                <th>Quotation Title</th>
                                                <th>Adult/Child/Infant</th>
                                                <th>Amount</th>
                                                <?php /*<th>Agent</th> */ ?>
                                                
                                                <th class="hidden-phone">Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
        <!--                                 <tfoot>
                                            <tr>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Quotation ID" type="text" class="md-input" colPos="0">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Created Date" type="text" class="md-input" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}" colPos="1">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Quotation Title" type="text" class="md-input" colPos="2"><span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td></td>
                                                <td></td> -->
                                                <?php /*
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Agent" type="text" class="md-input" colPos="4">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td> */ ?>
                                                <!-- <td> -->
                                                    <?php /*
                                                    <select class="md-input uk-form-width-small" data-placeholder="Choose a Status" tabindex="1" colPos="6">
                                                        <option value="">Status</option>
                                                        @if(isset($quotation_status))
                                                        @foreach($quotation_status as $quotation_status_key => $quotation_status_val)
                                                        <option value="{{$quotation_status_key}}">{{$quotation_status_val}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    */ ?>
                                                <!-- </td>
                                                <td></td>
                                            </tr>
                                        </tfoot> -->
                                        <tbody></tbody>
                                    </table>
                                </li>
                                <!--                                <li>
                                <table class="uk-table dt_default">

                                <thead>
                                <tr>
                                <th>Booking Date</th>
                                <th>Occupiency Date</th>
                                <th>Room type</th>
                                <th>No of Rooms</th>
                                <th>Adults</th>
                                <th>Childrens</th>
                                <th>Infants</th>
                                <th>Supplier</th>
                                <th>Agent</th>
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="Booking Date" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="Occupiency Date" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="Room type" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="No of Rooms" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="Adults" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="Childrens" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="Infants" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="Supplier" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>
                                <div class="md-input-wrapper">
                                <input placeholder="Agent" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                                </div>
                                </td>
                                <td>

                                </td>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php for ($i = 1; $i <= rand(3, 15); $i++) { ?>
                                <tr class="odd gradeX">
                                <td>Mar 5, 2018</td>
                                <td>Nov 10, 2018</td>
                                <td>lacus.</td>
                                <td>2</td>
                                <td>2</td>
                                <td>1</td>
                                <td>1</td>
                                <td>Supplier 1</td>
                                <td>Agent 1</td>
                                <td>
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                <div class="uk-dropdown">
                                <ul class="uk-nav uk-nav-dropdown">
                                <li><a href=""> Cancel Booking</a></li>
                                <li><a href=""> Print Voucher</a></li>
                                <li><a href=""> Email Voucher</a></li>
                                <li><a href=""> Edit Booking</a></li>
                                </ul>
                                </div>
                                </div>

                                </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                </table>
                                </li>-->
                                <!--                                <li>
                                <table class="uk-table dt_default">

                                <thead>
                                <tr>
                                <th>Booking Date</th>
                                <th>Activity Date & Time</th>
                                <th>Activity type</th>
                                <th>Adults</th>
                                <th>Child</th>
                                <th>Infant</th>
                                <th>Supplier</th>
                                <th>Agent</th>
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($i = 1; $i <= rand(3, 15); $i++) { ?>
                                <tr class="odd gradeX">
                                <td>Mar 5, 2018</td>
                                <td>Nov 10, 2018 10 AM</td>
                                <td>Sightseeing</td>
                                <td>2</td>
                                <td>1</td>
                                <td>1</td>
                                <td>Supplier</td>
                                <td>Agent</td>
                                <td>
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                <div class="uk-dropdown">
                                <ul class="uk-nav uk-nav-dropdown">
                                <li><a href=""> Cancel Booking</a></li>
                                <li><a href=""> Print Voucher</a></li>
                                <li><a href=""> Email Voucher</a></li>
                                <li><a href=""> Edit Activity</a></li>
                                </ul>
                                </div>
                                </div>

                                </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                </table>
                                </li>-->
                                <!--                                <li>
                                <table class="uk-table dt_default">

                                <thead>
                                <tr>
                                <th>Traveler Name</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Date & Time</th>
                                <th>Flight No.</th>
                                <th>PNR No.</th>
                                <th>Airline Name</th>
                                <th>Supplier</th>
                                <th>Agent</th>
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($i = 1; $i <= rand(3, 15); $i++) { ?>
                                <tr class="odd gradeX">
                                <td>Bhavik Shah</td>
                                <td>Ahmedabad</td>
                                <td>Delhi</td>
                                <td>Nov 10, 2018 10 AM</td>
                                <td>FT101</td>
                                <td>GJ3456</td>
                                <td>Air India</td>
                                <td>Supplier</td>
                                <td>Agent</td>
                                <td>
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                <div class="uk-dropdown">
                                <ul class="uk-nav uk-nav-dropdown">
                                <li><a href=""> Cancel Booking</a></li>
                                <li><a href=""> Print Ticket</a></li>
                                <li><a href=""> Email Ticket</a></li>
                                <li><a href=""> Edit Flight</a></li>
                                </ul>
                                </div>
                                </div>

                                </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                </table>
                                </li>-->
                                <!--                                <li style="min-height: 50px;">
                                <table class="uk-table dt_default">

                                <thead>
                                <tr>
                                <th>Tour ID</th>
                                <th>Tour Code</th>
                                <th>Tour Name</th>
                                <th>Departure Date</th>
                                <th>Duration</th>
                                <th>Adult/Child/Infant</th>
                                <th>Type</th>
                                <th>Supplier</th>
                                <th>Agent</th>
                                <th>Cost</th>
                                <th>Status</th>
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($i = 1; $i <= rand(1, 2); $i++) { ?>
                                <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td>KL852</td>
                                <td>Kerala 5 Nigh 6 day</td>
                                <td>10 July, 2019</td>
                                <td>5 Night 6 Day</td>
                                <td>2/1/0</td>
                                <td>Deluxe</td>
                                <td>Supplier 1</td>
                                <td>Agent 1</td>
                                <td>50,000</td>
                                <td>Pending</td>
                                <td>
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                <div class="uk-dropdown">
                                <ul class="uk-nav uk-nav-dropdown">
                                <li><a href=""> Cancel Booking</a></li>
                                <li><a href=""> Print Voucher</a></li>
                                <li><a href=""> Email Voucher</a></li>
                                <li><a href=""> View Tour Details</a></li>
                                </ul>
                                </div>
                                </div>

                                </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                </table>
                                </li>-->
                                <li>
                                    <table class="uk-table" id="lead_notes_tbl" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Agent</th>
                                                <th>Date Added</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Reminder</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <!-- <tfoot>
                                            <tr>
                                                <td class="pd0">
                                                    <input class="md-input" type="text"  placeholder="Agent" name="" colPos="0">
                                                </td>
                                                <td class="pd0">
                                                    <input class="md-input" type="text"  placeholder="Date Added" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}" colPos="1">
                                                </td>
                                                <td class="pd0">
                                                    <input class="md-input" type="text"  placeholder="Titel" name="" colPos="2">
                                                </td>
                                                <td class="pd0">

                                                </td>
                                                <td class="pd0">
                                                    <input class="md-input" type="text"  placeholder="Reminder" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}" colPos="4">
                                                </td>
                                            </tr>
                                        </tfoot> -->
                                        <tbody></tbody>
                                    </table>
                                </li>
                                <li>
                                    <table class="uk-table" id="document_tbl" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Date Added</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <!-- <tfoot>
                                            <tr>
                                                <td class="pd0">
                                                    <input class="md-input" type="text"  placeholder="Date Added" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}" colPos="0">
                                                </td>
                                                <td class="pd0">
                                                    <input class="md-input" type="text"  placeholder="Title" colPos="1">
                                                </td>
                                                <td class="pd0"></td>
                                                <td class="pd0"></td>
                                            </tr>
                                        </tfoot> -->
                                        <tbody></tbody>
                                    </table>
                                </li>
                                <li>
                                    <!-- <div class=" row box box-info disply_none" id="add_edit_form">
                                        <div id="display_update_form"></div>
                                    </div> -->    
                                    <div class="row uk-width-1-1 uk-text-right">
                                        <a href="{{ route('lead.payment.add_new_payment',Request::route('id'))}}" class="md-btn md-btn-primary">Add New Payment</a>
                                        <a href="{{ route('lead.payment.add_customer_discount',Request::route('id'))}}" class="md-btn md-btn-primary">Add New Customer Discount</a>
                                        <a href="{{ route('lead.payment.add_additional_services',Request::route('id'))}}" class="md-btn md-btn-primary">Add New Additional Services</a>
                                    </div> 
                                    <div id="page_content_inner" >
                                        <div class="uk-margin-medium-bottom">
                                            @include('admin.includes.alert')
                                        </div>
                                        <?php /*
                                        <table class="uk-table" id="paymrnt_info" style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <th>Date Added</th>
                                                    <th>Title</th>
                                                    <th>Description</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td class="pd0"><input class="md-input" type="text"  placeholder="Date Added" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}" colPos="0">
                                                    </td>
                                                    <td class="pd0"><input class="md-input" type="text"  placeholder="Title" colPos="1">
                                                    </td>
                                                    <td class="pd0"></td>
                                                    <td class="pd0"></td>
                                                </tr>
                                            </tfoot>
                                            <tbody></tbody>
                                        </table>
                                        */ ?>
                                        <table class="uk-table" id="payment_tbl" style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <!-- <th>RECEIPT NO</th>
                                                    <th>DATE & TIME</th>
                                                    <th> NAME</th> 
                                                    <th> COST</th>
                                                    <th>DISCOUNT</th>
                                                    <th>PAID AMT</th> -->
                                                    <!-- <th>CANCEL AMT</th>
                                                    <th>REFUND AMT</th> -->
                                                    <!-- <th>BALANCE AMT</th>
                                                    <th>MODE</th>
                                                    <th>NOTE</th>
                                                    <th style="width: 10%;">ACTION</th> -->

                                                     <th>PARTICULAR</th>
                                                     <th>DATE</th>
                                                     <th>AMOUNT</th>
                                                     <th>DISCOUNT</th>
                                                     <th>PAID AMT</th>
                                                     <th>BALANCE AMT</th>
                                                     <th style="width: 10%;">ACTION</th>
                                                </tr>
                                            </thead>
                                            <?php /*
                                            <tfoot>
                                                <tr>
                                                    <!-- <td><div class="md-input-wrapper"><input placeholder="PARTICULAR" name="" colPos="0" type="text" class="md-input"><span class="md-input-bar "></span></div></td> -->
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                    <!-- <td><div class="md-input-wrapper"><input placeholder="AMOUNT" name="" colPos="2" type="text" class="md-input"><span class="md-input-bar "></span></div></td> -->

                                                    <td><div class="md-input-wrapper"><input placeholder="DISCOUNT" name="" colPos="3" type="text" class="md-input"><span class="md-input-bar "></span></div></td>

                                                    <td><div class="md-input-wrapper"><input placeholder="PAID AMT" name="" colPos="4" type="text" class="md-input"><span class="md-input-bar "></span></div></td>

                                                    <td><div class="md-input-wrapper"><input placeholder="BALANCE AMT" name="" colPos="5" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                                                    */ ?>

                                                    <!-- <td><div class="md-input-wrapper"><input placeholder="Cancel Amount" name="" colPos="6" type="text" class="md-input"><span class="md-input-bar "></span></div></td>

                                                    <td><div class="md-input-wrapper"><input placeholder="Refund Amount" name="" colPos="7" type="text" class="md-input"><span class="md-input-bar "></span></div></td> -->

                                                    <!-- <td><div class="md-input-wrapper"><input placeholder="Balance Amount" name="" colPos="6" type="text" class="md-input"><span class="md-input-bar "></span></div></td> -->
                                                    <?php /*
                                                    <td>
                                                        <select class="md-input uk-form-width-small" data-placeholder="Choose a Group" tabindex="1" colPos="6">
                                                            <option value="">Choose Payment Mode</option>
                                                            @foreach($paymentMode as $paymentMode_key => $paymentMode_val)
                                                            <option value="{{$paymentMode_key}}">{{$paymentMode_val}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><div class="md-input-wrapper"><input placeholder="Note" name="" colPos="7" type="text" class="md-input"><span class="md-input-bar "></span></div></td>
                                                    <td></td>
                                                    */ ?>
                                                  <?php /*  
                                                </tr>
                                            </tfoot>
                                            */ ?>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                                <li>
                                    
                                    <div id="page_content_inner" >
                                        <div class="uk-margin-medium-bottom">
                                            @include('admin.includes.alert')
                                        </div>
                                        <table class="uk-table" id="package_table" style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Guest Name</th>
                                                    <th>Voucher Number</th>
                                                    <th>Package</th>
                                                    <th>Guests</th>
                                                    <th style="width: 10%;">ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-grid">
                                        <div class="uk-width-1-1">
                                            <div class="uk-grid" data-uk-grid-margin="">
                                                <div class="uk-width-large-1-6 uk-width-1-1 uk-row-first" style="padding-top: 10px;">
                                                    <h3>Filter History</h3>
                                                </div>
                                                <div class="uk-width-large-2-10 uk-width-1-1" style="padding-left: 0;">
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                                        <div class="md-input-wrapper"><label for="uk_dp_start">Start Date</label><input class="md-input" type="text" id="uk_dp_start"><span class="md-input-bar "></span></div>

                                                    </div>
                                                </div>
                                                <div class="uk-width-large-2-10 uk-width-medium-1-1">
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                                        <div class="md-input-wrapper"><label for="uk_dp_end">End Date</label><input class="md-input" type="text" id="uk_dp_end"><span class="md-input-bar "></span></div>

                                                    </div>
                                                </div>
                                                <div class="uk-width-large-1-10 uk-width-1-1">
                                                    <a class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light" href="javascript:void(0)">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear clearfix"></div> <br>
                                    <div class="uk-width-large-1-2 m-t-15" style="margin-left: 25%;">
                                        <div class="timeline timeline_small uk-margin-bottom">
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_success"><i class="material-icons">add</i></div>
                                                <div class="timeline_date">
                                                    09 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">Deluxe room inventory increased to 10 for 05 Jan 2019 to 08 Feb 2019 by user name.</div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_danger"><i class="material-icons">remove</i></div>
                                                <div class="timeline_date">
                                                    15 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">Deluxe room inventory decrease to 10 for 05 Jan 2019 to 08 Feb 2019 by user name.<a href="#"><strong>Velit est quisquam dolorem officiis quis.</strong></a></div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_success"><i class="material-icons">done</i></div>
                                                <div class="timeline_date">
                                                    09 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">New booking received for Super Deluxe room for 3 rooms for the 01 Jan 2019 in inquiry #LCT123. </div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_danger"><i class="material-icons">clear</i></div>
                                                <div class="timeline_date">
                                                    15 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">3 Deluxe rooms canceled for the date 01 Jan 2019 in inquiry #LCT123.</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Agent</th>
                                                <th>Date Added</th>
                                                <th>Title</th>
                                                <th>Debit</th>
                                                <th>Credit</th>
                                                <th>Balance</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>Agent 1</td>
                                                <td>17/03/2019</td>
                                                <td>Kerala Tour Package</td>
                                                <td>50,000</td>
                                                <td>-</td>
                                                <td>50,000</td>
                                                <td >
                                                    <a href="" title="View Invoice" class=""><i class="md-icon material-icons">visibility</i></a> 
                                                    <a href="" title="Edit notes" class=""><i class="md-icon material-icons">edit</i></a> 
                                                    <a href="" onclick="return confirm('Are you sure to delete?');" title="Delete notes" class=""><i class="md-icon material-icons">delete</i></a> 
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>Agent 1</td>
                                                <td>17/03/2019</td>
                                                <td>Customer Payment</td>
                                                <td>-</td>
                                                <td>10,000</td>
                                                <td>40,000</td>
                                                <td >
                                                    <a href="" title="View Receipt" class=""><i class="md-icon material-icons">visibility</i></a> 
                                                    <a href="" title="Edit notes" class=""><i class="md-icon material-icons">edit</i></a> 
                                                    <a href="" onclick="return confirm('Are you sure to delete?');" title="Delete notes" class=""><i class="md-icon material-icons">delete</i></a> 
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php /*
            <div id="quotation_changestatus" class="uk-modal" >
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Status</h3>
                    </div>
                    <div class="">
                        <div class="control-group">
                            <input type="hidden" value="" id="change-status-row-id" />
                            <label class="control-label">Status</label>
                            <div class="controls">
                                <span class="icheck-inline">
                                    <input type="radio" name="status" class="user-status-change" value="1" id="status_dialog_active" data-md-icheck />
                                    <label for="status_dialog_active" class="inline-label">Accept</label>
                                </span>
                                <span class="icheck-inline">
                                    <input type="radio" name="status" class="user-status-change" value="0" id="status_dialog_inactive" data-md-icheck />
                                    <label for="status_dialog_inactive" class="inline-label">Send</label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close" id="status-dlg-btn">Submit</button> 
                    </div>
                </div>
            </div>
            */ ?>
            <div id="quotation_update_status" class="uk-modal uk-hide uk-fade" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999;">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Update Status </h3>
                    </div>
                    <input type="hidden" value="" id="quotation-change-status-row-id" />
                    <select id="quotation_status_id" class="md-input uk-width-large-1-2 p-t-4" data-uk-tooltip="{pos:'bottom'}" title="Select Status...">
                        <option value="0">Select Status...</option>
                        @if(isset($quotation_status))
                        @foreach($quotation_status as $quotation_status_key => $quotation_status_val)
                        <option value="{{$quotation_status_key}}">{{$quotation_status_val}}</option>
                        @endforeach
                        @endif
                    </select>
                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close" id="quotation-close-statud-dialog">Close</button>
                        <button type="button" id="quotation-status-dlg-btn" class="md-btn md-btn-flat md-btn-flat-success">Update</button>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="assign_group_tour">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Assign Group Tour</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Tour</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Gujarat Tour</option>
                                <option value="1">Kerala Tour</option>
                                <option value="2">Kashmir Tour</option>
                                <option value="2">Rajasthan Tour</option>
                                <option value="2">Punjab Tour</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Departure Date</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">01 Jan 2019</option>
                                <option value="2">01 Feb 2019</option>
                                <option value="2">01 Mar 2019</option>
                                <option value="2">01 Jun 2019</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-modal-footer">
                        <div class="uk-text-left uk-width-medium-1-2 uk-float-left">
                            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-flat">Create New Group Tour</a>
                        </div>
                        <div class="uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="assign_customised_tour">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Assign Customised Tour</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Tour</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Gujarat Tour</option>
                                <option value="1">Kerala Tour</option>
                                <option value="2">Kashmir Tour</option>
                                <option value="2">Rajasthan Tour</option>
                                <option value="2">Punjab Tour</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                        <div class="uk-width-medium-1-1 ">
                            <div class="md-input-wrapper"><label for="invoice_dp">Departure Date</label><input class="md-input uk-width-1-1 " type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}"><span class="md-input-bar "></span></div>

                        </div>
                        <div class="clear clearfix"></div>
                    </div>
                    <div class="uk-modal-footer">
                        <div class="uk-text-left uk-width-medium-1-2 uk-float-left">
                            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-flat">Create New Customised Tour</a>
                        </div>
                        <div class="uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="update_status">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Update Status</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Status</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Status 1</option>
                                <option value="2">Status 2</option>
                                <option value="2">Status 3</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                    </div>
                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/get_regions.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://naimish/lowcosttrip/public/theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="http://naimish/lowcosttrip/public/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(function () {
        if (location.hash && location.hash.length) {
            var hash = decodeURIComponent(location.hash.substr(1));
            setTimeout(function (){
                $("ul#user_profile_tabs li." + hash).trigger("click");
            }, 1000);
        }
        //Lead Notes tbl
        // var leadnotesTable = $('#lead_notes_tbl').DataTable({
        //     processing: true,
        //     serverSide: true,
        //     destroy: true,
                // pageLength: 20,
                // lengthMenu: [ 10, 20, 50, 75, 100 ],
        //     searching: true,
        //     order: [[ 1, "desc" ]],
        //     "ajax": {
        //         "url": "{{ route('lead.notes.data', Request::route('id')) }}",
        //         "dataType": "json",
        //         "type": "POST",
        //         "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
        //         beforeSend: function () {
        //             if (typeof leadnotesTable != 'undefined' && leadnotesTable.hasOwnProperty('settings')) {
        //                 leadnotesTable.settings()[0].jqXHR.abort();
        //             }
        //         }
        //     },
        //     "columns": [
        //         {"data": "full_name", "name": "full_name"},
        //         {"data": "created_at", "name": "lead_notes.created_at"},
        //         {"data": "title", "name": "lead_notes.title"},
        //         {"data": "description", "name": "lead_notes.description"},
        //         {"data": "followup_date", "name": "lead_notes.followup_date"},
        //         {"data": "action", 'sortable': false, "render": function (data, type, row) {
        //                 var lead_note_edit = "{{ route('lead.note.edit', ['lead_id' => ':lead_id', 'id' => ':notId']) }}";
        //                 var lead_note_delete = "{{ route('lead.notes.delete', ['lead_id' => ':lead_id', 'id' => ':notId']) }}";
        //                 lead_note_edit = lead_note_edit.replace(':lead_id', row.lead_id).replace(':notId', row.notId);
        //                 lead_note_delete = lead_note_delete.replace(':lead_id', row.lead_id).replace(':notId', row.notId);
        //                 return '<a href="' + lead_note_edit + '" title="Edit Lead notes" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + lead_note_delete + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete Lead Note" class=""><i class="md-icon material-icons">delete</i></a>';
        //             },
        //         },
        //     ]
        // });
        var leadnotesTable = $('#lead_notes_tbl').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,            
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            searching: true,
            order: [[ 1, "desc" ]],
            "ajax": {
                "url": "{{ route('lead.notes.data', Request::route('id')) }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {
                    if (typeof leadnotesTable != 'undefined' && leadnotesTable.hasOwnProperty('settings')) {
                        leadnotesTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {data: "full_name", name: "full_name"},
                {data: "created_at", name: "created_at"},
                {data: "title", name: "title"},
                {data: "description", name: "description"},
                {data: "followup_date", name: "followup_date"},
                {data: "action", 'sortable': false, "render": function (data, type, row) {
                        var lead_note_edit = "{{ route('lead.note.edit', ['lead_id' => ':lead_id', 'id' => ':notId']) }}";
                        var lead_note_delete = "{{ route('lead.notes.delete', ['lead_id' => ':lead_id', 'id' => ':notId']) }}";
                        lead_note_edit = lead_note_edit.replace(':lead_id', row.lead_id).replace(':notId', row.notId);
                        lead_note_delete = lead_note_delete.replace(':lead_id', row.lead_id).replace(':notId', row.notId);
                        return '<a href="' + lead_note_edit + '" title="Edit Lead notes" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + lead_note_delete + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete Lead Note" class=""><i class="md-icon material-icons">delete</i></a>';
                    },
                },
            ]
        });
        $(leadnotesTable.table().container()).on('keyup change', 'tfoot input', function () {
            leadnotesTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(leadnotesTable.table().container()).on('change', 'tfoot select', function () {
            leadnotesTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        //Lead Document tbl 
        var documentTable = $('#document_tbl').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
            order: [[ 0, "desc" ]],
            "ajax": {
                "url": "{{ route('lead.document.data',Request::route('id')) }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {
                    if (typeof documentTable != 'undefined' && documentTable.hasOwnProperty('settings')) {
                        documentTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "created_at", "name": "created_at"},
                {"data": "name", "name": "name"},
    //                                                                    {"data": "document", "name": "document"},
                {"data": "description", "name": "description"},
                {"data": "action", 'sortable': false, "render": function (data, type, row) {
                        var lead_document_edit = "{{ route('lead.document.edit', ['lead_id' => ':lead_id', 'id' => ':docId']) }}";
                        var lead_document_delete = "{{ route('lead.document.delete', ['id' => ':docId']) }}";
                        lead_document_edit = lead_document_edit.replace(':lead_id', row.lead_id).replace(':docId', row.docId);
                        lead_document_delete = lead_document_delete.replace(':lead_id', row.lead_id).replace(':docId', row.docId);
                        return '<a href="' + lead_document_edit + '" title="Edit Lead Document" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + lead_document_delete + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete Lead Document" class="document-lead-note"><i class="md-icon material-icons">delete</i></a>';
                    },
                },
            ]
        });
        $(documentTable.table().container()).on('keyup change', 'tfoot input', function () {
            documentTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(documentTable.table().container()).on('change', 'tfoot select', function () {
            documentTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        //Lead Document Delete
        $(document.body).on('click', '.document-lead-note', function () {
            var $obj = $(this);
            var document_delete_url = $obj.attr('href');
            $.ajax({
                type: 'get',
                url: document_delete_url,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                }, success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        documentTable.draw();
                    }
                    showNotify(data.msg_status, data.message);
                }
            });
            return false;
        });
        //Lead Quotation tbl 
        // var quotationTable = $('#quotation_tbl').DataTable({
        //     processing: true,
        //     serverSide: true,
        //     destroy: true,
        //     order: [[ 1, "desc" ]],
        //     "ajax": {
        //         "url": "{{ route('quotation.data',Request::route('id')) }}",
        //         "dataType": "json",
        //         "type": "POST",
        //         "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
        //         beforeSend: function () {
        //             if (typeof quotationTable != 'undefined' && quotationTable.hasOwnProperty('settings')) {
        //                 quotationTable.settings()[0].jqXHR.abort();
        //             }
        //         }
        //     },
        //     "columns": [
        //         {"data": "quatation_id", "name": "lead_quatation.quatation_id"},
        //         {"data": "created_at", "name": "lead_quatation.created_at"},
        //         {"data": "subject", "name": "lead_quatation.subject"},
        //         {"data": "members", "name": "lead_quatation.adult"},
        //         {"data": "total_amount", "name": "lead_quatation.total_amount"},
        //         // {"data": "full_name", "name": "full_name"},
        //         {"data": "status_id","name": "status_id"},
        //         {"data": "action", 'sortable': false, "render": function (data, type, row) {
        //                 var quotation_edit = "{{ route('quotation.edit', ['lead_id' => ':lead_id', 'id' => ':id']) }}";
        //                 var quotation_delete = "{{ route('quotation.delete', ['id' => ':id']) }}";
        //                 quotation_edit = quotation_edit.replace(':lead_id', row.lead_id).replace(':id', row.quaId);
        //                 quotation_delete = quotation_delete.replace(':id', row.quaId);

        //                 return '<a href="' + quotation_edit + '" title="Edit Quotation" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + quotation_delete + '" data-id="' + row.quaId + '"  title="Delete Quotation" class="quotation-delete"><i class="md-icon material-icons">delete</i></a>';
        //             },
        //         },
        //     ]
        // });
        var quotationTable = $('#quotation_tbl').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
            order: [[ 1, "desc" ]],
            "ajax": {
                "url": "{{ route('quotation.data',Request::route('id')) }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {
                    if (typeof quotationTable != 'undefined' && quotationTable.hasOwnProperty('settings')) {
                        quotationTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {data: "quatation_id", name: "quatation_id"},
                {data: "created_at", name: "created_at"},
                {data: "subject", name: "subject"},
                {data: "members", name: "members"},
                {data: "total_amount", name: "total_amount"},
                {data: "status_id",name: "status_id"},
                {data: "action", 'sortable': false, "render": function (data, type, row) {
                        var quotation_edit = "{{ route('quotation.edit', ['lead_id' => ':lead_id', 'id' => ':id']) }}";
                        var quotation_delete = "{{ route('quotation.delete', ['id' => ':id']) }}";
                        quotation_edit = quotation_edit.replace(':lead_id', row.lead_id).replace(':id', row.quaId);
                        quotation_delete = quotation_delete.replace(':id', row.quaId);

                        return '<a href="' + quotation_edit + '" title="Edit Quotation" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + quotation_delete + '" data-id="' + row.quaId + '"  title="Delete Quotation" class="quotation-delete"><i class="md-icon material-icons">delete</i></a>';
                    },
                },
            ]
        });
        $(quotationTable.table().container()).on('keyup change', 'tfoot input', function () {
            quotationTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(quotationTable.table().container()).on('change', 'tfoot select', function () {
            quotationTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(document.body).on('click', '.quotation-delete', function () {
            var cfrm = confirm("Are you sure to delete?");
            if (cfrm) {
                var $obj = $(this);
                var quo_id = $obj.attr('data-id');
                var delete_url = $obj.attr('href');
                $.ajax({
                    type: 'get',
                    url: delete_url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {

                    }, success: function (data, textStatus, jqXHR) {
                        if (data.success) {
                            quotationTable.draw();
                        }
                        showNotify(data.msg_status, data.message);
                    }
                });
            }
            return false;
        });

        $(document.body).on('click', '.quotation-change-status-btn', function () {
            var $obj = $(this);
            var row_id = $obj.attr('row-id');
            var status = $obj.attr('data-id');
            $('input#quotation-change-status-row-id').val(row_id);
            if (status) {
                $("select#quotation_status_id option[value='" + status + "']").attr("selected", "selected");
            }
        });

        $(document.body).on('click', '#quotation-status-dlg-btn', function () {
            var $obj = $(this);
            var btn_txt = $obj.text();
            var row_id = $('#quotation-change-status-row-id').val();
            var status = $('select#quotation_status_id').val();
            if (row_id == false && status == false) {
                return false;
            }
            $.ajax({
                type: 'get',
                url: '{{ route("quotation_change_status")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table": "lead_quatation",
                    "id": row_id,
                    "column_name": "quaId",
                    "column_update": "status_id",
                    "status": status
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $obj.prop('disabled', true);
                    $obj.text('Wait...');
                },
                success: function (data, textStatus, jqXHR) {
                    $obj.text(btn_txt);
                    $obj.prop('disabled', false);
                    if (data.success) {
                        quotationTable.draw();
                    }
                    $('select#quotation_status_id option:selected', this).removeAttr('selected');
                    $('#quotation-close-statud-dialog').trigger('click');
                    showNotify(data.msg_status, data.message);
                }
            });
        });

    });


    $(document).on("click", ".open_my_form_form", function (event) {
        var $obj = $(this);
        var url = $obj.attr('data-action');
        $.ajax({
            type: 'POST',
            url: url,
            async: false,
            data: {"_token": "{{ csrf_token() }}",},
            dataType: 'html',
            beforeSend: function () {
    //                                                                    $('#display_update_form').jmspinner('large');
                $('#add_edit_form').show();
            },
            success: function (returnData) {
                $('#display_update_form').html(returnData);
    //                                                                    $('.load_hide').hide();
            },
            error: function (xhr, textStatus, errorThrown) {
                new PNotify({ title: 'Error', text: 'There was an unknown error that occurred. You will need to refresh the page to continue working.', type: 'error', styling: 'bootstrap3' });
            },
            complete: function () {
                
            }
        });
        return false;
    });

    </script>
    <script type = "text/javascript" >
    $(function () {
        var paymentTable = $('#payment_tbl').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            searching: true,
            "order": [[ 1, "desc" ]],
            "ajax": {
                "url": "{{ route('lead_payment.data', Request::route('id') ) }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {
                    if (typeof paymentTable != 'undefined' && paymentTable.hasOwnProperty('settings')) {
                        paymentTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "receipt_no", "name": "receipt_no"},
                {"data": "created_at", "name": "created_at"},
                //{"data": "additional_services_name", "name": "additional_services_name"},
                {"data": "additional_service_fees", "name": "additional_service_fees"},
                {"data": "discount_amount", "name": "discount_amount"},
                {"data": "paid_amount", "name": "paid_amount"},
                // {"data": "cancel_amount", "name": "cancel_amount"},
                // {"data": "refund_amount", "name": "refund_amount"},
                {"data": "balance", "name": "balance"},
                //{"data": "payment_mode", "name": "payment_mode"},
                //{"data": "payment_note", "name": "payment_note"},
                {"data": "action", "sortable": false,"name": "action"}
            ]
        });
        $(paymentTable.table().container()).on('keyup', 'tfoot input', function () {
            paymentTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(paymentTable.table().container()).on('change', 'tfoot select', function () {
            paymentTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
    });
    </script>
    <script type = "text/javascript" >
     
    $(function () {
        var paymentTable = $('#package_table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            searching: true,
            "order": [[ 0, "desc" ]],
            "ajax": {
                "url": "{{ route('lead.voucher.data', Request::route('id') ) }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {
                    if (typeof paymentTable != 'undefined' && paymentTable.hasOwnProperty('settings')) {
                        paymentTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "id", "name": "id"},
                {"data": "guest_name", "name": "guest_name"},
                {"data": "voucher_number", "name": "voucher_number"},
                {"data": "name", "name": "name"},
                {"data": "no_of_guests", "name": "no_of_guests"},
                                //{"data": "payment_note", "name": "payment_note"},
                {"data": "action", "sortable": false,"name": "action"}
            ]
        });
        $(paymentTable.table().container()).on('keyup', 'tfoot input', function () {
            paymentTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
        $(paymentTable.table().container()).on('change', 'tfoot select', function () {
            paymentTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });
    });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".destination_add_btn").click(function () {
                var i = $(".destination_row").length;
                console.log(i);
                var html = '<tr class="destination_row">'
                        + '<td>'
                        + '<input type="text" name="destination['+i+'][city_name]" class="md-input autocomplete" placeholder="Search by city name">'
                        + '<input type="hidden" name="destination['+i+'][city_id]">'
                        + '</td>'
                        + '<td>'
                        + '<input type="number" name="destination['+i+'][number_of_nights]" class="md-input onlynumber" value="1" min="1" max="10">'
                        + '</td>'
                        + '<td>'
                        + '<a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus" data-field="quant[1]">delete</i></a>'
                        + '</td>'
                        + '</tr>';
                $(html).insertBefore(".add_btn_tr");

                $('#lead_frm').validate();

                var destination = $('input[name^="destination"]');
                destination.filter('input[name$="[city_name]"]').each(function() {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "This city name field is require"
                        }
                    });
                });

                var city_id = $('input[name^="destination"]');
                city_id.filter('input[name$="[city_id]"]').each(function() {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "This city id field is require"
                        }
                    });
                });
                
                var number_of_nights = $('input[name^="destination"]');
                number_of_nights.filter('input[name$="[number_of_nights]"]').each(function() {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "This number of nights field is require"
                        }
                    });
                });
                $(".autocomplete").autocomplete({
                    source: cityData,
                    minLength: 3,
                    focus: function (event, ui) {
                        event.preventDefault();
                        $(this).val(ui.item.label);
                    },
                    select: function (event, ui) {
                        event.preventDefault();
                        $(this).val(ui.item.label);
                        $(this).next().val(ui.item.value);
                    }
                });
                $(".onlynumber").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
            });
            $("body").on("click", ".destination_delete_btn", function () {
                $(this).closest("tr").remove();
            });
            $(".autocomplete").autocomplete({
                source: cityData,
                minLength: 3,
                focus: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                },
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.label);
                    $(this).next().val(ui.item.value);
                }
            });
            $(".onlynumber").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });       
        });
        $(document).ready(function () {
            if($('#intrested_in').val() == '7'){
                $('.holidays-information').removeAttr("style");
            } 

            $(document).on('change', '#intrested_in', function() {
                if($(this).val() == '7'){
                    $('.holidays-information').removeAttr("style");
                }else{
                    $(".holidays-information").css("display", "none");
                }
            });

            $(document).on("click", ".add_tour_activites", function (event) {
                $.ajax({
                    type: 'GET',
                    url: "{{ route('lead.get-activiti-html') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {

                    },
                    success: function (data, textStatus, jqXHR) {
                        $('.append_new_row').before( data.html ); 

                        
                            // $('input[name^="activiti_details"]').each(function() {
                            //     alert("hi2");
                            //     alert($(this).val());
                            //     $(this).rules("add", {
                            //         required: true,
                            //         messages: {
                            //             required: "This number of nights field is require"
                            //         }
                            //     });
                            // });
                        


                        $('.country_id_activity').each(function() {
                            $(this).rules("add", 
                                {
                                    required: true,
                                    messages: {
                                        required: "This Country field is require",
                                    }
                                });
                        });
                        $('.tour_activity').each(function() {
                            $(this).rules("add", 
                                {
                                    required: true,
                                    messages: {
                                        required: "This Activity field is require",
                                    }
                                });
                        });

                    }
                });
            });

            $(document).on("change", ".country_id_activity", function (event) {
                $th = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('lead.get-activiti-by-city') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id" :  $th.val(),
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Please wait..</option>');
                    },
                    success: function (data, textStatus, jqXHR) {
                        $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Choose Activity</option>');

                        $.map(data.activities, function (item) {
                            $th.closest('tr').find('td:eq(1) .tour_activity').append('<option value="' + item.id + '">' + item.title + '</option>');
                        });
                    }
                });
            });

            $( ".edit_country_id" ).each(function() {
                $th = $(this);
                $c_id = $th.attr("data-id");
                var activity_class = '.edit_tour_activity_'+$c_id;
                var activity_id = '.activity_id_'+$c_id;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('lead.get-activiti-by-city') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id" :  $th.val(),
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        $(activity_class).html('<option value="" selected>Please wait..</option>');
                    },
                    success: function (data, textStatus, jqXHR) {
                        $(activity_class).html('<option value="" selected>Choose Activity</option>');
                        $.map(data.activities, function (item) {
                            var selected = ''
                            if($(activity_id).val() == item.id){
                                selected = 'selected';
                            }
                            $(activity_class).append('<option '+selected+' value="' + item.id + '">' + item.title + '</option>');
                        });
 
                    }
                });
            }); 
        }); 
    </script>
@endsection