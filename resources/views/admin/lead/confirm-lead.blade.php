@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner" >
			<form name="confirm_lead_frm" id="confirm_lead_frm" onsubmit="return false;">
				{{csrf_field()}}
				<input type="hidden" name="lead_id" value="{{$lead->id}}"/>
				<div class="uk-width-medium-1-12">
					<div class="uk-form-row">
						<div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">
								<select class="md-input" name="status_id" id="status_id" data-uk-tooltip="{pos:'bottom'}" title="Select Status...">
									<option value="10" selected>Confirm</option>
								</select>
							</div>
							<div class="uk-width-medium-1-2 p-l-50">
								<label>Confirm Lead No</label>
								<input type="text" class="md-input" readonly="readonly" name="cnf_lead_id" value="{{$lead->cnf_lead_id}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-12">
					<div class="uk-form-row">
						<div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">
								<select class="md-input required_field" name="tour_id" id="tour_id" data-uk-tooltip="{pos:'bottom'}" title="Select Tour Code...">
									<option value="" selected>Select Tour Code</option>
									@foreach($tours as $key => $tour)
									<option value="{{$tour->id}}" {{($lead->tour_id == $tour->id) ? "selected='selected'" : ""}}>{{$tour->tour_code}}</option>
									@endforeach
								</select>
							</div>
							<div class="uk-width-medium-1-2 p-l-50">
								<label>No.Of Audlt</label>
								<input type="text" class="md-input required_field" name="adult" value="{{$lead->adult}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-12">
					<div class="uk-form-row">
						<div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">
								<select class="md-input required_field" name="departure_date" id="departure_date" data-uk-tooltip="{pos:'bottom'}" title="Select Confirm Departure Date">
									<option value="">Confirm Departure Date</option>
									@foreach($departure_date as $key => $date)
									<option value="{{$date->id}}" {{($lead->departure_date_id == $date->id) ? "selected='selected'" : ""}}>{{$date->departure_date}}</option>
									@endforeach
								</select>
							</div>
							<div class="uk-width-medium-1-2 p-l-50">
								<label>No.Of Child</label>
								<input type="text" class="md-input required_field" name="child" value="{{$lead->child}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-12">
					<div class="uk-form-row">
						<div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">
								<select class="md-input required_field" name="tour_type" id="tour_type" data-uk-tooltip="{pos:'bottom'}" title="Select Type">
									<option value="">Type</option>
									@foreach($tour_type as $key => $tour_type)
									<option value="{{$tour_type->typeId}}" {{($lead->tour_type == $tour_type->typeId) ? "selected='selected'" : ""}}>{{$tour_type->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="uk-width-medium-1-2 p-l-50">
								<label>No. Of Rooms</label>
								<input type="text" class="md-input required_field" name="total_room" value="{{$lead->no_of_room}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-12">
					<div class="uk-form-row">
						<div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">
								<label>Invoice Name</label>
								<input type="text" class="md-input" name="invoice_name" value="{{$lead->invoice_name}}" />
							</div>
							<div class="uk-width-medium-1-2 p-l-50">

							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-12 accomodation-wrap"><?= $accomodation_view; ?></div>
				<div class="uk-width-medium-1-12 uk-margin-medium-top">
					<div class="uk-form-row">
						<div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">
								<button type="button" class="md-btn md-btn-primary add-new-room" style="{{(!empty($lead->tour_type)) ? "" : "display: none;"}}">Add Room</button>
							</div>
							<div class="uk-width-medium-1-2 p-l-50">

							</div>
						</div>
					</div>
				</div>
				<div class="uk-form-row">
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">

						</div>
						<div class="uk-width-medium-1-6 p-l-50">
							<label>Sub Total 1</label>
							<input type="text" class="md-input" name="sub_total1" value="{{($lead->sub_total1 > 0) ? $lead->sub_total1 : 0}}" />
						</div>
					</div>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">

						</div>
						<div class="uk-width-medium-1-6 p-l-50">
							<label>Discount(%)</label>
							<input type="text" class="md-input" name="discount" value="{{($lead->discount > 0) ? $lead->discount : 0}}" />
						</div>
					</div>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">

						</div>
						<div class="uk-width-medium-1-6 p-l-50">
							<label>Discount Amount</label>
							<input type="text" class="md-input" name="discount_amount" value="{{($lead->discount_amount > 0) ? $lead->discount_amount : 0}}" />
						</div>
					</div>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">

						</div>
						<div class="uk-width-medium-1-6 p-l-50">
							<label>Sub Total 2</label>
							<input type="text" class="md-input" name="sub_total2" value="{{($lead->sub_total2 > 0) ? $lead->sub_total2 : 0}}" />
						</div>
					</div>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">

						</div>
						<div class="uk-width-medium-1-6 p-l-50">
							<label>IGST</label>
							<input type="text" class="md-input" name="igst" value="{{($lead->IGST_amount > 0) ? $lead->IGST_amount : 0}}" />
						</div>
					</div>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">

						</div>
						<div class="uk-width-medium-1-6 p-l-50">
							<label>Total Payble Amount</label>
							<input type="text" class="md-input" name="total_amount" value="{{($lead->total_amount > 0) ? $lead->total_amount : 0}}" />
						</div>
					</div>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-2 p-r-50 md-p-t-16">

						</div>
						<div class="uk-width-medium-1-6 p-l-50">
							<button type="button" class="md-btn md-btn-primary save_confirm_lead" style="">Update</button>
						</div>
					</div>
				</div>
			</form>
        </div>
    </div>

</div>
<div id="accomodation" style="display: none;">
	<div class="uk-form-row">
		<div class="uk-grid" data-uk-grid-margin>
			<div class="uk-width-medium-1-4 p-r-50 md-p-t-16">
				<select class="md-input accomodation-drop-down" name="accomodation[]" data-uk-tooltip="{pos:'bottom'}" title="Select Type">
					<option value="">Select Accomodation</option>
					@foreach(get_accomodation() as $key => $value)
					<option value="{{$key}}">{{$value}}</option>
					@endforeach
				</select>
			</div>
			<div class="uk-width-medium-1-2 p-l-50">

			</div>
		</div>
	</div>
	<div class="uk-form-row accomodation-data">

	</div>
	<div class="uk-grid customer-row" data-uk-grid-margin>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<button type="button" class="md-btn md-btn-primary tmp-remove-room" style="">Remove</button>
	</div>
	</div>
</div>
<style>
	.error-border{border: 1px solid red !important;}
	.customer-row > div{width: 8% !important;}
	.customer-row > div:not(.uk-row-first){padding-left: 5px !important;}
</style>
@endsection

@section('javascript')
<script src="{{env('APP_URL')}}public/admin_js/lead.js"></script>
@endsection