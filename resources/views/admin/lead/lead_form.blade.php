<style type="text/css" media="screen">
    .table-border td, .table-border th {
      border: 1px solid #ddd;
      padding: 8px;
    }
</style>
<!-- <script>
    var cityData = <?= $cities_for_holidays ?>;
</script> -->
<form action="{{ route('lead.store') }}" name="lead_frm" id="lead_frm" class="" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ isset($lead->id) ? $lead->id : '' }}" />
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <select class="md-input" id="customer_type" name="customer_type" data-uk-tooltip="{pos:'bottom'}" title="Select Customer Type...">
                            <option value="">Select Customer...</option>
                            @if(isset($customer_type))
                            @foreach($customer_type as $c_type)
                            <option value="{{ $c_type->typeId }}" {{ (isset($lead->customer_type_id) && $lead->customer_type_id == $c_type->typeId) ? 'selected' : '' }}>{{ $c_type->name }}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('customer_type'))<small class="text-danger">{{ $errors->first('customer_type') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select class="md-input" id="intrested_in" name="intrested_in" data-uk-tooltip="{pos:'bottom'}" title="Select Intrested In...">
                            <option value="">Select Interested In...</option>
                            @if(isset($intrested_ins))
                            @foreach($intrested_ins as $in_key => $intrested_in)
                            <option value="{{ $in_key }}" {{ (isset($lead->interested_in) && $lead->interested_in == $in_key) ? 'selected' : '' }}>{{ $intrested_in }}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('intrested_in'))<small class="text-danger">{{ $errors->first('intrested_in') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <select class="md-input" id="lead_source" name="lead_source" data-uk-tooltip="{pos:'bottom'}" title="Select Lead Source..">
                            <option value="">Select Lead Source...</option>
                            @if(isset($lead_sources))
                            @foreach($lead_sources as $led_key => $lead_source)
                            <option value="{{ $led_key }}" {{ (isset($lead->lead_source) && $lead->lead_source == $led_key) ? 'selected' : '' }}>{{ $lead_source }}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('lead_source'))<small class="text-danger">{{ $errors->first('lead_source') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50" id="uk_dp_1">
                        <label for="uk_dp_1">Interested Date</label>
                        <input class="md-input" type="text" name="intrested_date" value="{{ isset($lead->interested_date) ? date('d F Y', strtotime($lead->interested_date)) : '' }}" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 md-p-t-16">
                        <label>Name</label>
                        <input type="text" class="md-input" name="lead_name" value="{{ isset($lead->name) ? $lead->name : '' }}" />
                        @if($errors->has('lead_name'))<small class="text-danger">{{ $errors->first('lead_name') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select class="md-input" id="domestic_international" name="domestic_international" data-uk-tooltip="{pos:'bottom'}" title="Select Domestic/International...">
                            <option value="">Select Domestic/International...</option>
                            @if(isset($lead_places_type))
                            @foreach($lead_places_type as $place_key => $lead_plac)
                            <option value="{{ $place_key }}" {{ (isset($lead->domestic_international) && $lead->domestic_international == $place_key) ? 'selected' : '' }}>{{ $lead_plac }}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('domestic_international'))<small class="text-danger">{{ $errors->first('domestic_international') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 md-p-t-16">
                        <label>Email ID</label>
                        <input type="email" class="md-input" name="email_id" value="{{ isset($lead->email) ? $lead->email : '' }}" />
                        @if($errors->has('email_id'))<small class="text-danger">{{ $errors->first('email_id') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select class="md-input" name="country_id" id="country_id" data-uk-tooltip="{pos:'bottom'}" title="Select Country...">
                            <option value="" selected>Choose Country</option>
                            @if(isset($countrys))
                            @foreach($countrys as $country)
                            <option value="{{ $country->country_id }}" {{ (isset($lead->country_id) && $lead->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('country_id'))<small class="text-danger">{{ $errors->first('country_id') }}</small>@endif
                    </div>
                    <?php /*
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select class="md-input" id="group_customised" name="group_customised" data-uk-tooltip="{pos:'bottom'}" title="Select Group Tour / Customised Tour...">
                            <option value="">Select Tour Type...</option>
                            @if(isset($group_customised))
                            @foreach($group_customised as $group_key => $group_name)
                            <option value="{{ $group_key }}" {{ (isset($lead->group_customised) && $lead->group_customised == $group_key) ? 'selected' : '' }}>{{ $group_name }}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('group_customised'))<small class="text-danger">{{ $errors->first('group_customised') }}</small>@endif
                    </div> */ ?>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>Phone No</label>
                        <input type="text" class="md-input" name="phone_no" value="{{ isset($lead->phone) ? $lead->phone : '' }}" />
                        @if($errors->has('phone_no'))<small class="text-danger">{{ $errors->first('phone_no') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select class="md-input" name="state_id" id="state_id" data-uk-tooltip="{pos:'bottom'}" title="Select State...">
                            <option value="" selected>Choose State</option>
                            @if(isset($states))
                            @foreach($states as $state)
                            <option value="{{ $state->state_id }}" {{ (isset($lead->state_id) && $lead->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('state_id'))<small class="text-danger">{{ $errors->first('state_id') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>No of Adults</label>
                        <input type="text" class="md-input" name="no_adults" value="{{ isset($lead->adult) ? $lead->adult : '' }}" />
                        @if($errors->has('no_adults'))<small class="text-danger">{{ $errors->first('no_adults') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select class="md-input" name="city_id" id="city_id" data-uk-tooltip="{pos:'bottom'}" title="Select City...">
                            <option value="" selected>Choose City</option>
                            @if(isset($cities))
                            @foreach($cities as $city)
                            <option value="{{ $city->city_id }}" {{ (isset($lead->city_id) && $lead->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('city_id'))<small class="text-danger">{{ $errors->first('city_id') }}</small>@endif
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>No of Childrens (3 to 12 Years)</label>
                        <input type="text" class="md-input" name="no_child" value="{{ isset($lead->child) ? $lead->child : '' }}" />
                        @if($errors->has('no_child'))<small class="text-danger">{{ $errors->first('no_child') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <label>Pincode</label>
                        <input type="text" class="md-input" name="pin_code" value="{{ isset($lead->pincode) ? $lead->pincode : '' }}" />
                        @if($errors->has('pin_code'))<small class="text-danger">{{ $errors->first('pin_code') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>No of Infants (Below 3 Years)</label>
                        <input type="text" class="md-input" name="no_infants" value="{{ isset($lead->infant) ? $lead->infant : '' }}" />
                        @if($errors->has('no_infants'))<small class="text-danger">{{ $errors->first('no_infants') }}</small>@endif
                    </div>
                    
                </div>
            </div>
        </div>
        <?php /* ?>
        @if (\Auth::user()->role_id == 1 && Request::route('id') OR (\Auth::user()->role_id == 2 && Request::route('id')))
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <select class="md-input" name="agent_id[]" id="agent_id" data-uk-tooltip="{pos:'bottom'}" title="Select Agent..." multiple="" data-md-select2>
                            <option value="">Choose Agent</option>
                            @if(isset($manager_users))
                            @php
                            if(isset($lead->assign_to))
                            $assign_arr = explode(',',$lead->assign_to);
                            @endphp
                            @foreach($manager_users as $m_user)
                            <option value="{{ $m_user->id }}" {{ (isset($lead->assign_to) && in_array($m_user->id,$assign_arr)) ? 'selected' : '' }}>{{ $m_user->first_name .' '. $m_user->last_name}}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('agent_id'))<small class="text-danger">{{ $errors->first('agent_id') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
        @endif

        <?php */ ?>
		<div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 common_fields">
                        <label>From City</label>
                        <input type="text" class="md-input autocomplete_lead ui-autocomplete-input valid" name="from_city" value="<?= (!empty($from_city->city_name)) ? $from_city->city_name : ""; ?>" />
                        <input type="hidden" value="{{ isset($lead->from_city_id) ? $lead->from_city_id : '' }}" name="from_city_id">
                        @if($errors->has('from_city'))<small class="text-danger">{{ $errors->first('from_city') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50 common_fields">
                        <label>To City</label>
                        <input type="text" class="md-input autocomplete_lead ui-autocomplete-input valid" name="to_city" value="<?= (!empty($to_city->city_name)) ? $to_city->city_name : ""; ?>" />
                        <input type="hidden" value="{{ isset($lead->to_city_id) ? $lead->to_city_id : '' }}" name="to_city_id">
                        @if($errors->has('to_city'))<small class="text-danger">{{ $errors->first('to_city') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
		<div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 common_fields">
                        <label>Deprature Date</label>
						<input class="md-input" type="text" name="deprature_date" value="{{ isset($lead->deprature_date) ? date('d F Y', strtotime($lead->deprature_date)) : '' }}" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                        @if($errors->has('deprature_date'))<small class="text-danger">{{ $errors->first('deprature_date') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50 common_fields">
                        <label>Return Date</label>
                        <input class="md-input" type="text" name="return_date" value="{{ isset($lead->return_date) ? date('d F Y', strtotime($lead->return_date)) : '' }}" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                        @if($errors->has('return_date'))<small class="text-danger">{{ $errors->first('return_date') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
		<div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 common_fields">
                        <select class="md-input" name="flight_class" id="flight_class" data-uk-tooltip="{pos:'bottom'}" title="Select flight class">
                            <option value="" selected>Choose flight class</option>
                            @foreach(get_flight_class() as $key => $flight_clas)
                            <option value="{{ $key }}" {{ (isset($lead->flight_class) && $lead->flight_class == $key) ? 'selected' : '' }}>{{ $flight_clas}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('flight_class'))<small class="text-danger">{{ $errors->first('flight_class') }}</small>@endif
                    </div>
                    
                    <div class="uk-width-medium-1-2 p-l-50 common_fields">
                        <label>Flight Type</label>
						<div>
							<span class="icheck-inline">
								<input type="radio" name="flight_type" value="One Way" data-md-icheck {{ (isset($lead->flight_type) && $lead->flight_type == "One Way") ? 'checked' : '' }} />
									   <label class="inline-label">One Way</label>
							</span>
							<span class="icheck-inline">
								<input type="radio" name="flight_type" value="Round Trip" data-md-icheck {{ (isset($lead->flight_type) && $lead->flight_type == "Round Trip") ? 'checked' : '' }}/>
									   <label class="inline-label">Round Trip</label>
							</span>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 md-p-t-16 common_fields">
                        <label>Destination City</label>
                        <input type="text" class="md-input autocomplete_lead ui-autocomplete-input valid" name="destination_city" value="<?= (!empty($destination_city->city_name)) ? $destination_city->city_name : ""; ?>" />
                        <input type="hidden" value="{{ isset($lead->destination_city_id) ? $lead->destination_city_id : '' }}" name="destination_city_id">
                        @if($errors->has('destination_city'))<small class="text-danger">{{ $errors->first('destination_city') }}</small>@endif
                    </div>
                    
                    <div class="uk-width-medium-1-2 p-l-50 common_fields">
                        <label>Number Of Nights</label> 
                        <input type="text" class="md-input" name="number_of_nights" value="<?= (!empty($lead->number_of_nights)) ? $lead->number_of_nights : ""; ?>" />
                        @if($errors->has('number_of_nights'))<small class="text-danger">{{ $errors->first('number_of_nights') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
		<div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 common_fields" id="departure_city_classs">
                        <label>Departure City</label>
                        <input type="text" class="md-input autocomplete_lead ui-autocomplete-input valid" name="departure_city" value="<?= (!empty($departure_city->city_name)) ? $departure_city->city_name : ""; ?>" />
                        <input type="hidden" value="{{ isset($lead->departure_city_id) ? $lead->departure_city_id : '' }}" name="departure_city_id">
                        @if($errors->has('departure_city'))<small class="text-danger">{{ $errors->first('departure_city') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50 common_fields">
                        <select class="md-input" name="travel_type" id="travel_type" data-uk-tooltip="{pos:'bottom'}" title="Select travel type">
                            <option value="" selected>Choose travel type</option>
                            @foreach(get_travel_type() as $key => $travel_type)
                            <option value="{{ $key }}" {{ (isset($lead->travel_type) && $lead->travel_type == $key) ? 'selected' : '' }}>{{ $travel_type}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('travel_type'))<small class="text-danger">{{ $errors->first('travel_type') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
		<div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 common_fields">
                        <label>Check In Date</label>
						<input class="md-input" type="text" name="check_in_date" value="{{ isset($lead->check_in_date) ? date('d F Y', strtotime($lead->check_in_date)) : '' }}" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                        @if($errors->has('check_in_date'))<small class="text-danger">{{ $errors->first('check_in_date') }}</small>@endif
                    </div>
                    
                </div>
            </div>
        </div>
		<div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 common_fields">
                        <select class="md-input" name="visa_type" id="visa_type" data-uk-tooltip="{pos:'bottom'}" title="Select visa type">
                            <option value="" selected>Choose visa type</option>
                            @foreach(get_visa_type() as $key => $visa_type)
                            <option value="{{ $key }}" {{ (isset($lead->visa_type) && $lead->visa_type == $key) ? 'selected' : '' }}>{{ $visa_type}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('visa_type'))<small class="text-danger">{{ $errors->first('visa_type') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50 common_fields">
                        <select class="md-input" name="number_of_entries" id="number_of_entries" data-uk-tooltip="{pos:'bottom'}" title="Select number of entries">
                            <option value="" selected>Choose number of entries</option>
                            @foreach(get_number_of_entries() as $key => $number_of_entries)
                            <option value="{{ $key }}" {{ (isset($lead->number_of_entries) && $lead->number_of_entries == $key) ? 'selected' : '' }}>{{ $number_of_entries}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('number_of_entries'))<small class="text-danger">{{ $errors->first('number_of_entries') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
		<div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50 common_fields">
                        <label>Travel Date</label>
                        <input class="md-input" type="text" name="travel_date" value="{{ isset($lead->deprature_date) ? date('d F Y', strtotime($lead->deprature_date)) : '' }}" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                        @if($errors->has('travel_date'))<small class="text-danger">{{ $errors->first('travel_date') }}</small>@endif
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50 common_fields">
                        <label>Duration Days</label>
                        <input type="text" class="md-input" name="duration_days" value="{{ isset($lead->duration_days) ? $lead->duration_days : '' }}" />
                        @if($errors->has('duration_days'))<small class="text-danger">{{ $errors->first('duration_days') }}</small>@endif
                    </div>
                </div>
            </div>
        </div>
        <?php //if(isset($lead->interested_in) && $lead->interested_in==7){?>
        <div class="uk-width-medium-1-12 holidays-information" style="display: none;">
            <div class="uk-form-row">
                <div class="uk-grid uk-margin-medium-top" data-uk-grid-margin>
                    <div class="uk-width-medium-1-12">
                        <h2 class="heading_a">Destination Details</h2>
                    </div>
                </div>
                <div class="uk-grid uk-margin-medium-top" data-uk-grid-margin>
                    <div class="uk-width-medium-1-12">
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-1">
                                    <div class="uk-width-1-1">
                                        <table class="uk-table table-border">
                                            <thead>
                                                <tr>
                                                    <td width="50%">Destination</td>
                                                    <td width="30%">Number of Nights</td>
                                                    <td width="20%">Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if( isset($holiday_destination_details) && count($holiday_destination_details)>0)
                                                @foreach($holiday_destination_details as $key=>$holiday)
                                                    <tr class="destination_row">
                                                        <td>
                                                            <input type="text" name="destination[{{$key}}][city_name]" class="md-input autocomplete"
                                                            value="{{(isset($holiday->destinationCity->city_name) ? $holiday->destinationCity->city_name :'')}}"
                                                            placeholder="Search by city name">
                                                            <input type="hidden" name="destination[{{$key}}][city_id]" value="{{(isset($holiday->destination_city_id) ? $holiday->destination_city_id : '')}}">
                                                        </td>
                                                        <td>
                                                            <input type="number" name="destination[{{$key}}][number_of_nights]" class="md-input onlynumber" value="{{(isset($holiday->number_of_nights) ? $holiday->number_of_nights : '')}}" min="1" >
                                                        </td>
                                                        <td>
                                                            <a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus" data-field="quant[1]">delete</i></a>    
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @endif
                                                    <tr class="add_btn_tr">
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                             <a href="javaScript:void(0);" class="btnSectionClone destination_add_btn" data-type="plus" data-field="quant[1]"><i class="material-icons md-24">add_box</i></a> 
                                                        </td>
                                                    </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12 holidays-information" style="display: none;">
            <div class="uk-form-row">
                <div class="uk-grid uk-margin-medium-top" data-uk-grid-margin>
                    <div class="uk-width-medium-1-12">
                        <h2 class="heading_a">Activity Details</h2>
                    </div>
                </div>
                <div class="uk-grid uk-margin-medium-top" data-uk-grid-margin>
                    <div class="uk-width-medium-1-12">
                        <div class="uk-form-row">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-1">
                                    <div class="uk-width-1-1">
                                        <table class="uk-table table-border">
                                            <thead>
                                                <tr>
                                                    <td width="50%">Country</td>
                                                    <td width="30%">Activity</td>
                                                    <td width="20%">Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if( isset($holiday_activity_details) && count($holiday_activity_details)>0)
                                                @foreach($holiday_activity_details as $key=>$activity)
                                                    <tr>
                                                        <td>
                                                            <select id="country_id_activity" class="md-input country_id_activity edit_country_id" data-id="{{ $key }}" name=" activiti_details['city'][]">
                                                            <option value="" selected>Choose Country</option>
                                                                @if(isset($countrys))
                                                                @foreach($countrys as $country)
                                                                <option value="{{ $country->country_id }}" {{ (isset($activity->country_id) && $activity->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </td>
                                                        <td>
                                                        <?php /*
                                                            <input type="hidden" class="activity_id_{{ $key }}" value="{{$activity->activiti_id}}">
                                                            */ ?>
                                                            <input type="hidden" class="activity_id_{{ $key }}" value="{{(isset($activity->activiti_id) ? $activity->activiti_id :'')}}">
                                                            <select class="md-input tour_activity edit_tour_activity_{{ $key }}" name=" activiti_details['activiti'][]">
                                                                <option value="" selected>Choose Activity</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <a href="javaScript:void(0);" class="btnSectionRemove destination_delete_btn"><i class="material-icons md-24" data-type="plus" data-field="quant[1]">delete</i></a>  
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @endif
                                                    <tr class="append_new_row">
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <a href="javaScript:void(0);" class="btnSectionClone add_tour_activites" data-type="plus" data-field="quant[1]"><i class="material-icons md-24">add_box</i></a> 
                                                        </td>
                                                    </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php //} ?>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>More Details</label>
                        <textarea cols="30" rows="4" class="md-input" name="notes" id="notes">{{ (isset($lead->notes)) ? $lead->notes : '' }}</textarea>  
                        <small class="text-danger">{{ $errors->first('notes') }}</small>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <hr class="form_hr">
    <div class="uk-grid uk-margin-medium-top uk-text-right">
        <div class="uk-width-1-1">
            <a href="{{ route('lead.index') }}" class="md-btn md-btn-danger">Cancel</a>
            <button type="submit" class="md-btn md-btn-primary">Save</button>
        </div>
    </div>
</form>
<script>
	$(document).ready(function () {
        $('#lead_frm').validate(
            // {
            //     rules: {
            //        // "pin_code": "required",
            //        // "activiti_details['city']": "required",
            //     },
            //     messages: {
            //         // "pin_code": "This Field is require",
            //         // "activiti_details['city']": "This Field is require"
            //     },
            //     errorPlacement: function (error, element) {
            //         if (element.attr("type") == "radio") {
            //             error.insertBefore(element);
            //         } else {
            //             error.insertAfter($(element).parents('div.md-input-wrapper'));
            //         }
            //     },
            //     submitHandler: function (form) {
            //         form.submit();
            //     }
            // }
        );

        var destination = $('input[name^="destination"]');
        destination.filter('input[name$="[city_name]"]').each(function() {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "This city name field is require"
                }
            });
        });

        var city_id = $('input[name^="destination"]');
        city_id.filter('input[name$="[city_id]"]').each(function() {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "This city id field is require"
                }
            });
        });
        
        var number_of_nights = $('input[name^="destination"]');
        number_of_nights.filter('input[name$="[number_of_nights]"]').each(function() {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "This number of nights field is require"
                }
            });
        });
        
        $('.country_id_activity').each(function() {
            $(this).rules("add", 
                {
                    required: true,
                    messages: {
                        required: "This Country field is require",
                    }
                });
        });
        $('.tour_activity').each(function() {
            $(this).rules("add", 
                {
                    required: true,
                    messages: {
                        required: "This Activity field is require",
                    }
                });
        });
        

        // $('.country_id_activity').each(function() {
        //     $name = $(this).attr('name');
        //     $("input[name="+$name+"]").rules("add", 
        //         {
        //             required: true,
        //             messages: {
        //                 required: "This Country field is require",
        //             }
        //         });
        // });
        // $('.tour_activity').each(function() {
        //     $name = $(this).attr('name');
        //     $("input[name="+$name+"]").rules("add", 
        //         {
        //             required: true,
        //             messages: {
        //                 required: "This Country field is require",
        //             }
        //         });
        // });

        // $('input.country_id_activity').each(function() {
        //     $(this).rules("add", 
        //         {
        //             required: true,
        //             messages: {
        //                 required: "This Country field is require",
        //             }
        //         });
        // });



        // $('input.tour_activity').each(function() {
        //     $(this).rules("add", 
        //         {
        //             required: true,
        //             messages: {
        //                 required: "This Activity field is require",
        //             }
        //         });
        // });


        // $('.tour_activity').each(function() {
        //      $name = $(this).attr('name');
        //     $("input[name="+$name+"]").rules("add", 
        //         {
        //             required: true,
        //             messages: {
        //                 required: "This Activity field is require",
        //             }
        //         });
        // });


        // $("form#lead_frm").validate({
        //     rules: {
        //        "activiti_details['city']": "required",
        //        "activiti_details['activiti']": "required",
        //     },
        //     messages: {
        //         "activiti_details['city']": "This Field is require",
        //         "activiti_details['activiti']": "This Field is require",
        //     },
        //     errorPlacement: function (error, element) {
        //         if (element.attr("type") == "radio") {
        //             error.insertBefore(element);
        //         } else {
        //             error.insertAfter($(element).parents('div.md-input-wrapper'));
        //         }
        //     },
        //     submitHandler: function (form) {
        //         form.submit();
        //     }
        // });


		$(".common_fields").closest(".uk-width-medium-1-12").hide();
		$(".common_fields").hide();
		$("#intrested_in").change(function () {
			common_field($(this).val());
		});
		setTimeout(function(){ common_field($("#intrested_in").val()); }, 1000);
		function common_field(val) {
			$(".common_fields").closest(".uk-width-medium-1-12").hide();
			$(".common_fields").hide();
			if (val == 2) {
                $("input[name=from_city]").closest(".uk-width-medium-1-12").show();
                $("input[name=from_city]").closest(".common_fields").show();
                
                $("input[name=to_city]").closest(".uk-width-medium-1-12").show();
                $("input[name=to_city]").closest(".common_fields").show();
                
				// $("input[name=from_city]").closest(".common_fields").show();
				// $("input[name=from_city]").closest(".uk-width-medium-1-12").show();
				// $("input[name=to_city]").closest(".uk-width-medium-1-12").show();
				// $("input[name=to_city]").closest(".common_fields").show();
				$("select[name=flight_class]").closest(".uk-width-medium-1-12").show();
				$("select[name=flight_class]").closest(".common_fields").show();
				$("input[name=deprature_date]").closest(".uk-width-medium-1-12").show();
				$("input[name=deprature_date]").closest(".common_fields").show();
				$("input[name=return_date]").closest(".uk-width-medium-1-12").show();
				$("input[name=return_date]").closest(".common_fields").show();
				$("input[name=flight_type]").closest(".uk-width-medium-1-12").show();
				$("input[name=flight_type]").closest(".common_fields").show();
			} else if (val == 3) {
                $("input[name=number_of_nights]").closest(".uk-width-medium-1-12").show();
                $("input[name=number_of_nights]").closest(".common_fields").show();
				$("input[name=destination_city]").closest(".uk-width-medium-1-12").show();
				$("input[name=destination_city]").closest(".common_fields").show();
				$("input[name=deprature_date]").closest(".uk-width-medium-1-12").show();
				$("input[name=deprature_date]").closest(".common_fields").show();
				$("input[name=return_date]").closest(".uk-width-medium-1-12").show();
				$("input[name=return_date]").closest(".common_fields").show();
			} else if (val == 7) {
                $("input[name=departure_city]").closest(".uk-width-medium-1-12").show();
                $("input[name=departure_city]").closest(".common_fields").show();
                //$("#departure_city_classs").removeClass('uk-width-medium-1-2 p-l-50 common_fields');
                //$("#departure_city_classs").addClass('uk-width-medium-1-2 p-r-50 common_fields');
				$("input[name=check_in_date]").closest(".uk-width-medium-1-12").show();
				$("input[name=check_in_date]").closest(".common_fields").show();
				$("select[name=travel_type]").closest(".uk-width-medium-1-12").show();
				$("select[name=travel_type]").closest(".common_fields").show();
			} else if (val == 8) {
				$("input[name=from_city]").closest(".common_fields").show();
				$("input[name=from_city]").closest(".uk-width-medium-1-12").show();
				$("input[name=to_city]").closest(".uk-width-medium-1-12").show();
				$("input[name=to_city]").closest(".common_fields").show();
				$("input[name=travel_date]").closest(".uk-width-medium-1-12").show();
				$("input[name=travel_date]").closest(".common_fields").show();
                $("input[name=duration_days]").closest(".uk-width-medium-1-12").show();
                $("input[name=duration_days]").closest(".common_fields").show();
				$("input[name=travel_date]").closest(".uk-width-medium-1-12").show();
				$("input[name=travel_date]").closest(".common_fields").show();
				$("select[name=visa_type]").closest(".uk-width-medium-1-12").show();
				$("select[name=visa_type]").closest(".common_fields").show();
				$("select[name=number_of_entries]").closest(".uk-width-medium-1-12").show();
				$("select[name=number_of_entries]").closest(".common_fields").show();
			}
            $(window).trigger("resize");
		}
        $(".autocomplete_lead").autocomplete({
            source: cityData,
            minLength: 3,
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(this).closest('.common_fields').find("input:hidden").val(ui.item.value);
            }
        });
	});
</script>
