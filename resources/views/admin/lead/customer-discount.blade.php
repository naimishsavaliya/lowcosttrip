@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>
    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('customerdiscount.store') }}" name="discount_frm" id="discount_frm" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="lead_id" id="lead_id" value="{{ isset($lead->id) ? $lead->id : '' }}" />
                    <?php /*
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Discount Amount <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input onlynumber" name="discount_amount" id="discount_amount"/>
                                        <small class="text-danger">{{ $errors->first('discount_amount') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Discount Note <span class="required-lbl">*</span></label>
                                        <textarea cols="30" rows="4" class="md-input" name="discount_notes" id="discount_notes"></textarea>
                                        <small class="text-danger">{{ $errors->first('discount_notes') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    */ ?>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                       <label>Discount Title <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="discount_title" id="discount_title"/>
                                        <small class="text-danger">{{ $errors->first('discount_title') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Discount Amount <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input onlynumber" name="discount_amount" id="discount_amount"/>
                                        <small class="text-danger">{{ $errors->first('discount_amount') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1 p-r-50">
                                        <label>Discount Note <span class="required-lbl">*</span></label>
                                        <textarea cols="30" rows="4" class="md-input" name="discount_notes" id="discount_notes"></textarea>
                                        <small class="text-danger">{{ $errors->first('discount_notes') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{route('lead.lead_detail', $lead->id)}}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
    $(".onlynumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("form#discount_frm").validate({
        rules: {
            discount_title: "required",
            discount_amount: "required",
            discount_notes: "required"
        },
        messages: {
            discount_title: "Title is required.",
            discount_amount:"Amount is required.",
            discount_notes: "Note is required."
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});

</script>
@endsection