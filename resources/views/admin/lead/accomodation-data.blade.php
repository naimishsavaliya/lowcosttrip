@for($i = 0;$i<$total_adult;$i++)
<div class="uk-grid customer-row" data-uk-grid-margin>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input required_field" name="customer[{{$total_element}}][{{$i}}][name]" value="" placeholder="Name"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$total_element}}][{{$i}}][birth_date]" value="" placeholder="Birth Date"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$total_element}}][{{$i}}][anniversary_date]" value="" placeholder="Anniversary Date"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" name="customer[{{$total_element}}][{{$i}}][mobile_no]" value="" placeholder="Mobile No"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" name="customer[{{$total_element}}][{{$i}}][email]" value="" placeholder="Email ID"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" name="customer[{{$total_element}}][{{$i}}][passport_no]" value="" placeholder="Passport No"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$total_element}}][{{$i}}][passport_issue_date]" value="" placeholder="Issue Date"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" name="customer[{{$total_element}}][{{$i}}][passport_issue_place]" value="" placeholder="Issue Place"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$total_element}}][{{$i}}][passport_exp_date]" value="" placeholder="Passport Exp. Date"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<select class="md-input" name="customer[{{$total_element}}][{{$i}}][gender]" data-uk-tooltip="{pos:'bottom'}" title="Select Gender">
			<option value="">Gender</option>
			@foreach(get_gender() as $key => $value)
			<option value="{{$key}}">{{$value}}</option>
			@endforeach
		</select>
	</div>
	@if(($total_adult == 1 && $i==0) || $i>0)
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input amount_txt required_field" name="customer[{{$total_element}}][{{$i}}][amount]" value="{{$cost[$i]}}" placeholder="Amount"/>
	</div>
	@endif
</div>
@endfor

@for($i = 0;$i<$total_child;$i++)
<div class="uk-grid customer-row" data-uk-grid-margin>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input required_field" name="customer[{{$total_element}}][{{$i+$total_adult}}][name]" value="" placeholder="Name"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$total_element}}][{{$i+$total_adult}}][birth_date]" value="" placeholder="Birth Date"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$total_element}}][{{$i+$total_adult}}][anniversary_date]" value="" placeholder="Anniversary Date"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" name="customer[{{$total_element}}][{{$i+$total_adult}}][mobile_no]" value="" placeholder="Mobile No"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" name="customer[{{$total_element}}][{{$i+$total_adult}}][email]" value="" placeholder="Email ID"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" name="customer[{{$total_element}}][{{$i+$total_adult}}][passport_no]" value="" placeholder="Passport No"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$total_element}}][{{$i+$total_adult}}][passport_issue_date]" value="" placeholder="Issue Date"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" name="customer[{{$total_element}}][{{$i+$total_adult}}][passport_issue_place]" value="" placeholder="Issue Place"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$total_element}}][{{$i+$total_adult}}][passport_exp_date]" value="" placeholder="Passport Exp. Date"/>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<select class="md-input" name="customer[{{$total_element}}][{{$i+$total_adult}}][gender]" data-uk-tooltip="{pos:'bottom'}" title="Select Gender">
			<option value="">Gender</option>
			@foreach(get_gender() as $key => $value)
			<option value="{{$key}}">{{$value}}</option>
			@endforeach
		</select>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<select class="md-input child_age_group" name="customer[{{$total_element}}][{{$i+$total_adult}}][child_age_group]" data-uk-tooltip="{pos:'bottom'}" title="Select Age">
			<option value="">Select Child Age</option>
			@foreach(get_child_age_group() as $key => $value)
			<option amount="{{$child_cost[$key]}}" value="{{$key}}">{{$value}}</option>
			@endforeach
		</select>
	</div>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<input type="text" class="md-input amount_txt required_field" name="customer[{{$total_element}}][{{$i+$total_adult}}][amount]" value="" placeholder="Amount"/>
	</div>
</div>
@endfor