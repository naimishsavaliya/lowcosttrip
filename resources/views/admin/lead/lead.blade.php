@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}lead/add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table" id="lead_tbl">
                <thead>
                    <tr>
                        <th style="width:25px;">Lead ID</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Email ID</th>
                        <th>Phone No</th>
                        <th>Interested In</th>
                        <!-- <th>Lead Type</th> -->
                        <th>Lead Source</th>
                        <th>Assign To</th>
                        <!-- <th>Confirmed By</th> -->
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <?php /*
                <tfoot>
                    <tr>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Lead ID" type="text" class="md-input" colPos="0">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td></td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Name" type="text" class="md-input" colPos="2">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Email ID" type="text" class="md-input" colPos="3">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Phone No" type="text" class="md-input" colPos="4">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <select class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Intrested In..." colPos="5">
                                <option value="">Select Interested In...</option>
                                @if(isset($intrested_ins))
                                @foreach($intrested_ins as $in_key => $intrested_in)
                                <option value="{{ $in_key }}">{{ $intrested_in }}</option>
                                @endforeach
                                @endif
                            </select>
                        </td>
                        <td>
                            <select class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Domestic/International..." colPos="5">
                                <option value="">Select Domestic/International...</option>
                                @if(isset($lead_places_type))
                                @foreach($lead_places_type as $place_key => $lead_plac)
                                <option value="{{ $place_key }}">{{ $lead_plac }}</option>
                                @endforeach
                                @endif
                            </select>
                        </td>
                        <td>
                            <select class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Lead Source.." colPos="6">
                                <option value="">Select Lead Source...</option>
                                @if(isset($lead_sources))
                                @foreach($lead_sources as $led_key => $lead_source)
                                <option value="{{ $led_key }}">{{ $lead_source }}</option>
                                @endforeach
                                @endif
                            </select>
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Confirmed By" type="text" class="md-input" colPos="7">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <select class="md-input uk-form-width-small" colPos="8">
                                <option value="">Select...</option>
                                @if(isset($lead_status))
                                @foreach($lead_status as $lead_sts)
                                <option value="{{ $lead_sts->typeId }}">{{ $lead_sts->name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </td>
                        <td>

                        </td>
                    </tr>
                </tfoot>
                */?>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!--<div class="uk-modal" id="update_status" >-->
<div id="update_status" class="uk-modal uk-hide uk-fade" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999;">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Update Status </h3>
        </div>
        <input type="hidden" value="" id="change-status-row-id" />
        <select id="lead_status_change_id" class="md-input uk-width-large-1-2 p-t-4" data-uk-tooltip="{pos:'bottom'}" title="Select Status...">
            <option value="0">Select Status...</option>
            @if(isset($lead_status))
            @foreach($lead_status as $lead_sts)
            <option value="{{ $lead_sts->typeId }}">{{ $lead_sts->name }}</option>
            @endforeach
            @endif
        </select>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close" id="close-statud-dialog">Close</button>
            <button type="button" id="status-dlg-btn" class="md-btn md-btn-flat md-btn-flat-success">Update</button>
        </div>
    </div>
</div>


<div id="assign-to" class="uk-modal" >
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Assign To</h3>
        </div>
        <div class="">
            <div class="control-group">
                <input type="hidden" value="" id="user_lead_id" />
                <select id="assign_to_user" class="md-input">
                    <option value="">Select User</option>
                    @if(isset($admin_user))
                    @foreach($admin_user as $user_info)
                    <option value="{{ $user_info->id }}">{{ $user_info->first_name.' '.$user_info->last_name }}</option>
                    @endforeach
                    @endif
                </select>
               
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close" id="assign-to-dlg-btn">Submit</button> 
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function () {

        var homebannerTable = $('#lead_tbl').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            searching: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            order: [[ 0, "desc" ]],
//            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
//            "pagingType": "full_numbers", //full,full_numbers
            order: [[ 1, "desc" ]],
            "ajax": {
                "url": "{{ route('lead.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof homebannerTable != 'undefined' && homebannerTable.hasOwnProperty('settings')) {
                        homebannerTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                 {"data": "lead_id", "name": "lead_id"},
                 {"data": "created_at", "name": "created_at"},
                // {"data": "id", "name": "id"},
                {"data": "name", "name": "name"},
                {"data": "email", "name": "email"},
                {"data": "phone", "name": "phone"},
                {"data": "interested_in", "name": "interested_in"},
                // {"data": "domestic_international", "name": "domestic_international"},
                {"data": "lead_source", "name": "lead_source"},
                {"data": "assign_to_user", "name": "assign_to_user"},
                // {"data": "confirm_by", "name": "confirm_by"},
                {"data": "status_name", "name": "status_name"},
                {"data": "action", 'sortable': false, "render": function (data, type, row) {
                        var lead_view = "{{ route('lead.lead_detail', ':id') }}";
                        var confirm = "{{ route('lead.confirm', ':id') }}";
                        var lead_delete = "{{ route('lead.delete', ':id') }}";
                        lead_view = lead_view.replace(':id', row.id);
                        confirm = confirm.replace(':id', row.id);
                        lead_delete = lead_delete.replace(':id', row.id);

                        var action_str = '';
                        action_str += '<div class="uk-button-dropdown" data-uk-dropdown="{pos:\'bottom-right\'}">';
                        action_str += '<button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>';
                        action_str += '<div class="uk-dropdown">';
                        action_str += '<ul class="uk-nav uk-nav-dropdown">';
//                        action_str += '<li><a href="' + lead_view + '" title="View Agent" class="">View</a></li>';
                        action_str += '<li><a href="' + lead_view + '" title="Edit Agent" class="">Edit</a></li>';
                        action_str += '<li><a href="' + confirm + '" title="Confirm" class="">Confirm</a></li>';
                        action_str += '<li><a class="change-status-btn" row-id="' + row.id + '" data-id="' + row.status_id + '" data-uk-modal="{target:\'#update_status\'}"> Update Status</a></li>';

                        action_str += '<li><a class="change-assign-to" row-id="' + row.id + '" data-id="' + row.assign_to + '" data-uk-modal="{target:\'#assign-to\'}"> Assign to</a></li>';

                        action_str += '<li><a href="' + lead_view + '#quotation" title="Quotation" class="">Quotation</a></li>';
                        action_str += '<li><a href="' + lead_view + '#followup" title="Follow-Up Notes" class="">Follow-Up Notes</a></li>';
                        action_str += '<li><a href="' + lead_view + '#documents" title="Manage Documents" class="">Manage Documents</a></li>';
                        if(row.interested_in == 'Holiday' || row.interested_in == 'Hotel' || row.interested_in == 'Tour'){
                            action_str += '<li><a href="' + lead_view + '#voucher" title="Voucher" class="">Voucher</a></li>';
                        }
//                        action_str += '<li><a href="' + lead_view + '#subscription" title="Manage Subscription" class="">Manage Subscription</a></li>';
//                        action_str += '<li><a href="' + lead_view + '#invoice" title="Manage Invoice" class="">Manage Invoice</a></li>';
//                        action_str += '<li><a href="' + lead_view + '#ledger" title="Manage Ledger" class="">Manage Ledger</a></li>';
                        action_str += '<li><a href="' + lead_delete + '" title="Delete Lead" class="delete-notes" data-id="' + row.id + '">Delete</a></li>';

                        
                        action_str += '</ul>';
                        action_str += '</div>';
                        action_str += '</div>';
                        return action_str;
                    },
                },
            ]
        });


        $(homebannerTable.table().container()).on('keyup', 'tfoot input', function () {
            homebannerTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(homebannerTable.table().container()).on('change', 'tfoot select', function () {
            homebannerTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(document.body).on('click', '.change-status-btn', function () {
            var $obj = $(this);
            var row_id = $obj.attr('row-id');
            var status = $obj.attr('data-id');
            $('input#change-status-row-id').val(row_id);
            if (status) {
                $("select#lead_status_change_id option[value='" + status + "']").attr("selected", "selected");
            }
        });
        $(document.body).on('click', '.change-assign-to', function () {
            $("#assign_to_user").val($("#assign_to_user option:first").val());
            var $obj = $(this);
            var row_id = $obj.attr('row-id');
            var status = $obj.attr('data-id');
            $('input#user_lead_id').val(row_id);
            if (status) {
                $("select#assign_to_user option[value='" + status + "']").attr("selected", "selected");
            }
        });

        $(document.body).on('click', '#status-dlg-btn', function () {
            var $obj = $(this);
            var btn_txt = $obj.text();
            var row_id = $('#change-status-row-id').val();
            var status = $('select#lead_status_change_id').val();
            if (row_id == false && status == false) {
                return false;
            }
            $.ajax({
                type: 'get',
                url: '{{ route("change_status")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table": "lead",
                    "id": row_id,
                    "column_name": "id",
                    "column_update": "status_id",
                    "status": status
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $obj.prop('disabled', true);
                    $obj.text('Wait...');
                },
                success: function (data, textStatus, jqXHR) {
                    $obj.text(btn_txt);
                    $obj.prop('disabled', false);
                    if (data.success) {
                        homebannerTable.draw();
                    }
                    $('select#lead_status_change_id option:selected', this).removeAttr('selected');
                    $('#close-statud-dialog').trigger('click');
                    showNotify(data.msg_status, data.message);
                }
            });
        });

        $(document.body).on('click', '#assign-to-dlg-btn', function () {
            var $obj = $(this);
            var btn_txt = $obj.text();
            var row_id = $('#user_lead_id').val();
            var assign_user_id = $('select#assign_to_user').val();
            if (row_id == false && assign_user_id == false) {
                return false;
            }
            $.ajax({
                type: 'get',
                url: '{{ route("assign.to.user")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table": "lead",
                    "id": row_id,
                    "column_name": "id",
                    "column_update": "assign_to",
                    "assign_user_id": assign_user_id
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $obj.prop('disabled', true);
                    $obj.text('Wait...');
                },
                success: function (data, textStatus, jqXHR) {
                    $obj.text(btn_txt);
                    $obj.prop('disabled', false);
                    if (data.success) {
                        homebannerTable.draw();
                    }
                    $('select#lead_status_change_id option:selected', this).removeAttr('selected');
                    $('#close-statud-dialog').trigger('click');
                    showNotify(data.msg_status, data.message);
                }
            });
        });

        $(document.body).on('click', '.delete-notes', function () {
            var crm = confirm("Are you sure to delete?");
            if (crm) {
                var $obj = $(this);
                var lead_id = $obj.attr('data-id');
                var delete_url = $obj.attr('href');
                $.ajax({
                    type: 'get',
                    url: delete_url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {

                    }, success: function (data, textStatus, jqXHR) {
                        if (data.success) {
                            homebannerTable.draw();
                        }
                        showNotify(data.msg_status, data.message);
                    }
                });
            }
            return false;
        });

    });
</script>
@endsection