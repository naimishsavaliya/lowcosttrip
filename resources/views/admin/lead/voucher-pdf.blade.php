
<style>
    .invoice-box {
        padding: 0px 10px 10px 10px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 15px;
        line-height: 18px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #000000;
    }

    .invoice-box table {
        width: 100%;
        /*line-height: inherit;*/
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-text{
        font-size:25px;
        text-align: center;
    }
    .address{
        font-size:10px;
        padding: 7px;
        text-align: left !important;
        vertical-align: top !important;
        line-height: 23px;
    }
    .declaration{
        border:1px solid #000; 
        padding:10px 0px; 
        font-size:10px;
        border-spacing: 0;
    }
    .info-td{
        border-bottom :1px solid #c6c6c6; 
        padding:9px; 
        font-size:13px; 
        border-spacing: 0;
        color: #5e5b5b;
        vertical-align: bottom;
        line-height: 20px;
    }
    .hotel-td{
        border :1px solid #c6c6c6; 
        padding:9px; 
        font-size:13px; 
        border-spacing: 0;
        color: #5e5b5b;
        vertical-align: bottom;
        line-height: 20px;
    }
    
    .hotel{
        width: 100%;
        text-align: left;
        padding:10px 0px;
        font-size:13px;
        border-spacing: 0;
        float: left;
        width: 100%;
    }
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta charset="utf-8">
        <title>A simple, clean, and responsive HTML invoice template</title> 
    </head>
    <body>
        <div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td>
                        <table>
                            <tr>
                                <td class="title" width="55%">
                                <a  target="_blank" href="http://lowcosttrip.com/"><img src="https://www.lowcosttrip.com/public/new-theme/images/logo.jpg"></a>
                                </td>
                                <td class="address"  width="30%">
                                    <strong>Low Cost Trip</strong><br>
                                    IQ Comtech Pvt Ltd ,<br>
                                    1205,Sai Indu Tower ,<br>
                                    Lbs marg, Bhandup, Mumbai - 78. 
                                </td>
                                <td class="address"  width="15%">
                                    <p style="line-height: 10px;">Phone No:</p>
                                    <p style="line-height: 10px;">8182838485</p>
                                    <p style="line-height: 10px;padding-top: 9px;">Email:</p>
                                    <p style="line-height: 10px;">info@lowcosttrip.com</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-top:1px solid #DDD; padding: 25px;">
                        <table>
                            <tr> 
                                <td class="invoice-text">
                                    HOTEL VOUCHER
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td> 
                        <table>
                            <tr>
                                <td>
                                    <table style="line-height: 8px;"   cellspacing="0">
                                        <tr>
                                            <td class="info-td" width="35%" ><strong>Guest Name</strong></td>
                                            <td class="info-td" width="65%" >: {{$lead_package->guest_name}}</td>
                                        </tr>

                                        <tr>
                                            <td class="info-td" width="35%"><strong>Package</strong></td>
                                            <td class="info-td" width="65%">: {{$lead_package->package_id}}</td>
                                        </tr>

                                        <tr>
                                            <td class="info-td" width="35%"><strong>No. of Guest</strong></td>
                                            <td class="info-td" width="65%">: {{$lead_package->no_of_guests}}</td>
                                        </tr>
                                        <tr>
                                            <td class="info-td" width="35%"><strong>Meal Plan</strong></td>
                                            <td class="info-td" width="65%">: {{$lead_package->meal_plan}}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10%"></td>
                                <td>
                                    <table style="line-height: 8px;" cellspacing="0"> 
                                        <tr>
                                            <td class="info-td" width="50%" ><strong>Book Through</strong></td>
                                            <td class="info-td" width="50%" >: {{$lead_package->book_through}}</td>
                                        </tr>

                                        <tr>
                                            <td class="info-td" width="50%"><strong>Voucher Issued By</strong></td>
                                            <td class="info-td" width="50%">: {{$lead_package->issued_by}}</td>
                                        </tr>

                                        <tr>
                                            <td class="info-td" width="50%"><strong>Voucher Number</strong></td>
                                            <td class="info-td" width="50%">: {{$lead_package->voucher_number}}</td>
                                        </tr>
                                        <tr>
                                            <td class="info-td" width="50%"><strong>Voucher Issue Date</strong></td>
                                            <td class="info-td" width="50%">: {{date('d M Y', strtotime($lead_package->issue_date))}}</td>
                                        </tr>
                                    </table> 
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table cellspacing="0">
                            <tr>
                                <td style="font-size: 20px; padding-top: 20px; color: #5e5b5b;"> Hotel Details </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php foreach ($lead_package_meta as $k => $meta) { ?>
                    <tr>
                        <td>
                            <table cellspacing="0">
                                <tr>
                                    <td width="30%" class="hotel-td">Rooming details</td>
                                    <td width="70%" class="hotel-td">{{$meta->rooming_details}}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="hotel-td">Hotel details</td>
                                    <td width="70%" class="hotel-td"><strong>{{$meta->hotel_name}}</strong><br>{{$meta->location}}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="hotel-td">Dates of stay</td>
                                    <td width="70%" class="hotel-td">
                                        <strong>Check-In:</strong> {{date('d M Y gi A', strtotime($meta->check_in))}}<br>
                                        <strong>Check-Out:</strong> {{date('d M Y gi A', strtotime($meta->check_out))}}<br>
                                        <strong>Number Of Nights:</strong> {{$meta->number_of_nights}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%" class="hotel-td">Inclusions</td>
                                    <?php $inclusions =  $meta->inclusions; ?>
                                    <td width="70%" class="hotel-td"><?php echo nl2br($inclusions); ?></td>
                                </tr>
                                <tr>
                                    <td width="30%" class="hotel-td">Exclusions</td>
                                    <?php $exclusions =  $meta->exclusions; ?>
                                    <td width="70%" class="hotel-td"><?php echo nl2br($exclusions); ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                      
                <?php } ?>
                <tr>
                    <td>
                        <table cellspacing="0">
                            <tr>
                                <td style="font-size: 20px; padding-top: 20px; color: #5e5b5b;"> Emergency Contacts </td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td>
                        <table cellspacing="0">
                            <tr>
                                <td width="27%" class="hotel-td"><strong>Contact Person Name 1:</strong></td>
                                <td width="23%" class="hotel-td">{{$lead_package->em_name_1}}</td>
                                <td width="27%" class="hotel-td"><strong>Contact Person Number 1:</strong></td>
                                <td width="23%" class="hotel-td">{{$lead_package->em_number_1}}</td>
                            </tr>
                            <tr>
                                <td class="hotel-td"><strong>Contact Person Name 2:</strong></td>
                                <td class="hotel-td">{{$lead_package->em_name_2}}</td>
                                <td class="hotel-td"><strong>Contact Person Number 2:</strong></td>
                                <td class="hotel-td">{{$lead_package->em_number_2}}</td>
                            </tr>
                            <tr>
                                <td class="hotel-td"><strong>Contact Person Name 3:</strong></td>
                                <td class="hotel-td">{{$lead_package->em_name_3}}</td>
                                <td class="hotel-td"><strong>Contact Person Number 3:</strong></td>
                                <td class="hotel-td">{{$lead_package->em_number_3}}</td>
                            </tr>
                               
                        </table>
                    </td>
                </tr>
                @if(isset($itinarary))
                     <tr>
                        <td>
                            <table cellspacing="0">
                                <tr>
                                     <td style="font-size: 20px; padding-top: 20px; color: #5e5b5b;"> 
                                        Daywise Itinerary Plan:
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @foreach($itinarary as $iti_key => $iti_value)
                    <tr>
                        <td>
                            <table cellspacing="0">
                                <tr>
                                    <td width="20%" class="hotel-td">
                                        <img width="125px" height="125px" src="{{ env('APP_PUBLIC_URL') }}/uploads/itinerary/{{$iti_value->image}}">
                                    </td>
                                    <td width="80%" class="hotel-td">
                                        <strong>Day {{ $iti_value->day .' '. $iti_value->title }}</strong><br>
                                        {{ $iti_value->description }}
                                    </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @endforeach
                @endif

                <tr>
                    <td>
                        <table cellspacing="0">
                            <tr>
                                <td style="font-size: 15px; padding-top: 20px; color: #5e5b5b;">
                                    <strong> Note : </strong>{{$lead_package->note}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>



               
               
            </table>

        </div>
    </body>
</html>