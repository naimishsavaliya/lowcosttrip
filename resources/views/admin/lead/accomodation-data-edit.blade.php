@foreach($accomodation as $j => $val)
<div class="uk-form-row">
	<div class="uk-grid" data-uk-grid-margin>
		<div class="uk-width-medium-1-4 p-r-50 md-p-t-16">
			<select class="md-input accomodation-drop-down required_field" name="accomodation[]" data-uk-tooltip="{pos:'bottom'}" title="Select Type">
				<option value="">Select Accomodation</option>
				@foreach(get_accomodation() as $key => $value)
				<option value="{{$key}}" {{($key == $val->accomodation) ? "selected='selected'" : ""}}>{{$value}}</option>
				@endforeach
			</select>
		</div>
		<div class="uk-width-medium-1-2 p-l-50">

		</div>
	</div>
</div>
<div class="uk-form-row accomodation-data">

	@foreach($val->customer as $i => $custmer)
	<div class="uk-grid customer-row" data-uk-grid-margin>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input required_field" name="customer[{{$j}}][{{$i}}][name]" value="{{$custmer->name}}" placeholder="Name"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$j}}][{{$i}}][birth_date]" value="{{(!empty($custmer->birth_date)) ? date("d-m-Y", strtotime($custmer->birth_date)) : ""}}" placeholder="Birth Date"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$j}}][{{$i}}][anniversary_date]" value="{{(!empty($custmer->anniversary_date)) ? date("d-m-Y", strtotime($custmer->anniversary_date)) : ""}}" placeholder="Anniversary Date"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input" name="customer[{{$j}}][{{$i}}][mobile_no]" value="{{$custmer->mobile_no}}" placeholder="Mobile No"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input" name="customer[{{$j}}][{{$i}}][email]" value="{{$custmer->email}}" placeholder="Email ID"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input" name="customer[{{$j}}][{{$i}}][passport_no]" value="{{$custmer->passport_no}}" placeholder="Passport No"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$j}}][{{$i}}][passport_issue_date]" value="{{(!empty($custmer->issue_date)) ? date("d-m-Y", strtotime($custmer->issue_date)) : ""}}" placeholder="Issue Date"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input" name="customer[{{$j}}][{{$i}}][passport_issue_place]" value="{{$custmer->issue_place}}" placeholder="Issue Place"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input" data-uk-datepicker="{format:'DD-MM-YYYY'}" name="customer[{{$j}}][{{$i}}][passport_exp_date]" value="{{(!empty($custmer->passport_exp_date)) ? date("d-m-Y", strtotime($custmer->passport_exp_date)) : ""}}" placeholder="Passport Exp. Date"/>
		</div>
		<div class="uk-width-medium-1-6 md-p-t-16">
			<select class="md-input" name="customer[{{$j}}][{{$i}}][gender]" data-uk-tooltip="{pos:'bottom'}" title="Select Gender">
				<option value="">Gender</option>
				@foreach(get_gender() as $key => $value)
				<option value="{{$key}}" {{($key == $custmer->gender) ? "selected='selected'" : ""}}>{{$value}}</option>
				@endforeach
			</select>
		</div>
		@if(!empty($custmer->child_age_group) && $custmer->child_age_group > 0)
		<div class="uk-width-medium-1-6 md-p-t-16">
			<select class="md-input child_age_group" name="customer[{{$j}}][{{$i}}][child_age_group]" data-uk-tooltip="{pos:'bottom'}" title="Select Age">
				<option value="">Select Child Age</option>
				@foreach(get_child_age_group() as $key => $value)
				<option amount="" {{($key == $custmer->child_age_group) ? "selected='selected'" : ""}} value="{{$key}}">{{$value}}</option>
				@endforeach
			</select>
		</div>
		@endif
		@if($custmer->amount > 0 || $custmer->child_age_group > 0)
		<div class="uk-width-medium-1-6 md-p-t-16">
			<input type="text" class="md-input amount_txt required_field" name="customer[{{$j}}][{{$i}}][amount]" value="{{$custmer->amount}}" placeholder="Amount"/>
		</div>
		@endif
	</div>
	@endforeach
</div>
<div class="uk-grid customer-row" data-uk-grid-margin>
	<div class="uk-width-medium-1-6 md-p-t-16">
		<button type="button" class="md-btn md-btn-primary added-remove-room" acc_id="{{$val->id}}">Remove</button>
	</div>
</div>
@endforeach