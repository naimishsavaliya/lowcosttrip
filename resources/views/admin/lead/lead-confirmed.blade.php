@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Manage Reservation</span></li>
        </ul>
<!--        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('supplier.add') }}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>-->
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dt_default">
                <thead>
                    <tr>
                        <th>Lead ID</th>
                        <th>Customer Name</th>
                        <th class="hidden-phone">contact</th>
                        <th class="hidden-phone">Particular</th>
                        <!--<th class="hidden-phone">Title</th>-->
                        <th class="hidden-phone">Booking DATETIME</th>
                        <th class="hidden-phone">Actual DATETIME</th>
                        <th class="hidden-phone">Amount</th>
                        <th class="hidden-phone">Supplier</th>
                        <th class="hidden-phone">Agent</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?= $i; ?>254</td>
                            <td>Customer <?= $i; ?></td>
                            <td>payment@lead.com <br /><?php echo "982". rand(111111, 999999); ?></td>
                            <td>Hotel/Tour/Flight/Activity</td>
                            <!--<td>Title <?= $i; ?></td>-->
                            <td><?= $i; ?> Jul 18 12:35</td>
                            <td><?= $i; ?> Jul 18 12:35</td>
                            <td><?= $i; ?>,244</td>
                            <td>Supplier <?= $i; ?></td>
                            <td>Agent <?= $i; ?></td>
                            <td ><span class="uk-badge uk-badge-success">Active</span></td>
                            <td>
                                <a href="javascript:;" title="View Payment" class=""><i class="md-icon material-icons">visibility</i></a> 
                                <a href="javascript:;" title="Edit Payment" class=""><i class="md-icon material-icons">edit</i></a> 
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
