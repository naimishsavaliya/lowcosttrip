@extends('layouts.admin')
@section('content')


<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="{{ route('sector.index')}}">Sector</a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:,;">Add New</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->   
                <div class="widget">
                    <!--                    <div class="widget-title">
                                            <h4><i class="icon-reorder"></i>Sector</h4>
                                        </div>-->
                    <div class="widget-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{ route('sector.save') }}" class="" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ isset($sector->id) ? $sector->id : '' }}" />
                            <div class="control-group span6 m-l-0">
                                <label class="control-label">Sector Name</label>
                                <div class="controls">
                                    <input type="text" class="span12" name="name" value="{{ isset($sector->name) ? $sector->name : '' }}" />
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group span8 m-l-0">
                                <label class="control-label" >Description</label>
                                <div class="controls">
                                    <textarea type="text" class="span12 editor" rows="6" name="description">{{ isset($sector->description) ? $sector->description : '' }}</textarea>
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group span4 m-l-0">
                                <label class="control-label">Category</label>
                                <div class="controls">
                                    <select class="span12 chosen" data-placeholder="Sector Category" tabindex="1" name="category_id">
                                        <option value=""></option>
                                        <option value="1" {{ (isset($sector->category_id) && $sector->category_id == 1) ? 'selected' : '' }}>Domestic</option>
                                        <option value="2" {{ (isset($sector->category_id) && $sector->category_id == 2)  ? 'selected' : '' }}>International</option>
                                    </select>
                                </div>
                            </div>

                            <div class="clear clearfix"></div>
                            <div class="control-group span4 m-l-0">
                                <label class="control-label">Country</label>
                                <div class="controls">
                                    <select class="span12 chosen" data-placeholder="Choose a Country" name="country_id" tabindex="1">
                                        <option value=""></option>
                                        <option value="1" {{ (isset($sector->country_id) && $sector->country_id == 1) ? 'selected' : '' }}>India</option>
                                        <option value="2" {{ (isset($sector->country_id) && $sector->country_id == 2) ? 'selected' : '' }}>US</option>
                                        <option value="3" {{ (isset($sector->country_id) && $sector->country_id == 3) ? 'selected' : '' }}>UK</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group span4 m-l-0">
                                <label class="control-label">State</label>
                                <div class="controls">
                                    <select class="span12 chosen" data-placeholder="Choose a State" name="state_id" tabindex="1">
                                        <option value=""></option>
                                        <option value="1" {{ (isset($sector->state_id) && $sector->state_id == 1) ? 'selected' : '' }}>Gujarat</option>
                                        <option value="2" {{ (isset($sector->state_id) && $sector->state_id == 1) ? 'selected' : '' }}>Maharashtra</option>
                                        <option value="3" {{ (isset($sector->state_id) && $sector->state_id == 1) ? 'selected' : '' }}>Punjab</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group span4 m-l-0">
                                <label class="control-label">City</label>
                                <div class="controls">
                                    <select class="span12 chosen" data-placeholder="Choose a City" name="city_id" tabindex="1">
                                        <option value=""></option>
                                        <option value="1" {{ (isset($sector->city_id) && $sector->city_id == 1) ? 'selected' : '' }}>Mumbai</option>
                                        <option value="2" {{ (isset($sector->city_id) && $sector->city_id == 1) ? 'selected' : '' }}>Pune</option>
                                        <option value="3" {{ (isset($sector->city_id) && $sector->city_id == 1) ? 'selected' : '' }}>Thane</option>
                                    </select>
                                </div>
                            </div>

                            <div class="clear clearfix"></div>
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                    <label class="radio">
                                        <input type="radio" value="1" name="status" {{ (isset($sector->status) && $sector->status == 1) ? 'checked' : ''  }} {{ (!isset($sector)) ? 'checked' : '' }} />
                                        Active
                                    </label>
                                    <label class="radio">
                                        <input type="radio" value="0" name="status" {{ (isset($sector->status) && $sector->status == 0) ? 'checked' : '' }} />
                                        In Active
                                    </label>
                                </div>
                            </div>


                            <div class = "form-actions">
                                <a href="{{ env('ADMIN_URL')}}agent/subscription" class="btn btn-danger pull-right">Cancel</a>
                                <button type="submit" class="btn btn-success pull-right m-r-5">Submit</button>
                            </div>
                        </form>
                        <!--END FORM-->
                    </div>
                </div>
                <!--END SAMPLE FORM widget-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" >
    $(function () {
        $('.editor').wysihtml5({"font-styles": false});
    })
</script>
<link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

@endsection