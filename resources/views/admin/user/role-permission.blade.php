@extends('layouts.theme')
@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form method="POST" action="{{ env('ADMIN_URL')}}role/update/{{$role->id}}" accept-charset="UTF-8" class="">
                    {{ csrf_field() }}
                    <h3 class="heading_a">{{ isset($role->name) ? $role->name : 'Role Name' }}</h3>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    @foreach($permissions as $key => $perm)								
                                    <?php
                                    $per_found = null;

                                    if (isset($role)) {
                                        $per_found = $role->hasPermissionTo($perm->name);
                                    }

                                    if (isset($user)) {
                                        $per_found = $user->hasDirectPermission($perm->name);
                                    }
                                    ?>
                                    <div class="uk-width-medium-1-6">
                                        <?php
                                        $label_id = "permission_" . $key;
                                        ?>
                                        {!! Form::checkbox("permissions[]", $perm->name, $per_found,  array('data-md-icheck','id' => $label_id )) !!} 
                                        <label for="{{ $label_id }}" class="inline-label {{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }}">{{ $perm->name }}</label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>


                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('role.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                </div>
                        </div>-->

        </div>
    </div>
</div>

@endsection