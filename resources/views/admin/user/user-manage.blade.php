@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ route('user.store') }}" name="user_frm" id="user_frm" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($users[0]->user_id) ? $users[0]->user_id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>First Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="first_name" value="{{ isset($users[0]->first_name) ? $users[0]->first_name : old('first_name') }}" />
                                        <small class="text-danger">{{ $errors->first('first_name') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>Last Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="last_name" value="{{ isset($users[0]->last_name) ? $users[0]->last_name : old('last_name') }}" />
                                        <small class="text-danger">{{ $errors->first('last_name') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>E-mail <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="email" value="{{ isset($users[0]->email) ? $users[0]->email : old('email') }}" {{ isset($users[0]->email) ? 'readonly' : '' }} />
                                               <small class="text-danger">{{ $errors->first('email') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(isset($users[0]->id) == false)
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Password <span class="required-lbl">*</span></label>
                                        <input type="password" class="md-input" value="{{ old('password')}}" name="password" id="password" />
                                        <small class="text-danger">{{ $errors->first('password') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Confirm Password <span class="required-lbl">*</span></label>
                                        <input type="password" class="md-input" value="{{ old('confirm_password')}}" name="confirm_password" id="confirm_password" />
                                        <small class="text-danger">{{ $errors->first('confirm_password') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Mobile Number <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="phone_no" value="{{ isset($users[0]->phone_no) ? $users[0]->phone_no :  old('phone_no') }}" />
                                        <small class="text-danger">{{ $errors->first('phone_no') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Landline Number <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="landline_no" value="{{ isset($users[0]->landline_no) ? $users[0]->landline_no : old('landline_no') }}" />
                                        <small class="text-danger">{{ $errors->first('landline_no') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Age</label>
                                        <input type="text" class="md-input" name="age" value="{{ isset($users[0]->age) ? $users[0]->age :old('age') }}" />
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <select id="select_demo_3" class="md-input" name="gender">
                                            <option value="" selected>Choose Gender</option>
                                            <option value="M" {{ (isset($users[0]->gender) && $users[0]->gender == "M" || old('gender') == "M" ) ? 'selected' : '' }}>Male</option>
                                            <option value="F" {{ (isset($users[0]->gender) && $users[0]->gender == "F" || old('gender') == "F" ) ? 'selected' : '' }}>Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Birth Date</label>
                                        <input type="text" class="md-input" name="birth_date" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}" autocomplete="off" value="{{ isset($users[0]->birth_date) ? date('d F Y',strtotime($users[0]->birth_date)) : old('birth_date') }}" />
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <select id="select_demo_3" class="md-input" name="time_zone">
                                            <option value="" selected>Choose Timezone</option>
                                            @if(isset($timezones))
                                            @foreach($timezones as $t=>$zone)
                                            <option value="{{ $t }}" {{ (isset($users[0]->time_zone) && $users[0]->time_zone == $t ||  old('time_zone') == $t) ? 'selected' : '' }}>{{ $zone }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <small class="text-danger">{{ $errors->first('time_zone') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Address <span class="required-lbl">*</span></label>
                                        <textarea cols="30" rows="4" class="md-input" name="address">{{ isset($users[0]->address) ? $users[0]->address : old('address') }}</textarea>
                                        <small class="text-danger">{{ $errors->first('address') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <div class="uk-width-medium-1-12 display-inline-flex">
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <select class="md-input" name="country_id" id="country_id">
                                                    <option value="" selected>Choose Country</option>
                                                    @if(isset($countrys))
                                                    @foreach($countrys as $country)
                                                    <option value="{{ $country->country_id }}" {{ (isset($users[0]->country_id) && $users[0]->country_id == $country->country_id  ||  old('country_id') == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                <small class="text-danger">{{ $errors->first('country_id') }}</small>
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                <select class="md-input" name="state_id" id="state_id">
                                                    <option value="" selected>Choose State</option>
                                                    @if(isset($states))
                                                    @foreach($states as $state)
                                                    <option value="{{ $state->state_id }}" {{ (isset($users[0]->state_id) && $users[0]->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                <small class="text-danger">{{ $errors->first('state_id') }}</small>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-12 p-t-20 display-inline-flex">
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <select class="md-input" name="city_id" id="city_id">
                                                    <option value="" selected>Choose City</option>
                                                    @if(isset($cities))
                                                    @foreach($cities as $city)
                                                    <option value="{{ $city->city_id }}" {{ (isset($users[0]->city_id) && $users[0]->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                <small class="text-danger">{{ $errors->first('city_id') }}</small>
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                <label>Pincode</label>
                                                <input type="text" class="md-input" name="pin_code" value="{{ isset($users[0]->pin_code) ? $users[0]->pin_code : old('pin_code')  }}" />
                                                <small class="text-danger">{{ $errors->first('pin_code') }}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <select id="role_id" class="md-input" name="role_id">
                                            <option value="">Choose Role</option>
                                            <?php foreach ($roles as $role) { ?>
                                                <option value="{{$role['id']}}" {{ (isset($users[0]->role_id) && $users[0]->role_id == $role['id'] || old('role_id') == $role['id']) ? 'selected' : '' }}>{{$role['name']}}</option>
                                            <?php } ?>
                                        </select>
                                        <small class="text-danger">{{ $errors->first('role_id') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50" id="manger-sector-id" style="display:none;">
                                        <select id="sector_id" class="md-input" name="sector_id[]" multiple="" data-md-select2 data-allow-clear="true" data-placeholder="Select Sector..." required="" style="width: 100%;">
                                            <option value="">Choose Sector</option>
                                            @if(isset($sectors))
                                            @php
                                            if(isset($users[0]->sector_id))
                                            $secrot_id_arr = explode(',',$users[0]->sector_id);
                                            @endphp
                                            @foreach($sectors as $sec_key => $sector)
                                            <option value="{{ $sector->id }}" {{ (isset($users[0]->sector_id) && in_array($sector->id,$secrot_id_arr)) ? 'selected' : '' }}>{{ $sector->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <small class="text-danger">{{ $errors->first('time_zone') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1 p-r-50">
                                        <label>Brief Description</label>
                                        <textarea cols="30" rows="2" class="md-input" name="information" rows="2">{{ isset($users[0]->information) ? $users[0]->information : old('information') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6">
                                        <h3 class="heading_a uk-margin-small-bottom">Profile</h3>
                                        <input type="file" id="input-file-a" name="image"  class="dropify" data-default-file="{{  isset($users[0]->image) ?  env('APP_PUBLIC_URL').'uploads/user_profile/'.$users[0]->image : '' }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="status_1" data-md-icheck {{ (isset($users[0]->status) && $users[0]->status == 1 || old('status') == 1) ? 'checked' : ''  }} {{ (!isset($users[0]) ) ? 'checked' : '' }} />
                                                       <label for="status_1" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="status_0" data-md-icheck {{ (isset($users[0]->status) && $users[0]->status == 0 || old('status') == 0) ? 'checked' : ''  }} />
                                                       <label for="status_0" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('status') }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('user.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                </div>
                        </div>-->

        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
// It has the name attribute "user_frm"
    $("form#user_frm").validate({
// Specify validation rules
        rules: {
            first_name: "required",
            last_name: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                required: true,
                equalTo: "#password"
            },
            phone_no: "required",
            landline_no: "required",
            time_zone: "required",
            address: "required",
            country_id: "required",
            state_id: "required",
            city_id: "required",
            pin_code: "required",
            role_id: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            first_name: "Please enter firstname",
            last_name: "Please enter lastname",
            email: "Please enter a valid email address",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please provide a confirm password",
                equalTo: "Enter Confirm Password Same as Password"
            },
            phone_no: "Please enter phone number",
            landline_no: "Please enter landline number",
            time_zone: "Please selet timezone",
            address: "Please enter address",
            country_id: "Please select country id",
            state_id: "Please select state",
            city_id: "Please select city",
            pin_code: "Please enter pin code",
            role_id: "Please select user role",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    $('#country_id').change(function () {
        var country_id = $(this).val();
        var url = '{{ route("state_by_country", ":country_id") }}';
        url = url.replace(':country_id', country_id);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#state_id')
                        .find('option')
                        .remove();
                $('#state_id').append('<option value="">Choose State</option>');
                $.map(data, function (item) {
                    $('#state_id').append('<option value="' + item.state_id + '">' + item.state_name + '</option>');
                });
                $("#state_id").trigger("chosen:updated");
                $("#state_id").trigger("liszt:updated");
            }
        });
    });

    $('#state_id').change(function () {
        var state_id = $(this).val();
        var url = '{{ route("city_by_state", ":state_id") }}';
        url = url.replace(':state_id', state_id);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#city_id')
                        .find('option')
                        .remove();
                $('#city_id').append('<option value="">Choose City</option>');
                $.map(data, function (item) {
                    $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
                });
                $("#city_id").trigger("chosen:updated");
                $("#city_id").trigger("liszt:updated");
            }
        });
    });

    if ($('#role_id').val() == 2) {
        $('#manger-sector-id').show();
    }
    $('#role_id').change(function () {
        var role_id = $(this).val();
        if (role_id == 2) {
            $('#manger-sector-id').show();
        } else {
            $('#manger-sector-id').hide();
        }
    });
});

</script>
@endsection