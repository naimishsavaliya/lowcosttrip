@extends('layouts.admin')
@section('content')
<link href="{{ env('APP_PUBLIC_URL')}}front/css/bootstrap.min.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/components/toastr/calendar.otakita.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/components/toastr/toastr.min.css" rel="stylesheet" />
<style type="text/css">
    .form-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143 !important;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>
<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Hotel Inventory</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                <div class="widget">
                    <div class="widget-body">


                        <div class="row">
                            <div class="col-lg-12">
                                <h1>Room Price Calendar</h1>
                                <hr />
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                Room Setting
                                            </div>

                                            <div class="panel-body" id="roomSetting">
                                                <div class="form-group">
                                                    <label for="room" class="control-label">Room</label>
                                                    <select name="room" id="event_priority" data-placeholder="Select Room..." class="span12 chosen">
                                                        <option value=""></option>
                                                        <option value="default">Delux</option>
                                                        <option value="default">A/C Room</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="status" class="control-label">Price</label>
                                                    <input type="text" value="" class="span12" placeholder="Enter Price..." id="event_title" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="status" class="control-label">Total Rooms</label>
                                                    <input type="text" value="" class="span12" placeholder="Enter Total Rooms..." id="event_title" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="status" class="control-label">Start Date</label>
                                                    <input type="text" value="<?php echo date("d-m-Y", strtotime("-4 months")); ?>" class="span12 date-picker" placeholder="Start Date..." id="event_title" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="status" class="control-label">End Date</label>
                                                    <input type="text" value="<?php echo date('d-m-Y'); ?>" class="span12 date-picker" placeholder="End Date..." id="event_title" />
                                                </div>

                                                <div class="form-group" style="text-align:right">
                                                    <button id="update" class="btn btn-success" disabled>Search</button>
                                                </div>
                                                <hr />
                                                <div class="alert alert-info" role="alert">
                                                    <p><b>Important Note!</b></p>
                                                    <p>Please select the date you want to change.</p>
                                                    <p>You can change the status or price the room for any certain date.</p>
                                                    <p>If you want not to change the price, keep the text price empty.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Room Calendar
                                            </div>
                                            <div class="panel-body" id="calendarData">
                                                <p style="text-align:center">Please select room first.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}front/components/toastr/toastr.min.js"></script>   
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}front/components/toastr/jquery.blockUI.js"></script>   
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}front/components/toastr/calendar.otakita.js"></script>   

<script type="text/javascript">function sessionMessage() {};</script>
<script type="text/javascript">
    $(function () {
        // currentDate format mm-yyyy
        var currentDate = "05-2018";
        var cal;

        $(document).on("click", "#update", function () {
            roomId = $("select[name=room]").val();
            roomStatus = $("select[name=status]").val();
            roomPrice = $("input[name=price]").val();

            var calendarSelected = "";
            $.each($(".calendar-selected"), function (index, value, as) {
                calendarSelected += $(value).attr("data-value") + ",";
            });
            calendarSelected = calendarSelected.replace(/,+$/, "");

            $.ajax({
                url: "{{ env('ADMIN_URL')}}/room/calendar-price/update",
                data: {room_id: roomId, room_status: roomStatus, room_price: roomPrice, calendar_selected: calendarSelected, current_date: currentDate},
                type: "POST",
                success: function (data) {
                    json = jQuery.parseJSON(data);

                    if (json.status == "false") {
                        alert(json.message);
                    } else {
                        cal = new Calendar(json.dataEvent, json.month, json.year);
                        cal.generateHTML();
                        $("#calendarData").html(cal.getHTML());
                    }
                    $("#roomSetting").unblock();
                    $("#calendarData").unblock();
                },
                beforeSend: function () {
                    $("#roomSetting").block({message: null});
                    $("#calendarData").block({message: null});
                }
            });
        });

        $("select[name=room]").change(function () {
            roomId = $(this).val();
            $.ajax({
                url: "{{ env('ADMIN_URL')}}room/calendar-price/get-room",
                data: {room_id: roomId},
                type: "GET",
                success: function (data) {
                    json = jQuery.parseJSON(data);
                    if (json.status == "false") {
                        alert(json.message);
                        $("#calendarData").html("<p style=\"text-align:center\">Please select room first.</p>");
                    } else {
                        $("#update").prop("disabled", false);
                        cal = new Calendar(json.dataEvent, json.month, json.year);
                        cal.generateHTML();
                        $("#calendarData").html(cal.getHTML());
                    }
                    $("#roomSetting").unblock();
                    $("#calendarData").unblock();
                },
                beforeSend: function () {
                    $("#update").prop("disabled", true);
                    $("#roomSetting").block({message: null});
                    $("#calendarData").block({message: null});
                }
            });
        });

        $(document).on("click", ".calendar-nav", function (e) {
            e.preventDefault();
            roomId = $("select[name=room]").val();
            var month = $(this).attr("data-month");
            var year = $(this).attr("data-year");
            $.ajax({
                url: "{{ env('ADMIN_URL')}}room/calendar-price/get-navigate",
                data: {room_id: roomId, month: month, year: year},
                type: "POST",
                success: function (data) {
                    json = jQuery.parseJSON(data);
                    if (json.status == "false") {
                        alert(json.message);
                    } else {
                        currentDate = json.month + "-" + json.year;
                        cal = new Calendar(json.dataEvent, json.month, json.year);
                        cal.generateHTML();
                        $("#calendarData").html(cal.getHTML());
                    }
                    $("#roomSetting").unblock();
                    $("#calendarData").unblock();
                },
                beforeSend: function () {
                    $("#roomSetting").block({message: null});
                    $("#calendarData").block({message: null});
                }
            });
        });
        var isDragging = false;
        var isClicking = false;
        var firstRange = "";
        var secondRange = "";

        $(document).on("mouseover", ".calendar-active", function () {
            $(this)
                    .mousedown(function () {
                        isClicking = true;
                        firstRange = $(this).attr("data-value");
                        $(".calendar-selected").removeClass("calendar-selected");
                        if ($(this).hasClass("calendar-selected")) {
                            $(this).removeClass("calendar-selected");
                        } else {
                            $(this).addClass("calendar-selected");
                        }
                    })
                    .mousemove(function () {
                        if (isClicking) {
                            isDragging = true;
                            if (!$(this).hasClass("calendar-selected")) {
                                $(this).addClass("calendar-selected");
                            }
                        }
                    })

                    .mouseup(function (e) {
                        isDragging = false;
                        isClicking = false;
                    });
        });

        function str_pad(data) {
            var str = "" + data;
            var pad = "00";
            return pad.substring(0, pad.length - str.length) + str;
        }

        function countdown(minute, second) {
            second -= 1;
            if (second < 0) {
                minute -= 1;
                second = 59;
            }
            $("#timeout").html(str_pad(minute) + ":" + str_pad(second));
            setTimeout(function () {
                countdown(minute, second)
            }, 1000);
        }

        countdown(24, 0);
        sessionMessage();
    });
</script>
@endsection
