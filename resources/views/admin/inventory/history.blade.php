@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Hotel Inventory History</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>

        @if(isset($deleted) && $deleted==true)
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong>Customer deleted successfully.</strong>
        </div>
        @endif
        @if(isset($added) && $added==true)
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong>Customer added successfully.</strong>
        </div>
        @endif

        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Hotel Inventory History</h4>
                        <span class="tools">
                            <a href="{{ env('ADMIN_URL')}}supplier/add" class="icon-plus"></a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <table class="table table-striped table-bordered" id="sample_1">
                            <thead>
                                <tr>
                                    <th style="width: 25px;">ID</th>
                                    <th>Hotel Name</th>
                                    <th>No. of rooms</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td>Supplier <?php echo $i; ?></td>
                                        <td><?php echo $i; ?></td>
                                        <td>05-Apr-2018</td>
                                        <td>05-Apr-2018</td>
                                        <td>750</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
            </div>
        </div> 
    </div>
</div>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection

