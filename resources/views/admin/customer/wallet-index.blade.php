@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>
    <?php //dd(Request::route('id'));?>
    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')
            <table class="uk-table" id="wallet_tbl">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Withdrawal</th>
                        <th>Deposit</th>
                        <th>Closing balance</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type = "text/javascript" >
$(function () {
    var wallet = $('#wallet_tbl').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        searching: true,
        pageLength: 20,
        lengthMenu: [ 10, 20, 50, 75, 100 ],
        //order: [[ 0, "desc" ]],
        "ajax": {
            "url": "{{ route('customer.wallet.data', Request::route('id')) }}",
            "dataType": "json",
            "type": "POST",
            "data": {_token: "{{csrf_token()}}"},
            beforeSend: function () {
                if (typeof wallet != 'undefined' && wallet.hasOwnProperty('settings')) {
                    wallet.settings()[0].jqXHR.abort();
                }
            }
        },
        "columns": [
            {data: "created_at", name: "created_at"},
            {data: "debit", name: "debit"},
            {data: "credit", name: "credit"},
            {data: "current_balance", name: "current_balance"}
        ]
    });
    $(wallet.table().container()).on('keyup', 'tfoot input', function () {
        wallet
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });
    $(wallet.table().container()).on('change', 'tfoot select', function () {
        wallet
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

});
</script>
@endsection