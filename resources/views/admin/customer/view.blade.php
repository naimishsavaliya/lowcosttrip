@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
<style type="text/css">
    .user_heading {
        padding: 26px 26px 0 26px !important;
    }
    .uk-sticky-placeholder .uk-tab{background: #1976D2;}
    .user_heading{ padding: 24px 24px 0px 24px;}
    .select2{ width: 100% !important;}
    .uk-tab > li.uk-active > a{color: #FFF;}
    .uk-tab > li.uk-active > a{border-bottom-color: #00071f;}
    .uk-dropdown{width: 200px !important;}
    .select2-container--open{z-index: 9999;}
    .uk-dropdown-shown{min-width: 260px !important;}

</style>
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('customer.index') }}">Customer Name</a></li>
            <li><span>Details</span></li>
        </ul>
    </div>
    <div id="page_content">
        <div id="page_content_inner" class="p-0">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-1-1">
                    <div class="">
                        <div class="user_heading">
                            <div class="user_heading_menu hidden-print">
                                <div class="uk-display-inline-block" data-uk-dropdown="{pos:'left-top'}">
                                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="javascript:;" data-uk-modal="{target:'#update_status'}" ><i class="material-icons ">add</i> Update Status</a></li>
<!--                                            <li><a href="{{ route('agent.notes.add', '1') }}"><i class="material-icons ">add</i> Followup</a></li>
                                            <li><a href="{{ route('agent.document.add', '1') }}"><i class="material-icons ">add</i> Document</a></li>-->
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="user_heading">
                                <div class="user_heading_avatar">
                                    <div class="thumbnail">
                                        <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/avatars/user@2x.png" alt="user avatar"/>
                                    </div>
                                </div>
                                <div class="user_heading_content" style="display:inline-flex;">
                                    <h2 class="heading_b uk-margin-bottom">
                                        <span class="uk-text-truncate">Customer Name</span>
                                        <span class="sub-heading"><strong>ID:</strong>1234</span>
                                        <span class="md-btn md-btn-primary">Active Status</span>
                                    </h2>
                                </div>
                            </div>
                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">Customer Info</a></li>
                                <li class="leads"><a href="#">Leads</a></li>
                                <li class="quotations"><a href="#">Quotations</a></li>
                                <li class="tours"><a href="#">Tours</a></li>
                                <li class="hotels"><a href="#">Hotels</a></li>
                                <li class="activities"><a href="#">Activities</a></li>
                                <li class="flights"><a href="#">Flights</a></li>
                                <li class="payments"><a href="#">Payments</a></li>
                                <li class="history"><a href="#">History</a></li>
                            </ul>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin" style="overflow:inherit;">
                                <li>
                                    <div class="p-t-30">
                                        <form action="{{ route('customer.index') }}" class="" method="get">
                                            <div class="uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-4 p-r-50">
                                                                <label>First Name <span class="required-lbl">*</span></label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-4 p-r-50">
                                                                <label>Last Name <span class="required-lbl">*</span></label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <label>E-mail <span class="required-lbl">*</span></label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label>Contact No</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <label>Alternate No.</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label class="uk-form-label">Send Sms</label>
                                                                <div>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio"  id="male" data-md-icheck name="gender" value="M" />
                                                                        <label for="mal" class="inline-label">Yes</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" id="female" data-md-icheck name="gender" value="F" />
                                                                        <label for="female" class="inline-label">No</label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label>Address</label>
                                                                <textarea cols="30" rows="4" class="md-input"></textarea>
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <div class="uk-width-medium-1-12 display-inline-flex">
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <select id="select_demo_3" class="md-input">
                                                                            <option value="" selected>Choose Country</option>
                                                                            <option value="1">India</option>
                                                                            <option value="2">US</option>
                                                                            <option value="3">UK</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                                        <select id="select_demo_3" class="md-input">
                                                                            <option value="" selected>Choose State</option>
                                                                            <option value="1">India</option>
                                                                            <option value="2">US</option>
                                                                            <option value="3">UK</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="uk-width-medium-1-12 p-t-20 display-inline-flex">
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <select id="select_demo_3" class="md-input">
                                                                            <option value="" selected>Choose City</option>
                                                                            <option value="1">Mumbai</option>
                                                                            <option value="2">Pune</option>
                                                                            <option value="3">Thane</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                                        <label>Pincode</label>
                                                                        <input type="text" class="md-input" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-4 p-r-50">
                                                                <label>GST No</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-4 p-r-50">
                                                                <label>GST Name</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <label>GST Address</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Cutomer Note</label>
                                                                <textarea cols="30" rows="2" class="md-input"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <div>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" name="settings_cache_type" id="settings_cache_file" data-md-icheck checked />
                                                                        <label for="settings_cache_file" class="inline-label">Active</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" name="settings_cache_type" id="settings_cache_mysql" data-md-icheck />
                                                                        <label for="settings_cache_mysql" class="inline-label">In Active</label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="form_hr">
                                            <div class="uk-grid uk-margin-medium-top uk-text-right">
                                                <div class="uk-width-1-1">
                                                    <a href="{{ route('customer.index') }}" class="md-btn md-btn-danger">Cancel</a>
                                                    <button type="submit" class="md-btn md-btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </li> 

                                <li>
                                    <!--Leads-->
                                    <div class="p-t-30">
                                        <table class="uk-table dt_default">
                                            <thead>
                                                <tr>
                                                    <th>Lead ID</th>
                                                    <th>Name</th>
                                                    <th>Email ID</th>
                                                    <th>Phone No</th>
                                                    <th>Interested In</th>
                                                    <th>Lead Type</th>
                                                    <th>Lead Source</th>
                                                    <th>Assign To</th>
                                                    <th>Confirmed By</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td class="pd0"><input class="md-input" type="text" placeholder="Lead ID" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Name" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Email ID" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Phone No" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Interested In" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Lead Type" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Lead Source" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Assign To" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Confirmed By" /></td>
                                                    <td>
                                                        <select id="select_demo_1" class="md-input uk-form-width-small">
                                                            <option value="" disabled selected hidden>Select...</option>
                                                            <option value="a1">Active</option>
                                                            <option value="b1">Pending</option>
                                                        </select>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                                    <tr class="odd gradeX">
                                                        <td><?php echo $i; ?></td>
                                                        <td>Name <?php echo $i; ?></td>
                                                        <td>demo@bcreative.in</td>
                                                        <td>9876543210</td>
                                                        <td class="hidden-phone">Kerala Tour</td>
                                                        <td class="hidden-phone">Tour Package</td>
                                                        <td class="hidden-phone">Web</td>
                                                        <td class="hidden-phone"><?php echo "Agent " . rand(1, 4); ?> <span data-uk-tooltip title="Agent 1, Agent, Agent 3">...</span></td>
                                                        <td class="hidden-phone"><?php echo "Agent " . rand(1, 4); ?></td>
                                                        <td class="hidden-phone">Pending</td>
                                                        <td>
                                                            <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                                <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                                                <div class="uk-dropdown">
                                                                    <ul class="uk-nav uk-nav-dropdown">
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/lead_detail/1"> View</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/add"> Edit</a></li>
                                                                        <li><a data-uk-modal="{target:'#update_status'}"> Update Status</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/lead_detail/1#followup"> Follow Up Notes</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/lead_detail/1#quotation"> Manage Quotation</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/lead_detail/1#hotel"> Manage Hotel Room</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/lead_detail/1#activity"> Book Activity</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/lead_detail/1#flight"> Book Flight</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/lead_detail/1#document"> Upload Document</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}lead/lead_detail/1#history"> View History</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                                <li>
                                    <!--Quotations-->
                                    <div class="p-t-30">
                                        <table class="uk-table dt_default">

                                            <thead>
                                                <tr>
                                                    <th>Lead ID</th>
                                                    <th>Agent</th>
                                                    <th>Created Date</th>
                                                    <th>Quotation Title</th>
                                                    <th>No of Adults</th>
                                                    <th>No of Kids</th>
                                                    <th>Amount</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td class="pd0"><input class="md-input" type="text" placeholder="Lead ID" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Agent" /></td>
                                                    <td><input class="md-input" type="text" id="uk_dp_1" placeholder="Created Date" data-uk-datepicker="{format:'DD/MM/YYYY'}"></td>
                                                    <td><input class="md-input" type="text" placeholder="Quotation Title" /></td>
                                                    <td><input class="md-input" type="text" placeholder="No Of Adult" /></td>
                                                    <td><input class="md-input" type="text" placeholder="No Of Kids" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Amount" /></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                                    <tr class="odd gradeX">
                                                        <td><?php echo $i; ?></td>
                                                        <td>Agent <?php echo $i; ?></td>
                                                        <td>21/09/2017</td>
                                                        <td>lorem tristique aliquet. Phasellus fermentum convallis</td>
                                                        <td class="hidden-phone"><?php echo rand(2, 4); ?></td>
                                                        <td class="hidden-phone"><?php echo rand(0, 2); ?></td>
                                                        <td ><?php echo rand(0, 1); ?></td>
                                                        <td>
                                                            <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                                <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                                                <div class="uk-dropdown">
                                                                    <ul class="uk-nav uk-nav-dropdown">
                                                                        <li><a href=""> View</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}quotation/edit/1"> Edit</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}quotation/delete/1"> Delete</a></li>
                                                                        <li><a href=""> Duplicate</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>

                                <li>
                                    <!--Tours-->
                                    <div class="p-t-30">
                                        <table class="uk-table dt_default">
                                            <thead>
                                                <tr>
                                                    <th>Tour ID</th>
                                                    <th>Tour Code</th>
                                                    <th>Tour Name</th>
                                                    <th>Departure Date</th>
                                                    <th>Duration</th>
                                                    <th>Adult/Child/Infant</th>
                                                    <th>Type</th>
                                                    <th>Agent</th>
                                                    <th>Cost</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td class="pd0"><input class="md-input" type="text" placeholder="Travel ID" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Tour Code" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Tour Name" /></td>
                                                    <td><input class="md-input" type="text" id="uk_dp_1" placeholder="Depature Date" data-uk-datepicker="{format:'DD/MM/YYYY'}"></td>
                                                    <td><input class="md-input" type="text" placeholder="Duration" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Adult/Child/Infant" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Type" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Agent" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Cost" /></td>
                                                    <td>
                                                        <select id="select_demo_1" class="md-input uk-form-width-small">
                                                            <option value="" disabled selected hidden>Select...</option>
                                                            <option value="a1">Active</option>
                                                            <option value="b1">Pending</option>
                                                        </select>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php for ($i = 1; $i <= rand(1, 2); $i++) { ?>
                                                    <tr class="odd gradeX">
                                                        <td><?php echo $i; ?></td>
                                                        <td>KL852</td>
                                                        <td>Kerala 5 Nigh 6 day</td>
                                                        <td>10 July, 2019</td>
                                                        <td>5 Night 6 Day</td>
                                                        <td>2/1/0</td>
                                                        <td>Deluxe</td>
                                                        <td>Agent 1</td>
                                                        <td>50,000</td>
                                                        <td>Pending</td>
                                                        <td>
                                                            <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                                <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                                                <div class="uk-dropdown">
                                                                    <ul class="uk-nav uk-nav-dropdown">
                                                                        <li><a href=""> Cancel Booking</a></li>
                                                                        <li><a href=""> Print Voucher</a></li>
                                                                        <li><a href=""> Email Voucher</a></li>
                                                                        <li><a href=""> View Tour Details</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                                <li>
                                    <!--Hotels-->
                                    <div class="p-t-30">
                                        <table class="uk-table dt_default">
                                            <thead>
                                                <tr>
                                                    <th style="width: 25px;">ID</th>
                                                    <th>Hotel Name</th>
                                                    <th>Location</th>
                                                    <th class="hidden-phone">Email/Mobile</th>
                                                    <th class="hidden-phone">Supplier</th>
                                                    <th class="hidden-phone">Agent</th>
                                                    <th class="hidden-phone">Check In</th>
                                                    <th class="hidden-phone">Check Out</th>
                                                    <th class="hidden-phone">Nights</th>
                                                    <th class="hidden-phone">Rooms</th>
                                                    <th class="hidden-phone">Rate</th>
                                                    <th class="hidden-phone">Total</th>
                                                    <th class="hidden-phone">Stars</th>
                                                    <th class="hidden-phone">Status</th>
                                                    <th style="width: 10%;">Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td class="pd0"><input class="md-input" type="text" placeholder="ID" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Hotel Name" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Location" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Contact" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Supplier" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Agent" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Check In" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Check Out" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Nights" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Rooms" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Rate" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Total" /></td>
                                                    <td></td>
                                                    <td>
                                                        <select id="select_demo_1" class="md-input uk-form-width-small">
                                                            <option value="" disabled selected hidden>Select...</option>
                                                            <option value="a1">Active</option>
                                                            <option value="b1">Pending</option>
                                                        </select>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                                    <tr class="odd gradeX">
                                                        <td><?php echo $i; ?></td>
                                                        <td><a href="{{ env('ADMIN_URL')}}hotel/view/1" title="">Hotel <?php echo $i; ?></a></td>
                                                        <td>City,State<br />Country</td>
                                                        <td>email@gmail.com<br />123456789</td>
                                                        <td>Supplier Name <?= $i ?></td>
                                                        <td>Agent Name <?= $i ?></td>
                                                        <td><?= $i ?>/Jul/2018</td>
                                                        <td><?= $i ?>/Jul/2018</td>
                                                        <td><?= $i ?></td>
                                                        <td><?= $i ?></td>
                                                        <td><?= $i ?>,216</td>
                                                        <td><?= $i ?>,324</td>
                                                        <td class="hidden-phone">
                                                            <span class="rating" style="font-size: 14px;">
                                                                <span class="star"></span>
                                                                <span class="star"></span>
                                                                <span class="star"></span>
                                                                <span class="star"></span>
                                                                <span class="star"></span>
                                                            </span>
                                                        </td>
                                                        <td ><span class="uk-badge uk-badge-success">Active</span></td>
                                                        <td>
                                                            <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                                <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                                                <div class="uk-dropdown">
                                                                    <ul class="uk-nav uk-nav-dropdown">
                                                                        <li><a href=""> View Hotel</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/edit/1"> Edit Hotel</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/room_inventory/1"> View/Update Inventory</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/view_booking/1"> View Booking List</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/view_booking_calendar/1"> View Booking Calendar</a></li>
                                                                        <li><a href=""> Manage Room</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/discount/1"> Manage Discount</a></li>
                                                                        <!--<li><a href="{{ env('ADMIN_URL')}}hotel/delete/1"> Delete</a></li>-->
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </li>
                                <li>
                                    <!--Activities-->
                                    <div class="p-t-30">
                                        <table class="uk-table dt_default">
                                            <thead>
                                                <tr>
                                                    <th style="width: 25px;">ID</th>
                                                    <th>Activities Name</th>
                                                    <th>Location</th>
                                                    <th class="hidden-phone">Email/Mobile</th>
                                                    <th class="hidden-phone">Supplier</th>
                                                    <th class="hidden-phone">Agent</th>
                                                    <th class="hidden-phone">Time</th>
                                                    <th class="hidden-phone">Activitie By</th>
                                                    <th class="hidden-phone">Rate</th>
                                                    <th class="hidden-phone">Total</th>
                                                    <th class="hidden-phone">Status</th>
                                                    <th style="width: 10%;">Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td class="pd0"><input class="md-input" type="text" placeholder="ID" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Activities" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Location" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Contact" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Supplier" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Agent" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Time" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Adult/Child" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Rate" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Total" /></td>
                                                    <td>
                                                        <select id="select_demo_1" class="md-input uk-form-width-small">
                                <option value="" disabled selected hidden>Select...</option>
                                <option value="a1">Active</option>
                                <option value="b1">Pending</option>
                            </select>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                                    <tr class="odd gradeX">
                                                        <td><?php echo $i; ?></td>
                                                        <td>Activitie <?php echo $i; ?></td>
                                                        <td>City,State<br />Country</td>
                                                        <td class="hidden-phone">email@lct.com <br /> 9988776655</td>
                                                        <td class="hidden-phone">Supplier <?= $i ?></td>
                                                        <td class="hidden-phone">Agent <?= $i ?></td>
                                                        <td class="hidden-phone">10:5<?= $i ?></td>
                                                        <td class="hidden-phone">Child</td>
                                                        <td><?= $i ?>,216</td>
                                                        <td><?= $i ?>,324</td>
                                                        <td ><span class="uk-badge uk-badge-success">Active</span></td>
                                                        <td>
                                                            <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                                <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                                                <div class="uk-dropdown">
                                                                    <ul class="uk-nav uk-nav-dropdown">
                                                                        <li><a href=""> View Hotel</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/edit/1"> Edit Hotel</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/room_inventory/1"> View/Update Inventory</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/view_booking/1"> View Booking List</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/view_booking_calendar/1"> View Booking Calendar</a></li>
                                                                        <li><a href=""> Manage Room</a></li>
                                                                        <li><a href="{{ env('ADMIN_URL')}}hotel/discount/1"> Manage Discount</a></li>
                                                                        <!--<li><a href="{{ env('ADMIN_URL')}}hotel/delete/1"> Delete</a></li>-->
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>

                                <li>
                                    <!--Flights-->
                                    <div class="p-t-30">
                                        <table class="uk-table dt_default">

                                            <thead>
                                                <tr>
                                                    <th>Traveler Name</th>
                                                    <th>Supplier</th>
                                                    <th>Agent</th>
                                                    <th>From</th>
                                                    <th>To</th>
                                                    <th>Date & Time</th>
                                                    <th>Flight No.</th>
                                                    <th>PNR No.</th>
                                                    <th>Airline Name</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td class="pd0"><input class="md-input" type="text" placeholder="Travel Name" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Supplier" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Agent" /></td>
                                                    <td><input class="md-input" type="text" placeholder="From" /></td>
                                                    <td><input class="md-input" type="text" placeholder="To" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Date & Time" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Flight No" /></td>
                                                    <td><input class="md-input" type="text" placeholder="PNR No" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Airline Name" /></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php for ($i = 1; $i <= rand(3, 15); $i++) { ?>
                                                    <tr class="odd gradeX">
                                                        <td>Bhavik Shah</td>
                                                        <td>Supplier <?= $i ?></td>
                                                        <td>Agent <?= $i ?></td>
                                                        <td>Ahmedabad</td>
                                                        <td>Delhi</td>
                                                        <td>Nov 10, 2018 10 AM</td>
                                                        <td>FT101</td>
                                                        <td>GJ3456</td>
                                                        <td>Air India</td>
                                                        <td>
                                                            <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                                <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                                                <div class="uk-dropdown">
                                                                    <ul class="uk-nav uk-nav-dropdown">
                                                                        <li><a href=""> Cancel Booking</a></li>
                                                                        <li><a href=""> Print Ticket</a></li>
                                                                        <li><a href=""> Email Ticket</a></li>
                                                                        <li><a href=""> Edit Flight</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                                <li>
                                    <div class="p-t-30">
                                        <table class="uk-table dt_default">
                                            <thead>
                                                <tr>
                                                    <th style="width:5%;">No</th>
                                                    <th style="width:15%;">Agent</th>
                                                    <th style="width:10%;">Date Added</th>
                                                    <th style="width:25%;">Title</th>
                                                    <th style="width:10%;">Debit</th>
                                                    <th style="width:10%;">Credit</th>
                                                    <th style="width:10%;">Balance</th>
                                                    <th style="width:20%;">Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td class="pd0"><input class="md-input" type="text" placeholder="ID" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Agent" /></td>
                                                    <td>
                                                        <div class="uk-input-group">
                                                            <input class="md-input" type="text" id="uk_dp_1" data-uk-datepicker="{format:'DD/MM/YYYY'}" placeholder="Date Added" >
                                                        </div>
                                                    </td>
                                                    <td><input class="md-input" type="text" placeholder="Title" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Debit" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Credit" /></td>
                                                    <td><input class="md-input" type="text" placeholder="Balance" /></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <tr class="odd gradeX">
                                                    <td>1</td>
                                                    <td>Agent 1</td>
                                                    <td>17/03/2019</td>
                                                    <td>Kerala Tour Package</td>
                                                    <td>50,000</td>
                                                    <td>-</td>
                                                    <td>50,000</td>
                                                    <td >
                                                        <a href="javascript:;" title="Receipt" class=""><i class="md-icon material-icons">receipt</i></a> 
                                                        <a href="javascript:;" title="Invoice" class=""><i class="md-icon material-icons">assignment_returned</i></a> 
                                                        <a href="javascript:;" title="Edit notes" class=""><i class="md-icon material-icons">edit</i></a> 
                                                        <a href="javascript:;" onclick="return confirm('Are you sure to delete?');" title="Delete notes" class=""><i class="md-icon material-icons">delete</i></a> 
                                                    </td>
                                                </tr>
                                                <tr class="odd gradeX">
                                                    <td>2</td>
                                                    <td>Agent 1</td>
                                                    <td>17/03/2019</td>
                                                    <td>Customer Payment</td>
                                                    <td>-</td>
                                                    <td>10,000</td>
                                                    <td>40,000</td>
                                                    <td >
                                                        <a href="javascript:;" title="Receipt" class=""><i class="md-icon material-icons">receipt</i></a> 
                                                        <a href="javascript:;" title="Invoice" class=""><i class="md-icon material-icons">assignment_returned</i></a> 
                                                        <a href="javascript:;" title="Edit notes" class=""><i class="md-icon material-icons">edit</i></a> 
                                                        <a href="javascript:;" onclick="return confirm('Are you sure to delete?');" title="Delete notes" class=""><i class="md-icon material-icons">delete</i></a> 
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-grid">
                                        <div class="uk-width-1-1">
                                            <div class="uk-grid" data-uk-grid-margin="">
                                                <div class="uk-width-large-1-6 uk-width-1-1 uk-row-first" style="padding-top: 10px;">
                                                    <h3>Filter History</h3>
                                                </div>
                                                <div class="uk-width-large-2-10 uk-width-1-1" style="padding-left: 0;">
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                                        <div class="md-input-wrapper"><label for="uk_dp_start">Start Date</label><input class="md-input" type="text" id="uk_dp_start"><span class="md-input-bar "></span></div>

                                                    </div>
                                                </div>
                                                <div class="uk-width-large-2-10 uk-width-medium-1-1">
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                                        <div class="md-input-wrapper"><label for="uk_dp_end">End Date</label><input class="md-input" type="text" id="uk_dp_end"><span class="md-input-bar "></span></div>

                                                    </div>
                                                </div>
                                                <div class="uk-width-large-1-10 uk-width-1-1">
                                                    <a class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light" href="javascript:void(0)">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear clearfix"></div> <br>

                                    <div class="uk-width-large-1-2 m-t-15" style="margin-left: 25%;">
                                        <div class="timeline timeline_small uk-margin-bottom">
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_success"><i class="material-icons">add</i></div>
                                                <div class="timeline_date">
                                                    09 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">Deluxe room inventory increased to 10 for 05 Jan 2019 to 08 Feb 2019 by user name.</div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_danger"><i class="material-icons">remove</i></div>
                                                <div class="timeline_date">
                                                    15 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">Deluxe room inventory decrease to 10 for 05 Jan 2019 to 08 Feb 2019 by user name.<a href="#"><strong>Velit est quisquam dolorem officiis quis.</strong></a></div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_success"><i class="material-icons">done</i></div>
                                                <div class="timeline_date">
                                                    09 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">New booking received for Super Deluxe room for 3 rooms for the 01 Jan 2019 in inquiry #LCT123. </div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_danger"><i class="material-icons">clear</i></div>
                                                <div class="timeline_date">
                                                    15 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">3 Deluxe rooms canceled for the date 01 Jan 2019 in inquiry #LCT123.</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>                                                                                

            <div class="uk-modal" id="assign_group_tour">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Assign Group Tour</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Tour</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Gujarat Tour</option>
                                <option value="1">Kerala Tour</option>
                                <option value="2"                                                                                        >Kashmir Tour</option>
                                <option value="2">Rajasthan Tour</option>
                                <option value="2">Punjab Tour</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Departure Date</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">01 Jan 2019</option>
                                <option value="2">01 Feb 2019</option>
                                <option value="2">01 Mar 2019</option>
                                <option value="2">01 Jun 2019</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-modal-footer">
                        <div class="uk-text-left uk-width-medium-1-2 uk-float-left">
                            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-flat">Create New Group Tour</a>
                        </div>
                        <div class="uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" class="md-b                                                                                                                                                                                                        tn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="assign_customised_tour">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Assign Customised Tour</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Tour</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Gujarat Tour</option>
                                <option value="1">Kerala Tour</option>
                                <option value="2">Kashmir Tour</option>
                                <option value="2">Rajasthan Tour</option>
                                <option value="2">Punjab Tour</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                        <div class="uk-width-medium-1-1 ">
                            <div class="md-input-wrapper"><label for="invoice_dp">Departure Date</label><input class="md-input uk-width-1-1 " type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}"><span class="md-input-bar "></span></div>

                        </div>
                        <div class="clear clearfix"></div>
                    </div>
                    <div class="uk-modal-footer">
                        <div class="uk-text-left uk-width-medium-1-2 uk-float-left">
                            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-flat">Create New Customised Tour</a>
                        </div>
                        <div class="uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="update_status">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Update Status</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Status</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Status 1</option>
                                <option value="2">Status 2</option>
                                <option value="2">Status 3</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                    </div>
                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="update_status" class="uk-modal uk-hide uk-fade" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999;">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Update Status</h3>
        </div>
        <select id="selec_adv_s2_1" name="selec_adv_s2_1" class="select_no_search uk-width-large-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
            <option value="1">Status 1</option>
            <option value="2">Status 2</option>
            <option value="2">Status 3</option>
        </select>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button> <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Update</button>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_advanced.min.js"></script>

<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script type="text/javascript">
                                                            $(function () {
                                                                if (location.hash && location.hash.length) {
                                                                    var hash = decodeURIComponent(location.hash.substr(1));
                                                                    setTimeout(
                                                                            function ()
                                                                            {
                                                                                $("ul#user_profile_tabs li." + hash).trigger("click");
//                        alert(hash);
                                                                            }, 1000);
                                                                }
                                                            });
</script>

@endsection
