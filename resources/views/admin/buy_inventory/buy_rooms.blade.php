@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Hotel Rooms</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Hotel Rooms</h4>
                    </div>
                    <div class="widget-body">
                        <h3>
                            Hotel 1
                            <span class="rating">
                                <span class="star"></span>
                                <span class="star"></span>
                                <span class="star"></span>
                                <span class="star"></span>
                                <span class="star"></span>
                            </span>							
                        </h3>
                        <span>Hotel Address</span>
                        <div class="hotels-detials-tab">
                            <div class="span2">OVERVIEW</div>
                            <div class="span2">ROOM TYPE</div>
                            <div class="span2">LOCATION</div>
                            <div class="span2">POLICIES</div>
                        </div>
                        <div class="hotel-thumbs-slider span8 m-l-0">
                            <img src="{{ env('APP_PUBLIC_URL')}}admin/img/hotel-thumb.jpg" alt="Hotel Rooms" />
                        </div>

                        <div class="clear clearfix"></div>

                        <div class="rooms-area pull-left">
                            <h4>Select room</h4>
                            <p class="filter-room-desc">
                                Choose a room with below filters and room types
                            </p>
                            <ul class="the-icons clearfix filter-options">
                                <li class="filter-item" style="width: 35%;">
                                    <span>Including Meal(s)</span><i class="icon-adjust"></i>
                                </li>
                                <li class="filter-item" style="width: 35%;">
                                    <span>Free Cancellation</span><i class="icon-remove-sign"></i>
                                </li>
                                <li class="filter-item" style="width: 35%;">
                                    <span>Pay At Checkout</span><i class="icon-money"></i>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="clear clearfix"></div>
                </div>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection


