@extends('layouts.admin')
@section('content')
<style>
    .row {
        margin-right: -15px;
        margin-left: -15px;
    }
    .col-lg-4 {
        width: 33.33333333%;
    }
    .col-lg-6 {
        width: 50%;
    }
    .col-lg-8 {
        width: 66.66666667%;
    }
    .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9 {
        float: left;
    }
    .hotel-details {
        padding: 20px;
        margin-bottom: 30px;
        border: 1px solid #DDD;
    }
    .option-bar {
        margin-bottom: 10px;
    }
    .section-heading .media {
        margin: 0px;
    }
    .media, .media-body {
        overflow: hidden;
        zoom: 1;
    }
    .media-body, .media-left, .media-right {
        display: table-cell;
        vertical-align: top;
    }
    .media-left{
        width: 100px;
    }
    .section-heading .media .media-left i {
        color: #ed5565;
        width: 47px;
        height: 47px;
        line-height: 47px;
        text-align: center;
        font-size: 30px;
    }
    .media-body {
        width: 500px;
        padding-left: 5px;
    }
    .section-heading .media .media-body h4 {
        margin: 0px;
        font-weight: 600;
        color: #ed5565;
    }
    .section-heading .media .media-body .border {
        height: 2px;
        width: 60px;
        background: #ed5565;
        margin: 5px 0px 3px;
    }
    .section-heading .media .media-body p {
        margin: 0 0 0px;
        color: #868686;
    }
    .option-bar .header-price {
        text-align: right;
        float: right;
    }
    .option-bar .header-price h3 {
        font-size: 21px;
        font-weight: 600;
        margin: 0px;
        color: #ed5565;
    }
    .option-bar .header-price p {
        color: orange;
        margin-bottom: 0px;
    }
    .rooms-details-body .about-room {
        margin: 50px 0px;
    }
    .rooms-details-body .title {
        font-weight: 700;
        margin: 0px 0 40px;
        color: #333;
        font-size: 24px;
        text-transform: uppercase;
        border-bottom: solid 1px #e0e0e0;
        padding-bottom: 10px;
    }
    .rooms-details-body .about-room p {
        line-height: 20px;
    }
    .amenities-box {
        margin-bottom: 50px;
    }
    .amenities-box ul {
        margin: 0px;
        padding: 0px;
    }
    .amenities-box ul li {
        list-style: none;
        margin-bottom: 20px;
        color: #868686;
        font-size: 13px;
    }
    .amenities-box ul li i {
        margin-right: 20px;
        color: #ed5565;
        text-align: center;
        font-size: 15px;
        font-weight: 400;
    }
    .carousel-inner {
        position: relative;
        width: 100%;
        overflow: hidden;
    }
    .owl-item .item{
        text-align: center;
    }

    .hotel-slider {
        width:100%;
    }
    ul.lightSlider {
        list-style: none outside none;
        padding-left: 0;
        margin-bottom:0;
    }
    ul.lightSlider li {
        display: block;
        float: left;
        margin-right: 6px;
        cursor:pointer;
    }
    ul.lightSlider li img {
        display: block;
        height: auto;
        max-width: 100%;
    }
</style>

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Hotel Rooms</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-body rooms-details-body">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="hotel-details">
                                <!-- Option Bar Start-->
                                <div class="option-bar clearfix">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="section-heading">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <img src="{{ env('APP_PUBLIC_URL')}}admin/img/build14-512.png" alt="Hotel Logo" />
                                                    </div>
                                                    <div class="media-body hidden-xs">
                                                        <h4>Hotel Name</h4>
                                                        <div class="border"></div>
                                                        <p>Checkout the latest deal</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="header-price">
                                                <!--<h3>$399,500</h3>-->
                                                <p>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Option Bar End-->

                                <!-- Rooms Detail Slider Start-->
                                <div class="rooms-detail-slider simple-slider">
                                    <div class="hotel-slider">
                                        <ul id="lightSlider">
                                            <li data-thumb="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-1.jpg">
                                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-1.jpg" />
                                            </li>
                                            <li data-thumb="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-2.jpg">
                                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-2.jpg" />
                                            </li>
                                            <li data-thumb="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-3.jpg">
                                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-3.jpg" />
                                            </li>
                                            <li data-thumb="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-4.jpg">
                                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-4.jpg" />
                                            </li>
                                            <li data-thumb="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-5.jpg">
                                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-5.jpg" />
                                            </li>
                                            <li data-thumb="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-6.jpg">
                                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-6.jpg" />
                                            </li>
                                            <li data-thumb="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-7.jpg">
                                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-7.jpg" />
                                            </li>
                                            <li data-thumb="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-8.jpg">
                                                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/rooms-detail-slider-8.jpg" />
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <!-- About Room start-->
                                <div class="about-room">
                                    <h2 class="title">
                                        General Information About Room
                                    </h2>
                                    <p> Quisque non dictum eros. Praesent porta vehicula arcu eu ornare. Donec id egestas arcu. Suspendisse auctor condimentum ligula ultricies cursus. Vestibulum vel orci vel lacus rhoncus sagittis sed vitae mi. Pellentesque molestie elit bibendum tincidunt semper. Aliquam ac volutpat risus. In felis felis, posuere commodo aliquet eget, sagittis sed turpis. Phasellus commodo turpis non nunc egestas, et egestas felis pretium. Pellentesque dignissim libero vitae tincidunt semper. Nam id ante nisi. Nam sollicitudin, dolor non suscipit feugiat, nibh lacus commodo metus, nec tempus dui velit sagittis velit. Pellentesque elementum risus rhoncus justo porta, at varius odio consequat. Duis non augue adipiscing, posuere quam non, tempus urna.</p>
                                </div>
                                <!-- About Room End-->

                                <!-- Amenities Start-->
                                <div class="amenities">
                                    <h2 class="title">Hotel Room Types </h2>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Room Type 1
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Room Type 2
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Room Type 3
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="amenities">
                                    <h2 class="title">Hotel Agents  </h2>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Agent 1
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Agent 2
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Agent 3
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- About Room End-->

                                <div class="about-room">
                                    <h2 class="title">
                                        Hotel polices
                                    </h2>
                                    <p>Quisque non dictum eros. Praesent porta vehicula arcu eu ornare. Donec id egestas arcu. Suspendisse auctor condimentum ligula ultricies cursus. Vestibulum vel orci vel lacus rhoncus sagittis sed vitae mi. Pellentesque molestie elit bibendum tincidunt semper. Aliquam ac volutpat risus. In felis felis, posuere commodo aliquet eget, sagittis sed turpis. Phasellus commodo turpis non nunc egestas, et egestas felis pretium. Pellentesque dignissim libero vitae tincidunt semper. Nam id ante nisi. Nam sollicitudin, dolor non suscipit feugiat, nibh lacus commodo metus, nec tempus dui velit sagittis velit. Pellentesque elementum risus rhoncus justo porta, at varius odio consequat. Duis non augue adipiscing, posuere quam non, tempus urna.</p>
                                </div>

                                <div class="amenities">
                                    <h2 class="title">Facilities</h2>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Facilities 1
                                                </li>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Facilities 1
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Facilities 2
                                                </li>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Facilities 2
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 amenities-box">
                                            <ul>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Facilities 3
                                                </li>
                                                <li>
                                                    <i class="flaticon-no-smoking"></i>Facilities 3
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="about-room">
                                    <h2 class="title">
                                        Hotel Details
                                    </h2>
                                    <p> Quisque non dictum eros. Praesent porta vehicula arcu eu ornare. Donec id egestas arcu. Suspendisse auctor condimentum ligula ultricies cursus. Vestibulum vel orci vel lacus rhoncus sagittis sed vitae mi. Pellentesque molestie elit bibendum tincidunt semper. Aliquam ac volutpat risus. In felis felis, posuere commodo aliquet eget, sagittis sed turpis. Phasellus commodo turpis non nunc egestas, et egestas felis pretium. Pellentesque dignissim libero vitae tincidunt semper. Nam id ante nisi. Nam sollicitudin, dolor non suscipit feugiat, nibh lacus commodo metus, nec tempus dui velit sagittis velit. Pellentesque elementum risus rhoncus justo porta, at varius odio consequat. Duis non augue adipiscing, posuere quam non, tempus urna.</p>
                                </div>

                                <div class="about-room">
                                    <h2 class="title">
                                        Average Rates
                                    </h2>
                                    <span class="rating">
                                        <span class="star"></span>
                                        <span class="star"></span>
                                        <span class="star"></span>
                                        <span class="star"></span>
                                        <span class="star"></span>
                                    </span>
                                </div>

                                <div class="about-room">
                                    <h2 class="title">Past 6 Month Booking</h2>
                                    <div class="search-area">
                                        <div class="control-group span3 m-l-0">
                                            <label class="control-label">Start Date</label>
                                            <div class="controls">
                                                <input type="text" value="01-01-2018" class="span12" placeholder="Start Date" />
                                            </div>
                                        </div>
                                        <div class="control-group span3">
                                            <label class="control-label">End Date</label>
                                            <div class="controls">
                                                <input type="text" value="30-06-2018" class="span12" placeholder="End Date" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <button type="button" class="btn btn-success m-l-5 m-t-25">Search</button>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="width: 25px;">No</th>
                                                <th>Agent Name</th>
                                                <th>Booking Date</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Agent Name</td>
                                                <td>18-07-2018</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Agent Name</td>
                                                <td>18-07-2018</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Agent Name</td>
                                                <td>18-07-2018</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="about-room">
                                    <h2 class="title">Next 6 Month Inventory</h2>
                                    <div class="search-area">
                                        <div class="control-group span3 m-l-0">
                                            <label class="control-label">Start Date</label>
                                            <div class="controls">
                                                <input type="text" value="01-01-2018" class="span12" placeholder="Start Date" />
                                            </div>
                                        </div>
                                        <div class="control-group span3">
                                            <label class="control-label">End Date</label>
                                            <div class="controls">
                                                <input type="text" value="30-06-2018" class="span12" placeholder="End Date" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <button type="button" class="btn btn-success m-l-5 m-t-25">Search</button>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="width: 25px;">No</th>
                                                <th>Agent Name</th>
                                                <th>No. Of Rooms</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Agent Name</td>
                                                <td>12</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Agent Name</td>
                                                <td>15</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Agent Name</td>
                                                <td>10</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="about-room">
                                    <h2 class="title">Agent Details</h2>
                                    <table class="table table-striped table-bordered" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="width: 25px;">No</th>
                                                <th>Agent Name</th>
                                                <th>Email ID</th>
                                                <th>Mobile No.</th>
                                                <th>Location</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>1</td>
                                                <td>Agent Name 1</td>
                                                <td>agent@bcreative.in</td>
                                                <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                                <td class="hidden-phone">Ahmdabad,Gujrat <br/>India</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>2</td>
                                                <td>Agent Name 1</td>
                                                <td>agent@bcreative.in</td>
                                                <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                                <td class="hidden-phone">Ahmdabad,Gujrat <br/>India</td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>2</td>
                                                <td>Agent Name 1</td>
                                                <td>agent@bcreative.in</td>
                                                <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                                <td class="hidden-phone">Ahmdabad,Gujrat <br/>India</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="about-room">
                                    <h2 class="title">My Inventory</h2>
                                    <span class="tools">
                                        <a href="{{ env('ADMIN_URL')}}inventory/add" class="btn btn-primary pull-right m-b-5">Add Inventory</a>
                                    </span>
                                    <table class="table table-striped table-bordered" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="width: 25px;">No</th>
                                                <th>Rooms</th>
                                                <th>No. Of Rooms</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>1</td>
                                                <td>Deluxe Room</td>
                                                <td>5</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                                <td class="text-center">
                                                    <a href="{{ env('ADMIN_URL')}}inventory/edit/1" title="Edit Inventory" class="btn btn-mini btn-primary"><i class="icon-pencil icon-white"></i></a> 
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>2</td>
                                                <td>Deluxe Room</td>
                                                <td>12</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                                <td class="text-center">
                                                    <a href="{{ env('ADMIN_URL')}}inventory/edit/1" title="Edit Inventory" class="btn btn-mini btn-primary"><i class="icon-pencil icon-white"></i></a> 
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>3</td>
                                                <td>Deluxe Room</td>
                                                <td>18</td>
                                                <td>01-08-2018</td>
                                                <td>30-11-2018</td>
                                                <td class="text-center">
                                                    <a href="{{ env('ADMIN_URL')}}inventory/edit/1" title="Edit Inventory" class="btn btn-mini btn-primary"><i class="icon-pencil icon-white"></i></a> 
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <!-- content div End-->


                        </div>
                        <div class="clear clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/lightslider/css/lightslider.css" />
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/lightslider/js/lightslider.js"></script>
<script type="text/javascript">
$(document).ready(function () {

    $('#lightSlider').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 9
    });

});
</script>

@endsection



