@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Buy Hotel</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Buy Hotel</h4>
                    </div>
                    <div class="widget-body">
                        <div class="span2">
                            <form method="get">
                                <div class="control-group span10 m-l-0">
                                    <label class="control-label">Country</label>
                                    <div class="controls">
                                        <select class="span12 chosen" data-placeholder="Choose a Country" tabindex="1">
                                            <option value=""></option>
                                            <option value="1">India</option>
                                            <option value="2">US</option>
                                            <option value="3">UK</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clear clearfix"></div>

                                <div class="control-group span10 m-l-0">
                                    <label class="control-label">State</label>
                                    <div class="controls">
                                        <select class="span12 chosen" data-placeholder="Choose a State" tabindex="1">
                                            <option value=""></option>
                                            <option value="1">Gujarat</option>
                                            <option value="2">Maharashtra</option>
                                            <option value="3">Punjab</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clear clearfix"></div>

                                <div class="control-group span10 m-l-0">
                                    <label class="control-label">City</label>
                                    <div class="controls">
                                        <select class="span12 chosen" data-placeholder="Choose a City" tabindex="1">
                                            <option value=""></option>
                                            <option value="1">Mumbai</option>
                                            <option value="2">Pune</option>
                                            <option value="3">Thane</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clear clearfix"></div>

                                <div class="control-group span10 m-l-0">
                                    <label class="control-label">Pincode</label>
                                    <div class="controls">
                                        <input type="text" class="span12" data-mask="99999" />
                                    </div>
                                </div>
                                <div class="clear clearfix"></div>

                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success">Search</button>
                                </div>
                            </form>
                        </div>
                        <div class="span10 hotel-room-grid">
                            <div class="span4 m-l-0">
                                <div class="grid-thumb">
                                    <img src="{{ env('APP_PUBLIC_URL')}}admin/img/hotel-thumb.jpg" alt="Admin Lab" />
                                </div>
                                <div class="grid-title">
                                    <h5>Hotem 1 | rooms</h5>
                                </div>
                                <div class="grid-pricing">
                                    <span class="fd_price__discount">2754</span>
                                    <span class="fd_price__actual">3531</span>
                                </div>
                                <div class="grid-footer">
                                    <a href="{{ env('ADMIN_URL')}}buy-hotel-rooms/1" class="btn">Buy</a>
                                </div>
                            </div>
                            <div class="span4 m-l-0">
                                <div class="grid-thumb">
                                    <img src="{{ env('APP_PUBLIC_URL')}}admin/img/hotel-thumb.jpg" alt="Admin Lab" />
                                </div>
                                <div class="grid-title">
                                    <h5>Hotem 2 | rooms</h5>
                                </div>
                                <div class="grid-pricing">
                                    <span class="fd_price__discount">2754</span>
                                    <span class="fd_price__actual">3531</span>
                                </div>
                                <div class="grid-footer">
                                    <a href="javascript:,;" class="btn">Buy</a>
                                </div>
                            </div>
                            <div class="span4 m-l-0">
                                <div class="grid-thumb">
                                    <img src="{{ env('APP_PUBLIC_URL')}}admin/img/hotel-thumb.jpg" alt="Admin Lab" />
                                </div>
                                <div class="grid-title">
                                    <h5>Hotem 3 | rooms</h5>
                                </div>
                                <div class="grid-pricing">
                                    <span class="fd_price__discount">2754</span>
                                    <span class="fd_price__actual">3531</span>
                                </div>
                                <div class="grid-footer">
                                    <a href="javascript:,;" class="btn">Buy</a>
                                </div>
                            </div>
                            <div class="span4 m-l-0">
                                <div class="grid-thumb">
                                    <img src="{{ env('APP_PUBLIC_URL')}}admin/img/hotel-thumb.jpg" alt="Admin Lab" />
                                </div>
                                <div class="grid-title">
                                    <h5>Hotem 4 | rooms</h5>
                                </div>
                                <div class="grid-pricing">
                                    <span class="fd_price__discount">2754</span>
                                    <span class="fd_price__actual">3531</span>
                                </div>
                                <div class="grid-footer">
                                    <a href="javascript:,;" class="btn">Buy</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear clearfix"></div>
                </div>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection

