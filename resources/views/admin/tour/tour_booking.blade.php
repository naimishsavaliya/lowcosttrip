@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Tour Booking</span></li>
        </ul>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <div class="p-t-30">
                <table class="uk-table dt_default">
                    <thead>
                        <tr>
                            <th>Lead ID</th>
                            <th>Tour Name</th>
                            <th>Customer Name</th>
                            <th>Booking Date</th>
                            <th>Departure Date</th>
                            <th>Hotel Type</th>
                            <th>No of Rooms</th>
                            <th>Adult/Child/Infant</th>
                            <th>Cost</th>
                            <th>Agent</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i = 1; $i <= rand(3, 15); $i++) { ?>
                            <tr class="odd gradeX">
                                <td>LCT<?php echo rand(1111, 9999); ?></td>
                                <td>Tour Name</td>
                                <td>Customer Name</td>
                                <td>Mar 5, 2018</td>
                                <td>Nov 10, 2018</td>
                                <td>Deluxe</td>
                                <td>1</td>
                                <td>2/1/0</td>
                                <td>50,000</td>
                                <td>Agent <?php echo $i; ?></td>
                                <td>Pending</td>
                                <td>
                                    <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                        <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                        <div class="uk-dropdown">
                                            <ul class="uk-nav uk-nav-dropdown">
                                                <li><a href=""> Cancel Booking</a></li>
                                                <li><a href=""> Edit Booking</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/wizard_steps.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_wizard.min.js"></script>

<script>
// load parsley config (altair_admin_common.js)
altair_forms.parsley_validation_config();
// load extra validators
altair_forms.parsley_extra_validators();
</script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/parsleyjs/dist/parsley.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/instafilta.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/page_invoices.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/jquery.waitforimages.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/page_gallery.min.js"></script>
@endsection