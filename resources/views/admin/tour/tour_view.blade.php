@extends('layouts.theme')
@section('content')

<link href="{{ env('APP_PUBLIC_URL')}}front/css/style.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/css/bootstrap.min.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/css/animate.min.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/css/updates.css" rel="stylesheet" />
<link href="{{ env('APP_PUBLIC_URL')}}front/css/responsive.css" rel="stylesheet" />

<!--<script src="{{ env('APP_PUBLIC_URL')}}front/js/js/jquery-1.11.1.min.js"></script>-->
<!--<script src="{{ env('APP_PUBLIC_URL')}}front/js/jquery-ui.1.10.4.min.js"></script>-->
<!--<script src="{{ env('APP_PUBLIC_URL')}}front/js/bootstrap.js"></script>-->
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}front/components/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}front/js/jquery.noconflict.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}front/js/theme-scripts.js"></script>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row">
            <div id="main" class="col-md-9">
                <div class="featured-gallery image-box">
                    <span class="discount"><span class="discount-text">10% Discount</span></span>
                    <div class="flexslider photo-gallery style1" id="post-slideshow1" data-sync="#post-carousel1" data-func-on-start="showTourDetailedDiscount">
                        <ul class="slides">
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                            <li><a href="#"><img src="http://placehold.it/870x442" alt=""></a></li>
                        </ul>
                    </div>
                    <div class="flexslider image-carousel style1" id="post-carousel1"  data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#post-slideshow1">
                        <ul class="slides">
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                            <li><img src="http://placehold.it/70x70" alt="" /></li>
                        </ul>
                    </div>
                </div>

                <div id="tour-details" class="travelo-box">
                    <div class="intro2 small-box border-box table-wrapper hidden-table-sms">
                        <div class="image-container table-cell"><img src="http://placehold.it/166x104" alt=""></div>
                        <div class="table-cell">
                            <dl class="term-description">
                                <dt>Location:</dt><dd>Rome</dd>
                                <dt>Available Seats:</dt><dd>45</dd>
                                <dt>Duration:</dt><dd>1 Day</dd>
                                <dt>Price:</dt><dd>Rs. 35,700</dd>
                            </dl>
                        </div>
                        <div class="price-section table-cell">
                            <div class="price"><small>3 days tour</small><div class="price-per-unit">Rs. 35,700</div></div>
                            <a href="#" class="button green btn-small uppercase">Book Tour</a>
                        </div>
                    </div>

                    <div class="intro2 small-box border-box table-wrapper hidden-table-sms">
                        <div class="table-cell">
                            <dl class="term-description">
                                <dt>Total Visit:</dt><dd>50</dd>
                                <dt>Publish Date:</dt><dd>16-06-2018</dd>
                                <dt>Status:</dt><dd>Approved</dd>
                                <dt>Inquiry Received:</dt><dd>700</dd>
                                <dt>Confirmed Inquiry:</dt><dd>140</dd>
                                <dt>Number of Pax:</dt><dd>30</dd>
                                <dt>Tour Manager:</dt><dd>Manager Name</dd>
                            </dl>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                            <tr>
                                <th style="width: 25px;">No</th>
                                <th>Agent Name</th>
                                <th>Email ID</th>
                                <th>Mobile No.</th>
                                <th>Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX">
                                <td>1</td>
                                <td>Agent Name 1</td>
                                <td>agent@bcreative.in</td>
                                <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                <td class="hidden-phone">Ahmdabad,Gujrat <br/>India</td>
                            </tr>
                            <tr class="odd gradeX">
                                <td>2</td>
                                <td>Agent Name 1</td>
                                <td>agent@bcreative.in</td>
                                <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                <td class="hidden-phone">Ahmdabad,Gujrat <br/>India</td>
                            </tr>
                            <tr class="odd gradeX">
                                <td>2</td>
                                <td>Agent Name 1</td>
                                <td>agent@bcreative.in</td>
                                <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                <td class="hidden-phone">Ahmdabad,Gujrat <br/>India</td>
                            </tr>
                        </tbody>
                    </table>
                    <!--<div class="tour-google-map block"></div>-->

                    <h2>General Information About Rome</h2>
                    <p>Sed aliquam nunc eget velit imperdiet, in rutrum mauris malesuada. Quisque ullamcorper vulputate nisi, et fringilla ante convallis quis. Nullam vel tellus non elit suscipit volutpat. Integer id felis et nibh rutrum dignissim ut non risus. In tincidunt urna quis sem luctus, sed accumsan magna pellentesque. Donec et iaculis tellus. Vestibulum ut iaculis justo, auctor sodales lectus. Donec et tellus tempus, dignissim maurornare, consequat lacus. Integer dui neque, scelerisque nec sollicitudin sit amet, sodales a erat. Duis vitae condimentum ligula. Integer eu mi nisl. Donec massa dui, commodo id arcu quis, venenatis scelerisque velit.</p>

                    <h2>Day 01</h2>
                    <p>Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla condimentum, semper dolor non, faucibus dolor. Vivamus adipiscing eros quis orci fringilla, sed pretium lectus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec nec velit non odio aliquam suscipit. Sed non neque faucibus, condimentum lectus at, accumsan enim. Fusce pretium egestas cursus. Etiam consectetur, orci vel rutrum volutpat, odio odio pretium nisiodo tellus libero et urna. Sed commodo ipsum ligula, id volutpat risus vehicula in. Pellentesque non massa eu nibh posuere bibendum non sed enim. Maecenas lobortis nulla sem, vel egestas dui ullamcorper ac.</p>

                    <h2>Day 02</h2>
                    <p>Sed scelerisque lectus sit amet faucibus sodales. Proin ut risus tortor. Etiam fermentum tellus auctor, fringilla sapien et, congue quam. In a luctus tortor. Suspendisse eget tempor libero, ut sollicitudin ligula. Nulla vulputate tincidunt est non congue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus at est imperdiet, dapibus ipsum vel, lacinia nulla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus id interdum lectus, ut elementum elit. Nullam a molestie magna. Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla condimentum, semper dolor non, faucibus dolor. Vivamus adipiscing eros quis orci fringilla, sed pretium lectus viverra.</p>

                    <h2>Day 03</h2>
                    <p>Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla condimentum, semper dolor non, faucibus dolor. Vivamus adipiscing eros quis orci fringilla, sed pretium lectus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec nec velit non odio aliquam suscipit. Sed non neque faucibus, condimentum lectus at, accumsan enim. Fusce pretium egestas cursus. Etiam consectetur, orci vel rutrum volutpat, odio odio pretium nisiodo tellus libero et urna. Sed commodo ipsum ligula, id volutpat risus vehicula in. Pellentesque non massa eu nibh posuere bibendum non sed enim. Maecenas lobortis nulla </p>
                </div>
            </div>
            <div class="sidebar col-md-3">
                <div class="travelo-box">
                    <h4 class="box-title">Last Minute Deals</h4>
                    <div class="image-box style14">
                        <article class="box">
                            <figure><a href="#" title=""><img width="63" height="59" src="http://placehold.it/63x60" alt=""></a></figure>
                            <div class="details">
                                <h5 class="box-title"><a href="#">Plaza Tour Eiffel</a></h5>
                                <label class="price-wrapper"><span class="price-per-unit">$170</span>avg/night</label>
                            </div>
                        </article>
                        <article class="box">
                            <figure><a href="#" title=""><img width="63" height="59" src="http://placehold.it/63x60" alt=""></a></figure>
                            <div class="details">
                                <h5 class="box-title"><a href="#">Ocean Park Tour</a></h5>
                                <label class="price-wrapper"><span class="price-per-unit">$620</span>avg/night</label>
                            </div>
                        </article>
                        <article class="box">
                            <figure><a href="#" title=""><img width="63" height="59" src="http://placehold.it/63x60" alt=""></a></figure>
                            <div class="details">
                                <h5 class="box-title"><a href="#">Dream World Trip</a></h5>
                                <label class="price-wrapper"><span class="price-per-unit">$322</span>avg/night</label>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="travelo-box book-with-us-box">
                    <h4>Why Book with us?</h4>
                    <ul>
                        <li>
                            <i class="soap-icon-hotel-1 circle"></i>
                            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                        <li>
                            <i class="soap-icon-savings circle"></i>
                            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                        <li>
                            <i class="soap-icon-support circle"></i>
                            <h5 class="title"><a href="#">Excellent Support</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                    </ul>
                </div>
                <div class="travelo-box contact-box">
                    <h4 class="box-title">Need Travelo Help?</h4>
                    <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                        <br />
                        <a href="#" class="contact-email">help@travelo.com</a>
                    </address>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
