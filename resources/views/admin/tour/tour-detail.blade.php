@extends('layouts.theme')
@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}css/dropzone.css">
<style type="text/css">
    .uk-sticky-placeholder .uk-tab{background: #1976D2;}
    .user_heading{ padding: 0px 24px 0px 24px;}
    .select2{ width: 100% !important;}
    .uk-tab > li.uk-active > a{color: #FFF;}
    .uk-tab > li.uk-active > a{border-bottom-color: #00071f;}
    /*.uk-dropdown{width: 200px !important;}*/
    .select2-container--open{z-index: 9999;}
    /*.uk-dropdown-shown{min-width: 260px !important;}*/
    .selectize-dropdown{z-index: 9999 !important;}
    ul.uk-switcher li, ul.uk-switcher li table{overflow: -webkit-paged-x;}
</style>
@endsection
@section('content')

<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('tour.index') }}">Manage Tour Package</a></li>
            <li><span>{{$tour->tour_name}}</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner" class="p-0">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-1-1">
                    <div class="">
                        <div class="user_heading">
                                                    <div class="user_heading_menu hidden-print">
                            @if((\Auth::user()->role_id == 1) || (\Auth::user()->role_id == 2 && in_array($tour->sector,explode(',', \Session::get('sector_id')))) || (\Auth::user()->id==$tour->created_by) )
                                <div class="uk-display-inline-block" data-uk-dropdown="{pos:'left-top'}">
                                    <a href="javascript:void(0)" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                                        <i class="uk-icon-plus f-s-13"></i> Add New
                                    </a>
                                <div class="uk-dropdown uk-dropdown-small">
                                    <ul class="uk-nav">
                                        <li><a href="javascript:;" data-uk-modal="{target:'#quotation_form_model'}"><i class="material-icons ">add</i> Quotation</a></li>
                                        <li><a href="javascript:;" data-uk-modal="{target:'#booking_form_model'}"><i class="material-icons ">add</i> Booking</a></li>
                                        @can('add_tour_hotel')
                                        <li><a href="javascript:;" data-uk-modal="{target:'#hotelroom_form_model'}"><i class="material-icons ">add</i> Hotel Room</a></li>
                                        @endcan
                                        @can('add_tour_itinerary')
                                        <li><a href="javascript:;" data-uk-modal="{target:'#itenary_form_model'}"><i class="material-icons ">add</i> Itinerary</a></li>
                                        @endcan
                                        <!--<li><a href="javascript:;" data-uk-modal="{target:'#departure_form_model'}"><i class="material-icons ">add</i> Departure Dates</a></li>-->
                                        @can('add_tour_cost')
                                        <li><a href="{{route('tour.cost.manage',$tour->id)}}" ><i class="material-icons ">add</i> Cost</a></li>
                                        @endcan
                                        @can('add_tour_faq')
                                        <li><a href="javascript:;" data-uk-modal="{target:'#faq_form_model'}"><i class="material-icons ">add</i> FAQ</a></li>
                                        @endcan
                                        <li><a href="javascript:;" data-uk-modal="{target:'#payment_form_model'}"><i class="material-icons ">add</i> Payment</a></li>
                                    </ul>
                                </div>
                            </div>
                            @endif
                        </div>
                         

                        <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content'}">
                            <li class="uk-active"><a href="#">Tour Info</a></li>
                            <!-- <li class="quotation"><a href="#" >Quotation</a></li> -->
                            <li class="booking"><a href="javascript:;">Booking</a></li>
                            <li class="hotel"><a href="#" >Hotel Rooms</a></li>
                            <li class="itinerary"><a href="#" >Itinerary</a></li>
                            <li class="departure"><a href="#" >Departure Dates</a></li>
                            <li class="cost"><a href="#" >Cost</a></li>
                            <li class="faq"><a href="#" >FAQ</a></li>
                            <li class="payment"><a href="#" >Payment</a></li>
                            <li class="history"><a href="#" >History</a></li>
                        </ul>
                        </div>
                        @include('admin.includes.alert')
                        <div class="user_content">
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li class="p-t-10">
                                    @include('admin/tour/tour-manage-form')
                                </li>
                                <?php /*
                                <li>
                                    @include('admin/tour/tour-quotation-list')
                                </li>
                                */ ?>
                                <li>
                                    <p>Booking data not available.</p>
                                </li>
                                <li>
                                <!--room booking tab-->
                                    <table class="uk-table" id="dt_default">
                                        <thead>
                                            <tr>
                                                <th>Hotel Type</th>
                                                <th>Location</th>
                                                <th>Hotel Name</th>
                                                <th>Star</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($tour_hotels as $tHotel)
                                                <tr class="gradeX">
                                                    <td>{{$tHotel->name}}</td>
                                                    <td>{{$tHotel->location}}</td>
                                                    <td>{{$tHotel->hotel}}</td>
                                                    <td>{{$tHotel->hotel_star}}</td>
                                                    <td>
                                                        @can('update_tour_hotel')
                                                        @if((\Auth::user()->role_id == 1) || (\Auth::user()->role_id == 2 && in_array($tour->sector,explode(',', \Session::get('sector_id')))) || (\Auth::user()->id==$tour->created_by) )
                                                        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                        <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                                        <div class="uk-dropdown">
                                                        <ul class="uk-nav uk-nav-dropdown">
                                                        <li><a href="javascript:;" data-uk-modal="{target:'#hotelroom_form_model'}"> Edit Hotel</a></li>
                                                        </ul>
                                                        </div>
                                                        </div>
                                                        @endif
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <!--Itinerary tab-->
                                    @include('admin/tour/tour-itinerary-list')
                                </li>
                                <li>
                                    <!--departure tab-->
                                    @include('admin/tour/tour-departure')
                                </li>
                                <li>
                                <!--cost tab-->
                                    <table class="uk-table" id="dt_default">
                                        <thead>
                                            <tr>
                                                <th>Price Type</th>
                                                <th>Discount %</th>
                                                @foreach($tour_package_type as $type)
                                                <th>{{$type->name}}</th>
                                                @endforeach
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tourCostData as $cId => $tourCost)
                                            <tr style="background-color: #ededed;"><td class="v-a-m" colspan="{{count($tour_package_type)+2}}"><b>{{date('d M y',strtotime($tourCost['from_date']))}} To {{date('d M y',strtotime($tourCost['to_date']))}}</b></td>
                                                <td>
                                                    @can('edit_tour_cost')
                                                    @if((\Auth::user()->role_id == 1) || (\Auth::user()->role_id == 2 && in_array($tour->sector,explode(',', \Session::get('sector_id')))) || (\Auth::user()->id==$tour->created_by) )
                                                    <a href="{{route('tour.cost.edit',[$tour->id,$cId])}}" title="Edit Cost" class=""><i class="md-icon material-icons">edit</i></a> 
                                                    <a href="{{route('tour.cost.delete',$cId)}}" onclick="return confirm('Are you sure to delete?');" title="Delete Cost" class=""><i class="md-icon material-icons">delete</i></a> 
                                                    @endif
                                                    @endcan
                                                </td>
                                            </tr>
                                            @foreach($tourCost['rates'] as $rates)
                                                <tr class="odd gradeX">
                                                    <td>{{$rates['type']}}</td>
                                                    <td>{{$tourCost['discount']}}</td>
                                                    @foreach($rates['amount'] as $amount)
                                                    <td>{{isset($amount['cost'])?$amount['cost']:''}}/{{isset($amount['sell'])?$amount['sell']:''}}</td>
                                                    @endforeach
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <!--faq tag-->
                                    @include('admin/tour/tour-faq-list')
                                </li>
                                <li>
                                    <!--payment tab-->
                                    <p>Payment data not available.</p>
                                </li>
                                <li>
                                    <div class="uk-width-large-1-2 m-t-15" style="margin-left: 25%;">
                                        <div class="timeline timeline_small uk-margin-bottom">
                                        @foreach($tour_history as $TourHiatory)
                                            <div class="timeline_item">
                                            <div class="timeline_icon {{($TourHiatory->type=='remove')?'timeline_icon_danger':'timeline_icon_success'}}"><i class="material-icons">{{$TourHiatory->type}}</i></div>
                                            <div class="timeline_date">
                                            <span>{{date('d M Y h:i A',strtotime($TourHiatory->created_at))}}</span>
                                            </div>
                                            <div class="timeline_content">{{$TourHiatory->message}} by {{$TourHiatory->first_name}} {{$TourHiatory->last_name}}.</div>
                                            </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--add Booking model-->
    <div class="uk-modal uk-modal-card-fullscreen" id="booking_form_model">
        <div class="uk-modal-dialog uk-modal-dialog-blank">
            <div class="md-card uk-height-viewport">
                <div class="md-card-toolbar">
                    <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                    <h3 class="md-card-toolbar-heading-text">
                        Add Booking
                    </h3>
                </div>
                <div class="md-card-content">
                    <div class="p-t-30">
                        <form method="get">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Lead ID</label>
                                                <input type="text" class="md-input" name="name" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <label>Departure Date</label>
                                                <input class="md-input" type="text" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <select class="md-input">
                                                    <option value="" selected="">Chooose Hotel Type</option>
                                                    <option>Standard</option>
                                                    <option>Deluxe</option>
                                                    <option>Super Delux</option>
                                                </select>
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <label>No. Of Adults</label>
                                                <input type="text" class="md-input" name="name" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>No. Of Child Tourist</label>
                                                <input type="text" class="md-input" name="name" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <label>No. Of Roomns</label>
                                                <input type="text" class="md-input" name="name" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Invoice Name</label>
                                                <input type="text" class="md-input" name="name" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <label>GST Name</label>
                                                <input type="text" class="md-input" name="name" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>GST Address</label>
                                                <input type="text" class="md-input" name="name" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-width-medium-1-12">
                                                        <div class="uk-form-row parsley-row p-t-10">
                                                            <label for="payment_mode_lbl" class="uk-form-label check-box-lbl">Payment Mode</label>
                                                            <span class="icheck-inline">
                                                                <input type="radio" name="payment_mode" id="offline_mode" data-md-icheck required />
                                                                <label for="offline_mode" class="inline-label">OffLine</label>
                                                            </span>
                                                            <span class="icheck-inline">
                                                                <input type="radio" name="payment_mode" id="online_mode" data-md-icheck />
                                                                <label for="online_mode" class="inline-label">OnLine</label>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row parsley-row">
                                                        <label for="gender" class="uk-form-label check-box-lbl">Hotel Update</label>
                                                        <span class="icheck-inline">
                                                            <input type="radio" name="hotel_update" id="hotel_update_yes" data-md-icheck required />
                                                            <label for="hotel_update_yes" class="inline-label">Yes</label>
                                                        </span>
                                                        <span class="icheck-inline">
                                                            <input type="radio" name="hotel_update" id="hotel_update_no" data-md-icheck />
                                                            <label for="hotel_update_no" class="inline-label">No</label>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="p-t-30">
                        <div class="uk-margin-medium-bottom">
                            <table class="uk-table" id="supplier_tbl">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Birth Date</th>
                                        <th>Mobile No</th>
                                        <th class="hidden-phone">Email ID</th>
                                        <th class="hidden-phone">Passport No</th>
                                        <th class="hidden-phone">Expiry Date</th>
                                        <th class="hidden-phone">Gender</th>
                                        <th class="hidden-phone">Child</th>
                                        <th class="hidden-phone">Amount</th>
                                        <th class="hidden-phone">Cancellation Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" class="md-input" name="name" placeholder="Name" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Birth Date" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Mobile No" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Email ID" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Passport No" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Expiry Date" /></td>
                                        <td>
                                            <select class="md-input">
                                                <option value="" selected="">Gender</option>
                                                <option>Male</option>
                                                <option>Female</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Child" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Amount" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Cancellation Amount" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="md-input" name="name" placeholder="Name" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Birth Date" id="uk_dp_1" data-uk-datepicker="{format:'DD MMMM YYYY'}" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Mobile No" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Email ID" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Passport No" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Expiry Date" /></td>
                                        <td>
                                            <select class="md-input">
                                                <option value="" selected="">Gender</option>
                                                <option>Male</option>
                                                <option>Female</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Child" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Amount" /></td>
                                        <td><input type="text" class="md-input" name="name" placeholder="Cancellation Amount" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="p-t-30">
                        <form method="get">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Booking Note</label>
                                                <textarea id="wysiwyg_ckeditor" cols="30" rows="20" name="notes"></textarea>
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>Total</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>Discount</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>Other Charges</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>Sub Total</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>CGST</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>SGST</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>IGST</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>Total Payable</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12 p-r-50">
                                                                <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                    <label>Amount</label>
                                                                    <input class="md-input" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <hr class="form_hr">
                                                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                                                        <div class="uk-width-1-1">
                                                            <a href="javascript:;" class="md-btn md-btn-danger">Cancel</a>
                                                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                                                        </div>
                                                    </div> 
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--hotel room booking-->
    @include('admin/tour/tour-hotel-manage')

    <!--faq model-->
    @include('admin/tour/tour-faq-manage')

    <!--paymnt model-->
    <div class="uk-modal uk-modal-card-fullscreen" id="payment_form_model">
        <div class="uk-modal-dialog uk-modal-dialog-blank">
            <div class="md-card uk-height-viewport">
                <div class="md-card-toolbar">
                    <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                    <h3 class="md-card-toolbar-heading-text">
                        Add Payment
                    </h3>
                </div>
                <div class="md-card-content">
                    <div class="p-t-30">
                        <form class="" method="get">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Lead ID</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Customer Name</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Departure Date</label>
                                                <input type="text" class="md-input" data-uk-datepicker="{format:'DD MMMM YYYY'}" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Total Amount</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Payed Amount</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Balance Amount</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr class="form_hr">
                            <div class="uk-grid uk-margin-medium-top uk-text-right">
                                <div class="uk-width-1-1">
                                    <button type="submit" class="md-btn md-btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Itinerary model-->
    @include('admin/tour/tour-itinerary-manage')
</div>
@endsection


    @section('javascript')
    <link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker-multi/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker-multi/css/bootstrap-datepicker3.min.css" />

    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ckeditor/ckeditor.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ckeditor/adapters/jquery.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_wysiwyg.min.js"></script>
    <!--  form file input functions -->
    <!--<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>-->

    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/wizard_steps.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_wizard.min.js"></script>

    <script>
        altair_forms.parsley_validation_config();
        altair_forms.parsley_extra_validators();
    </script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/parsleyjs/dist/parsley.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/instafilta.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/page_invoices.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/jquery.waitforimages.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/page_gallery.min.js"></script>


    <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker-multi/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" >
        $(function () {
            if (location.hash && location.hash.length) {
                var hash = decodeURIComponent(location.hash.substr(1));
                setTimeout(function () {
                    $("ul#user_profile_tabs li." + hash).trigger("click");
                }, 1000);
            }
        });
    </script>


    <!--  dropify -->
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_advanced.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

    <script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}js/dropzone.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form#tour_frm").validate({
                rules: {
                    tour_name: "required",
                    tour_code: "required",
                    tour_nights: "required",
                },
                messages: {
                    tour_name: "Please enter tour name",
                    tour_code: "Please enter tour code",
                    tour_nights: "Please enter number of nights",
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                        error.insertAfter($(element).parents('div.status-column'));
                    } else if (element.attr("type") == "file") {
                        error.insertAfter($(element).parents('div.dropify-wrapper'));
                    } else if (element.is('select')) {
                        error.insertAfter($(element).parents('div.md-input-wrapper'));
                    } else {
                        error.insertAfter($(element).parents('div.md-input-wrapper'));
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });

            $(".onlynumber").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
    <script type="text/javascript">
        var total_photos_counter = 0;
        var name = "";
        var baseUrl = "{{ route('tour.images.store') }}";
        var token = "{{ csrf_token() }}";
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("div#dropzoneFileUpload", {
            url: baseUrl,
            params: {
                _token: token,
                tour_temp_id: $('#tour_temp_id').val()
            },
            uploadMultiple: true,
            parallelUploads: 2,
            maxFilesize: 16,
            previewTemplate: document.querySelector('#preview').innerHTML,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove file',
            dictFileTooBig: 'Image is larger than 16MB',
            timeout: 10000,
            renameFile: function (file) {
                name = new Date().getTime() + Math.floor((Math.random() * 100) + 1) + '_' + file.name;
                return name;
            },
            init: function () {
                var myDropzone = this;
                $.get("{{ route('tour.images.fetch', Request::route('id')) }}", function (data) {
                    //console.log(data);
                $.each(data.images, function (key, value) {
                    var file = {name: value.original, size: value.size};
                    myDropzone.options.addedfile.call(myDropzone, file);
                    myDropzone.options.thumbnail.call(myDropzone, file, value.server);
                    myDropzone.emit("complete", file);
                    total_photos_counter++;
                    file.customName = value.original;
                    $("#counter").text("(" + total_photos_counter + ")");
                });
            });
            this.on("removedfile", function (file) {
                //console.log(file.customName);
                    $.post({
                        url: "{{ route('tour.images.destroy')}}",
                        data: {
                            _token: token,
                            id: file.customName,
                            //tour_temp_id: $('#tour_temp_id').val()
                        },
                        dataType: 'json',
                        success: function (data) {
                            total_photos_counter--;
                            $("#counter").text("# " + total_photos_counter);
                        }
                    });
                });
            },
            success: function (file, done) {
                total_photos_counter++;
                $("#counter").text("#" + total_photos_counter);
                file.customName = file.upload.filename;
            }
        });
    </script>
    <script type="text/javascript">
        $(function () {

            var tourTourTable = $('#tour_quotation_tbl').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                pageLength: 20,
                lengthMenu: [ 10, 20, 50, 75, 100 ],
                searching: true,
                //order: [[ 0, "desc" ]],
                "ajax": {
                    "url": "{{ route('tour.quotation.data') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}",'tourId':{{$tour->id}}}, 
                    beforeSend: function () {

                        if (typeof tourTourTable != 'undefined' && tourTourTable.hasOwnProperty('settings')) {
                            tourTourTable.settings()[0].jqXHR.abort();
                        }
                    }
                },
                "columns": [
                    {data: "quatation_id", name: "quatation_id"},
                    {data: "quatation_date", name: "quatation_date"},
                    {data: "validup_date", name: "validup_date"},
                    {data: "subject", name: "subject"},
                    {data: "members", name: "members"},
                    {data: "total_amount", name: "total_amount"},
                    {data: "status_id", name: "status_id"},
                    {data: "action", 'sortable': false, "render": function (data, type, row) {
                            var edit_url = "{{ route('tour.features.edit', ':id') }}";
                            var quotation_edit = "{{ route('quotation.edit', ['lead_id' => ':lead_id', 'id' => ':id']) }}";
                            var quotation_delete = "{{ route('quotation.delete', ['id' => ':id']) }}";
                            quotation_edit = quotation_edit.replace(':lead_id', row.lead_id).replace(':id', row.quaId);
                            quotation_delete = quotation_delete.replace(':id', row.quaId);

                            return '<a href="' + quotation_edit + '" title="Edit Quotation" class=""><i class="md-icon material-icons">&#xE254;</i></a> ';
                        },
                    },
                ]
            });


            $(tourTourTable.table().container()).on('keyup change', 'tfoot input', function () {
                tourTourTable
                        .column($(this).attr('colPos'))
                        .search(this.value)
                        .draw();
            });

            $(tourTourTable.table().container()).on('change', 'tfoot select', function () {
                tourTourTable
                        .column($(this).attr('colPos'))
                        .search(this.value)
                        .draw();
            });

        });

        // $(document).ready(function() {
        //     $('#dt_default_hotel').DataTable( {
        //         "pageLength": 20,
        //         "lengthMenu": [ 10, 20, 50, 75, 100 ],
        //     } );
        // });

        // $(document).ready(function() {
        //     $('#dt_default_price').DataTable( {
        //         "pageLength": 20,
        //         "lengthMenu": [ 10, 20, 50, 75, 100 ],
        //     } );
        // });
    </script>
    
    <script type="text/javascript">
        //$(function () {
        //         var tourTourTable = $('#tour_quotation_tbl').DataTable({
        //             processing: true,
        //             serverSide: true,
        //             destroy: true,
                    // pageLength: 20,
                    // lengthMenu: [ 10, 20, 50, 75, 100 ],
        //             searching: true,
        //             order: [[ 1, "desc" ]],
        // //            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        // //            "pagingType": "full_numbers", //full,full_numbers
        //             "ajax": {
        //                 "url": "{{ route('tour.quotation.data') }}",
        //                 "dataType": "json",
        //                 "type": "POST",
        //                 "data": {_token: "{{csrf_token()}}",'tourId':{{$tour->id}}}, //, 'category_name': $("#Scategory_name").val()
        //                 beforeSend: function () {

        //                     if (typeof tourTourTable != 'undefined' && tourTourTable.hasOwnProperty('settings')) {
        //                         tourTourTable.settings()[0].jqXHR.abort();
        //                     }
        //                 }
        //             },
        //             "columns": [
        //                 {"data": "quaId", "name": "lead_quatation.quaId"},
        //                 {"data": "quatation_date", "name": "lead_quatation.quatation_date"},
        //                 {"data": "validup_date", "name": "lead_quatation.validup_date"},
        //                 {"data": "subject", "name": "lead_quatation.subject"},
        //                 {"data": "adult", "name": "lead_quatation.adult"},
        //                 {"data": "total_amount", "name": "lead_quatation.total_amount"},
        //                 {"data": "status_id", "name": "lead_quatation.status_id"},
        // //                {"data": "status_id", "name": "lead_quatation.status_id", "render": function (data, type, row) {
        // //                        if (row.status_id == 1) {
        // //                            return '<a class="change-status-btn" data-id="' + row.status_id + '" row-id="' + row.typeId + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a>';
        // //                        } else {
        // //                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.typeId + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-danger">InActive</span></a>';
        // //                        }
        // //                    },
        // //                },
        //                 {"data": "action", 'sortable': false, "render": function (data, type, row) {
        //                         var edit_url = "{{ route('tour.features.edit', ':id') }}";
        //                         var quotation_edit = "{{ route('quotation.edit', ['lead_id' => ':lead_id', 'id' => ':id']) }}";
        //                         var quotation_delete = "{{ route('quotation.delete', ['id' => ':id']) }}";
        //                         quotation_edit = quotation_edit.replace(':lead_id', row.lead_id).replace(':id', row.quaId);
        //                         quotation_delete = quotation_delete.replace(':id', row.quaId);

        //                         return '<a href="' + quotation_edit + '" title="Edit Quotation" class=""><i class="md-icon material-icons">&#xE254;</i></a> ';
        // //                        return '<a href="' + quotation_edit + '" title="Edit Quotation" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + quotation_delete + '" data-id="' + row.quaId + '"  title="Delete Quotation" class="quotation-delete"><i class="md-icon material-icons">delete</i></a>';
        //                     },
        //                 },
        //             ]
        //         });
        </script>
    @endsection


