@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Tour Price Type</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}tour/price-type/add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 15%;">Price Type</th>
                        <th style="width: 60%;" class="hidden-phone">Description</th>
                        <th style="width: 10%;" class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
<!--                <tfoot>
                    <tr>
                        <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                        <td><input type="text"  placeholder="Price Type" name="" colPos="1"></td>
                        <td><input type="text"  placeholder="Discription" name="" colPos="1"></td>
                        <td>
                            <select class="chosen" data-placeholder="Choose a Status" tabindex="1" colPos="6">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="2">InActive</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                </tfoot>-->
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>Price Type <?php echo $i; ?></td>
                            <td class="hidden-phone">Description</td>
                            <td ><span class="label label-success">Active</span></td>
                            <td>
                                <a href="{{ env('ADMIN_URL')}}tour/price-type/edit/1" title="Edit Price Type" class=""><i class="md-icon material-icons">&#xE254;</i></a> 
                                <a href="{{ env('ADMIN_URL')}}tour/price-type/delete/1" onclick="return confirm('Are you sure to delete?');" title="Delete Price Type" class=""><i class="md-icon material-icons">delete</i></a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection



