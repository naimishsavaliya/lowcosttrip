<div class="uk-modal uk-modal-card-fullscreen" id="faq_form_model">
    <div class="uk-modal-dialog uk-modal-dialog-blank">
        <div class="md-card uk-height-viewport">
            <div class="md-card-toolbar">
                <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                <h3 class="md-card-toolbar-heading-text">
                    FAQ
                </h3>
            </div>
            <div class="md-card-content">
                <div class="p-t-30">
                    <form class="uk-form-stacked" action="{{ route('tour.faq.store')}}" id="tour_faq_form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ isset($tour->id) ? $tour->id : '' }}" />
                        <input type="hidden" name="faq_id" id="faq_id" value="" />
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-form-row">
                                    <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Question</label>
                                            <input type="text" name="question" id="question" required="" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear clearfix m-b-10"> </div><br/>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-form-row">
                                    <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Answer</label>
                                            <textarea cols="30" rows="4" class="md-input" name="answer" id="answer" required=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clear clearfix m-b-10"> </div><br/>


                            <div class="uk-width-medium-1-1">
                                <div class="uk-form-row">
                                    <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="status" value="1" id="active" data-md-icheck checked="" />
                                                    <label for="active" class="inline-label">Active</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="status" value="0" id="inactive" data-md-icheck />
                                                    <label for="inactive" class="inline-label">In Active</label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="form_hr">
                        <div class="uk-grid uk-margin-medium-top uk-text-right">
                            <div class="uk-width-1-1">
                                <button type="submit" class="md-btn md-btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>