<div class="uk-modal uk-modal-card-fullscreen" id="itenary_form_model">
    <div class="uk-modal-dialog uk-modal-dialog-blank">
        <div class="md-card uk-height-viewport">
            <div class="md-card-toolbar">
                <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                <h3 class="md-card-toolbar-heading-text">
                    Add Itinerary
                </h3>
            </div>
            <div class="md-card-content">
                <div class="p-t-30">
                    <form class="uk-form-stacked" action="{{ route('tour.itinerary.store')}}" id="tour_itinerary_form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ isset($tour->id) ? $tour->id : '' }}" />
                        <section>
                            <div data-dynamic-fields="manage-itinerary" class="uk-grid" data-uk-grid-margin>
                            </div>
                        </section>


                        <script id="manage-itinerary" type="text/x-handlebars-template" dynamic-fields-counter="5">
                        <div class="uk-width-medium-1-1 parsley-row form_section m-t-10">
                            <input type="hidden" name="cnt[]" value="@{{counter}}" />
                            <div class="uk-input-group uk-grid" style="width:100%">
                                <div class="uk-width-medium-1-3 p-t-8">
                                    <div class="uk-form-row">
                                        <label for="invoice_dp">Day</label>
                                        <input class="md-input" type="text" value=""  name="tour[tour_day][]" id="tour_day@{{counter}}" required>
                                    </div>
                                    <div class="uk-form-row">
                                        <label for="inv_service">Title</label>
                                        <input type="text" name="tour[title][]" id="title@{{counter}}" class="md-input" required />
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <label for="inv_service_rate">Description</label>
                                    <textarea cols="30" name="tour[description][]" id="description@{{counter}}" rows="4" class="md-input" required></textarea>
                                </div>
                                <div class="uk-width-medium-1-6" style="margin-top:7%;">
                                    <div class="uk-form-file md-btn md-btn-primary">
                                        Select image
                                        <input id="image[]" name="image[]" type="file" accept="image/*">
                                    </div>
                                    <span class="uk-input-group-addon" style="padding: 0;float: right;display: inline-block;">
                                        <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        </script>
                        <hr class="form_hr">
                        <div class="uk-grid uk-margin-medium-top uk-text-right">
                            <div class="uk-width-1-1">
                                <button type="submit" class="md-btn md-btn-primary">Save</button>
                            </div>                                                        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("form#tour_itinerary_form").validate({
            rules: {
                tour_day: "required",
                title: "required",
                description: "required",
                image: {
                    accept: "jpg,png,jpeg,gif"
                },
            },
            messages: {
                tour_day: "Please enter tour day",
                title: "Please enter Itinerary title",
                description: "Please enter description",
                image: {
                    accept: "Only image type jpg/png/jpeg/gif is allowed"
                },
            },
            errorPlacement: function (error, element) {
                if (element.attr("type") == "radio") {
                    error.insertAfter($(element).parents('div.status-column'));
                } else if (element.attr("type") == "file") {
                    error.insertAfter($(element).parents('div.dropify-wrapper'));
                } else if (element.is('select')) {
                    error.insertAfter($(element).parents('div.md-input-wrapper'));
                } else {
                    error.insertAfter($(element).parents('div.md-input-wrapper'));
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>