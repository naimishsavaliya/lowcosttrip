@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li>Tour Name</li>
            <li><span>Itinerary</span></li>
        </ul>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')


            <form class="uk-form-stacked" id="wizard_advanced_form">


                <section>
                    <div data-dynamic-fields="d_field_wizard_1" class="uk-grid" data-uk-grid-margin>
                        
                    </div>
                </section>


                <script id="d_field_wizard_1" type="text/x-handlebars-template">
                    <div class="uk-width-medium-1-1 parsley-row form_section">
                    <div class="uk-input-group uk-grid" style="width:100%">
                    <div class="uk-width-medium-1-3 p-t-8">
                    <div class="uk-form-row">
                    <label for="invoice_dp">Day 101</label>
                    <input class="md-input" type="text" id="invoice_dp" value=""  name="wizard_address101"                                                 id="wizard_address1">
                    </div>
                    <div class="uk-form-row">
                    <label for="inv_service">Title</label>
                    <input type="text" name="wizard_address101" id="wizard_address101" class="md-input" id="inv_service" name="inv_service_id" />
                    </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                    <label for="inv_service_rate">Description</label>
                    <textarea cols="30" name="wizard_address101" id="wizard_address101" rows="4" class="md-input"></textarea>
                    </div>
                    <div class="uk-width-medium-1-6" style="margin-top:7%;">
                    <div class="uk-form-file md-btn md-btn-primary">
                    Select image
                    <input id="form-file" type="file">
                    </div>
                    <span class="uk-input-group-addon" style="padding: 0;float: right;display: inline-block;">
                    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                    </span>
                    </div>
                    </div>
                    </div>
                </script>
            </form>
        </div>
    </div>

    <?php /*


      <link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/css/datepicker.css" />
      <link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
      <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
      <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
      <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
      <script type="text/javascript">
      $(function () {
      $('#term-condition').wysihtml5({"font-styles": false});
      $('#description').wysihtml5({"font-styles": false});

      $('.date-picker').datepicker({'autoclose': true});
      });

      function removedata(obj) {
      var conf = confirm('Are you sure to delete?');
      if (conf) {
      $(obj).parents('.branch-items').remove();
      }
      }

      function cloneData() {
      var clonedata = $(".cloneData").clone();
      clonedata.removeClass("cloneData");

      var new_clone_data = '';
      new_clone_data += '<div class="branch-items">';
      new_clone_data += '<a href="javascript:;" onclick="removedata(this);" title="Delete Price" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>';
      new_clone_data += '<div class="clear clearfix"></div>';
      new_clone_data += $(clonedata).html();
      new_clone_data += '</div>';
      $(".appendDiv").append(new_clone_data);
      }

      </script>
     * 
     */ ?>

    @endsection
    @section('javascript')
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/wizard_steps.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_wizard.min.js"></script>

    <script>
// load parsley config (altair_admin_common.js)
        altair_forms.parsley_validation_config();
// load extra validators
        altair_forms.parsley_extra_validators();
    </script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/parsleyjs/dist/parsley.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/instafilta.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/page_invoices.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/jquery.waitforimages.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/page_gallery.min.js"></script>
    @endsection