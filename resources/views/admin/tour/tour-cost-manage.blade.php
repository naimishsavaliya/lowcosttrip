@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><a href="{{route('tour.detail',$tour->id)}}">{{$tour->tour_name}}</a></li>
            <li><span>Manage Cost</span></li>
        </ul>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <form class="uk-form-stacked" action="{{ route('tour.cost.store')}}" id="tour_itinerary_form" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ isset($tour->id) ? $tour->id : '' }}" />

                <div data-dynamic-fields="cost_field_wizard" class="uk-grid" data-uk-grid-margin>

                </div>
                <hr class="form_hr">
                <div class="uk-grid uk-margin-medium-top uk-text-right">
                    <div class="uk-width-1-1">
                        <button type="submit" class="md-btn md-btn-primary">Save</button>
                    </div>
                </div>

            </form>


<script id="cost_field_wizard" type="text/x-handlebars-template">
            <div class="uk-width-medium-1-1 parsley-row form_section">
                <div class="uk-input-group uk-grid" style="width:100%">
                    <div class="uk-width-1-1">
                        <div class="">
                            <div class="uk-grid" data-uk-grid-margin="">
                                <div class="uk-width-medium-1-4 uk-width-1-1 uk-row-first">
                                    <label for="date_from">Date From</label>
                                    <input class="md-input" type="text" id="date_from" name="date_from[]" autocomplete="off" value="" required data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                </div>
                                <div class="uk-width-medium-1-4 uk-width-medium-1-1">
                                    <label for="date_to">Date To</label>
                                    <input class="md-input" type="text" id="date_to" name="date_to[]" autocomplete="off" value="" required data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                </div>
                                <div class="uk-width-medium-1-4 uk-width-medium-1-1">
                                    <label for="discount">Discount %</label>
                                    <input class="md-input" type="text" id="discount" name="discount[]" autocomplete="off" value="">
                                </div>
                                <div class="uk-width-medium-1-4 uk-width-medium-1-1">
                                    <select class="md-input" name="currency_id[]" id="currency_id" data-uk-tooltip="{pos:'top'}" title="Choose a currency">
                                        <option value="" selected>Choose a currency</option>
                                        @if(isset($currencys))
                                        @foreach($currencys as $currency)
                                        <option value="{{ $currency->currId }}">{{ $currency->currency_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small class="text-danger">{{ $errors->first('currency_id') }}</small>
                                     
                                </div>
                                <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div>
                                <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div> <br/>


                                <div class="uk-width-medium-1-1">
                                    <table class="uk-table uk-table-hover">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                @foreach($tour_package_type as $type)
                                                <th>{{$type->name}}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($tour_rate_master as $rate)
                                            <tr>
                                                <td>{{$rate->name}}</td>
                                                @foreach($tour_package_type as $type)
                                                <td>
                                                    <div class="uk-width-medium-1-2 uk-float-left">
                                                        <div class="md-input-wrapper">
                                                            <label>Cost</label>
                                                            <input type="text" class="md-input" name="cost[@{{counter}}][{{$rate->typeId}}-{{$type->typeId}}][]" id="" value=""><span class="md-input-bar "></span>
                                                        </div>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 uk-float-left">
                                                        <div class="md-input-wrapper uk-width-medium-1-2">
                                                            <label>Sell</label>
                                                            <input type="text" name="sell[@{{counter}}][{{$rate->typeId}}-{{$type->typeId}}]" class="md-input"><span class="md-input-bar "></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>


                                <div class="uk-width-medium-1-10">
                                    <span class="uk-input-group-addon">
                                        <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </script>
        </div>
    </div>
</div>
@endsection


@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>
@endsection