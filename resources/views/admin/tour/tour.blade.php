@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>
    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')
            <table class="uk-table width-100" id="tour_list">
                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Tour Code</th>
                        <th>Tour Name</th>
                        <th>Tour Type</th>
                        <th>Supplier</th>
                        <th>Leads</th>
                        <th>Confirm Leads</th>
                        <th>Travelers</th>
                        <th>Tour Duration</th> 
                        <th>Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody></tbody>   
            </table>
        </div>
    </div>
</div>
<div id="changestatus" class="uk-modal" >
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Status</h3>
        </div>
        <div class="">
            <div class="control-group">
                <input type="hidden" value="" id="change-status-row-id" />
                <label class="control-label">Status</label>
                <div class="controls">
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="1" id="status_dialog_active" data-md-icheck />
                        <label for="status_dialog_active" class="inline-label">Active</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="0" id="status_dialog_inactive" data-md-icheck />
                        <label for="status_dialog_inactive" class="inline-label">InActive</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close" id="status-dlg-btn">Submit</button> 
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(function () {
        var usector = @php echo json_encode($userSector); @endphp;
        var uRoleID = {{\Auth::user()->role_id}};
        var uID = {{\Auth::user()->id}};
        var tourTable = $('#tour_list').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            autoWidth: false,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            searching: true,
            order: [[ 0, "desc" ]],
            "ajax": {
                "url": "{{ route('tour.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"},
                beforeSend: function () {

                    if (typeof tourTable != 'undefined' && tourTable.hasOwnProperty('settings')) {
                        tourTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {data: "id", name: "id"},
                {data: "tour_code", name: "tour_code"},
                {data: "tour_name", name: "tour_name"},
                {data: "domestic_international", name: "domestic_international"},
                {data: "supplier_id", name: "supplier_id"},
                {data: "total_lead", name: "total_lead"},
                {data: "total_confirmed_lead", name: "total_confirmed_lead"},
                {data: "total_travelers", name: "total_travelers"},
                {data: "nights", name: "nights"},
                {data: "tour_status", name: "tour.tour_status", "render": function (data, type, row) {
                        if (row.tour_status == 1) {
                            return '<a class="change-status-btn" data-id="' + row.tour_status + '" row-id="' + row.id + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a>';
                        } else {
                            return '<a class="change-status-btn" data-id="' + row.tour_status + '" row-id="' + row.id + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-danger">InActive</span></a>';
                        }
                    },
                },
                {data: "action", 'sortable': false, "render": function (data, type, row) {
                        var detail_url = "{{ route('tour.detail', ':id') }}";
                        var dele_url = "{{ route('tour.delete', ':id') }}";
                        detail_url = detail_url.replace(':id', row.id);
                        dele_url = dele_url.replace(':id', row.id);
                        var tour_delete = "{{ route('tour.delete', ':id') }}";
                        tour_delete = tour_delete.replace(':id', row.id);
                        
                        
                        var LinkString = '<div class="uk-button-dropdown" data-uk-dropdown="{pos:\'bottom-right\'}"><button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button> <div class="uk-dropdown"> <ul class="uk-nav uk-nav-dropdown">';
                         LinkString += ' <li><a href="'+detail_url+'"> View/Edit</a></li>';
                         <?php /*
                         @can('delete_tour')
                         @if(\Auth::user()->role_id == 1)
                          LinkString += ' <li><a href="'+dele_url+'" onclick="return confirm(\'Are you sure to delete?\');"> Delete</a></li>';
                         @else 
                             if((uRoleID==2 &&  usector.length > 0 && jQuery.inArray(row.sector, usector) != -1) ||(uID==row.created_by)){
                               LinkString += ' <li><a href="'+dele_url+'" onclick="return confirm(\'Are you sure to delete?\');"> Delete</a></li>';
                              }
                         @endif
                         @endcan
                         */?>
                         LinkString += '<li><a href="' + tour_delete + '" title="Delete Tour" class="delete-tours" data-id="' + row.id + '">Delete</a></li>';
                         LinkString += ' <li><a href="'+detail_url+'#hotel">Manage Hotels</a></li><li><a href="'+detail_url+'#itinerary">Manage Itinerary</a></li> <li><a href="'+detail_url+'#departure">Manage Departure Date</a></li> <li><a href="'+detail_url+'#cost">Manage Cost</a></li> <li><a href="'+detail_url+'#faq">Faq</a></li>';
                         //LinkString += '<div class="uk-button-dropdown" data-uk-dropdown="{pos:\'bottom-right\'}"><button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button> <div class="uk-dropdown"> <ul class="uk-nav uk-nav-dropdown"> <li><a href="'+detail_url+'"> View/Edit</a></li> <li><a href="'+dele_url+'" onclick="return confirm(\'Are you sure to delete?\');"> Delete</a></li><li><a href="'+detail_url+'#hotel">Manage Hotels</a></li><li><a href="'+detail_url+'#itinerary">Manage Itinerary</a></li> <li><a href="'+detail_url+'#departure">Manage Departure Date</a></li> <li><a href="'+detail_url+'#cost">Manage Cost</a></li> <li><a href="'+detail_url+'#faq">Faq</a></li> </ul> </div></div>';
                         LinkString += '  </ul> </div></div>';
                        return LinkString;
                        //return '<a href="' + edit_url + '" title="Edit Home Banner" class=""><i class="md-icon material-icons">&#xE254;</i></a> <a href="' + dele_url + '" onclick="return confirm(\'Are you sure to delete?\');" title="Delete Home Banner" class=""><i class="md-icon material-icons">delete</i></a>';
                    },
                },
            ]
        });

        $(tourTable.table().container()).on('keyup', 'tfoot input', function () {
            tourTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(tourTable.table().container()).on('change', 'tfoot select', function () {
            tourTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(document.body).on('click', '.change-status-btn', function () {
            var $obj = $(this);
            var row_id = $obj.attr('row-id');
            var status = $obj.attr('data-id');
            $('input#change-status-row-id').val(row_id);
            if (status == "1") {
                $('#status_dialog_inactive').removeAttr('checked');
                $('#status_dialog_active').iCheck('check');
            } else {
                $('#status_dialog_active').removeAttr('checked');
                $('#status_dialog_inactive').iCheck('check');
            }
        });

        $(document.body).on('click', '#status-dlg-btn', function () {
            var $obj = $(this);
            var row_id = $('#change-status-row-id').val();
            var status = $('input.user-status-change:checked').val();
            $.ajax({
                type: 'get',
                url: '{{ route("tour.change_status")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table": "tour",
                    "id": row_id,
                    "column_name": "id",
                    "status": status
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        tourTable.draw();
                    }
                    showNotify(data.msg_status, data.message);
                }
            });
        });

        $(document.body).on('click', '.delete-tours', function () {
            var crm = confirm("Are you sure to delete?");
            if (crm) {
                var $obj = $(this);
                var tour_id = $obj.attr('data-id');
                var delete_url = $obj.attr('href');
                $.ajax({
                    type: 'get',
                    url: delete_url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {

                    }, success: function (data, textStatus, jqXHR) {
                        if (data.success) {
                            tourTable.draw();
                        }
                        showNotify(data.msg_status, data.message);
                    }
                });
            }
            return false;
        });
    });
</script>
@endsection