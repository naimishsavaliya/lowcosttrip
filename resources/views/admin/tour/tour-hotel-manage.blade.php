<div class="uk-modal uk-modal-card-fullscreen" id="hotelroom_form_model">
    <div class="uk-modal-dialog uk-modal-dialog-blank">
        <div class="md-card uk-height-viewport">
            <div class="md-card-toolbar">
                <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                <h3 class="md-card-toolbar-heading-text">
                    Manage Hotel
                </h3>
            </div>
            <div class="md-card-content">
                <div class="p-t-30">
                    <form class="uk-form-stacked" action="{{ route('tour.hotel.store')}}" id="tour_hotel_form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ isset($tour->id) ? $tour->id : '' }}" />
                        <div class="uk-grid" data-uk-grid-margin>
                            @foreach($tour_package_type as $type)
                            <div class="uk-width-medium-1-1"><b>{{$type->name}}</b></div>
                            <div class="clear clearfix"> </div><br/>
                            <section class="uk-width-medium-1-1 ">
                                
                                <div data-dynamic-fields="manage-hotel-{{$type->typeId}}" class="uk-grid uk-width-medium-1-1" data-uk-grid-margin  >
                                    @foreach($tour_hotels as $tHotel)
                                    @if($tHotel->package_type != $type->typeId)
                                    @continue
                                    @endif
                                    <div class="uk-width-medium-1-1 parsley-row form_section m-t-10">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-3-10 p-r-10">
                                                <div class="md-input-wrapper md-input-filled">
                                                    <label>Location<span class="required-lbl">*</span></label>
                                                    <input type="text" class="md-input" id="location" name="update[{{$tHotel->id}}][location]" value="{{$tHotel->location}}" required=""><span class="md-input-bar "></span>
                                                    <input type="hidden" name="update[{{$tHotel->id}}][package_type]" value="{{$type->typeId}}" />
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-3-10 p-r-10">
                                                <div class="md-input-wrapper md-input-filled">
                                                    <label>Hotel Name <span class="required-lbl">*</span></label>
                                                    <input type="text" class="md-input" id="hotel_name" name="update[{{$tHotel->id}}][hotel]" value="{{$tHotel->hotel}}" required=""><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-2-10 p-r-10">
                                                <select id="" name="update[{{$tHotel->id}}][hotel_star]" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'bottom'}" title="Hotel Star">
                                                    <option value="">Hotel Star</option>
                                                    @for($s=1; $s<=5; $s++)
                                                    <option {{($tHotel->hotel_star==$s)?'selected':''}} value="{{$s}}">{{str_repeat("*", $s)}}</option>
                                                    @endfor
                                                </select>
                                                <span class="md-input-bar "></span>
                                            </div>
                                            <div class="uk-width-medium-1-10 p-r-10">
                                                <div class="md-input-wrapper md-input-filled">
                                                    <label>No. of night <span class="required-lbl">*</span></label>
                                                    <input type="text" class="md-input" id="night" name="update[{{$tHotel->id}}][night]" value="{{$tHotel->night}}" required=""><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-10">
                                                <div class="md-input-wrapper md-input-filled">
                                                    <span class="uk-input-group-addon" style="padding: 0;float: right;display: inline-block;">
                                                        <a href="#" class="btnSectionRemove"><i class="material-icons md-24">delete</i></a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear clearfix"> </div><br/>
                                @endforeach
                                </div>
                            </section>
                            
                            <script id="manage-hotel-{{$type->typeId}}" type="text/x-handlebars-template" >
                            <div class="uk-width-medium-1-1 parsley-row form_section m-t-10">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-3-10 p-r-10">
                                            <div class="md-input-wrapper ">
                                                <label>Location<span class="required-lbl">*</span></label>
                                                <input type="text" class="md-input" id="location" name="location[]" value="" ><span class="md-input-bar "></span>
                                                <input type="hidden" name="package_type[]" value="{{$type->typeId}}" />
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-3-10 p-r-10">
                                            <div class="md-input-wrapper">
                                                <label>Hotel Name <span class="required-lbl">*</span></label>
                                                <input type="text" class="md-input" id="hotel_name" name="hotel[]" value="" ><span class="md-input-bar "></span>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-2-10 p-r-10">
                                            <select id="select_demo_2" class="md-input" name="hotel_star[]" data-md-selectize data-md-selectize-bottom >
                                                <option value="" selected>Hotel Star</option>
                                                 @for($s=1; $s<=5; $s++)
                                                    <option value="{{$s}}">{{str_repeat("*", $s)}}</option>
                                                 @endfor
                                            </select>
                                            <span class="md-input-bar "></span>
                                        </div>
                                        <div class="uk-width-medium-1-10 p-r-10">
                                            <div class="md-input-wrapper">
                                                <label>No. of night <span class="required-lbl">*</span></label>
                                                <input type="text" class="md-input" id="night" name="night[]" value="" ><span class="md-input-bar "></span>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-10">
                                            <div class="md-input-wrapper md-input-filled">
                                                <span class="uk-input-group-addon" style="padding: 0;float: right;display: inline-block;">
                                                    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear clearfix"> </div><br/>
                            </script>
                            @endforeach

                        </div>
                        <hr class="form_hr">
                        <div class="uk-grid uk-margin-medium-top uk-text-right">
                            <div class="uk-width-1-1">
                                <a href="javascript:;" class="md-btn md-btn-danger uk-modal-close">Cancel</a>
                                <button type="submit" class="md-btn md-btn-primary">Save</button>
                            </div>
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>