@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li>Tour Name</li>
            <li><span>Manage Cost</span></li>
        </ul>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <form class="uk-form-stacked" id="wizard_advanced_form">

                <div data-dynamic-fields="d_field_wizard" class="uk-grid" data-uk-grid-margin></div>

            </form>
        </div>
    </div>
    @endsection

    <?php /*
      @extends('layouts.admin')
      @section('content')


      <div id="main-content">
      <div class="container-fluid">
      <div class="row-fluid">
      <div class="span12">
      <ul class="breadcrumb">
      <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
      <li><a href="javascript:;">Manage Tour Cost</a><span class="divider-last">&nbsp;</span></li>
      </ul>
      </div>
      </div>
      <div class="row-fluid">
      <div class="span12">
      <!-- BEGIN SAMPLE FORM widget-->
      <div class="widget">
      <div class="widget-title">
      <h4><i class="icon-reorder"></i>Manage Tour Cost</h4>
      <span class="tools">
      </span>
      </div>
      <div class="widget-body form">
      <!-- BEGIN FORM-->
      <form action="{{ env('ADMIN_URL')}}hotel/room" class="" method="get">
      <div class="cloneDiv cloneData">
      <div class="control-group span2">
      <label class="control-label">Standard Price</label>
      <div class="controls">
      <input type="text" class="span6 date-picker" placeholder="From" />
      <input type="text" class="span6 date-picker" placeholder="To" />
      </div>
      </div>
      <div class="control-group span2 ">
      <label class="control-label">Agent Subscription</label>
      <div class="controls">
      <select class="span12 chosen" data-placeholder="Choose a Subscription" tabindex="1">
      <option value=""></option>
      <option value="1">Subscription 1</option>
      <option value="2">Subscription 2</option>
      <option value="2">Subscription 3</option>
      <option value="2">Subscription 4</option>
      </select>
      </div>
      </div>
      <div class="control-group span1 ">
      <label class="control-label">1 Pax</label>
      <div class="controls">
      <input type="text" class="span12" />
      </div>
      </div>
      <div class="control-group span1 ">
      <label class="control-label">2 Pax</label>
      <div class="controls">
      <input type="text" class="span12" />
      </div>
      </div>
      <div class="control-group span1 ">
      <label class="control-label">3 Pax</label>
      <div class="controls">
      <input type="text" class="span12" />
      </div>
      </div>
      <div class="control-group span1 ">
      <label class="control-label">4 Pax</label>
      <div class="controls">
      <input type="text" class="span12" />
      </div>
      </div>
      <div class="control-group span1 ">
      <label class="control-label">1 Child</label>
      <div class="controls">
      <input type="text" class="span12" />
      </div>
      </div>
      <div class="control-group span1 ">
      <label class="control-label">2 Child</label>
      <div class="controls">
      <input type="text" class="span12" />
      </div>
      </div>
      <div class="control-group span1 ">
      <label class="control-label">3 Child</label>
      <div class="controls">
      <input type="text" class="span12" />
      </div>
      </div>
      <div class="control-group span1 ">
      <label class="control-label">&nbsp;</label>
      <div class="controls">
      <a onclick="cloneData();" title="Delete Price" class="btn btn-mini btn-success addPrice"><i class="icon-plus icon-white"></i></a>
      <a href="javascript:;" onclick="removedata(this);" title="Delete Price" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
      </div>
      </div>
      <div class="clear clearfix"></div>
      </div>
      <div class="appendDiv">
      </div>




      <div class="clear clearfix"></div>


      <div class = "form-actions">
      <button type = "submit" class = "btn btn-success">Submit</button>
      <button type = "button" class = "btn">Cancel</button>
      </div>
      </form>
      <!--END FORM-->
      </div>
      </div>
      <!--END SAMPLE FORM widget-->
      </div>
      </div>
      </div>
      </div>
      <script type = "text/javascript" >
      $(function () {
      $('.date-picker').datepicker({'autoclose': true});

      //        $(".addPrice").on('click', function () {
      //            $(".appendDiv").append($(".cloneDiv").clone());
      //        });
      });
      function removedata(obj) {
      var conf = confirm('Are you sure to delete?');
      if (conf) {
      $(obj).parents('.cloneDiv').remove();
      }
      }
      function cloneData() {
      var clonedata = $(".cloneData").clone();
      clonedata.removeClass("cloneData");
      $(".appendDiv").append(clonedata);

      $('.date-picker').datepicker({'autoclose': true});
      }
      </script>
      <link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/css/datepicker.css" />
      <script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
      @endsection
     * 
     */ ?>

    <script id="d_field_wizard" type="text/x-handlebars-template">
    <div class="uk-width-medium-1-1 parsley-row form_section">
        <div class="uk-input-group uk-grid" style="width:100%">
            <div class="uk-width-1-1">
                <div class="">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-4 uk-width-1-1 uk-row-first">
                            <label for="invoice_dp">Date From</label>
                            <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
                        </div>
                        <div class="uk-width-medium-1-4 uk-width-medium-1-1">
                            <label for="invoice_dp">Date To</label>
                            <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
                        </div>
                        <div class="uk-width-medium-1-4 uk-width-medium-1-1">
                            <label for="discount">Discount %</label>
                            <input class="md-input" type="text" id="discount" value="">
                        </div>
                        <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div>
                        <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div> <br/>


                        <div class="uk-width-medium-1-1">
                            <table class="uk-table uk-table-hover">
                                <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Standard</th>
                                        <th>Deluxe</th>
                                        <th>Super Deluxe</th>
                                        <th>Luxury</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Per Person (On Twin Sharing)</td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Per Couple</td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Child with bed (2 to 11)</td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper">
                                                    <label>Cost</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-1-2 uk-float-left">
                                                <div class="md-input-wrapper uk-width-medium-1-2">
                                                    <label>Sell</label>
                                                    <input type="text" class="md-input"><span class="md-input-bar "></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div class="uk-width-medium-1-10">
                            <span class="uk-input-group-addon">
                                <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </script>

    @section('javascript')
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>
    @endsection