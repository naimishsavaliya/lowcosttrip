<div class="uk-grid" data-uk-grid-margin="">
    <div class="uk-width-large-1-6 uk-width-1-1 uk-row-first" style="padding-top: 10px;">
        <h3>Filter By Year</h3>
    </div>
    <div class="uk-width-large-2-10 uk-width-1-1" style="padding-left: 0;">
        <div class="uk-input-group">
            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
            <div class="md-input-wrapper">
                <select id="year" class="md-input" name="year">
                    <option value="">Choose Year</option>
                    <?php for ($y = 2018; $y <= date('Y', strtotime('+5 year')); $y++) { ?>
                        <option value="{{ $y }}">{{ $y }}</option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="uk-width-large-1-10 uk-width-1-1">
        <a class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light" id="yearFilter" href="javascript:void(0)">Filter</a>
    </div>
</div>

<form class="uk-form-stacked" action="{{ route('tour.departure.store')}}" id="tour_departure_form" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="tourId" id="tourId" value="{{ isset($tour->id) ? $tour->id : '' }}" />
    <div class="uk-width-medium-1-1">
        <?php
        for ($i = 1; $i <= 4; $i++) {
            echo '<div class="uk-width-medium-1-4 uk-float-left custom-date-picker" id ="datepickerBlock' . $i . '"></div>';
        }
        ?>

    </div>
    <div class="uk-width-medium-1-1">
        <?php
        for ($i = 5; $i <= 8; $i++) {
            echo '<div class="uk-width-medium-1-4 uk-float-left custom-date-picker" id ="datepickerBlock' . $i . '"></div>';
        }
        ?>

    </div>
    <div class="uk-width-medium-1-1">
        <?php
        for ($i = 9; $i <= 12; $i++) {
            echo '<div class="uk-width-medium-1-4 uk-float-left custom-date-picker" id ="datepickerBlock' . $i . '"></div>';
        }
        ?>

    </div>
    @can('add_tour_departuredate')
    @if((\Auth::user()->role_id == 1) || (\Auth::user()->role_id == 2 && in_array($tour->sector,explode(',', \Session::get('sector_id')))) || (\Auth::user()->id==$tour->created_by) )
    <div class="clear clearfix"></div>
    <hr class="form_hr">
    <div class="uk-grid uk-margin-medium-top uk-text-right">
        <div class="uk-width-1-1">
            <button type="button" id="saveDepartureDate" class="md-btn md-btn-primary">Save</button>
        </div>
    </div>
    @endif
    @endcan
</form>
<?php
$dateArray['dateList'] = array();
foreach ($tour_departure As $departure) {
    $dateArray['dateList'][] = $departure['departure_date'];
}
$dateAray = json_encode($dateArray);
?>
<script type = "text/javascript" >
    $(document).ready(function () {
        var jsondata =<?php echo $dateAray; ?>;
        var availableDates = jsondata.dateList;
        var currentYear = (new Date).getFullYear();
//        var currentMonth = (new Date).getMonth() + 1;
        var currentMonth = 1;
        var currentDay = (new Date).getDate();
        var datesDis = 0;
        var removedDate = [];
        drowCalendar('init');

        $("#yearFilter").click(function () {
            currentYear = $("#year").val();
            currentMonth = 1;
            $("#yearFilter").prop("disabled", true).addClass('disabled');
            $.ajax({
                type: 'get',
                url: ADMIN_URL + 'tour/get_departuredates/' + currentYear + '/' + $('#tourId').val(),
                dataType: "json",
                success: function (data, textStatus, xhr) {
                    availableDates = data.dateList;
                    drowCalendar('update');
                    $("#yearFilter").prop("disabled", false).removeClass('disabled');
                },
                error: function (xhr, textStatus, error) {
                }
            });
        });

        function drowCalendar(cType) {
            for (var i = 1; i <= 12; i++) {
                if (cType == "update") {
                    $('#datepickerBlock' + i).datepicker('remove');
                }
                var days = daysInMonth(currentMonth + i - 2, currentYear);
                $('#datepickerBlock' + i).datepicker({
                    startDate: new Date(currentYear, currentMonth + i - 2),
                    endDate: new Date(currentYear, currentMonth + i - 2, days),
                    format: "yyyy-mm-dd",
                    multidate: true,
                    viewMode: "years",
                })
                $('#datepickerBlock' + i).datepicker('setDate', availableDates);
            }
            if (i == 13) {
                datesDis = 1;
            }
        }
        function daysInMonth(month, year) {
            return new Date(year, month + 1, 0).getDate();
        }


        $('#saveDepartureDate').click(function () {
            $("#saveDepartureDate").prop("disabled", true).addClass('disabled');
            var datesArray = [];
            for (var i = 1; i <= 12; i++) {
                var dateData = $('#datepickerBlock' + i).datepicker("getDates");
                datesArray.push(dateData);
            }
            var formUrl = $('#tour_departure_form').attr('action');
            var tourId = $('#tourId').val();
            $.ajax({
                type: 'POST',
                url: formUrl,
                data: {"_token": "{{ csrf_token() }}", tourDepartureDate: datesArray, tourId: tourId, dateR: removedDate, currentYear: currentYear},
                dataType: "json",
                success: function (data, textStatus, xhr) {
                    if (data.success == 'ok') {
                        showNotify('success', 'Departure date update successfully.');
                        $("#saveDepartureDate").prop("disabled", false).removeClass('disabled');
                    }
                },
                error: function (xhr, textStatus, error) {
                    showNotify('warning', 'Something went wrong!');
                    $("#saveDepartureDate").prop("disabled", false).removeClass('disabled');
                }
            });
        });
    });
</script>