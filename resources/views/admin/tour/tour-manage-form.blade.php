<form action="{{ route('tour.save')}}" id="tour_frm" class="" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ isset($tour->id) ? $tour->id : '' }}" />
    <input type="hidden" name="temp_id" id="tour_temp_id" value="{{ str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT) }}" />
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>Tour Name <span class="required-lbl">*</span></label>
                        <input type="text" class="md-input" id="tour_name" name="tour_name" value="{{ isset($tour->tour_name) ? $tour->tour_name : old('tour_name') }}" />
                        <small class="text-danger">{{ $errors->first('tour_name') }}</small>
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <label>Tour Code <span class="required-lbl">*</span></label>
                        <input type="text" class="md-input" id="tour_code" name="tour_code" value="{{ isset($tour->tour_code) ? $tour->tour_code : old('tour_code') }}" />
                        <small class="text-danger">{{ $errors->first('tour_code') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>Tour Nights</label>
                        <input type="text" class="md-input onlynumber" id="tour_nights" name="tour_nights" value="{{ isset($tour->tour_nights) ? $tour->tour_nights : old('tour_nights') }}" />
                        <small class="text-danger">{{ $errors->first('tour_nights') }}</small>
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select id="domestic_international" class="md-input" name="domestic_international">
                            <option value="">Choose Domestic/International</option>
                            @if(isset($tour_location_type))
                            @foreach($tour_location_type as $typeId => $typeName)
                            <option value="{{ $typeId }}" {{ (isset($tour->domestic_international) && $tour->domestic_international == $typeId) ? 'selected' : '' }}>{{ $typeName }}</option>
                            @endforeach
                            @endif
                        </select>
                        <small class="text-danger">{{ $errors->first('domestic_international') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <?php /*
                    <div class="uk-width-medium-1-2 p-r-50">
                        <select id="tour_type" class="md-input" name="tour_type" data-uk-tooltip="{pos:'bottom'}" title="Choose Tour Category">
                            <option value="">Choose Tour Category</option>
                            @if(isset($tour_type))
                            @foreach($tour_type as $type)
                            <option value="{{ $type->typeId }}" {{ (isset($tour->tour_type) && $tour->tour_type == $type->typeId) ? 'selected' : '' }}>{{ $type->name }}</option>
                            @endforeach
                            @endif
                        </select>
                        <small class="text-danger">{{ $errors->first('tour_type') }}</small>
                    </div>
                    */ ?>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <select id="tour_type" class="md-input" name="tour_type[]" multiple data-md-select2 data-placeholder="Choose tour Category" data-uk-tooltip="{pos:'bottom'}" title="Choose tour Category">
                            <option value="">Choose Tour Category</option>
                            @if(isset($tour_type))
                            @php
                            if(isset($tour->tour_type))
                            $tour_category = explode(',',$tour->tour_type);
                            @endphp
                            @foreach($tour_type as $type)
                            <option value="{{ $type->typeId }}" {{ (isset($tour->tour_type) && in_array($type->typeId,$tour_category)) ? 'selected' : '' }}>{{ $type->name }}</option>
                            @endforeach
                            @endif
                        </select>
                        <small class="text-danger">{{ $errors->first('tour_type') }}</small>
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select id="public_private" class="md-input" name="public_private" data-uk-tooltip="{pos:'bottom'}" title="Choose Public/Private">
                            <option value="">Choose Public/Private</option>
                            @if(isset($tour_display))
                            @foreach($tour_display as $displayId => $displayName)
                            <option value="{{ $displayId }}" {{ (isset($tour->public_private) && $tour->public_private == $displayId) ? 'selected' : '' }}>{{ $displayName }}</option>
                            @endforeach
                            @endif
                        </select>
                        <small class="text-danger">{{ $errors->first('public_private') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <select id="sector" name="sector" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Choose Sector">
                            <option value="" selected>Choose Sector</option>
                            @if(isset($sectors))
                            @foreach($sectors as  $sector)
                            <option value="{{ $sector->id }}" {{ (isset($tour->sector) && $tour->sector == $sector->id) ? 'selected' : '' }}>{{ $sector->name }}</option>
                            @endforeach
                            @endif
                        </select>
                        <small class="text-danger">{{ $errors->first('sector') }}</small>
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select id="group_customized" name="group_customized" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Choose Group/Customized">
                            <option value="">Choose Group/Customized</option>
                            @if(isset($tour_group))
                            @foreach($tour_group as $groupId => $groupName)
                            <option value="{{ $groupId }}" {{ (isset($tour->group_customized) && $tour->group_customized == $groupId) ? 'selected' : '' }}>{{ $groupName }}</option>
                            @endforeach
                            @endif
                        </select>
                        <small class="text-danger">{{ $errors->first('group_customized') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>Minimum Tourist</label>
                        <input type="text" class="md-input" id="minimum_tourist" name="minimum_tourist" value="{{ isset($tour->minimum_tourist) ? $tour->minimum_tourist : old('minimum_tourist') }}" />
                        <small class="text-danger">{{ $errors->first('minimum_tourist') }}</small>
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <label>Maximum Tourist</label>
                        <input type="text" class="md-input" id="maximum_tourist" name="maximum_tourist" value="{{ isset($tour->maximum_tourist) ? $tour->maximum_tourist : old('maximum_tourist') }}" />
                        <small class="text-danger">{{ $errors->first('maximum_tourist') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>Online Booking Minimum Amount</label>
                        <input type="text" class="md-input" id="online_booking_minimum_amount" name="online_booking_minimum_amount" value="{{ isset($tour->online_booking_minimum_amount) ? $tour->online_booking_minimum_amount : old('online_booking_minimum_amount') }}" />
                        <small class="text-danger">{{ $errors->first('online_booking_minimum_amount') }}</small>
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <select id="feature" class="md-input" name="feature[]" multiple data-md-select2 data-placeholder="Choose tour feature" data-uk-tooltip="{pos:'bottom'}" title="Choose tour feature">
                            @if(isset($tour_feature))
                            @php
                            if(isset($tour->feature))
                            $features = explode(',',$tour->feature);
                            @endphp
                            @foreach($tour_feature as $feature)
                            <option value="{{ $feature->typeId }}" {{ (isset($tour->feature) && in_array($feature->typeId,$features)) ? 'selected' : '' }}>{{ $feature->name }}</option>
                            @endforeach
                            @endif
                        </select>
                        <small class="text-danger">{{ $errors->first('feature') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>Starting Points</label>
                        <input type="text" class="md-input" id="starting_point" name="starting_point" value="{{ isset($tour->starting_point) ? $tour->starting_point : old('starting_point') }}" />                                        
                        <small class="text-danger">{{ $errors->first('starting_point') }}</small>
                    </div>
                    <div class="uk-width-medium-1-2 p-l-50">
                        <label>Ending Points</label>
                        <input type="text" class="md-input" id="ending_point" name="ending_point" value="{{ isset($tour->ending_point) ? $tour->ending_point : old('ending_point') }}" />  
                        <small class="text-danger">{{ $errors->first('ending_point') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 p-r-50">
                        <label>Key Point</label>
                        <input type="text" class="md-input" id="key_point" name="key_point" value="{{ isset($tour->key_point) ? $tour->key_point : old('key_point') }}" />  
                        <small class="text-danger">{{ $errors->first('key_point') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>Short Description</label>
                        <textarea cols="30" rows="4" class="md-input" name="short_description" id="short_description">{{ isset($tour->short_description) ? $tour->short_description : old('short_description') }}</textarea>
                        <small class="text-danger">{{ $errors->first('short_description') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>Long Description</label>
                        <textarea cols="30" rows="4" class="md-input" name="long_description" id="long_description">{{ isset($tour->long_description) ? $tour->long_description : old('long_description') }}</textarea>  
                        <small class="text-danger">{{ $errors->first('long_description') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>Inclusions</label>
                        <textarea cols="30" rows="4" class="md-input" name="inclusions" id="inclusions">{{ isset($tour->inclusions) ? $tour->inclusions : old('inclusions') }}</textarea>      
                        <small class="text-danger">{{ $errors->first('inclusions') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>Exclusions</label>
                        <textarea cols="30" rows="4" class="md-input" name="exclusions" id="exclusions">{{ isset($tour->exclusions) ? $tour->exclusions : old('exclusions') }}</textarea>      
                        <small class="text-danger">{{ $errors->first('exclusions') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>Tour Tips/Instructions</label>
                        <textarea cols="30" rows="4" class="md-input" name="tour_tips" id="tour_tips">{{ isset($tour->tour_tips) ? $tour->tour_tips : old('tour_tips') }}</textarea>   
                        <small class="text-danger">{{ $errors->first('tour_tips') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>Cancellation Policy</label>
                        <textarea cols="30" rows="4" class="md-input" name="cancellation_policy" id="cancellation_policy">{{ isset($tour->cancellation_policy) ? $tour->cancellation_policy : old('cancellation_policy') }}</textarea>
                        <small class="text-danger">{{ $errors->first('cancellation_policy') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>Keywords</label>
                        <textarea cols="30" rows="4" class="md-input" name="keywords" id="keywords">{{ isset($tour->keywords) ? $tour->keywords : old('keywords') }}</textarea>      
                        <small class="text-danger">{{ $errors->first('keywords') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 p-r-50">
                        <label>Terms & Conditions</label>
                        <textarea cols="30" rows="4" class="md-input" name="terms_conditions" id="terms_conditions">{{ isset($tour->terms_conditions) ? $tour->terms_conditions : old('terms_conditions') }}</textarea>  
                        <small class="text-danger">{{ $errors->first('terms_conditions') }}</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-6">
                        <h3 class="heading_a uk-margin-small-bottom">Image</h3>
                        <input type="file" id="input-file-a" class="dropify" name="image" data-default-file="{{  isset($tour->image) ? \App\Helpers\S3url::s3_get_image($tour->image) : '' }}" />
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-6">
                        <h3 class="heading_a uk-margin-small-bottom">Full Size Image</h3>
                        <input type="file" id="input-file-a" class="dropify" name="full_size_image" data-default-file="{{  isset($tour->full_size_image) ? \App\Helpers\S3url::s3_get_image($tour->full_size_image) : '' }}" />
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-6">
                        <h3 class="heading_a uk-margin-small-bottom">English Pdf</h3>
                        <input type="file" id="input-file-a" class="dropify" name="english_pdf" data-default-file="{{  isset($tour->english_pdf) ? \App\Helpers\S3url::s3_get_image($tour->english_pdf) : '' }}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-6">
                        <h3 class="heading_a uk-margin-small-bottom">Gujarati Pdf</h3>
                        <input type="file" id="input-file-a" class="dropify" name="gujarati_pdf" data-default-file="{{  isset($tour->gujarati_pdf) ? \App\Helpers\S3url::s3_get_image($tour->gujarati_pdf) : '' }}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">
                        <div class="dropzone" id="dropzoneFileUpload">
                        </div>
<!--                        <div class="fallback">
                            <input type="file" name="file" multiple>
                        </div>-->
                    </div>
                </div>
            </div>

            <!--Dropzone Preview Template-->
            <div id="preview" style="display: none;">

                <div class="dz-preview dz-file-preview">
                    <div class="dz-image"><img data-dz-thumbnail /></div>

                    <div class="dz-details">
                        <div class="dz-size"><span data-dz-size></span></div>
                        <div class="dz-filename"><span data-dz-name></span></div>
                    </div>
                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                    <div class="dz-error-message"><span data-dz-errormessage></span></div>



                    <div class="dz-success-mark">

                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                        <title>Check</title>
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                        </g>
                        </svg>

                    </div>
                    <div class="dz-error-mark">

                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                        <title>error</title>
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                        <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                        </g>
                        </g>
                        </svg>
                    </div>
                </div>
            </div>
        </div>



        <div class="uk-width-medium-1-12">
            <div class="uk-form-row">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="status-column">
                        <span class="icheck-inline">
                            <input type="radio" name="tour_status" value="1" id="active" data-md-icheck {{ (isset($tour->tour_status) && $tour->tour_status == 1) ? 'checked' : ''  }} {{ (!isset($tour)) ? 'checked' : '' }} />
                                   <label for="active" class="inline-label">Active</label>
                        </span>
                        <span class="icheck-inline">
                            <input type="radio" name="tour_status" value="2" id="inactive" data-md-icheck {{ (isset($tour->tour_status) && $tour->tour_status == 2) ? 'checked' : ''  }} />
                                   <label for="inactive" class="inline-label">In Active</label>
                        </span>
                    </div>
                    <small class="text-danger">{{ $errors->first('tour_status') }}</small>
                </div>
            </div>
        </div>
    </div>
    <hr class="form_hr">
    <div class="uk-grid uk-margin-medium-top uk-text-right">
        <div class="uk-width-1-1">
            <a href="{{ route('tour.index') }}" class="md-btn md-btn-danger">Cancel</a>
            @if(!isset($tour->sector))
            <button type="submit" class="md-btn md-btn-primary">Save</button>
            @elseif((\Auth::user()->role_id == 1) || (\Auth::user()->role_id == 2 && in_array($tour->sector,explode(',', \Session::get('sector_id')))) || (\Auth::user()->id==$tour->created_by) )
            @can('update_tour')
            <button type="submit" class="md-btn md-btn-primary">Save</button>
            @endcan
            @endif
        </div>
    </div>
</form>
