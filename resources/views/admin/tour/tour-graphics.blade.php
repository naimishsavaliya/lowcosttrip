@extends('layouts.admin')
@section('content')


<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Tour Graphics</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->   
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i>Tour Graphics</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{ env('ADMIN_URL')}}tour" class="" method="post">
                            <div class="control-group">
                                <label class="control-label">Thumb Image</label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" class="default" />
                                            </span>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group">
                                <label class="control-label">Background Image</label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" class="default" />
                                            </span>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group">
                                <label class="control-label">Brochure</label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" class="default" />
                                            </span>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear clearfix"></div>

                            <div class = "form-actions">
                                <button type = "submit" class = "btn btn-success">Submit</button>
                                <a href="{{ env('ADMIN_URL')}}tour" class = "btn">Cancel</a>
                            </div>
                        </form>
                        <!--END FORM-->
                    </div>
                </div>
                <!--END SAMPLE FORM widget-->
            </div>
        </div>
    </div>
</div>

<link href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap/js/bootstrap-fileupload.js"></script>
@endsection