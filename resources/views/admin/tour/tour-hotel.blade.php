@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li>Tour Name</li>
            <li><span>Manage Destination & Hotels </span></li>
        </ul>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')


            <form class="uk-form-stacked" id="wizard_advanced_form">


                <section>
                    <div data-dynamic-fields="d_field_wizard_1" class="uk-grid" data-uk-grid-margin></div>
                </section>


                <script id="d_field_wizard_1" type="text/x-handlebars-template">
                    <div class="uk-width-medium-1-1 parsley-row form_section">
                    <div class="uk-input-group uk-grid" style="width:100%">
                    <div class="uk-width-medium-1-3 p-t-8">
                    <div class="uk-form-row">
                    <label for="invoice_dp">Day </label>
                    <input class="md-input" type="text" id="invoice_dp" value=""  name="wizard_address101"                                                 id="wizard_address1">
                    </div>
                    <div class="uk-form-row">
                    <label for="inv_service">Title</label>
                    <input type="text" name="wizard_address101" id="wizard_address101" class="md-input" id="inv_service" name="inv_service_id" />
                    </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                    <label for="inv_service_rate">Description</label>
                    <textarea cols="30" name="wizard_address101" id="wizard_address101" rows="4" class="md-input"></textarea>
                    </div>
                    <div class="uk-width-medium-1-6" style="margin-top:7%;">
                    <div class="uk-form-file md-btn md-btn-primary">
                    Select image
                    <input id="form-file" type="file">
                    </div>
                    <span class="uk-input-group-addon" style="padding: 0;float: right;display: inline-block;">
                    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
                    </span>
                    </div>
                    </div>
                    </div>
                </script>
            </form>
        </div>
    </div>
    <script type = "text/javascript" >
        $(function () {
        });
        function removedata(obj) {
            var conf = confirm('Are you sure to delete?');
            if (conf) {
                $(obj).parents('.cloneDiv').remove();
            }
        }
        function cloneData() {
            var clonedata = $(".cloneData").clone();
            clonedata.removeClass("cloneData");
            $(".appendDiv").append(clonedata);

        }
    </script>

    @endsection
    @section('javascript')
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
    <!--<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>-->

    @endsection