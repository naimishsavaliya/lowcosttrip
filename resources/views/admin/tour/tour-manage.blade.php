@extends('layouts.theme')
@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}css/dropzone.css">
@endsection
@section('content')
<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                @include('admin/tour/tour-manage-form')
            </div>
        </div>
    </div>

</div>

@endsection


@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_advanced.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/dropzone.js"></script>
<script type="text/javascript">
$(function () {
    $("form#tour_frm").validate({
        rules: {
            tour_name: "required",
            tour_code: "required",
            tour_nights: "required"
            // domestic_international: "required",
            // tour_type: "required",
            // public_private: "required",
            // sector: "required",
            // group_customized: "required"
            //                image: {
            //                    accept: "jpg,png,jpeg,gif"
            //                },
            //        status: "required",
        },
        messages: {
            tour_name: "Please enter tour name",
            tour_code: "Please enter tour code",
            tour_nights: "Please enter number of nights"
            // domestic_international: "Please select type",
            // tour_type: "Please select category",
            // public_private: "Please select public/private",
            // sector: "Please select sector",
            // group_customized: "Please select group/customized"
            //        image: {
            //            accept: "Only image type jpg/png/jpeg/gif is allowed"
            //        },
            //        status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else if (element.is('select')) {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    $(".onlynumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

});

</script>

<script type="text/javascript">
    var total_photos_counter = 0;
    var name = "";
    var baseUrl = "{{ route('tour.images.store') }}";
    var token = "{{ csrf_token() }}";
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#dropzoneFileUpload", {
        url: baseUrl,
        params: {
            _token: token,
            tour_temp_id: $('#tour_temp_id').val()
        },
        uploadMultiple: true,
        //acceptedFiles: 'image/*',
        parallelUploads: 2,
        maxFilesize: 16,
        previewTemplate: document.querySelector('#preview').innerHTML,
        addRemoveLinks: true,
        dictRemoveFile: 'Remove file',
        dictFileTooBig: 'Image is larger than 16MB',
        timeout: 10000,
        renameFile: function (file) {
            name = new Date().getTime() + Math.floor((Math.random() * 100) + 1) + '_' + file.name;
            return name;
        },
        init: function () {
            this.on("removedfile", function (file) {
                $.post({
                    url: "{{ route('tour.images.destroy')}}",
                    data: {
                        _token: token,
                        id: file.customName,
                        //tour_temp_id: $('#tour_temp_id').val()
                    },
                    dataType: 'json',
                    success: function (data) {
                        total_photos_counter--;
                        $("#counter").text("# " + total_photos_counter);
                    }
                });
            });
        },
        success: function (file, done) {
            total_photos_counter++;
            $("#counter").text("#" + total_photos_counter);
            file.customName = file.upload.filename;
        }
    });


</script>
@endsection
