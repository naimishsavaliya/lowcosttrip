@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Tour Booking</span></li>
        </ul>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <form class="uk-form-stacked" id="wizard_advanced_form">
                <div id="wizard_advanced">
                    <!-- first section -->
                    <h3>Step 1 - Tour Information</h3>
                    <section style="padding: 24px 35px;">
                        <table class="uk-table dt_default">
                            <thead>
                                <tr>
                                    <th>Lead ID</th>
                                    <th>Tour Name</th>
                                    <th>Customer Name</th>
                                    <th>Booking Date</th>
                                    <th>Departure Date</th>
                                    <th>Hotel Type</th>
                                    <th>No of Rooms</th>
                                    <th>Adult/Child/Infant</th>
                                    <th>Cost</th>
                                    <th>Agent</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 1; $i <= rand(3, 15); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td>LCT<?php echo rand(1111, 9999); ?></td>
                                        <td>Tour Name</td>
                                        <td>Customer Name</td>
                                        <td>Mar 5, 2018</td>
                                        <td>Nov 10, 2018</td>
                                        <td>Deluxe</td>
                                        <td>1</td>
                                        <td>2/1/0</td>
                                        <td>50,000</td>
                                        <td>Agent <?php echo $i; ?></td>
                                        <td>Pending</td>
                                        <td>
                                            <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                                                <div class="uk-dropdown">
                                                    <ul class="uk-nav uk-nav-dropdown">
                                                        <li><a href=""> Cancel Booking</a></li>
                                                        <li><a href=""> Edit Booking</a></li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </section>
                    <!-- second section -->
                    <h3>Step 2 - Personal Information</h3>
                    <section style="padding: 24px 35px;">
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>First Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="first_name" value="{{ isset($users[0]->first_name) ? $users[0]->first_name : '' }}" />
                                        <small class="text-danger">{{ $errors->first('first_name') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>Last Name <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="last_name" value="{{ isset($users[0]->last_name) ? $users[0]->last_name : '' }}" />
                                        <small class="text-danger">{{ $errors->first('last_name') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>E-mail <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="email" value="{{ isset($users[0]->email) ? $users[0]->email : '' }}" />
                                        <small class="text-danger">{{ $errors->first('email') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Address</label>
                                        <textarea cols="30" rows="4" class="md-input" name="address">{{ isset($users[0]->address) ? $users[0]->address : '' }}</textarea>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <div class="uk-width-medium-1-12 display-inline-flex">
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <select id="select_demo_3" class="md-input" name="country_id">
                                                    <option value="" selected>Choose Country</option>
                                                    @if(isset($countrys))
                                                    @foreach($countrys as $country)
                                                    <option value="{{ $country->country_id }}" {{ (isset($users[0]->country_id) && $users[0]->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                <select id="select_demo_3" class="md-input" name="state_id">
                                                    <option value="" selected>Choose State</option>
                                                    @if(isset($states))
                                                    @foreach($states as $state)
                                                    <option value="{{ $state->state_id }}" {{ (isset($users[0]->state_id) && $users[0]->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-12 p-t-20 display-inline-flex">
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <select id="select_demo_3" class="md-input" name="city_id">
                                                    <option value="" selected>Choose City</option>
                                                    @if(isset($cities))
                                                    @foreach($cities as $city)
                                                    <option value="{{ $city->city_id }}" {{ (isset($users[0]->city_id) && $users[0]->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                                <label>Pincode</label>
                                                <input type="text" class="md-input" name="pin_code" value="{{ isset($users[0]->pin_code) ? $users[0]->pin_code : '' }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>PAN NO.</label>
                                        <input type="text" class="md-input" name="first_name" value="" />
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>GST Number</label>
                                        <input type="text" class="md-input" name="last_name" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>GST Name</label>
                                        <input type="text" class="md-input" name="gst_name" value="" />
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>GST Address</label>
                                        <input type="text" class="md-input" name="gst_address" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="select_demo_3" class="md-input" name="">
                                            <option value="" selected>Choose Sales Executive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>

                    <!-- third section -->
                    <h3>Step 3 - Booking Information</h3>
                    <section>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <select id="select_demo_3" class="md-input" name="">
                                            <option value="" selected>Choose Group</option>
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <select id="select_demo_3" class="md-input" name="">
                                            <option value="" selected>Choose Vehicle</option>
                                            <option value="1">Bus</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Confirm Departure Date</label>
                                        <input type="text" class="md-input" name="gst_address" value="" id="uk_dp_1" data-uk-datepicker="{format:'DD/MM/YYYY'}" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!--                        <div class="md-card-content">
                                                    <div data-dynamic-fields="field_template_b" dynamic-fields-counter="0">
                                                        <div class="uk-grid form_section">
                                                            <div class="uk-width-1-1">
                                                                <div class="uk-input-group">
                                                                    <div class="md-input-wrapper">
                                                                        <label for="d_form_input__0">Company Name</label>
                                                                        <input type="text" class="md-input" name="d_form_input__0" id="d_form_input__0">
                                                                        <span class="md-input-bar "></span>
                                                                    </div>
                                                                    <span class="uk-input-group-addon">
                                                                        <a href="#" class="btnSectionClone"><i class="material-icons md-24"></i></a>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->
                        <p><a href="javascript:void(0);" class="md-btn md-btn-primary" id="pushDemo">Add Room</a></p>
                    </section>

                    <!-- forth section -->
                    <h3>Step 4 - Booking Summery</h3>
                    <section>

                    </section>
                </div>
            </form>
        </div>
    </div>

    @endsection

    @section('javascript')
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/wizard_steps.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_wizard.min.js"></script>

    <script>
// load parsley config (altair_admin_common.js)
altair_forms.parsley_validation_config();
// load extra validators
altair_forms.parsley_extra_validators();
    </script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/parsleyjs/dist/parsley.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/instafilta.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/page_invoices.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/jquery.waitforimages.min.js"></script>
    <script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/page_gallery.min.js"></script>
    @endsection