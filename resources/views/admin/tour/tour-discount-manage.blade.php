@extends('layouts.admin')
@section('content')


<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Tour Discount Manage</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->   
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i>Tour Discount Manage</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{ env('ADMIN_URL')}}tour/discount/1" class="" method="get">
                            <div class="control-group span6">
                                <label class="control-label">Discount Code</label>
                                <div class="controls">
                                    <input type="text" class="span12" />
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group span6 m-l-0">
                                <label class="control-label">From Date</label>
                                <div class="controls">
                                    <input type="text" class="span12" />
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group span6 m-l-0">
                                <label class="control-label">To Date</label>
                                <div class="controls">
                                    <input type="text" class="span12" />
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group span4 m-l-0">
                                <label class="control-label">Discount Type</label>
                                <div class="controls">
                                    <select class="span12 chosen" data-placeholder="Discount Type" tabindex="1">
                                        <option value=""></option>
                                        <option value="1">%</option>
                                        <option value="2">Fix Amount</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clear clearfix"></div>
                            <div class="control-group span4 m-l-0">
                                <label class="control-label">Discount</label>
                                <div class="controls">
                                    <input type="text" class="span12" />
                                </div>
                            </div>




                            <div class="clear clearfix"></div>
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                    <label class="radio">
                                        <input type="radio" name="optionsRadios1" value = "option1" />
                                        Active
                                    </label>
                                    <label class="radio">
                                        <input type = "radio" name = "optionsRadios1" value = "option2" checked />
                                        InActive
                                    </label>
                                </div>
                            </div>


                            <div class = "form-actions">
                                <button type = "submit" class = "btn btn-success">Submit</button>
                                <a href="{{ env('ADMIN_URL')}}tour/discount/1" class = "btn">Cancel</a>
                            </div>
                        </form>
                        <!--END FORM-->
                    </div>
                </div>
                <!--END SAMPLE FORM widget-->
            </div>
        </div>
    </div>
</div>
@endsection