<table class="uk-table tour-itinerary dt_default">
    <thead>
        <tr>
            <th>Day</th>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tour_itinerary as $tTtinerary)
        <tr class="odd gradeX">
            <td>{{$tTtinerary->tour_day}}</td>
            <td>{{$tTtinerary->title}}</td>
            <td>{{$tTtinerary->description}}</td>
            <td><img src="{{ env('APP_PUBLIC_URL')}}uploads/itinerary/{{$tTtinerary->image}}" alt="" style="height: 50px;"></td>
            <td class="itinerary-{{$tTtinerary->id}}">
                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                    <button class="md-btn"><i class="material-icons">settings</i><i class="material-icons">&#xE313;</i></button>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown">
                            @can('delete_tour_itinerary')
                            @if((\Auth::user()->role_id == 1) || (\Auth::user()->role_id == 2 && in_array($tour->sector,explode(',', \Session::get('sector_id')))) || (\Auth::user()->id==$tour->created_by) )
                            <li><a class="remove-itinerary"  href="javascript:;" url="{{route('tour.itinerary.delete',$tTtinerary->id)}}">Delete</a></li>
                            @endif
                            @endcan
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script type="text/javascript">
    $(function () {
        $("a.remove-itinerary").on("click", function () {
            var obj = $(this);
            var conf = confirm('Are you sure you want to delete?');
            if (conf) {
                $.ajax({
                    type: 'GET',
                    url: $(this).attr('url'),
                    dataType: 'json',
                    beforeSend: function (xhr) {

                    },
                    success: function (data, textStatus, jqXHR) {
                        if (data.status == "ok") {
                            obj.parents("tr").remove();
                        }
                    }
                });
            }
        });
    });
</script>