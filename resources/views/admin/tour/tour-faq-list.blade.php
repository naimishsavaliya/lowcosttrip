<table class="uk-table dt_default">

    <thead>
        <tr>
            <th style="width: 5%;">ID</th>
            <th style="width: 25%;">Question</th>
            <th style="width: 50%;" class="hidden-phone">Answer</th>
            <th style="width: 10%;" class="hidden-phone">Status</th>
            <th style="width: 10%;">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tour_faq as $faq)
        <tr class="odd gradeX">
            <td>{{$faq->id}}</td>
            <td>{{$faq->question}}</td>
            <td>{{$faq->answer}}</td>
            <td >
                @if($faq->status==1)
                <a class="change-status-btn" title="Status"><span class="uk-badge uk-badge-success">Active</span></a>
                @else
                <a class="change-status-btn" title="Status"><span class="uk-badge uk-badge-danger">InActive</span></a>
                @endif
            </td>
            <td>
                @can('edit_tour_faq')
                @if((\Auth::user()->role_id == 1) || (\Auth::user()->role_id == 2 && in_array($tour->sector,explode(',', \Session::get('sector_id')))) || (\Auth::user()->id==$tour->created_by) )
                <a href="javascript:;" data-id="{{$faq->id}}"  data-uk-modal="{target:'#faq_form_model'}" title="Edit Faq" class="edit-faq"><i class="md-icon material-icons">&#xE254;</i></a> 
                @endif
                @endcan
                @can('delete_tour_faq')
                @if((\Auth::user()->role_id == 1) || (\Auth::user()->role_id == 2 && in_array($tour->sector,explode(',', \Session::get('sector_id')))) || (\Auth::user()->id==$tour->created_by) )
                <a class="remove"  href="javascript:;" url="{{route('tour.faq.delete',$faq->id)}}"><i class="md-icon material-icons">delete</i></a>
                @endif
                @endcan
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script type="text/javascript">
    $(function () {
        $("a.edit-faq").on("click", function () {
            var obj = $(this);
            var edit_url = "{{ route('tour.faq.detail', ':id') }}";
            edit_url = edit_url.replace(':id', obj.attr('data-id'));
            $.ajax({
                type: 'GET',
                url: edit_url,
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    if (data.id >= 0) {
                        $("#faq_id").val(data.id);
                        $("#question").val(data.question);
                        $("#answer").val(data.answer);

                        if (data.status == 1) {
                            $('#inactive').removeAttr('checked');
                            $('#active').iCheck('check');
                        } else {
                            $('#inactive').removeAttr('checked');
                            $('#active').iCheck('check');
                        }
                    }
                }
            });
        });
        $("a.remove").on("click", function () {
            var obj = $(this);
            var conf = confirm('Are you sure you want to delete?');
            if (conf) {
                $.ajax({
                    type: 'GET',
                    url: $(this).attr('url'),
                    dataType: 'json',
                    beforeSend: function (xhr) {

                    },
                    success: function (data, textStatus, jqXHR) {
                        if (data.status == "ok") {
                            obj.parents("tr").remove();
                            showNotify('success', 'FAQ deleted successfully!');
                        }
                    }
                });
            }
        });
    });
</script>