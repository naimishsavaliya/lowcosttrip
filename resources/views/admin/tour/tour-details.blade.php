@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Tour Information</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>

        @if(isset($deleted) && $deleted==true)
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong>Agent deleted successfully.</strong>
        </div>
        @endif
        @if(isset($added) && $added==true)
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong>Agent added successfully.</strong>
        </div>
        @endif

        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Data</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body">
                        <a href="{{ env('ADMIN_URL')}}tour/payment/1/2018-05-21" class="btn btn-primary">Payment Collections</a>

                    </div>
                </div>
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Tour Information of Amazing Andaman Tour(VW-AN-02-S2-2018) for 29-04-2018</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body">
                        <div class="span6">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td>Tour Name</td>
                                        <td>Amazing Andaman Tour</td>
                                    </tr>
                                    <tr>
                                        <td>Tour Code</td>
                                        <td>VW-AN-02-S2-2018</td>
                                    </tr>
                                    <tr>
                                        <td>Sector</td>
                                        <td>Andaman</td>
                                    </tr>
                                    <tr>
                                        <td>No of Night's</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td>Customized/Group</td>
                                        <td>Group</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="span6">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td>Total Tour Fee</td>
                                        <td>0.00</td>
                                    </tr>
                                    <tr>
                                        <td>Total Paid Amount</td>
                                        <td>0.00</td>
                                    </tr>
                                    <tr>
                                        <td>Total Unpaid Amount</td>
                                        <td>0.00</td>
                                    </tr>
                                    <tr>
                                        <td>Departure Date</td>
                                        <td>29-04-2018</td>
                                    </tr>
                                    <tr>
                                        <td>Arrival Date</td>
                                        <td>05-05-2018</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="clear clearfix"></div> <br/><br/><br/>
                    <div class="widget-body">
                        <table class="table table-striped table-bordered" id="sample_1">
                            <thead>
                                <tr>
                                    <th style="width: 25px;">ID</th>
                                    <th>Customer Name</th>
                                    <th>Contact No.</th>
                                    <th class="hidden-phone">PAX</th>
                                    <th class="hidden-phone">No. Of Room</th>
                                    <th class="hidden-phone">No. of Extra Bed</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Customer Name" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Contact No." name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="PAX" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="No. Of Room" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="No. of Extra Bed" name="" colPos="1"></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td>Customer <?php echo $i; ?></td>
                                        <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                        <td class="hidden-phone"><?php echo rand(2, 5); ?></td>
                                        <td class="hidden-phone"><?php echo rand(1, 2); ?></td>
                                        <td class="hidden-phone"><?php echo rand(0, 1); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection