@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Tour Payment Collections</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>


        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Tour Payment Collections</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body">
                        <table class="table table-striped table-bordered" id="sample_1">
                            <thead>
                                <tr>
                                    <th style="width: 25px;">ID</th>
                                    <th>Customer Name</th>
                                    <th>Contact No.</th>
                                    <th class="hidden-phone">PAX</th>
                                    <th class="hidden-phone">No. Of Room</th>
                                    <th class="hidden-phone">No. of Extra Bed</th>
                                    <th class="hidden-phone">Total Amount</th>
                                    <th class="hidden-phone">Pending Amount</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Customer Name" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Contact No." name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="PAX" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="No. Of Room" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="No. of Extra Bed" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Total Amount" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Pending Amount" name="" colPos="1"></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td>Customer <?php echo $i; ?></td>
                                        <td class="hidden-phone"><?php echo "9" . rand(111111111, 999999999); ?></td>
                                        <td class="hidden-phone"><?php echo rand(2, 5); ?></td>
                                        <td class="hidden-phone"><?php echo rand(1, 2); ?></td>
                                        <td class="hidden-phone"><?php echo rand(0, 1); ?></td>
                                        <td class="hidden-phone"><?php echo rand(50000, 90000); ?></td>
                                        <td class="hidden-phone"><?php echo rand(5000, 9000); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection