@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Tour Discount</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>

        @if(isset($deleted) && $deleted==true)
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong>Tour Discount deleted successfully.</strong>
        </div>
        @endif
        @if(isset($added) && $added==true)
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong>Tour Discount added successfully.</strong>
        </div>
        @endif

        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Tour Discount</h4>
                        <span class="tools">
                            <a href="{{ env('ADMIN_URL')}}tour/add_discount/1" class="icon-plus"></a>
                        </span>
                    </div>
                    <div class="widget-body">
                        <table class="table table-striped table-bordered" id="sample_1">
                            <thead>
                                <tr>
                                    <th style="width: 25px;">ID</th>
                                    <th>Discount Code</th>
                                    <th>From  Date</th>
                                    <th class="hidden-phone">To  Date</th>
                                    <th class="hidden-phone">Discount</th>
                                    <th class="hidden-phone">Status</th>
                                    <th style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Discount Code" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="From  Date" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="To  Date" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Discount" name="" colPos="1"></td>
                                    <td ><select class="chosen" data-placeholder="Choose a Status" tabindex="1" colPos="6">
                                            <option value="">Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">InActive</option>
                                        </select></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td>Code<?php echo $i; ?></td>
                                        <td>01 May,2018</td>
                                        <td>30 May,2018</td>
                                        <td class="hidden-phone">5%</td>
                                        <td ><span class="label label-success">Active</span></td>
                                        <td >
                                            <a href="{{ env('ADMIN_URL')}}tour/edit_discount/1" title="Edit Tour Discount" class="btn btn-mini btn-primary"><i class="icon-pencil icon-white"></i></a> 
                                            <a href="{{ env('ADMIN_URL')}}tour/delete_discount/1" onclick="return confirm('Are you sure to delete?');" title="Delete Tour Discount" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a> 
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
            </div>
        </div> 
    </div>
</div>


<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection