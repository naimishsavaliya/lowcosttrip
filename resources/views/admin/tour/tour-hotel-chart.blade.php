@extends('layouts.admin')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Tour Hotel Chart</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>


        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> Tour Hotel Chart</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body">
                        <table class="table table-striped table-bordered" id="sample_1">
                            <thead>
                                <tr>
                                    <th style="width: 25px;">ID</th>
                                    <th>Booking Id</th>
                                    <th>Customer Name</th>
                                    <th class="hidden-phone">Night</th>
                                    <th class="hidden-phone">Booking Dates</th>
                                    <th class="hidden-phone">Location</th>
                                    <th class="hidden-phone">Hotel Name</th>
                                    <th class="hidden-phone">Room Type</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Booking Id" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Customer Name" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Night" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Booking Dates" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Location" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Hotel Name" name="" colPos="1"></td>
                                    <td ><input type="text"  placeholder="Room Type" name="" colPos="1"></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td>TB000<?php echo $i; ?></td>
                                        <td>Customer <?php echo $i; ?></td>
                                        <td class="hidden-phone"><?php echo rand(2, 5); ?></td>
                                        <td class="hidden-phone">2018-01-02</td>
                                        <td class="hidden-phone">Location <?php echo rand(1, 4); ?></td>
                                        <td class="hidden-phone">Hotel <?php echo rand(1, 4); ?></td>
                                        <td class="hidden-phone">Room <?php echo rand(1, 3); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/data-tables/DT_bootstrap.js"></script>
@endsection