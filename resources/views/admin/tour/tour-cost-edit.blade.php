@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><a href="{{route('tour.detail',$tour->id)}}">{{$tour->tour_name}}</a></li>
            <li><span>Manage Cost</span></li>
        </ul>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <form class="uk-form-stacked" action="{{ route('tour.cost.update')}}" id="tour_itinerary_form" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ isset($tour->id) ? $tour->id : '' }}" />
                <input type="hidden" name="cost_id" value="{{ isset($cost[0]->id) ? $cost[0]->id : '' }}" />

                <div class="uk-width-medium-1-1 parsley-row form_section">
                    <div class="uk-input-group uk-grid" style="width:100%">
                        <div class="uk-width-1-1">
                            <div class="">
                                <div class="uk-grid" data-uk-grid-margin="">
                                    <div class="uk-width-medium-1-4 uk-width-1-1 uk-row-first">
                                        <label for="date_from">Date From</label>
                                        <input class="md-input" type="text" id="date_from" name="date_from[{{$cost[0]->id}}]" autocomplete="off" value="{{date('d F Y',strtotime($cost[0]->from_date))}}" required data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                    </div>
                                    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
                                        <label for="date_to">Date To</label>
                                        <input class="md-input" type="text" id="date_to" name="date_to[{{$cost[0]->id}}]" autocomplete="off" value="{{date('d F Y',strtotime($cost[0]->to_date))}}" required data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                    </div>
                                    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
                                        <label for="discount">Discount %</label>
                                        <input class="md-input" type="text" id="discount" name="discount[{{$cost[0]->id}}]" autocomplete="off" value="{{$cost[0]->discount}}">
                                    </div>
                                    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
                                    <select class="md-input" name="currency_id[{{$cost[0]->id}}]" id="currency_id" data-uk-tooltip="{pos:'top'}" title="Choose a currency">
                                        <option value="">Choose a currency</option>
                                        @if(isset($currencys))
                                        @foreach($currencys as $currency)
                                        <option value="{{ $currency->currId }}" {{ (isset($cost[0]->currency_id) && $currency->currId == $cost[0]->currency_id) ? 'selected' : '' }}>{{ $currency->currency_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <small class="text-danger">{{ $errors->first('currency_id') }}</small>
                                     
                                </div>
                                    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div>
                                    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div> <br/>


                                    <div class="uk-width-medium-1-1">
                                        <table class="uk-table uk-table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    @foreach($tour_package_type as $type)
                                                    <th>{{$type->name}}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($tour_rate_master as $rate)
                                                <tr>
                                                    <td>{{$rate->name}}</td>
                                                    @foreach($tour_package_type as $type)
                                                    <td>
                                                        <div class="uk-width-medium-1-2 uk-float-left">
                                                            <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                                                <label>Cost</label>
                                                                @if(isset($tourCostData[$rate->typeId][$type->typeId]))
                                                                <input type="text" class="md-input" name="cost[{{$tourCostData[$rate->typeId][$type->typeId]['id']}}]" id="" value="{{$tourCostData[$rate->typeId][$type->typeId]['cost']}}"><span class="md-input-bar "></span>
                                                                @else
                                                                <input type="text" class="md-input" name="cost_new[{{$rate->typeId}}-{{$type->typeId}}]" id="" value=""><span class="md-input-bar "></span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-2 uk-float-left">
                                                            <div class="md-input-wrapper md-input-wrapper-danger md-input-filled uk-width-medium-1-2">
                                                                <label>Sell</label>
                                                                @if(isset($tourCostData[$rate->typeId][$type->typeId]))
                                                                <input type="text" class="md-input" name="sell[{{$tourCostData[$rate->typeId][$type->typeId]['id']}}]" id="" value="{{$tourCostData[$rate->typeId][$type->typeId]['sell']}}"><span class="md-input-bar "></span>
                                                                @else
                                                                <input type="text" class="md-input" name="sell_new[{{$rate->typeId}}-{{$type->typeId}}]" id="" value=""><span class="md-input-bar "></span>
                                                                @endif

                                                            </div>
                                                        </div>
                                                    </td>
                                                    @endforeach
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="form_hr">
                <div class="uk-grid uk-margin-medium-top uk-text-right">
                    <div class="uk-width-1-1">
                        <button type="submit" class="md-btn md-btn-primary">Save</button>
                    </div>
                </div>

            </form>


        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function () {
        altair_md.init();
        altair_md.inputs();
//        altair_forms.textarea_autosize();

    });
</script>
@endsection