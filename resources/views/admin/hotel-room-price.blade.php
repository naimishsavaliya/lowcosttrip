@extends('layouts.admin')
@section('content')


<div id="main-content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="{{ env('ADMIN_URL')}}home"><i class="icon-home"></i></a><span class="divider">&nbsp;</span></li>
                    <li><a href="javascript:;">Manage Hotel Room Price</a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->   
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i>Manage Hotel Room Price</h4>
                        <span class="tools">
                        </span>
                    </div>
                    <div class="widget-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{ env('ADMIN_URL')}}hotel/room" class="" method="get">
                            <div class="cloneDiv cloneData">
                                <div class="control-group span3">
                                    <label class="control-label">Standard Price</label>
                                    <div class="controls">
                                        <input type="text" class="span6 date-picker" placeholder="From" />
                                        <input type="text" class="span6 date-picker" placeholder="To" />
                                    </div>
                                </div>
                                <div class="control-group span1 ">
                                    <label class="control-label">Mon</label>
                                    <div class="controls">
                                        <input type="text" class="span12" />
                                    </div>
                                </div>
                                <div class="control-group span1 ">
                                    <label class="control-label">Tue</label>
                                    <div class="controls">
                                        <input type="text" class="span12" />
                                    </div>
                                </div>
                                <div class="control-group span1 ">
                                    <label class="control-label">Wed</label>
                                    <div class="controls">
                                        <input type="text" class="span12" />
                                    </div>
                                </div>
                                <div class="control-group span1 ">
                                    <label class="control-label">Thu</label>
                                    <div class="controls">
                                        <input type="text" class="span12" />
                                    </div>
                                </div>
                                <div class="control-group span1 ">
                                    <label class="control-label">Fri</label>
                                    <div class="controls">
                                        <input type="text" class="span12" />
                                    </div>
                                </div>
                                <div class="control-group span1 ">
                                    <label class="control-label">Sat</label>
                                    <div class="controls">
                                        <input type="text" class="span12" />
                                    </div>
                                </div>
                                <div class="control-group span1 ">
                                    <label class="control-label">Sun</label>
                                    <div class="controls">
                                        <input type="text" class="span12" />
                                    </div>
                                </div>
                                <div class="control-group span1 ">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="controls">
                                        <a onclick="cloneData();" title="Delete Price" class="btn btn-mini btn-success addPrice"><i class="icon-plus icon-white"></i></a> 
                                        <a href="javascript:;" onclick="removedata(this);" title="Delete Price" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
                                    </div>
                                </div>
                                <div class="clear clearfix"></div>
                            </div>
                            <div class="appendDiv">
                            </div>




                            <div class="clear clearfix"></div>


                            <div class = "form-actions">
                                <button type = "submit" class = "btn btn-success">Submit</button>
                                <button type = "button" class = "btn">Cancel</button>
                            </div>
                        </form>
                        <!--END FORM-->
                    </div>
                </div>
                <!--END SAMPLE FORM widget-->
            </div>
        </div>
    </div>
</div>
<script type = "text/javascript" >
    $(function () {
        $('.date-picker').datepicker({'autoclose': true});

//        $(".addPrice").on('click', function () {
//            $(".appendDiv").append($(".cloneDiv").clone());
//        });
    });
    function removedata(obj) {
        var conf = confirm('Are you sure to delete?');
        if (conf) {
            $(obj).parents('.cloneDiv').remove();
        }
    }
    function cloneData() {
        var clonedata = $(".cloneData").clone();
        clonedata.removeClass("cloneData");
        $(".appendDiv").append(clonedata);

        $('.date-picker').datepicker({'autoclose': true});
    }
</script>
<link rel="stylesheet" type="text/css" href="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/css/datepicker.css" />
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL')}}admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
@endsection