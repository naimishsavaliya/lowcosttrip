@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Quotation</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}quotation/add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th>Lead ID</th>
                        <th>Created Date</th>
                        <th>Quotation Title</th>
                        <th>No of Adults</th>
                        <th>No of Kids</th>
                        <th>Amount</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Lead ID" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Created Date" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Quotation Title" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="No of Adults" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="No of Kids" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Amount" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>21/09/2017</td>
                            <td>lorem tristique aliquet. Phasellus fermentum convallis</td>
                            <td class="hidden-phone"><?php echo rand(2, 4); ?></td>
                            <td class="hidden-phone"><?php echo rand(0, 2); ?></td>
                            <td ><?php echo rand(0, 1); ?></td>
                            <td>
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                    <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav uk-nav-dropdown">
                                            <li><a href="{{ env('ADMIN_URL')}}quotation/edit/1"> View</a></li>
                                            <li><a href="{{ env('ADMIN_URL')}}quotation/edit/1"> Edit</a></li>
                                            <li><a href="{{ env('ADMIN_URL')}}quotation/delete/1"> Delete</a></li>
                                            <li><a href=""> Duplicate</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
