@if(isset($particular) && count($particular) > 0)
@foreach($particular as $par_key => $par_value)
@php
$line_total = 0;
$line_total = $par_value->qty * $par_value->cost;
if(isset($par_value->discount)){
$line_total = $line_total - $par_value->discount;
}
@endphp
<div class="uk-width-medium-1-1 parsley-row form_section particular-row">
    <input type="hidden" name="particular[{{$par_key}}][per_id]" class="md-input par_item_no" id="perId_{{$par_key}}" value="{{ isset($par_value->perId) ? $par_value->perId : '' }}" required="" />
    <div class="uk-input-group uk-grid" style="width:100%">
        <?php /*
        <div class="uk-width-medium-1-10 parsley-row">
            <label for="inv_service_rate">Item No</label>
            <input type="text" name="particular[{{$par_key}}][item_no]" class="md-input par_item_no label-fixed" id="item_no_{{$par_key}}" value="{{ isset($par_value->item_no) ? $par_value->item_no : '' }}" required="" />
        </div> */ ?>
        <div class="uk-width-medium-2-10 parsley-row automplete-div">
            <input type="hidden" name="particular[{{$par_key}}][item_id]" class="par_item_id" id="item_item_id_{{$par_key}}" value="{{ isset($par_value->item_id) ? $par_value->item_id : '' }}" />
            <input type="hidden" name="particular[{{$par_key}}][item_type]" class="par_item_type" id="item_item_type_{{$par_key}}" value="{{ isset($par_value->item_type) ? $par_value->item_type : '' }}" />
            <label for="inv_service">Item Title</label>
            <input type="text" name="particular[{{$par_key}}][item_title]" class="md-input par_item_title automplete_template label-fixed" id="item_title_{{$par_key}}" value="{{ isset($par_value->item_title) ? $par_value->item_title : '' }}" onkeyup="searchAutocomplate(this)" required="" />
            <div id="suggesstion-box"></div>
        </div>
        <div class="uk-width-medium-2-10 parsley-row automplete-div">
            <label for="inv_service">Description</label>
            <input type="text" name="particular[{{$par_key}}][description]" class="md-input par_Description label-fixed" id="par_Description_{{ $par_key }}" value="{{ isset($par_value->description) ? $par_value->description : '' }}" required="" />
            <div id="suggesstion-box"></div>
            <?php /*
            <textarea rows="1" class="md-input par_description label-fixed" name="particular[{{$par_key}}][description]" id="par_description_{{ $par_key }}">{{ isset($par_value->description) ? $par_value->description : '' }}</textarea>
            <div id="suggesstion-box"></div> */ ?>
        </div>
        <div class="uk-width-medium-1-10 parsley-row">
            <label for="inv_service_rate">Qty/Night/Room</label>
            <input type="text" name="particular[{{$par_key}}][qty]" class="md-input par_qty label-fixed" id="par_qty_{{ $par_key }}" value="{{ isset($par_value->qty) ? $par_value->qty : '' }}" required="" />
        </div>
        <div class="uk-width-medium-1-10 parsley-row">
            <label for="inv_service_hours">Cost</label>
            <input type="text" name="particular[{{$par_key}}][cost]" class="md-input par_cost label-fixed" id="par_cost_{{$par_key}}" value="{{ isset($par_value->cost) ? $par_value->cost : '' }}" required="" />
        </div>
        <div class="uk-width-medium-1-10">
            <label for="inv_service_vat">Discount</label>
            <input type="text" name="particular[{{$par_key}}][discount]" class="md-input discount label-fixed" id="discount_{{$par_key}}" value="{{ isset($par_value->discount) ? $par_value->discount : '' }}" />
        </div>
        <div class="uk-width-medium-1-10">
            <label for="inv_service_gst">GST (%)</label>
            <input type="number" name="particular[{{$par_key}}][gst]" class="md-input gst label-fixed" id="gst_{{$par_key}}" value="{{ isset($par_value->gst) ? $par_value->gst : '' }}" />
            <input type="hidden" class="md-input gst_value label-fixed" id="gst_value_{{$par_key}}" value="{{ isset($par_value->gst) ? ($line_total * $par_value->gst) / (100) : '' }}" />
        </div>
        <div class="uk-width-medium-1-10 parsley-row">
            <label for="inv_service_vat">Line Total</label>
            <input type="text" name="particular[{{$par_key}}][line_total]" class="md-input line_total label-fixed" id="line_total_{{$par_key}}" value="{{ $line_total }}" readonly required=""/>
        </div>
        <div class="uk-width-medium-1-10">
            <span class="uk-input-group-addon">
                <a href="#"  data-id="particular"  class="btnSectionClone"><i class="material-icons md-24">add_box</i></a>
                <a href="#" class="btnSectionRemove"><i class="material-icons md-24">delete</i></a>
            </span>
        </div>
    </div>
</div>
@endforeach
@endif