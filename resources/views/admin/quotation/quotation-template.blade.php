
<style>
    .invoice-box {
        /*max-width: 800px;*/
        /*margin: auto;*/
        padding: 10px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 15px;
        line-height: 18px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #000000;
    }

    .invoice-box table {
        width: 100%;
        /*line-height: inherit;*/
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 10px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    .txt-c {
        text-align: center;
    }

    .txt-l {
        text-align: left !important;
    }
    .txt-r {
        text-align: right; !important;
    }
    .invoice-text{
        font-size:25px;
        text-align: center;
    }
    .address{
        font-size:10px;
        padding: 7px;
        text-align: left !important;
        vertical-align: top !important;
    }
    .date{
        font-size:10px;
        padding: 7px;
        text-align: left;
    }
    .mode{
        font-size:10px;
        padding: 7px;
        text-align: left;
    }
    .br-1{
        border:1px solid #000; 
        padding:0px; 
        font-size:10px;
        border-spacing: 0;
    }
    .user-info{
        line-height: 8px;
    }
    .product-list td{
        border:1px solid #000; 
        padding:10px 0px; 
        font-size:10px;
        border-spacing: 0;
    }
    .total-amount{
        text-align: right; line-height: 8px; border-bottom: none !important; border-top: none !important;
    }
    .total-amt{
        text-align: right; line-height: 8px;
    }
    .declaration{
        border:1px solid #000; 
        padding:10px 0px; 
        font-size:10px;
        border-spacing: 0;
    }
</style>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>A simple, clean, and responsive HTML invoice template</title> 
    </head>
    <body>
        <div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td class="title" width="55%">
                                <a  target="_blank" href="http://lowcosttrip.com/"><img src="https://www.lowcosttrip.com/public/new-theme/images/logo.jpg" style="max-width:300px; max-width:300px;"></a>
                                </td>
                                <td class="address"  width="30%">
                                    <strong>Low Cost Trip</strong><br>
                                    IQ Comtech Pvt Ltd ,<br>
                                    1205,Sai Indu Tower ,<br>
                                    Lbs marg, Bhandup, Mumbai - 78. 
                                </td>
                                <td class="address"  width="15%">
                                    <p style="line-height: 5px;">Phone No:</p>
                                    <p style="line-height: 5px;">8182838485</p>
                                    <p style="line-height: 5px;padding-top: 9px;">Email:</p>
                                    <p style="line-height: 5px;">info@lowcosttrip.com</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border-top:1px solid #DDD; padding: 25px;">
                        <table>
                            <tr> 
                                <td class="invoice-text">
                                    Proforma Invoice No.: {{$receipt_no}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="information">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td class="date">
                                    <strong>Invoice Date:</strong><br>
                                    {{ isset($quotation->quatation_date) ? date('d M Y', strtotime($quotation->quatation_date)) : '-' }}
                                </td>

                                <td class="mode">
                                    <strong>Payment Mode:</strong><br>
                                    Cheque/NEFT/RTGS/Payment Gateway
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="60%"> 
                        <table class="user-info" cellspacing="0">
                            <tr>
                                <td class="br-1"><strong>{{ $lead->first_name .' '. $lead->last_name }}</strong></td>
                                <td class="br-1"><strong>State Code:</strong></td>
                                <td class="br-1"><strong>24</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ $lead->city_name }}</td>
                                <td class="br-1">PAN No.</td>
                                <td class="br-1">24</td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ $lead->city_name .', '. $lead->pin_code }}</td>
                                <td class="br-1">GSTIN</td>
                                <td class="br-1">24</td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ $lead->state_name .', '. $lead->country_name }}</td>
                                <td class="br-1">Supply At</td>
                                <td class="br-1">-</td>
                            </tr>
                        </table>
                    </td>
                    <td width="40%">
                        <table class="user-info"  cellspacing="0">
                            <tr>
                                <td class="br-1 txt-c" colspan="2"><strong>Seller GST Details</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1">GSTIN</td>
                                <td class="br-1 txt-l">{{env('GSTIN', '')}}</td>
                            </tr>
                            <tr>
                                <td class="br-1">CIN</td>
                                <td class="br-1 txt-l">{{env('CIN', '')}}</td>
                            </tr>
                            <tr>
                                <td class="br-1">PAN No.</td>
                                <td class="br-1 txt-l">{{env('PAN_NO', '')}}</td>
                            </tr>
                        </table> 
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <table class="product-list"  cellspacing="0">
                            <tr>
                                <td width="8%"><strong>Sr. No. </strong></td>
                                <td width="28%" style="text-align: left"><strong>Particulars </strong></td>
                                <td width="10%"><strong>Rate (INR) </strong></td>
                                <td width="14%"><strong>Qty/Night/Room </strong></td>
                                <td width="10%"><strong>Discount </strong></td>
                                <td width="10%"><strong>GST(%) </strong></td>
                                <td width="10%"><strong>HSN/SAC </strong></td>
                                <td width="10%"><strong>Amount(INR) </strong></td>
                            </tr>

                        @if(isset($particular))

                        @php 
                        $total_cost = $final_cost = $cgst = $igst = $sgst  = 0; 
                        @endphp

                        @foreach($particular as $par_key=>$par_value)

                        @php
                        $linetotal = $par_value->qty * $par_value->cost;
                        if(isset($par_value->discount)){
                        $linetotal = $linetotal - $par_value->discount;
                        }
                        $total_cost = $total_cost + $linetotal;
                        
                        $cgst = $cgst + (($par_value->cgst > 0)?  (($linetotal *  $par_value->cgst) / 100) :0);
                        $sgst = $sgst + (($par_value->sgst > 0)?  (($linetotal *  $par_value->sgst) / 100) :0);
                        $igst = $igst + (($par_value->igst > 0)?  (($linetotal *  $par_value->igst) / 100) :0);
                        
                        @endphp 
                            <tr>
                                <td>{{ $par_value->item_no }}</td>
                                <td style="text-align: left">{{ $par_value->item_title }}</td>
                                <td class="txt-r">{{ $par_value->cost }}</td>
                                <td class="txt-r">{{ $par_value->qty }}</td>
                                <td class="txt-r">{{ $par_value->discount }}</td>
                                <td class="txt-r">{{ number_format($par_value->gst , 2) }}</td>
                                <td class="txt-r">-</td>
                                <td class="txt-r">{{  number_format($linetotal, 2) }}</td>
                            </tr>
                        @endforeach

                            <tr>
                                <td style="text-align: right; line-height: 8px; border-bottom: none; "  colspan="7">Sub Total:</td>
                                <td class="total-amt">{{ $total_cost }}</td>
                            </tr>
                            <tr>
                                <td class="total-amount"  colspan="7">CGST (+):</td>
                                <td  class="total-amt">{{ number_format(round($cgst , 2)) }}</td>
                            </tr>
                            <tr>
                                <td class="total-amount"  colspan="7">SGST (+):</td>
                                <td  class="total-amt">{{ number_format(round($sgst , 2)) }}</td>
                            </tr>
                            <tr>
                                <td class="total-amount"   colspan="7">IGST (+):</td>
                                <td   class="total-amt">{{ number_format(round($igst , 2)) }}</td>
                            </tr>
                            <tr>
                                <?php 
                                     $total_amount = round($total_cost + $cgst + $sgst + $igst);
                                ?>
                                <td class="total-amount"   colspan="7">Total ({{$quotation->code}}):</td>
                                <td  class="total-amt">{{ number_format( $total_amount , 2) }}</td>
                            </tr>
                            <tr>
                                <?php 
                                   //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                   //$fianl_amt = number_format(($total_cost + $cgst + $sgst + $igst) , 2);
                                   //$f->format($fianl_amt)
                                ?>
                                <td colspan="8" >Amount (in words): {{number_to_word($total_amount, $quotation->code)}} </td>
                            </tr>
                        @endif

                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" class="br-1"><strong>Daywise Itinerary Plan</strong></td>
                </tr>
                @if(isset($itinarary))
                    @foreach($itinarary as $iti_key => $iti_value)
                        <tr>
                            <td class="br-1" width="10%">
                                <img class="md-card-head-img" src="{{ base_path() }}/uploads/itinerary/{{$iti_value->image}}" alt="">
                            </td>
                            <td class="br-1" width="90%">
                                <p style="text-align: left; font-weight: bold; line-height: 12px;">Day {{ $iti_value->day .' '. $iti_value->title }}</p>
                                <p style="text-align: left; line-height: 12px;">{{ $iti_value->description }}</p>
                            </td> 
                        </tr>
                    @endforeach
                @endif



                <tr>
                    <td colspan="2">
                        <table class="product-list"  cellspacing="0">
                            <tr>
                                <td class="br-1"><strong>Inclusions</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1"> {{ isset($quotation->inclusion) ? $quotation->inclusion : '' }}</td>
                            </tr>
                        </table>
                    </td>
                </tr> 
                <tr>
                    <td colspan="2">
                        <table class="declaration" cellspacing="0">
                            <tr>
                                <td class="br-1"><strong>Exclusions</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ isset($quotation->exclusion) ? $quotation->exclusion : '' }}</td>
                            </tr>
                        </table>
                    </td>
                </tr> 

                <tr>
                    <td colspan="2" > 
                        <table class="declaration" cellspacing="0">
                            <tr>
                                <td class="br-1"><strong>Terms & Conditions</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1"> {{ isset($quotation->terms_condition) ? $quotation->terms_condition : '' }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr> 

        </div>
    </body>
</html>