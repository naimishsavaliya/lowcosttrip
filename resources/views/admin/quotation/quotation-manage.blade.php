@extends('layouts.theme')

@section('style')
<style>
    .frmSearch {/*border: 1px solid #a8d4b1;/*background-color: #c6f7d0;*/margin: 2px 0px;padding:10px 10px 10px 0;border-radius:4px;}
    #country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;z-index: 999; 
                  width: 400px; height: 200px; overflow: auto;
    }
    #country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
    #country-list li:hover{background:#ece3d2;cursor: pointer;}
    #search-box, .search-box-single{padding: 10px;border-color: rgba(0, 0, 0, 0.12);border-radius:4px;width: 95%;}
    .search-loader{background:#FFF url({{ env('APP_PUBLIC_URL')}}images/LoaderIcon.gif) no-repeat right !important;}
</style>
@endsection

@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <div id="wizard_advanced">
                <!-- first section -->
                <h3>Step 1 - Quotation Details</h3>
                <section style="padding: 24px 35px;">
                    <form class="uk-form-stacked" id="wizard_advanced_form" name="quatation-add-frm" method="post" enctype="multipart/form-data" />
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ isset($quotation->quaId) ? $quotation->quaId : '' }}" id="quotation_id" />
                    <input type="hidden" name="lead_id" value="{{ Request::route('lead_id') }}" id="quotation_lead_id" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">

                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="lead_no">Lead No <span class="req">*</span></label>
                                        <input class="md-input" type="text" name="lead_no" id="lead-no" value="{{ isset($quotation->lead_no) ? $quotation->lead_no : (isset($lead_info->lead_id) ? $lead_info->lead_id : '') }}"  required />
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="subject">Subject</label>
                                        <input type="text" class="md-input" id="quotation-title" name="subject" value="{{ isset($quotation->subject) ? $quotation->subject : '' }}" required />
                                    </div>
                                </div>
                            </div>

                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="no_adults">No of Adults</label>
                                        <input type="text" class="md-input" name="no_adults" id="no_adult" value="{{ isset($quotation->adult) ? $quotation->adult : (isset($lead_info->adult) ? $lead_info->adult : '') }}" required />
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <div class="md-input-wrapper md-input-filled">
                                        <select id="quotation_status" name="quotation_status" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Status..." style="color:#727272" required>
                                            <option value="">Choose Quote Status</option>
                                            @if(isset($quotation_status))
                                            @foreach($quotation_status as $quo_key => $quo_value)
                                            <option value="{{ $quo_key }}" {{ (isset($quotation->status_id) && $quotation->status_id == $quo_key) ? 'selected' : '' }}>{{ $quo_value }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="no_infants">No of Infants (Below 3 Years)</label>
                                        <input type="text" class="md-input" name="no_infants" id="no_infant" value="{{ isset($quotation->infant) ? $quotation->infant : (isset($lead_info->infant) ? $lead_info->infant : '') }}" />
                                    </div>
                                </div>
                                <?php /*
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="no_child">No of Childrens (3 to 12 Years)</label>
                                        <input type="text" class="md-input" name="no_child" id="no_child" value="{{ isset($quotation->child) ? $quotation->child : (isset($lead_info->child) ? $lead_info->child : '') }}" />
                                    </div>
                                </div>
                                */ ?>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="quotation_date">Quote Date</label>
                                        <input class="md-input" type="text" id="quotation_date" name="quotation_date" value="{{ isset($quotation->quatation_date) ? date('d F Y', strtotime($quotation->quatation_date)) : '' }}" required="" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                    </div>
                                </div>
                            </div>

                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="no_child">No of Childrens (3 to 12 Years)</label>
                                        <input type="text" class="md-input" name="no_child" id="no_child" value="{{ isset($quotation->child) ? $quotation->child : (isset($lead_info->child) ? $lead_info->child : '') }}" />
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="valid_date">Valid Up To</label>
                                        <input class="md-input" type="text" id="valid_date" name="valid_date" value="{{ isset($quotation->validup_date) ? date('d F Y', strtotime($quotation->validup_date)) : '' }}" required="" data-uk-datepicker="{format:'DD MMMM YYYY'}">
                                    </div>
                                </div>
                            </div>



                             <div class="uk-width-medium-1-2 p-t-30 parsley-row">
                                <select id="quotation_currency" name="quotation_currency" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Status..." style="color:#727272" required>
                                    <option value="">Choose Currency</option>
                                            @if(isset($quotation_currency))
                                            @foreach($quotation_currency as $currency)
                                            <option data-code="{{ $currency->code }}" value="{{ $currency->currId }}" {{ (isset($quotation->quotation_currency) && $quotation->quotation_currency == $currency->currId) ? 'selected' : '' }}>{{ $currency->symbol.' '.$currency->currency_name }}</option>
                                            @endforeach
                                            @endif
                                </select>
                            </div>
                            <?php /*
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <div class="md-input-wrapper md-input-filled">
                                        <label for="address">Address <span class="required-lbl">*</span></label>
                                        <textarea cols="30" rows="4" class="md-input" name="address">{{ isset($quotation->address) ? $quotation->address : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <div class="uk-width-medium-1-12 display-inline-flex">
                                        <div class="uk-width-medium-1-2 p-r-50 md-input-filled">
                                            <select class="md-input" name="country_id" id="country_id">
                                                <option value="" selected>Choose Country</option>
                                                @if(isset($countrys))
                                                @foreach($countrys as $country)
                                                <option value="{{ $country->country_id }}" {{ (isset($quotation->country_id) && $quotation->country_id == $country->country_id) ? 'selected' : ((isset($lead_info->country_id) && $lead_info->country_id == $country->country_id) ? 'selected' : '') }}>{{ $country->country_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div> */ ?>
                                        <div class="uk-width-medium-1-2 p-r-50 p-l-20 md-input-filled" style="display: none">
                                            <select class="md-input" name="state_id" id="state_id">
                                                <option value="" selected>Choose State</option>
                                                @if(isset($states))
                                                @foreach($states as $state)
                                                <option value="{{ $state->state_id }}" {{ (isset($lead_info->state_id) && $lead_info->state_id == $state->state_id) ? 'selected' : ((isset($lead_info->state_id) && $lead_info->state_id == $state->state_id) ? 'selected' : '') }}>{{ $state->state_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <?php /*
                                    </div>
                                    <div class="uk-width-medium-1-12 p-t-20 display-inline-flex">
                                        <div class="uk-width-medium-1-2 p-r-50 md-input-filled">
                                            <select class="md-input" name="city_id" id="city_id">
                                                <option value="" selected>Choose City</option>
                                                @if(isset($cities))
                                                @foreach($cities as $city)
                                                <option value="{{ $city->city_id }}" {{ (isset($quotation->city_id) && $quotation->city_id == $city->city_id) ? 'selected' : ((isset($lead_info->city_id) && $lead_info->city_id == $city->city_id) ? 'selected' : '') }}>{{ $city->city_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-r-50 p-l-20">
                                            <div class="md-input-wrapper md-input-filled">
                                                <label for="pin_code">Pincode</label>
                                                <input type="text" class="md-input" name="pin_code" id="pin_code" value="{{ isset($quotation->pin_code) ? $quotation->pin_code : (isset($lead_info->pincode) ? $lead_info->pincode : '') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> */ ?>
                        </div>
                    </div>
                    <hr class="form_hr"/>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <h2 class="heading_a">Particular</h2>
                            <div id="particular_field_wizard" data-dynamic-fields="d_field_wizard" class="uk-grid" data-uk-grid-margin  {{ isset($particular) ? "dynamic-fields-counter" : "" }}>
                                 @include('admin.quotation.particular-row')
                        </div>
                        <hr class="md-hr"/>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2 p-r-50">
                                <span class="sub-heading" style="font-weight: bold; font-size: 16px;">Amount in words:</span> 
                                <span class="sub-heading currency_type" style="font-weight: bold; font-size: 16px;"></span>
                                <span class="sub-heading" id="amount-in-word" style="font-weight: bold; font-size: 16px;">-</span>
                            </div>
                            <div class="uk-width-medium-1-2 p-l-50" style="text-align:right">
                                <span class="sub-heading">Sub Total:  <span id="sub-line-amount">00</span></span>
                                <input type="hidden" id="sub-line-amount-hidden" name="sub_total_line_amount" value="" />
                            </div>
                            <div class="uk-width-medium-1-1 p-l-50" style="text-align:right">
                                <span class="sub-heading">CGST:  <span id="cgst-line-amount">00</span></span>
                                <input type="hidden" id="cgst-line-amount-hidden" name="cgst_line_amount" value="" />
                            </div>
                            <div class="uk-width-medium-1-1 p-l-50" style="text-align:right">
                                <span class="sub-heading">SGST:  <span id="sgst-line-amount">00</span></span>
                                <input type="hidden" id="sgst-line-amount-hidden" name="sgst_line_amount" value="" />
                            </div>
                            <div class="uk-width-medium-1-1 p-l-50" style="text-align:right">
                                <span class="sub-heading">IGST:  <span id="igst-line-amount">00</span></span>
                                <input type="hidden" id="igst-line-amount-hidden" name="igst_line_amount" value="" />
                            </div>
                            <div class="uk-width-medium-1-1 p-l-50" style="text-align:right">
                                <span class="sub-heading">Total: <span class="sub-heading currency_type"></span> <span id="total-line-amount">00</span></span>
                                <input type="hidden" id="total-line-amount-hidden" name="total_line_amount" value="" />
                            </div>
                            <input type="hidden" id="company_state"  value="{{strtolower(env('COMPANY_STATE', 'gujarat'))}}" />
                        </div> 
                    </div>
                </div>
            </section>

            <!-- second section -->
            <h3>Step 2 - Itinerary & Terms</h3>
            <section>
                <div class="uk-grid uk-width-medium-1-12">
                    <div class="uk-width-medium-1-1 parsley-row">
                        <div class="frmSearch uk-width-medium-1-2 p-r-50">
                            <label>Search Tour</label>
                            <input type="text" class="search-box-single" id="search-box-single" value="{{ isset($tour->tour_name) ? $tour->tour_name : '' }}" tour_id="{{ isset($tour->id) ? $tour->id : '' }}" required />
                            <div id="suggesstion-box-single"></div>
                        </div>
                    </div>
                </div>
                <div id="itinarary_field_wizard_html" data-dynamic-fields="d_field_wizard_1" class="uk-grid" data-uk-grid-margin {{ isset($itinerarys) ? "dynamic-fields-counter" : "" }}>
                     @include('admin.quotation.itinerary-rows')
            </div>
            <div class="uk-grid" data-uk-grid-margin>

                <div class="uk-width-medium-1-2 p-t-30 parsley-row">
                    <select id="inclusion_exclusion" name="inclusion_exclusion" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Status..." style="color:#727272" required>
                        <option value="">Select Inclusions Exclusions</option>
                        @if(isset($inclusion_exclusion))
                        @foreach($inclusion_exclusion as $inc_exc)
                        <option value="{{ $inc_exc->id }}" {{ (isset($quotation->inclusion_exclusion) && $quotation->inclusion_exclusion == $inc_exc->id) ? 'selected' : '' }}>{{  $inc_exc->title }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="uk-width-medium-1-12 p-t-30 parsley-row">
                    <label for="inclusions">Inclusions</label>
                    <textarea rows="4" cols="30"  name="inclusions" id="inclusions" class="md-input">{{ isset($quotation->inclusion) ? $quotation->inclusion : (isset($tour_info) ? $tour_info->inclusions : '') }}</textarea>
                </div>
                <div class="uk-width-medium-1-12 p-t-30 parsley-row">
                    <label for="exclusions">Exclusions</label>
                    <textarea rows="4" cols="30"  name="exclusions" id="exclusions" class="md-input" style="overflow: auto !important">{{ isset($quotation->exclusion) ? $quotation->exclusion : (isset($tour_info) ? $tour_info->exclusions : '') }}</textarea>
                </div>
                <div class="uk-width-medium-1-12 p-t-30 parsley-row">
                    <label for="terms_conditions">Terms & Conditions</label>
                    <textarea rows="6" cols="30"  name="terms_conditions" id="terms_conditions" class="md-input" required="">{{ isset($quotation->terms_condition) ? $quotation->terms_condition : (isset($tour_info) ? $tour_info->terms_conditions : '') }}</textarea>
                </div>
            </div>
            </form>
        </section>

        <!-- third section -->
        <h3>Step 3 - Review & Send</h3>
        <section>
            <div id="quotation_preview">

            </div>
            <form class="uk-form-stacked" id="send-mail-quote" action="{{ route('quot.send.mail.to.user') }}" method="post" enctype="multipart/form-data" />
                        @csrf
                <div id="send_to_customer" class="p-10">
                    <div class="uk-grid" data-uk-grid-margin>

                        <input type="hidden" name="lead_id" id="hidden_lead_id" value="{{ isset($lead->id) ? $lead->id : '' }}">
                        <input type="hidden" name="quotation_id" id="hidden_quotation_id" value="{{ isset($quotation->quaId) ? $quotation->quaId : '' }}">
                        <div class="uk-width-medium-1-12">
                            <h2 class="heading_c uk-margin-small-bottom">Send To Customer </h2>
                        </div>
                        <div class="uk-width-medium-1-2 p-r-50">
                            <label for="invoice_dp">Customer Name</label>  
                            <input class="md-input" name="name" value="<?php echo ((isset($lead_info->name))? $lead_info->name: ''); ?>" type="text"/>
                        </div>
                        <div class="uk-width-medium-1-2 p-l-50">
                            <label for="invoice_dp">Email ID</label>
                            <input class="md-input" name="email_id" value="<?php echo ((isset($lead_info->email))? $lead_info->email: ''); ?>" type="text"/>
                        </div>
                        <div class="uk-width-medium-1-2 p-r-50">
                            <label for="invoice_dp">CC</label>
                            <input class="md-input"  name="email_cc"  type="text"/>
                        </div>
                        <div class="uk-width-medium-1-2 p-l-50">
                            <label for="invoice_dp">Subject</label>
                            <input class="md-input" id="form_quotation_subject" name="subject" value="{{ isset($quotation->subject) ? $quotation->subject : '' }}" type="text"/>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <label for="invoice_dp">Text area for message draft</label>
                            <textarea cols="30" name="message" rows="6" class="md-input" style="overflow-y: hidden;"></textarea>
                        </div>
                    </div>

                </div>
            </form>
        </section>
    </div>
    <!-- Particular and Itinarary form handler script include -->
    @include('admin.quotation.quotation-script')

</div>
</div>
</div>
<input type="hidden" id="wizard_current_step" value="" />
@endsection

@section('javascript')

<script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    // load extra validators
    altair_forms.parsley_extra_validators();</script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/parsleyjs/dist/parsley.min.js"></script>
<!-- jquery steps -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/wizard_steps.min.js"></script>
<!-- handlebars.js -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>

<!--  forms wizard functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/quotation_forms_wizard.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/get_regions.js"></script>
<script>
    $(document).ready(function () {
        
        $("#search-box-single").keyup(function () {
            var keyword = $.trim($(this).val());
            if (keyword == '') {
                $("#suggesstion-box-single").hide();
                $("#suggesstion-box-single").html('');
                return false;
            }
            $.ajax({
                type: "POST",
                url: "{{ route('tour.name.read') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "keyword": keyword,
                },
                dataType: 'html',
                beforeSend: function () {
                    $("#search-box-single").addClass("search-loader");
                },
                success: function (data) {
                    $("#suggesstion-box-single").show();
                    $("#suggesstion-box-single").html(data);
                    $("#search-box-single").removeClass("search-loader");
                    $("#search-box-single").css("background", "#FFF");
                }
            });
        });

        $('#lead-no').keyup(function () {
            var lead_no = $.trim($(this).val());
            if (lead_no.length > 3) {
                var url = "{{ route('lead.get.detial',':lead_no') }}";
                url = url.replace(':lead_no', lead_no);
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: "json",
                    beforeSend: function () {

                    },
                    success: function (response) {
                        if (response.success == true) {
                            $("select#country_id option[value='" + response.data.country_id + "']").attr('selected', 'selected');
                            getStates(response.data.country_id, response.data.state_id);
                            getCities(response.data.state_id, response.data.city_id);
                            $('input#pin_code').val(response.data.pincode);
                            $('input#no_adult').val(response.data.adult);
                            $('input#no_child').val(response.data.child);
                            $('input#no_infant').val(response.data.infant);
                        } else {
                            $('select#country_id option:selected').removeAttr('selected');
                            $('select#state_id option:selected').removeAttr('selected');
                            $('select#city_id option:selected').removeAttr('selected');
                            $('input#pin_code').val('');
                            $('input#no_adult').val('');
                            $('input#no_child').val('');
                            $('input#no_infant').val('');
                        }
                    }
                });
            }
        });

        $(document.body).on('click', 'li a', function () {
            $txt = $(this).text(); 
            if($txt == 'Finish And Send'){
                 $('#send-mail-quote').submit();
                return false;
            }
        });
        $(document.body).on('change', '#inclusion_exclusion', function () {
             $.ajax({
                type: 'POST',
                url: "{{ route('get.single.data.ajax') }}",
                data: {"_token": "{{ csrf_token() }}", "id" :$(this).val()},
                dataType: 'json',
                beforeSend: function (xhr) {},
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        $('#inclusions').val(data.info.inclusions);
                        $('#exclusions').val(data.info.exclusions);
                        $('#terms_conditions').val(data.info.terms_conditions);
                    }
                }
            });
        });
        $(document.body).on('click', 'li.button_next', function () {
            var $obj = $(this);
            var step = $('#wizard_current_step').val();
            if ($('ul[role="tablist"]').find('li.current').hasClass('error')) {
                return false;
            }

            if (step == 1) {
                var form_serialize = $('form#wizard_advanced_form').serialize();
                var quo_url = "{{ route('quotation.save', ':lead_id') }}";
                quo_url = quo_url.replace(':lead_id', '{{ Request::route("lead_id") }}');

                var form_data = new FormData();
                form_serialize += "&inclusions=" + $('#inclusions').val();
                form_serialize += "&exclusions=" + $('#exclusions').val();
                form_serialize += "&quotation_currency=" + $('#quotation_currency').val();
                form_serialize += "&terms_conditions=" + $('#terms_conditions').val();
                form_serialize += "&inclusion_exclusion=" + $('#inclusion_exclusion').val();
                form_serialize += "&tour_id=" + $('#search-box-single').attr('tour_id');
                form_data.append('data', form_serialize);
                form_data.append('_token', '{{ csrf_token() }}');

                var itinarary_arr = [];
                $('#itinarary_field_wizard_html').find('div.itinarary-row').each(function (key) {
                    var $each_obj = $(this);
                    itinarary_arr.push({day: $each_obj.find('input.itinerary_day').val(),
                        id: $each_obj.find('input.iti_id').val(),
                        title: $each_obj.find('input.itinerary_title').val(),
                        description: $each_obj.find('textarea.itinerary_desciption ').val(),
                    });
                    form_data.append('itenary_image_' + key, $each_obj.find('.itinerary_image')[0].files[0]);
                });

                form_data.append('itinarary', JSON.stringify(itinarary_arr));

                $.ajax({
                    url: quo_url,
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    beforeSend: function (xhr) {
                        var loder_img = "{{ env('APP_PUBLIC_URL')}}theme/assets/img/spinners/spinner_large.gif";
                        $('#quotation_preview').html('<img src="' + loder_img + '" alt="" width="64" height="64">').addClass('text-center');
                    },
                    success: function (data) {
                        var response = $.parseJSON(data);
                        if (response.success) {
                            $('input#quotation_id').val(response.quotation_id);
                            showNotify(response.msg_status, response.message);
                            getQuotationView(response.lead_id, response.quotation_id);
                            $("#hidden_quotation_id").val($('#quotation_id').val());
                            setTimeout( function(){
                                $("#hidden_lead_id").val($('#lead_id_for_quote').val());
                                $("#form_quotation_subject").val($('#quotation_subject').val()).parents('div').addClass('md-input-filled');
                            }, 500);
                        }
                    }
                });
            }

        });

        $(document.body).on('blur', '.par_qty', function () {
            var $obj = $(this);
            var qty = $obj.val();
            var cost = $obj.parents('.particular-row').find('input.par_cost').val();
            var discount = $obj.parents('.particular-row').find('input.discount').val();
            var total_line = qty * cost;
            if (discount != '') {
                total_line = total_line - discount;
            }
            $obj.parents('.particular-row').find('input.line_total').val(total_line);
            calculation_total();
        });

        $(document.body).on('change', '#quotation_currency', function () {
            $('.currency_type').html($('#quotation_currency').find(":selected").attr('data-code'));
        });

        $(document.body).on('blur', '.par_cost', function () {
            var $obj = $(this);
            var cost = $obj.val();
            var qty = $obj.parents('.particular-row').find('input.par_qty').val();
            var discount = $obj.parents('.particular-row').find('input.discount').val();
            var total_line = qty * cost;
            if (discount != '') {
                total_line = total_line - discount;
            }
            $obj.parents('.particular-row').find('input.line_total').val(total_line);
            calculation_total();
        });

        $(document.body).on('blur', '.discount', function () {
            var $obj = $(this);
            var discount = $obj.val();
            var cost = $obj.parents('.particular-row').find('input.par_cost').val();
            var qty = $obj.parents('.particular-row').find('input.par_qty').val();
            var total_line = qty * cost;
            if (discount != '') {
                total_line = total_line - discount;
            }
            $obj.parents('.particular-row').find('input.line_total').val(total_line);
            calculation_total();

        });

        $(document.body).on('blur', '.gst', function () {
            var $obj = $(this);
            var gst = $obj.val();
            var cost = $obj.parents('.particular-row').find('input.line_total').val();
            $obj.parents('.particular-row').find('input.gst_value').val((cost * gst) / (100));
            calculation_total();
        });

        $(document.body).on('change', '.itinerary_image', function () {
            filePreview(this);
        });

        $(document.body).on('change', '#state_id', function () {
            calculation_total();
        }); 

        $(document.body).on('change', '.quotation-title', function () {
            $("#quotation-title").val($('#form_quotation_subject').val());
        });

        calculation_total();

        $("#quotation_currency").trigger('change');
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(input).parents('.image-row').find('.previewHolder').show();
                $(input).parents('.image-row').find('.previewHolder').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    calculation_total();
    function calculation_total() {
        var total_amount = sub_amount =0;
        if($('#particular_field_wizard .particular-row').length > 0){
            $('#particular_field_wizard').find('.particular-row').each(function () {
                var total_line = $(this).find('input.line_total').val();
                if (total_line != '') {
                    total_amount = parseInt(total_amount) + parseInt(total_line);
                    sub_amount = parseInt(sub_amount) + parseInt(total_line);
                }
                
                var gst_amount = $(this).find('input.gst_value').val();
                if (gst_amount != '') {
                    total_amount = parseInt(total_amount) + parseInt(gst_amount);
                } 
                $('#sub-line-amount').html(sub_amount);
                $('#sub-line-amount-hidden').val(sub_amount);
                
                $('#total-line-amount').html(total_amount);
                $('#total-line-amount-hidden').val(total_amount);
                $('#amount-in-word').html(convertNumberToWords(total_amount));
            });
        }else{
            $('#sub-line-amount').html(0.00);
            $('#sub-line-amount-hidden').val(0.00);
            
            $('#total-line-amount').html(0.00);
            $('#total-line-amount-hidden').val(0.00);
            $('#amount-in-word').html(convertNumberToWords(0.00));
        }
         
        var gst_amount = parseFloat(total_amount) - parseFloat(sub_amount);
        
        var state = $("#state_id option:selected").text();
        var company_state = $("#company_state").val();
        
        if(state.toLowerCase() == company_state.toLowerCase()){
            $('#cgst-line-amount-hidden').val(gst_amount / 2);
            $('#sgst-line-amount-hidden').val(gst_amount / 2);
            $('#cgst-line-amount').html(gst_amount / 2);
            $('#sgst-line-amount').html(gst_amount / 2);
            
            $('#igst-line-amount-hidden').val(0);
            $('#igst-line-amount').html(0);
        }else{
            $('#igst-line-amount-hidden').val(gst_amount);
            $('#igst-line-amount').html(gst_amount);
            
            $('#cgst-line-amount-hidden').val(0.00);
            $('#sgst-line-amount-hidden').val(0.00);
            $('#cgst-line-amount').html(0.00);
            $('#sgst-line-amount').html(0.00);
        }
    }

    function getQuotationView(lead_id, quotation_id) {
        var url = "{{ route('quotation.view', ['lead_id' => ':lead_id', 'quotation_id' => ':quotation_id']) }}";
        url = url.replace(':lead_id', lead_id).replace(':quotation_id', quotation_id);
        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {
                var loder_img = "{{ env('APP_PUBLIC_URL')}}theme/assets/img/spinners/spinner_large.gif";
                $('#quotation_preview').html('<img src="' + loder_img + '" alt="" width="64" height="64">').addClass('text-center');
            },
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    $('#quotation_preview').removeClass('text-center');
                    $('#quotation_preview').html(data.contents);
                    $(window).resize();
                }
            }
        });
    }


    function selectTour(val, tour_id) {
        $("#search-box-single").val(val);
        $("#search-box-single").attr('tour_id', tour_id);
        $("#suggesstion-box-single").html('');
        $("#suggesstion-box-single").hide();
        getTourItninarary(tour_id);
    }

    function getStates(country_id, state_id) {
        var url = '{{ route("state_by_country", ":country_id")}}';
        url = url.replace(':country_id', country_id);
        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#state_id')
                        .find('option')
                        .remove();
                $('#state_id').append('<option value="">Choose State</option>');
                $.map(data, function (item) {
                    $('#state_id').append('<option value="' + item.state_id + '">' + item.state_name + '</option>');
                });
                $("select#state_id option[value='" + state_id + "']").attr('selected', 'selected');
                $("#state_id").trigger("chosen:updated");
                $("#state_id").trigger("liszt:updated");
            }
        });
    }

    function getCities(state_id, city_id) {
        var url = '{{ route("city_by_state", ":state_id")}}';
        url = url.replace(':state_id', state_id);
        $.ajax({
            type: 'GET',
            url: url,
            data: {
                "_token": "{{csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#city_id')
                        .find('option')
                        .remove();
                $('#city_id').append('<option value="">Choose City</option>');
                $.map(data, function (item) {
                    $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
                });
                $("select#city_id option[value='" + city_id + "']").attr('selected', 'selected');
                $("#city_id").trigger("chosen:updated");
                $("#city_id").trigger("liszt:updated");
            }
        });
    }

    function getTourItninarary(tour_id) {
        var url = '{{ route("tour.itinerary.quotation" , ":tour_id")}}';
        url = url.replace(":tour_id", tour_id);
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                if (data.contents != '') {
                    $('#itinarary_field_wizard_html').html(data.contents);
                    $('#inclusions').val(data.inclusions);
                    $('#exclusions').val(data.exclusions);
                    $('#terms_conditions').val(data.terms_conditions);
                } else {
                    showNotify('warning', "No itinarary available in this tour");
                }
                $(window).resize();
            }
        });
    }

    function searchAutocomplate1(obj) {
        // return false;
        var parent = $(obj).parents('.automplete-div');
        var keyword = $.trim($(obj).val());
        if (keyword == '') {
            parent.find("#suggesstion-box").hide();
            parent.find("#suggesstion-box").html('');
            return false;
        }

        $.ajax({
            type: "POST",
            url: "{{ route('tour.name.read.json') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "keyword": keyword,
            },
            dataType: 'html',
            beforeSend: function () {
                $(obj).addClass("search-loader");
            },
            success: function (data) {
                parent.find("#suggesstion-box").show();
                parent.find("#suggesstion-box").html(data);
                $(obj).removeClass("search-loader");
                $(obj).css("background", "#FFF");
            }
        });
    }

    function selectTourDynamic(obj, val, item_id, item_type) {
        var parent = $(obj).parents('div.automplete-div');
        var t = parent.find("input.automplete_template").val(val);
        var t = parent.find("input.par_item_id").val(item_id);
        var t = parent.find("input.par_item_type").val(item_type);
        parent.find("#suggesstion-box").html('');
        parent.find("#suggesstion-box").hide();
    }


</script>
@endsection