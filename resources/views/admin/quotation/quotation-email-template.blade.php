
<!doctype html>
<html>
    <head>
        <meta charset="utf-8"> 
    </head>
    <body>
        <div style="width: 800px;">
            <?php echo ((isset($user_name) && $user_name != '') ? '<pre>Dear Customer <strong>'.$user_name.',</strong></pre>': ''); ?>
            <?php echo ((isset($draft_message) && $draft_message != '') ? '<pre>'.$draft_message.'</pre>': ''); ?>
        </div>

        <div style="padding: 10px; border: 1px solid #eee; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 15px; line-height: 18px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #000000; width: 800px;">
            
            <table style="width: 100%; text-align: left;" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3">
                        <table style="width: 100%; text-align: left;">
                            <tr>
                                <td width="55%">
                                    <a  target="_blank" href="http://lowcosttrip.com/"><img src="https://www.lowcosttrip.com/public/new-theme/images/logo.jpg" style="max-width:300px; max-width:300px;"></a>
                                </td>
                                <td style="font-size:13px; padding: 7px; text-align: left !important;vertical-align: top " width="30%">
                                    <strong>Low Cost Trip</strong><br>
                                    IQ Comtech Pvt Ltd ,<br>
                                    1205,Sai Indu Tower ,<br>
                                    Lbs marg, Bhandup,<br>
                                    Mumbai - 400 078. 
                                </td>
                                <td style="font-size:13px; padding: 7px; text-align: left !important;vertical-align: top " width="15%">
                                    Phone no:<br>
                                    8182838485 <br><br>
                                    Email:<br>
                                    info@lowcosttrip.com
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3" style="border-top:1px solid #DDD; padding: 25px; ">
                        <table style="width: 100%; text-align: left;">
                            <tr> 
                                <td class="invoice-text" style="font-size:25px; text-align: center; font-weight: lighter ">
                                    Quotation #{{ isset($quotation->quatation_id) ? $quotation->quatation_id : ''}}
                                </td>
                            </tr>
                            <?php if(isset($receipt_no) && $receipt_no != ''){ ?>
                            <tr> 
                                <td class="invoice-text" style="font-size:25px; text-align: center; font-weight: lighter; padding-top: 25px">
                                    Proforma invoice no. : {{ $receipt_no }}
                                </td>
                            </tr>
                            <?php }  ?>
                            <tr> 
                                <td class="invoice-text" style="font-size:25px; text-align: center; font-weight: lighter; padding-top: 25px ">
                                    Quote For {{$quotation->subject}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <table style="width: 100%; text-align: left;">
                            <td>
                                Quotation Date: {{ isset($quotation->quatation_date) ? date('d M Y', strtotime($quotation->quatation_date)) : '-' }}
                            </td>
                            <td style="text-align: right;">
                                Valid Up To: {{ isset($quotation->validup_date) ? date('d M Y', strtotime($quotation->validup_date)) : '-' }}
                            </td>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="40%"  style="vertical-align: top;"> 
                        <table style="width: 100%; text-align: left; line-height: 8px;"   cellspacing="0">
                            <tr>
                                <td width="40%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Customer Name</strong></td>
                                <td width="60%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;">{{ isset($user_info->first_name) ? ucwords($user_info->first_name. ' '. $user_info->last_name): '' }}</td>
                            </tr>

                            <tr>
                                <td width="40%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Address</strong></td>
                                <td width="60%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;line-height: 18px;">
                                <?php 
                                 $address = ( (isset($user_info->address) &&(strlen($user_info->address) > 50)) ?substr($user_info->address, 0, 50).'...':(isset($user_info->address) ? $user_info->address : '')); 
                                ?>
                                {{ $address }}</td>
                            </tr>
                            <tr>
                                <td width="40%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Country</strong></td>
                                <td width="60%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;">{{ isset($user_info->country_name) ? $user_info->country_name : '' }}</td>
                            </tr>
                            <tr>
                                <td width="40%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>State</strong></td>
                                <td width="60%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;">{{ isset($user_info->state_name) ? $user_info->state_name : '' }}</td>
                            </tr>
                            <tr>
                                <td width="40%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>City</strong></td>
                                <td width="60%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;">{{ isset($user_info->city_name) ? $user_info->city_name : '' }}</td>
                            </tr>
                        </table>
                    </td>
                    <td width="10%">
                    </td>
                    <td width="50%" style="vertical-align: top;">
                        <table style="width: 100%; text-align: left; line-height: 8px;" cellspacing="0"> 
                           
                            <tr>
                                <td width="30%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Customer Mo.</strong></td>
                                <td width="70%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;text-align: left !important;">{{ isset($user_info->phone_no) ? $user_info->phone_no : '' }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Customer Email</strong></td>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;text-align: left !important;">{{ isset($user_info->email) ? $user_info->email : '' }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Quote Status</strong></td>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;text-align: left !important;">{{ isset($quotation->quotation_status) ? $quotation->quotation_status : '-' }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Quote Made By</strong></td>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;text-align: left !important;">{{ $quotation->first_name .' '. $quotation->last_name }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Quick contact</strong></td>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;text-align: left !important;">987456325</td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3">
                        <table style="margin-top: 35px; width: 100%;" cellspacing="0">
                            <tr>
                                <td style="font-size:20px; font-weight: lighter ">
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <table style="margin-top: 15px; width: 100%;" cellspacing="0">
                            <tr>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0;" width="5%"><strong>No</strong></td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0;" width="50%"><strong>Particulars </strong></td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0;" width="5%"><strong>Qty/Night/Room</strong></td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0;" width="10%"><strong>Rate </strong></td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0;" width="10%"><strong>Discount </strong></td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0;" width="10%"><strong>GST(%) </strong></td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0;" width="10%"><strong>Line Total</strong></td>
                            </tr>

                            @if(isset($particular))

                            @php 
                            $total_cost = $final_cost = $cgst = $igst = $sgst  = 0; 
                            @endphp

                            @foreach($particular as $par_key=>$par_value)

                            @php
                            $linetotal = $par_value->qty * $par_value->cost;
                            if(isset($par_value->discount)){
                            $linetotal = $linetotal - $par_value->discount;
                            }
                            $total_cost = $total_cost + $linetotal;

                            $cgst = $cgst + (($par_value->cgst > 0)?  (($linetotal *  $par_value->cgst) / 100) :0);
                            $sgst = $sgst + (($par_value->sgst > 0)?  (($linetotal *  $par_value->sgst) / 100) :0);
                            $igst = $igst + (($par_value->igst > 0)?  (($linetotal *  $par_value->igst) / 100) :0);

                            @endphp 
                            <tr>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0;">{{ $par_value->item_no }}</td>
                                <td style="padding:5px; font-size:13px;text-align: left;border:1px solid #000; border-spacing: 0;">{{ $par_value->item_title }}</td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0; text-align: right;">{{ $par_value->qty }}</td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0; text-align: right;">{{ $par_value->cost }}</td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0; text-align: right;">{{ $par_value->discount,2 }}</td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0; text-align: right;">{{ number_format($par_value->gst , 2) }}</td>
                                <td style="padding:5px; font-size:13px;border:1px solid #000; border-spacing: 0; text-align: right;">{{ number_format($linetotal, 2) }}</td>
                            </tr>
                            @endforeach

                            <tr>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px; border-bottom: none; "  colspan="6">Sub Total:</td>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px;">{{ number_format($total_cost, 2) }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px; border-bottom: none !important; border-top: none !important;" colspan="6">CGST (+):</td>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px;">{{ number_format(round($cgst , 2)) }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px; border-bottom: none !important; border-top: none !important;" colspan="6">SGST (+):</td>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px;">{{ number_format(round($sgst , 2)) }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px; border-bottom: none !important; border-top: none !important;"  colspan="6">IGST (+):</td>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px;">{{ number_format(round($igst , 2)) }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px; border-top: none !important;"  colspan="6">Total ({{$quotation->code}}):</td>
                                <?php
                                $total_amount = round($total_cost + $cgst + $sgst + $igst);
                                ?>
                                <td style="border:1px solid #000;padding:9px; font-size:13px;text-align: right; line-height: 8px;">{{ number_format( $total_amount , 2) }}</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000;padding:9px; font-size:13px; line-height: 8px;border-right: none;" colspan="5" ><strong>Amount (in words): </strong> {{number_to_word($total_amount, $quotation->code)}}
                                   
                                </td>
                                <td style="border:1px solid #000;padding:9px; font-size:13px; line-height: 8px;border-left: none;text-align: right" colspan="3" >
                                    <?php if ($quotation->status_id == 3) { ?>
                                         <a style=" padding: 6px 12px; line-height: 1.42; border: 1px solid transparent;color: #fff; background-color: #449d44; border-color: #398439;text-decoration: none" href="{{route('quotation.make.payment', [$quotation->lead_id, $quotation->quaId])}}">PAY ONLINE</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            @endif

                        </table>
                    </td>
                </tr>

               @if(isset($itinarary))
                <tr>
                    <td colspan="3" > 
                        <table style="width: 100%; text-align: left;  padding:10px 0px; font-size:13px; border-spacing: 0;float: left"   cellspacing="0">
                            <tr>
                                <td colspan="2" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Daywise Itinerary Plan</strong></td>
                            </tr>
                                <?php $itinerary_image = $itinerary_no_image = ''; $image_exits = 0;
                                foreach($itinarary as $iti_key => $iti_value){
                                    $img = env('APP_PUBLIC_URL').'/uploads/itinerary/'.$iti_value->image;
                                    if(@getimagesize( $img )){
                                        $image_exits++;
                                    }
                                    $itinerary_image .='<tr>
                                        <td  width="20%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;text-align: center">
                                            <img height="120px" width="120px" src="'.env('APP_PUBLIC_URL').'/uploads/itinerary/'.$iti_value->image.'">
                                            
                                        </td>
                                        <td  width="80%" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;">
                                            <p style="text-align: left; font-weight: bold; line-height: 12px;">Day '. $iti_value->day .' '. $iti_value->title.'</p>
                                            '.$iti_value->description.'
                                        </td>
                                    </tr>';

                                    $itinerary_no_image .='<tr>
                                        <td colspan="2" style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;">
                                             <p style="text-align: left; font-weight: bold; line-height: 12px;">Day '. $iti_value->day .' '. $iti_value->title.'</p>
                                            '.$iti_value->description.'
                                        </td>
                                    </tr>';
                                }
                                echo (($image_exits > 0) ? $itinerary_image : $itinerary_no_image); ?>
                        </table>
                    </td>
                </tr>
               @endif

                <tr>
                    <td colspan="3" > 
                        <table style="width: 50%; text-align: left;  padding:10px 0px; font-size:13px; border-spacing: 0;float: left"   cellspacing="0">
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Inclusions</strong></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"> {{ isset($quotation->inclusion) ? $quotation->inclusion : '' }}
                                </td>
                            </tr>
                        </table>
                        <table style="width: 50%; text-align: left;  padding:10px 0px; font-size:13px; border-spacing: 0;"   cellspacing="0">
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Exclusions</strong></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"> {{ isset($quotation->exclusion) ? $quotation->exclusion : '' }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="3" > 
                        <table style="width: 100%; text-align: left;  padding:10px 0px; font-size:13px; border-spacing: 0;"   cellspacing="0">
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Terms & Conditions</strong></td>
                            </tr>
                            <tr>
                                <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"> {{ isset($quotation->terms_condition) ? $quotation->terms_condition : '' }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php /*
                  <tr>
                  <td colspan="3" >
                  <table style="width: 100%; text-align: left; padding:10px 0px; font-size:13px; border-spacing: 0;"  cellspacing="0">
                  <tr>
                  <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;"><strong>Notes</strong></td>
                  </tr>
                  <tr>
                  <td style="border:1px solid #000; padding:9px; font-size:13px; border-spacing: 0;">simply dummy text of the printing and typesetting industry<br>
                  simply dummy text of the printing and typesetting industry
                  </td>
                  </tr>
                  </table>
                  </td>
                  </tr> */ ?>
        </div>
    </body>
</html>
