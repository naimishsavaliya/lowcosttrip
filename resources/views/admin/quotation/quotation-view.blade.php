<div class="md-card">
    <div class="md-card-content invoice_content print_bg">
        <div class="uk-grid" data-uk-grid-margin="">

            <div class="uk-width-small-12 uk-row-first uk-text-center">
                <h3 style="float:none" class="uk-margin-bottom lead_no md-card-toolbar-heading-text large" id="invoice_name">
                     Quotation Number : {{ isset($quotation->quatation_id) ? $quotation->quatation_id : ''}}
                </h3>
            </div>

            <div class="uk-grid uk-width-medium-1-1" style="display: block;">
                <div class="uk-width-medium-1-4">
                    <table class="uk-table">
                        <thead>
                            <tr>
                                <th colspan="2"><strong>Quote For :{{ isset($lead->name) ? $lead->name : '' }} 
                                </strong></th>
                            </tr>
                            <tr>
                                <td>Lead Id</td>
                                <td>{{ isset($lead->lead_id) ? $lead->lead_id : '' }}</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>{{ $lead->address }}</td>
                            </tr>
                            <tr>
                                <td>Phone No.</td>
                                <td>{{ isset($lead->phone) ? $lead->phone : '' }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{ isset($lead->email) ? $lead->email : '' }}</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="uk-width-medium-1-4" style="float: right">
                    <table class="uk-table">
                        <thead>
                            <tr>
                                <th colspan="2"><strong>{{ $quotation->subject }}</strong></th>
                            </tr>
                            <tr>
                                <td>Quote Date</td>
                                <td>{{ isset($quotation->quatation_date) ? date('d M Y', strtotime($quotation->quatation_date)) : '-' }}</td>
                            </tr>
                            <tr>
                                <td>Valid Up To</td>
                                <td>{{ isset($quotation->validup_date) ? date('d M Y', strtotime($quotation->validup_date)) : '-' }}</td>
                            </tr>
                            <tr>
                                <td>Quote Status</td>
                                <td>{{ isset($quotation->quotation_status) ? $quotation->quotation_status : '-' }}</td>
                            </tr>
                            <tr>
                                <td>Quote Made By</td>
                                <td>{{ $quotation->first_name .' '. $quotation->last_name }}</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
 
            <?php /*
            <div class="uk-width-small-3-5 uk-row-first">
                <p style="float:none; font-size: 25px" class="uk-margin-bottom lead_no md-card-toolbar-heading-text large" id="invoice_name">
                    {{ isset($lead->name) ? $lead->name : '' }} 
                    #{{ isset($lead->id) ? $lead->id : '' }}/
                    {{ isset($quotation->quaId) ? $quotation->quaId : '' }}
                </p>
                <table style="border:none;" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top"><span class="lead_no">Quote Date:</span></td>
                        <td valign="bottom" align="right"><span class="lead_no">{{ isset($quotation->quatation_date) ? date('d M Y', strtotime($quotation->quatation_date)) : '-' }}</span></td>
                    </tr>
                    <tr>
                        <td valign="top"><span class="lead_no">Valid Up To:</span></td>
                        <td valign="bottom" align="right"><span class="lead_no">{{ isset($quotation->validup_date) ? date('d M Y', strtotime($quotation->validup_date)) : '-' }}</span></td>
                    </tr>
                    <tr>
                        <td valign="top"><span class="lead_no">Quote Status:</span></td>
                        <td valign="bottom" align="right"><span class="uk-badge uk-badge-danger">{{ isset($quotation->quotation_status) ? $quotation->quotation_status : '-' }}</span></td>
                    </tr>
                </table>
                
            </div>
           
            <div class="uk-width-small-3-5 uk-row-first">
                <p style="float:none; font-size: 25px" class="uk-margin-bottom lead_no md-card-toolbar-heading-text large" id="invoice_name"> {{ $quotation->subject }}
                </p>
                <table style="border:none;" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top"><span class="lead_no">Quote Date:</span></td>
                        <td valign="bottom" align="right"><span class="lead_no">{{ isset($quotation->quatation_date) ? date('d M Y', strtotime($quotation->quatation_date)) : '-' }}</span></td>
                    </tr>
                    <tr>
                        <td valign="top"><span class="lead_no">Valid Up To:</span></td>
                        <td valign="bottom" align="right"><span class="lead_no">{{ isset($quotation->validup_date) ? date('d M Y', strtotime($quotation->validup_date)) : '-' }}</span></td>
                    </tr>
                    <tr>
                        <td valign="top"><span class="lead_no">Quote Status:</span></td>
                        <td valign="bottom" align="right"><span class="uk-badge uk-badge-danger">{{ isset($quotation->quotation_status) ? $quotation->quotation_status : '-' }}</span></td>
                    </tr>
                </table>
                
            </div>
            <div class="uk-width-small-2-5" style="text-align:right">
                <address>
                    <p><span>Address:</span></p>
                    <p><strong>{{ $quotation->first_name .' '. $quotation->last_name }}</strong></p>
                    <p>{{ $quotation->country_name .', '. $quotation->state_name }}</p>
                    <p>{{ $quotation->city_name .', '. $quotation->pin_code }}</p>
                </address>          
            </div>
        </div>
        <h3 style="float:none" class="md-card-toolbar-heading-text large">
            {{ $quotation->subject }}
        </h3>
         */ ?>
        <div class="uk-grid uk-width-medium-1-1">
            <div class="uk-width-1-1">
                <table class="uk-table">
                    <thead>
                        <tr class="uk-text-upper">
                            <th>Item No</th>
                            <th>Particulars</th>
                            <th class="uk-text-center">Qty/Night/Room</th>
                            <th class="uk-text-center">Cost</th>
                            <th class="uk-text-center">Discount</th>
                            <th class="uk-text-center">GST (%)</th>
                            <th class="uk-text-right">Line Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($particular))

                        @php 
                        $total_cost = $final_cost = $cgst = $igst = $sgst  = 0; 
                        @endphp

                        @foreach($particular as $par_key=>$par_value)

                        @php
                        $linetotal = $par_value->qty * $par_value->cost;
                        if(isset($par_value->discount)){
                        $linetotal = $linetotal - $par_value->discount;
                        }
                        $total_cost = $total_cost + $linetotal;
                        
                        $cgst = $cgst + (($par_value->cgst > 0)?  (($linetotal *  $par_value->cgst) / 100) :0);
                        $sgst = $sgst + (($par_value->sgst > 0)?  (($linetotal *  $par_value->sgst) / 100) :0);
                        $igst = $igst + (($par_value->igst > 0)?  (($linetotal *  $par_value->igst) / 100) :0);
                        
                        @endphp

                        <tr class="uk-table-middle">
                            <td>{{ $par_value->item_no }}</td>
                            <td>{{ $par_value->item_title }}</td>
                            <td class="uk-text-center">{{ $par_value->qty }}</td>
                            <td class="uk-text-center">{{ $par_value->cost }}</td>
                            <td class="uk-text-center">{{ $par_value->discount }}</td>
                            <td class="uk-text-center">{{ number_format($par_value->gst , 2) }}</td>
                            <td class="uk-text-right">{{ number_format($linetotal, 2) }}</td>
                        </tr>
                        @endforeach
                        <tr class="uk-table-middle">
                            <td colspan="7" class="uk-text-right"><b>Sub Total : {{$quotation->code}} {{ number_format(round($total_cost, 2)) }}</b></td>
                        </tr>
                        
                        <tr class="uk-table-middle">
                            <td colspan="7" class="uk-text-right"><b>CGST : {{$quotation->code}} {{ number_format(round($cgst , 2)) }}</b></td>
                        </tr>
                        <tr class="uk-table-middle">
                            <td colspan="7" class="uk-text-right"><b>SGST : {{$quotation->code}} {{ number_format(round($sgst , 2)) }}</b></td>
                        </tr>
                        <tr class="uk-table-middle">
                            <td colspan="7" class="uk-text-right"><b>IGST : {{$quotation->code}} {{ number_format(round($igst , 2)) }}</b></td>
                        </tr>
                        
                        
                        <tr class="uk-table-middle">
                        <?php $total_amount = $total_cost + $cgst + $sgst + $igst; ?>
                            <td colspan="7" class="uk-text-right"><b>Total : {{$quotation->code}} {{ number_format(round(($total_amount) , 2)) }}</b></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <p class="uk-text-small"><span style="font-weight: bold; font-size: 16px;">Amount In Word:   {{number_to_word($total_amount, $quotation->code)}}</span></p>
            </div>
        </div>
    </div>
</div>
<div class="md-card">
    <div class="md-card-content invoice_content print_bg list_view">
        <h2 class="heading_c uk-margin-small-bottom daywise-itinerary-plan">Daywise Itinerary Plan</h2>
        @if(isset($itinarary))
        @foreach($itinarary as $iti_key => $iti_value)
        <div class="line">
            <div class="md-card-head uk-text-center uk-position-relative">
                <img class="md-card-head-img" src="{{ env('APP_PUBLIC_URL')}}/uploads/itinerary/{{$iti_value->image}}" alt="">
            </div>
            <div class="md-card-content">
                <h4 class="heading_c uk-margin-bottom">
                    Day {{ $iti_value->day .' '. $iti_value->title }} </h4>
                <p>{{ $iti_value->description }} </p>
            </div>
        </div>
        @endforeach
        @endif
    </div>

    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-large-1-2 uk-width-medium-1-2">
                <h3 class="heading_a">Inclusions</h3>
                {{ isset($quotation->inclusion) ? $quotation->inclusion : '' }}
            </div>
            <div class="uk-width-large-1-2 uk-width-medium-1-2">
                <h3 class="heading_a">Exclusions </h3>
                {{ isset($quotation->exclusion) ? $quotation->exclusion : '' }}
            </div>
        </div>
    </div>
    <div class="md-card-content">
        <div class="uk-grid uk-grid-divider" data-uk-grid-margin>
            <div class="uk-width-large-1-12 uk-width-medium-1-2">
                <h2 class="heading_c uk-margin-small-bottom">Terms & Conditions</h2>
                {{ isset($quotation->terms_condition) ? $quotation->terms_condition : '' }}
            </div>
        </div>
    </div>
                                
    <input type="hidden" id="lead_id_for_quote" value="{{ isset($lead->id) ? $lead->id : '' }}">
    <input type="hidden" id="quotation_id" value="{{ isset($quotation->quaId) ? $quotation->quaId : '' }}">
    <input type="hidden" id="quotation_subject" value="{{ isset($quotation->subject) ? $quotation->subject : '' }}">
</div>
