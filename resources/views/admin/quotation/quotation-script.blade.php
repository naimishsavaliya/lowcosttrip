<script id="d_field_wizard" type="text/x-handlebars-template">
    <div class="uk-width-medium-1-1 parsley-row form_section particular-row js-row">
    <div class="uk-input-group uk-grid" style="width:100%">
    
    <div class="uk-width-medium-2-10 parsley-row automplete-div">
    <input type="hidden" name="particular[@{{counter}}][item_id]" class="par_item_id" id="item_item_id@{{counter}}" />
    <input type="hidden" name="particular[@{{counter}}][item_type]" class="par_item_type" id="item_item_type@{{counter}}" />
    <label for="inv_service">Item Title</label>
    <input type="text" name="particular[@{{counter}}][item_title]" class="md-input par_item_title automplete_template label-fixed" id="item_title@{{counter}}" onkeyup="searchAutocomplate(this)" required="" />
    <div id="suggesstion-box"></div>
    </div>
    <div class="uk-width-medium-2-10 parsley-row automplete-div">
        <label for="inv_service">Description</label>
        <input type="text" name="particular[@{{counter}}][description]" class="md-input par_description label-fixed" id="par_description@{{counter}}" required="" />
        <div id="suggesstion-box"></div>
    </div>
    <div class="uk-width-medium-1-10 parsley-row">
    <label for="inv_service_rate">Qty/Night/Room</label>
    <input type="text" name="particular[@{{counter}}][qty]" class="md-input par_qty label-fixed" id="par_qty@{{counter}}" required="" />
    </div>
    <div class="uk-width-medium-1-10 parsley-row">
    <label for="inv_service_hours">Cost</label>
    <input type="text" name="particular[@{{counter}}][cost]" class="md-input par_cost label-fixed" id="par_cost@{{counter}}" required="" />
    </div>
    <div class="uk-width-medium-1-10">
    <label for="inv_service_vat">Discount</label>
    <input type="text" name="particular[@{{counter}}][discount]" class="md-input discount label-fixed" id="discount@{{counter}}" />
    </div>
    <div class="uk-width-medium-1-10">
        <label for="inv_service_gst">GST (%)</label>
        <input type="number" name="particular[@{{counter}}][gst]" class="md-input gst label-fixed" id="gst_@{{counter}}"  />
        <input type="hidden" class="md-input gst_value label-fixed" id="gst_value_@{{par_key}}" />
    </div>
    <div class="uk-width-medium-1-10 parsley-row">
    <label for="inv_service_vat">Line Total</label>
    <input type="text" name="particular[@{{counter}}][line_total]" class="md-input line_total label-fixed" id="line_total@{{counter}}" readonly required=""/>
    </div>
    <div class="uk-width-medium-1-10">
    <span class="uk-input-group-addon">
    <a href="#" data-id="particular" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
    <!-- <a href="#" class="btnSectionRemove display-none"><i class="material-icons md-24">delete</i></a> -->
    </span>
    </div>
    </div>
    </div>
</script>
<script id="d_field_wizard_1" type="text/x-handlebars-template">
    <div class="uk-width-medium-1-1 parsley-row form_section itinarary-row">
    <div class="uk-input-group uk-grid" style="width:100%">
    <div class="uk-width-medium-1-3 p-t-8">
    <div class="uk-form-row parsley-row">
    <label for="invoice_dp">Day 101</label>
    <input type="text" name="itinerary[@{{counter}}][day]" id="itinerary_day" class="md-input itinerary_day" required="" />
    </div>
    <div class="uk-form-row parsley-row">
    <label for="inv_service">Title</label>
    <input type="text" name="itinerary[@{{counter}}][title]" id="itinerary_title" class="md-input itinerary_title" required="" />
    </div>
    </div>
    <div class="uk-width-medium-1-2 parsley-row">
    <label for="inv_service_rate">Description</label>
    <textarea cols="30" name="itinerary[@{{counter}}][description]" id="itinerary_desciption" rows="4" class="md-input itinerary_desciption" required=""></textarea>
    </div>
    <div class="uk-width-medium-1-6 image-row">
    <img class="previewHolder" src="" alt="Preview Holder" width="150px" height="150px" style="max-width: 69%;padding: 5px;display:none; "/>
    <div class="uk-form-file md-btn md-btn-primary">
    Select image
    <input type="file" name="itinerary[@{{counter}}][image]" class="itinerary_image" id="form-file" />
    </div>
    <span class="uk-input-group-addon" style="padding: 0;float: right;display: inline-block;">
    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
    </span>
    </div>
    </div>
    </div>
    <hr class="form_hr width-100">
</script>
