@if(Session::has('success_msg'))            
<div class="uk-alert uk-alert-success" data-uk-alert="">
    <a href="javascript:;" class="uk-alert-close uk-close"></a>
    {{ Session::get('success_msg') }}
</div>
@endif
@if(Session::has('error_msg'))            
<div class="uk-alert uk-alert-danger" data-uk-alert="">
    <a href="javascript:;" class="uk-alert-close uk-close"></a>
    {{ Session::get('error_msg') }}
</div>
@endif
@if(isset($deleted) && $deleted==true)
<div class="uk-alert uk-alert-success" data-uk-alert="">
    <a href="javascript:;" class="uk-alert-close uk-close"></a>
    Deleted successfully.
</div>
@endif
@if(isset($added) && $added==true)
<div class="uk-alert uk-alert-success" data-uk-alert="">
    <a href="javascript:;" class="uk-alert-close uk-close"></a>
    Added successfully.
</div>
@endif