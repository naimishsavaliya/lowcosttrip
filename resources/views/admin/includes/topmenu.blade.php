<!-- main header -->
<header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">
            <div class="main_logo_top">
                <a href="{{ env('ADMIN_URL')}}home" style="color: #FFF;font-size: 20px;">
                    <img src="{{ env('APP_PUBLIC_URL')}}admin/img/logo1.png" alt="Low Cost Trip" />
                </a>
            </div>
            <ul id="main_menu" class="uk-navbar-nav user_actions">
                <li>
                    <a href="{{ env('ADMIN_URL')}}home" class="md-color-white"><span>Home</span></a>
                </li>
                <li data-uk-dropdown class="uk-hidden-small">
                    <a href="#" class="md-color-white"><span>Lead</span></a>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                            <!--<li><a class="" href="{{ env('ADMIN_URL')}}lead/add">Add New Lead</a></li>-->
                            <li><a class="" href="{{ env('ADMIN_URL')}}lead">Manage Leads</a></li>
                            <!--                            <li><a class="" href="{{ env('ADMIN_URL')}}quotation">Manage Quotation</a></li>
                                                        <li><a class="" href="{{ env('ADMIN_URL')}}lead/confirmed">Manage Reservation</a></li>
                                                        <li><a class="" href="{{ route('lead.payment') }}">Manage Payment</a></li>
                                                        <li><a class="" href="{{ env('ADMIN_URL')}}lead/notes">Manage Follow Up</a></li>
                                                        <li><a class="" href="{{ env('ADMIN_URL')}}lead/cancelled">Cancelled Lead</a></li>
                                                        <li><a class="" href="{{ env('ADMIN_URL')}}lead/confirmed">Confirmed Lead</a></li>
                                                        <li><a class="" href="{{ env('ADMIN_URL')}}lead/status">Status</a></li>-->
                        </ul>
                    </div>
                </li>
                <li data-uk-dropdown class="uk-hidden-small">
                    <a href="#" class="md-color-white"><span>Hotel</span></a>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                            <li><a class="" href="{{ route('hotel.index')}}">Manage Hotels</a></li>
                            <?php /*
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel/room">Manage Rooms</a></li>
                            */ ?>
                            <li><a class="" href="{{ route('hotel.room.booking') }}">Manage Room Booking</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel/room-type">Room Type</a></li>
                            <li><a class="" href="javascript:void(0)">Room Price Type</a></li>
                            <li><a class="" href="javascript:void(0)">Buy Hotel</a></li>
                           <?php /* <li><a class="" href="{{ route('room.price.type') }}">Room Price Type</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel-rooms">Buy Hotel</a></li> */ ?>
                        </ul>
                    </div>
                </li>
                <!--                <li data-uk-dropdown class="uk-hidden-small">
                                    <a href="#" class="md-color-white"><span>Activity</span></a>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                                            <li><a class="" href="{{ route('activities.index')}}">Manage Activity</a></li>
                                            <li><a class="" href="{{ route('activities.mange.booking.index')}}">Manage Activity Booking</a></li>
                                        </ul>
                                    </div>
                                </li>-->
                <li data-uk-dropdown class="uk-hidden-small">
                    <a href="#" class="md-color-white"><span>Tours</span></a>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                            @can('view_tour')
                            <li><a class="" href="{{ env('ADMIN_URL')}}tour">Manage Tour Package</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}domestic-holidays">Manage Domestic Holidays </a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}international-holidays">Manage International Holidays</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}featured-tours">Manage Featured Tour</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}hot-deals-tours">Manage Hot Deals Tour</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}new-arrivals">Manage New Arrival</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}featured-destinations">Manage Featured Destination</a></li>
                            <li><a class="" href="{{ route('book-activities.view') }}">Manage Tour Activities</a></li>
                            @endcan
                            <!--                            <li><a class="" href="{{ route('tour.booking') }}">Manage Tour Booking</a></li>
                                                        <li><a class="" href="{{ env('ADMIN_URL')}}tour/price-type">Price Type</a></li>
                                                        <li><a class="" href="{{ env('ADMIN_URL')}}tour/feature-list">Features</a></li>-->
                        </ul>
                    </div>
                </li>
                <li data-uk-dropdown class="uk-hidden-small">
                    <a href="#" class="md-color-white"><span>Flights</span></a>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                            <li><a class="" href="{{ route('manage.flight.booking') }}">Manage Flight Booking</a></li>
                        </ul>
                    </div>
                </li>
                <li data-uk-dropdown class="uk-hidden-small">
                    <a href="#" class="md-color-white"><span>Web</span></a>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                            @can('view_homebanner')
                            <li><a class="" href="{{ route('website.banner.list') }}">Manage Home Page Banners</a></li>
                            @endcan
                             <li><a class="" href="{{ route('website.tour.section.name.list') }}">Manage Home Page Section Name</a></li>
                             <?php /*
                            
                            @can('view_featured_tour')
                            <li><a class="" href="{{ env('ADMIN_URL')}}website/featured-tours">Manage Featured Tours</a></li>
                            @endcan
                            */ ?>
                            @can('view_domestic_hot_tour')
                            <li><a class="" href="{{ route('website.hottour.index') }}">Domestic Hot Tours</a></li>
                            @endcan
                            @can('view_international_hot_tour')
                            <li><a class="" href="{{ route('website.international.tour') }}">International Tours</a></li>
                            @endcan
                            @can('view_discount_code')
                            <li><a class="" href="{{ route('discount.index') }}">Manage Discount Codes</a></li>
                            @endcan
                            @can('view_album_category')
                            <li><a class="" href="{{ route('website.gallery') }}">Manage Album Category</a></li>
                            @endcan
                            @can('view_manage_album')
                            <li><a class="" href="{{ route('website.manage.album') }}">Manage Album</a></li>
                            @endcan
                            @can('view_video_category')
                            <li><a class="" href="{{ route('video.category.index') }}">Manage Video Category</a></li>
                            @endcan
                            @can('view_video_album')
                            <li><a class="" href="{{ route('video.album.index') }}">Manage Video Album</a></li>
                            @endcan
                            @can('view_testimonial')
                            <li><a class="" href="{{ route('website.testimonial') }}">Manage Testimonials</a></li>
                            @endcan
                            @can('view_web_popup')
                            <li><a claCss="" href="{{ route('website.popup') }}">Manage Pop up</a></li>
                            @endcan
                            @can('view_hotel_promoted')
                            <li><a class="" href="{{ route('website.hotel.promoted') }}">Manage Promoted Hotel</a></li>
                            @endcan
                            @can('view_featured_hotel')
                            <li><a class="" href="{{ route('website.hotel.featured') }}">Manage Featured Hotel</a></li>
                            @endcan
                            <!--                           
                                                        
                                                        <li><a class="" href="{{ route('website.hotel.bestdeal') }}">Manage Best Deal Hotel</a></li>
                                                        <li><a class="" href="{{ route('website.activity.deal') }}">Manage Activity Deals</a></li> -->
                                                        <?php /*
                                                        <li><a class="" href="{{ route('website.feature.destination') }}">Featured Destinations</a></li>
                                                        */ ?>
                                                        <!--
                                                        <li><a class="" href="{{ route('website.flight.deal') }}">Manage Flight Deals</a></li>
                                                        <li><a class="" href="{{ route('website.pushnotification') }}">Manage Push Notifications</a></li>
                                                        <li><a class="" href="{{ route('website.search_keywords') }}">Search Keywords</a></li>-->
                        </ul>
                    </div>
                </li>
                <!--                <li data-uk-dropdown class="uk-hidden-small">
                                    <a href="#" class="md-color-white"><span>Reports</span></a>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                                            <li><a href="#">Lead Reports</a></li>
                                            <li><a href="#">Quotation Report</a></li>
                                            <li><a href="#">Reservation Report</a></li>
                                            <li><a href="#">Hotel (Room) Booking Report</a></li>
                                            <li><a href="#">Activity Booking Report</a></li>
                                            <li><a href="#">Tour Booking Report</a></li>
                                            <li><a href="#">Agent Booking Report</a></li>
                                            <li><a href="#">Supplier Report</a></li>
                                        </ul>
                                    </div>
                                </li>-->
                <li data-uk-dropdown class="uk-hidden-small">
                    <a href="#" class="md-color-white"><span>Masters</span></a>
                    <div class="uk-dropdown uk-dropdown-width-2">
                        <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                            <div class="uk-width-1-2">
                                <ul class="uk-nav uk-nav-dropdown uk-panel">
                                    @can('view_currency')
                                    <li><a href="{{ route('currency.index') }}">Manage Currency</a></li>
                                    @endcan
                                    @can('view_country')
                                    <li><a href="{{ route('country.index') }}">Manage City / State / Country</a></li>
                                    @endcan
                                    @can('view_hotel_type')
                                    <li><a href="{{ route('hotel.category.index') }}">Manage Hotel Type</a></li>
                                    @endcan
                                    @can('view_room_type')
                                    <li><a href="{{ env('ADMIN_URL')}}hotel/room-type">Manage Room Type</a></li>
                                    @endcan
                                    <li><a href="{{ route('hotel-facilities.index') }}">Manage Hotel Facility</a></li>
                                    <li><a href="{{ route('room.facilities') }}">Manage Room Facility</a></li>
                                    @can('view_tour_type')
                                    <li><a href="{{ route('tour.type.index') }}">Manage Tour Category</a></li>
                                    @endcan
                                    @can('view_activity_type')
                                    <li><a href="{{ route('activities.type') }}">Manage Activity Type</a></li>
                                    @endcan
                                    @can('view_lead_status')
                                    <li><a href="{{ env('ADMIN_URL')}}lead/status">Manage Lead Status</a></li>
                                    @endcan
                                </ul>
                            </div>
                            <div class="uk-width-1-2">
                                <ul class="uk-nav uk-nav-dropdown uk-panel">
                                    <!--<li><a href="{{ route('email.template.index') }}">Manage Email Templates</a></li>-->
                                    @can('view_supplier_type')
                                    <li><a href="{{ env('ADMIN_URL')}}supplier/type">Manage Supplier Type</a></li>
                                    @endcan
                                    @can('view_tour_rate_type')
                                    <li><a href="{{ route('rate.type.index')}}">Manage Tour Rate Type</a></li>
                                    @endcan
                                    <!--<li><a href="{{ route('room.price.type')}}">Manage Room Price Type</a></li>-->
                                    @can('view_sector')
                                    <li><a href="{{ route('sector.index')}}">Manage Sector Master</a></li>
                                    @endcan
                                    @can('view_destination')
                                    <li><a href="{{ route('destination.index')}}">Manage Destination Master</a></li>
                                    @endcan
                                    @can('view_certificate')
                                    <li><a href="{{ route('certificates.index')}}">Manage Certificates</a></li>
                                    @endcan
                                    <!--<li><a href="{{ route('tour.price.type')}}">Manage Tour Price Type</a></li>-->
                                    @can('view_tour_feature')
                                    <li><a href="{{ route('tour.features')}}">Manage Tour Features</a></li>
                                    @endcan
                                    @can('view_customer_type')
                                    <li><a href="{{ route('customer.type') }}">Manage Customer Type</a></li>
                                    @endcan
                                    @can('view_tour_package_type')
                                    <li><a href="{{ route('tour.category.index') }}">Manage Tour Package Type</a></li>
                                    @endcan
                                    @can('view_inclusion_exclusion_master')
                                    <li><a href="{{ route('tourinfo.index') }}">Manage Inclusion Exclusion Master</a></li>
                                    @endcan
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li data-uk-dropdown class="uk-hidden-small">
                    <a href="#" class="md-color-white"><span>User</span></a>
                    <div class="uk-dropdown">
                        <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                            <?php /*
                            @can('view_agent')
                            <!-- <li><a href="{{ route('agent.index') }}">Manage Travel Agents</a></li> -->
                            @endcan
                             */ ?>
                            <!--<li><a href="{{ env('ADMIN_URL')}}agent">Manage Agent Subscription</a></li>-->
                            <!--<li><a href="{{ env('ADMIN_URL')}}suppliers">Manage Suppliers</a></li>-->
                            @can('view_user')
                            <li><a href="{{ route('user.index') }}">Manage Admin User</a></li>
                            @endcan
                            @can('view_role')
                            <li><a href="{{ route('role.index') }}">Manage User Groups</a></li>
                            @endcan
                            <li><a href="{{ env('ADMIN_URL')}}customer">Manage Customers</a></li>
                            <?php /*
                            @can('view_subscription')
                            <!-- <li><a href="{{ route('agent.subscription') }}">Manage Subscription Package</a></li> -->
                            @endcan
                            */ ?>
                        </ul>
                    </div>
                </li>
            </ul>    
            <!-- secondary sidebar switch -->
            <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                <span class="sSwitchIcon"></span>
            </a>

            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    <!--<li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                    <li><a href="#" id="main_search_btn" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE8B6;</i></a></li>-->
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">0</span></a>
                        <div class="uk-dropdown uk-dropdown-xlarge">
                            <div class="md-card-content">
                                <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                    <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                                    <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
                                </ul>
                                <ul id="header_alerts" class="uk-switcher uk-margin">
                                    <li>
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <span class="md-user-letters md-bg-cyan">wl</span>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">Aliquid maxime est.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Praesentium sed et cum consectetur veritatis sunt.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_07_tn.png" alt=""/> 
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">Voluptatibus consectetur.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Earum expedita consequatur inventore voluptatibus dolores vel.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <span class="md-user-letters md-bg-light-green">ep</span>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">Harum sed.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Laudantium eum velit iste asperiores quaerat rem non aut qui.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_02_tn.png" alt=""/>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">Corrupti et deleniti.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Recusandae totam quos animi dignissimos exercitationem fuga quia temporibus est eum.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_09_tn.png" alt=""/>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">Quis occaecati.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Accusamus velit incidunt autem quia.</span>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                            <a href="#" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                                        </div>
                                    </li>
                                    <li>
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Eos recusandae.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Consectetur qui aperiam dolorum eligendi atque officia.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Quis iure minima.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Ut voluptatem voluptatum rerum earum.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Suscipit recusandae sed.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Quis dolorem corporis vitae tenetur quia.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Quae sint.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Quos in repellat iste maxime necessitatibus et quis.</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li data-uk-dropdown class="uk-hidden-small">
                        <a href="#" class="user_action_image">
                            <img class="md-user-image" src="assets/img/avatars/avatar_11_tn.png" alt=""/>
                            <span>{{{ ucfirst(Auth::user()->first_name) .' '. ucfirst(Auth::user()->last_name) }}}</span>
                        </a>
                        <div class="uk-dropdown">
                            <ul class="uk-nav uk-nav-dropdown uk-nav-multilevel">
                                <li><a href="#">My profile</a></li>
                                <li><a href="#">Settings</a></li>
                                <li><a href="<?= env('ADMIN_URL') ?>logout">Log Out</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

</header><!-- main header end -->





