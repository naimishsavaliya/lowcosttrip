<div id="sidebar" class="nav-collapse collapse" style="display: none;">

    <div class="sidebar-toggler hidden-phone"></div>   

    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    <div class="navbar-inverse">
        <form class="navbar-search visible-phone">
            <input type="text" class="search-query" placeholder="Search" />
        </form>
    </div>
    <!-- END RESPONSIVE QUICK SEARCH FORM -->
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="sidebar-menu">
        <li class="has-sub">
            <a href="javascript:;" class="">
                <span class="icon-box"> <i class="icon-dashboard"></i></span> Dashboard
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><a class="" href="index.html">Dashboard 1</a></li>
                <li><a class="" href="index_2.html">Dashboard 2</a></li>

            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;" class="">
                <span class="icon-box"> <i class="icon-book"></i></span> Manage Book
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><a class="" href="{{ env('ADMIN_URL')}}category">View Category</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}category/add">Add Category</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}level">View Level</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}level/add">Add Level</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}course">View Course</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}course/add">Add Course</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}lesson">View Lessons</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}lesson/add">Add Lesson</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}assignment">View Assignments</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}assignment/add">Add Assignment</a></li>

            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;" class="">
                <span class="icon-box"><i class="icon-home"></i></span> Batch Master
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><a class="" href="{{ env('ADMIN_URL')}}batch">View Batches</a></li>
                <li><a class="" href="{{ env('ADMIN_URL')}}batch/add">Add Batch</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;" class="">
                <span class="icon-box"><i class="icon-tasks"></i></span> Form Stuff
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><a class="" href="form_layout.html">Form Layouts</a></li>
                <li><a class="" href="form_component.html">Form Components</a></li>
                <li><a class="" href="form_wizard.html">Form Wizard</a></li>
                <li><a class="" href="form_validation.html">Form Validation</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;" class="">
                <span class="icon-box"><i class="icon-fire"></i></span> Icons
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><a class="" href="font_awesome.html">Font Awesome</a></li>
                <li><a class="" href="glyphicons.html">Glyphicons</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;" class="">
                <span class="icon-box"><i class="icon-map-marker"></i></span> Maps
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><a class="" href="maps_google.html"> Google Maps</a></li>
                <li><a class="" href="maps_vector.html"> Vector Maps</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;" class="">
                <span class="icon-box"><i class="icon-file-alt"></i></span> Sample Pages
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><a class="" href="profile.html">Profile</a></li>
                <li class="active"><a class="" href="blank.html">Blank Page</a></li>
                <li><a class="" href="sidebar_closed.html">Sidebar Closed Page</a></li>
                <li><a class="" href="pricing_tables.html">Pricing Tables</a></li>
                <li><a class="" href="faq.html">FAQ</a></li>
                <li><a class="" href="errors.html">Errors</a></li>
            </ul>
        </li>
        <li><a class="" href="login.html"><span class="icon-box"><i class="icon-user"></i></span> Login Page</a></li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->  