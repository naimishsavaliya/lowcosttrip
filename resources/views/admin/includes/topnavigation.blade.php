<div id="header" class="navbar navbar-inverse navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div class="container-fluid">
            <!-- BEGIN LOGO -->
            <a class="brand" href="{{ env('ADMIN_URL')}}home">
                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/logo.png" alt="Admin Lab" />
            </a>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="arrow"></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <div id="top_menu" class="nav notify-row" style="color: #868686; ">
                <!-- BEGIN NOTIFICATION -->
                <ul class="nav top-menu">
                    <li class="r-b-m">
                        <a href="{{ env('ADMIN_URL')}}home">
                            Dashboard
                        </a>
                    </li>
                    <li class="dropdown r-b-m" >
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Leads
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}lead/add">Add New Lead</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}lead">All Leads</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}lead/cancelled">Cancelled Lead</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}lead/confirmed">Confirmed Lead</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}lead/notes">Notes</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}lead/status">Status</a></li>
                        </ul>
                    </li>
                    <li class="dropdown r-b-m">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Masters
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}sector">Sector Master</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}destination">Destination Master</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}activities/type">Activities Type</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}activities">Activities</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}activities/inventory">Activities Inventory</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel-facilities">Hotel Facilities</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}certificate">Certificates</a></li>
                            <li><a cass="" href="{{ env('ADMIN_URL')}}discount">Discount</a></li>
                        </ul>
                    </li>
                    <li class="dropdown r-b-m">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Suppliers
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}suppliers">All Suppliers</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}inventory">Hotel Inventory</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}room/inventory">Room Inventory</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}supplier/type">Supplier Category</a></li>
                        </ul>
                    </li>
                    <li class="dropdown r-b-m">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Hotels
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel">View Hotels</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel/room">Hotel Rooms</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel/room-type">Room Type</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel/room-price-type">Room Price Type</a></li>
                        </ul>
                    </li>
                    <li class="dropdown r-b-m">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Tours
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}tour">View Tours</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}tour/price-type">Price Type</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}tour/feature-list">Features</a></li>
                        </ul>
                    </li>
                    <li class="dropdown r-b-m">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            Agent
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}agent">Agent List</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}agent/add">Create New Agent</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}agent/subscription">Agent Subscription List</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}agent/subscription_add">Create New Agent Subscription</a></li>
                        </ul>
                    </li>
                    <li class="dropdown r-b-m">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            Web
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}website/homebanner">Home Banners</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}website/featured-tours">Featured Tours</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}website/featured-destinations">Featured Destinations</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}website/hot-tours">Hot Tours</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}website/international-tours">International Tours</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}testimonial">Client Testimonials</a></li>
                            <li><a class="" href="{{ env('ADMIN_URL')}}video">Videos</a></li>
                        </ul>
                    </li>


                    <li class="dropdown r-b-m">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            Buy
                        </a>
                        <ul class="dropdown-menu extended">
                            <li><a class="" href="{{ env('ADMIN_URL')}}hotel-rooms">Buy Hotel</a></li>
                        </ul>
                    </li>

                    <li class="dropdown r-b-m">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            Customers
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}customer">All Customers</a></li>
                        </ul>
                    </li>

                    <li class="dropdown r-b-m">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            User
                        </a>
                        <ul class="dropdown-menu extended ">
                            <li><a class="" href="{{ env('ADMIN_URL')}}user">Users</a></li>
                            @can('view_role')
                            <li><a class="" href="{{ env('ADMIN_URL')}}role">Roles</a></li>
                            @endcan
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- END  NOTIFICATION -->
            <div class="top-nav">
                <ul class="nav pull-right top-menu notify-row" >
                    <!-- BEGIN SUPPORT -->

                    <li class="dropdown mtop5" id="header_inbox_bar">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-envelope-alt"></i>
                            <span class="badge badge-important">5</span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <li>
                                <p>You have 5 new messages</p>
                            </li>
                            <li>
                                <a href="{{ env('ADMIN_URL')}}user/message">
                                    <span class="photo"><img src="{{ env('APP_PUBLIC_URL')}}admin/img/avatar-mini.png" alt="avatar" /></span>
                                    <span class="subject">
                                        <span class="from">Dulal Khan</span>
                                        <span class="time">Just now</span>
                                    </span>
                                    <span class="message">
                                        Hello, this is an example messages please check
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown mtop5" id="header_notification_bar">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <i class="icon-bell-alt"></i>
                            <span class="badge badge-warning">7</span>
                        </a>
                        <ul class="dropdown-menu extended notification">
                            <li>
                                <p>You have 7 new notifications</p>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-important"><i class="icon-bolt"></i></span>
                                    Server #3 overloaded.
                                    <span class="small italic">34 mins</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-warning"><i class="icon-bell"></i></span>
                                    Server #10 not respoding.
                                    <span class="small italic">1 Hours</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-important"><i class="icon-bolt"></i></span>
                                    Database overloaded 24%.
                                    <span class="small italic">4 hrs</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-success"><i class="icon-plus"></i></span>
                                    New user registered.
                                    <span class="small italic">Just now</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                    Application error.
                                    <span class="small italic">10 mins</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">See all notifications</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ env('APP_PUBLIC_URL')}}admin/img/avatar1_small.jpg" alt="">
                            <span class="username">{{Auth::user()->name}}</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="icon-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
                            <li><a href="#"><i class="icon-calendar"></i> Calendar</a></li>
                            <li class="divider"></li>
                            <li><a href="<?= env('ADMIN_URL') ?>logout"><i class="icon-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>