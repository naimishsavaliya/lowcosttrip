@if(isset($breadcrumb))
<ul id="breadcrumbs" style="display:inline-block;">
    <?php /*<li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li> */ ?>
    @foreach($breadcrumb as $bread)
    @if(isset($bread['link']))
    <li><a href="{{ $bread['link'] }}">{{ $bread['title'] }}</i></a></li>
    @else
    <li><span>{{ $bread['title'] }}</span></li>
    @endif
    @endforeach
</ul>
@endif
