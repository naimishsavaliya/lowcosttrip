@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>
    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('new-arrivals.update') }}" name="new_arrival_frm_edit" id="new_arrival_frm_edit" class="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id" value="{{ isset($new_arrival_data->id) ? $new_arrival_data->id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Title <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="title" id="title" value="{{ isset($new_arrival_data->title) ? $new_arrival_data->title : '' }}"/>
                                        <small class="text-danger">{{ $errors->first('title') }}</small>
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <select class="md-input" name="tour_id" id="tour_id">
                                            <option value="">Choose Tour</option>
                                            @if(isset($tour))
                                                @foreach($tour as $tour_key => $tour_val)
                                                <option value="{{$tour_val->id}}" {{ (isset($new_arrival_data->tour_id) && $new_arrival_data->tour_id == $tour_val->id) ? 'selected' : '' }}>{{ (isset($tour_val->tour_name) && $tour_val->tour_name != '') ?  $tour_val->tour_name: '' }}&nbsp;({{ (isset($tour_val->tour_code) && $tour_val->tour_code != '') ?  $tour_val->tour_code: '' }})</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <small class="text-danger">{{ $errors->first('tour_id') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>Start Date <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="start_date" id="start_date" value="{{ isset($new_arrival_data->start_date) ? date('d F Y', strtotime($new_arrival_data->start_date)) : '' }}" data-uk-datepicker="{format:'DD MMMM YYYY'}" autocomplete="off" />
                                        <small class="text-danger">{{ $errors->first('start_date') }}</small>
                                    </div>
                                    
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <label>End Date <span class="required-lbl">*</span></label>
                                        <input type="text" class="md-input" name="end_date" id="end_date" value="{{ isset($new_arrival_data->end_date) ? date('d F Y', strtotime($new_arrival_data->end_date)) : '' }}" data-uk-datepicker="{format:'DD MMMM YYYY'}" autocomplete="off" />
                                        <small class="text-danger">{{ $errors->first('end_date') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-l-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="status_1" data-md-icheck {{ (isset($new_arrival_data->status) && $new_arrival_data->status == 1) ? 'checked' : ''  }} {{ (!isset($users[0])) ? 'checked' : '' }} />
                                                       <label for="status_1" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="status_0" data-md-icheck {{ (isset($new_arrival_data->status) && $new_arrival_data->status == 0) ? 'checked' : ''  }} />
                                                       <label for="status_0" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('status') }}</small>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{route('tour.new-arrivals')}}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
    $('select#tour_id').on('change', function() {  // when the value changes
        $(this).valid(); // trigger validation on this element
    });
    $("form#new_arrival_frm_edit").validate({
        rules: {
            title: "required",
            tour_id: "required",
            status: "required"
        },
        messages: {
            title: "Please enter title",
            tour_id: "Please select tour",
            status: "Please select status"
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element);
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("select#tour_id").select2();
});
</script>
@endsection