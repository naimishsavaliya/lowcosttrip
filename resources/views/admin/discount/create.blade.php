@extends('layouts.theme')
@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('discount.store')}}" name="discount_frm" id="discount_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($discount->desId) ? $discount->desId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <h3 class="heading_a"><span class="sub-heading">Discount For</span></h3>
                                <div class="uk-grid m-t-10 md-input-wrapper" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <select id="discount_for" name="discount_for[]" class="uk-width-1-1" multiple data-md-select2>
                                            <?php
                                            if (count($discount_for) > 0) {
                                                $discount_for_arr = array();
                                                if (isset($discount->discount_for)) {
                                                    $discount_for_arr = explode(',', $discount->discount_for);
                                                }
                                                foreach ($discount_for as $des_key => $dis_for) {
                                                    ?>
                                                    <option value="{{ $des_key }}" {{ (isset($discount->discount_for) && in_array($des_key, $discount_for_arr)) ? "selected" : "" }}>{{ $dis_for }}</option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        @if($errors->has('discount_for'))<small class="text-danger">{{ $errors->first('discount_for') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label>Discount Code</label>
                                        <input type="text" class="md-input" name="discount_code" autocomplete="off" value="{{ isset($discount->discount_code) ? $discount->discount_code : old('discount_code') }}" />
                                        @if($errors->has('discount_code'))<small class="text-danger">{{ $errors->first('discount_code') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-4 p-r-50">
                                        <label>Maximum Discount Amount</label>
                                        <input type="text" class="md-input" name="max_discount_amount" autocomplete="off" value="{{ isset($discount->max_discount_amount) ? $discount->max_discount_amount : old('max_discount_amount') }}" />
                                    </div>
                                    <div class="uk-width-medium-1-4">
                                        <label>Minimum Cart Value</label>
                                        <input type="text" class="md-input" name="min_cart_amount" autocomplete="off" value="{{ isset($discount->min_cart_amount) ? $discount->min_cart_amount : old('min_cart_amount') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="uk-width-medium-1-2 uk-row-first">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 uk-row-first">
                                        <label class="control-label">Type</label>
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio"  id="discount_type_0" data-md-icheck name="type" value="1" {{ (isset($discount->type) && $discount->type == 1) ? 'checked' : ''  }} {{ (!isset($discount)) ? 'checked' : '' }} />
                                                <label for="discount_type_0" class="inline-label">Amount</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" id="discount_type_1" data-md-icheck name="type" value="2" {{ (isset($discount->type) && $discount->type == 2) ? 'checked' : ''  }} />
                                                <label for="discount_type_1" class="inline-label">Percentage</label>
                                            </span>
                                        </div>
                                        @if($errors->has('type'))<small class="text-danger">{{ $errors->first('type') }}</small>@endif
                                    </div>
                                    <div class="uk-width-medium-1-2 p-l-5">
                                        <label>Amount/Percentage</label>
                                        <input type="text" class="md-input" name="amount" value="{{ isset($discount->amount) ? $discount->amount : old('amount') }}" />
                                        @if($errors->has('amount'))<small class="text-danger">{{ $errors->first('amount') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-4 uk-width-1-1 p-r-50">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_start">Start Date</label>
                                        <input class="md-input" type="text" id="uk_dp_start" name="start_date" autocomplete="off" value="{{ isset($discount->start_date) ? date('d M Y', strtotime($discount->start_date)) : '' }}">
                                    </div>
                                    @if($errors->has('start_date'))<small class="text-danger">{{ $errors->first('start_date') }}</small>@endif
                                </div>
                                <div class="uk-width-large-1-4 uk-width-medium-1-1">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon p-0"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_end">End Date</label>
                                        <input class="md-input" type="text" id="uk_dp_end" name="end_date" autocomplete="off" value="{{ isset($discount->end_date) ? date('d M Y', strtotime($discount->end_date)) : '' }}">
                                    </div>
                                    @if($errors->has('end_date'))<small class="text-danger">{{ $errors->first('end_date') }}</small>@endif
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2">
                                        <label>Valid On</label>
                                        <textarea cols="30" rows="4" class="md-input" name="valid_on">{{ isset($discount->valid_on) ? $discount->valid_on : old('valid_on') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($discount->status) && $discount->status == 1) ? 'checked' : ''  }} {{ (!isset($discount)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($discount->status) && $discount->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('discount.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection


@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_advanced.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
// It has the name attribute "discount_frm"
    $("form#discount_frm").validate({
// Specify validation rules
        rules: {
            "discount_for[]": "required",
            discount_code: "required",
            type: "required",
            amount: "required",
            start_date: "required",
            end_date: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            "discount_for[]": "Please select discount for",
            discount_code: "Please enter discount code",
            type: "Please select discount type",
            amount: "Please enter amount/percentage",
            start_date: "Please select start date",
            end_date: "Please select end date",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else if (element.is('select')) {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});

</script>
@endsection


