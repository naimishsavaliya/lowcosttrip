@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Manage Hotel Room Booking</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}hotel/lead_room_buy/1" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <h3 class="heading_b uk-margin-bottom">Hotel Room Booking for #123456 (Bhavik Shah)</h3>

            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th>Booking Date</th>
                        <th>Occupiency Date</th>
                        <th>Room type</th>
                        <th>No of Rooms</th>
                        <th>Adults</th>
                        <th>Childrens</th>
                        <th>Infants</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Booking Date" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Occupiency Date" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Room type" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="No of Rooms" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Adults" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Childrens" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Infants" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td>5 Mar 2018</td>
                            <td>10 Nov 2018</td>
                            <td>lacus.</td>
                            <td>21</td>
                            <td>13</td>
                            <td>10</td>
                            <td>14</td>
                            <td>
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                    <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav uk-nav-dropdown">
                                            <li><a href=""> Cancel Booking</a></li>
                                            <li><a href=""> Print Voucher</a></li>
                                            <li><a href=""> Email Voucher</a></li>
                                            <li><a href=""> Edit Booking</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection

