@extends('layouts.theme')
@section('content')

<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><span>Hotel XYZ</span></li>
            <li><span>View/Update Inventory</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="">

                <style>
                    table.avltable td{width: 35px !important; border-left: solid 1px #e0e0e0; border-top: solid 1px #e0e0e0;}
                    table.avltable td input{width: 20px !important; border: none; color: #b3aaaa;}
                    table.avltable td.dateselected {background-color: #29b729; color: #FFF;}
                    tr:hover {
                        background-color: #ffa;
                    }

                    td, th {
                        position: relative;
                    }
                    td:hover::after,
                    th:hover::after {
                        content: "";
                        position: absolute;
                        background-color: #ffa;
                        left: 0;
                        top: -5000px;
                        height: 10000px;
                        width: 100%;
                        z-index: -1;
                    }
                </style>
                <div class="uk-margin-medium-bottom">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1_content'}" id="tabs_1">
                                <li class="uk-active"><a href="#">All</a></li>
                                <li><a href="#">Room 1</a></li>
                                <li class="named_tab"><a href="#">Room 2</a></li>
                                <li class="named_tab"><a href="#">Room 3</a></li>
                                <li class="named_tab"><a href="#">History</a></li>
                                <li class="uk-disabled" style="float:right;"><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" data-uk-modal="{target:'#update_inventory'}" href="" style="color: #FFF; padding-bottom: 5px !important;">Update Inventory</a></li>
                            </ul>
                            <ul id="tabs_1_content" class="uk-switcher uk-margin">
                                <li>
                                    <?php
                                    $current_year = date('Y');
                                    $yearPass = $current_year;
                                    $dateId = 1;
                                    $room = array();
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 1, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 2, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 3, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 4, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 5, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 6, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 7, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 8, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 9, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 10, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 11, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 12, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);

                                    if ($yearPass < $current_year) {
                                        $disable = "disabled=disabled";
                                    }

                                    function GetMonthMaxDay($year, $month) {
                                        if (empty($month))
                                            $month = date('m');
                                        if (empty($year))
                                            $year = date('Y');
                                        $result = strtotime("{$year}-{$month}-01");
                                        $result = strtotime('-1 second', strtotime('+1 month', $result));
                                        return date('d', $result);
                                    }

                                    $nl = "\n";

                                    $lang['weeks'][0] = 'Su';
                                    $lang['weeks'][1] = 'Mo';
                                    $lang['weeks'][2] = 'Tu';
                                    $lang['weeks'][3] = 'We';
                                    $lang['weeks'][4] = 'Th';
                                    $lang['weeks'][5] = 'Fr';
                                    $lang['weeks'][6] = 'Sa';

                                    $lang['months'][1] = 'January';
                                    $lang['months'][2] = 'February';
                                    $lang['months'][3] = 'March';
                                    $lang['months'][4] = 'April';
                                    $lang['months'][5] = 'May';
                                    $lang['months'][6] = 'June';
                                    $lang['months'][7] = 'July';
                                    $lang['months'][8] = 'August';
                                    $lang['months'][9] = 'September';
                                    $lang['months'][10] = 'October';
                                    $lang['months'][11] = 'November';
                                    $lang['months'][12] = 'December';


                                    $year = $yearPass;
                                    $ids_list = '';
                                    $max_days = 0;
                                    $output = '';
                                    $output_week_days = '';
                                    $current_month = date('m');

                                    $selected_year = $year;

                                    $room_count = 1;

                                    $output .= '<form id="basicvalidations" action="/Managehotelrooms/UpdateAvailability" method="post">';
                                    $output .= '<input type="hidden" id="task" name="task" value="update">';
                                    $output .= '<input type="hidden" id="changeValue" name="changeValue" value="">';
                                    $output .= '<input type="hidden" id="comment" name="comment" value="">';
                                    $output .= '<input type="hidden" id="year" name="year" value="' . $yearPass . '">';
                                    $output .= '<div class="col-md-12">'
                                            . '<div class="portlet light portlet-fit portlet-form ">'
                                            . '<div class="portlet-body">'
                                            . '<div class="form-body">';
                                    $output .= '<table cellpadding="0" cellspacing="0" border="0" class="avltable">';
//                                        $output .= '<tr>';
//                                        $output .= '<td align="left" colspan="27">
//					</td>
//					
//					
//					<td align="right" colspan="0">
//						
//					</td>';
//                                        $output .= '</tr>';
//                                        $output .= '<tr><td colspan="39">&nbsp;</td></tr>';

                                    $count = 0;
                                    $week_day = date('w', mktime('0', '0', '0', '1', '1', $selected_year));
// fill empty cells from the beginning of month line
                                    while ($count < $week_day) {
                                        $td_class = (($count == 0 || $count == 6) ? 'day_td_w' : ''); // 0 - 'Sun', 6 - 'Sat'
                                        $output_week_days .= '<td class="' . $td_class . '">' . $lang['weeks'][$count] . '</td>';
                                        $count++;
                                    }
// fill cells at the middle
                                    for ($day = 1; $day <= 31; $day ++) {
                                        $week_day = date('w', mktime('0', '0', '0', '1', $day, $selected_year));
                                        $td_class = (($week_day == 0 || $week_day == 6) ? 'day_td_w' : ''); // 0 - 'Sun', 6 - 'Sat'
                                        $output_week_days .= '<td  class="' . $td_class . '">' . $lang['weeks'][$week_day] . '</td>';
                                    }
                                    $max_days = $count + 31;
// fill empty cells at the end of month line
                                    if ($max_days < 37) {
                                        $count = 0;
                                        while ($count < (37 - $max_days)) {
                                            $week_day++;
                                            $count++;
                                            $week_day_mod = $week_day % 7;
                                            $td_class = (($week_day_mod == 0 || $week_day_mod == 6) ? 'day_td_w' : ''); // 0 - 'Sun', 6 - 'Sat'
                                            $output_week_days .= '<td class="' . $td_class . '">' . $lang['weeks'][$week_day_mod] . '</td>';
                                        }
                                        $max_days += $count;
                                    }
                                    $nextY = $current_year + 1;
                                    $outputOPtions = "";
                                    for ($yer = 2015; $yer <= $nextY; $yer++) {
                                        $outputOPtions .= '<option value="' . $yer . '" ' . (($yer == $yearPass) ? 'selected="selected"' : '') . '>' . $yer . '</option>';
                                    }
// draw week days
                                    $output .= '<tr style="text-align:center;background-color:#cccccc;">';
                                    $output .= '<td style="text-align:left;background-color:#ffffff; border:none;">';
                                    $output .= '<select name="selYear" onchange="loadData(\'/Managehotelrooms/roomavailability/\'+this.value)">';
                                    $output .= $outputOPtions;
                                    //$output .= '<option value="' . $nextY . '" ' . (($year == 'next') ? 'selected="selected"' : '') . '>' . ($current_year + 1) . '</option>';
                                    $output .= '</select>';
                                    $output .= '</td>';
//                                        $output .= '<td align="center" style="padding:0px 4px;background-color:#ffffff;"><img src="/app/webroot/images/check_all.gif" alt="" /></td>';
                                    $output .= $output_week_days;
                                    $output .= '</tr>';


                                    for ($i = 0; $i < 12; $i++) {
                                        $selected_month = $room[$i]['m'];
                                        if ($selected_month == $current_month)
                                            $tr_class = 'm_current';
                                        else
                                            $tr_class = (($i % 2 == 0) ? 'm_odd' : 'm_even');

                                        $output .= '<tr align="center" class="' . $tr_class . '">';
                                        $output .= '<td align="left">&nbsp;<b>' . $lang['months'][$selected_month] . '</b></td>';
//                                            $output .= '<td><input type="checkbox" class="form_checkbox" onclick="toggleAvailability(this.checked,\'' . $room[$i]['id'] . '\',\'' . $room_count . '\')" /></td>';

                                        $max_day = GetMonthMaxDay($selected_year, $selected_month);

                                        // fill empty cells from the beginning of month line
                                        $count = date('w', mktime('0', '0', '0', $selected_month, 1, $selected_year));
                                        $max_days -= $count; /* subtract days that were missed from the beginning of the month */
                                        while ($count--)
                                            $output .= '<td></td>';
                                        // fill cells at the middle
                                        for ($day = 1; $day <= $max_day; $day ++) {
                                            if ($room[$i]['d' . $day] >= $room_count) {
                                                $day_color = 'dc_all';
                                            } else if ($room[$i]['d' . $day] > 0 && $room[$i]['d' . $day] < $room_count) {
                                                $day_color = 'dc_part';
                                            } else {
                                                $day_color = 'dc_none';
                                            }
                                            $week_day = date('w', mktime('0', '0', '0', $selected_month, $day, $selected_year));
                                            $dateBook = date('Y-m-d', mktime('0', '0', '0', $selected_month, $day, $selected_year));
                                            $time = mktime('0', '0', '0', $selected_month, $day, $selected_year);

                                            $noOfBook = array();


                                            $title = '';
                                            if (!empty($noOfBook)) {

                                                $totalRoom = 0;
                                                $title = '';
                                                foreach ($noOfBook as $noOfBookA) {

                                                    $customer_name = $this->TbHotelbooking->getBookingData($noOfBookA['TbHotelsroomsrecord']['customer_hotel_id']);
                                                    $roomBV = $noOfBookA[0]['roomB'];
                                                    if ($roomBV < 0) {
                                                        $roomN = ($roomBV) * (-1);
                                                    } else {
                                                        $roomN = 0;
                                                    }
                                                    if (isset($noOfBookA['TbHotelsroomsrecord']['tour_departure']) && !empty($noOfBookA['TbHotelsroomsrecord']['tour_departure'])) {
                                                        $tourInfo = $this->TbTourdep->getTourInfo($noOfBookA['TbHotelsroomsrecord']['tour_departure']);
// if(strtotime($dateBook) ==  strtotime('2016-12-29')){
//     echo $noOfBookA['TbHotelsroomsrecord']['tour_departure'];
//                            echo "<pre>";
//                            print_r($noOfBook);
//                            //print_r($tourInfo);
//                            echo "</pre>";
//                            }
                                                        $deptDate = date("d-m-Y", strtotime($tourInfo[0]['TbTourdeparturedatemaster']['start_date']));
                                                        $tourCode = $tourInfo[0]['tourmaster']['tour_code'];
                                                        if ($roomN != 0 && strtotime($deptDate) <= strtotime($dateBook)) {
                                                            if (isset($tourCode) && !empty($tourCode)) {
                                                                $title .= "Tour Code: $tourCode, Dept Date: $deptDate,Room booked:$roomN &#13;";
                                                            }
                                                        }
                                                    } elseif ($noOfBookA['TbHotelsroomsrecord']['isFIT'] == 'Y' && $noOfBookA['TbHotelsroomsrecord']['revert_booking'] == 'N') {
                                                        $title .= "FIT -Room Booked:" . $roomN . ",FIT-Name: " . $customer_name['TbHotelbooking']['vouchername'] . " &#13;";
                                                    } else {
                                                        $title .= "No room booked yet";
                                                    }
                                                }
                                            } else {

                                                $title .= "No room booked yet";
                                            }

                                            $td_class = (($week_day == 0 || $week_day == 6) ? 'day_td_w' : 'day_td'); // 0 - 'Sun', 6 - 'Sat'
                                            $output .= '<td dateid="' . $dateId . '" time="' . $time . '"  class="' . $td_class . '"><label class="l_day">' . $day . '</label><br><input title="' . $title . '" class="day_a ' . $day_color . '" maxlength="3" name="aval_' . $room[$i]['id'] . '_' . $day . '" id="aval_' . $room[$i]['id'] . '_' . $day . '" value="' . $room[$i]['d' . $day] . '" /></td>';
                                            $dateId++;
                                        }
                                        // fill empty cells at the end of the month line
                                        while ($day <= $max_days) {
                                            $output .= '<td></td>';
                                            $day++;
                                        }
                                        $output .= '</tr>';
                                        if ($ids_list != '')
                                            $ids_list .= ',' . $room[$i]['id'];
                                        else
                                            $ids_list = $room[$i]['id'];
                                    }

//                                    $output .= '<tr><td colspan="39">&nbsp;</td></tr>';
//                                    $output .= '<tr><td align="right" colspan="39"><button type="submit"  class="btn btn-success">Save</button></td></tr>';
//                                    $output .= '<tr><td colspan="39" style="float: left;"><b>Legend:</b> </td></tr>';
//                                    $output .= '<tr><td colspan="39" nowrap="nowrap" height="5px"></td></tr>';
//                                    $output .= '<tr><td colspan="39" style="float: left;"><div class="dc_all" style="height:15px;float:left;margin:1px;"></div> &nbsp;- Available</td></tr>';
//                                    $output .= '<tr><td colspan="39" style="float: left;"><div class="dc_part" style="height:15px;float:left;margin:1px;"></div> &nbsp;- Partially Available</td></tr>';
//                                    $output .= '<tr><td colspan="39" style="float: left;"><div class="dc_none" style="height:15px;float:left;margin:1px;"></div> &nbsp;- Not Available</td></tr>';
                                    $output .= '</table>';
                                    $output .= '</table>';
                                    $output .= '<input type="hidden" name="ids_list" value="' . $ids_list . '">';
                                    $output .= '</div></div></div></div>';
                                    $output .= '</form>';

                                    echo $output;
//}
                                    ?>

                                </li>
                                <li>Content 2</li>
                                <li>Content 3</li>
                                <li>Content 4</li>
                                <li >

                                    <div class="uk-grid">
                                        <div class="uk-width-1-1">
                                            <div class="uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-large-1-6 uk-width-1-1" style="padding-top: 10px;">
                                                    <h3>Filter History</h3>
                                                </div>
                                                <div class="uk-width-large-2-10 uk-width-1-1" style="padding-left: 0;">
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                                        <label for="uk_dp_start">Start Date</label>
                                                        <input class="md-input" type="text" id="uk_dp_start">
                                                    </div>
                                                </div>
                                                <div class="uk-width-large-2-10 uk-width-medium-1-1">
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                                        <label for="uk_dp_end">End Date</label>
                                                        <input class="md-input" type="text" id="uk_dp_end">
                                                    </div>
                                                </div>
                                                <div class="uk-width-large-1-10 uk-width-1-1">
                                                    <a class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light" href="javascript:void(0)">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear clearfix"></div> <br/>

                                    <div class="uk-width-large-1-2 m-t-15" style="margin-left: 25%;">
                                        <div class="timeline timeline_small uk-margin-bottom">
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_success"><i class="material-icons">add</i></div>
                                                <div class="timeline_date">
                                                    09 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">Deluxe room inventory increased to 10 for 05 Jan 2019 to 08 Feb 2019 by user name.</div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_danger"><i class="material-icons">remove</i></div>
                                                <div class="timeline_date">
                                                    15 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">Deluxe room inventory decrease to 10 for 05 Jan 2019 to 08 Feb 2019 by user name.<a href="#"><strong>Velit est quisquam dolorem officiis quis.</strong></a></div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_success"><i class="material-icons">done</i></div>
                                                <div class="timeline_date">
                                                    09 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">New booking received for Super Deluxe room for 3 rooms for the 01 Jan 2019 in inquiry #LCT123. </div>
                                            </div>
                                            <div class="timeline_item">
                                                <div class="timeline_icon timeline_icon_danger"><i class="material-icons">clear</i></div>
                                                <div class="timeline_date">
                                                    15 <span>Jan</span>
                                                </div>
                                                <div class="timeline_content">3 Deluxe rooms canceled for the date 01 Jan 2019 in inquiry #LCT123.</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>

<div class="uk-modal uk-modal-card-fullscreen" id="update_inventory">
    <div class="uk-modal-dialog uk-modal-dialog-blank">
        <div class="md-card uk-height-viewport">
            <div class="md-card-toolbar">
                <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                <h3 class="md-card-toolbar-heading-text">
                    Update Inventory & Rate
                </h3>
            </div>
            <div class="md-card-content">
                <h2>Inventory</h2>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper md-input-filled"><label>Start Date</label><input type="text" value="01-07-2018" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper md-input-filled"><label>End Date</label><input type="text" value="25-07-2018" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper"><label>New Inventory</label><input type="text" value="" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-1 ">
                            <div class="md-input-wrapper md-input-filled"><label>Remark</label>
                                <textarea class="md-input no_autosize" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <h2>Room Rate</h2>
                <hr class="form_hr">

                <div data-dynamic-fields="d_field_wizard" class="uk-grid" data-uk-grid-margin></div>
                <hr class="form_hr">
                <div class="uk-grid uk-margin-medium-top uk-text-right">
                    <div class="uk-width-1-1">
                    <button type="button" class="md-btn md-btn-danger uk-modal-close">Cancel</button>
                        <button type="submit" class="md-btn md-btn-primary uk-modal-close">Save</button>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                </div>
            </div>

        </div>
    </div>
</div>
<div class="uk-modal" id="update_inventory1">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Update Inventory</h3>
        </div>
        <div class="uk-modal-body">
            <div class="uk-width-medium-1-1 uk-row-first">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper md-input-filled"><label>Start Date</label><input type="text" value="01-07-2018" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper md-input-filled"><label>End Date</label><input type="text" value="25-07-2018" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper"><label>New Inventory</label><input type="text" value="" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-1 ">
                            <div class="md-input-wrapper md-input-filled"><label>Remark</label>
                                <textarea class="md-input no_autosize" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Update</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        var selectStartDate = "";
        var startDateId = "";
        var endDateId = "";
        $("table.avltable td").click(function () {
            if ($("table.avltable td").hasClass("endDate")) {
                $("table.avltable td").removeClass("startDate");
                $("table.avltable td").removeClass("endDate");
                $("table.avltable td").removeClass("dateselected");
            }
            if ($("table.avltable td").hasClass("startDate")) {
                var selectedDateId = $(this).attr('dateid');
                if (selectedDateId > startDateId) {
                    for (i = startDateId; i < selectedDateId; i++) {
                        $('td[dateid="' + i + '"]').addClass("dateselected");
                    }
                    $(this).addClass("endDate dateselected");
                }
//                alert("end");
            } else {
                $(this).addClass("startDate dateselected");
                startDateId = $(this).attr('dateid');
//                alert("start");

            }
        });
    });
</script>
<script id="d_field_wizard" type="text/x-handlebars-template">
    <div class="uk-width-medium-1-1 parsley-row form_section">
    <div class="uk-input-group uk-grid" style="width:100%">
    <div class="uk-width-1-1">
    <div class="">
    <div class="uk-grid" data-uk-grid-margin="">
    <div class="uk-width-medium-1-4 uk-width-1-1 uk-row-first">
    <label for="invoice_dp">Date From</label>
    <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="invoice_dp">Date To</label>
    <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="discount">Customer Discount %</label>
    <input class="md-input" type="text" id="discount" value="">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="discount">Agent Discount %</label>
    <input class="md-input" type="text" id="discount" value="">
    </div>
    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div>
    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div> <br/>


    <div class="uk-width-medium-1-1">
    <table class="uk-table ">
    <tbody>
    <tr>
    <td style="width: 15%;">
    <div class="md-input-wrapper">
    <label> </label>
    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
    <option value="1">Room Only</option>
    <option value="2">Breakfast</option>
    <option value="3">Breakfast + 1 Meal</option>
    <option value="4">Breakfast + 2 Meal</option>
    <option value="5">All Including</option>
    </select>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Standard</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Su</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Mo</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Tu</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>We</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Th</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Fr</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Sa</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    </tr>

    </tbody>
    </table>
    </div>

    <hr/>


    <div class="uk-width-medium-1-10">
    <span class="uk-input-group-addon">
    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
    </span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</script>

@endsection
@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>
@endsection

