@extends('layouts.theme')
@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('room.price.type.store')}}" name="roomprice_frm" id="roomprice_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($roomprice->id) ? $roomprice->id : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Price Name</label>
                                        <input type="text" class="md-input" name="name" value="{{ isset($roomprice->name) ? $roomprice->name : '' }}" />
                                        @if($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Description</label>
                                        <textarea cols="30" rows="4" class="md-input" name="description">{{ isset($roomprice->description) ? $roomprice->description : '' }}</textarea>
                                        @if($errors->has('description'))<small class="text-danger">{{ $errors->first('description') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($roomprice->status) && $roomprice->status == 1) ? 'checked' : ''  }} {{ (!isset($roomprice)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($roomprice->status) && $roomprice->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('room.price.type') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>

<script type="text/javascript">
$(function () {
// It has the name attribute "roomfacilities_frm"
    $("form#roomprice_frm").validate({
// Specify validation rules
        rules: {
            name: "required",
            description: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            name: "Please enter  name",
            description: "Please enter description",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

});

</script>
@endsection

