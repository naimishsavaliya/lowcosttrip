@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Hotel Room Booking</span></li>
        </ul>
    </div>

    <div id="page_content_inner">
        <ul class="uk-tab uk-tab-double-header uk-margin-bottom" data-uk-tab="{connect:'#tabs_search_content', animation:'slide-left'}" id="tabs_search" style="display: none;">
            <li class="uk-active"><a href="#">News</a></li>
        </ul>
        <div class="uk-grid" data-uk-grid-margin="">
            <div class="uk-width-medium-6-10">
                <div class="uk-margin-small-top">
                    <select id="product_search_status" data-md-selectize multiple data-md-selectize-bottom>
                        <option value="">Search Hotel Name, Area, City</option>
                        <option value="1">Gateway Hotel</option>
                        <option value="2">Welcome hotel</option>
                        <option value="3">Fortune</option>
                    </select>
                </div>
            </div>
            <div class="uk-width-medium-1-10">
                <label for="product_search_name">Check In</label>
                <input type="text" class="md-input" value="" data-uk-datepicker="{format:'DD MMMM YYYY'}">
            </div>
            <div class="uk-width-medium-1-10">
                <label for="product_search_price">Check OUt</label>
                <input type="text" class="md-input" value="" data-uk-datepicker="{format:'DD MMMM YYYY'}">
            </div> 
            <div class="uk-width-medium-1-10">
                <label for="product_search_price">No of Rooms</label>
                <input type="text" class="md-input" value="">
            </div>
            <div class="uk-width-medium-1-10 uk-text-center">
                <a href="#" class="md-btn md-btn-primary md-btn-large md-btn-wave-light waves-effect waves-button waves-light">Search</a>
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin="">
            <div class="uk-width-medium-1-5">
                <label>Star Rating</label>
                <select id="product_search_status" class="md-input">
                    <option value=""></option>
                    <option value="1">1 Star</option>
                    <option value="2">2 Star</option>
                    <option value="3">3 Star</option>
                    <option value="3">4 Star</option>
                    <option value="3">5 Star</option>
                </select>
            </div>
            <div class="uk-width-medium-1-5">
                <label>Hotel Type</label>
                <select id="product_search_status" class="md-input">
                    <option value=""></option>
                    <option value="1">Business</option>
                    <option value="2">Service Apartment</option>
                    <option value="3">Resort</option>
                    <option value="3">Home Stay</option>
                </select>
            </div>
            <div class="uk-width-medium-1-5">
                <label>Budget</label>
                <select id="product_search_status" class="md-input">
                    <option value=""></option>
                    <option value="1">500 to 2000</option>
                    <option value="2">2001 to 5000</option>
                    <option value="3">5001 to 10,000</option>
                    <option value="3">10,000 and above</option>
                </select>
            </div> 
            <div class="uk-width-medium-1-5">
                <label>Meal Plan</label>
                <select id="product_search_status" class="md-input">
                    <option value=""></option>
                    <option value="1">Full Board</option>
                    <option value="2">Half Board</option>
                    <option value="3">Breakfast Only</option>
                    <option value="3">No Meal</option>
                </select>
            </div>
            <div class="uk-width-medium-1-5">
                <label>Sort By</label>
                <select id="product_search_status" class="md-input">
                    <option value=""></option>
                    <option value="1">By Rating</option>
                    <option value="2">By Price</option>
                    <option value="3">By Name</option>
                </select>
            </div>
        </div>
        <ul id="tabs_search_content" class="uk-switcher">
            <li>
                <div>
                    <ul class="search_list">
                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <h3 class="search_list_heading"><a href="#">The Gateway Hotel</a></h3>
                                    <div class="line p-t-5">
                                        <span class="search_list_link">Vadodara</span>
                                        <div class="pricing_table_plan">
                                            <i class="material-icons icon-star" >&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                        </div>
                                    </div>
                                    <div class="line p-t-16">
                                        <i data-uk-tooltip title="Meal" class="uk-icon-hover uk-icon-button uk-icon-spoon"></i>
                                        <i data-uk-tooltip title="Bed" class="uk-icon-hover uk-icon-button uk-icon-bed"></i>
                                        <i data-uk-tooltip title="Car Parking" class="uk-icon-hover uk-icon-button uk-icon-car"></i>
                                        <i data-uk-tooltip title="Wifi" class="uk-icon-hover uk-icon-button uk-icon-wifi"></i>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <div class="pricing_table_price">
                                        <span class="currency">Rs.</span>20<span class="period">/ Per Night</span>
                                    </div>
                                    <div class="line" style="text-align:right;">
                                        <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/detail/1">Book Room</a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <h3 class="search_list_heading"><a href="#">The Gateway Hotel</a></h3>
                                    <div class="line p-t-5">
                                        <span class="search_list_link">Vadodara</span>
                                        <div class="pricing_table_plan">
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                        </div>
                                    </div>
                                    <div class="line p-t-16">
                                        <i data-uk-tooltip title="Meal" class="uk-icon-hover uk-icon-button uk-icon-spoon"></i>
                                        <i data-uk-tooltip title="Bed" class="uk-icon-hover uk-icon-button uk-icon-bed"></i>
                                        <i data-uk-tooltip title="Car Parking" class="uk-icon-hover uk-icon-button uk-icon-car"></i>
                                        <i data-uk-tooltip title="Wifi" class="uk-icon-hover uk-icon-button uk-icon-wifi"></i>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <div class="pricing_table_price">
                                        <span class="currency">Rs.</span>20<span class="period">/ Per Night</span>
                                    </div>
                                    <div class="line" style="text-align:right;">
                                        <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <h3 class="search_list_heading"><a href="#">The Gateway Hotel</a></h3>
                                    <div class="line p-t-5">
                                        <span class="search_list_link">Vadodara</span>
                                        <div class="pricing_table_plan">
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                        </div>
                                    </div>
                                    <div class="line p-t-16">
                                        <i data-uk-tooltip title="Meal" class="uk-icon-hover uk-icon-button uk-icon-spoon"></i>
                                        <i data-uk-tooltip title="Bed" class="uk-icon-hover uk-icon-button uk-icon-bed"></i>
                                        <i data-uk-tooltip title="Car Parking" class="uk-icon-hover uk-icon-button uk-icon-car"></i>
                                        <i data-uk-tooltip title="Wifi" class="uk-icon-hover uk-icon-button uk-icon-wifi"></i>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <div class="pricing_table_price">
                                        <span class="currency">Rs.</span>20<span class="period">/ Per Night</span>
                                    </div>
                                    <div class="line" style="text-align:right;">
                                        <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <h3 class="search_list_heading"><a href="#">The Gateway Hotel</a></h3>
                                    <div class="line p-t-5">
                                        <span class="search_list_link">Vadodara</span>
                                        <div class="pricing_table_plan">
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                        </div>
                                    </div>
                                    <div class="line p-t-16">
                                        <i data-uk-tooltip title="Meal" class="uk-icon-hover uk-icon-button uk-icon-spoon"></i>
                                        <i data-uk-tooltip title="Bed" class="uk-icon-hover uk-icon-button uk-icon-bed"></i>
                                        <i data-uk-tooltip title="Car Parking" class="uk-icon-hover uk-icon-button uk-icon-car"></i>
                                        <i data-uk-tooltip title="Wifi" class="uk-icon-hover uk-icon-button uk-icon-wifi"></i>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <div class="pricing_table_price">
                                        <span class="currency">Rs.</span>20<span class="period">/ Per Night</span>
                                    </div>
                                    <div class="line" style="text-align:right;">
                                        <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <h3 class="search_list_heading"><a href="#">The Gateway Hotel</a></h3>
                                    <div class="line p-t-5">
                                        <span class="search_list_link">Vadodara</span>
                                        <div class="pricing_table_plan">
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                        </div>
                                    </div>
                                    <div class="line p-t-16">
                                        <i data-uk-tooltip title="Meal" class="uk-icon-hover uk-icon-button uk-icon-spoon"></i>
                                        <i data-uk-tooltip title="Bed" class="uk-icon-hover uk-icon-button uk-icon-bed"></i>
                                        <i data-uk-tooltip title="Car Parking" class="uk-icon-hover uk-icon-button uk-icon-car"></i>
                                        <i data-uk-tooltip title="Wifi" class="uk-icon-hover uk-icon-button uk-icon-wifi"></i>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <div class="pricing_table_price">
                                        <span class="currency">Rs.</span>20<span class="period">/ Per Night</span>
                                    </div>
                                    <div class="line" style="text-align:right;">
                                        <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <h3 class="search_list_heading"><a href="#">The Gateway Hotel</a></h3>
                                    <div class="line p-t-5">
                                        <span class="search_list_link">Vadodara</span>
                                        <div class="pricing_table_plan">
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                        </div>
                                    </div>
                                    <div class="line p-t-16">
                                        <i data-uk-tooltip title="Meal" class="uk-icon-hover uk-icon-button uk-icon-spoon"></i>
                                        <i data-uk-tooltip title="Bed" class="uk-icon-hover uk-icon-button uk-icon-bed"></i>
                                        <i data-uk-tooltip title="Car Parking" class="uk-icon-hover uk-icon-button uk-icon-car"></i>
                                        <i data-uk-tooltip title="Wifi" class="uk-icon-hover uk-icon-button uk-icon-wifi"></i>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <div class="pricing_table_price">
                                        <span class="currency">Rs.</span>20<span class="period">/ Per Night</span>
                                    </div>
                                    <div class="line" style="text-align:right;">
                                        <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <h3 class="search_list_heading"><a href="#">The Gateway Hotel</a></h3>
                                    <div class="line p-t-5">
                                        <span class="search_list_link">Vadodara</span>
                                        <div class="pricing_table_plan">
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                        </div>
                                    </div>
                                    <div class="line p-t-16">
                                        <i data-uk-tooltip title="Meal" class="uk-icon-hover uk-icon-button uk-icon-spoon"></i>
                                        <i data-uk-tooltip title="Bed" class="uk-icon-hover uk-icon-button uk-icon-bed"></i>
                                        <i data-uk-tooltip title="Car Parking" class="uk-icon-hover uk-icon-button uk-icon-car"></i>
                                        <i data-uk-tooltip title="Wifi" class="uk-icon-hover uk-icon-button uk-icon-wifi"></i>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <div class="pricing_table_price">
                                        <span class="currency">Rs.</span>20<span class="period">/ Per Night</span>
                                    </div>
                                    <div class="line" style="text-align:right;">
                                        <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <h3 class="search_list_heading"><a href="#">The Gateway Hotel</a></h3>
                                    <div class="line p-t-5">
                                        <span class="search_list_link">Vadodara</span>
                                        <div class="pricing_table_plan">
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                            <i class="material-icons icon-star">&#xE838;</i>
                                        </div>
                                    </div>
                                    <div class="line p-t-16">
                                        <i data-uk-tooltip title="Meal" class="uk-icon-hover uk-icon-button uk-icon-spoon"></i>
                                        <i data-uk-tooltip title="Bed" class="uk-icon-hover uk-icon-button uk-icon-bed"></i>
                                        <i data-uk-tooltip title="Car Parking" class="uk-icon-hover uk-icon-button uk-icon-car"></i>
                                        <i data-uk-tooltip title="Wifi" class="uk-icon-hover uk-icon-button uk-icon-wifi"></i>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2" style="padding-left:0px;">
                                    <div class="pricing_table_price">
                                        <span class="currency">Rs.</span>20<span class="period">/night</span>
                                    </div>
                                    <div class="line" style="text-align:right;">
                                        <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="uk-pagination uk-margin-medium-top">
                    <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
                    <li class="uk-active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><span>&hellip;</span></li>
                    <li><a href="#">20</a></li>
                    <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>


@endsection

