@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('hotel.room') }}">Hotel Room</i></a></li>
            <li><span>Add / Edit Hotel Room</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner" style="padding-top:0;">
            <div>
                <form action="{{ env('ADMIN_URL')}}hotel/room" class="" method="get">
                    <div class="p-t-30">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                <option value="" selected>Choose Hotel</option>
                                                <option value="1">Hotel 1</option>
                                                <option value="2">Hotel 2</option>
                                                <option value="3">Hotel 3</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                <option value="" selected>Choose Room Type</option>
                                                <option value="1">Room Type 1</option>
                                                <option value="2">Room Type 2</option>
                                                <option value="3">Room Type 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Max Children</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>No.of rooms (in hotel)</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                <option value="" selected>Bed Type</option>
                                                <option value="1">Single Bed</option>
                                                <option value="2">Twin Bed</option>
                                                <option value="2">Bunk Bed</option>
                                                <option value="2">Small Double Bed</option>
                                                <option value="2">Double Bed</option>
                                                <option value="2">Queen Bed</option>
                                                <option value="2">King Bed</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom multiple="" >
                                                <option value="" selected>Good For</option>
                                                <option value="1">Couples</option>
                                                <option value="2">Families</option>
                                                <option value="3">Friends</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                <option value="" selected>Number Of Beds</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                <option value="" selected>Number Of BathRooms</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Room Area</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Video Url</label>
                                            <input type="text" class="md-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Short Description</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Brief Description</label>
                                            <textarea cols="30" rows="4" class="md-input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label for="Facilities" class="uk-form-label">Facilities</label>
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="checkbox" name="facilities[]" id="facilities_1" data-md-icheck />
                                                    <label for="facilities_1" class="inline-label">Board Room</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="checkbox" name="facilities[]" id="facilities_2" data-md-icheck />
                                                    <label for="facilities_2" class="inline-label">Restaurant</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="checkbox" name="facilities[]" id="facilities_3" data-md-icheck />
                                                    <label for="facilities_3" class="inline-label">Wi-Fi Internet</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="checkbox" name="facilities[]" id="facilities_4" data-md-icheck />
                                                    <label for="facilities_4" class="inline-label">Room Service</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="checkbox" name="facilities[]" id="facilities_5" data-md-icheck />
                                                    <label for="facilities_5" class="inline-label">Front desk</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="checkbox" name="facilities[]" id="facilities_6" data-md-icheck />
                                                    <label for="facilities_6" class="inline-label">Doctor on Call</label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="uk-width-medium-1-2 uk-row-first">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label class="control-label">Is Refundable</label>
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="radio"  id="discount_type_0" data-md-icheck name="type" value="0" />
                                                    <label for="discount_type_0" class="inline-label">Yes</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" id="discount_type_1" data-md-icheck name="type" value="1" />
                                                    <label for="discount_type_1" class="inline-label">No</label>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label class="control-label">Pay at Checkout</label>
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="radio"  id="discount_type_3" data-md-icheck name="type" value="0" />
                                                    <label for="discount_type_3" class="inline-label">Yes</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" id="discount_type_4" data-md-icheck name="type" value="1" />
                                                    <label for="discount_type_4" class="inline-label">No</label>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1">
                                            <h3 class="heading_a uk-margin-small-bottom">Room images</h3>
                                            <div id="file_upload-drop" class="uk-file-upload">
                                                <p class="uk-text">Drop file to upload</p>
                                                <p class="uk-text-muted uk-text-small uk-margin-small-bottom">or</p>
                                                <a class="uk-form-file md-btn">choose file<input id="file_upload-select" type="file"></a>
                                            </div>
                                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                                <div class="uk-progress-bar" style="width:0">0%</div>
                                            </div>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <div>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="status" value="1" id="status_1" data-md-icheck checked="" />
                                                    <label for="status" class="inline-label">Active</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="status" value="0" id="status_0" data-md-icheck  />
                                                    <label for="status_0" class="inline-label">In Active</label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('supplier.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>  
                </form>
            </div>

        </div>
    </div>

</div>

@endsection
