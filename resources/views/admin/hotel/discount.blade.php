@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li>Hotel Name</li>
            <li><span>Discount</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}hotel/manage_discount/1" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')


            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Discount Code</th>
                        <th>Room Type</th>
                        <th>Room Option</th>
                        <th>Price</th>
                        <th>Discount %</th>
                        <th>Discounted Price</th>
                        <th>Date Period</th>
                        <th>Travel Agent</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <!-- <tfoot>
                    <tr>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="ID" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Discount Code" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Room Type" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Room Option" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Price" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Discount %" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Discounted Price" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Date Period" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Travel Agent" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                </tfoot> -->
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>DIS<?php echo rand(111,999);?></td>
                            <td>Deluxe</td>
                            <td>Breakfast / Lunch</td>
                            <td>1500</td>
                            <td>10</td>
                            <td>1350</td>
                            <td>01 Jan 19 To 05 Feb 19</td>
                            <td>Travel Agent</td>
                            <td>
                                <a href="{{ env('ADMIN_URL')}}hotel/room-type/edit/1" title="Edit Discount" class=""><i class="md-icon material-icons">&#xE254;</i></a> 
                                <a href="{{ env('ADMIN_URL')}}hotel/room-type/delete/1" onclick="return confirm('Are you sure to delete?');" title="Delete Discount" class=""><i class="md-icon material-icons">delete</i></a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection

