@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')
<style>
    .md-list .uk-nestable-list > li, .md-list > li{min-height: 0; padding:0 0 0 0 !important;}
</style>
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><span>Room Book</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-6-10">
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <label>Lead ID</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-1-2">
                                <label>Email</label>
                                <input type="text" class="md-input"  />
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-2-10">
                                <label>Title</label>
                                <select id="product_search_status" class="md-input">
                                    <option value=""></option>
                                    <option value="1">Ms.</option>
                                    <option value="2">Mrs.</option>
                                </select>
                            </div>
                            <div class="uk-width-medium-4-10">
                                <label>First Name</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-4-10">
                                <label>Last Name</label>
                                <input type="text" class="md-input" />
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-2-10">
                                <label>No. Of Room</label>
                                <select id="product_search_status" class="md-input">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="uk-width-medium-4-10">
                                <label>Adult</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-4-10">
                                <label>Child</label>
                                <input type="text" class="md-input" />
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-5-10">
                                <label>Phone No</label>
                                <input type="text" class="md-input" />
                            </div>
                            <div class="uk-width-medium-5-10">
                                <label>Add Discount Code</label>
                                <input type="text" class="md-input" />
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <a class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" href="./manage_hotel_detail.php">Submit</a>
                    </div>
                </div>
                <div class="uk-width-medium-4-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                The Gateway Hotel 
                                <div class="pricing_table_plan fac-star-wrapper display-inline-block p-l-10">
                                    <i class="material-icons icon-star">&#xE838;</i>
                                    <i class="material-icons icon-star">&#xE838;</i>
                                    <i class="material-icons icon-star">&#xE838;</i>
                                    <i class="material-icons icon-star">&#xE838;</i>
                                    <i class="material-icons icon-star">&#xE838;</i>
                                </div>
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <b class="uk-text-small uk-display-block">Areal</b>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-12">
                                    <ul class="md-list hotel-detail-list ">
                                        <li class="uk-width-medium-1-2 uk-float-left">
                                            <div class="md-list-content">
                                                <span class="uk-text-small uk-text-muted uk-display-block">Adults</span>
                                                <span class="md-list-heading uk-text-medium">2</span>
                                            </div>
                                        </li>
                                        <li class="uk-width-medium-1-2 uk-float-left">
                                            <div class="md-list-content">
                                                <span class="uk-text-small uk-text-muted uk-display-block">Room</span>
                                                <span class="md-list-heading uk-text-medium">1</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-medium-1-1">
                                    <ul class="md-list hotel-detail-list ">
                                        <li>
                                            <div class="md-list-content">
                                                <span class="md-list-heading uk-text-medium uk-display-block">Delux Room</span>
                                                <span class="md-list-heading uk-text-medium">Breakfast Included</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-medium-1-1">
                                    <ul class="md-list hotel-detail-list  ">
                                        <li class="uk-width-medium-7-10 uk-float-left">
                                            <span class="uk-text-small uk-display-block">Price (1 room x 5 Nights)</span>
                                        </li>
                                        <li class="uk-width-medium-3-10 text-right uk-float-left">
                                            <span class="md-list-heading uk-text-large"><i class="icon-rupee"></i> 2000</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-medium-1-12">
                                    <ul class="md-list hotel-detail-list  ">
                                        <li class="uk-width-medium-7-10 uk-float-left">
                                            <span class="uk-text-small uk-display-block">VAT / GST</span>
                                        </li>
                                        <li class="uk-width-medium-3-10 text-right uk-float-left">
                                            <span class="md-list-heading uk-text-large"><i class="icon-rupee"></i> 2000</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-medium-1-12">
                                    <hr class="uk-article-divider">
                                    <ul class="md-list hotel-detail-list ">
                                        <li class="uk-width-medium-7-10 uk-float-left">
                                            <div class="md-list-content">
                                                <span class="md-list-heading uk-text-medium">Total</span>
                                            </div>
                                        </li>
                                        <li class="uk-width-medium-3-10 text-right uk-float-left">
                                            <span class="md-list-heading uk-text-large uk-text-success"><i class="icon-rupee"></i> 4000</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2 text-center">
                            <a class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" >Make Payment</a>
                        </div>
                        <div class="uk-width-medium-1-2 text-center">
                            <a class="md-btn md-btn-default md-btn-wave-light waves-effect waves-button waves-light">Payment Offline</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

@endsection
