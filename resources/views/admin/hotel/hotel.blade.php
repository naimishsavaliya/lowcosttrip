@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Hotel</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}hotel/add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Hotel
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table" id="hotel_tbl">
                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Hotel Name</th>
                        <th>Location</th>
                        <th class="hidden-phone">Email/Mobile</th>
                        <th class="hidden-phone">Stars</th>
                        <th class="hidden-phone">Total Agents</th>
                        <th class="hidden-phone">Total Inventory</th>
                        <th class="hidden-phone">Total Booking</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <?php /*
                <tfoot>
                    <tr>
                        <td><input type="text"  placeholder="ID" class="md-input" colPos="0"></td>
                        <td><input type="text"  placeholder="Hotel Name" class="md-input" colPos="1"></td>
                        <td></td>
                        <td><input type="text"  placeholder="Email/Mobile" class="md-input" colPos="3"></td>
                        <td>
                            <select class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Star..." colPos="4">
                                <option value="">Select Stars...</option>
                                <?php
                                for ($st_i = 1; $st_i <= 5; $st_i++) {
                                    ?>
                                    <option value="{{ $st_i }}">{{ str_repeat('*',$st_i) .' ('.$st_i. ')'}}</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <select class="md-input" data-placeholder="Choose a Status" tabindex="6" colPos="8">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="0">InActive</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                </tfoot>
                */ ?>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="changestatus" class="uk-modal" >
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Status</h3>
        </div>
        <div class="">
            <div class="control-group">
                <input type="hidden" value="" id="change-status-row-id" />
                <label class="control-label">Status</label>
                <div class="controls">
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="1" id="status_dialog_active" data-md-icheck />
                        <label for="status_dialog_active" class="inline-label">Active</label>
                    </span>
                    <span class="icheck-inline">
                        <input type="radio" name="status" class="user-status-change" value="0" id="status_dialog_inactive" data-md-icheck />
                        <label for="status_dialog_inactive" class="inline-label">InActive</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close" id="status-dlg-btn">Submit</button> 
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function () {

        var hotelTable = $('#hotel_tbl').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            searching: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            order: [[ 0, "desc" ]],
//            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
//            "pagingType": "full_numbers", //full,full_numbers
            "ajax": {
                "url": "{{ route('hotel.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof hotelTable != 'undefined' && hotelTable.hasOwnProperty('settings')) {
                        hotelTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "id", "name": "hotel.id"},
                {"data": "name", "name": "hotel.name"},
                {"data": "location", "name": "location",render:function(data,type,row){
                    return data.split('\n').join('<br>');
                }},
                {"data": "hotel_info", "name": "hotel_info",render:function(data,type,row){
                    return data.split('\n').join('<br>');
                }},
                {"data": "star", "name": "hotel.star"},
                {"data": "total_agent", "name": "total_agent", 'searchable': false},
                {"data": "total_inventory", "name": "total_inventory", 'searchable': false},
                {"data": "total_booking", "name": "total_booking", 'searchable': false},
                {"data": "status", "name": "status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.id + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-success">Active</span></a>';
                        } else {
                            return '<a class="change-status-btn" data-id="' + row.status + '" row-id="' + row.id + '" data-uk-modal="{target:\'#changestatus\'}" title="Status"><span class="uk-badge uk-badge-danger">InActive</span></a>';
                        }
                    },
                },
                {"data": "action", 'sortable': false, "render": function (data, type, row) {
                        var action_str = '';
                        var edit_url = "{{ route('hotel.edit', ':id') }}";
                        var detail_url = "{{ route('hotel.detail', ':id') }}";
                        var dele_url = "{{ route('hotel.delete', ':id') }}";
                        var inventory_url = "{{ route('hotel.room.invntory', ':id') }}";
                        var booking_url = "{{ route('hotel.view.booking', ':id') }}";
                        var calendar_url = "{{ route('hotel.view.calendar', ':id') }}";
                        var discount_url = "{{ route('hotel.discount', ':id') }}";
                        var room_url = "{{ route('hotel.room', ':id') }}";
                        edit_url = edit_url.replace(':id', row.id);
                        detail_url = detail_url.replace(':id', row.id);
                        dele_url = dele_url.replace(':id', row.id);
                        inventory_url = inventory_url.replace(':id', row.id);
                        booking_url = booking_url.replace(':id', row.id);
                        calendar_url = calendar_url.replace(':id', row.id);
                        discount_url = discount_url.replace(':id', row.id);
                        room_url = room_url.replace(':id', row.id);

                        action_str += '<div class="uk-button-dropdown" data-uk-dropdown="{pos:\'bottom - right\'}">';
                        action_str += '<button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>';
                        action_str += '<div class="uk-dropdown">';
                        action_str += '<ul class="uk-nav uk-nav-dropdown">';
                        // action_str += '<li><a href="' + detail_url + '"> View Hotel</a></li>';
                        action_str += '<li><a href="' + edit_url + '"> View/Edit Hotel</a></li>';
                        action_str += '<li><a href="' + inventory_url + '"> View/Update Inventory</a></li>';
                        action_str += '<li><a href="' + booking_url + '"> View Booking List</a></li>';
                        action_str += '<li><a href="' + calendar_url + '"> View Booking Calendar</a></li>';
                        action_str += '<li><a href="' + room_url + '"> Manage Room</a></li>';
                        // action_str += '<li><a href="' + discount_url + '"> Manage Discount</a></li>';
                        action_str += '<li><a href="' + dele_url + '" class="delete-hotel" data-id="' + row.id + '">Delete</a></li>';
                        action_str += '</ul>';
                        action_str += '</div>';
                        action_str += '</div>';
                        return action_str;
                    },
                },
            ]
        });


        $(hotelTable.table().container()).on('keyup', 'tfoot input', function () {
            hotelTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(hotelTable.table().container()).on('change', 'tfoot select', function () {
            hotelTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(document.body).on('click', '.change-status-btn', function () {
            var $obj = $(this);
            var row_id = $obj.attr('row-id');
            var status = $obj.attr('data-id');
            $('input#change-status-row-id').val(row_id);
            if (status == "1") {
                $('#status_dialog_inactive').removeAttr('checked');
                $('#status_dialog_active').iCheck('check');
            } else {
                $('#status_dialog_active').removeAttr('checked');
                $('#status_dialog_inactive').iCheck('check');
            }
        });

        $(document.body).on('click', '#status-dlg-btn', function () {
            var $obj = $(this);
            var row_id = $('#change-status-row-id').val();
            var status = $('input.user-status-change:checked').val();
            $.ajax({
                type: 'get',
                url: '{{ route("change_status")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table": "hotel",
                    "id": row_id,
                    "column_name": "id",
                    "status": status
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    if (data.success) {
                        hotelTable.draw();
                    }
                    showNotify(data.msg_status, data.message);
                }
            });
        });

        $(document.body).on('click', '.delete-hotel', function () {
            var crm = confirm("Are you sure to delete?");
            if (crm) {
                var $obj = $(this);
                var lead_id = $obj.attr('data-id');
                var delete_url = $obj.attr('href');
                $.ajax({
                    type: 'get',
                    url: delete_url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {

                    }, success: function (data, textStatus, jqXHR) {
                        if (data.success) {
                            hotelTable.draw();
                        }
                        showNotify(data.msg_status, data.message);
                    }
                });
            }
            return false;
        });

    });
</script>
@endsection