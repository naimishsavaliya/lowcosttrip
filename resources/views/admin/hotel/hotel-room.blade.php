@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Manage Room</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}hotel/room-add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Room Name</th>
                        <th>Hotel Name</th>
                        <th class="hidden-phone">Location</th>
                        <th class="hidden-phone">Can Sleep</th>
                        <th class="hidden-phone">Total Inventory</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
<!--                <tfoot>
                    <tr>
                        <td class="pd0"><input type="text"  placeholder="ID" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Room Name" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Hotel Name" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Destination" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Can Sleep" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Total Inventory" name="" colPos="1"></td>
                        <td ><input type="text"  placeholder="Total Agent" name="" colPos="1"></td>
                        <td ><select class="chosen" data-placeholder="Choose a Status" tabindex="1" colPos="6">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="2">InActive</option>
                            </select></td>
                        <td></td>
                    </tr>
                </tfoot>-->
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>Room Name</td>
                            <td>Hotel <?php echo $i; ?></td>
                            <td>City,State,Country</td>
                            <td class="hidden-phone"><?php echo rand(1, 5); ?></td>
                            <td class="hidden-phone"><a href="#inventory_dialog" data-toggle="modal" title="Inventory"><?php echo $i; ?></a></td>
                            <td ><span class="label label-success">Active</span></td>
                            <td >
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                    <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav uk-nav-dropdown">
                                            <li><a href="{{ route('hotel.room.detail',1) }}">View Details</a> </li>
                                            <li><a href="{{ env('ADMIN_URL')}}hotel/room-edit/1">Edit Room</a></li>
                                            <li><a href="{{ env('ADMIN_URL')}}hotel/room-delete/1" onclick="return confirm('Are you sure to delete?');" >Delete Room</a> </li>
                                            <li><a href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a></li>
                                            <li><a href="{{ route('hotel.room.detail',1) }}#rates" >Manage Rates</a> </li>
                                            <li><a href="{{ route('hotel.room.detail',1) }}#booking" >Current Booking</a> </li>
                                            <li><a href="{{ route('hotel.room.detail',1) }}#calendar" >Room Booking Calendar</a> </li>
                                            <li><a href="{{ route('hotel.room.detail',1) }}#availability" >Room Availability Calendar</a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php /*
<div id="inventory_dialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel1">Inventory Information</h3>

    </div>
    <div class="modal-body">
        <h2>Inventory Information here</h2>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>

<div id="agent_dialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel1">Agent Information</h3>

    </div>
    <div class="modal-body">
        <h2>Agent Information here</h2>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
 * 
 */ ?>

@endsection