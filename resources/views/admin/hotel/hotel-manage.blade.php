@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}css/dropzone.css">
<style type="text/css">
    .fileuploader {
        position: relative;
        /*width: 60%;*/
        margin: auto;
        /*height: 400px;*/
        border: 4px dashed #ddd;
        background: #f6f6f6;
        /*margin-top: 85px;*/
    }
    .fileuploader #upload-label{
        background: rgba(231, 97, 92, 0);
        color: #fff;
        position: absolute;
        height: 115px;
        top: 20%;
        left: 0;
        right: 0;
        margin-right: auto;
        margin-left: auto;
        min-width: 20%;
        text-align: center;
        cursor: pointer;
    }
    .fileuploader.active{
        background: #fff;
    }
    .fileuploader.active #upload-label{
        background: #fff;
        color: #e7615c;
    }
    .fileuploader #upload-label i:hover {
        color: #444;
        font-size: 9.4rem;
        -webkit-transition: width 2s;
    }
    .fileuploader #upload-label span.title{
        font-size: 1em;
        font-weight: bold;
        display: block;
    }
    span.tittle {
        position: relative;
        top: 222px;
        color: #bdbdbd;
    }
    .fileuploader #upload-label i{
        text-align: center;
        display: block;
        color: #e7615c;
        height: 115px;
        font-size: 9.5rem;
        position: absolute;
        top: -12px;
        left: 0;
        right: 0;
        margin-right: auto;
        margin-left: auto;
    }
    /** Preview of collections of uploaded documents **/
    .preview-container{
        position: relative;
        bottom: 0px;
        width: 35%;
        margin: auto;
        top: 25px;
        visibility: hidden;
    }
    .preview-container #previews{
        max-height: 400px;
        overflow: auto; 
    }
    .preview-container #previews .zdrop-info{
        width: 88%;
        margin-right: 2%;
    }
    .preview-container #previews.collection{
        margin: 0;
        box-shadow: none;
    }

    .preview-container #previews.collection .collection-item {
        background-color: #e0e0e0;
    }

    .preview-container #previews.collection .actions a{
        width: 1.5em;
        height: 1.5em;
        line-height: 1;
    }
    .preview-container #previews.collection .actions a i{
        font-size: 1em;
        line-height: 1.6;
    }
    .preview-container #previews.collection .dz-error-message{
        font-size: 0.8em;
        margin-top: -12px;
        color: #F44336;
    }

    /*media queries*/

    @media only screen and (max-width: 601px){
        .fileuploader {
            width: 100%;
        }

        .preview-container {
            width: 100%;
        }
    }
</style>
@endsection

@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner" style="padding-top:0;">
            <div>
                <form action="{{ route('hotel.store') }}" name="hotel_frm" id="hotel_frm" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="temp_id" id="hotel_temp_id" value="{{ isset($hotel->temp_id) ? $hotel->temp_id : str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT) }}" />
                    <input type="hidden" name="id" id="hotel_id" value="{{ isset($hotel->id) ? $hotel->id : '' }}" />
                    <div class="p-t-30">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Hotel Name <span class="required-lbl">*</span></label>
                                            <input type="text" class="md-input" name="name" value="{{ isset($hotel->name) ? $hotel->name : '' }}" />
                                            @if($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small>@endif
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select id="destination_id" name="destination_id" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Location...">
                                                <option value="">Choose Location...</option>
                                                <?php
                                                if (count($destinations) > 0) {
                                                    foreach ($destinations as $destination) {
                                                        ?>
                                                        <option value="{{ $destination->desId }}" {{ (isset($hotel->destination_id) && $hotel->destination_id == $destination->desId) ? "selected" : "" }}>{{ $destination->name }}</option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="star" name="star" class="md-input" data-uk-tooltip="{pos:'bottom'}" title="Select Star...">
                                                <option value="">Select Stars...</option>
                                                <?php
                                                for ($st_i = 1; $st_i <= 5; $st_i++) {
                                                    ?>
                                                    <option value="{{ $st_i }}" {{ (isset($hotel->star) && $hotel->star == $st_i) ? "selected" : "" }}>{{ str_repeat('*',$st_i) .' ('.$st_i. ')'}}</option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Address</label>
                                            <textarea cols="30" rows="4" class="md-input" name="address">{{ isset($hotel->address) ? $hotel->address : '' }}</textarea>
                                            @if($errors->has('address'))<small class="text-danger">{{ $errors->first('address') }}</small>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select class="md-input" name="country_id" id="country_id" data-uk-tooltip="{pos:'bottom'}" title="Select Country...">
                                                <option value="" selected>Select Country...</option>
                                                @if(isset($countrys))
                                                @foreach($countrys as $country)
                                                <option value="{{ $country->country_id }}" {{ (isset($hotel->country_id) && $hotel->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            @if($errors->has('country_id'))<small class="text-danger">{{ $errors->first('country_id') }}</small>@endif
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select class="md-input" name="state_id" id="state_id" data-uk-tooltip="{pos:'bottom'}" title="Select State...">
                                                <option value="">Choose a State</option>
                                                @if(isset($states))
                                                @foreach($states as $state)
                                                <option value="{{ $state->state_id }}" {{ (isset($hotel->state_id) && $hotel->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                                                @endforeach
                                                @endif
                                                @if($errors->has('state_id'))<small class="text-danger">{{ $errors->first('state_id') }}</small>@endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select class="md-input" name="city_id" id="city_id" data-uk-tooltip="{pos:'bottom'}" title="Select City...">
                                                <option value="">Choose City...</option>
                                                @if(isset($cities))
                                                @foreach($cities as $city)
                                                <option value="{{ $city->city_id }}" {{ (isset($hotel->city_id) && $hotel->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                                @endforeach
                                                @endif
                                                @if($errors->has('city_id'))<small class="text-danger">{{ $errors->first('city_id') }}</small>@endif
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Pincode</label>
                                            <input type="text" class="md-input" name="pincode" value="{{ isset($hotel->pincode) ? $hotel->pincode : '' }}" />
                                            @if($errors->has('pincode'))<small class="text-danger">{{ $errors->first('pincode') }}</small>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Mobile Number</label>
                                            <input type="text" class="md-input" name="mobile" value="{{ isset($hotel->mobile) ? $hotel->mobile : '' }}" />
                                            @if($errors->has('mobile'))<small class="text-danger">{{ $errors->first('mobile') }}</small>@endif
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Landline Number</label>
                                            <input type="text" class="md-input" name="landline" value="{{ isset($hotel->landline) ? $hotel->landline : '' }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Fax</label>
                                            <input type="text" class="md-input" name="fax" value="{{ isset($hotel->fax) ? $hotel->fax : '' }}" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Email Address</label>
                                            <input type="text" class="md-input" name="email"  value="{{ isset($hotel->email) ? $hotel->email : '' }}" />
                                            @if($errors->has('email'))<small class="text-danger">{{ $errors->first('email') }}</small>@endif                                            
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Manager Name</label>
                                            <input type="text" class="md-input" name="manager_name" value="{{ isset($hotel->manager_name) ? $hotel->manager_name : '' }}" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Reference No.</label>
                                            <input type="text" class="md-input" name="reference_no"  value="{{ isset($hotel->reference_no) ? $hotel->reference_no : '' }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Map Code</label>
                                            <input type="text" class="md-input" name="map_code" value="{{ isset($hotel->map_code) ? $hotel->map_code : '' }}" />
                                        </div>
                                        <div class="uk-width-medium-1-4 p-l-50">
                                            <label>Check-In Time</label>
                                            <input type="text" class="md-input" id="check_in_time" name="check_in_time" value="{{ isset($hotel->check_in_time) ? $hotel->check_in_time : '' }}" data-uk-timepicker autocomplete="off" />
                                            @if($errors->has('check_in_time'))<small class="text-danger">{{ $errors->first('check_in_time') }}</small>@endif
                                        </div>
                                        <div class="uk-width-medium-1-4 p-l-50">
                                            <label>Check-Out Time</label>
                                            <input type="text" class="md-input" id="check_out_time" name="check_out_time" value="{{ isset($hotel->check_out_time) ? $hotel->check_out_time : '' }}" data-uk-timepicker autocomplete="off" />
                                            @if($errors->has('check_out_time'))<small class="text-danger">{{ $errors->first('check_out_time') }}</small>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                             

                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label for="meal_includes" class="uk-form-label">Tags</label>
                                            <input type="text" class="md-input" name="tags" value="{{ isset($hotel->tags) ? $hotel->tags : '' }}" />
                                        </div> 
                                        <div class="uk-width-medium-1-2">
                                            <label>Email Address</label>
                                            <input type="text" class="md-input" name="email"  value="{{ isset($hotel->email) ? $hotel->email : '' }}" />
                                            @if($errors->has('email'))<small class="text-danger">{{ $errors->first('email') }}</small>@endif                                            
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Brief Description</label>
                                            <textarea cols="30" rows="4" class="md-input" name="brief_description">{{ isset($hotel->brief_description) ? $hotel->brief_description : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label>Hotel Policies</label>
                                            <textarea cols="30" rows="4" class="md-input" name="hotel_policies">{{ isset($hotel->hotel_policies) ? $hotel->hotel_policies : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 p-r-50">
                                            <label for="Facilities" class="uk-form-label">Facilities</label>
                                            <div>
                                                <?php
                                                if (count($facilities) > 0) {
                                                    $facilities_arr = isset($hotel->facilities) ? explode(',', $hotel->facilities) : array();
                                                    foreach ($facilities as $facilitie) {
                                                        ?>
                                                        <span class="icheck-inline">
                                                            <input type="checkbox" name="facilities[]" value="{{ $facilitie->facId }}" id="facilities_{{ $facilitie->facId }}" data-md-icheck {{ (isset($hotel->facilities) && in_array($facilitie->facId,$facilities_arr)) ? "checked" : ""  }} />
                                                                   <label for="facilities_{{ $facilitie->facId }}" class="inline-label">{{ $facilitie->name }}</label>
                                                        </span>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-2-6">
                                            <h3 class="heading_a uk-margin-small-bottom">Hotel Logo</h3>
                                            <input type="file" id="input-file-a" name="hotel_logo" class="dropify" data-default-file="{{  isset($hotel->hotel_logo) ? \App\Helpers\S3url::s3_get_image($hotel->hotel_logo) : '' }}" />
                                        </div>
                                        <div class="uk-width-medium-2-3">
                                            <h3 class="heading_a uk-margin-small-bottom">Photos</h3>
                                            <div class="dropzone fileuploader" id="dropzoneFileUpload">

                                            </div>	
                                            <!--                                            <div class="fallback">
                                                                                            <input type="file" name="file" multiple>
                                                                                        </div>-->
                                            <!--Dropzone Preview Template-->
                                            <div id="preview" style="display: none;">

                                                <div class="dz-preview dz-file-preview">
                                                    <div class="dz-image"><img data-dz-thumbnail /></div>

                                                    <div class="dz-details">
                                                        <div class="dz-size"><span data-dz-size></span></div>
                                                        <div class="dz-filename"><span data-dz-name></span></div>
                                                    </div>
                                                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                                    <div class="dz-error-message"><span data-dz-errormessage></span></div>



                                                    <div class="dz-success-mark">

                                                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                                        <title>Check</title>
                                                        <desc>Created with Sketch.</desc>
                                                        <defs></defs>
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                        </g>
                                                        </svg>

                                                    </div>
                                                    <div class="dz-error-mark">

                                                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                        <title>error</title>
                                                        <desc>Created with Sketch.</desc>
                                                        <defs></defs>
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                                                        <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                        </g>
                                                        </g>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <div class="status-column">
                                                <span class="icheck-inline">
                                                    <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($hotel->status) && $hotel->status == 1) ? 'checked' : ''  }} {{ (!isset($hotel)) ? 'checked' : '' }} />
                                                           <label for="active" class="inline-label">Active</label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($hotel->status) && $hotel->status == 0) ? 'checked' : ''  }} />
                                                           <label for="inactive" class="inline-label">In Active</label>
                                                </span>
                                            </div>
                                            @if($errors->has('status'))<small class="text-danger">{{ $errors->first('status') }}</small>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('hotel.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/get_regions.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/dropzone.js"></script>
<script type="text/javascript">
$(function () {
// It has the name attribute "currency_frm"
    $("form#hotel_frm").validate({
// Specify validation rules
        rules: {
            name: "required",
            country_id: "required",
            state_id: "required",
            city_id: "required",
            pincode: "required",
            mobile: "required",
            email: "required",
            check_in_time: "required",
            check_out_time: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            name: "Please enter hotel name",
            country_id: "Please select country",
            state_id: "Please select state",
            city_id: "Please select city",
            pincode: "Please enter pin code ",
            mobile: "Please enter phone number",
            email: "Please enter email",
            check_in_time: "Please select check-in time",
            check_out_time: "Please select check-out time",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

});

</script>
<script type="text/javascript">
    var total_photos_counter = 0;
    var name = "";
    var baseUrl = "{{ route('hotel.images.store') }}";
    var token = "{{ csrf_token() }}";
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#dropzoneFileUpload", {
        url: baseUrl,
        params: {
            _token: token,
            hotel_id: $('#hotel_id').val(),
            hotel_temp_id: $('#hotel_temp_id').val()
        },
        uploadMultiple: true,
        parallelUploads: 2,
        maxFilesize: 16,
        previewTemplate: document.querySelector('#preview').innerHTML,
        addRemoveLinks: true,
        dictRemoveFile: 'Remove file',
        dictFileTooBig: 'Image is larger than 16MB',
        timeout: 10000,
        renameFile: function (file) {
            customName = new Date().getTime() + Math.floor((Math.random() * 100) + 1) + '_' + file.name;
            return customName;
        },
        init: function () {
            var myDropzone = this;
                    @if (Request::route('id'))
            $.get("{{ route('hotel.images.fetch', Request::route('id')) }}", function (data) {
                $.each(data.images, function (key, value) {
                    var file = {name: value.original, size: value.size};
                    myDropzone.options.addedfile.call(myDropzone, file);
                    myDropzone.options.thumbnail.call(myDropzone, file, value.server);
                    myDropzone.emit("complete", file);
                    total_photos_counter++;
                    $("#counter").text("# " + total_photos_counter);
                    file["customName"] = value.original;
                });
            });
            @endif
                    this.on("removedfile", function (file) {
                    $.post({
                    url: "{{ route('hotel.images.destroy')}}",
                            data: {
                            _token: token,
                                    id: file.customName,
                                    hotel_temp_id: $('#hotel_temp_id').val()
                            },
                            dataType: 'json',
                            success: function (data) {
                            total_photos_counter--;
                                    $("#counter").text("# " + total_photos_counter);
                            }
                    });
                    });
            },
                    success: function (file, done) {
                    total_photos_counter++;
                    $("#counter").text("# " + total_photos_counter);
                    file["customName"] = customName;
                }
    });


</script>
@endsection