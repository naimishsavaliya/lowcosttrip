@extends('layouts.theme')

@section('content')

<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('hotel.index') }}">Hotel</a></li>
            <li><span>Room Details</span></li>
        </ul>
    </div>
    <style>
        .uk-sticky-placeholder .uk-tab{background: #1976D2;}
        .user_heading{ padding: 24px 24px 0px 24px;}
        .select2{ width: 100% !important;}
        .uk-tab > li.uk-active > a{color: #FFF;}
        .uk-tab > li.uk-active > a{border-bottom-color: #00071f;}
        .uk-dropdown{width: 200px !important;}
        .select2-container--open{z-index: 9999;}
        .uk-dropdown-shown{min-width: 260px !important;}
    </style>
    <div id="page_content">
        <div id="page_content_inner" class="p-0">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-1-1">
                    <div class="">
                        <div class="user_heading">
                            <div class="user_heading_menu hidden-print">
                                <div class="uk-display-inline-block" data-uk-dropdown="{pos:'left-top'}">
                                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="{{ env('ADMIN_URL')}}hotel/room_book/1"><i class="material-icons ">add</i> Room Book</a></li>
<!--                                            <li><a href="{{ env('ADMIN_URL')}}hotel/lead_room_buy/1"><i class="material-icons ">add</i> Hotel Room</a></li>
                                            <li><a href="#"><i class="material-icons ">add</i> Activity</a></li>
                                            <li><a href="#"><i class="material-icons ">add</i> Flight</a></li>
                                            <li><a href="javascript:;" data-uk-modal="{target:'#assign_group_tour'}" ><i class="material-icons ">add</i> Assign Group Tour</a></li>
                                            <li><a href="javascript:;" data-uk-modal="{target:'#assign_customised_tour'}" ><i class="material-icons ">add</i> Assign Customised Tour</a></li>
                                            <li><a href="javascript:;" data-uk-modal="{target:'#update_status'}" ><i class="material-icons ">add</i> Update Status</a></li>
                                            <li><a href="{{ env('ADMIN_URL')}}lead/notes/add"><i class="material-icons ">add</i> Followup</a></li>
                                            <li><a href="{{ env('ADMIN_URL')}}lead/notes/add"><i class="material-icons ">add</i> Document</a></li>-->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate">Deluxe Room</span><span class="sub-heading"> The Imperial Palace</span></h2>
                            </div>

                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">Room Info</a></li>
                                <li class="rates"><a href="#" >Rates</a></li>
                                <li class="booking"><a href="#" >Current Booking</a></li>
                                <li class="calendar"><a href="#" >Booking Calendar</a></li>
                                <li class="availability"><a href="#" >Availability</a></li>
                            </ul>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <form action="{{ env('ADMIN_URL')}}hotel/room" class="" method="get">
                                        <div class="p-t-30">
                                            <div class="uk-grid" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                                    <option value="" selected>Choose Hotel</option>
                                                                    <option value="1">Hotel 1</option>
                                                                    <option value="2">Hotel 2</option>
                                                                    <option value="3">Hotel 3</option>
                                                                </select>
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                                    <option value="" selected>Choose Room Type</option>
                                                                    <option value="1">Room Type 1</option>
                                                                    <option value="2">Room Type 2</option>
                                                                    <option value="3">Room Type 3</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label>Max Children</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <label>No.of rooms (in hotel)</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-1">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                                    <option value="" selected>Bed Type</option>
                                                                    <option value="1">Single Bed</option>
                                                                    <option value="2">Twin Bed</option>
                                                                    <option value="2">Bunk Bed</option>
                                                                    <option value="2">Small Double Bed</option>
                                                                    <option value="2">Double Bed</option>
                                                                    <option value="2">Queen Bed</option>
                                                                    <option value="2">King Bed</option>
                                                                </select>
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <select id="select_demo_5" data-md-selectize data-md-selectize-bottom multiple="" >
                                                                    <option value="" selected>Good For</option>
                                                                    <option value="1">Couples</option>
                                                                    <option value="2">Families</option>
                                                                    <option value="3">Friends</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                                    <option value="" selected>Number Of Beds</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
                                                                    <option value="" selected>Number Of BathRooms</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label>Room Area</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <label>Video Url</label>
                                                                <input type="text" class="md-input" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Short Description</label>
                                                                <textarea cols="30" rows="4" class="md-input"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Brief Description</label>
                                                                <textarea cols="30" rows="4" class="md-input"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label>Inclusion</label>
                                                                <textarea cols="30" rows="4" class="md-input"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1 p-r-50">
                                                                <label for="Facilities" class="uk-form-label">Facilities</label>
                                                                <div>
                                                                    <span class="icheck-inline">
                                                                        <input type="checkbox" name="facilities[]" id="facilities_1" data-md-icheck />
                                                                        <label for="facilities_1" class="inline-label">Board Room</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="checkbox" name="facilities[]" id="facilities_2" data-md-icheck />
                                                                        <label for="facilities_2" class="inline-label">Restaurant</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="checkbox" name="facilities[]" id="facilities_3" data-md-icheck />
                                                                        <label for="facilities_3" class="inline-label">Wi-Fi Internet</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="checkbox" name="facilities[]" id="facilities_4" data-md-icheck />
                                                                        <label for="facilities_4" class="inline-label">Room Service</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="checkbox" name="facilities[]" id="facilities_5" data-md-icheck />
                                                                        <label for="facilities_5" class="inline-label">Front desk</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="checkbox" name="facilities[]" id="facilities_6" data-md-icheck />
                                                                        <label for="facilities_6" class="inline-label">Doctor on Call</label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="uk-width-medium-1-2 uk-row-first">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <label class="control-label">Is Refundable</label>
                                                                <div>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio"  id="discount_type_0" data-md-icheck name="type" value="0" />
                                                                        <label for="discount_type_0" class="inline-label">Yes</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" id="discount_type_1" data-md-icheck name="type" value="1" />
                                                                        <label for="discount_type_1" class="inline-label">No</label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="uk-width-medium-1-2 p-l-50">
                                                                <label class="control-label">Pay at Checkout</label>
                                                                <div>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio"  id="discount_type_3" data-md-icheck name="type" value="0" />
                                                                        <label for="discount_type_3" class="inline-label">Yes</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" id="discount_type_4" data-md-icheck name="type" value="1" />
                                                                        <label for="discount_type_4" class="inline-label">No</label>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-1">
                                                                <h3 class="heading_a uk-margin-small-bottom">Room images</h3>
                                                                <div id="file_upload-drop" class="uk-file-upload">
                                                                    <p class="uk-text">Drop file to upload</p>
                                                                    <p class="uk-text-muted uk-text-small uk-margin-small-bottom">or</p>
                                                                    <a class="uk-form-file md-btn">choose file<input id="file_upload-select" type="file"></a>
                                                                </div>
                                                                <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                                                    <div class="uk-progress-bar" style="width:0">0%</div>
                                                                </div>	
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-1-12">
                                                    <div class="uk-form-row">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-2 p-r-50">
                                                                <div>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" name="status" value="1" id="status_1" data-md-icheck checked="" />
                                                                        <label for="status" class="inline-label">Active</label>
                                                                    </span>
                                                                    <span class="icheck-inline">
                                                                        <input type="radio" name="status" value="0" id="status_0" data-md-icheck  />
                                                                        <label for="status_0" class="inline-label">In Active</label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="form_hr">
                                        <div class="uk-grid uk-margin-medium-top uk-text-right">
                                            <div class="uk-width-1-1">
                                                <a href="{{ route('supplier.index') }}" class="md-btn md-btn-danger">Cancel</a>
                                                <button type="submit" class="md-btn md-btn-primary">Save</button>
                                            </div>
                                        </div>  
                                    </form>
                                </li>
                                <li>
                                    <form class="uk-form-stacked" id="wizard_advanced_form">
                                        <div data-dynamic-fields="d_field_wizard" class="uk-grid" data-uk-grid-margin></div>
                                    </form>
                                </li>
                                <li>
                                    <table class="uk-table dt_default">

                                        <thead>
                                            <tr>
                                                <th>Lead ID</th>
                                                <th>Customer</th>
                                                <th>Booking Date</th>
                                                <th>Occupancy Date</th>
                                                <th>No of Rooms</th>
                                                <th>Adult/Child/Infant</th>
                                                <th>Supplier</th>
                                                <th>Agent</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Lead Id" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Customer" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Booking Date" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Occupiency Date" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="No of Rooms" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Adult/Child/Infant" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Supplier" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="md-input-wrapper">
                                                        <input placeholder="Agent" type="text" class="md-input">
                                                        <span class="md-input-bar "></span>
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td><span data-uk-tooltip title="Kerala Tour <?php echo $i; ?>">LCT<?php echo rand(1111, 9999); ?></span></td>
                                                    <td>Customer Name</td>
                                                    <td>Mar 5, 2018</td>
                                                    <td>Nov 10, 2018</td>
                                                    <td>1</td>
                                                    <td><?php echo rand(2, 3); ?>/<?php echo rand(0, 2); ?>/<?php echo rand(0, 1); ?></td>
                                                    <td>Supplier 1</td>
                                                    <td>Agent 1</td>
                                                    <td>
                                                        <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                                            <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                                            <div class="uk-dropdown">
                                                                <ul class="uk-nav uk-nav-dropdown">
                                                                    <li><a href=""> Cancel Booking</a></li>
                                                                    <li><a href=""> Print Voucher</a></li>
                                                                    <li><a href=""> Email Voucher</a></li>
                                                                    <li><a href=""> Edit Booking</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <div id="calendar_selectable"></div>
                                </li>
                                <li class="inventoryCal">
                                    <style>
                                        li.inventoryCal table.avltable td{width: 35px !important; border-left: solid 1px #e0e0e0; border-top: solid 1px #e0e0e0;}
                                        li.inventoryCal table.avltable td input{width: 20px !important; border: none; color: #b3aaaa;}
                                        li.inventoryCal table.avltable td.dateselected {background-color: #29b729; color: #FFF;}
                                        li.inventoryCal tr:hover {
                                            background-color: #ffa;
                                        }

                                        li.inventoryCal td, th {
                                            position: relative;
                                        }
                                        li.inventoryCal td:hover::after,
                                        li.inventoryCal th:hover::after {
                                            content: "";
                                            position: absolute;
                                            background-color: #ffa;
                                            left: 0;
                                            top: -5000px;
                                            height: 10000px;
                                            width: 100%;
                                            z-index: -1;
                                        }
                                    </style>
                                    <?php
                                    $current_year = date('Y');
                                    $yearPass = $current_year;
                                    $dateId = 1;
                                    $room = array();
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 1, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 2, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 3, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 4, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 5, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 6, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 7, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 8, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 9, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 10, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 11, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);
                                    $room[] = array('id' => 86329, 'room_id' => 1577, 'y' => 2018, 'm' => 12, 'd1' => 10, 'd2' => 10, 'd3' => 10, 'd4' => 10, 'd5' => 10, 'd6' => 10, 'd7' => 10, 'd8' => 10, 'd9' => 10, 'd10' => 10, 'd11' => 10, 'd12' => 10, 'd13' => 10, 'd14' => 10, 'd15' => 10, 'd16' => 10, 'd17' => 10, 'd18' => 10, 'd19' => 10, 'd20' => 10, 'd21' => 10, 'd22' => 10, 'd23' => 10, 'd24' => 10, 'd25' => 10, 'd26' => 10, 'd27' => 10, 'd28' => 10, 'd29' => 10, 'd30' => 10, 'd31' => 10);

                                    if ($yearPass < $current_year) {
                                        $disable = "disabled=disabled";
                                    }

                                    function GetMonthMaxDay($year, $month) {
                                        if (empty($month))
                                            $month = date('m');
                                        if (empty($year))
                                            $year = date('Y');
                                        $result = strtotime("{$year}-{$month}-01");
                                        $result = strtotime('-1 second', strtotime('+1 month', $result));
                                        return date('d', $result);
                                    }

                                    $nl = "\n";

                                    $lang['weeks'][0] = 'Su';
                                    $lang['weeks'][1] = 'Mo';
                                    $lang['weeks'][2] = 'Tu';
                                    $lang['weeks'][3] = 'We';
                                    $lang['weeks'][4] = 'Th';
                                    $lang['weeks'][5] = 'Fr';
                                    $lang['weeks'][6] = 'Sa';

                                    $lang['months'][1] = 'January';
                                    $lang['months'][2] = 'February';
                                    $lang['months'][3] = 'March';
                                    $lang['months'][4] = 'April';
                                    $lang['months'][5] = 'May';
                                    $lang['months'][6] = 'June';
                                    $lang['months'][7] = 'July';
                                    $lang['months'][8] = 'August';
                                    $lang['months'][9] = 'September';
                                    $lang['months'][10] = 'October';
                                    $lang['months'][11] = 'November';
                                    $lang['months'][12] = 'December';


                                    $year = $yearPass;
                                    $ids_list = '';
                                    $max_days = 0;
                                    $output = '';
                                    $output_week_days = '';
                                    $current_month = date('m');

                                    $selected_year = $year;

                                    $room_count = 1;

                                    $output .= '<form id="basicvalidations" action="/Managehotelrooms/UpdateAvailability" method="post">';
                                    $output .= '<input type="hidden" id="task" name="task" value="update">';
                                    $output .= '<input type="hidden" id="changeValue" name="changeValue" value="">';
                                    $output .= '<input type="hidden" id="comment" name="comment" value="">';
                                    $output .= '<input type="hidden" id="year" name="year" value="' . $yearPass . '">';
                                    $output .= '<div class="col-md-12">'
                                            . '<div class="portlet light portlet-fit portlet-form ">'
                                            . '<div class="portlet-body">'
                                            . '<div class="form-body">';
                                    $output .= '<table cellpadding="0" cellspacing="0" border="0" class="avltable">';
//                                        $output .= '<tr>';
//                                        $output .= '<td align="left" colspan="27">
//					</td>
//					
//					
//					<td align="right" colspan="0">
//						
//					</td>';
//                                        $output .= '</tr>';
//                                        $output .= '<tr><td colspan="39">&nbsp;</td></tr>';

                                    $count = 0;
                                    $week_day = date('w', mktime('0', '0', '0', '1', '1', $selected_year));
// fill empty cells from the beginning of month line
                                    while ($count < $week_day) {
                                        $td_class = (($count == 0 || $count == 6) ? 'day_td_w' : ''); // 0 - 'Sun', 6 - 'Sat'
                                        $output_week_days .= '<td class="' . $td_class . '">' . $lang['weeks'][$count] . '</td>';
                                        $count++;
                                    }
// fill cells at the middle
                                    for ($day = 1; $day <= 31; $day ++) {
                                        $week_day = date('w', mktime('0', '0', '0', '1', $day, $selected_year));
                                        $td_class = (($week_day == 0 || $week_day == 6) ? 'day_td_w' : ''); // 0 - 'Sun', 6 - 'Sat'
                                        $output_week_days .= '<td  class="' . $td_class . '">' . $lang['weeks'][$week_day] . '</td>';
                                    }
                                    $max_days = $count + 31;
// fill empty cells at the end of month line
                                    if ($max_days < 37) {
                                        $count = 0;
                                        while ($count < (37 - $max_days)) {
                                            $week_day++;
                                            $count++;
                                            $week_day_mod = $week_day % 7;
                                            $td_class = (($week_day_mod == 0 || $week_day_mod == 6) ? 'day_td_w' : ''); // 0 - 'Sun', 6 - 'Sat'
                                            $output_week_days .= '<td class="' . $td_class . '">' . $lang['weeks'][$week_day_mod] . '</td>';
                                        }
                                        $max_days += $count;
                                    }
                                    $nextY = $current_year + 1;
                                    $outputOPtions = "";
                                    for ($yer = 2015; $yer <= $nextY; $yer++) {
                                        $outputOPtions .= '<option value="' . $yer . '" ' . (($yer == $yearPass) ? 'selected="selected"' : '') . '>' . $yer . '</option>';
                                    }
// draw week days
                                    $output .= '<tr style="text-align:center;background-color:#cccccc;">';
                                    $output .= '<td style="text-align:left;background-color:#ffffff; border:none;">';
                                    $output .= '<select name="selYear" onchange="loadData(\'/Managehotelrooms/roomavailability/\'+this.value)">';
                                    $output .= $outputOPtions;
                                    //$output .= '<option value="' . $nextY . '" ' . (($year == 'next') ? 'selected="selected"' : '') . '>' . ($current_year + 1) . '</option>';
                                    $output .= '</select>';
                                    $output .= '</td>';
//                                        $output .= '<td align="center" style="padding:0px 4px;background-color:#ffffff;"><img src="/app/webroot/images/check_all.gif" alt="" /></td>';
                                    $output .= $output_week_days;
                                    $output .= '</tr>';


                                    for ($i = 0; $i < 12; $i++) {
                                        $selected_month = $room[$i]['m'];
                                        if ($selected_month == $current_month)
                                            $tr_class = 'm_current';
                                        else
                                            $tr_class = (($i % 2 == 0) ? 'm_odd' : 'm_even');

                                        $output .= '<tr align="center" class="' . $tr_class . '">';
                                        $output .= '<td align="left">&nbsp;<b>' . $lang['months'][$selected_month] . '</b></td>';
//                                            $output .= '<td><input type="checkbox" class="form_checkbox" onclick="toggleAvailability(this.checked,\'' . $room[$i]['id'] . '\',\'' . $room_count . '\')" /></td>';

                                        $max_day = GetMonthMaxDay($selected_year, $selected_month);

                                        // fill empty cells from the beginning of month line
                                        $count = date('w', mktime('0', '0', '0', $selected_month, 1, $selected_year));
                                        $max_days -= $count; /* subtract days that were missed from the beginning of the month */
                                        while ($count--)
                                            $output .= '<td></td>';
                                        // fill cells at the middle
                                        for ($day = 1; $day <= $max_day; $day ++) {
                                            if ($room[$i]['d' . $day] >= $room_count) {
                                                $day_color = 'dc_all';
                                            } else if ($room[$i]['d' . $day] > 0 && $room[$i]['d' . $day] < $room_count) {
                                                $day_color = 'dc_part';
                                            } else {
                                                $day_color = 'dc_none';
                                            }
                                            $week_day = date('w', mktime('0', '0', '0', $selected_month, $day, $selected_year));
                                            $dateBook = date('Y-m-d', mktime('0', '0', '0', $selected_month, $day, $selected_year));
                                            $time = mktime('0', '0', '0', $selected_month, $day, $selected_year);

                                            $noOfBook = array();


                                            $title = '';
                                            if (!empty($noOfBook)) {

                                                $totalRoom = 0;
                                                $title = '';
                                                foreach ($noOfBook as $noOfBookA) {

                                                    $customer_name = $this->TbHotelbooking->getBookingData($noOfBookA['TbHotelsroomsrecord']['customer_hotel_id']);
                                                    $roomBV = $noOfBookA[0]['roomB'];
                                                    if ($roomBV < 0) {
                                                        $roomN = ($roomBV) * (-1);
                                                    } else {
                                                        $roomN = 0;
                                                    }
                                                    if (isset($noOfBookA['TbHotelsroomsrecord']['tour_departure']) && !empty($noOfBookA['TbHotelsroomsrecord']['tour_departure'])) {
                                                        $tourInfo = $this->TbTourdep->getTourInfo($noOfBookA['TbHotelsroomsrecord']['tour_departure']);
// if(strtotime($dateBook) ==  strtotime('2016-12-29')){
//     echo $noOfBookA['TbHotelsroomsrecord']['tour_departure'];
//                            echo "<pre>";
//                            print_r($noOfBook);
//                            //print_r($tourInfo);
//                            echo "</pre>";
//                            }
                                                        $deptDate = date("d-m-Y", strtotime($tourInfo[0]['TbTourdeparturedatemaster']['start_date']));
                                                        $tourCode = $tourInfo[0]['tourmaster']['tour_code'];
                                                        if ($roomN != 0 && strtotime($deptDate) <= strtotime($dateBook)) {
                                                            if (isset($tourCode) && !empty($tourCode)) {
                                                                $title .= "Tour Code: $tourCode, Dept Date: $deptDate,Room booked:$roomN &#13;";
                                                            }
                                                        }
                                                    } elseif ($noOfBookA['TbHotelsroomsrecord']['isFIT'] == 'Y' && $noOfBookA['TbHotelsroomsrecord']['revert_booking'] == 'N') {
                                                        $title .= "FIT -Room Booked:" . $roomN . ",FIT-Name: " . $customer_name['TbHotelbooking']['vouchername'] . " &#13;";
                                                    } else {
                                                        $title .= "No room booked yet";
                                                    }
                                                }
                                            } else {

                                                $title .= "No room booked yet";
                                            }

                                            $td_class = (($week_day == 0 || $week_day == 6) ? 'day_td_w' : 'day_td'); // 0 - 'Sun', 6 - 'Sat'
                                            $output .= '<td dateid="' . $dateId . '" time="' . $time . '"  class="' . $td_class . '"><label class="l_day">' . $day . '</label><br><input title="' . $title . '" class="day_a ' . $day_color . '" maxlength="3" name="aval_' . $room[$i]['id'] . '_' . $day . '" id="aval_' . $room[$i]['id'] . '_' . $day . '" value="' . $room[$i]['d' . $day] . '" /></td>';
                                            $dateId++;
                                        }
                                        // fill empty cells at the end of the month line
                                        while ($day <= $max_days) {
                                            $output .= '<td></td>';
                                            $day++;
                                        }
                                        $output .= '</tr>';
                                        if ($ids_list != '')
                                            $ids_list .= ',' . $room[$i]['id'];
                                        else
                                            $ids_list = $room[$i]['id'];
                                    }

//                                    $output .= '<tr><td colspan="39">&nbsp;</td></tr>';
//                                    $output .= '<tr><td align="right" colspan="39"><button type="submit"  class="btn btn-success">Save</button></td></tr>';
//                                    $output .= '<tr><td colspan="39" style="float: left;"><b>Legend:</b> </td></tr>';
//                                    $output .= '<tr><td colspan="39" nowrap="nowrap" height="5px"></td></tr>';
//                                    $output .= '<tr><td colspan="39" style="float: left;"><div class="dc_all" style="height:15px;float:left;margin:1px;"></div> &nbsp;- Available</td></tr>';
//                                    $output .= '<tr><td colspan="39" style="float: left;"><div class="dc_part" style="height:15px;float:left;margin:1px;"></div> &nbsp;- Partially Available</td></tr>';
//                                    $output .= '<tr><td colspan="39" style="float: left;"><div class="dc_none" style="height:15px;float:left;margin:1px;"></div> &nbsp;- Not Available</td></tr>';
                                    $output .= '</table>';
                                    $output .= '</table>';
                                    $output .= '<input type="hidden" name="ids_list" value="' . $ids_list . '">';
                                    $output .= '</div></div></div></div>';
                                    $output .= '</form>';

                                    echo $output;
//}
                                    ?>

                                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                                        <div class="uk-width-1-1">
                                            <a href="javascript:;" data-uk-modal="{target:'#update_inventory'}"  class="md-btn md-btn-primary">Update Inventory</a>
                                        </div>
                                    </div>

                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>

<div class="uk-modal uk-modal-card-fullscreen" id="update_inventory">
    <div class="uk-modal-dialog uk-modal-dialog-blank">
        <div class="md-card uk-height-viewport">
            <div class="md-card-toolbar">
                <span class="md-icon material-icons uk-modal-close">&#xE5C4;</span>
                <h3 class="md-card-toolbar-heading-text">
                    Update Inventory & Rate
                </h3>
            </div>
            <div class="md-card-content">
                <h2>Inventory</h2>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper md-input-filled"><label>Start Date</label><input type="text" value="01-07-2018" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper md-input-filled"><label>End Date</label><input type="text" value="25-07-2018" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                        <div class="uk-width-medium-1-3 uk-row-first">
                            <div class="md-input-wrapper"><label>New Inventory</label><input type="text" value="" class="md-input"><span class="md-input-bar "></span></div>
                        </div>
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-1 ">
                            <div class="md-input-wrapper md-input-filled"><label>Remark</label>
                                <textarea class="md-input no_autosize" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <h2>Room Rate</h2>
                <hr class="form_hr">

                <div data-dynamic-fields="d_room_inventory_update" class="uk-grid" data-uk-grid-margin></div>
                <hr class="form_hr">
                <div class="uk-grid uk-margin-medium-top uk-text-right">
                    <div class="uk-width-1-1">
                        <button type="button" class="md-btn md-btn-danger uk-modal-close">Cancel</button>
                        <button type="submit" class="md-btn md-btn-primary uk-modal-close">Save</button>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                </div>
            </div>

        </div>
    </div>
</div>
<script id="d_room_inventory_update" type="text/x-handlebars-template">
    <div class="uk-width-medium-1-1 parsley-row form_section">
    <div class="uk-input-group uk-grid" style="width:100%">
    <div class="uk-width-1-1">
    <div class="">
    <div class="uk-grid" data-uk-grid-margin="">
    <div class="uk-width-medium-1-4 uk-width-1-1 uk-row-first">
    <label for="invoice_dp">Date From</label>
    <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="invoice_dp">Date To</label>
    <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="discount">Customer Discount %</label>
    <input class="md-input" type="text" id="discount" value="">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="discount">Agent Discount %</label>
    <input class="md-input" type="text" id="discount" value="">
    </div>
    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div>
    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div> <br/>


    <div class="uk-width-medium-1-1">
    <table class="uk-table ">
    <tbody>
    <tr>
    <td style="width: 15%;">
    <div class="md-input-wrapper">
    <label> </label>
    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
    <option value="1">Room Only</option>
    <option value="2">Breakfast</option>
    <option value="3">Breakfast + 1 Meal</option>
    <option value="4">Breakfast + 2 Meal</option>
    <option value="5">All Including</option>
    </select>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Common</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Su</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Mo</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Tu</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>We</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Th</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Fr</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Sa</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    </tr>

    </tbody>
    </table>
    </div>

    <hr/>


    <div class="uk-width-medium-1-10">
    <span class="uk-input-group-addon">
    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
    </span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</script>
<script id="d_field_wizard" type="text/x-handlebars-template">
    <div class="uk-width-medium-1-1 parsley-row form_section">
    <div class="uk-input-group uk-grid" style="width:100%">
    <div class="uk-width-1-1">
    <div class="">
    <div class="uk-grid" data-uk-grid-margin="">
    <div class="uk-width-medium-1-4 uk-width-1-1 uk-row-first">
    <label for="invoice_dp">Date From</label>
    <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="invoice_dp">Date To</label>
    <input class="md-input" type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="discount">Customer Discount %</label>
    <input class="md-input" type="text" id="discount" value="">
    </div>
    <div class="uk-width-medium-1-4 uk-width-medium-1-1">
    <label for="discount">Agent Discount %</label>
    <input class="md-input" type="text" id="discount" value="">
    </div>
    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div>
    <div class="uk-clear uk-clearfix" style="clear: both; width: 100%;"></div> <br/>


    <div class="uk-width-medium-1-1">
    <table class="uk-table ">
    <tbody>
    <tr>
    <td style="width: 15%;">
    <div class="md-input-wrapper">
    <label> </label>
    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom >
    <option value="1">Room Only</option>
    <option value="2">Breakfast</option>
    <option value="3">Breakfast + 1 Meal</option>
    <option value="4">Breakfast + 2 Meal</option>
    <option value="5">All Including</option>
    </select>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Common</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Su</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Mo</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Tu</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>We</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Th</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Fr</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    <td>
    <div class="md-input-wrapper">
    <label>Sa</label>
    <input type="text" class="md-input"><span class="md-input-bar "></span>
    </div>
    </td>
    </tr>

    </tbody>
    </table>
    </div>

    <hr/>


    <div class="uk-width-medium-1-10">
    <span class="uk-input-group-addon">
    <a href="#" class="btnSectionClone"><i class="material-icons md-24">&#xE146;</i></a>
    </span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</script>
@endsection
@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/handlebars/handlebars.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/custom/handlebars_helpers.min.js"></script>

<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/fullcalendar/dist/fullcalendar.min.css">
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/plugins_fullcalendar.js"></script>

<script type="text/javascript">
$(function () {
    if (location.hash && location.hash.length) {
        var hash = decodeURIComponent(location.hash.substr(1));
        setTimeout(
                function ()
                {
                    $("ul#user_profile_tabs li." + hash).trigger("click");
//                        alert(hash);
                }, 1000);
    }
});
$(function () {
    var selectStartDate = "";
    var startDateId = "";
    var endDateId = "";
    $("table.avltable td").click(function () {
        if ($("table.avltable td").hasClass("endDate")) {
            $("table.avltable td").removeClass("startDate");
            $("table.avltable td").removeClass("endDate");
            $("table.avltable td").removeClass("dateselected");
        }
        if ($("table.avltable td").hasClass("startDate")) {
            var selectedDateId = $(this).attr('dateid');
            if (selectedDateId > startDateId) {
                for (i = startDateId; i < selectedDateId; i++) {
                    $('td[dateid="' + i + '"]').addClass("dateselected");
                }
                $(this).addClass("endDate dateselected");
            }
//                alert("end");
        } else {
            $(this).addClass("startDate dateselected");
            startDateId = $(this).attr('dateid');
//                alert("start");

        }
    });
});
</script>

@endsection