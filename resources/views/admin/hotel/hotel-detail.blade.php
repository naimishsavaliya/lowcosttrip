@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Hotel Details</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ env('ADMIN_URL')}}hotel/add" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Hotel
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <div class="uk-grid blog_article">
                <div class="uk-width-medium-1-12">
                    <h1 class="uk-article-title">
                        The Gateway Hotel 
                    </h1>
                    <p class="uk-article-meta p-b-20">
                        Vadodara 
                    </p>	
                </div>
                <div class="uk-width-medium-7-10">
                    <div>
                        <div class="large-padding">
                            <div class="uk-article">
                                <div class="uk-slidenav-position" data-uk-slideshow="{animation:'scale'}">
                                    <ul class="uk-slideshow">
                                        <li><img src="http://peru-tripadvisors.com/blog/wp-content/uploads/2013/01/Tambo-del-Inka-624x376.jpg" alt=""></li>
                                        <li><img src="https://i1.wp.com/www.jugendzimmer.bid/wp-content/uploads/2018/05/therme-erding-e280a2-hotel-apfelbaum-saunaparadies-erding-2018.jpg?w=740&ssl=1" alt=""></li>
                                        <li><img src="http://img.mp.itc.cn/upload/20161214/ebb1b43883004646b03c030e88048b1f_th.jpeg" alt=""></li>
                                        <li><img src="http://www.hotel-r.net/im/hotel/lv/malpils-manor-12.jpg" alt=""></li>
                                    </ul>
                                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                                    <ul class="hotel-img-slide uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
                                        <li data-uk-slideshow-item="0"><a href=""></a></li>
                                        <li data-uk-slideshow-item="1"><a href=""></a></li>
                                        <li data-uk-slideshow-item="2"><a href=""></a></li>
                                        <li data-uk-slideshow-item="3"><a href=""></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-3-10 p-l-0 bg-pink">
                    <div class="md-card md-box bg-pink brd-radius-0" style="padding-bottom: 2px;">
                        <div class="md-card-content large-padding">
                            <div class="pricing_table_plan fac-star-wrapper">
                                <i class="material-icons icon-star">&#xE838;</i>
                                <i class="material-icons icon-star">&#xE838;</i>
                                <i class="material-icons icon-star">&#xE838;</i>
                                <i class="material-icons icon-star">&#xE838;</i>
                                <i class="material-icons icon-star">&#xE838;</i>
                            </div>
                            <div style="padding:30px 0 20px 0;">
                                <div class="pricing_table_price position-relative" style="text-align:center;">
                                    <span class="currency text-white"><i class="icon-rupee"></i>2000 
                                        <span style="left: calc(50% - 20px);" class="period crossed-line old-price text-white">
                                            <i class="icon-rupee"></i> 3000
                                        </span>
                                    </span>
                                    <div class="period text-white">Per Night on Twin Sharing Basis</div>
                                </div>

                                <div class="line p-t-16" style="text-align:center;">
                                    <a class="md-btn md-btn-default btn-book-room md-btn-large md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book Room</a>
                                </div>
                                <div class="pricing_table_price position-relative p-t-16" style="text-align:center;">
                                    <div class="period text-white">3 room options</div>
                                </div>
                            </div>
                            <h5 class="uk-margin-small-bottom uk-text-upper text-white text-center">Hotel Facilities</h5>
                            <ul class="md-list md-list-addon uk-margin-remove hotel-facilities-ul text-center">
                                <li>
                                    <i class="uk-icon-spoon"></i>
                                </li>
                                <li>
                                    <i class="uk-icon-bed"></i>
                                </li>
                                <li>
                                    <i class="uk-icon-car"></i>
                                </li>
                                <li>
                                    <i class="uk-icon-wifi"></i>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-12">
                <div>
                    <div class="large-padding">
                        <p class="line-height-2 p-t-16 text-justify">
                            Illo voluptatum et blanditiis quasi ea provident id saepe amet aut et iste commodi accusantium in repellat aut laudantium ex est ullam et quaerat eos omnis accusantium ad omnis quia est sint ullam molestiae eum  est nobis voluptas recusandae fugit quaerat ut neque ipsam ipsa omnis velit magni placeat maxime atque sit dicta illo dolores quos perferendis porro perspiciatis explicabo facilis sequi nihil neque fugit magnam ut magni eum eos voluptates quasi accusantium inventore sed soluta voluptatum alias repellat 
                        </p>
                        <ul class="search_list room-detail-list">
                            <li>
                                <div class="md-card md-box">
                                    <div class="md-card-content">
                                        <h3 class="search_list_heading">
                                            <a href="{{ env('ADMIN_URL')}}hotel/room_book/1">Delux Room</a>
                                        </h3>
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-4-10">
                                                <a href="{{ env('ADMIN_URL')}}hotel/room_book/1" style="display:block">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image.jpg" alt="" class="search_list_thumb">
                                                </a>
                                                <div class="view-link-wrapper">
                                                    <a href="{{ env('ADMIN_URL')}}hotel/room_book/1">View Photo</a> | 
                                                    <a href="{{ env('ADMIN_URL')}}hotel/room_book/1">View Amenities</a>
                                                </div>
                                            </div>
                                            <div class="uk-width-medium-6-10">

                                                <div class="line" data-uk-grid-margin>
                                                    <div class="uk-width-medium-4-10 f-left" style="padding-left:0px;">
                                                        <h3 class="search_list_sub_heading"><a href="{{ env('ADMIN_URL')}}hotel/room_book/1">Only Stay</a></h3>
                                                    </div>
                                                    <div class="uk-width-medium-6-10 f-left" style="padding-left:0px;">
                                                        <div class="room_option_price">
                                                            <div class="display-inline-block v-middle">
                                                                <div>
                                                                    <span class="currency position-relative text-primary">
                                                                        <i class="icon-rupee"></i>2000
                                                                    </span>
                                                                </div>
                                                                <div>
                                                                    <span class="period">Per Night on Twin Sharing</span>
                                                                </div>
                                                            </div>
                                                            <div class="display-inline-block v-middle" style="margin-left:10px;">
                                                                <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book this</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="uk-article-divider m-t-b-10">

                                                <div class="line" data-uk-grid-margin>
                                                    <div class="uk-width-medium-4-10 f-left" style="padding-left:0px;">
                                                        <h3 class="search_list_sub_heading"><a href="{{ env('ADMIN_URL')}}hotel/room_book/1">Breakfast Included</a></h3>
                                                    </div>
                                                    <div class="uk-width-medium-6-10 f-left" style="padding-left:0px;">
                                                        <div class="room_option_price">
                                                            <div class="display-inline-block v-middle">
                                                                <div>
                                                                    <span class="currency position-relative text-primary">
                                                                        <i class="icon-rupee"></i>2000
                                                                    </span>
                                                                </div>
                                                                <div>
                                                                    <span class="period">Per Night on Twin Sharing</span>
                                                                </div>
                                                            </div>
                                                            <div class="display-inline-block v-middle" style="margin-left:10px;">
                                                                <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book this</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="uk-article-divider m-t-b-10">

                                                <div class="line" data-uk-grid-margin>
                                                    <div class="uk-width-medium-4-10 f-left" style="padding-left:0px;">
                                                        <h3 class="search_list_sub_heading"><a href="{{ env('ADMIN_URL')}}hotel/room_book/1">Breakfast and lunch/dinner</a></h3>
                                                    </div>
                                                    <div class="uk-width-medium-6-10 f-left" style="padding-left:0px;">
                                                        <div class="room_option_price">
                                                            <div class="display-inline-block v-middle">
                                                                <div>
                                                                    <span class="currency position-relative text-primary">
                                                                        <i class="icon-rupee"></i>2000
                                                                    </span>
                                                                </div>
                                                                <div>
                                                                    <span class="period">Per Night on Twin Sharing</span>
                                                                </div>
                                                            </div>
                                                            <div class="display-inline-block v-middle" style="margin-left:10px;">
                                                                <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book this</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="uk-article-divider m-t-b-10">

                                                <div class="line" data-uk-grid-margin>
                                                    <div class="uk-width-medium-4-10 f-left" style="padding-left:0px;">
                                                        <h3 class="search_list_sub_heading"><a href="{{ env('ADMIN_URL')}}hotel/room_book/1">All Inclusive</a></h3>
                                                    </div>
                                                    <div class="uk-width-medium-6-10 f-left" style="padding-left:0px;">
                                                        <div class="room_option_price">
                                                            <div class="display-inline-block v-middle">
                                                                <div>
                                                                    <span class="currency position-relative text-primary">
                                                                        <i class="icon-rupee"></i>2000
                                                                    </span>
                                                                </div>
                                                                <div>
                                                                    <span class="period">Per Night on Twin Sharing</span>
                                                                </div>
                                                            </div>
                                                            <div class="display-inline-block v-middle" style="margin-left:10px;">
                                                                <a class="md-btn md-btn-primary md-btn-wave-light" href="{{ env('ADMIN_URL')}}hotel/room_book/1">Book this</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin="">
                <div class="uk-width-large-1-4">
                    <h3 class="heading_a uk-margin-bottom">Facilities</h3>
                    <div>
                        <ul class="md-list md-list-addon gmap_list" id="map_users_list">
                            <li data-gmap-lat="37.406267"  data-gmap-lon="-122.06742" data-gmap-user="Weldon Krajcik Sr." data-gmap-user-company="Treutel Group">
                                <div class="md-list-addon-element">
                                    <i class="material-icons">beach_access</i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Beach </span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.379267"  data-gmap-lon="-122.02148" data-gmap-user="Liam O'Connell" data-gmap-user-company="Smith, Torphy and D'Amore">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-bell"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Temple</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.410267"  data-gmap-lon="-122.11048" data-gmap-user="Arne Jacobs" data-gmap-user-company="Padberg-Cruickshank">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-motorcycle"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Fuel Station</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.397267"  data-gmap-lon="-122.084417" data-gmap-user="Ashlee Vandervort" data-gmap-user-company="Rippin-Glover">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-cart-plus"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Shopping Mall</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.372267"  data-gmap-lon="-122.090417" data-gmap-user="Dolly Kshlerin" data-gmap-user-company="Gerhold Inc">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-shield"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Police Station</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="uk-width-large-1-4">
                    <h3 class="heading_a uk-margin-bottom">&nbsp;</h3>
                    <div>
                        <ul class="md-list md-list-addon gmap_list" id="map_users_list">
                            <li data-gmap-lat="37.406267"  data-gmap-lon="-122.06742" data-gmap-user="Weldon Krajcik Sr." data-gmap-user-company="Treutel Group">
                                <div class="md-list-addon-element">
                                    <i class="material-icons">beach_access</i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Beach </span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.379267"  data-gmap-lon="-122.02148" data-gmap-user="Liam O'Connell" data-gmap-user-company="Smith, Torphy and D'Amore">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-bell"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Temple</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.410267"  data-gmap-lon="-122.11048" data-gmap-user="Arne Jacobs" data-gmap-user-company="Padberg-Cruickshank">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-motorcycle"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Fuel Station</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.397267"  data-gmap-lon="-122.084417" data-gmap-user="Ashlee Vandervort" data-gmap-user-company="Rippin-Glover">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-cart-plus"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Shopping Mall</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.372267"  data-gmap-lon="-122.090417" data-gmap-user="Dolly Kshlerin" data-gmap-user-company="Gerhold Inc">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-shield"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Police Station</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="uk-width-large-1-4">
                    <h3 class="heading_a uk-margin-bottom">&nbsp;</h3>
                    <div>
                        <ul class="md-list md-list-addon gmap_list" id="map_users_list">
                            <li data-gmap-lat="37.406267"  data-gmap-lon="-122.06742" data-gmap-user="Weldon Krajcik Sr." data-gmap-user-company="Treutel Group">
                                <div class="md-list-addon-element">
                                    <i class="material-icons">beach_access</i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Beach </span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.379267"  data-gmap-lon="-122.02148" data-gmap-user="Liam O'Connell" data-gmap-user-company="Smith, Torphy and D'Amore">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-bell"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Temple</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.410267"  data-gmap-lon="-122.11048" data-gmap-user="Arne Jacobs" data-gmap-user-company="Padberg-Cruickshank">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-motorcycle"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Fuel Station</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.397267"  data-gmap-lon="-122.084417" data-gmap-user="Ashlee Vandervort" data-gmap-user-company="Rippin-Glover">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-cart-plus"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Shopping Mall</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.372267"  data-gmap-lon="-122.090417" data-gmap-user="Dolly Kshlerin" data-gmap-user-company="Gerhold Inc">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-shield"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Police Station</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="uk-width-large-1-4">
                    <h3 class="heading_a uk-margin-bottom">&nbsp;</h3>
                    <div>
                        <ul class="md-list md-list-addon gmap_list" id="map_users_list">
                            <li data-gmap-lat="37.406267"  data-gmap-lon="-122.06742" data-gmap-user="Weldon Krajcik Sr." data-gmap-user-company="Treutel Group">
                                <div class="md-list-addon-element">
                                    <i class="material-icons">beach_access</i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Beach </span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.379267"  data-gmap-lon="-122.02148" data-gmap-user="Liam O'Connell" data-gmap-user-company="Smith, Torphy and D'Amore">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-bell"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Temple</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.410267"  data-gmap-lon="-122.11048" data-gmap-user="Arne Jacobs" data-gmap-user-company="Padberg-Cruickshank">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-motorcycle"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Fuel Station</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.397267"  data-gmap-lon="-122.084417" data-gmap-user="Ashlee Vandervort" data-gmap-user-company="Rippin-Glover">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-cart-plus"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Shopping Mall</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.372267"  data-gmap-lon="-122.090417" data-gmap-user="Dolly Kshlerin" data-gmap-user-company="Gerhold Inc">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-shield"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Police Station</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin="">
                <div class="uk-width-large-1-2">
                    <h3 class="heading_a uk-margin-bottom">Hotel location on map</h3>
                    <div class="md-card">
                        <div id="map_users_controls"></div>
                        <div id="map_users" class="gmap"></div>
                    </div>
                </div>
                <div class="uk-width-large-1-2">
                    <h3 class="heading_a uk-margin-bottom">Near by landmarks</h3>
                    <div>
                        <ul class="md-list md-list-addon gmap_list" id="map_users_list">
                            <li data-gmap-lat="37.406267"  data-gmap-lon="-122.06742" data-gmap-user="Weldon Krajcik Sr." data-gmap-user-company="Treutel Group">
                                <div class="md-list-addon-element">
                                    <i class="material-icons">beach_access</i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Beach </span>
                                    <span class="uk-text-small uk-text-muted">1.5 km</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.379267"  data-gmap-lon="-122.02148" data-gmap-user="Liam O'Connell" data-gmap-user-company="Smith, Torphy and D'Amore">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-bell"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Temple</span>
                                    <span class="uk-text-small uk-text-muted">500 mtr</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.410267"  data-gmap-lon="-122.11048" data-gmap-user="Arne Jacobs" data-gmap-user-company="Padberg-Cruickshank">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-motorcycle"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Fuel Station</span>
                                    <span class="uk-text-small uk-text-muted">500 mtr</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.397267"  data-gmap-lon="-122.084417" data-gmap-user="Ashlee Vandervort" data-gmap-user-company="Rippin-Glover">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-cart-plus"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Shopping Mall</span>
                                    <span class="uk-text-small uk-text-muted">750 mtr</span>
                                </div>
                            </li>
                            <li data-gmap-lat="37.372267"  data-gmap-lon="-122.090417" data-gmap-user="Dolly Kshlerin" data-gmap-user-company="Gerhold Inc">
                                <div class="md-list-addon-element">
                                    <i class="uk-icon-shield"></i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Police Station</span>
                                    <span class="uk-text-small uk-text-muted">1 km</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="uk-grid points-wrapper" data-uk-grid-margin>
                <div class="uk-width-large-1-12 uk-width-medium-1-12">
                    <h3 class="heading_a">Booking Policy</h3>
                    <ul class="md-list md-text-left">
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Perspiciatis cumque at repudiandae et maxime alias dolorem voluptas aut.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Quaerat quo molestiae et dolores velit inventore voluptas inventore corporis dolore dolorem.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Nesciunt doloribus quasi aut aut odio vero eaque suscipit reprehenderit.</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="uk-grid points-wrapper p-t-16" data-uk-grid-margin>
                <div class="uk-width-large-1-2 uk-width-medium-1-2">
                    <h3 class="heading_a">T & C</h3>
                    <ul class="md-list md-text-left">
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Perspiciatis cumque at repudiandae et maxime alias dolorem voluptas aut.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Quaerat quo molestiae et dolores velit inventore voluptas inventore corporis dolore dolorem.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Nesciunt doloribus quasi aut aut odio vero eaque suscipit reprehenderit.</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-large-1-2 uk-width-medium-1-2">
                    <h3 class="heading_a">Cancellation Policy</h3>
                    <ul class="md-list md-text-left">
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Perspiciatis cumque at repudiandae et maxime alias dolorem voluptas aut.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Quaerat quo molestiae et dolores velit inventore voluptas inventore corporis dolore dolorem.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <i class="material-icons bullet-icon">fiber_manual_record</i>
                                <span class="uk-text-small uk-text-muted">Nesciunt doloribus quasi aut aut odio vero eaque suscipit reprehenderit.</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<!-- maplace (google maps) -->
<script src="http://maps.google.com/maps/api/js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/maplace-js/dist/maplace.min.js"></script>
@endsection