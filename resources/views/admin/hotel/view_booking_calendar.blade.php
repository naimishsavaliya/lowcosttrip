@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li>Hotel Name</li>
            <li><span>Room Booking</span></li>
        </ul>
        <div class="uk-navbar-flip" style="padding:8px 0;">
            <a href="{{ env('ADMIN_URL')}}hotel/lead_room_buy/1" style="vertical-align: middle;" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add primary-color">
                <i class="uk-icon-plus no_margin"></i> Add New
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <style>
                .fc-toolbar h2{line-height: 32px;}
            </style>
            <div id="calendar_selectable"></div>

        </div>
    </div>
</div>


@endsection

@section('javascript')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/bower_components/fullcalendar/dist/fullcalendar.min.css">
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/plugins_fullcalendar.js"></script>
@endsection