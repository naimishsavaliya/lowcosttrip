@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Manage Room Booking</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <!-- <a href="{{ env('ADMIN_URL')}}hotel/lead_room_buy/1" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m"> -->
            <a href="javascript:void(0)" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>


    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')


            <table class="uk-table dt_default">

                <thead>
                    <tr>
                        <th>Hotel</th>
                        <th>Room type</th>
                        <th>Lead ID</th>
                        <th>Customer</th>
                        <!--<th>Lead Title</th>-->
                        <th>Booking Date</th>
                        <th>Occupancy Date</th>
                        <th>No of Rooms</th>
                        <th>Adult/Child/Infant</th>
                        <!-- <th>Supplier</th>
                        <th>Agent</th> -->
                        <th>Action</th>
                    </tr>
                </thead>
<!--                 <tfoot>
                    <tr>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Hotel" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Room type" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Lead Id" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Customer" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td> -->
<!--                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Lead Title" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>-->
                        <!-- <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Booking Date" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Occupiency Date" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="No of Rooms" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Adult/Child/Infant" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Supplier" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>
                            <div class="md-input-wrapper">
                                <input placeholder="Agent" type="text" class="md-input">
                                <span class="md-input-bar "></span>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                </tfoot> -->
                <tbody>
                    <?php for ($i = 1; $i <= rand(19, 99); $i++) { ?>
                        <tr class="odd gradeX">
                            <td>Hotel <?php echo $i;?></td>
                            <td>Deluxe</td>
                            <td><span data-uk-tooltip title="Kerala Tour <?php echo $i;?>">LCT<?php echo rand(1111, 9999); ?></span></td>
                            <td>Customer Name</td>
                            <td>5 Mar 2018</td>
                            <td>10 Nov 2018</td>
                            <td>1</td>
                            <td><?php echo rand(2, 3); ?>/<?php echo rand(0, 2); ?>/<?php echo rand(0, 1); ?></td>
                           <!--  <td>Supplier 1</td>
                            <td>Agent 1</td> -->
                            <td>
                                <div class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                    <button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav uk-nav-dropdown">
                                            <li><a href=""> Cancel Booking</a></li>
                                            <li><a href=""> Print Voucher</a></li>
                                            <li><a href=""> Email Voucher</a></li>
                                            <li><a href=""> Edit Booking</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection

