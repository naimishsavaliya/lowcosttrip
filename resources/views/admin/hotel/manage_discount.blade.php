@extends('layouts.theme')

@section('content')

<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('hotel.category.index') }}">Hotel Name</a></li>
            <li><span>Discount</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <!--            <div class="md-card">
                            <div class="md-card-content">-->
            <div class="p-t-30">
                <form action="{{ route('hotel.category.index')}}" name="category_frm" class="" method="get">
                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-1-1">
                            <div class="uk-form-row">


                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-4">

                                        <select id="select_demo_3" class="md-input">
                                            <option value="" selected>Room Type</option>
                                            <option value="1">All Rooms</option>
                                            <option value="1">Deluxe</option>
                                            <option value="2">Semi Deluxe</option>
                                            <option value="3">Garden</option>
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-1-5">
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                            <label for="uk_dp_start">Start Date</label>
                                            <input class="md-input" type="text" id="uk_dp_start">
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-5">
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                            <label for="uk_dp_end">End Date</label>
                                            <input class="md-input" type="text" id="uk_dp_end">
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-2-10">
                                        <div class="uk-input-group">
                                            <div class="md-input-wrapper"><label>Discount Percentage %</label><input type="text" class="md-input"><span class="md-input-bar "></span></div>

                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-10">
                                        <div class="uk-input-group">
                                            <a href="#" class="btnSectionClone"><i class="material-icons md-36"></i></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('hotel.category.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                </div>
                        </div>-->

        </div>
    </div>

</div>

@endsection
