@extends('layouts.theme')

@section('content')

<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form name="currency_frm" id="currency_frm" class="" method="post" action="{{ route('currency.store') }}" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($currency->currId) ? $currency->currId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Currency</label>
                                        <input type="text" class="md-input" name="name" value="{{ isset($currency->currency_name) ? $currency->currency_name : '' }}" />
                                        <small class="text-danger">{{ $errors->first('name') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Currency Value</label>
                                        <input type="text" class="md-input" name="currency_value" value="{{ isset($currency->currency_value) ? $currency->currency_value : '' }}" />
                                        <small class="text-danger">{{ $errors->first('currency_value') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>code</label>
                                        <input type="text" class="md-input" name="code" value="{{ isset($currency->code) ? $currency->code : '' }}" />
                                        <small class="text-danger">{{ $errors->first('code') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>symbol</label>
                                        <input type="text" class="md-input" name="symbol" value="{{ isset($currency->symbol) ? $currency->symbol : '' }}" />
                                        <small class="text-danger">{{ $errors->first('symbol') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <!--<label>Select Country</label>-->
                                        <select class="md-input" name="country_id" id="country_id" data-uk-tooltip="{pos:'top'}" title="Choose a Country">
                                            <option value="" selected>Choose a Country</option>
                                            @if(isset($countrys))
                                            @foreach($countrys as $country)
                                            <option value="{{ $country->country_id }}" {{ (isset($currency->country_id) && $currency->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <small class="text-danger">{{ $errors->first('country_id') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-2-6">
                                        <h3 class="heading_a uk-margin-small-bottom">Currency Icon</h3>
                                        <input type="file" id="input-file-a" name="currency_icon"  class="dropify" data-default-file="{{  isset($currency->currency_icon) ?  env('APP_PUBLIC_URL').'uploads/currency_icons/'.$currency->currency_icon : '' }}" />
                                    </div>
                                </div>
                                <small class="text-danger">{{ $errors->first('currency_icon') }}</small>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Description</label>
                                        <textarea cols="30" rows="4" class="md-input" name="description">{{ isset($currency->description) ? $currency->description : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div class="status-column">
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="active" data-md-icheck {{ (isset($currency->status) && $currency->status == 1) ? 'checked' : ''  }} {{ (!isset($currency)) ? 'checked' : '' }} />
                                                       <label for="active" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="inactive" data-md-icheck {{ (isset($currency->status) && $currency->status == 0) ? 'checked' : ''  }} />
                                                       <label for="inactive" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                        <small class="text-danger">{{ $errors->first('status') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('currency.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

@endsection


@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>
<script type="text/javascript">
$(function () {
// It has the name attribute "currency_frm"
    $("form#currency_frm").validate({
// Specify validation rules
        rules: {
            name: "required",
            currency_value: "required",
            country_id: "required",
//            currency_icon: {
//                accept: "jpg,png,jpeg,gif"
//            },
            status: "required",
        },
        // Specify validation error messages
        messages: {
            name: "Please enter currency name",
            currency_value: "Please enter currency value",
            country_id: "Please select country",
//            currency_icon: {
//                accept: "Only image type jpg/png/jpeg/gif is allowed"
//            },
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

/*
    @if (isset($currency) == false)
    var settings = $("form#currency_frm").validate().settings;
    $.extend(true, settings, {
        rules: {
            currency_icon: {
                required: true,
//                accept: "jpg,png,jpeg,gif"
            }
        },
        messages: {
            currency_icon: {
                required: "Please upload currency icon",
//                accept: "Only image type jpg/png/jpeg/gif is allowed"
            }
        }
    });
    @endif
    */

});

</script>
@endsection