@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        <div class="uk-navbar-flip p-t-8">
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table">
                <thead>
                    <tr>
                        <th colspan="7" class="uk-text-center"><strong><h3>Booking Info</h3> </strong></th>
                    </tr>
                </thead>
            </table>

           <div class="uk-width-medium-1-12">
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-3 p-r-50">
                            <table class="uk-table" >
                                <tbody> 
                                    <tr>
                                        <td colspan="2" style="text-align: center;"><strong><h3>Flight Info</h3></strong></td>
                                    </tr>
                                    <tr>
                                        <td width="50%"><strong>From City</strong></td>
                                        <td width="50%">{{ (isset( $flight_books->flight_from_city ) ? $flight_books->flight_from_city : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>To City</strong></td>
                                        <td>{{ (isset( $flight_books->flight_to_city ) ? $flight_books->flight_to_city : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Depart </strong></td>
                                        <td>{{ (isset( $flight_books->depart_date ) ? date("d M Y", strtotime($flight_books->depart_date)) : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Passengers|Class </strong></td>
                                        <td>{{ (isset( $flight_books->passengers_class ) ? get_air_class()[ $flight_books->passengers_class ]: ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Adults </strong></td>
                                        <td>{{ (isset( $flight_books->adults ) ? $flight_books->adults : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Child </strong></td>
                                        <td>{{ (isset( $flight_books->child ) ? $flight_books->child : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Infants </strong></td>
                                        <td>{{ (isset( $flight_books->infants ) ? $flight_books->infants : ' - ') }}</td>
                                    </tr>
                                </tbody>
                            </table>    
                        </div>

                        <div class="uk-width-medium-1-3 p-r-50">
                            <table class="uk-table" >
                                <tbody> 
                                    <tr>
                                        <td colspan="2" style="text-align: center;"><strong><h3>Flight Price Info</h3></strong></td>
                                    </tr>
                                    <tr>
                                        <td width="50%"><strong>Base</strong></td>
                                        <td width="50%" class="uk-text-right">{{ (isset( $flight_books->base_fare ) ? number_format($flight_books->base_fare, 2) : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>CongestionCharge</strong></td>
                                        <td class="uk-text-right">{{ (isset( $flight_books->congestioncharge ) ? number_format($flight_books->congestioncharge, 2) : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>IGSTax</strong></td>
                                        <td class="uk-text-right">{{ (isset( $flight_books->igstax ) ? number_format($flight_books->igstax, 2) : ' - ') }}</td>
                                    </tr>
                                    <?php /*
                                    <tr>
                                        <td><strong>TGSTTax</strong></td>
                                        <td class="uk-text-right">{{ (isset( $flight_books->tgsttax ) ? number_format($flight_books->tgsttax, 2) : ' - ') }}</td>
                                    </tr> */ ?>
                                    <tr>
                                        <td><strong>AirportTax</strong></td>
                                        <td class="uk-text-right">{{ (isset( $flight_books->airporttax ) ? number_format($flight_books->airporttax, 2) : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>FuelSurcharge</strong></td>
                                        <td class="uk-text-right">{{ (isset( $flight_books->fuelSurcharge ) ? number_format($flight_books->fuelSurcharge, 2) : ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Grand Total</strong></td>
                                        <td class="uk-text-right">{{ (isset( $flight_books->grand_total ) ? number_format($flight_books->grand_total, 2) : ' - ') }}</td>
                                    </tr>
                                
                                </tbody>
                            </table>
                        </div>

                        <div class="uk-width-medium-1-3 p-r-50">
                            <table class="uk-table" >
                                <tbody> 
                                    <tr>
                                        <td colspan="2" style="text-align: center;"><strong><h3>Payment Info</h3></strong></td>

                                    </tr>
                                    <tr>
                                        <td width="50%"><strong>Transaction ID</strong></td>
                                        <td width="50%">{{ (isset( $flight_payment_info->transaction_id ) ? $flight_payment_info->transaction_id: ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>User Email</strong></td>
                                        <td>{{ (isset( $flight_payment_info->user_email ) ? $flight_payment_info->user_email: ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>User Mobile No</strong></td>
                                        <td>{{ (isset( $flight_payment_info->user_mobile_no ) ? $flight_payment_info->user_mobile_no: ' - ') }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Bank Name</strong></td>
                                        <td>{{ (isset( $flight_payment_info->bank_name ) ? $flight_payment_info->bank_name: ' - ') }}</td>
                                    </tr>
                                   
                                    <tr>
                                        <td><strong>Status</strong></td>
                                        <td>
                                            <?php if(isset( $flight_payment_info->status ) && $flight_payment_info->status=='Ok'){ ?>
                                                <span class="uk-badge uk-badge-success">Transction Success</span>
                                            <?php }else{ ?>
                                                <span class="uk-badge uk-badge-danger">Transction Failed</span>
                                            <?php } ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <?php if(isset( $flight_payment_info->status ) && $flight_payment_info->status=='Ok'){ ?>
                                            <td class="uk-text-success"><strong>Paid Amount</strong></td>
                                            <td class="uk-text-success"><strong>{{ (isset( $flight_payment_info->paid_amount ) ? number_format($flight_payment_info->paid_amount, 2): ' - ') }}</strong></td>
                                        <?php }else{ ?>
                                            <td class="uk-text-danger"><strong>Unpaid Amount</strong></td>
                                            <td class="uk-text-danger"><strong>{{ (isset( $flight_payment_info->paid_amount ) ? number_format($flight_payment_info->paid_amount, 2): (isset( $flight_books->grand_total ) ? number_format($flight_books->grand_total, 2) : ' - ') ) }}</strong></td>
                                        <?php } ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



            <table class="uk-table">
                <thead>
                    <tr>
                        <th colspan="7" class="uk-text-center"><strong><h3>Tour Info</h3> </strong></th>
                    </tr>
                    <tr>
                        <th><strong>Flight Name</strong></th>
                        <th><strong>Drrival Airportcity</strong></th>
                        <th><strong>Aeparture Airportcity</strong></th>
                        <th><strong>Departur Time</strong></th>
                        <th><strong>Arrival Time</strong></th>
                        <th><strong>Time Duration</strong></th>
                        <th><strong>Flight Type</strong></th>
                      
                    </tr>
                </thead>
                <tbody> 
                    <?php if($flight_review_info){ ?>
                        <?php foreach ($flight_review_info as $key => $flight_review) { ?>
                            <tr>
                                <td>{{ (isset( $flight_review->flight_name ) ? $flight_review->flight_name : ' - ') }} <br>
                                    {{ (isset( $flight_review->flight_number ) ? $flight_review->flight_number : ' - ') }}
                                    {{ (isset( $flight_review->carrier_code ) ? '-- '.$flight_review->carrier_code : ' - ') }}
                                </td>
                                <td>{{ (isset( $flight_review->drrival_airportcity ) ? $flight_review->drrival_airportcity : ' - ') }}</td>
                                <td>{{ (isset( $flight_review->aeparture_airportcity ) ? $flight_review->aeparture_airportcity : ' - ') }}</td>
                                
                                <td>{{ (isset( $flight_review->departure_time ) ? date("d M Y h:i A", strtotime($flight_review->departure_time)) : ' - ') }}</td>
                                <td>{{ (isset( $flight_review->arrival_time ) ? date("d M Y h:i A", strtotime($flight_review->arrival_time)) : ' - ') }}</td>

                                <td>{{ (isset( $flight_review->time_duration ) ? $flight_review->time_duration : ' - ') }}</td>
                                <td>{{ (isset( $flight_review->flight_type ) ? $flight_review->flight_type : ' - ') }}</td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>



            <table class="uk-table">
                <thead>
                    <tr>
                        <th colspan="10" class="uk-text-center"><strong><h3>Passengers Info</h3> </strong></th>
                    </tr>
                    <tr>
                        <th><strong>Type</strong></th> 
                        <th><strong>Name</strong></th> 
                        <th><strong>DOB</strong></th> 
                        <th><strong>Gender</strong></th> 
                        <th><strong>Meals</strong></th> 
                        <th><strong>Meals Price</strong></th> 
                        <th><strong>Seats</strong></th> 
                        <th><strong>Seats Price</strong></th> 
                        <th><strong>Buggage</strong></th> 
                        <th><strong>Buggage Price</strong></th> 
                    </tr>
                </thead>
                <tbody> 
                    <?php if($flight_passengers){ ?>
                        <?php foreach ($flight_passengers as $key => $passengers) { ?>
                            <tr>
                                <td>{{ (isset( $passengers->passengers_type ) ? ucwords($passengers->passengers_type) : ' - ') }}</td>
                                <td>
                                    {{ (isset( $passengers->first_name ) ? $passengers->first_name : ' - ') }}
                                    {{ (isset( $passengers->last_name ) ? ' '.$passengers->last_name : ' - ') }}
                                </td>
                                <td>{{ (isset( $passengers->date_of_birth ) && $passengers->date_of_birth != '0000-00-00' ? date("d M Y", strtotime($passengers->date_of_birth)) : ' - ') }}</td>
                                <td>{{ (isset( $passengers->gender ) ? (($passengers->gender == 'M') ? 'Male' :'Female') : ' - ') }}</td>

                                <td>{{ (isset( $passengers->meals ) ? $passengers->meals : ' - ') }}</td>
                                <td>{{ (isset( $passengers->meals_price ) ? $passengers->meals_price : ' - ') }}</td>
                                <td>{{ (isset( $passengers->seats ) ? $passengers->seats : ' - ') }}</td>
                                <td>{{ (isset( $passengers->seats_price ) ? $passengers->seats_price : ' - ') }}</td>
                                <td>{{ (isset( $passengers->buggage ) ? $passengers->buggage : ' - ') }}</td>
                                <td>{{ (isset( $passengers->buggage_price ) ? $passengers->buggage_price : ' - ') }}</td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
 
@endsection

@section('javascript')

@endsection
