@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
        <div class="uk-navbar-flip p-t-8">
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table" id="currency_tbl">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Mobile Number</th>
                        <th>From City</th>
                        <th>To City</th>
                        <th>Depart</th>
                        <th>Passengers|Class</th>
                        <th>Adults / Child / Infants</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
 
@endsection

@section('javascript')
<script type="text/javascript">
    $(function () {

        var currencyTable = $('#currency_tbl').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            order: [[ 0, "desc" ]],
//            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
//            "pagingType": "full_numbers", //full,full_numbers
            "ajax": {
                "url": "{{ route('flights.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof currencyTable != 'undefined' && currencyTable.hasOwnProperty('settings')) {
                        currencyTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "id", "name": "id"},
                {"data": "email", "name": "email"},
                {"data": "mobile", "name": "mobile"},
                {"data": "flight_from", "name": "flight_from"},
                {"data": "flight_to", "name": "flight_to"},
                {"data": "depart_date", "name": "depart_date"},
                {"data": "passengers_class", "name": "passengers_class"}, 
                {"data": "passengers", "name": "passengers"},
              
                
                {"data": "action", 'sortable': false, "render": function (data, type, row) {
                        var view_url = "{{ route('flights.book.view', ':id') }}";
                        view_url = view_url.replace(':id', row.id);
                        return '<a href="' + view_url + '" title="Edit Currency" class=""><i class="material-icons"> visibility </i></a>';
                    },
                },
            ]
        });
 
    });
</script>
@endsection

