<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> 
<html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>

        <!--<link rel="icon" type="image/png" href="{{ env('APP_PUBLIC_URL')}}/theme/assets/img/favicon-16x16.png" sizes="16x16">-->
        <link rel="icon" type="image/ico" href="{{ env('APP_PUBLIC_URL')}}/admin/img/favicon.ico" >

        <title>Lowcosttrip - Login Page</title>

        <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

        <!-- uikit -->
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}/theme/bower_components/uikit/css/uikit.almost-flat.min.css"/>

        <!-- altair admin login page -->
        <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}/theme/assets/css/login_page.css" />
        <style type="text/css">
            input:-webkit-autofill {
                -webkit-box-shadow: 0 0 0 1000px white inset !important;
            }
        </style>
    </head>

    <body class="login_page">
        <div>
            <h1 class="logo_name">
                <img src="{{ env('APP_PUBLIC_URL')}}admin/img/logo.png" alt="LOWCOSTTRIP" class="center" />
            </h1>
        </div>

        <div class="login_page_wrapper">
            <div class="md-card" id="login_card">
                <div class="md-card-content large-padding" id="login_form">
                    <div class="login_heading">
                        <div class="user_avatar"></div>
                    </div>
                    @if($errors->first('email'))            
                    <div class="uk-alert uk-alert-danger" data-uk-alert="">
                        <a href="javascript:;" class="uk-alert-close uk-close"></a>
                        {{ $errors->first('email') }}
                    </div>
                    @endif
                    <!--<form id="loginform1" class="form-vertical no-padding no-margin" method="POST" action="{{-- route('login') --}}">-->
                    <form id="loginform1" class="form-vertical no-padding no-margin" method="POST" action="{{ env('ADMIN_URL') }}login/login">
                        {{ csrf_field() }}
                        <div class="uk-form-row">
                            <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                <label for="login_username">Username</label>
                                <input class="md-input" type="text" id="login_username" name="email" autocomplete="off" />
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                <label for="login_password">Password</label>
                                <input class="md-input" type="password" id="login_password" name="password" />
                            </div>
                        </div>
                        <?php /*
                        <div class="uk-margin-medium-top">
                            <a href="#" id="enter_OTP_pop_show" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</a>
                        </div>
                        */ ?>
                        <div class="uk-margin-medium-top">
                            <a href="#" id="submit_btn_login" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</a>
                        </div>
                        <div class="uk-margin-top">
                            <a href="#" id="password_reset_show" class="uk-float-right">Forget Password?</a>
                            <span class="icheck-inline">
                                <input type="checkbox" name="login_page_stay_signed" id="login_page_stay_signed" data-md-icheck />
                                <label for="login_page_stay_signed" class="inline-label">Stay signed in</label>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="md-card-content large-padding" id="login_password_reset" style="display: none">
                    <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                    <h2 class="heading_a uk-margin-large-bottom">Reset password</h2>
                    <form>
                        <div class="uk-form-row">
                            <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                <label for="login_email_reset">Your email address</label>
                                <input class="md-input" type="text" id="login_email_reset" name="login_email_reset" />
                            </div>
                        </div>
                        <div class="uk-margin-medium-top">
                            <a href="#" class="md-btn md-btn-primary md-btn-block">Reset password</a>
                        </div>
                    </form>
                </div>
                <div class="md-card-content large-padding" id="Enter_OTP_pop" style="display: none">
                    <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                    <h2 class="heading_a uk-margin-large-bottom">Enter OTP</h2>
                    <form action="admin/home">
                        <div class="uk-form-row">
                            <div class="md-input-wrapper md-input-wrapper-danger md-input-filled">
                                <label for="login_email_reset">Your OTP</label>
                                <input class="md-input" type="text" id="Enter_OTP" name="Enter_OTP" />
                                <a href="#" id="Resend_OTP" class="uk-float-right">Resend OTP?</a>
                            </div>
                        </div>
                        <div class="uk-margin-medium-top">
                            <a href="#" id="submit-otp-btn" class="md-btn md-btn-primary md-btn-block">Submit</a>
                            <!--<button  type="subloginform1mit" class="md-btn md-btn-primary md-btn-block">Confirm</button>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- BEGIN LOGIN -->
        <!-- common functions -->
        <script src="{{ env('APP_PUBLIC_URL')}}/theme/assets/js/common.min.js"></script>
        <!-- uikit functions -->
        <script src="{{ env('APP_PUBLIC_URL')}}/theme/assets/js/uikit_custom.min.js"></script>
        <!-- altair core functions -->
        <script src="{{ env('APP_PUBLIC_URL')}}/theme/assets/js/altair_admin_common.min.js"></script>

        <!-- altair login page functions -->
        <script src="{{ env('APP_PUBLIC_URL')}}/theme/assets/js/pages/login.js"></script>

        <script>
// check for theme
if (typeof (Storage) !== "undefined") {
    var root = document.getElementsByTagName('html')[0],
            theme = localStorage.getItem("altair_theme");
    if (theme == 'app_theme_dark' || root.classList.contains('app_theme_dark')) {
        root.className += ' app_theme_dark';
    }
}
        </script>

    </body>
</html>