@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
<style type="text/css">
    .user_heading {
        padding: 26px 26px 0 26px !important;
    }
    .uk-sticky-placeholder .uk-tab{background: #1976D2;}
    .user_heading{ padding: 24px 24px 0px 24px;}
    .select2{ width: 100% !important;}
    .uk-tab > li.uk-active > a{color: #FFF;}
    .uk-tab > li.uk-active > a{border-bottom-color: #00071f;}
    .uk-dropdown{width: 200px !important;}
    .select2-container--open{z-index: 9999;}
    .uk-dropdown-shown{min-width: 260px !important;}

</style>
@endsection

@section('content')

<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ route('home') }}"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('supplier.index') }}">Manage Supplier</a></li>
            <li><span>Details</span></li>
        </ul>
    </div>
    <div id="page_content">
        <div id="page_content_inner p-0">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-1-1">
                    <div class="">
                        <div class="user_heading">
                            <div class="user_heading_menu hidden-print">
                                <div class="uk-display-inline-block" data-uk-dropdown="{pos:'left-top'}">
                                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="javascript:;" data-uk-modal="{target:'#update_status'}" ><i class="material-icons ">add</i> Update Status</a></li>
                                            <li><a href="{{ route('supplier.notes.add', '1') }}"><i class="material-icons ">add</i> Followup</a></li>
                                            <li><a href="{{ route('supplier.document.add', '1') }}"><i class="material-icons ">add</i> Document</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="user_heading">
                                <div class="user_heading_avatar">
                                    <div class="thumbnail">
                                        <img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/avatars/user@2x.png" alt="user avatar"/>
                                    </div>
                                </div>
                                <div class="user_heading_content" style="display:inline-flex;">
                                    <h2 class="heading_b uk-margin-bottom">
                                        <span class="uk-text-truncate">Supplier Name</span>
                                        <span class="sub-heading">Address Here</span>
                                        <span class="md-btn md-btn-primary">Pending Status</span>
                                    </h2>
                                    <ul class="user_stats" style="margin-left:40px;padding: 5px;">
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Leads</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Quotations</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Confirm Leads</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Tours</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Customers</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Reviews</span></h4>
                                        </li>
                                        <li>
                                            <h4 class="heading_a">00 <span class="sub-heading">Revenue</span></h4>
                                        </li>
                                    </ul>
                                </div>
                                <!--                                <a class="md-fab md-fab-small md-fab-accent hidden-print" href="javascript:,;">
                                                                    <i class="material-icons">&#xE150;</i>
                                                                </a>-->
                            </div>
                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">Supplier Info</a></li>
                                <li class="followup"><a href="#">Follow-up</a></li>
                                <li class="documents"><a href="#">Documents</a></li>
                                <li class="invoice"><a href="#">Invoice</a></li>
                                <li class="ledger"><a href="#">Ledger</a></li>
                                <!--<li><a href="#">History</a></li>-->
                            </ul>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <div class="p-t-30">
                                        <form action="{{ route('supplier.store')}}" name="supplier_frm" class="" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{ isset($supplier->id) ? $supplier->id : '' }}" />
                                            <div>
                                                <div class="p-t-30">
                                                    <h3 class="heading_a">Contact Person</h3>
                                                    <div class="uk-grid" data-uk-grid-margin>

                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-4 p-r-50">
                                                                        <label>First Name</label>
                                                                        <input type="text" class="md-input" name="first_name" value="{{ isset($supplier->first_name) ? $supplier->first_name : '' }}" />
                                                                        <small class="text-danger">{{ $errors->first('first_name') }}</small>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-4 p-r-50">
                                                                        <label>Last Name</label>
                                                                        <input type="text" class="md-input" name="last_name" value="{{ isset($supplier->last_name) ? $supplier->last_name : '' }}" />
                                                                        <small class="text-danger">{{ $errors->first('last_name') }}</small>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <select id="select_demo_3" name="user_type"  class="md-input">
                                                                            <option value="" selected>Choose a category</option>
                                                                            @if(isset($supplier_types))
                                                                            @foreach($supplier_types as $supplier_type)
                                                                            <option value="{{ $supplier_type->supTypeId }}" {{ (isset($supplier->sup_type) && $supplier->sup_type == $supplier_type->supTypeId) ? 'selected' : '' }}>{{ $supplier_type->type }}</option>
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <label>Company Name</label>
                                                                        <input type="text" class="md-input" name="camapnay_name" value="{{ isset($supplier->camapnay_name) ? $supplier->camapnay_name : '' }}" />
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <label>Contact No.</label>
                                                                        <input type="text" class="md-input tele-number" name="contact_no" value="{{ isset($supplier->contact_no) ? $supplier->contact_no : '' }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <label>Office number</label>
                                                                        <input type="text" class="md-input tele-number" name="office_no" value="{{ isset($supplier->office_no) ? $supplier->office_no : '' }}" />
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <label>Email</label>
                                                                        <input type="text" class="md-input" name="supp_email" value="{{ isset($supplier->supp_email) ? $supplier->supp_email : '' }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <label>Address</label>
                                                                        <textarea cols="30" rows="4" class="md-input" name="address">{{ isset($supplier->address) ? $supplier->address : '' }}</textarea>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <label>GST Address</label>
                                                                        <textarea cols="30" rows="4" class="md-input" name="gst_address">{{ isset($supplier->gst_address) ? $supplier->gst_address : '' }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <label>GST No</label>
                                                                        <input type="text" class="md-input" name="gst_number" value="{{ isset($supplier->gst_number) ? $supplier->gst_number : '' }}" />
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <label>GST Name</label>
                                                                        <input type="text" class="md-input" name="gst_name" value="{{ isset($supplier->gst_name) ? $supplier->gst_name : '' }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <select class="md-input" name="country_id" id="country_id">
                                                                            <option value="" selected>Choose a Country</option>
                                                                            @if(isset($countrys))
                                                                            @foreach($countrys as $country)
                                                                            <option value="{{ $country->country_id }}" {{ (isset($supplier->country_id) && $supplier->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <select class="md-input" name="state_id" id="state_id">
                                                                            <option value="" selected>Choose a State</option>
                                                                            @if(isset($states))
                                                                            @foreach($states as $state)
                                                                            <option value="{{ $state->state_id }}" {{ (isset($supplier->state_id) && $supplier->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <select class="md-input" name="city_id" id="city_id">
                                                                            <option value="" selected>Choose a City</option>
                                                                            @if(isset($cities))
                                                                            @foreach($cities as $city)
                                                                            <option value="{{ $city->city_id }}" {{ (isset($supplier->city_id) && $supplier->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <label>Pincode</label>
                                                                        <input type="text" class="md-input" name="pin_code" value="{{ isset($supplier->pin_code) ? $supplier->pin_code : '' }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <select id="select_demo_3" class="md-input">
                                                                            <option value="" selected>Supplier Created By</option>
                                                                            <option value="a">Item A</option>
                                                                            <option value="b">Item B</option>
                                                                            <option value="c">Item C</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <select id="select_demo_3" class="md-input">
                                                                            <option value="" selected>Verified By</option>
                                                                            <option value="a">Item A</option>
                                                                            <option value="b">Item B</option>
                                                                            <option value="c">Item C</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <div class="p-t-30">
                                                    <h3 class="heading_a">Login Information</h3>
                                                    <div class="uk-grid" data-uk-grid-margin>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <label>E-mail</label>
                                                                        <input type="text" class="md-input" name="email" value="{{ isset($supplier->email) ? $supplier->email : '' }}" />
                                                                        <small class="text-danger">{{ $errors->first('email') }}</small>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <label>Registered Mobile No.</label>
                                                                        <input type="text" class="md-input" name="register_mobile" value="{{ isset($supplier->register_mobile) ? $supplier->register_mobile : '' }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-2 p-r-50">
                                                                        <label>Password</label>
                                                                        <input type="password" class="md-input" name="password" />
                                                                    </div>
                                                                    <div class="uk-width-medium-1-2 p-l-50">
                                                                        <label>Confirm Password</label>
                                                                        <input type="text" class="md-input" name="cfrm_password" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <div class="p-t-30">
                                                    <h3 class="heading_a">
                                                        Branch Information
                                                        <a onclick="cloneData();" title="Add New Brach" class="btn btn-mini btn-success addPrice"><i class="material-icons md-36">add_box</i></a> 
                                                    </h3>
                                                    <div class="cloneDiv cloneData">
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-medium-1-12">
                                                                <div class="uk-form-row">
                                                                    <div class="uk-grid" data-uk-grid-margin>
                                                                        <div class="uk-width-medium-1-2 p-r-50">
                                                                            <label>Name</label>
                                                                            <input type="text" class="md-input" />
                                                                        </div>
                                                                        <div class="uk-width-medium-1-2 p-l-50">
                                                                            <label>Address</label>
                                                                            <input type="text" class="md-input" />
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="uk-width-medium-1-12">
                                                                <div class="uk-form-row">
                                                                    <div class="uk-grid" data-uk-grid-margin>
                                                                        <div class="uk-width-medium-1-2 p-r-50">
                                                                            <label>Registered Mobile No.</label>
                                                                            <input type="text" class="md-input" />
                                                                        </div>
                                                                        <div class="uk-width-medium-1-2 p-l-50">
                                                                            <select id="select_demo_3" class="md-input">
                                                                                <option value="" selected>Choose a Country</option>
                                                                                <option value="a">Item A</option>
                                                                                <option value="b">Item B</option>
                                                                                <option value="c">Item C</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="uk-width-medium-1-12">
                                                                <div class="uk-form-row">
                                                                    <div class="uk-grid" data-uk-grid-margin>
                                                                        <div class="uk-width-medium-1-2 p-r-50">
                                                                            <select id="select_demo_3" class="md-input">
                                                                                <option value="" selected>Choose a State</option>
                                                                                <option value="a">Item A</option>
                                                                                <option value="b">Item B</option>
                                                                                <option value="c">Item C</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="uk-width-medium-1-2 p-l-50">
                                                                            <select id="select_demo_3" class="md-input">
                                                                                <option value="" selected>Choose a City</option>
                                                                                <option value="a">Item A</option>
                                                                                <option value="b">Item B</option>
                                                                                <option value="c">Item C</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="uk-width-medium-1-12">
                                                                <div class="uk-form-row">
                                                                    <div class="uk-grid" data-uk-grid-margin>
                                                                        <div class="uk-width-medium-1-2 p-r-50">
                                                                            <label>Pincode</label>
                                                                            <input type="text" class="md-input" />
                                                                        </div>
                                                                        <div class="uk-width-medium-1-2 p-l-50">
                                                                            <label>Contact Person</label>
                                                                            <input type="text" class="md-input" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="uk-width-medium-1-12">
                                                                <div class="uk-form-row">
                                                                    <div class="uk-grid" data-uk-grid-margin>
                                                                        <div class="uk-width-medium-1-2 p-r-50">
                                                                            <label>Contact No.</label>
                                                                            <input type="text" class="md-input" />
                                                                        </div>
                                                                        <div class="uk-width-medium-1-2 p-l-50">
                                                                            <label>Google Map Link</label>
                                                                            <input type="text" class="md-input" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="appendDiv"></div>
                                                </div>
                                            </div>

                                            <div>
                                                <div class="p-t-30">
                                                    <h3 class="heading_a">Supplier Can Provide</h3>
                                                    <div class="uk-grid" data-uk-grid-margin>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-6">
                                                                        <input type="checkbox" name="can_provide[]" value="" id="can_provide_1" data-md-icheck />
                                                                        <label for="can_provide_1" class="inline-label">Tours</label>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-6">
                                                                        <input type="checkbox" name="can_provide[]" value="" id="can_provide_2" data-md-icheck />
                                                                        <label for="can_provide_2" class="inline-label">Hotels</label>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-6">
                                                                        <input type="checkbox" name="can_provide[]" value="" id="can_provide_3" data-md-icheck />
                                                                        <label for="can_provide_3" class="inline-label">Activities</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h3 class="heading_a">Certifications</h3>
                                                    <div class="uk-grid" data-uk-grid-margin>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    @if(isset($certificates))
                                                                    @foreach($certificates as $cer => $certificate)
                                                                    <div class="uk-width-medium-1-6">
                                                                        <input type="checkbox" name="certificates[]" value="{{ $certificate->cerId }}" id="certificate_{{ $cer }}" data-md-icheck />
                                                                        <label for="certificate_{{ $cer }}" class="inline-label">{{ $certificate->name}}</label>
                                                                    </div>
                                                                    @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-12">
                                                                        <textarea id="wysiwyg_ckeditor" cols="30" rows="20" name="notes">{{ isset($supplier->notes) ? $supplier->notes : '' }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-2-6">
                                                                        <h3 class="heading_a uk-margin-small-bottom">Logo</h3>
                                                                        <input type="file" id="input-file-a" class="dropify" />
                                                                    </div>
                                                                    <div class="uk-width-medium-2-3">
                                                                        <h3 class="heading_a uk-margin-small-bottom">Office images</h3>
                                                                        <div id="file_upload-drop" class="uk-file-upload">
                                                                            <p class="uk-text">Drop file to upload</p>
                                                                            <p class="uk-text-muted uk-text-small uk-margin-small-bottom">or</p>
                                                                            <a class="uk-form-file md-btn">choose file<input id="file_upload-select" type="file"></a>
                                                                        </div>
                                                                        <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                                                            <div class="uk-progress-bar" style="width:0">0%</div>
                                                                        </div>	
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-12">
                                                            <div class="uk-form-row">
                                                                <div class="uk-grid" data-uk-grid-margin>
                                                                    <div class="uk-width-medium-1-6">
                                                                        <input type="radio" name="status" value="1" id="status_1" data-md-icheck />
                                                                        <label for="status_1" class="inline-label">Active</label>
                                                                    </div>
                                                                    <div class="uk-width-medium-1-6">
                                                                        <input type="radio" name="status" value="0" id="status_0" data-md-icheck />
                                                                        <label for="status_0" class="inline-label">In Active</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="form_hr">
                                            <div class="uk-grid uk-margin-medium-top uk-text-right">
                                                <div class="uk-width-1-1">
                                                    <a href="{{ route('supplier.index') }}" class="md-btn md-btn-danger">Cancel</a>
                                                    <button type="submit" class="md-btn md-btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </li> 

                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Date Added</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Reminder</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>17/03/2019</td>
                                                    <td>non, bibendum sed, est. Nunc laoreet</td>
                                                    <td>eleifend nec, malesuada ut, sem.</td>
                                                    <td><i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>  1 Jan 2019 10 AM</td>
                                                    <td >
                                                        <a href="{{ route('supplier.notes.edit', array($supid,'1')) }}" title="Edit notes" class=""><i class="md-icon material-icons">&#xE254;</i></a> 
                                                        <a href="javascript:;" onclick="return confirm('Are you sure to delete?');" title="Delete notes" class=""><i class="md-icon material-icons">delete</i></a> 
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Date Added</th>
                                                <th>Title</th>
                                                <th>Attachment</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>17/03/2019</td>
                                                    <td>PAN Card</td>
                                                    <td><img src="{{ env('APP_PUBLIC_URL')}}theme/assets/img/gallery/Image16.jpg" alt="" style="height: 50px;"></td>
                                                    <td>ID Card</td>
                                                    <td >
                                                        <a href="{{ route('supplier.document.edit', array($supid,'1')) }}" title="Edit Document" class=""><i class="md-icon material-icons">edit</i></a> 
                                                        <a href="javascript:;" onclick="return confirm('Are you sure to delete?');" title="Delete Document" class=""><i class="md-icon material-icons">delete</i></a> 
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Invoice ID</th>
                                                <th>DateTime</th>
                                                <th>Particular</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>12345687</td>
                                                    <td>11/Jun/2018 12:23:10</td>
                                                    <td>Particular <?= $i ?></td>
                                                    <td><?= $i ?>,256</td>
                                                    <td><span class="label label-success">Active</span></td>
                                                    <td>
                                                        <a href="javascript:;" title="Print" class=""><i class="md-icon material-icons">print</i></a> 
                                                        <a href="javascript:;" title="E-mail" class=""><i class="md-icon material-icons">email</i></a> 
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table class="uk-table dt_default">
                                        <thead>
                                            <tr>
                                                <th>Transaction ID</th>
                                                <th>DateTime</th>
                                                <th>Particular</th>
                                                <th>Debit</th>
                                                <th>Credit</th>
                                                <th>Balance</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i = 1; $i <= rand(1, 9); $i++) { ?>
                                                <tr class="odd gradeX">
                                                    <td>12345687</td>
                                                    <td>11/Jun/2018 12:23:10</td>
                                                    <td>Particular <?= $i ?></td>
                                                    <td>
                                                        <?php
                                                        if ($i % 2 == 0) {
                                                            echo $i . ",256";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($i % 2 != 0) {
                                                            echo $i . ",256";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?= $i ?>,256</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>                                                                                

            <div class="uk-modal" id="assign_group_tour">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Assign Group Tour</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Tour</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Gujarat Tour</option>
                                <option value="1">Kerala Tour</option>
                                <option value="2"                                                                                        >Kashmir Tour</option>
                                <option value="2">Rajasthan Tour</option>
                                <option value="2">Punjab Tour</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Departure Date</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">01 Jan 2019</option>
                                <option value="2">01 Feb 2019</option>
                                <option value="2">01 Mar 2019</option>
                                <option value="2">01 Jun 2019</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-modal-footer">
                        <div class="uk-text-left uk-width-medium-1-2 uk-float-left">
                            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-flat">Create New Group Tour</a>
                        </div>
                        <div class="uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" class="md-b                                                                                                                                                                                                        tn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="assign_customised_tour">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Assign Customised Tour</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Tour</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Gujarat Tour</option>
                                <option value="1">Kerala Tour</option>
                                <option value="2">Kashmir Tour</option>
                                <option value="2">Rajasthan Tour</option>
                                <option value="2">Punjab Tour</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                        <div class="uk-width-medium-1-1 ">
                            <div class="md-input-wrapper"><label for="invoice_dp">Departure Date</label><input class="md-input uk-width-1-1 " type="text" id="invoice_dp" value="" data-uk-datepicker="{format:'DD.MM.YYYY'}"><span class="md-input-bar "></span></div>

                        </div>
                        <div class="clear clearfix"></div>
                    </div>
                    <div class="uk-modal-footer">
                        <div class="uk-text-left uk-width-medium-1-2 uk-float-left">
                            <a href="{{ env('ADMIN_URL')}}tour/add" class="md-btn md-btn-flat">Create New Customised Tour</a>
                        </div>
                        <div class="uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="update_status">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Update Status</h3>
                    </div>
                    <div class="uk-modal-page">
                        <div class="uk-width-medium-1-1">
                            <span class="sub-heading">Select Status</span>
                            <select id="tourID" name="selec_adv_s2_1" class="select_no_search uk-width-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
                                <option value="2">Status 1</option>
                                <option value="2">Status 2</option>
                                <option value="2">Status 3</option>
                            </select>
                        </div>
                        <div class="clear clearfix"></div> <br/>
                    </div>
                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="update_status" class="uk-modal uk-hide uk-fade" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999;">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Update Status</h3>
        </div>
        <select id="selec_adv_s2_1" name="selec_adv_s2_1" class="select_no_search uk-width-large-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
            <option value="1">Status 1</option>
            <option value="2">Status 2</option>
            <option value="2">Status 3</option>
        </select>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button> <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Update</button>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!-- ckeditor -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ckeditor/ckeditor.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ckeditor/adapters/jquery.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_wysiwyg.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script type="text/javascript">
                                                        $(function () {
                                                            if (location.hash && location.hash.length) {
                                                                var hash = decodeURIComponent(location.hash.substr(1));
                                                                setTimeout(
                                                                        function ()
                                                                        {
                                                                            $("ul#user_profile_tabs li." + hash).trigger("click");
                                                                        }, 1000);
                                                            }
                                                        });
</script>
@endsection
