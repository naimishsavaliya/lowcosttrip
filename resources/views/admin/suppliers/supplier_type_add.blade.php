@extends('layouts.theme')
@section('content')

<div id="page_content">

    <div id="top_bar">
        @include('admin.includes.breadcrumbs', $breadcrumb)
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="p-t-30">
                <form action="{{ route('supplier.type.store') }}" name="supplier_type_frm" id="supplier_type_frm" class="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ isset($supplier_type->supTypeId) ? $supplier_type->supTypeId : '' }}" />
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Name</label>
                                        <input type="text" class="md-input" name="type" value="{{ isset($supplier_type->type) ? $supplier_type->type : '' }}" />
                                        @if($errors->has('type'))<small class="text-danger">{{ $errors->first('type') }}</small>@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <label>Description</label>
                                        <textarea cols="30" rows="4" class="md-input" name="description">{{ isset($supplier_type->description) ? $supplier_type->description : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-12">
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 p-r-50">
                                        <div>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="1" id="status_1" data-md-icheck {{ (isset($supplier_type->status) && $supplier_type->status == 1) ? 'checked' : ''  }} {{ (!isset($supplier_type)) ? 'checked' : '' }} />
                                                       <label for="status" class="inline-label">Active</label>
                                            </span>
                                            <span class="icheck-inline">
                                                <input type="radio" name="status" value="0" id="status_0" data-md-icheck {{ (isset($supplier_type->status) && $supplier_type->status == 0) ? 'checked' : ''  }} />
                                                       <label for="status_0" class="inline-label">In Active</label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="form_hr">
                    <div class="uk-grid uk-margin-medium-top uk-text-right">
                        <div class="uk-width-1-1">
                            <a href="{{ route('supplier.type.index') }}" class="md-btn md-btn-danger">Cancel</a>
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

@endsection

@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/additional-methods.min.js"></script>

<script type="text/javascript">
$(function () {
    // It has the name attribute "banner_frm"
    $("form#supplier_type_frm").validate({
        // Specify validation rules
        rules: {
            type: "required",
            status: "required",
        },
        // Specify validation error messages
        messages: {
            type: "Please enter supplier type",
            status: "Please select status",
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('div.status-column'));
            } else if (element.attr("type") == "file") {
                error.insertAfter($(element).parents('div.dropify-wrapper'));
            } else if (element.is('select')) {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            } else {
                error.insertAfter($(element).parents('div.md-input-wrapper'));
            }
        },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

});
</script>
@endsection
