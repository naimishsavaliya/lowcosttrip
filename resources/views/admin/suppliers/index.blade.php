@extends('layouts.theme')
@section('content')
<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="{{ env('ADMIN_URL')}}home"><i class="material-icons">home</i></a></li>
            <li><span>Manage Suppliers</span></li>
        </ul>
        <div class="uk-navbar-flip p-t-8">
            <a href="{{ route('supplier.add') }}" class="md-btn md-btn-primary md-btn-mini md-btn-icon btn-add v-a-m">
                <i class="uk-icon-plus f-s-13"></i> Add New
            </a>
        </div>
    </div>

    <div id="page_content_inner" >
        <div class="uk-margin-medium-bottom">
            @include('admin.includes.alert')

            <table class="uk-table" id="supplier_tbl">
                <thead>
                    <tr>
                        <th style="width: 25px;">ID</th>
                        <th>Name</th>
                        <th>Email ID</th>
                        <th class="hidden-phone">Mobile No.</th>
                        <th class="hidden-phone">Location</th>
                        <th class="hidden-phone">Category</th>
                        <th class="hidden-phone">Follow-Up Note</th>
                        <th class="hidden-phone">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="update_status" class="uk-modal uk-hide uk-fade" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999;">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Update Status</h3>
        </div>
        <select id="selec_adv_s2_1" name="selec_adv_s2_1" class="select_no_search uk-width-large-1-1 p-t-4" data-md-select2 data-allow-clear="true" data-placeholder="">
            <option value="1">Status 1</option>
            <option value="2">Status 2</option>
            <option value="2">Status 3</option>
        </select>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button> <button type="button" class="md-btn md-btn-flat md-btn-flat-success uk-modal-close">Update</button>
        </div>
    </div>
</div>
<script type = "text/javascript" >
    $(function () {

        var categoryTable = $('#supplier_tbl').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 20,
            lengthMenu: [ 10, 20, 50, 75, 100 ],
            destroy: true,
            language: {search: ""},
//            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
//            "pagingType": "full_numbers", //full,full_numbers
            "ajax": {
                "url": "{{ route('supplier.data') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}, //, 'category_name': $("#Scategory_name").val()
                beforeSend: function () {

                    if (typeof categoryTable != 'undefined' && categoryTable.hasOwnProperty('settings')) {
                        categoryTable.settings()[0].jqXHR.abort();
                    }
                }
            },
            "columns": [
                {"data": "id", "name": "id"},
                {"data": "full_name", "name": "full_name"},
                {"data": "email", "name": "email"},
                {"data": "contact_no", "name": "contact_no"},
                {"data": "country_name", "name": "created_at"},
                {"data": "type", "name": "created_at"},
                {"data": "notes", "name": "notes"},
                {"data": "status", "name": "status", "render": function (data, type, row) {
                        if (data == 1) {
                            return '<span class="uk-badge uk-badge-success">Active</span>';
                        } else {
                            return '<span class="uk-badge uk-badge-danger">InActive</span>';
                        }
                    },
                },
                {"data": "action", 'sortable': false},
            ]
        });

        $('#supplier_tbl_filter').find('input').attr('placeholder', 'Search');

        $(categoryTable.table().container()).on('keyup', 'tfoot input', function () {
            categoryTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

        $(categoryTable.table().container()).on('change', 'tfoot select', function () {
            categoryTable
                    .column($(this).attr('colPos'))
                    .search(this.value)
                    .draw();
        });

    });
</script>

@endsection
