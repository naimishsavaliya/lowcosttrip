@extends('layouts.theme')

@section('style')
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}theme/assets/skins/dropify/css/dropify.css">
@endsection

@section('content')


<div id="page_content">

    <div id="top_bar">
        <ul id="breadcrumbs" style="display:inline-block;">
            <li><a href="./index.php"><i class="material-icons">home</i></a></li>
            <li><a href="{{ route('supplier.index') }}">Manage Suppliers</i></a></li>
            <li><span>Add / Edit Supplier</span></li>
        </ul>
    </div>

    <div id="page_content">
        <div id="page_content_inner" style="padding-top:0;">
            <form action="{{ route('supplier.store')}}" name="supplier_frm" class="" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ isset($supplier->id) ? $supplier->id : '' }}" />
                <div>
                    <div class="p-t-30">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-form-row">
                                <div class="uk-width-medium-1-12">
                                    <select id="select_demo_3" class="md-input">
                                        <option value="" selected>Choose type</option>
                                        <option value="a">Item A</option>
                                        <option value="b">Item B</option>
                                        <option value="c">Item C</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h3 class="heading_a">Contact Person</h3>
                        <div class="uk-grid" data-uk-grid-margin>

                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-4 p-r-50">
                                            <label>First Name</label>
                                            <input type="text" class="md-input" name="first_name" value="{{ isset($supplier->first_name) ? $supplier->first_name : '' }}" />
                                            <small class="text-danger">{{ $errors->first('first_name') }}</small>
                                        </div>
                                        <div class="uk-width-medium-1-4 p-r-50">
                                            <label>Last Name</label>
                                            <input type="text" class="md-input" name="last_name" value="{{ isset($supplier->last_name) ? $supplier->last_name : '' }}" />
                                            <small class="text-danger">{{ $errors->first('last_name') }}</small>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select id="select_demo_3" name="user_type"  class="md-input">
                                                <option value="" selected>Choose a category</option>
                                                @if(isset($supplier_types))
                                                @foreach($supplier_types as $supplier_type)
                                                <option value="{{ $supplier_type->supTypeId }}" {{ (isset($supplier->sup_type) && $supplier->sup_type == $supplier_type->supTypeId) ? 'selected' : '' }}>{{ $supplier_type->type }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Company Name</label>
                                            <input type="text" class="md-input" name="camapnay_name" value="{{ isset($supplier->camapnay_name) ? $supplier->camapnay_name : '' }}" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Contact No.</label>
                                            <input type="text" class="md-input tele-number" name="contact_no" value="{{ isset($supplier->contact_no) ? $supplier->contact_no : '' }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Office number</label>
                                            <input type="text" class="md-input tele-number" name="office_no" value="{{ isset($supplier->office_no) ? $supplier->office_no : '' }}" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Email</label>
                                            <input type="text" class="md-input" name="supp_email" value="{{ isset($supplier->supp_email) ? $supplier->supp_email : '' }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Address</label>
                                            <textarea cols="30" rows="4" class="md-input" name="address">{{ isset($supplier->address) ? $supplier->address : '' }}</textarea>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>GST Address</label>
                                            <textarea cols="30" rows="4" class="md-input" name="gst_address">{{ isset($supplier->gst_address) ? $supplier->gst_address : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>GST No</label>
                                            <input type="text" class="md-input" name="gst_number" value="{{ isset($supplier->gst_number) ? $supplier->gst_number : '' }}" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>GST Name</label>
                                            <input type="text" class="md-input" name="gst_name" value="{{ isset($supplier->gst_name) ? $supplier->gst_name : '' }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select class="md-input" name="country_id" id="country_id">
                                                <option value="" selected>Choose a Country</option>
                                                @if(isset($countrys))
                                                @foreach($countrys as $country)
                                                <option value="{{ $country->country_id }}" {{ (isset($supplier->country_id) && $supplier->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select class="md-input" name="state_id" id="state_id">
                                                <option value="" selected>Choose a State</option>
                                                @if(isset($states))
                                                @foreach($states as $state)
                                                <option value="{{ $state->state_id }}" {{ (isset($supplier->state_id) && $supplier->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select class="md-input" name="city_id" id="city_id">
                                                <option value="" selected>Choose a City</option>
                                                @if(isset($cities))
                                                @foreach($cities as $city)
                                                <option value="{{ $city->city_id }}" {{ (isset($supplier->city_id) && $supplier->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Pincode</label>
                                            <input type="text" class="md-input" name="pin_code" value="{{ isset($supplier->pin_code) ? $supplier->pin_code : '' }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <select id="select_demo_3" class="md-input">
                                                <option value="" selected>Supplier Created By</option>
                                                <option value="a">Item A</option>
                                                <option value="b">Item B</option>
                                                <option value="c">Item C</option>
                                            </select>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <select id="select_demo_3" class="md-input">
                                                <option value="" selected>Verified By</option>
                                                <option value="a">Item A</option>
                                                <option value="b">Item B</option>
                                                <option value="c">Item C</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="p-t-30">
                        <h3 class="heading_a">Login Information</h3>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>E-mail</label>
                                            <input type="text" class="md-input" name="email" value="{{ isset($supplier->email) ? $supplier->email : '' }}" />
                                            <small class="text-danger">{{ $errors->first('email') }}</small>
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Registered Mobile No.</label>
                                            <input type="text" class="md-input" name="register_mobile" value="{{ isset($supplier->register_mobile) ? $supplier->register_mobile : '' }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2 p-r-50">
                                            <label>Password</label>
                                            <input type="password" class="md-input" name="password" />
                                        </div>
                                        <div class="uk-width-medium-1-2 p-l-50">
                                            <label>Confirm Password</label>
                                            <input type="text" class="md-input" name="cfrm_password" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="p-t-30">
                        <h3 class="heading_a">
                            Branch Information
                            <a onclick="cloneData();" title="Add New Brach" class="btn btn-mini btn-success addPrice"><i class="material-icons md-36">add_box</i></a> 
                        </h3>
                        <div class="cloneDiv cloneData">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Name</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <label>Address</label>
                                                <input type="text" class="md-input" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Registered Mobile No.</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <select id="select_demo_3" class="md-input">
                                                    <option value="" selected>Choose a Country</option>
                                                    <option value="a">Item A</option>
                                                    <option value="b">Item B</option>
                                                    <option value="c">Item C</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <select id="select_demo_3" class="md-input">
                                                    <option value="" selected>Choose a State</option>
                                                    <option value="a">Item A</option>
                                                    <option value="b">Item B</option>
                                                    <option value="c">Item C</option>
                                                </select>
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <select id="select_demo_3" class="md-input">
                                                    <option value="" selected>Choose a City</option>
                                                    <option value="a">Item A</option>
                                                    <option value="b">Item B</option>
                                                    <option value="c">Item C</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Pincode</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <label>Contact Person</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-12">
                                    <div class="uk-form-row">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 p-r-50">
                                                <label>Contact No.</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                            <div class="uk-width-medium-1-2 p-l-50">
                                                <label>Google Map Link</label>
                                                <input type="text" class="md-input" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="appendDiv"></div>
                    </div>
                </div>

                <div>
                    <div class="p-t-30">
                        <h3 class="heading_a">Supplier Can Provide</h3>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-6">
                                            <input type="checkbox" name="can_provide[]" value="" id="can_provide_1" data-md-icheck />
                                            <label for="can_provide_1" class="inline-label">Tours</label>
                                        </div>
                                        <div class="uk-width-medium-1-6">
                                            <input type="checkbox" name="can_provide[]" value="" id="can_provide_2" data-md-icheck />
                                            <label for="can_provide_2" class="inline-label">Hotels</label>
                                        </div>
                                        <div class="uk-width-medium-1-6">
                                            <input type="checkbox" name="can_provide[]" value="" id="can_provide_3" data-md-icheck />
                                            <label for="can_provide_3" class="inline-label">Activities</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="heading_a">Certifications</h3>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        @if(isset($certificates))
                                        @foreach($certificates as $cer => $certificate)
                                        <div class="uk-width-medium-1-6">
                                            <input type="checkbox" name="certificates[]" value="{{ $certificate->cerId }}" id="certificate_{{ $cer }}" data-md-icheck />
                                            <label for="certificate_{{ $cer }}" class="inline-label">{{ $certificate->name}}</label>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-12">
                                            <textarea id="wysiwyg_ckeditor" cols="30" rows="20" name="notes">{{ isset($supplier->notes) ? $supplier->notes : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-2-6">
                                            <h3 class="heading_a uk-margin-small-bottom">Logo</h3>
                                            <input type="file" id="input-file-a" class="dropify" />
                                        </div>
                                        <div class="uk-width-medium-2-3">
                                            <h3 class="heading_a uk-margin-small-bottom">Office images</h3>
                                            <div id="file_upload-drop" class="uk-file-upload">
                                                <p class="uk-text">Drop file to upload</p>
                                                <p class="uk-text-muted uk-text-small uk-margin-small-bottom">or</p>
                                                <a class="uk-form-file md-btn">choose file<input id="file_upload-select" type="file"></a>
                                            </div>
                                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                                <div class="uk-progress-bar" style="width:0">0%</div>
                                            </div>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-12">
                                <div class="uk-form-row">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-6">
                                            <input type="radio" name="status" value="1" id="status_1" data-md-icheck />
                                            <label for="status_1" class="inline-label">Active</label>
                                        </div>
                                        <div class="uk-width-medium-1-6">
                                            <input type="radio" name="status" value="0" id="status_0" data-md-icheck />
                                            <label for="status_0" class="inline-label">In Active</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="form_hr">
                <div class="uk-grid uk-margin-medium-top uk-text-right">
                    <div class="uk-width-1-1">
                        <a href="{{ route('supplier.index') }}" class="md-btn md-btn-danger">Cancel</a>
                        <button type="submit" class="md-btn md-btn-primary">Save</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>
@endsection


@section('javascript')
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script><!-- ckeditor -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ckeditor/ckeditor.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/ckeditor/adapters/jquery.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_wysiwyg.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script type="text/javascript">
                                $(function () {

                                    $('#country_id').change(function () {
                                        var country_id = $(this).val();
                                        var url = '{{ route("state_by_country", ":country_id") }}';
                                        url = url.replace(':country_id', country_id);

                                        $.ajax({
                                            type: 'GET',
                                            url: url,
                                            data: {
                                                "_token": "{{ csrf_token() }}",
                                            },
                                            dataType: 'json',
                                            beforeSend: function (xhr) {

                                            },
                                            success: function (data, textStatus, jqXHR) {
                                                $('#state_id')
                                                        .find('option')
                                                        .remove();
                                                $('#state_id').append('<option value="">Choose</option>');
                                                $.map(data, function (item) {
                                                    $('#state_id').append('<option value="' + item.state_id + '">' + item.state_name + '</option>');
                                                });
                                                $("#state_id").trigger("chosen:updated");
                                                $("#state_id").trigger("liszt:updated");
                                            }
                                        });
                                    });

                                    $('#state_id').change(function () {
                                        var state_id = $(this).val();
                                        var url = '{{ route("city_by_state", ":state_id") }}';
                                        url = url.replace(':state_id', state_id);

                                        $.ajax({
                                            type: 'GET',
                                            url: url,
                                            data: {
                                                "_token": "{{ csrf_token() }}",
                                            },
                                            dataType: 'json',
                                            beforeSend: function (xhr) {

                                            },
                                            success: function (data, textStatus, jqXHR) {
                                                $('#city_id')
                                                        .find('option')
                                                        .remove();
                                                $('#city_id').append('<option value="">Choose</option>');
                                                $.map(data, function (item) {
                                                    $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
                                                });
                                                $("#city_id").trigger("chosen:updated");
                                                $("#city_id").trigger("liszt:updated");
                                            }
                                        });
                                    });

                                });

                                function removedata(obj) {
                                    var conf = confirm('Are you sure to delete?');
                                    if (conf) {
                                        $(obj).parents('.branch-items').remove();
                                    }
                                }

                                function cloneData() {
                                    var clonedata = $(".cloneData").clone();
                                    clonedata.removeClass("cloneData");

                                    var new_clone_data = '';
                                    new_clone_data += '<div class="branch-items">';
                                    new_clone_data += '<hr class="form_hr">';
                                    new_clone_data += '<a href="javascript:;" onclick="removedata(this);" title="Delete Price" class="btn btn-mini btn-danger"><i class="md-icon material-icons">delete</i></a>';
                                    new_clone_data += '<div class="clear clearfix"></div>';
                                    new_clone_data += $(clonedata).html();
                                    new_clone_data += '</div>';
                                    $(".appendDiv").append(new_clone_data);
                                }

</script>

@endsection