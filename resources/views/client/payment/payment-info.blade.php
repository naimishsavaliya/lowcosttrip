@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
    .book-div{
        border-bottom: 1px solid #E5E5E5;
        padding: 21px 0px;
    }
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }
    .panel .panel-body{ padding-top: 0; padding-bottom: 0;}
    .panel .panel-body .table{ margin-bottom: 0;}

    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }
    .table th, .table td  {border-right: solid 1px #DDD;}
    .table th {background-color: #eee;}
    .btn-warning{
        border-radius: 0;
    }
</style>
<div class="container">
@if(session()->has('message'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('log_error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session()->get('log_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row book-row">
        <div class="card">
            <div class="card-header col-md-12 book-div text-success">{{ __('Thank you for your booking !') }}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6">
                                <address>
                                    Name : {{$lead_info->name}}<br>
                                    Mobile : {{$lead_info->phone}}<br>
                                    Email : {{$lead_info->email}}<br>
                                    Address : <br>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <th width="25%"><strong>Booked tour</strong></th>
                                                <th width="25%"><strong>Confirm Departur Date</strong></th>
                                                <th width="10%"><strong>Adults</strong></th>
                                                <th width="10%"><strong>Child</strong></th>
                                                <th width="10%"><strong>Room</strong></th>
                                                <th width="20%" class="text-right"><strong>Subtotal(Rs.)</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                            <tr>
                                                <!-- date_format($lead_info->deprature_date, 'd M, y') -->
                                                <td>{{$lead_info->lead_id}}</td>
                                                <td>{{$lead_info->deprature_date}}</td>
                                                <td>{{$lead_info->adult}}</td>
                                                <td>{{$lead_info->child}}</td>
                                                <td>$10.99</td>
                                                <td class="text-right">25,000.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="5">Online Payment & Maha Bachat Discount(0%)</td>
                                                <td class="text-right">0.00</td>
                                            </tr> 

                                            <tr>
                                                <td class="text-right" colspan="5">Online Discount(%)</td>
                                                <td class="text-right">55.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="5">For Online Payment ({{env('ONLINE_PAYMENT_CHARGE','1.60')}}%) convenience</td>
                                                <td class="text-right">55.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="5">CGST ({{env('CGST','2.50')}}%)</td>
                                                <td class="text-right">55.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="5">SGST ({{env('CGST','2.50')}}%)</td>
                                                <td class="text-right">55.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="5">Sub Total</td>
                                                <td class="text-right">55.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="5">Paid</td>
                                                <td class="text-right">55.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="5">Grand Total</td>
                                                <td class="text-right">55.00</td>
                                            </tr>
                                           <?php /*
                                            <tr>
                                                <td class="text-right" colspan="5"></td>
                                                <td class="text-right"><a href="{{ route('send_to_atom') }}" class="btn btn-warning ">Pay now</a></td>
                                            </tr> */ ?>
                                            <form method="POST" id="payment_info_form" name="payment_info_form" action="{{ route('send_to_atom') }}">
                                            @csrf
                                                <tr>
                                                    <td class="text-right" colspan="5"></td>
                                                    <td class="text-right">
                                                        <input type="hidden" name="id" value="{{$lead_info->id}}"> 
                                                        <input type="hidden" name="lead_id" value="{{$lead_info->lead_id}}"> 
                                                        <button type="submit" class="btn btn-warning">Pay now</button>
                                                    </td>
                                            </tr>
                                            </form>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
    // LCT10007
    $(document).ready(function () {

    });
</script>
@endsection
