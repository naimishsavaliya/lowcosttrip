@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
</style>
<script type="text/javascript">
    cityData='';
</script>
<div class="container">
@if(session()->has('message'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('log_error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session()->get('log_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header col-md-12">Online Payment for lead #{{$lead->lead_id}} </div>
                <div class="card-body">
                    <form method="POST" id="payment_info_form" name="payment_info_form" action="{{ route('make.quotation.payment') }}">
                        @csrf
                        @if(isset($wallet_user_data) && $wallet_user_data!='')
                        <div class="col-md-12" style="padding-bottom: 45px;">
                            <div class="inner-breadcrumbs" style="background: #F9DDD2;">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="inner-breadcrumbs-block">
                                                <div class="inner-breadcrumbs-page-title">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div style="padding: 23px 0px;">
                                                            <input type="checkbox" id="is_wallet" name="is_wallet" checked>
                                                            <input type="hidden" id="wallet_amount" name="wallet_amount" value="{{(isset($wallet_user_data->current_balance) ? $wallet_user_data->current_balance :0)}}">
                                                            <span>Wallet Balance</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div style="padding: 23px 70px; text-align: right;">
                                                            <span>&#8377 {{(isset($wallet_user_data->current_balance) ? $wallet_user_data->current_balance :0)}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <input type="hidden" name="lead_id" value="{{$lead->id}}">
                        <input type="hidden" name="quote_id" value="{{$quotation->quaId}}">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-3" for="customer_name">Name:</label>
                                <div class="col-md-9">
                                    <input type="text"  name="customer_name" id="customer_name" class="form-control" placeholder="Customer Name" value="{{ (( isset($user_info->first_name) ) ? $user_info->first_name.' '.$user_info->last_name :'') }}">
                                    <small class="text-danger">{{ $errors->first('customer_name') }}</small>
                                </div>
                                
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-3" for="mobile_number">Mobile No.:</label>
                                <div class="col-md-9">
                                     <input type="text" name="mobile_number" id="mobile_number" class="form-control" placeholder="Mobile Number" value="{{ (( isset($user_info->phone_no) ) ? $user_info->phone_no :'') }}">
                                     <small class="text-danger">{{ $errors->first('mobile_number') }}</small>
                                </div>
                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-3" for="email">Email:</label>
                                <div class="col-md-9">
                                     <input type="text"  name="email" id="email" class="form-control" placeholder="Email" value="{{ (( isset($user_info->email) ) ? $user_info->email :'') }}">
                                    <small class="text-danger">{{ $errors->first('email') }}</small>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-3" for="quote_number">Quote No.:</label>
                                <div class="col-md-9">
                                    <input type="text" name="quote_number" id="quote_number" class="form-control" placeholder="Quotation number" value="{{ (( isset($quotation->quatation_id) ) ? $quotation->quatation_id :'') }}">
                                    <small class="text-danger">{{ $errors->first('quote_number') }}</small>
                                </div>
                            </div>
                        </div>
 

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-3" for="amount">Amount:</label>
                                <div class="col-md-5">
                                    <label class="radio-inline">
                                      <input type="radio" value="quot_amount" name="amount_type" checked>Quotation Amount
                                    </label>
                                    <input type="text" readonly name="amount" id="amount" class="form-control amount" placeholder="Amount" value="{{ (( isset($quotation->total_amount) ) ? round($quotation->total_amount, 2) :'') }}">
                                    <small class="text-danger">{{ $errors->first('amount') }}</small>
                                </div>
                                
                                <div class="col-md-4">
                                    <label class="radio-inline">
                                      <input type="radio"  value="custom_amount" name="amount_type">Custome Amount
                                    </label>
                                    <input type="text" readonly name="custome_amount" class="form-control amount" placeholder="Amount" id="custome_amount" value="{{ (( isset($quotation->total_amount) ) ? round($quotation->total_amount, 2) :'') }}">
                                    <small class="text-danger">{{ $errors->first('amount') }}</small>
                                </div>
                                 
                            </div>
                            <?php 
                            $grace = (($quotation->total_amount * env('ONLINE_PAYMENT_CHARGE','1.60')) / 100 ) + $quotation->total_amount;
                            ?>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-3" for="gross_amount">Gross Amount:</label>
                                <div class="col-md-9">
                                    <input type="text" readonly value="{{ round($grace,2) }}" name="gross_amount" id="gross_amount" class="form-control" placeholder="Gross Amount">
                                    <small class="text-danger">{{ $errors->first('gross_amount') }}</small>
                                    <input type="hidden" id="online_change" value="{{env('ONLINE_PAYMENT_CHARGE','1.60')}}">
                                     <span class="col-form-label clearfix">( For Online Payment ({{env('ONLINE_PAYMENT_CHARGE','1.60')}}%) convenience Will Be Added.)</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-3" for="refered_by">Refered By:</label>
                                <div class="col-md-9">
                                    <input type="text" readonly name="refered_by" id="refered_by" class="form-control" placeholder="Refered By" value="{{ (( isset($assign_to) && isset($assign_to->first_name) )? $assign_to->first_name.' '.$assign_to->last_name : '')}}" >
                                    <small class="text-danger">{{ $errors->first('refered_by') }}</small>
                                    <span class="col-form-label">&nbsp;</span>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-3" for="remarks">Remarks:</label>
                                <div class="col-md-9">
                                    <textarea class="form-control " name="remarks" id="remarks" rows="3" placeholder="Remarks">{{ old('remarks')}}</textarea>
                                    <small class="text-danger">{{ $errors->first('remarks') }}</small>
                                </div>
                            </div>
                        </div>
                                                                        
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6 text-center col-md-offset-3">
                            <button type="submit" class="btn btn-primary submit-btn gr2">
                                {{ __('MAKE PAYMENT') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
    $( "#amount" ).trigger( "change" );
});
$(function () {
    $("#payment_info_form").validate({
        rules: {
            customer_name:'required',
            mobile_number:'required',
            email:'required',
            quote_number:'required',
            // currency_code:'required',
            amount: {
              required: true,
              number: true
            },
            gross_amount:'required',
            remarks: {
                required:true,
                // minlength:8
            },
        },
        messages: {
            customer_name:'Please enter customer name',
            mobile_number:'Please enter mobile number',
            email:'Please enter email',
            quote_number:'Please enter quotation number',
            // currency_code:'Please enter currency code',
            amount:'Please enter amount',
            gross_amount:'Please enter gross amount',
            remarks:'Please enter remarks',
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    
    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });

});
</script>
@endsection
