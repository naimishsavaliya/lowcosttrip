@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
</style>
<script type="text/javascript">
    cityData='';
</script>
<div class="container">
@if(session()->has('message'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('log_error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session()->get('log_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header col-md-12">{{ __('Online Payment for Low Cost Trip') }}</div>
                <div class="card-body">
                    <form method="POST" id="payment_info_form" name="payment_info_form" action="{{ route('form_submit') }}">
                        @csrf
                        <?php //dd(isset($wallet_user_data) && $wallet_user_data=='');?>
                        @if(isset($wallet_user_data) && $wallet_user_data!='')
                        <div class="col-md-12" style="padding-bottom: 45px;">
                            <div class="inner-breadcrumbs" style="background: #F9DDD2;">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="inner-breadcrumbs-block">
                                                <div class="inner-breadcrumbs-page-title">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div style="padding: 23px 0px;">
                                                            <input type="checkbox" id="is_wallet" name="is_wallet" checked>
                                                            <input type="hidden" id="wallet_amount" name="wallet_amount" value="{{(isset($wallet_user_data->current_balance) ? $wallet_user_data->current_balance :0)}}">
                                                            <span>Wallet Balance</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div style="padding: 23px 70px; text-align: right;">
                                                            <span>&#8377 {{(isset($wallet_user_data->current_balance) ? $wallet_user_data->current_balance :0)}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="form-group col-md-6">
                            <input type="text" value="{{ old('customer_name')}}" name="customer_name" id="customer_name" class="form-control" placeholder="Customer Name ">
                            <small class="text-danger">{{ $errors->first('customer_name') }}</small>
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" value="{{ old('mobile_number')}}" name="mobile_number" id="mobile_number" class="form-control" placeholder="Mobile Number">
                            <small class="text-danger">{{ $errors->first('mobile_number') }}</small>
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" value="{{ old('email')}}" name="email" id="email" class="form-control" placeholder="Email">
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" value="{{ old('tour_code')}}" name="tour_code" id="tour_code" class="form-control" placeholder="Tour Code">
                            <small class="text-danger">{{ $errors->first('tour_code') }}</small>
                        </div>
                        <?php /*
                        <div class="form-group col-md-6">
                            <input type="text" value="{{ old('currency_code')}}" name="currency_code" id="currency_code" class="form-control" placeholder="Currency Code">
                            <small class="text-danger">{{ $errors->first('currency_code') }}</small>
                        </div> */ ?>
    
                        <div class="form-group col-md-6">
                            <input type="text" value="{{ old('amount')}}" name="amount" id="amount" class="form-control" placeholder="Amount">
                            <small class="text-danger">{{ $errors->first('amount') }}</small>
                            <span class="col-form-label clearfix">( For Online Payment ({{env('ONLINE_PAYMENT_CHARGE','1.60')}}%) convenience Will Be Added.)</span>
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" readonly value="{{ old('gross_amount')}}" name="gross_amount" id="gross_amount" class="form-control" placeholder="Gross Amount">
                            <small class="text-danger">{{ $errors->first('gross_amount') }}</small>
                            <input type="hidden" id="online_change" value="{{env('ONLINE_PAYMENT_CHARGE','1.60')}}">
                        </div>
                        
                        <div class="form-group col-md-6">
                            <input type="text" value="{{ old('refered_by')}}" name="refered_by" id="refered_by" class="form-control" placeholder="Refered By">
                            <small class="text-danger">{{ $errors->first('refered_by') }}</small>
                            <span class="col-form-label">&nbsp;</span>
                        </div>  

                        <div class="clearfix"></div>
                        <div class="form-group col-md-12">
                            <textarea class="form-control " name="remarks" id="remarks" rows="3" placeholder="Remarks">{{ old('remarks')}}</textarea>
                            <small class="text-danger">{{ $errors->first('remarks') }}</small>
                        </div>
                        <div class="clearfix"></div>
                        <?php /*
                        @if(isset($wallet_user_data->current_balance) && $wallet_user_data->current_balance!='')
                        <div class="form-group col-md-6">
                        <input type="checkbox" value="{{(isset($wallet_user_data->current_balance) ? $wallet_user_data->current_balance :0)}}" id="wallet_balance" checked>&nbsp;
                        <span>Wallet Balance ({{(isset($wallet_user_data->current_balance) ? $wallet_user_data->current_balance :0)}})</span>
                        </div>
                        <div class="clearfix"></div>
                        @endif
                        */?>
                        <div class="form-group col-md-6 text-center col-md-offset-3">
                            <button type="submit" class="btn btn-primary submit-btn gr2">
                                {{ __('BOOK NOW') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>

<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>


<script type="text/javascript">
$(function () {
    $("#payment_info_form").validate({
        rules: {
            customer_name:'required',
            mobile_number:'required',
            email:'required',
            tour_code:'required',
            // currency_code:'required',
            amount: {
              required: true,
              number: true
            },
            gross_amount:'required',
            remarks: {
                required:true,
                // minlength:8
            },
        },
        messages: {
            customer_name:'Please enter customer name',
            mobile_number:'Please enter mobile number',
            email:'Please enter email',
            tour_code:'Please enter tour code',
            // currency_code:'Please enter currency code',
            amount:'Please enter amount',
            gross_amount:'Please enter gross amount',
            remarks:'Please enter remarks',
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $('#amount').change(function () {
        var amt = $('#amount').val();
        var online_charge = $('#online_change').val();
        var servicetax =  ( ((parseFloat(amt) * parseFloat(online_charge)) / 100 ) +  parseFloat(amt) ).toFixed(2);
        if(servicetax > 0){
            $("#gross_amount").val(servicetax);
        }
    });

    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });

    // $('#state_id').change(function () {
    //     var state_id = $(this).val();
    //     var url = '{{ route("city_by_state_front", ":state_id") }}';
    //     url = url.replace(':state_id', state_id);
    //     $.ajax({
    //         url: url,
    //         type: 'GET',
    //         data: {state_id:state_id},
    //         dataType: 'json',
    //         headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
    //         beforeSend: function (xhr) {

    //         },
    //         success: function (data, textStatus, jqXHR) {
    //             $('#city_id')
    //                     .find('option')
    //                     .remove();
    //             $('#city_id').append('<option value="">Choose City</option>');
    //             $.map(data, function (item) {
    //                 $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
    //             });
    //             $("#city_id").trigger("chosen:updated");
    //             $("#city_id").trigger("liszt:updated");
    //         }
    //     });
    // });
});
</script>
@endsection
