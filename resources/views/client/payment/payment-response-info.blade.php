@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
    .book-div{
        border-bottom: 1px solid #E5E5E5;
        padding: 21px 0px;
    }
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }
    .panel .panel-body{ padding-top: 0; padding-bottom: 0;}
    .panel .panel-body .table{ margin-bottom: 0;}

    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }
    .table th, .table td  {border-right: solid 1px #DDD;}
    .table th {background-color: #eee;}
    .btn-warning{
        border-radius: 0;
    }
</style>
<script>
    var cityData = '';
</script>
<div class="container">
@if(session()->has('message'))
    <div class="row">
        <div class="col-md-12" style="margin-top: 15px;">
            <div class="alert {{ session()->get('alert-class') }}">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('log_error'))
    <div class="row">
        <div class="col-md-12" style="margin-top: 15px;">
            <div class="alert alert-danger">
                {{ session()->get('log_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row book-row">
        <div class="card">
            <?php //echo number_format(($transaction_data->is_wallet=='1')?($transaction_data->is_payment_type!='2')?$transaction_data->paid_amount+$transaction_data->wallet_amount:$transaction_data->paid_amount:$transaction_data->paid_amount, 2)?>
            <div class="card-header col-md-12 book-div text-success">Thank you for your booking. {{ ((!empty($payment_data) && isset($payment_data)) ? 'Your payment of Rs.'.number_format(($transaction_data->is_wallet=='1')?($transaction_data->is_payment_type!='2')?$transaction_data->paid_amount+$transaction_data->wallet_amount:$transaction_data->paid_amount:$transaction_data->paid_amount, 2).' is successfull.' : '')}}
            <button type="button" id="hrefPrint" onclick="PrintElem('.card-body')" class="btn btn-danger pull-right">Print</button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                            <img src="{{ env('APP_PUBLIC_URL')}}client/images/Web_LCT.png" class="img-responsive center-block">
                    </div>
                    <div class="col-md-12 text-center">
                        IQ Comtech Pvt Ltd , 1205,Sai Indu Tower , Lbs marg, Bhandup, Mumbai - 78.
                    </div>
                    <div class="col-md-12 text-center">
                        +918182838485,info@lowcosttrip.com
                    </div>
                    <!-- <div class="col-md-12 text-center">
                        info@lowcosttrip.com
                    </div> -->
                </div>
                <hr/>
                <div style="height: -52px;"></div>
                <?php
                    $payment_mode='';
                    $receipt_no='';
                    $payment_date='';
                    if(!empty($payment_data) && isset($payment_data)){
                        $receipt_no=isset($payment_data->receipt_no)?$payment_data->receipt_no:'-';
                        $payment_date=isset($payment_data->created_at)?date('d M Y', strtotime($payment_data->created_at)):'-';
                        $payment_mode=isset($payment_data->payment_mode)?$payment_data->payment_mode:'-';
                    } 
                ?>
                <tr>
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr> 
                                <td class="invoice-text" style="text-align: center;font-size: 30px; padding: 25px">
                                    <strong>Receipt # : {{$receipt_no}}</strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr> 
                                <td class="invoice-text" style="text-align: left;font-size: 15px;">
                                   <strong>Date.:</strong> <br>{{$payment_date}}
                                </td>
                                <td class="invoice-text" style="text-align: right;font-size: 15px">
                                   <strong>Payment Mode.:</strong> <br>Online
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <div style="height: 20px;"></div>
                <?php
                    $country_name='';
                    $state_name='';
                    $city_name='';
                    $pin_code='';
                    if(!empty($lead_info) && isset($lead_info)){
                        $country_name=isset($lead_info->country_name)?$lead_info->country_name:'-';
                        $state_name=isset($lead_info->state_name)?$lead_info->state_name:'-';
                        $city_name=isset($lead_info->city_name)?$lead_info->city_name:'-';
                        $pin_code=isset($lead_info->pin_code)?$lead_info->pin_code:'-';
                    } 
                ?>
                <div class="row">
                
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <table class="table table-bordered">
                            <tr>
                                <td><b>{{ isset($lead_info->name) ? $lead_info->name : '' }}</b></td>
                            </tr>
                            <tr>
                                <td>{{ $lead_info->city_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ $lead_info->city_name .', '. $lead_info->pin_code }}</td>
                            </tr>
                            <tr>
                                <td>{{ $lead_info->state_name .', '. $lead_info->country_name }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <table class="table table-bordered">
                            <tr>
                                <th colspan="2"><b>Seller GST Details</b></th>
                            </tr>
                            <tr>
                                <td><b>GSTIN :</b></td>
                                <td>{{ env('GSTIN')}}</td>
                            </tr>
                            <tr>
                                <td><b>CIN :</b></td>
                                <td>{{env('CIN')}}</td>
                            </tr>
                            <tr>
                                <td><b>PAN No. :</b></td>
                                <td>{{env('PAN_NO')}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
                    $quotation_subject='-';
                    if(!empty($quotation) && isset($quotation)){
                        $quotation_subject=isset($quotation->subject)?$quotation->subject:'-';
                    } 
                ?>
                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12 col-sm-12  col-xs-12   ">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="45%">Particular</th>
                                                <th width="5%">Rate(INR)</th>
                                                <th width="5%">Qty/Night/Room</th>
                                                <th width="10%">Discount</th>
                                                <th width="10%">GST (%)</th>
                                                <th width="10%">HSN/SAC</th>
                                                <th width="15%">Amount(INR)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($particular))

                                            @php 
                                            $total_cost = $final_cost = $cgst = $igst = $sgst  = 0; 
                                            @endphp

                                            @foreach($particular as $par_key=>$par_value)

                                            @php
                                            $linetotal = $par_value->qty * $par_value->cost;
                                            if(isset($par_value->discount)){
                                            $linetotal = $linetotal - $par_value->discount;
                                            }
                                            $total_cost = $total_cost + $linetotal;

                                            $cgst = $cgst + (($par_value->cgst > 0)?  (($linetotal *  $par_value->cgst) / 100) :0);
                                            $sgst = $sgst + (($par_value->sgst > 0)?  (($linetotal *  $par_value->sgst) / 100) :0);
                                            $igst = $igst + (($par_value->igst > 0)?  (($linetotal *  $par_value->igst) / 100) :0);

                                            @endphp
                                            <tr class="table-middle">
                                                <td>{{ $par_value->item_title }}</td>
                                                <td>{{ $par_value->cost }}</td>
                                                <td class="text-right">{{ $par_value->qty }}</td>
                                                <td class="text-right">{{ $par_value->discount }}</td>
                                                <td class="text-right">{{ number_format($par_value->gst , 2) }}</td>
                                                <td class="text-right">0</td>
                                                <td class="text-right">{{ number_format($linetotal, 2) }}</td>
                                            </tr>
                                            @endforeach
                                            <tr class="table-middle">
                                                <td colspan="4" rowspan="5">
                                                <td colspan="2" class="text-right"><b>Sub Total</b></td>
                                                <td class="text-right">{{ number_format($total_cost, 2) }}</td>
                                            </tr>
                                            <tr class="table-middle">
                                                <td colspan="2" class="text-right"><b>CGST (+)</b></td>
                                                <td class="text-right">{{ number_format($cgst , 2) }}</td>
                                            </tr>
                                            <tr class="table-middle">
                                                <td colspan="2" class="text-right"><b>SGST (+) </b></td>
                                                <td class="text-right">{{ number_format($sgst , 2) }}</td>
                                            </tr>
                                            <tr class="table-middle">
                                                <td colspan="2" class="text-right"><b>IGST (+) </b></td>
                                                <td class="text-right">{{ number_format($igst , 2) }}</td>
                                            </tr>
                                            <tr class="table-middle">
                                                <td colspan="2" class="text-right"><b>Grand Total (INR)</b></td>
                                                 <?php $total_amount = $total_cost + $cgst + $sgst + $igst; ?>
                                                <td class="text-right">{{ number_format(($total_amount) , 2) }}</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
    // LCT10007
    function PrintElem(elem) {
        Popup(jQuery(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        mywindow.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />');  
        mywindow.document.write('<style type="text/css"></style></head><body>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.document.close();
        mywindow.print();                        
    }

</script>
@endsection
