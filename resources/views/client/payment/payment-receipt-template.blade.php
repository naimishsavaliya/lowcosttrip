
<style>
    .invoice-box {
        /*max-width: 800px;*/
        /*margin: auto;*/
        padding: 10px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 15px;
        line-height: 18px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #000000;
    }

    .invoice-box table {
        width: 100%;
        /*line-height: inherit;*/
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 10px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    .txt-c {
        text-align: center;
    }
    .txt-r {
        text-align: right;
    }

    .txt-l {
        text-align: left !important;
    }
    .invoice-text{
        font-size:25px;
        text-align: center;
    }
    .address{
        font-size:10px;
        padding: 7px;
        text-align: left !important;
        vertical-align: top !important;
    }
    .date{
        font-size:10px;
        padding: 7px;
        text-align: left;
    }
    .mode{
        font-size:10px;
        padding: 7px;
        text-align: left;
    }
    .br-1{
        border:1px solid #000; 
        padding:0px; 
        font-size:10px;
        border-spacing: 0;
    }
    .user-info{
        line-height: 8px;
    }
    .product-list td{
        border:1px solid #000; 
        padding:0px; 
        font-size:10px;
        border-spacing: 0;
    }
    .total-amount{
        text-align: right; line-height: 8px; border-bottom: none !important; border-top: none !important;
    }
    .total-amt{
        text-align: right; line-height: 8px;
    }
    .declaration{
        border:1px solid #000; 
        padding:10px 0px; 
        font-size:10px;
        border-spacing: 0;
    }
    .address-top td{
        padding: 5px;
        vertical-align: top
    }
</style>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>A simple, clean, and responsive HTML invoice template</title> 
    </head>
    <body>
        <div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td colspan="2">
                        <table class="address-top">
                            <tr>
                                <td style="text-align: center;   padding: 2px;">
                                    <a  target="_blank" href="http://lowcosttrip.com/"><img src="https://www.lowcosttrip.com/public/new-theme/images/logo.jpg" style="max-width:300px; max-width:300px;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;   padding: 2px;">
                                    IQ Comtech Pvt Ltd , 1205,Sai Indu Tower , Lbs marg, Bhandup, Mumbai - 78.
                                </td>
                            </tr> 
                            <tr>
                                <td style="text-align: center;   padding: 2px;">
                                    +918182838485,info@lowcosttrip.com
                                </td>
                            </tr>
                            <!-- <tr>
                                <td style="text-align: center;   padding: 2px;">
                                    info@lowcosttrip.com
                                </td>
                            </tr> -->
                        </table>
                    </td>
                </tr>
                <!-- <tr class="top">
                    <td colspan="2" style="border-top: 1px solid #DDD">
                    </td>
                </tr> -->
                <?php /*
                <tr class="top">
                    <td width="50%">
                        <table class="address-top">
                            <?php
                                $payment_mode='';
                                $receipt_no='';
                                $payment_date='';
                                if(!empty($payment_data) && isset($payment_data)){
                                    $receipt_no=isset($payment_data->receipt_no)?$payment_data->receipt_no:'-';
                                    $payment_date=isset($payment_data->created_at)?date('d M Y', strtotime($payment_data->created_at)):'-';
                                    $payment_mode=isset($payment_data->payment_mode)?$payment_data->payment_mode:'-';
                                } 
                            ?>
                            <tr>
                                <td style="background-color: #eee; padding: 2px;">Receipt#</td>
                                <td style="padding: 2px;"><strong>{{$receipt_no}}</strong></td>
                            </tr>
                            <tr>
                                <td style="background-color: #eee; padding: 2px;">Date</td>
                                <td style="padding: 2px;">{{$payment_date}}</td>
                            </tr>
                            <tr>
                                <td style="background-color: #eee; padding: 2px;">Payment Mode</td>
                                <td style="padding: 2px;">{{$payment_mode}}</td>
                            </tr>
                        </table>
                    </td>
                </tr> 
                */ ?>
                <?php
                    $payment_mode='';
                    $receipt_no='';
                    $payment_date='';
                    if(!empty($payment_data) && isset($payment_data)){
                        $receipt_no=isset($payment_data->receipt_no)?$payment_data->receipt_no:'-';
                        $payment_date=isset($payment_data->created_at)?date('d M Y', strtotime($payment_data->created_at)):'-';
                        $payment_mode=isset($payment_data->payment_mode)?$payment_data->payment_mode:'-';
                    } 
                ?>
                <tr>
                    <td colspan="2" style="border-top:1px solid #DDD; padding: 25px;">
                        <table>
                            <tr> 
                                <td class="invoice-text">
                                    Receipt # : {{$receipt_no}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="information">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td class="date">
                                    <strong>Date:</strong><br>
                                    {{$payment_date}}
                                </td>

                                <td class="mode">
                                    <strong>Payment Mode:</strong><br>
                                    Online
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php
                    $country_name='';
                    $state_name='';
                    $city_name='';
                    $pin_code='';
                    if(!empty($lead_info) && isset($lead_info)){
                        $country_name=isset($lead_info->country_name)?$lead_info->country_name:'-';
                        $state_name=isset($lead_info->state_name)?$lead_info->state_name:'-';
                        $city_name=isset($lead_info->city_name)?$lead_info->city_name:'-';
                        $pin_code=isset($lead_info->pin_code)?$lead_info->pin_code:'-';
                    } 
                ?>
                <tr>
                    <?php /*
                    <td width="50%"> 
                        <table class="user-info" cellspacing="0">
                            <tr>
                                <td class="br-1"><strong>{{ isset($lead_info->name) ? $lead_info->name : '' }}</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1">
                                    <?php 
                                        if($country_name!=''){
                                            $country_name.=',';
                                        }
                                        if($state_name!=''){
                                            $state_name.=',';
                                        }
                                        if($city_name!=''){
                                            $city_name.=',';
                                        }
                                    ?>
                                    {{ $country_name }}{{ $state_name }}{{ $city_name }}{{ $pin_code }}
                                </td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ isset($lead_info->phone) ? $lead_info->phone : '' }}</td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ isset($lead_info->email) ? $lead_info->email : '' }}</td>
                            </tr>
                        </table>
                    </td>
                    */ ?>
                    <td width="50%"> 
                        <table class="user-info" cellspacing="0">
                            <tr>
                                <td class="br-1"><strong>{{ isset($lead_info->name) ? $lead_info->name : '' }}</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ $lead_info->city_name }}</td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ $lead_info->city_name .', '. $lead_info->pin_code }}</td>
                            </tr>
                            <tr>
                                <td class="br-1">{{ $lead_info->state_name .', '. $lead_info->country_name }}</td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%">
                        <table class="user-info"  cellspacing="0">
                            <tr>
                                <td class="br-1 txt-c" colspan="2"><strong>Seller GST Details</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1">GSTIN :</td>
                                <td class="br-1 txt-l">{{ env('GSTIN')}}</td>
                            </tr>
                            <tr>
                                <td class="br-1">CIN :</td>
                                <td class="br-1 txt-l">{{env('CIN')}}</td>
                            </tr>
                            <tr>
                                <td class="br-1">PAN No. :</td>
                                <td class="br-1 txt-l">{{env('PAN_NO')}}</td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="product-list"  cellspacing="0">
                            <tr>
                                <td width="40%" style="text-align: left"><strong>Particular</strong></td>
                                <td width="10%"><strong>Rate(INR) </strong></td>
                                <td width="10%"><strong>Qty/Night/Room</strong></td>
                                <td width="10%"><strong>Discount </strong></td>
                                <td width="10%"><strong>GST(%)</strong></td>
                                <td width="10%"><strong>HSN/SAC </strong></td>
                                <td width="10%"><strong>Amount(INR) </strong></td>
                            </tr>
                            @if(isset($particular))

                            @php 
                            $total_cost = $final_cost = $cgst = $igst = $sgst  = 0; 
                            @endphp

                            @foreach($particular as $par_key=>$par_value)

                            @php
                            $linetotal = $par_value->qty * $par_value->cost;
                            if(isset($par_value->discount)){
                            $linetotal = $linetotal - $par_value->discount;
                            }
                            $total_cost = $total_cost + $linetotal;

                            $cgst = $cgst + (($par_value->cgst > 0)?  (($linetotal *  $par_value->cgst) / 100) :0);
                            $sgst = $sgst + (($par_value->sgst > 0)?  (($linetotal *  $par_value->sgst) / 100) :0);
                            $igst = $igst + (($par_value->igst > 0)?  (($linetotal *  $par_value->igst) / 100) :0);

                            @endphp
                            <tr>
                                <td style="text-align: left">{{ $par_value->item_title }}</td>
                                <td class="txt-r">{{ $par_value->cost }}</td>
                                <td class="txt-r">{{ $par_value->qty }}</td>
                                <td class="txt-r">{{ $par_value->discount }}</td>
                                <td class="txt-r">{{ number_format($par_value->gst , 2) }}</td>
                                <td class="txt-r">-</td>
                                <td class="txt-r">{{ number_format($linetotal, 2) }}</td>
                            </tr>
                            @endforeach

                            <tr>
                                <td style="text-align: right; line-height: 8px; border-bottom: none; "  colspan="6">Sub Total:</td>
                                <td class="total-amt">{{ number_format($total_cost, 2) }}</td>
                            </tr>
                            <tr>
                                <td class="total-amount"  colspan="6">CGST (+):</td>
                                <td  class="total-amt">{{ number_format($cgst , 2) }}</td>
                            </tr>
                            <tr>
                                <td class="total-amount"  colspan="6">SGST (+):</td>
                                <td  class="total-amt">{{ number_format($sgst , 2) }}</td>
                            </tr>
                            <tr>
                                <td class="total-amount"   colspan="6">IGST (+):</td>
                                <td   class="total-amt">{{ number_format($igst , 2) }}</td>
                            </tr>
                            <tr>
                                <?php $total_amount = $total_cost + $cgst + $sgst + $igst; ?>
                                <td class="total-amount"   colspan="6">Grand Amount (INR):</td>
                                <td  class="total-amt">{{ number_format(($total_amount) , 2) }}</td>
                            </tr>
                            <tr>
                                <td colspan="7" >Grand Amount (in words): {{number_to_word($total_amount)}} </td>
                            </tr>
                            @endif
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2" > 
                        <table class="declaration" cellspacing="0">
                            <tr>
                                <td class="br-1"><strong>Declaration</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1"> -
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php /*
                <tr>
                    <td colspan="2" > 
                        <table class="declaration" cellspacing="0">
                            <tr>
                                <td class="br-1"><strong>Notes</strong></td>
                            </tr>
                            <tr>
                                <td class="br-1">simply dummy text of the printing and typesetting industry<br>
                                    simply dummy text of the printing and typesetting industry
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                */?>
            </table>
        </div>
    </body>
</html>