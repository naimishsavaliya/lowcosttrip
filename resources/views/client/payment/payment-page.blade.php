@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
    .book-div{
        border-bottom: 1px solid #E5E5E5;
        padding: 21px 0px;
    }
    h3{
        font-size: 18px;
        margin:0;
        padding: 10px;
    }
    .form-control{
        width: auto;
    }
    .bdr-lft{
        border-left:1px solid #E5E5E5; 
    }
    .book-row{
        padding: 60px 0px;
    }
    .pd-l-0{
        padding-left: 0px;
    }
</style>
<script type="text/javascript">
    cityData='';
</script>
<div class="container">
@if(session()->has('message'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('log_error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session()->get('log_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row book-row">
        <div class="card">
            <div class="card-header col-md-12 book-div">{{ __('Online Payment') }}</div>
            <div class="card-body">
                <div class="col-md-5">
                    <h3 class="pd-l-0">Already Booked Tour Balance Payment</h3>
                    <br>
                    <div class="message">
                        
                    </div>
                    <form method="POST" id="booking_form" name="payment_info_form">
                        <div class="row form-group">
                            <label class="col-md-6 col-sm-6 col-xs-12" for="booking_id">Enter Booking No.</label>
                            <input type="text" name="booking_id" value="{{ old('booking_id')}}" class="form-control col-md-6 col-sm-6 col-xs-1" id="booking_id">
                            <small class="text-danger">{{ $errors->first('booking_id') }}</small>
                        </div>
                        <?php /*
                        <div class="row form-group otp_textbox hidden">
                            <label class="col-md-6 col-sm-6 col-xs-6">Enter OTP</label>
                            <input type="text" class="form-control col-md-6 col-sm-6 col-xs-6"  id="booking">
                        </div> */ ?>
                        <div class="row form-group" >
                            <label class="col-md-6 col-sm-6 col-xs-6" for="booking">&nbsp;</label>
                            <button type="submit" class="btn btn-primary submit_btn">
                                {{ __('Send OTP') }}
                            </button>
                        </div>
                    </form>

                    <form method="POST" id="otp_form" class="hidden" name="otp_form">
                        <div class="row form-group otp_textbox ">
                            <label class="col-md-6">Enter OTP</label>
                            <input type="text" class="form-control col-md-6" name="otp"  id="booking">
                        </div>
                        <div class="row form-group" >
                            <label class="col-md-6" for="booking">&nbsp;</label>
                            <button type="submit" class="btn btn-primary submit_btn">
                                {{ __('Submit') }}
                            </button>
                        </div>
                    </form>

                </div>
                <div class="col-md-3">
                    <h3 class="pd-l-0">Pay Without Booking Id</h3>
                    <br>
                    <div class="form-group col-md-6 pd-l-0">
                        <a href="{{ route('user-form') }}" class="btn btn-info ">Pay Without Booking</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-bordered">
                        <tr>
                            <td><strong>Bank</strong></td>
                            <td>HDFC Bank</td>
                        </tr>
                        <tr>
                            <td><strong>Account No</strong></td>
                            <td>50200031848562</td>
                        </tr>
                        <tr>
                            <td><strong>Account Type</strong></td>
                            <td>Current Account </td>
                        </tr>
                        <tr>
                            <td><strong>Ac Name</strong></td>
                            <td>IQ Comtech Private limited</td>
                        </tr>
                        <tr>
                            <td><strong>IFSC Code</strong></td>
                            <td>HDFC0000967</td>
                        </tr>
                        <tr>
                            <td><strong>Bank Branch</strong></td>
                            <td>Bhandup (W), Mumbai</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">


// LCT10007
$(document).ready(function () {

    $("#booking_form").submit(function () {
        $.ajax({
            url: '{{ route("check-booking-id") }}',
            type: "POST",
            data: $("#booking_form").serialize(),
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
            beforeSend: function () {
                $('.validate-msg').html("").closest('.booking-form-padding').hide();
                $(".submit_btn").attr("disabled", true).css("opacity", "0.4");
            },
            success: function (data) {
                if (data.status == true) { 
                    $('.otp_textbox').removeClass('hidden');
                    $('.message').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> '+ data.message +'</div>').show();
                    $('#booking_form').addClass('hidden');
                    $('#otp_form').removeClass('hidden');
                } else {
                    $('.message').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Error!</strong> '+ data.message +'</div>').show();
                }
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                    $('.message').addClass('hidden');
                });
                $(".submit_btn").attr("disabled", false).css("opacity", "1");
            },
        });
        return false;
    }); 

    $("#otp_form").submit(function () {
        $.ajax({
            url: '{{ route("check-otp") }}',
            type: "POST",
            data: $("#otp_form").serialize(),
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
            beforeSend: function () {
                $('.message').removeClass('hidden').html('');
                $('.validate-msg').html("").closest('.booking-form-padding').hide();
                $(".submit_btn").attr("disabled", true).css("opacity", "0.4");
            },
            success: function (data) {
                if (data.status == true) { 
                    var booking_id = $('#booking_id').val();
                    window.location.href = '{{ url("payment-info") }}'+ '/'+booking_id;
                } else {
                    $('.message').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Error!</strong> '+ data.message +'</div>').show();
                    $(".submit_btn").attr("disabled", false).css("opacity", "1");
                }
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                    $('.message').addClass('hidden');
                });
                $(".submit_btn").attr("disabled", false).css("opacity", "1");

            },
        });
        return false;
    }); 

});


$(function () {
    $("#payment_info_form").validate({
        rules: {
            customer_name:'required',
        },
        messages: {
            customer_name:'Please enter customer name',
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});

</script>
@endsection
