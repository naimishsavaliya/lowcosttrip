@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
    .book-div{
        border-bottom: 1px solid #E5E5E5;
        padding: 21px 0px;
    }
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }
    .panel .panel-body{ padding-top: 0; padding-bottom: 0;}
    .panel .panel-body .table{ margin-bottom: 0;}

    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }
    .table th, .table td  {border-right: solid 1px #DDD;}
    .table th {background-color: #eee;}
    .btn-warning{
        border-radius: 0;
    }
</style>
<div class="container">
@if(session()->has('message'))
    <div class="row" style="margin-top: 30px;">
        <div class="col-md-12">
            <div class="alert {{ session()->get('alert-class') }}">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('log_error'))
    <div class="row" style="margin-top: 30px;">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session()->get('log_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row book-row">
        <div class="card">
            <div class="card-header col-md-12 book-div text-success" style="margin-top: -1px;">{{ __('Thank you for your payment !') }}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <th class="text-center" colspan="2">Payment Information</th>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td>{{$payment_info->user_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Mobile</td>
                                                <td>{{$payment_info->user_email}}</td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td>{{$payment_info->user_mobile_no}}</td>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td>{{$payment_info->user_address}}</td>
                                            </tr>
                                            <tr>
                                                <td>Transaction ID</td>
                                                <td>{{$payment_info->transaction_id}}</td>
                                            </tr>
                                            <tr>
                                                <td>Date</td>
                                                <td>{{$payment_info->paid_date}}</td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <?php  
                                                if($payment_info->status == 'Ok'){
                                                    $status = "<span class='text-success'>Paid</span>";
                                                }else{
                                                    $status = "<span class='text-danger'>Unpaid</span>";
                                                }
                                                ?>
                                                <td><?php echo $status; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Description</td>
                                                <td>{{$payment_info->description}}</td>
                                            </tr>
                                            <tr>
                                                <td>Amount</td>
                                                <?php //dd($payment_info);?>
                                                @if($payment_info->is_wallet=='1')
                                                    @if($payment_info->is_payment_type!='2')
                                                        <td>Rs.{{$payment_info->paid_amount+$payment_info->wallet_amount}}</td>
                                                    @else
                                                        <td>Rs.{{$payment_info->paid_amount}}</td>
                                                    @endif
                                                @else
                                                <td>Rs.{{$payment_info->paid_amount}}</td>
                                                @endif
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
    // LCT10007
    $(document).ready(function () {

    });
</script>
@endsection
