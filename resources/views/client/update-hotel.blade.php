@extends('layouts.client')
<script>
	var cityData = <?= $cities ?>;
</script>
@section('content')
<section>
   <div id = "myTabContent" class = "tab-content">
      <div  class="tab-pane fade in active" id="customizes-block" >
            <div class="customize">
                <div class="container">
                    <div class="row">
                        <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="tab-content">
									<div id="Hotel" class="tab-pane fade active in">
										<div class="booking-block  border-1">
                                 <div class="booking-block-title p-t-b-5">
												<h3>Hotel Details</h3>
											</div>
											<form name="hotel_frm_update" id="hotel_frm_update" action="" method="post" onsubmit="return false;">
												{{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ isset($lead->id) ? $lead->id : '' }}" />
												<div class="holiday-booking-flight booking-block-form">
													<div class="row booking-form-padding" style="display: none;">
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
															<ul class="validate-msg">

															</ul>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
															<div class="input-group">
                                                <label class="radio-inline nopadding">Destination City</label>
																<input type="text" name="destination_city" class="form-control autocomplete" value="{{ isset($lead->destinationCity->city_name) ? $lead->destinationCity->city_name : old('from_city') }}" placeholder="Destination City">
																<input type="hidden" name="destination_city_id" value="{{ isset($lead->destination_city_id) ? $lead->destination_city_id : old('destination_city_id') }}">
															</div>
														</div>
														<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
															<div class="input-group">
                                                <label class="radio-inline nopadding">Check In Date</label>

																<input type="text" name="deprature_date" class="form-control datepicker" readonly="" value="{{ isset($lead->deprature_date) ? date('d F Y', strtotime($lead->deprature_date)) : '' }}" placeholder="Check In Date">
															</div>
														</div>
														<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
															<div class="input-group">
                                                <label class="radio-inline nopadding">Check Out Date</label>

																<input type="text" name="return_date" class="form-control datepicker" readonly="" value="{{ isset($lead->return_date) ? date('d F Y', strtotime($lead->return_date)) : '' }}" placeholder="Check Out Date">
															</div>
														</div>
                                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                             <div class="input-group">
                                                 <label class="radio-inline nopadding">Number Of Nights</label>
                                                 <input type="text" name="number_of_nights" value="{{ (isset($lead->number_of_nights)) ? $lead->number_of_nights : '' }}" class="form-control"  placeholder="Number Of Nights">
                                             </div>
                                         </div>
													</div>

                                       <div class="row booking-form-padding sub-title">
                                          <label class="radio-inline">Number of Guests</label>
                                       </div>
                                       <?php /*
													<div class="row booking-form-padding sub-title">
													<label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
													</div> 
													*/?>
                                       <div class="row booking-form-padding">
                                          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                             <div class="input-group">
                                                <label class="radio-inline nopadding">Adults</label>
                                                <input type="text" name="adults" class="form-control" value="{{ (isset($lead->adult)) ? $lead->adult : '' }}" placeholder="Adults" >
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                             <div class="input-group">
                                                <label class="radio-inline nopadding">Child</label>
                                                <input type="text" name="child" class="form-control" value="{{ isset($lead->child) ? $lead->child : '' }}" placeholder="Kids" >
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                             <div class="input-group">
                                                <label class="radio-inline nopadding">Infants</label>
                                                <input type="text" name="infants" class="form-control" value="{{ isset($lead->infant) ? $lead->infant : '' }}" placeholder="Infants" >
                                             </div>
                                          </div>
                                       </div>
												
													 
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
                                                <label class="radio-inline nopadding">Name</label>
																<input type="text" name="contact_name" class="form-control" value="{{ (isset($lead->name)) ? $lead->name : '' }}" placeholder="Name">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
                                                <label class="radio-inline nopadding">Contact No</label>
																<input type="text" name="contact_no" class="form-control onlynumber" value="{{ (isset($lead->phone)) ? $lead->phone : '' }}" placeholder="Contact No" maxlength="10">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
                                                <label class="radio-inline nopadding">Email Id</label>
																<input type="text" name="email_id" class="form-control" value="{{ (isset($lead->email)) ? $lead->email : '' }}" placeholder="Email Id">
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
															<div class="input-group">
                                                <label class="radio-inline nopadding">More Details</label>
																<textarea name="notes" class="form-control" placeholder="More Details">{{ (isset($lead->notes)) ? $lead->notes : '' }}</textarea>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding ">
														<button class="btn btn-default btn-payment btn-desktop-right hotel_submit_btn">UPDATE</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
   </div>
   <!-- Search Compare Book Block Begins -->
   
   <!-- Search Compare Book Block Ends -->
   <!-- Hot Deals Begins-->
   

</section>
@endsection
@section('javascript')
<script type="text/javascript">

	// Pure Javascript
	document.getElementsByClassName("activity-details").onmouseover = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'block';
	}
	document.getElementsByClassName("activity-details").onmouseout = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'none';
	}


</script>
<?php /*
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
<script>
	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		dots: true,
		responsiveClass: true,
<!-- navText : ['<i class="	fa fa-caret-left" aria-hidden="true"></i>','<i class="	fa fa-caret-right" aria-hidden="true"></i>'], -->
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 2,
				nav: false
			},
			1000: {
				items: 3,
				nav: false,
				loop: false,
				margin: 20

			}
		}
	})
</script>
*/?>
@endsection