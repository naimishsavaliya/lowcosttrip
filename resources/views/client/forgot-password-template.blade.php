<html>
<head></head>
  <body>
    <div style="background:#ecf0f1;width: 500px;font-family: Arial, Helvetica, sans-serif;">
      <div style="width: 500px; background-color:#fefefe;font-family: Arial, Helvetica, sans-serif;">
        <div >
            <p>You have requested a password reset, please follow the link below to reset your password.</p>
            <p>Please ignore this email if you did not request a password change.</p>
            <p><a href="{{route('reset_password',[$forgot_token])}}">Follow this link to reset your password.</a></p>
            <p style="margin-left: 10px;">
                If you need any help please email the info@lowcosttrip.com or call them on  +91 81 82 83 84 85.
            </p>
        </div>
      </div>
    </div>
  </body>
</html>