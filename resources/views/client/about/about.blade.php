@extends('layouts.client')
@section('content')
<section>
    <div class="about-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="inner-breadcrumbs-nav">
                        <ul>
                            <li><a href="{{route('index')}}"> Home</a></li>
                            <li><a href="{{route('about')}}">About Us</a></li>
                        </ul>
                    </div>
                    <div class="about-center">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="about-logo">
                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/about/lct-about-logo.jpg" class="img-responsive center-block"/>
                                </div>		
                                <div class="about-desc">
                                    <p><!-- Low cost trip (LCT) is a b2b as well as b2c online travel company.  We are a one stop travel solution company. Focussing on holidays packages, air tickets, hotels, forex, transfers and visa.--></p>
                                </div>		 
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="about-travel-title">
                                    <h2>Travel</h2>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
                                <div class="about-desc">
                                    <p>The vision is to help you plan your holidays, providing you world class experience at low cost and creating memories for life time. Giving the best of luxury at the lowest price and making you feel important.</p>
                                    <p>Some of the tours we conduct are group tours, customised holidays, corporate tours, for India and world over, frequent Individual Travel, Short Tours, and Speciality Tours such as Women’s special, Students, Singles, Senior’s, Honeymoon Packages.</p>
                                    <p>Our services include air tickets and hotel bookings for both domestic and international sectors, bus, cruise, visa, forex transfers, weekend deals around your city and many more travel related services.</p>
                                    <p>We also help you start your own travel business with our b2b product offering with the lowest investment. Contact:- info@lowcosttrip.com.</p>
                                    <p>For our loyal guest we have a loyalty program where we provide them 24*7 travel support, no service charges on cancellation, amendments, reschedule of air tickets and hotel bookings thereby helping them save money, time and resources.</p>
                                    <?php /*
                                    <p>Kindly acknowledge the receipt of the mail.</p>
                                    <p>If you need any further details please feel free to contact us.</p>
                                    <p></p>
                                    */ ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
@endsection