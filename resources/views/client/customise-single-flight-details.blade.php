<?php if(isset($response->Status) && $response->Status=="Success"){ ?>
        <a href="javascript:void(0)" class="closemoredetails"><i class="fa fa-close"></i></a>
        <div class="info-div">    
            <ul>            
                <?php 
                    $flights = $response->Data->Cart->OrderItems;
                    foreach($flights as $key => $row){                
                        $item = $row->FlightSegment; 
                        $item_PriceInfo = $response->Data->Cart->OrderItems[0]->PriceInfo;                             
                        if($key>0){ ?>
                        <li class="changeplane-span">
                            <?php                        
                            // Get waiting time
                            $dteStart = new DateTime(date('Y-m-d H:i:s', strtotime( str_replace('/', '-',$flights[$key-1]->FlightSegment->ArrivalTime))));
                            $dteEnd   = new DateTime(date('Y-m-d H:i:s', strtotime( str_replace('/', '-',$item->DepartureTime) ))); 
                            $dteDiff  = $dteStart->diff($dteEnd); 
                            ?>
                            Plane change at <?php echo $item->DepartureAirport ?> | Waiting: <?php echo $dteDiff->format("%H").'h '.$dteDiff->format("%i").'m';  ?>
                        </li>
                        <?php } ?>
                        <h3><?php 
                            $date = new DateTime(str_replace("/","-", $item->DepartureTime));
                            echo $date->format('d M, Y');                            
                        ?></h3>
                        <li class="flight-row" id="row">
                            <div class="name">                        
                                <?php echo $item->OperatingAirline ?>
                                <span><?php echo $item->CarrierCode." ".$item->FlightNumber ?></span>
                            </div>    
                            <div class="flight-details">
                                <ul>
                                    <li>
                                        <span class="time"><?php
                                        $DepartureTime = str_replace("/", "-",$item->DepartureTime);
                                        echo date("H:i",strtotime($DepartureTime)); 
                                        
                                        ?></span>
                                        <?php echo $item->DepartureAirport ?>
                                    </li>
                                    <li>
                                        <span class="time"><?php 
                                            $ArrivalTime = str_replace("/", "-",$item->ArrivalTime);
                                            echo date("H:i",strtotime($ArrivalTime)); 
                                        ?></span>
                                        <?php echo $item->ArrivalAirport ?> 
                                    </li>
                                    <li>
                                        <?php 
                                        $minutes = $item->JourneyTime;
                                        $hours = floor($minutes / 60);
                                        $min = $minutes - ($hours * 60);
                                        $count_stop =(  (isset($item->StopoverCodes))  ? count($item->StopoverCodes) : 0);
                                        ?>
                                        <span class="time"><?php echo  $hours."h ".$min.'m'; ?></span>
                                        <?php echo $count_stop == 0 ? "Non stop" : $count_stop.' stop' ; ?>
                                    </li>                                    
                                </ul>
                            </div>                    
                        </li>                
                <?php } ?>
            </ul>                          
        </div>    
<?php }else{ ?>
    <p>Something happen bad</p>
<?php } ?>
