@extends('layouts.client')
<script>
	var cityData = <?= $cities ?>;
</script>
@section('content')
<section>
   <div id = "myTabContent" class = "tab-content">
      <div  class="tab-pane fade in active" id="customizes-block" >
            <div class="customize">
                <div class="container">
                    <div class="row">
                        <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="tab-content">
									<div id="Flight" class="tab-pane fade active in">
										<form name="flight_frm_update" id="flight_frm_update" action="" method="post" onsubmit="return false;">
											{{csrf_field()}}
											<input type="hidden" name="id" value="{{ isset($lead->id) ? $lead->id : '' }}" />
											<div class="booking-block  border-1">
												<div class="booking-block-title  p-t-b-5">
													<h3>Flight Details</h3>
												</div>
												<div class="holiday-booking-flight booking-block-form">
													<div class="row booking-form-padding" style="display: none;">
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<ul class="validate-msg">

															</ul>
														</div>
													</div>
													<div class="row booking-form-padding">
														<label class="radio-inline">Flight Type</label>
														<label class="radio-inline flight_type"><input type="radio" name="flight_type" value="One Way" {!! (old('flight_type',$lead->flight_type)=='One Way')?'checked':'' !!}>One Way</label>
														<label class="radio-inline flight_type"><input type="radio" name="flight_type" value="Round Trip" {!! (old('flight_type',$lead->flight_type)=='Round Trip')?'checked':'' !!}>Round Trip</label>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
                                                				<label class="radio-inline nopadding">From City</label>
																<input type="text" name="from_city" class="form-control autocomplete" value="{{ isset($lead->fromCity->city_name) ? $lead->fromCity->city_name : old('from_city') }}" placeholder="From City">
																<input type="hidden" name="from_city_id" value="{{ isset($lead->from_city) ? $lead->from_city : old('from_city_id') }}">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
                                                				<label class="radio-inline nopadding">To City</label>
																<input type="text" name="to_city" class="form-control autocomplete" value="{{ isset($lead->toCity->city_name) ? $lead->toCity->city_name : old('to_city') }}" placeholder="To City">
																<input type="hidden" name="to_city_id" value="{{ isset($lead->to_city) ? $lead->to_city : old('to_city_id') }}">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
                                                				<label class="radio-inline nopadding">Class</label>

																<select name="flight_class" class="select-style">
																	<option value="">Class</option>
																	@foreach(get_flight_class() as $key => $value)
																	<option value="{{$key}}" {{ (isset($lead->flight_class) && $lead->flight_class == $key) ? 'selected' : '' }}>{{$value}}</option>
																	@endforeach
																</select>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
                                                				<label class="radio-inline nopadding">Departure Date</label>

																<input type="text" name="deprature_date" class="form-control datepicker" value="{{ isset($lead->deprature_date) ? date('d F Y', strtotime($lead->deprature_date)) : '' }}" readonly="" placeholder="Departure Date">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 return_date {!! (old('flight_type',$lead->flight_type)=='One Way')?'hide':'' !!}">
															<div class="input-group">
                                                				<label class="radio-inline nopadding">Return Date</label>

																<input type="text" name="return_date" class="form-control datepicker" value="{{ isset($lead->return_date) ? date('d F Y', strtotime($lead->return_date)) : '' }}" readonly="" placeholder="Return Date">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">

														</div>
													</div>
													<div class="row booking-form-padding sub-title">
														<label class="radio-inline">Number of Guests</label>
													</div>
													<?php /*
													<div class="row booking-form-padding sub-title">
													<label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
													</div> 
													*/?>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<label class="radio-inline nopadding">Adults</label>
																<input type="text" name="adults" class="form-control" value="{{ (isset($lead->adult)) ? $lead->adult : '' }}" placeholder="Adults">
															</div>
														</div>
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<label class="radio-inline nopadding">Child</label>
																<input type="text" name="child" class="form-control" value="{{ isset($lead->child) ? $lead->child : '' }}" placeholder="Child">
															</div>
														</div>
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<label class="radio-inline nopadding">Infants</label>
																<input type="text" name="infants" class="form-control" value="{{ isset($lead->infant) ? $lead->infant : '' }}" placeholder="Infants">
															</div>
														</div>
													</div>
													
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Name</label>
																<input type="text" name="contact_name" class="form-control" value="{{ (isset($lead->name)) ? $lead->name : '' }}" placeholder="Name">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Contact No</label>
																<input type="text" name="contact_no" class="form-control onlynumber" maxlength="10" value="{{ (isset($lead->phone)) ? $lead->phone : '' }}" placeholder="Contact No">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Email Id</label>
																<input type="text" name="email_id" class="form-control" value="{{ (isset($lead->email)) ? $lead->email : '' }}" placeholder="Email Id">
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
															<div class="input-group">
																<label class="radio-inline nopadding">More Details</label>
																<textarea name="notes" class="form-control" placeholder="More Details">{{ (isset($lead->notes)) ? $lead->notes : '' }}</textarea>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<button type="submit" class="btn btn-default btn-payment btn-desktop-right flight_submit_btn">UPDATE</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
   </div>
  
   <!-- Search Compare Book Block Ends -->
   <!-- Hot Deals Begins-->
   <?php /*
   <div class="hot-deals">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                     <h3 class="hot-deals-title"><span class="hot">Hot</span> Deals in Domestic Holidays</h3>
                     <div class="row">
                        <div class="deal-blocks">
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/deal-1.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Kerala Tour</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/kashmir.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Kerala Tour</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                           <a href="#" class=" text-center">View More</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                     <h3 class="hot-deals-title"><span class="hot">Hot</span>Deals in International Holidays</h3>
                     <div class="row">
                        <div class="deal-blocks">
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/europe.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Europe</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/dubai.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Dubai</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                           <a href="#" class=" text-center">View More</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   */?>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
	// Pure Javascript
	document.getElementsByClassName("activity-details").onmouseover = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'block';
	}
	document.getElementsByClassName("activity-details").onmouseout = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'none';
	}

	$(document).ready(function () {
        $(document).on("change", ".flight_type", function (event) {
            if( $("input[name='flight_type']:checked").val()  == 'Round Trip'){
                $('.return_date').removeClass('hide');
            }else{
                $('.return_date').addClass('hide');
            }
        });
        $(".flight_type").trigger("change");
    });

</script>
<?php /*
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
<script>
	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		dots: true,
		responsiveClass: true,
<!-- navText : ['<i class="	fa fa-caret-left" aria-hidden="true"></i>','<i class="	fa fa-caret-right" aria-hidden="true"></i>'], -->
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 2,
				nav: false
			},
			1000: {
				items: 3,
				nav: false,
				loop: false,
				margin: 20

			}
		}
	})
</script>
*/?>
@endsection