@extends('layouts.client')
<script>
    var cityData = <?= $cities ?>;
</script>
@section('content')
<section>
    <div id = "myTabContent" class = "tab-content">
        <!-- Holiday Block Begins -->
        <div  class = "tab-pane fade" id="holiday-block">
            <!-- Sub Menu Begins -->
            <!-- Sub Menu Ends -->
            <!-- Search Block Begins -->
            <div class="search-block">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-lg-offset-1">
                                    <form role="search" method="get" action="#">
                                        <div class="row">
                                            <div >
                                                <input type="text" placeholder="Search Your Destination" name="search" class=" search-dest">
                                            </div>
                                            <div>
                                                <button type="submit" class=" btn-search ">Search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                                    <div class="row">
                                        <div class="search-cust">
                                            <span class="search-span">or</span>
                                            <a href="#" class="btn btn-default btn-customize">Customize</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Search Block Ends -->
            <!-- Banner Block Begins -->
            <div class="banner"></div>
            <div class="banner-main" >
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="banner-blocks">
                                        <div class="banner-block-left">
                                            <div class="thumbnail">
                                                <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/deal_1.png" alt = "Generic placeholder thumbnail">
                                            </div>
                                            <div class="caption big-caption bg-orange orange-border">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 big-caption-left">
                                                        <h4>On International Tours</h4>
                                                        <h2>Europe Tour Package</h2>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-5 col-md-7 col-lg-5  big-caption-right">
                                                        <h4>Starting From</h4>
                                                        <h3>Rs. 29,999/- Per Person</h3>
                                                    </div>
                                                </div>
                                                <img src="{{ env('APP_PUBLIC_URL')}}client/images/traingle-show.png" class="arrow  img-responsive"/>
                                            </div>
                                            <div class="off">
                                                <img src="{{ env('APP_PUBLIC_URL')}}client/images/off.png" class="img-responsive"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                            <div class="banner-blocks banner-block-right-top">
                                                <div class="thumbnail">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/deal_2.png" alt = "Generic placeholder thumbnail">
                                                </div>
                                                <div class="caption big-caption bg-blue">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <h4>Flat Rs. 800 OFF</h4>
                                                            <h2>The Resort Bech Hotel</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="banner-blocks banner-block-right-bottom">
                                                <div class="thumbnail">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/deal_3.png" alt = "Generic placeholder thumbnail">
                                                    <div class="pink-overlay bg-pink big-caption" >
                                                        <div class="pink-skew">
                                                            <h4>Goa Banana Boat</h4>
                                                            <h2>Flat 15% Off</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Banner Block Ends -->
        </div>
        <!-- Holiday Block Ends  -->
        <!-- Hotel Block Begins -->
        <div  class="tab-pane fade" id="hotel-block" >
            <!-- Search Block Begins -->
            <div class="search-block hotel-search">
                <div class="container">
                    <div class="row">
                        <form role="search" method="get" action="#">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <input  type="text" class="form-control" name="email" placeholder="Destination">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control"  placeholder="Check-In">
                                            <span class="form-control-feedback"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="email" placeholder="Check-Out">
                                            <span class="form-control-feedback"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <select class="form-control">
                                                <option>Rooms</option>
                                                <option>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <select class="form-control">
                                                <option>Adults</option>
                                                <option>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <button type="submit" class="btn-hotel">Search</button>
                                    </div>
                                </div>
                                <!--   <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                                   <div class="row">
                                       <div class="search-cust">
                                       <span class="search-span">or</span>
                                        <a href="#" class="btn btn-default btn-customize">Customize</a>
                                        </div>
                                       </div>
                                       
                                   
                                   </div> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Search Block Ends -->
            <!-- Banner Block Begins -->
            <div class="banner"></div>
            <div class="banner-main" >
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="  banner-blocks">
                                        <div class="banner-block-left">
                                            <div class="thumbnail">
                                                <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-1.jpg" alt = "Generic placeholder thumbnail">
                                            </div>
                                            <div class="caption big-caption bg-orange orange-border">
                                                <div class="row">
                                                    <div class="col-xs-6 big-caption-left">
                                                        <h4>On International Tours</h4>
                                                        <h2>
                                                            Europe Tour Package</h2>
                                                    </div>
                                                    <div class="col-xs-6 big-caption-right">
                                                        <h4>Starting From</h4>
                                                        <h3>Rs. 29,999/- Per Person</h3>
                                                    </div>
                                                </div>
                                                <img src="{{ env('APP_PUBLIC_URL')}}client/images/traingle-show.png" class="arrow  img-responsive"/>
                                            </div>
                                            <div class="off">
                                                <img src="{{ env('APP_PUBLIC_URL')}}client/images/off.png" class="img-responsive"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                            <div class="banner-blocks banner-block-right-top">
                                                <div class="thumbnail">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-2.jpg" alt = "Generic placeholder thumbnail">
                                                </div>
                                                <div class="caption big-caption bg-blue">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <h4>Flat Rs. 800 OFF</h4>
                                                            <h2>
                                                                The Resort Bech Hotel</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="banner-blocks banner-block-right-bottom">
                                                <div class="thumbnail">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-3.jpg" alt = "Generic placeholder thumbnail">
                                                    <div class="pink-overlay bg-pink big-caption" >
                                                        <div class="pink-skew">
                                                            <h4>Goa Banana Boat</h4>
                                                            <h2>
                                                                Flat 15% Off</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Banner Block Ends -->
        </div>
        <!-- Hotel Block Ends -->
        <!-- Flight Block Begins -->
        <div  class="tab-pane fade" id="flight-block" >
            <!-- Sub Menu Begins -->
            <!-- Sub Menu Ends -->
            <!-- Search Block Begins -->
            <div class="search-block flight-search">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <input  type="text" class="form-control" name="email" placeholder="From">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <input  type="text" class="form-control" name="email" placeholder="To">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="email" placeholder="Depart">
                                            <span class="form-control-feedback"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <select class="form-control">
                                                <option>Returns</option>
                                                <option>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="input-group">
                                            <select class="form-control">
                                                <option>Adults</option>
                                                <option>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <button type="submit" class="btn-flight ">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Search Block Ends -->
            <!-- Banner Block Begins -->
            <div class="banner"></div>
            <div class="banner-main" >
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="  banner-blocks">
                                            <div class="banner-block-left">
                                                <div class="thumbnail">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-1.jpg" alt = "Generic placeholder thumbnail">
                                                </div>
                                                <div class="caption big-caption bg-orange orange-border">
                                                    <div class="row">
                                                        <div class="col-xs-6 big-caption-left">
                                                            <h4>On International Tours</h4>
                                                            <h2>
                                                                Europe Tour Package</h2>
                                                        </div>
                                                        <div class="col-xs-6 big-caption-right">
                                                            <h4>Starting From</h4>
                                                            <h3>Rs. 29,999/- Per Person</h3>
                                                        </div>
                                                    </div>
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/traingle-show.png" class="arrow  img-responsive"/>
                                                </div>
                                                <div class="off">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/off.png" class="img-responsive"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                            <div class="banner-blocks banner-block-right-top">
                                                <div class="thumbnail">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-2.jpg" alt = "Generic placeholder thumbnail">
                                                </div>
                                                <div class="caption big-caption bg-blue">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <h4>Flat Rs. 800 OFF</h4>
                                                            <h2>
                                                                The Resort Bech Hotel</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="banner-blocks banner-block-right-bottom">
                                                <div class="thumbnail">
                                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-3.jpg" alt = "Generic placeholder thumbnail">
                                                    <div class="pink-overlay bg-pink big-caption" >
                                                        <div class="pink-skew">
                                                            <h4>Goa Banana Boat</h4>
                                                            <h2>
                                                                Flat 15% Off</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Banner Block Ends -->
        </div>
        <!-- Flight Block Ends -->
        <!-- Activity Block Begins -->
        <div  class="tab-pane fade" id="activity-block" >
            <!-- Search Block Begins -->
            <div class="search-block">
                <div class="container">
                    <div class="row">
                        <form role="search" method="get" action="#">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                        <input type="text" placeholder=" Search For Cities , Activites ,Tours " name="search" class="activity-text">
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <button type="submit" class="btn-activity ">Search</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Search Block Ends -->
                </div>
                <!-- Activity Block Ends -->
            </div>
        </div>
        <div  class="tab-pane fade in active" id="customizes-block" >
            <div class="customize">
                <div class="container">
                    <div class="row">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <!-- Nav tabs -->
                                <ul class="nav nav-pills inquiry_tab">
                                    <?php /* <li class="Flight"><a data-toggle="pill" href="#Flight">Flight</a></li> */ ?>
                                    <li class="Hotel"><a data-toggle="pill" href="#Hotel">Hotel</a></li>
                                    <li class="active holiday"><a data-toggle="pill" href="#holiday">Holiday</a></li>
                                    <li class="visa"><a data-toggle="pill" href="#visa">Visa</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="Flight" class="tab-pane fade">
                                        <form name="flight_frm" id="flight_frm" action="" method="post" onsubmit="return false;">
                                            {{csrf_field()}}
                                            <div class="booking-block border-1 border-1"> 
                                                <div class="holiday-booking-flight booking-block-form">
                                                    <div class="row booking-form-padding" style="display: none;">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                                            <ul class="validate-msg">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <label class="radio-inline">Flight Type</label>
                                                        <label class="radio-inline flight_type"><input type="radio" name="flight_type" value="One Way" checked>One Way</label>
                                                        <label class="radio-inline flight_type"><input type="radio" name="flight_type" value="Round Trip">Round Trip</label>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">From City</label>
                                                                <input type="text" name="from_city" class="form-control autocomplete" placeholder="From City">
                                                                <input type="hidden" name="from_city_id">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">To City</label>
                                                                <input type="text" name="to_city" class="form-control autocomplete" placeholder="To City">
                                                                <input type="hidden" name="to_city_id">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Class</label>
                                                                <select name="flight_class" class="select-style">
                                                                    <option value="">Class</option>
                                                                    @foreach(get_flight_class() as $key => $value)
                                                                    <option value="{{$key}}">{{$value}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Departure Date</label>
                                                                <input type="text" name="deprature_date" class="form-control datepicker" readonly="" placeholder="Departure Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 return_date hide">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Return Date</label>

                                                                <input type="text" name="return_date" class="form-control datepicker" readonly="" placeholder="Return Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">

                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Number of Guests</label>
                                                    </div>
                                                    <?php /*
                                                    <div class="row booking-form-padding sub-title">
                                                        <label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
                                                    </div> 
                                                    */?>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Adults</label>
                                                                <input type="text" name="adults" value="" class="form-control" placeholder="Adults">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Child</label>
                                                                <input type="text" name="child" class="form-control" placeholder="Child">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Infants</label>
                                                                <input type="text" name="infants" class="form-control" placeholder="Infants">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Name</label>
                                                                <input type="text"  value="{{(isset($user_data->first_name) && isset($user_data->last_name) ? $user_data->first_name.' '.$user_data->last_name :'')}}"  name="contact_name" class="form-control" placeholder="Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Contact No</label>
                                                                <input type="text"  value="{{(isset($user_data->phone_no) ? $user_data->phone_no :'')}}" name="contact_no" class="form-control" placeholder="Contact No">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Email Id</label>
                                                                <input type="text" value="{{(isset($user_data->email) ? $user_data->email :'')}}"   name="email_id" class="form-control" placeholder="Email Id">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">More Details</label>
                                                                <textarea name="notes" class="form-control" placeholder="More Details"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <button type="submit" class="btn btn-default btn-payment btn-desktop-right flight_submit_btn">SUBMIT</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="Hotel" class="tab-pane fade">
                                        <div class="booking-block border-1">
                                            <form name="hotel_frm" id="hotel_frm" action="" method="post" onsubmit="return false;">
                                                {{csrf_field()}}
                                                <div class="holiday-booking-flight booking-block-form">
                                                    <div class="row booking-form-padding" style="display: none;">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                                            <ul class="validate-msg">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Destination City</label>

                                                                <input type="text" name="destination_city" class="form-control autocomplete" placeholder="Destination City">
                                                                <input type="hidden" name="destination_city_id">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Check In Date</label>
                                                                <input type="text" name="deprature_date" class="form-control datepicker" readonly="" placeholder="Check In Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Check Out Date</label>
                                                                <input type="text" name="return_date" class="form-control datepicker" readonly="" placeholder="Check Out Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Number Of Nights</label>
                                                                <input type="text" name="number_of_nights" class="form-control"  placeholder="Number Of Nights">
                                                            </div>
                                                        </div>
                                                    </div>
                                                     
                                                    <div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Number of Guests</label>
                                                    </div>
                                                    <?php /*
                                                    <div class="row booking-form-padding sub-title">
                                                    <label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
                                                    </div> 
                                                    */?>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Adults</label>
                                                                <input type="text" name="adults" value="" class="form-control" placeholder="Adults">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Child</label>
                                                                <input type="text" name="child" class="form-control" placeholder="Child">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Infants</label>
                                                                <input type="text" name="infants" class="form-control" placeholder="Infants">
                                                            </div>
                                                        </div>
                                                    </div>
                                                  
                                                    
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Name</label>
                                                                <input type="text"  value="{{(isset($user_data->first_name) && isset($user_data->last_name) ? $user_data->first_name.' '.$user_data->last_name :'')}}"  name="contact_name" class="form-control" placeholder="Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Contact No</label>
                                                                <input type="text"  value="{{(isset($user_data->phone_no) ? $user_data->phone_no :'')}}" name="contact_no" class="form-control" placeholder="Contact No">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Email Id</label>
                                                                <input type="text" value="{{(isset($user_data->email) ? $user_data->email :'')}}"   name="email_id" class="form-control" placeholder="Email Id">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">More Details</label> 
                                                                <textarea name="notes" class="form-control" placeholder="More Details"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <button class="btn btn-default btn-payment btn-desktop-right hotel_submit_btn">SUBMIT</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div id="holiday" class="tab-pane fade active in">
                                        <div class="booking-block border-1">
                                            <div class="holiday-booking-flight booking-block-form">
                                                <form name="holiday_frm" id="holiday_frm" action="" method="post" onsubmit="return false;">
                                                    {{csrf_field()}}
                                                    <div class="row booking-form-padding" style="display: none;">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                                            <ul class="validate-msg">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Departure City</label>

                                                                <input type="text" name="departure_city" class="form-control autocomplete" placeholder="Departure City">
                                                                <input type="hidden" name="departure_city_id">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Interested Date</label>

                                                                <input type="text" name="check_in_date" class="form-control datepicker" readonly="" placeholder="Interested Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Choose travel type</label>
                                                                <select name="travel_type" class="select-style">
                                                                    <option value="" selected>Choose travel type</option>
                                                                    @foreach(get_travel_type() as $key => $travel_type)
                                                                    <option value="{{ $key }}">{{ $travel_type}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Destination Details</label>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <td width="50%">Destination</td>
                                                                        <td width="30%">Number of Nights</td>
                                                                        <td width="20%">Action</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr class="add_btn_tr">
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-default btn-number destination_add_btn" data-type="plus" data-field="quant[1]">
                                                                                <span class="glyphicon glyphicon-plus"></span>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>




                                                    <div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Activity Details</label>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <table class="table table-bordered activiti-table">
                                                                <thead class="append_new_row">
                                                                    <tr id="activity-header">
                                                                        <td width="30%">Country</td>
                                                                        <td width="50%">Activity</td>
                                                                        <td width="20%">Action</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody >
                                                                 
                                                                
                                                                    <tr >
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-default btn-number add_tour_activites" data-type="plus" data-field="quant[1]">
                                                                                <span class="glyphicon glyphicon-plus"></span>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                            </table>
                                                        </div>
                                                    </div>




                                                    <div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Number of Guests</label>
                                                    </div>
                                                               <?php /*
                                                    <div class="row booking-form-padding sub-title">
                                                        <label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
                                                    </div> 
                                                    */?>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Adults</label>
                                                                <input type="text" name="adults" value="" class="form-control" placeholder="Adults">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Child</label>
                                                                <input type="text" name="child" class="form-control" placeholder="Child">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Infants</label>
                                                                <input type="text" name="infants" class="form-control" placeholder="Infants">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Name</label>
                                                                <input type="text"  value="{{(isset($user_data->first_name) && isset($user_data->last_name) ? $user_data->first_name.' '.$user_data->last_name :'')}}"  name="contact_name" class="form-control" placeholder="Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Contact No</label>
                                                                <input type="text"  value="{{(isset($user_data->phone_no) ? $user_data->phone_no :'')}}" name="contact_no" class="form-control" placeholder="Contact No">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Email Id</label>
                                                                <input type="text" value="{{(isset($user_data->email) ? $user_data->email :'')}}"   name="email_id" class="form-control" placeholder="Email Id">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">More Details</label>
                                                                <textarea name="notes" class="form-control" placeholder="More Details"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <button class="btn btn-default btn-payment btn-desktop-right holiday_submit_btn">SUBMIT</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="visa" class="tab-pane fade">
                                        <div class="booking-block border-1">
                                            
                                            <div class="holiday-booking-flight booking-block-form">
                                                <form name="visa_frm" id="visa_frm" action="" method="post" onsubmit="return false;">
                                                    {{csrf_field()}}
                                                    <div class="row booking-form-padding" style="display: none;">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                                            <ul class="validate-msg">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Nationality</label>
                                                                <input type="text" name="where_live" class="form-control autocomplete" placeholder="Nationality">
                                                                <input type="hidden" name="where_live_id">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Where do you want to go?</label>
                                                                <input type="text" name="where_go" class="form-control autocomplete" placeholder="Where do you want to go?">
                                                                <input type="hidden" name="where_go_id">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Travel Date</label>
                                                                <input type="text" name="travel_date" class="form-control datepicker" placeholder="Travel Date">
                                                            </div>	
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Visa Type</label>
                                                                <select name="visa_type" class="select-style">
                                                                    <option value="">Visa Type</option>
                                                                    @foreach(get_visa_type() as $key => $visa_type)
                                                                    <option value="{{$key}}">{{$visa_type}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                            <label class="radio-inline nopadding">Number of Entries</label>
                                                                <select name="number_of_entries" class="select-style">
                                                                    <option value="">Number of Entries</option>
                                                                    @foreach(get_number_of_entries() as $key => $visa_type)
                                                                    <option value="{{$key}}">{{$visa_type}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Duration Days</label>
                                                                <input type="text" name="duration_days" class="form-control" placeholder="Duration Days" min="1" max="10">
                                                                
                                                            </div>	
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding sub-title">
                                                        <label class="radio-inline">Number of Guests</label>
                                                    </div>
                                                               <?php /*
                                                    <div class="row booking-form-padding sub-title">
                                                        <label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
                                                    </div> 
                                                    */?>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Adults</label>
                                                                <input type="text" name="adults" value="" class="form-control" placeholder="Adults">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Child</label>
                                                                <input type="text" name="child" class="form-control" placeholder="Child">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Infants</label>
                                                                <input type="text" name="infants" class="form-control" placeholder="Infants">
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Name</label>
                                                                <input type="text"  value="{{(isset($user_data->first_name) && isset($user_data->last_name) ? $user_data->first_name.' '.$user_data->last_name :'')}}"  name="contact_name" class="form-control" placeholder="Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Contact No</label>
                                                                <input type="text"  value="{{(isset($user_data->phone_no) ? $user_data->phone_no :'')}}" name="contact_no" class="form-control" placeholder="Contact No">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Email Id</label>
                                                                <input type="text" value="{{(isset($user_data->email) ? $user_data->email :'')}}"   name="email_id" class="form-control" placeholder="Email Id">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">More Details</label>
                                                                <textarea name="notes" class="form-control" placeholder="More Details"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <button class="btn btn-default btn-payment btn-desktop-right visa_submit_btn">SUBMIT</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Feature Activites Ends -->
</section>
@endsection
@section('javascript')
<script type="text/javascript">
    $(function () {
        var hash = window.location.hash;
        if (hash != '') {
            hash = document.URL.substr(document.URL.indexOf('#')+1); 
            $('ul.inquiry_tab li.' + hash + ' a').trigger('click');
        }
    });
    // Pure Javascript
    document.getElementsByClassName("activity-details").onmouseover = function () {
        document.getElementsByClassName("activity-overlay").style.display = 'block';
    }
    document.getElementsByClassName("activity-details").onmouseout = function () {
        document.getElementsByClassName("activity-overlay").style.display = 'none';
    }


</script>
<?php /*
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
*/ ?>
<script>
    $(document).ready(function () {
        
        $(document).on("change", ".flight_type", function (event) {
            if( $("input[name='flight_type']:checked").val()  == 'Round Trip'){
                $('.return_date').removeClass('hide');
            }else{
                $('.return_date').addClass('hide');
            }
        });
        $(".flight_type").trigger("change");

        $(document).on("click", ".add_tour_activites", function (event) {
            $.ajax({
                type: 'GET',
                url: "{{ route('get-activiti-html') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    $('.append_new_row').append( data.html );
                    // $(".append_new_row select").select2();
                }
            });
        });

        $(document).on("change", ".country_id", function (event) {
            $th = $(this);
            $.ajax({
                type: 'POST',
                url: "{{ route('get-activiti-by-city') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id" :  $th.val(),
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Please wait..</option>');
                },
                success: function (data, textStatus, jqXHR) {
                    $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Choose Activity</option>');

                    $.map(data.activities, function (item) {
                        $th.closest('tr').find('td:eq(1) .tour_activity').append('<option value="' + item.id + '">' + item.title + '</option>');
                    });
                }
            });
        });


    }); 



//     $('.owl-carousel').owlCarousel({
//         loop: true,
//         margin: 10,
//         dots: true,
//         responsiveClass: true,
// <!-- navText : ['<i class="	fa fa-caret-left" aria-hidden="true"></i>','<i class="	fa fa-caret-right" aria-hidden="true"></i>'], -->
//         responsive: {
//             0: {
//                 items: 1,
//                 nav: true
//             },
//             600: {
//                 items: 2,
//                 nav: false
//             },
//             1000: {
//                 items: 3,
//                 nav: false,
//                 loop: false,
//                 margin: 20

//             }
//         }
//     })
</script>
@endsection