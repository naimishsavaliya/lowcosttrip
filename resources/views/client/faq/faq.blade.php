@extends('layouts.client')

@section('content')
<section>
    <!-- Page Breadcrumbs Begins -->
    <div class="faq-breadcrumbs">
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="faq-breadcrumbs-block">
                        <h2>Faq & Help</h2>

                        <!--<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised.</p>-->

                    </div>



                </div>

            </div>
        </div>

    </div>

    <!-- Page Breadcrumbs Ends -->


    <div class="faq-main-block">

        <div class="container">


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">

                                        <span class="glyphicon glyphicon-minus accordion-icon"></span>
                                        <span class="accordion-title">General Information</span>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body in"> 

                                    <div class="faq-block">

                                        <h2>How Are The Seats Allocated?</h2>

                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Seats allotted in the coach are on ‘First come, First serve basis’<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Seat numbers 1 and 2 are reserved for the tour manager/guide<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Seat numbers 3 and 4 are ‘Prime Seats’ which if available can be booked by the Guest by paying an additional cost<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Guests wanting to avail the first seats, should book as early as possible by paying the registration tour amount and an additional amount to avail the first seats.</p>

                                        <h2>What Is The Duration Of Internal Travel?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Group<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  The length of the journey differs from country to country/ city to city / itinerary to itinerary<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  In group tours there would be restroom and refreshment halts during the journey
                                        </p>
                                        <h2>What Type Of Meals/Beverages Are Normally Provided On Tour?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Group tours . all our meals are pure vegetarian<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Continental Breakfast, Indian Lunch and Dinner<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Pre-set vegetarian food along with fast food item at times<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Signature Holidays<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Breakfast only<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Lunch and dinner available on request subject to location and the restaurant/hotel availability<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Breakfast and dinner are included in certain packages (Kindly confirm your on tour meals during booking)<br/>
                                        </p>
                                        <h2>Can I Get A Special Meal?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Yes, for a Group Tour<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Option of Jain Meal (which is without Onion and Garlic) is available<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Alternate food items for guests observing ‘Fast’ e.g. fruits, milk and finger chips is available<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Prior intimation needed<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Baby food (which includes tin milk, rice and plain dal) can be made available<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Prior intimation needed<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Request you to kindly fill in your preferred meal option at the time of booking the tour<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  customised / specialised Holidays<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Meals are subject to location, city, country, etc.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Prior intimation is needed<br/>
                                        </p>
                                        <h2>Does The Company Have Its Own Cook And Caravan Facility?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  World tours / india tours<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Meals are normally arranged in Indian restaurants or Packed meals are provided during long journeys<br/>
                                        </p>
                                        <h2>Will I Get Tea, Coffee And Other Refreshments On Tour?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Yes<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Tea, coffee and other refreshments are available on tour.<br/>
                                        </p>

                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <span class="glyphicon glyphicon-plus accordion-icon"></span>
                                        <span class="accordion-title">Hotel And Room Facilities</span>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">


                                    <div class="faq-block">

                                        <h2>What Does Twin Sharing Mean?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  2 adults sharing one room with two separate beds.<br/>
                                        </p>
                                        <h2>What Does Double Sharing Mean?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  2 adults sharing one room with one double bed<br/>
                                        </p>
                                        <h2>What Does Triple Room Mean?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  3 Adults share one room with either two separate/ one double bed and third person with an extra bed / mattress<br/>
                                        </p>
                                        <h2>What Do You Mean By Single Occupancy?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Single occupancy means only a single person stays in a room<br/>
                                        </p>
                                        <h2>Which Type Of Hotels Do We Stay In On Tour?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Standard and comfortable hotels which are conviniently located are selected. 3star / 4star / 5star category hotel are available based on the package booked by you .
                                        </p>
                                        <h2>What Happens In Case Of A Single Guest?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Option 1Single Room Occupancy *(subject to single occupancy charges applicable to the Guest)<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Option 2Room sharing with mutual consent between single travellers
                                        </p>
                                        <h2>Is There A Room Sharing Guarantee On The Speciality Tours Like Women’s Special, Seniors’ Special, Singles’ Special, Students’ Special Tours?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Room sharing guarantee is given on the above mentioned Speciality Tours
                                        </p>
                                        <h2>What Is The Room Sharing Price For One Adult And One Child?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  For Indian Tour, child below 11 years: child has to pay adult tour price<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  For World tours child below 11 years: payment is to be made in 2 parts – INR component of the child tour price and FOREX component of the adult tour price.
                                        </p>
                                        <h2>What Is The Room Sharing Policy For Three Adults And One Child?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Provision of 2 rooms for a family of 4 *(in case the guest has a child accompanying then s/he has to pay the adult forex tour price)
                                        </p>
                                        <h2>Are Porter Services Included?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Porter services are not included<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Porter services will be at an additional charge. If needed it can be provided on prior intimation /request. Charges for the same needs to be borne by the guest.
                                        </p>
                                        <h2>What About The Safety And Security Of My Belongings?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Provision of lockers are available in some hotels<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Guests are requested to take care of the locker keys or remember the password of the locker system in order to avoid paying the fine in case of loss or password mismatch<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Guests are advised not to carry valuable items on tour.
                                        </p>
                                        <h2>What Is The Role Of The Tour Manager/ Assistant Tour Manager?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Tour leaders / tour managers/ assistant tour managers/ are assisting guests throughout the tour.
                                        </p>
                                        <h2>What Language Do The Tour Managers/Assistant Tour Managers Use?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  English, Marathi and Hindi
                                        </p>
                                        <h2>How Many Tour Managers/Assistant Tour Managers Accompany The Tour?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  For Indian tours we have 1 tour manager and 1 assistant tour manager<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  For world tours we have 1 tour manager
                                        </p>
                                        <h2>Is Assistance Provided?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Assistance is provided throughout the tour at the sightseeing points and at scheduled pick up and drop locations.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Tour leaders / tour managers/ assistant tour managers/ are assisting guests throughout the tour.
                                        </p>
                                        <h2>How Do I Recognise The Tour Managers / Assistant Tour Managers?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Tour managers will be wearing the low cost trip t shirts / cap / jackets and s/he will be carrying a low cost trip signboard/flag For Group Tours<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  No tour manager accompanies on Signature Holidays. In case of a bigger group travelling on a Signature Holiday, the services of a tour manager can be added to the package at an additional cost.
                                        </p>
                                        <h2>Is Passport Mandatory</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Yes, it is Mandatory (In case of International tours)
                                        </p>
                                        <h2>Is Assistance With Regards To Passport Is Provided?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> No assistance regarding the renewal/fresh application of the passport is provided
                                        </p>
                                        <h2>What Is A VISA?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  A Visa means ( Visitor Intending to Stay Abroad)
                                        </p>
                                        <h2>Is Assistance Provided?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Documents list needed for Visa processing is provided and also visa assistance at an additionsl cost is provided .contact visa@lowcosttrip.com
                                        </p>
                                        <h2>Interview?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Guests are required to personally attend the interview
                                        </p>
                                        <h2>Fees And Grant?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Visa fees are included in the tour cost (In case the respective consulate or Ministry alters/increase/ decreases the visa facilitation fee, then the revised cost will be applicable)<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Probability of Grant<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Probability of Grant<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  As stated, lct is just a facilitator for procuring visas. We will not be able to represent any visa issue, either pre or post submission and after issuance of visa. lct requests all its guests to cross-check their visas copies and intimate any discrepancies well in time to their travel advisor
                                        </p>
                                        <h2>Refund In Case Of Rejection?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Tour price is refunded after deducting the Visa application fees and cancellation charges
                                        </p>
                                        <h2>Booking Name</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  All the travel arrangements are made as per the name on the Guest’s passport
                                        </p>
                                        <h2>What Is The VISA Extension Rule?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Any extension in stay will lead to an additional cost in case of VISA and Insurance and is to be borne by the guest
                                        </p>
                                        <h2>What Is The Insurance Policy For Group Tours?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Insurance fee is included in Dubai and Europe Tour Price*( for guests below 59 years of age)<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Additional fee, additional premium applicable *(for guests above 59)<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Insurance fee is not included in the tour price for the Signature Holiday<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Foreign Exchange
                                        </p>
                                        <h2>What Is The FOREX Carrying Limit While Travelling On An International Tour?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Basic Travel Quota (BTQ) per financial year is US$ 10000 or equivalent per person (maximum cash up to 3000 USD and 7000 USD by Travellers’ Cheques) for the leisure traveller<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  US$ 25000 for the business traveller<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Euro and USD is globally accepted
                                        </p>
                                        <h2>May I Carry The FOREX In Cash?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  Guests are free to carry the FOREX in cash, however carrying The American Express Travellers’ Cheques is always advisable.
                                        </p>
                                        <h2>Where Do I Purchase The FOREX From?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  You can purchase the FOREX from our FOREX Division. Kindly find the address of the same below<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  You can purchase the FOREX from our FOREX Division. Kindly find the address of the same below
                                        </p>
                                        <h2>How Much Baggage Is Allowed Per Ticket?</h2>
                                        <p>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>  At present for most of the destinations, it is 20 to 25 kg for cargo and 6 to 7 kg for handbag per ticket but it does vary with the airline. For guest traveling to US at present baggage is controlled by Piece Concept. Under the Piece Concept, passengers are permitted per ticket to check two bags with a per-bag weight of up to 23 kg for Economy Class. Hand baggage of 7 kg is allowed for both international and internal flights.<br/>
                                            (International airlines usually have different restrictions for carry-on luggage. Again, it is always best to check with your airline first to get their exact requirements).  <br/>
                                            Guests are specially requested to read the terms and conditions as mentioned on the website/brochure and booking form. It shall be presumed that the guest has read/understood and accepted all the terms and conditions and accepted the same.  <br/>
                                        </p>


                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>



        </div>



    </div>

</section>


@endsection

@section('javascript')
<script type="text/javascript">

    // Pure Javascript
    document.getElementsByClassName("activity-details").onmouseover = function () {
        document.getElementsByClassName("activity-overlay").style.display = 'block';
    }
    document.getElementsByClassName("activity-details").onmouseout = function () {
        document.getElementsByClassName("activity-overlay").style.display = 'none';
    }


</script>
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
<script>
    $('.testi-carousel').owlCarousel({
        loop: true,
        margin: 0,
        dots: false,
        responsiveClass: true,
        nav: true,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false,
                margin: 0
            }
        }
    });

    $('.ap-carousel').owlCarousel({
        loop: true,
        margin: 0,
        dots: false,
        responsiveClass: true,
        nav: true,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: false,
                margin: 0
            }
        }
    });
</script>
@endsection