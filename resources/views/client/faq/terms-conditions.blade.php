@extends('layouts.client')

@section('content')
<section>
    <!-- Page Breadcrumbs Begins -->
    <div class="faq-breadcrumbs">
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="faq-breadcrumbs-block">
                        <h2>Terms & Conditions(Holidays)</h2>
                        <p>Thank you for choosing low cost trip for the best travel experience. We take all efforts to ensure that you have a warm and a delightful holiday experience. We devote keen attention to every minute detail of your tour. At the same time, we request you to read and follow the below Terms and Conditions so as to avoid any misunderstanding and ensure a hassle-free and enjoyable tour.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Page Breadcrumbs Ends -->


    <div class="faq-main-block">

        <div class="container">


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">

                                        <span class="glyphicon glyphicon-minus accordion-icon"></span>
                                        <span class="accordion-title">Introduction And Commencement</span>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body in"> 

                                    <div class="faq-block">

                                        <h2>Introduction And Commencement</h2>

                                        <p>These Terms and Conditions be called i q comtech Pvt Ltd or lct(India and World Tourist) Terms and Conditions.</p>
                                        <p>They extend to the Indian & World Tours, Tailormade Holidays & also Corporate Tours organised by i q comtech Pvt Ltd.</p>
                                        <p>Foreign Exchange: For currency purchase from i q comtech pvt Ltd. the terms and conditions will be as per RBI & GOI rules. Kindly refer to our website for details.</p>
                                        <h2>Definitions </h2>
                                        <p>
                                            In these Terms and Conditions, unless the context otherwise requires<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> "low cost trip " or " lct" are brand name of i q comtech Pvt Ltd., therefore, all these names will be considered one and same and used synonymously therein.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Guest/s means the total number of people who have registered their names and booked the tour by having made the full payment for Indian/World tours organized by lct (which expression shall, unless it be repugnant to the context or meaning there of be deemed to mean and include their respective heirs, executors and administrators)<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Company means lowcosttrip.com (i q comtech Pvt.Ltd) and also includes all its brands and divisions.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Tour/s mean any Indian and World Tours organized by lowcosttrip.com<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> First day of the tour shall start at any time (i.e. in the morning / afternoon / evening) at the first destination depending on the arrival of the respective flight/train/cruise or any other mode of transport and the same shall be in case of the last day of the tour. In other words, a 'Day' shall mean a part of the day or 24 hours or its part thereof.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Force Majeure means an event or circumstance not within the reasonable control, directly or indirectly, of LCT World in its ability to perform its obligations/ responsibilities regarding the tour including<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> war, ostilities invasion, act of foreign enemies;<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> rebellion, revolution, insurrection or military or usurped power or civil war;<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> epidemic, explosion, fire, flood, earthquake or other exceptional natural calamity and act of God;<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> ionizing radiation or contamination by radioactivity from any nuclear fuel, or from any nuclear waste from the combustion of nuclear fuel, radioactive toxic explosive, or other hazardous properties of any explosive nuclear assembly or nuclear component of such assembly;<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> riot, commotion or disorder;<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> any act or omission of any Government instrumentality;<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> a Change in Legal Requirements;<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> strikes, lock outs or other industrial disturbances; &<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> abnormal weather or sea conditions including tempest, lightning, hurricane, typhoon, rain & temperatures.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> <b>Extra Mattress :</b>Means roll away mattress which is on floor (For domestic tour extra mattress & for international tour extra bed)<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> <b>Jain Food means :</b>Food without onion, garlic served at the time of tour groups meals and not necessarily before sunset, it may or may not be served on the table.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> <b>Meal :</b>Include breakfast, lunch, dinner (they are pre set menus) and/or any other snacks (dry or wet) supplied.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> <b>Tour Escort / Leader / Manager / Assistant :</b>A Person designated by Company to help / guide / assist the guest/s in and or during India and / or World Tours Organized by LCT<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> <b>Gender :</b>The masculine gender, shall also include feminine gender and vice versa, the singular shall include plural and vice versa and shall include grammatical variations if any.<br/>
                                            <i class="fa fa-caret-right" aria-hidden="true"></i> Each of these terms and conditions shall be severable and if any provision thereof is declared invalid, illegal or unenforceable, then remaining provisions nevertheless shall have full force & effect.

                                        <h2>Liberty Of Activity </h2>
                                        <p>
                                            Company as an organization is co-ordinating with various independent service providers for conducting the tour. The company takes utmost precaution to ensure that the tour is comfortable. Also, Guests are updated from time to time through different media of communication in case of changes.
                                        </p>
                                        <p>
                                            Though we take maximum care, neither we have control on the mode of transport like shipping, Airline, railways, coaches, buses etc nor on the hotels, restaurants or any other facilities.
                                        </p>
                                        <p>
                                            Thus as a company we shall not be responsible for any kind of damage to the person or to the property because of these independent vendors.
                                        </p>

                                        <h2>Booking Criteria</h2>
                                        <p>Guest's interest to participate in the tour.</p>
                                        <p>Payment of the registration amount indicates that the guest is interested in the tour and has read and accepted all the terms and conditions mentioned on the lowcosttrip.com brochure/website/booking form. In case of a guest who is processing the booking formalities on behalf of his/her family is deemed to have been authorised to do so by his family after reading all the lowcosttrip.com terms and conditions mentioned on the brochure/website/booking form</p>
                                        <p>Thus as a company we shall not be responsible for any kind of damage to the person or to the property because of these independent vendors.</p>

                                        <h2>Health</h2>
                                        <p>When you book a tour, or when someone else books a tour for you, on your name , that means you are physically fit to travel to that destination. It is very important for you to take care of your health throughout the tour. Any health issue on tour or any stay back due to the same shall be borne by that respective guest only, the company shall not be held responsible in whatsoever manner.</p>


                                        <h2>Non Payment Of Full Tour Price Before Departure</h2>
                                        <p>The company reserves the right to cancel and forfeit the paid amount in case of non payment of full tour cost 45 days prior to the departure date.</p>


                                        <h2>Tour Payment By Guest Registration Amount</h2>
                                        <p>The registration amount paid at the time of booking is non refundable and interest free. Guests have to pay the full tour price as per the guidelines given in the brochure.</p>
                                        <p>In case of a cheque paid by the guest in favour of the company is dishonored, a charge of Rs. 150 per cheque must be paid.</p>
                                        <p>If a Guest books a tour just 45 days prior to the departure date, he/she should make a payment of the entire amount. Also, the amount should be paid by demand draft.</p>
                                        <p>Payments can be made online at www.lowcosttrip.com by Credit Card/Debit Card/Net Banking or by Cheque/Demand Draft favouring "i q comtech Pvt. Ltd.". i q comtech Pvt. Ltd. reserves the right to cancel your booking without notice if the payment is not cleared by the given date.</p>

                                        <h2>Cost Of The Tour</h2>
                                        <p>When the tour cost is quoted, the Foreign Exchange component is calculated as per the prevailing rate of that day. The company reserves the right to alter the cost of the tour before the departure, accordingly the guest will have to make the full payment before tour departure. Tour costs do not include government or other taxes raised by independent contractors like airlines, which are to be paid by the guest as per the actual.</p>

                                        <h2>Discounts For Tour</h2>
                                        <p>Guests can avail early booking discounts declared by the company, which are valid for a declared period only and are applicable for limited seats or for the particular dates as specified. The company reserves all rights to make changes in the discount policy at any point of time without assigning any reason.</p>

                                        <h2>Additional Taxes By Airlines, Visa & Government</h2>
                                        <p>Airline is an independent vendor, the Company does not have any control on their operations; neither on the increase in air fare, fuel or any other taxes like YQ & YR etc. All such hikes have to be paid by the guest before the tour departure. Air Ticket Cancellation charges plus deposit forfeit charges applicable under the rules of the concerned Airline are to be paid by the guest immediately in the event of cancellation of the tour.</p>
                                        <p>Any Hike in visa fee/VFS fees or increase in government tax are not under the company's control. Therefore, any such additional charges shall be paid by the guest.</p>

                                        <br/>
                                        <h2>Documentation Required For A Tour</h2>
                                        <br/>
                                        <h2>Passport</h2>
                                        <p>For a guest travelling abroad, a passport is an official document and also an identity. A Visa is a must to enter any particular country.</p>
                                        <p>The Passport should be valid for a minimum of 180 days from the date of arrival conclusion in India. Please cross check all your particulars including your name/ address/validity/expiry date and Blank pages for VISAs as per requirement. All travellers booking tours for the European sector should ensure that the Passport validity is for a maximum of 10 years. The passports are well maintained and should have sufficient blank pages (that are not damaged/stapled/torn) for recording stamping of visas of various countries. Handwritten passports are not allowed.</p>

                                        <h2>Visa (World Tours)</h2>
                                        <p>
                                            Visa permits the guest to enter another country. For tourists, generally a short term visa is granted by the respective consulate. We are visa facilitators and generally assist guests for preparing Visa documents, submitting them & collecting the passports from the consulate. The Consulate may call the guest for a personal interview even after all relevant documents have been submitted. The company is not responsible for any kind of error caused by the consulate. The decision to issue the Visa is at the sole discretion of the respective consulate; the company does not have a hold on the decision of the consulate and cannot object to it either. Just in case your visa is granted on the old passport and has a validity kindly cross check weather you need to transfer the visa on the current passport.
                                            <br/>As stated, lowcosttrip.com is just a facilitator for procuring visas. We will not be able to represent any visa issue, either pre or post submission and after issuance of visa. lowcosttrip.com requests all its guests to cross-check their visas copies and intimate any discrepancies well in time to their travel advisor
                                            <br/>Sometimes your Visa which is granted may have a longer validity than your passport in such a case please remember to carry your old passport along with your new passport.
                                            <br/>Mandatory Documents required for the visa
                                            <br/>Duly signed & filled VISA forms by each guest as per Passport signature & details. For Children, Thumb impression - If passport has thumb impression or signature along with Parent's Signature - Male - Left Thumb & Female Right Thumb.
                                            <br/>A Personal Covering letter stating purpose of the travel - Personal / Tour/ Employment etc. along with financial/ sponsorship details. (For Employed on Plain paper, Self Employed on Plain paper with Visiting Card, Business or Professionals on their letter head) Sometimes due to load factor at the embassies and consulates, the VISA processing takes longer time and in few cases VISA is not granted before tour departure or it is delayed. In such cases, LCT as a facilitator shall not be responsible for any consequences and/or liable for any refund, the guest shall have to bear all the cancellation and re-booking charges for the next tour.
                                            <br/>Many countries are following the biometric system in a bid to ensure safer travel. For U.K & Schengen visas you will have to visit the VFS personally and in case of USA you will need to visit the VFS twice. Once for Biometric and then for a personal interview
                                        </p>


                                        <h2>Immigration</h2>
                                        <p>Though you are holding a valid a visa, it only gives you the permission to enter the respective countries immigration point. Granting or rejecting the permission to enter a country is solely dependent upon the discretion of the immigration authority only. Sometimes a guest may be deported/ interrogated/ and may be held back by the immigration authorities for a longer span than the usual time. In such a case if the guest misses out on the itinerary program the company is not liable.</p>

                                        <h2>Travel Insurance</h2>
                                        <div class="p_detail">
                                            <p>Insurance is included in all the international tours upto age of 59 years. The over and above difference to be paid by the respective guest. Usually overseas Mediclaim Insurance covers hospitalization, medical emergencies and the loss of passport; hence, the company strongly recommends to FIT guests that they too buy the Insurance.</p>
                                            <p>In case of claims, the insurance company shall be solely responsible for granting or rejecting claims and we do not have any hold over it. It is at a sole discretion of the respective company we are only facilitator. Also, insurance does not cover pre-diagnosed ailments. In case of foreign nationals, insurance will have to be obtained by the individuals on their own.</p>
                                            <p>In case an insurance is issued by a third party, and if any claims to the same arise, lowcosttrip.com  is not responsible for any coordination with the Insurance Company</p>
                                        </div>

                                        <h2>Itinerary Changes / Changes In The Itinerary</h2>
                                        <div class="p_detail">
                                            <p>The Company reserves the right to modify or alter the tour itinerary in case of force majeure conditions. Changes shall be informed to the guests at the earliest. Any additional charges, if applicable, are to be paid by the guest.</p>
                                            <p>Other than force majeure, for any other unavoidable circumstances, if the company curtails or cancels a certain day's sightseeing, refund for the concerned portion of the sightseeing/tour shall be given to the guest. The Company will not be responsible for any kind of claims.</p>
                                            <p>The Company reserves the right to change (addition/deletion on exchange) sightseeing places for betterment of the tour programme.</p>
                                            <p>If sightseeing timings aren't adhered to, or if an any add-ons are required then the same will be borne by the guests.</p>
                                            <p>Sightseeing days and closing of a particular sightseeing place are not in our hand, but we try our level best to cover all the sightseeings listed in the tour program. In case if any sightseeing is closed we may give you a replacement of that place or refund may be given for that particular sightseeing place only. Sightseeings of adventurous type are totally dependent on weather. In case, the weather turns out to be unfavourable &amp; sightseeing is not possible, refund will be given.</p>
                                        </div>

                                        <h2>Joining/Leaving Guests</h2>
                                        <p>Guest may join our tour at the first destination as per their convenience. In such a case, guests must possess all the air tickets of the family, Visa and Insurance documents and are required to join the group at the first destination of the Tour. Such guests should furnish all information such as Flight details, local contact details & e-mail ID a minimum of 15 days prior to the departure. Not to issue non refundable tickets incase any change in date charged to be borne by guest.</p>

                                        <h2>Deviation Guests</h2>
                                        <div class="p_detail">
                                            <p>If a guest who has booked a group tour wishes to deviate from the tour schedule before or after the tour, she/he can do so by paying an additional cost.</p>
                                            <p>For Individual Bookings, Guests on an Indian Tour can co-ordinate with the driver of the car/coach. Guests on a World tour can co-ordinate with ground handling agents to plan the next day's visit and sightseeing program. The car/coach has specific time limits within which it operates coach.</p>
                                        </div>

                                        <h2>Coach</h2>
                                        <div class="p_detail">
                                            <p>Seating arrangement in the coach shall be strictly as per the booking date i.e. the guest who books first will get the first seat and so on.</p>
                                            <p>In coach, the seating allotment shall start from seat number 5 onwards as first two seats are reserved for the tour managers &amp; seat nos. 3 &amp; 4 are reserved as Prime Seats which if available can be booked by the guest at an additional cost. The company reserves the right to allocate Prime seats. If paid and prime seat is not available due to any reason, company is liable only to refund the amount paid for the prime seat.</p>
                                            <p>There is a strict "no smoking", "no alcohol" and "no eating" policy in the Coach. Guests are advised not to keep valuables in coach while sightseeing. The Company shall not be responsible for the loss of valuables from the coach. If the coach is accidently or otherwise damaged by the guest he/she will be required to pay the compensation for the same.</p>
                                        </div>

                                        <h2>World Tours Coaches / Buses</h2>
                                        <p>The Company generally arranges air-conditioned/air-cooled, luxury coaches. Such coaches may be equipped with an emergency washroom, however this facility is not to be used in lieu of the rest rooms. In case, guest wishes to use this facility, he shall have to pay tips to the driver of the coach directly. The size and seating capacity of the coaches / buses generally varies from 20 to 40 and depending on the number of the guest/s. few coaches have one door or two door access, which varies from destination to destination.</p>

                                        <h2>Indian Tours Coaches / Buses</h2>
                                        <div class="p_detail">
                                            <p>The Company generally arranges air-conditioned/air-cooled, Non-AC coaches, mini coaches, jeeps or taxies of various seating capacities depending on the type of the tour, itinerary and number of guest/s, etc.</p>
                                            <p>In case of part tours which are a part of the main tours, the seating priority will be as per the booking.</p>
                                            <p>Sometimes certain tours are merged upon the guests request or due to situational conditions. In such a case, the seating priority will be as per the booking.</p>
                                            <div class="p_detail">
                                                <p>Seating arrangement in the coach shall be strictly as per the booking date i.e. the guest who books first will get the first seat and so on.</p>
                                                <p>In coach, the seating allotment shall start from seat number 5 onwards as first two seats are reserved for the tour managers &amp; seat nos. 3 &amp; 4 are reserved as Prime Seats which if available can be booked by the guest at an additional cost. The company reserves the right to allocate Prime seats. If paid and prime seat is not available due to any reason, company is liable only to refund the amount paid for the prime seat.</p>
                                                <p>There is a strict "no smoking", "no alcohol" and "no eating" policy in the Coach. Guests are advised not to keep valuables in coach while sightseeing. The Company shall not be responsible for the loss of valuables from the coach. If the coach is accidently or otherwise damaged by the guest he/she will be required to pay the compensation for the same.</p>
                                            </div></div>

                                        <h2>Baggage</h2>
                                        <p>Guest/s are responsible for their own baggage. The baggage at the airport, during any air travel, in coaches, in any train journey, at immigration point’s etc., guest/s have to verify and be responsible for the same. The porterage or the tips for services rendered to carry guest/s baggage to the rooms is guest/s responsibility and have to be paid directly to the service provider. The "porter" or any helper arranged by the guest/s at any train stations, airports, at immigration points or any other place, is the sole responsibility of the guest/s and guest/s shall have to pay tip directly to them.</p>
                                    </div>


                                </div>
                            </div>
                        </div>
                        

                    </div>

                </div>
            </div>



        </div>



    </div>

</section>


@endsection

@section('javascript')
<script type="text/javascript">

    // Pure Javascript
    document.getElementsByClassName("activity-details").onmouseover = function () {
        document.getElementsByClassName("activity-overlay").style.display = 'block';
    }
    document.getElementsByClassName("activity-details").onmouseout = function () {
        document.getElementsByClassName("activity-overlay").style.display = 'none';
    }


</script>
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
<script>
    $('.testi-carousel').owlCarousel({
        loop: true,
        margin: 0,
        dots: false,
        responsiveClass: true,
        nav: true,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false,
                margin: 0
            }
        }
    });

    $('.ap-carousel').owlCarousel({
        loop: true,
        margin: 0,
        dots: false,
        responsiveClass: true,
        nav: true,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: false,
                margin: 0
            }
        }
    });
</script>
@endsection