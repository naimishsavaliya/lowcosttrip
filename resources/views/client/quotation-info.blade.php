
@extends('layouts.client')

@section('content')
<script type="text/javascript">
    cityData = '';
</script>
<section>
    <style>
        .invoice-border{border-top:1px solid #DDD;border-bottom:1px solid #DDD;border-right:2px solid #DDD;border-left:2px solid #DDD;}
        .tour-desc{border-bottom:1px solid #DDD;margin-top:15px}
        .badge-bg{background-color: #17a2b8; border-color: #17a2b8;}
        .mg-b{margin-bottom:15px;}
        .lead-info-table table, td{padding:3px 0px;}
        .f-r{float: right;}
        .m-r{margin-left: 10px}
        td, th{
            font-size: 14px;
        }
    </style>
    <div class="dashboard-tabs">
        <div class="dashboard-tabs-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span class="dashboard-title">{{ isset($quotation->quatation_id) ? $quotation->quatation_id : ''}}</a></span>
                        <!-- <a class="btn btn-danger f-r m-r" href="{{route('view-quotation', $lead->lead_id )}}">Back</a> -->
                        <button class="btn btn-danger f-r m-r back-button">Back</button>
                        <a class="btn btn-danger f-r" href="{{route('mail-quotation', [$quotation->lead_id, $quotation->quaId])}}">Send quote to mail</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="dashboard-tabs-content">
            <div class="container">
                <div class="row">
                    @if(session()->has('message'))
                    <div class="col-md-12" style="margin-top:10px;padding: 0">
                        <div class="alert {{ session()->get('alert-class') }}">
                            {{ session()->get('message') }}
                        </div>
                    </div>
                    @endif
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="border" style="margin-top:10px;">
                            <div class="row invoice-border" >
                                <div class="col-md-12 col-sm-12"> 
                                    <p style="text-align: center; font-size: 25px;">Quote For {{ $quotation->subject }}</p>
                                </div>
                                <div class="col-md-5 col-sm-5"> 
                                    <div class="table-responsive">
                                        <span style="font-size: 14px !important ">Quotation Date: {{ isset($quotation->quatation_date) ? date('d M Y', strtotime($quotation->quatation_date)) : '-' }}</span>
                                        <table class="table table-bordered"  style="margin-top:10px;">
                                            <thead>
                                                <tr>
                                                    <th width="30%">Customer Name</th>
                                                    <td width="70%">{{ isset($user_info->first_name) ? ucwords($user_info->first_name. ' '. $user_info->last_name): '' }}</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>Address</th>
                                                    <td>
                                                        {{ $lead->address }}
                                                    </td>
                                                </tr>
                                                 <tr>
                                                <th>Country</th>
                                                <td>{{ isset($user_info->country_name) ? $user_info->country_name : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>State</th>
                                                <td>{{ isset($user_info->state_name) ? $user_info->state_name : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>City></th>
                                                <td>{{ isset($user_info->city_name) ? $user_info->city_name : '' }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="col-md-1 col-sm-1 text-right"> 
                                </div>
                                <div class="col-md-6 col-sm-6 text-right"> 
                                    <span style="font-size: 14px !important ">Valid Up To: {{ isset($quotation->validup_date) ? date('d M Y', strtotime($quotation->validup_date)) : '-' }}</span>
                                    <div class="table-responsive">
                                        <table class="table table-bordered"  style="margin-top:10px;"> 
                                            <tbody>
                                                <tr>
                                                    <th>Customer Mo.</th>
                                                    <td>{{ isset($user_info->phone_no) ? $user_info->phone_no : '' }}</td>
                                                </tr>

                                                <tr>
                                                    <th>Customer Email</th>
                                                    <td>{{ isset($user_info->email) ? $user_info->email : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Quote Status</th>
                                                    <td>
                                                        <a href="javascript:void(0)" class="quote_change_status"><i class="fa fa-edit text-danger" style="float: right; margin-top: -8px; margin-right: -8px;"></i></a>

                                                        <?php /*
                                                        <?php if ($quotation->status_id == 4) { ?>
                                                            <a href="javascript:void(0)" class="quotation-change-status-btn" 
                                                               data-id="{{ isset($quotation->status_id) ? $quotation->status_id : '' }}" row-id="{{ isset($quotation->quaId) ? $quotation->quaId : '' }}" data-toggle="modal" data-target="#quotation_update_status"><span class="badge badge-bg">Quote Rejected</span></a>
                                                           <?php } elseif ($quotation->status_id == 3) { ?>
                                                            <a href="javascript:void(0)" class="quotation-change-status-btn" 
                                                               data-id="{{ isset($quotation->status_id) ? $quotation->status_id : '' }}" row-id="{{ isset($quotation->quaId) ? $quotation->quaId : '' }}" data-toggle="modal" data-target="#quotation_update_status"><span class="badge badge-bg">Quote Approved</span></a> 
                                                           <?php } ?> 
                                                           */ ?>
                                                           <input type="hidden" value="{{$quotation->quaId}}" id="quotation-change-status-row-id" />
                                                            <select class="form-control" id="quotation_info_status_id"   title="Select Status..." style="display: none">
                                                                <option value="">Select Status</option>
                                                                <option value="3" <?php echo ((isset($quotation->status_id) && $quotation->status_id == '3')? 'selected="selected"' : '') ?> >Quote Approved</option>
                                                                <option value="4" <?php echo ((isset($quotation->status_id) && $quotation->status_id == '4')? 'selected="selected"' : '') ?> >Quote Rejected</option>
                                                            </select>
                                                            <span class="badge badge-bg quote_status_view">
                                                                 {{ isset($quotation->quotation_status) ? $quotation->quotation_status : '-' }}
                                                            </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Quote Made By</th>
                                                    <td>{{ $quotation->first_name .' '. $quotation->last_name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Quick contact</th>
                                                    <td>{{ $quotation->quick_contact }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <h3>
                                        
                                    </h3>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="5%">No</th>
                                                    <th width="45%">Particulars</th>
                                                    <th width="10%">Qty/Night/Room</th>
                                                    <th width="10%">Rate</th>
                                                    <th width="10%">Discount</th>
                                                    <th width="10%">GST (%)</th>
                                                    <th width="10%">Line Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($particular))

                                                @php 
                                                $total_cost = $final_cost = $cgst = $igst = $sgst  = 0; 
                                                @endphp

                                                @foreach($particular as $par_key=>$par_value)

                                                @php
                                                $linetotal = $par_value->qty * $par_value->cost;
                                                if(isset($par_value->discount)){
                                                $linetotal = $linetotal - $par_value->discount;
                                                }
                                                $total_cost = $total_cost + $linetotal;

                                                $cgst = $cgst + (($par_value->cgst > 0)?  (($linetotal *  $par_value->cgst) / 100) :0);
                                                $sgst = $sgst + (($par_value->sgst > 0)?  (($linetotal *  $par_value->sgst) / 100) :0);
                                                $igst = $igst + (($par_value->igst > 0)?  (($linetotal *  $par_value->igst) / 100) :0);

                                                @endphp

                                                <tr class="table-middle">
                                                    <td>{{ $par_value->item_no }}</td>
                                                    <td>{{ $par_value->item_title }}</td>
                                                    <td class="text-right">{{ $par_value->qty }}</td>
                                                    <td class="text-right">{{ $par_value->cost }}</td>
                                                    <td class="text-right">{{ $par_value->discount }}</td>
                                                    <td class="text-right">{{ number_format($par_value->gst , 2) }}</td>
                                                    <td class="text-right">{{ number_format($linetotal, 2) }}</td>
                                                </tr>
                                                @endforeach
                                                <tr class="table-middle">
                                                    <td colspan="4" rowspan="5">
                                                    <td colspan="2" class="text-right"><b>Sub Total</b></td>
                                                    <td class="text-right"> {{ number_format(round($total_cost), 2) }}</td>
                                                </tr>
                                                <tr class="table-middle">
                                                    <td colspan="2" class="text-right"><b>CGST (+)</b></td>
                                                    <td class="text-right">{{ number_format(round($cgst) , 2) }}</td>
                                                </tr>
                                                <tr class="table-middle">
                                                    <td colspan="2" class="text-right"><b>SGST (+) </b></td>
                                                    <td class="text-right">{{ number_format(round($sgst) , 2) }}</td>
                                                </tr>
                                                <tr class="table-middle">
                                                    <td colspan="2" class="text-right"><b>IGST (+) </b></td>
                                                    <td class="text-right">{{ number_format(round($igst) , 2) }}</td>
                                                </tr>
                                                <tr class="table-middle">
                                                    <td colspan="2" class="text-right"><b>Grand Total ({{$quotation->code}})</b></td>
                                                    <?php $total_amount = round($total_cost + $cgst + $sgst + $igst); ?>
                                                    <td class="text-right">{{ number_format(($total_amount) , 2) }}</td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12" style="padding-bottom:15px">
                                    <div class="col-md-10 col-sm-6"  style="padding:0px">
                                        <p class="text-small"><strong>Amount In Word:</strong><?php echo number_to_word(number_format(($total_amount), 2), $quotation->code); ?></p>
                                    </div>

                                    <div class="col-md-2 col-sm-6 text-right"  style="padding:0px">
                                        <?php if ($quotation->status_id == 3) { ?>
                                            <?php /* <a class="btn btn-success" href="{{route('quotation.pay-now', [$quotation->lead_id, $quotation->quaId])}}">PAY NOW</a>  */ ?>

                                             <a class="btn btn-success" href="{{route('quotation.make.payment', [$quotation->lead_id, $quotation->quaId])}}">PAY ONLINE</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div  class="border" style="margin:10px 0px;">
                            <div class="row invoice-border">
                                <div class="col-md-12 col-sm-12">
                                    <h3>Daywise Itinerary Plan</h3>
                                    @if(isset($itinarary))
                                    @foreach($itinarary as $iti_key => $iti_value)
                                    <div class="col-md-12 col-sm-12 tour-desc">
                                        <div class="col-md-2 col-sm-4 mg-b">
                                            <img height="150" width="150" class="md-card-head-img" src="{{ env('APP_PUBLIC_URL')}}/uploads/itinerary/{{$iti_value->image}}" alt="">
                                        </div>
                                        <div class="col-md-10 col-sm-8 ">
                                            <h4> Day {{ $iti_value->day .' '. $iti_value->title }} </h4>
                                            <h6 class="text-muted" style="line-height:20px">{{ $iti_value->description }} </h6>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class="col-md-12 col-sm-12"> 
                                    <div class="col-md-6 col-sm-12">
                                        <h3>Inclusions</h3>
                                        <h6 class="text-muted" style="line-height:20px">{{ isset($quotation->inclusion) ? $quotation->inclusion : '' }}</h6>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <h3>Exclusions </h3>
                                        <h6 class="text-muted" style="line-height:20px">{{ isset($quotation->exclusion) ? $quotation->exclusion : '' }}</h6>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="col-md-12 col-sm-12">
                                        <h3 class="heading_c margin-small-bottom">Terms & Conditions</h3>
                                        <h6 class="text-muted" style="line-height:20px">{{ isset($quotation->terms_condition) ? $quotation->terms_condition : '' }}</h6>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</section>
@endsection

@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
@endsection
