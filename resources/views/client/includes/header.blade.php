<header>
    <div class="hidden-xs">
        <div class="main-nav">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="logo">
                          <?php /*
                            @if(\Request::is('indian-holidays') || \Request::is('indian-holidays/*') ||
                              \Request::is('global-holidays') || \Request::is('global-holidays/*') || 
                              \Request::is('holiday-detail/*') || \Request::is('tour-compare') ||
                             \Request::is('tour-filter') ||  
                              \Request::is('home/customise-book')
                              )
                                <a class="navbar-brand" href="{{route('index')}}"><img src="{{ env('APP_PUBLIC_URL')}}client/images/Trip_Cafe_logo.jpg" class="img-responsive"></a>
                                <!-- <a class="navbar-brand" href="{{route('index')}}"><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/logo.jpg" class="img-responsive"></a> -->
                            @else
                                <a class="navbar-brand" href="{{route('index')}}"><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/logo.jpg" class="img-responsive"></a>
                            @endif
                            */ ?> 
                             <a class="navbar-brand" href="{{route('index')}}"><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/logo.jpg" class="img-responsive"></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <ul class="nav nav-tabs navbar-nav " id="myTab">
                            <li><a class="header-link rediret-link" href="{{env('APP_URL')}}home/customise-book#holiday"> Holidays </a> </li>
                            <li><a class="header-link rediret-link" href="{{env('APP_URL')}}home/customise-book#Hotel"> Hotels </a></li>
                            <li><a class="header-link rediret-link" href="{{ route('flight.booking') }}"> Flights </a></li>
                            <li><a class="header-link rediret-link" href="{{env('APP_URL')}}home/customise-book#visa"> Visa </a></li> 
                            @if (Auth::user())                            
                            <li><a class="header-link" href="{{ route('user_dashboard') }}" title="logout">Dashboard</a></li>
                            @else
                            <li><a class="header-link" href="{{ route('client_login_form') }}" title="login">Login/Sign Up</a></li>
                            <li><a class="header-link" href="{{ route('agent_login_form') }}" title="Agent login">For Agents</a></li>
                            @endif
                            <li><a class="header-link" href="javascript:void(0)"><i class="fa fa-phone"></i> &nbsp;81 82 83 84 85</a> </li>
                            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#search-dialog"><i class="header-link fa fa-search"></i> </a> </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div id="myTabContent" class="tab-content">
                <!-- Holiday Block Begins -->
                <div class="tab-pane fade in active" id="holiday-block">
                    <!-- Search Block Begins -->
                    <div class="search-block hotel-search">
                        <div class="main-sub-nav">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-offset-3">
                                        <nav>
                                            <ul class="ul-reset">
                                                <?php /*
                                                <li class="droppable"> <a href="{{route('indian-holiday')}}">Domestic Tours</a>
                                                    <div class="mega-menu">
                                                        <div class="cf">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="dropdown-menu-item">
                                                                                @if(isset($domestic_sectors) && !empty($domestic_sectors))
                                                                                  @foreach($domestic_sectors as $domesticSector)
                                                                                   @php $sname = preg_replace('/\W+/', '-', strtolower($domesticSector->name)); @endphp
                                                                                    <div class="col-sm-2">
                                                                                      <h4>
                                                                                        <a href="{{route('indian-holiday.sector',[$domesticSector->id,$sname])}}"> <?php echo isset($domesticSector->logo) ? '<img src="'.env('APP_PUBLIC_URL').'uploads/sector-logo/'.$domesticSector->logo.'" alt=" • " height="15" width="15">' : ' • '; ?>   {{$domesticSector->name}}</a>
                                                                                      </h4>
                                                                                    </div>
                                                                                  @endforeach
                                                                                @endif
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                */ ?>
                                                <li class="droppable"> <a href="{{route('indian-holiday')}}">Domestic Tours</a>
                                                  <div class='mega-menu'>
                                                    <div class="cf">
                                                      <div class="row">
                                                        @if(isset($domestic_sectors) && !empty($domestic_sectors))
                                                        @foreach($domestic_sectors as $domesticSector)
                                                        @php $sname = preg_replace('/\W+/', '-', strtolower($domesticSector->name)); @endphp
                                                        <a href="{{route('indian-holiday.sector',[$domesticSector->id,$sname])}}" class="">
                                                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="megamenu-image-block">
                                                            <?php echo isset($domesticSector->logo) ? '<img src="'.env('APP_PUBLIC_URL').'uploads/sector-logo/'.$domesticSector->logo.'" alt=" • " height="15" width="15">' : ' • '; ?>
                                                            <h4>{{$domesticSector->name}}</h4>   
                                                            </div>
                                                          </div>
                                                          </a>
                                                        @endforeach
                                                        @endif
                                                      </div>
                                                    </div>
                                                  </div>              
                                                </li>
                                                <li class="droppable"> <a href="{{route('global-holiday')}}">International Tours</a>
                                                  <div class='mega-menu'>
                                                    <div class="cf">
                                                      <div class="row">
                                                            @if(isset($global_sectors) && !empty($global_sectors))
                                                            @foreach($global_sectors as $globalSector)
                                                            @php $gname = preg_replace('/\W+/', '-', strtolower($globalSector->name)); @endphp
                                                            <a href="{{route('global-holiday.sector',[$globalSector->id,$gname])}}" class="">
                                                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          
                                                            <div class="megamenu-image-block">
                                                           <?php echo isset($globalSector->logo) ? '<img src="'.env('APP_PUBLIC_URL').'uploads/sector-logo/'.$globalSector->logo.'" alt=" • " height="15" width="15">' : ' • '; ?>  
                                                            <h4>{{$globalSector->name}}</h4>   
                                                            </div>
                                                          </div>
                                                          </a>
                                                        @endforeach
                                                        @endif
                                                      </div>
                                                    </div>
                                                  </div>              
                                                </li>
                                                <li class="droppable"><a href="{{route('speciality-tours')}}">Speciality Tours</a>
                                                  <div class='mega-menu'>
                                                    <div class="cf">
                                                      <?php /*
                                                      <div class="row">
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Festival Tours.png" class="img-responsive clearfix">
                                                            <h4>Festival Holidays</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Senior Citizen Tours.png" class="img-responsive clearfix">
                                                            <h4>
                                                            Senior Citizen Tours</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Honeymoon Tours.png" class="img-responsive clearfix">
                                                            <h4>Romantic Honeymoon tours</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Wildlife Tours.png" class="img-responsive clearfix">
                                                            <h4>Wild Life Tours</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Weekend Tours.png" class="img-responsive clearfix">
                                                            <h4>Weekend Gateways</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Woman Special Tours.png" class="img-responsive clearfix">
                                                            <h4>Woman Special Tours</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Solo Trip.png" class="img-responsive clearfix">
                                                            <h4>Solo Trips</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Adventure Tours.png" class="img-responsive clearfix">
                                                            <h4>Adventure Tours</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Spiritual Tours.png" class="img-responsive clearfix">
                                                            <h4>Spiritual Tours</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Pilgrimage-Yatra.png" class="img-responsive clearfix">
                                                            <h4>Pilgrimage / Yatra Tours</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Royal Holidays Tours.png" class="img-responsive clearfix">
                                                            <h4>Royal Holidays</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                        <a href="#" class="">
                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                          <div class="megamenu-image-block">
                                                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/Speciality Tours Icons/Cruise Holidays.png" class="img-responsive clearfix">
                                                            <h4>Cruise Holidays</h4>
                                                          </div>
                                                        </div>
                                                        </a>
                                                      </div>
                                                      */ ?>
                                                      <div class="row">
                                                      @if(isset($SpecialtyTours) && !empty($SpecialtyTours))
                                                      @foreach($SpecialtyTours as $SpecialtyTour)
                                                      @php $sname = preg_replace('/\W+/', '-', strtolower($SpecialtyTour->name)); @endphp
                                                        <a href="{{route('speciality-tours.category',[$SpecialtyTour->typeId,$sname])}}" class="">
                                                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="megamenu-image-block">
                                                            <?php echo isset($SpecialtyTour->logo) ? '<img src="'.env('APP_PUBLIC_URL').'uploads/tour-category/'.$SpecialtyTour->logo.'" alt=" • " height="15" width="15">' : ' • '; ?>  
                                                            <h4>{{$SpecialtyTour->name}}</h4>   
                                                            </div>
                                                          </div>
                                                        </a>
                                                      @endforeach
                                                      @endif
                                                      </div>
                                                    </div>
                                                  </div>              
                                                </li>
                                                <li class="droppable"> <a href="{{route('cruise-tours')}}">Cruise Tours</a>
                                                  <div class='mega-menu'>
                                                    <div class="cf">
                                                      <div class="row">
                                                        @if(isset($CruiseSections) && !empty($CruiseSections))
                                                        @foreach($CruiseSections as $cruise_section)
                                                        <a href="{{route('holiday-detail',[$cruise_section->id,str_slug($cruise_section->tour_name,'-')])}}" class="">
                                                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="megamenu-image-block">
                                                            <?php echo isset($cruise_section->image) ? '<img src="'.env('AWS_BUCKET').$cruise_section->image.'" alt=" • " height="15" width="15">' : ' • '; ?>
                                                            <h4>{{(isset($cruise_section->tour_name) ? $cruise_section->tour_name :'')}}</h4>   
                                                            </div>
                                                          </div>
                                                          </a>
                                                        @endforeach
                                                        @endif
                                                      </div>
                                                    </div>
                                                  </div>           
                                                </li>
                                                <li><a href='{{env("APP_URL")}}home/customise-book'>Customise & Book</a></li>
                                                <?php /*
                                                <li><a href="javascript:void(0)">Group Tours</a></li>
                                                <li><a href="javascript:void(0)">Deals</a></li>
                                                */ ?>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        @if(Request::url() == route('index'))
                        <div class="holiday-bottom">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-lg-offset-1">
                                        <form method="GET" id="tour_filter_form" name="tour_filter_form" action="{{ route('tour_filter') }}">
                                            <div class="row">
                                                <div>
                                                    <input type="text" placeholder="Where you want to go ?" name="search" class=" search-dest">
                                                </div>
                                                <div>
                                                    <button type="submit" class=" btn-search ">Search</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                        <div class="text-center">
                                            <p class="search-span">or</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
                                        <div class="row">
                                            <div class="search-cust">
                                                <a href='{{env("APP_URL")}}home/customise-book' class="btn btn-default btn-customize">Build your package</a>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- Search Block Ends -->
                </div>
                <!-- Holiday Block Ends  -->

                <!-- Flight Block Begins -->
                <div class="tab-pane fade" id="flight-block">
                    <!-- Sub Menu Begins -->
                    <!-- Sub Menu Ends -->
                    <!-- Search Block Begins -->
                    <div class="search-block flight-search">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                            <div class="search-head">
                                                <h4>Domestic and International Flights</h4>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <ul class="travel-type">
                                                <li class="active"><a href="javascript:void(0)">One Way</a></li>
                                                <li><a href="javascript:void(0)">Round Trip</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-6">
                                    <div class="input-group">
                                        <label>From</label>
                                        <input type="text" class="form-control" name="" placeholder="From city">
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-6">
                                    <div class="input-group">
                                        <label>To</label>
                                        <input type="text" class="form-control" name="" placeholder="To city">
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <label>Depart</label>
                                                <input type="text" class="form-control" name="email" placeholder="Depart">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <label>Return</label>
                                                <input type="text" class="form-control" name="email" placeholder="Return Date">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <label>Passengers</label>
                                                <input type="text" class="form-control" name="email" placeholder="1">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="input-group">
                                                <label>Class</label>
                                                <select class="form-control">
                                                    <option>Economy</option>
                                                    <option>Business Class</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12">
                                    <button type="submit" class="btn-flight ">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Search Block Ends -->
                </div>
                <!-- Flight Block Ends -->

                <!-- Hotel Block Begins -->
                <div class="tab-pane fade" id="hotel-block">
                    <!-- Search Block Begins -->
                    <div class="search-block hotel-search">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                            <div class="search-head">
                                                <h4>Domestic and International Hotels</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <div class="input-group">
                                        <label>City, Hotel or Area</label>
                                        <input type="text" class="form-control" name="" placeholder="From city">
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-6">
                                    <div class="input-group">
                                        <label>Check In</label>
                                        <input type="text" class="form-control" name="" placeholder="Check in date">
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12">
                                    <div class="input-group">
                                        <label>Check Out</label>
                                        <input type="text" class="form-control" name="email" placeholder="Check out date">
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12">
                                    <div class="input-group">
                                        <label>Room/guests </label>
                                        <input type="text" class="form-control" name="email" placeholder="Room">
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12">
                                    <button type="submit" class="btn-flight ">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Search Block Ends -->
                </div>
                <!-- Hotel Block Ends -->

                <!-- visa Block Begins -->
                <div class="tab-pane fade" id="visa-block">
                    <!-- Search Block Begins -->
                    <div class="search-block">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                                    <div class="search-head">
                                        <h4>Visa Services</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="visa-info">
                                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-10">
                                        <p>For More Info </p>
                                        <p><i class="fa fa-phone"></i> +00 00 0000 0000</p>
                                        <p><i class="fa fa-envelope"></i> visa@lowcosttrip.com</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                                        <button type="submit" class="btn-flight no-margin ">Send Enquiry</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Search Block Ends -->
                        </div>
                        <!-- Activity Block Ends -->
                    </div>
                </div>
                <!-- visa Block Ends -->

                <!-- customizes Block Begins -->
                <div class="tab-pane fade" id="customizes-block">
                    <div class="customize">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
                                    <div class="cust-form">
                                        <div class="text-center">
                                            <h2>Customise &amp; Book </h2>
                                        </div>
                                        <form action="/action_page.php">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Mobile No">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <select class="form-control">
                                                        <option>Within India</option>
                                                        <option>Asia</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <select class="form-control">
                                                        <option>Ahmedabad</option>
                                                        <option>Mumbai</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                                <option>Days</option>
                                                                <option>1</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                                <option>Adults</option>
                                                                <option>1</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn-cust">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- customizes Block Ends -->
                
            </div>
        </div>
    </div>
    <div class="mobile-nav hidden-sm hidden-md hidden-lg">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">

                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="javascript:void(0)"><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/logo.jpg" class="img-responsive"></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
						<li class="active dropdown">
						<a href="{{route('indian-holiday')}}">Domestic Tours</a>
							<img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/plus.png" class="dropdown-toggle img-plus" data-toggle="dropdown">
                            <ul class="dropdown-menu">
								  @if(isset($domestic_sectors) && !empty($domestic_sectors))
                                                        @foreach($domestic_sectors as $domesticSector)
                                                        @php $sname = preg_replace('/\W+/', '-', strtolower($domesticSector->name)); @endphp
                                                        <li>
														<a href="{{route('indian-holiday.sector',[$domesticSector->id,$sname])}}" class="">
                                                       
                                                           <!-- <?php echo isset($domesticSector->logo) ? '<img src="'.env('APP_PUBLIC_URL').'uploads/sector-logo/'.$domesticSector->logo.'" alt=" • " height="15" width="15">' : ' • '; ?>-->
                                                            {{$domesticSector->name}} 
                                                            
                                                          </a>
														  </li>
                                                        @endforeach
                                                        @endif
                            </ul>

                        </li>
                        <li class="active dropdown"><a href="{{route('global-holiday')}}">International Tours</a>
						<img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/plus.png" class="dropdown-toggle img-plus" data-toggle="dropdown">
                            <ul class="dropdown-menu">
							@if(isset($global_sectors) && !empty($global_sectors))
                                                            @foreach($global_sectors as $globalSector)
                                                            @php $gname = preg_replace('/\W+/', '-', strtolower($globalSector->name)); @endphp
                                <li> <a href="{{route('global-holiday.sector',[$globalSector->id,$gname])}}" class="">
								 <!-- <?php echo isset($globalSector->logo) ? '<img src="'.env('APP_PUBLIC_URL').'uploads/sector-logo/'.$globalSector->logo.'" alt=" • " height="15" width="15">' : ' • '; ?>-->
								{{$globalSector->name}}</a></li>
								 @endforeach
                                 @endif
								
                            </ul>

                        </li>
						<li class="active dropdown"><a href="{{route('speciality-tours')}}" >Speciality Tours</a>
						<img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/plus.png" class="dropdown-toggle img-plus" data-toggle="dropdown">
                            <ul class="dropdown-menu">
							 @if(isset($SpecialtyTours) && !empty($SpecialtyTours))
                                                      @foreach($SpecialtyTours as $SpecialtyTour)
                                                      @php $sname = preg_replace('/\W+/', '-', strtolower($SpecialtyTour->name)); @endphp
                                <li> <a href="{{route('speciality-tours.category',[$SpecialtyTour->typeId,$sname])}}" class="">
							 <!--	<?php echo isset($SpecialtyTour->logo) ? '<img src="'.env('APP_PUBLIC_URL').'uploads/tour-category/'.$SpecialtyTour->logo.'" alt=" • " height="15" width="15">' : ' • '; ?> -->
								{{$SpecialtyTour->name}}</a></li>
								 @endforeach
                                 @endif
								
                            </ul>

                        </li>
						<li class="active dropdown"><a href="{{route('cruise-tours')}}" >Cruise Tours</a>
						<img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/plus.png" class="dropdown-toggle img-plus" data-toggle="dropdown">
                            <ul class="dropdown-menu">
							 @if(isset($CruiseSections) && !empty($CruiseSections))
                                                      @foreach($CruiseSections as $cruise_section)
                                <li> <a href="{{route('holiday-detail',[$cruise_section->id,str_slug($cruise_section->tour_name,'-')])}}" class="">
						
								{{(isset($cruise_section->tour_name) ? $cruise_section->tour_name :'')}}</a></li>
								 @endforeach
                                 @endif
								
                            </ul>

                        </li>
						 <li><a href='{{env("APP_URL")}}home/customise-book'>Customise & Book</a></li>
                        <li><a href="{{env('APP_URL')}}home/customise-book#holiday">Holidays</a></li>
                        <li><a href="{{env('APP_URL')}}home/customise-book#Hotel">Hotels</a></li>
                        <li><a href="{{env('APP_URL')}}home/customise-book#Flight">Flights</a></li>
                        <li><a href="{{env('APP_URL')}}home/customise-book#visa">Visa</a></li>
						 @if (Auth::user()) 
                        <li><a href="{{ route('user_dashboard') }}">Dashboard</a></li>
						@else
						<li><a href="{{ route('client_login_form') }}">Login/Sign Up</a></li>
						<li><a href="{{ route('agent_login_form') }}">For Agents</a></li>
						 @endif
						 <li><a href="javascript:void(0);" data-toggle="modal" data-target="#search-dialog">Search</a></li>
                    </ul>

                </div>
            </div>
        </nav>

    </div>
    <div id="search-dialog" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="background: linear-gradient(to left, #ff2682 0%, #ff6541 100%) !important;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color: #FFF;">Search for your holiday destination</h4>
          </div>
          <div class="modal-body">
                <form method="GET" id="tour_filter_form" name="tour_filter_form" action="{{ route('tour_filter') }}">
                    <div class="row">
                        <div>
                            <input type="text" placeholder="Where you want to go ?" name="search" class=" search-dest">
                        </div>
                        <div>
                            <button type="submit" class=" btn-search ">Search</button>
                        </div>
                    </div>
                </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
</header>

