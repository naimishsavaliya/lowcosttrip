<footer>
    <!-- Subscrbe Section Begins -->
    <div class="subscribe">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                         <div class="social text-right">
                            <a class="btn-floating btn-lg btn-gplus social-share" href="https://plus.google.com/106248941767948734837" type="button" role="button"><i class="fa fa-google-plus"></i></a>
                            <a class="btn-floating btn-lg btn-fb social-share" href="https://www.facebook.com/lowcost.trip.92" type="button" role="button"><i class="fa fa-facebook"></i></a>

                            <a class="btn-floating btn-lg btn-tw social-share" href="https://twitter.com/LowcosttripJay" type="button" role="button"><i class="fa fa-twitter"></i></a>

                            <a class="btn-floating btn-lg btn-in social-share" href="https://www.instagram.com/lowcosttrip.com8182838485/" type="button" role="button"><i class="fa fa-instagram"></i></a>

                            
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="subscribe-text">
                            <h2 >Get Our Special Deals</h2>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form method="POST" id="newslettersform" name="newslettersform" action="{{ route('newsletter') }}">
                            @csrf
                            <div class="row">
                                <input type="text" name="newsletter_email" id="newsletter_email" placeholder="Your Email" class="subscribe-email">

                                <!-- <input type="text" placeholder="Your Email" name="search" class="subscribe-email"> -->
                                <button type="submit" id="newsletter_btn" class="btn-subscribe "><i class="fa fa-paper-plane "></i> Submit</button>
                                @if ($errors->has('newsletter_email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('newsletter_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Subscrbe Section Begins -->
    <div class="subscribe" style="background: #F9DDD2">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="footer-images" class="owl-carousel">
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Australia.jpg" class="img-responsive" style="width:auto">
                  </div>
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Bhutan.jpg" class="img-responsive" style="width:auto">
                  </div>

                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Dubai.jpg" class="img-responsive" style="width:auto">
                  </div>

                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Hong Kong.jpg" class="img-responsive" style="width:auto">
                  </div>

                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Malaysia.jpg" class="img-responsive" style="width:auto">
                  </div>
                  
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Maldives.jpg" class="img-responsive" style="width:auto">
                  </div>
                  
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/mauritius.jpg" class="img-responsive" style="width:auto">
                  </div>
                  
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Nepal.jpg" class="img-responsive" style="width:auto">
                  </div>
                  
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/New Zealand.jpg" class="img-responsive" style="width:auto">
                  </div>
                  
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Seychelles.jpg" class="img-responsive" style="width:auto">
                  </div>
                  
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Singapore.jpg" class="img-responsive" style="width:auto">
                  </div>
                  
                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/sri lanka.jpg" class="img-responsive" style="width:auto">
                  </div>

                  <div class="item">
                    <img src="{{env('APP_PUBLIC_URL')}}/new-theme/images/footer-image/Thailand.jpg" class="img-responsive" style="width:auto">
                  </div>
                 
                  <!--  Example item end -->
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Footer Top Begins -->
    <div class="footer-top">

        <div class="container">
<!--            <div class="footer-logo">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/footer-logo.png" class="img-responsive"/>
                    </div>
                </div>
            </div>-->
            <div class="footer-top-blocks">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                        <div class="footer-top-block">


                            <h3 class="footer-top-title">About Us</h3>



                            <p class="footer-top-desc">The vision is to help you plan your holidays, providing you world class experience at low cost and creating memories for life time. Giving the best of luxury at the lowest price and making you feel important.</p>
<!--                            <div class="footer-social">
                                <ul> 

                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                </ul>
                            </div>-->

                        </div>

                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">


                        <div class="footer-top-block">


                            <h3 class="footer-top-title">Quick Link</h3>

                            <div class="footer-top-links">
                                <ul>
                                    <li><a href="{{route('indian-holiday')}}">Indian Holidays</a></li>
                                    <li><a href="{{route('global-holiday')}}">Global Holidays</a></li>
                                    <li><a href='{{env("APP_URL")}}home/customise-book'>Customise & Book</a></li>
                                    <li><a href="{{ route('payment') }}">Online Payment</a></li>
                                </ul>




                            </div>


                        </div>


                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">

                        <div class="footer-top-block">


                            <!--<h3 class="footer-top-title">Discover</h3>-->
                            <h3 class="footer-top-title"> &nbsp; </h3>

                            <div class="footer-top-links">
                                <ul>
                                    <li><a href="{{ route('about') }}">About Us</a></li>
                                    <li><a href="{{ route('contact') }}">Contact</a></li>
                                    <li><a href="{{ route('faq') }}">Faq</a></li>
                                    <li><a href="{{ route('terms-conditions') }}">T &amp; C</a></li>


                                </ul>




                            </div>


                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                        <div class="footer-top-block">


                            <h3 class="footer-top-title">Contact Us</h3>

                            <div class="footer-address-block">


                                <div class="row">
                                    <div class="col-xs-4 footer-address">
                                        <i class="fa fa-map-marker"></i> Address
                                    </div>
                                    <div class="col-xs-8">
                                        <p>1205, 12th Floor, Sai Indu Towers, L.B.S. Road, Bhandup (W), Mumbai - 400078.  </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 footer-address">
                                        <i class="fa fa-envelope"></i> Email
                                    </div>
                                    <div class="col-xs-8">
                                        <p>info@lowcosttrip.com </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 footer-address">
                                        <i class="fa fa-phone"></i> Phone
                                    </div>
                                    <div class="col-xs-8">
                                        <p>81 82 83 84 85</p>
                                    </div>
                                </div>


                            </div>


                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="footer-copyright">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <p>&copy; All Rights Reserved</p>
                            </div>
                            <?php /*
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                                <ul class="footer-payment">
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/comodo.jpg"/></a></li>
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/visa.jpg"/></a></li>
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/master-card.jpg"/></a></li>
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/ae.jpg"/></a></li>
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/paypal.jpg"/></a></li>
                                </ul>
                            </div>
                            */ ?>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Top Ends -->


    <!-- Footer Bottom Begins -->
    <?php /*
    <div class="footer-bottom">

        <div class="container">


            <div class="row">

                <div class="footer-nav">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="footer-bottom-block">
                            <h3 class="footer-bottom-title">Hot Indian Holiday Destinations</h3>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <ul>
                                    @if(isset($domestic_sectors) && !empty($domestic_sectors))
                                        @foreach($domestic_sectors as $domesticSector)
                                            @php
                                                $sname = preg_replace('/\W+/', '-', strtolower($domesticSector->name));
                                            @endphp
                                            <li class="col-xs-12 col-sm-5 col-md-5 col-lg-5"><a href="{{route('indian-holiday.sector',[$domesticSector->id,$sname])}}">{{$domesticSector->name}}</a></li>
                                        @endforeach
                                    @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <div class="footer-bottom-block">
                            <h3 class="footer-bottom-title">Pilgrimage Tours</h3>
                            <ul>
                            @if(isset($pilgrimage_sectors) && !empty($pilgrimage_sectors))
                                @foreach($pilgrimage_sectors as $pilgrimageSector)
                                    @php
                                        $pname = preg_replace('/\W+/', '-', strtolower($pilgrimageSector->name));
                                    @endphp
                                <li><a href="{{route('indian-holiday.sector',[$pilgrimageSector->id,$pname])}}">{{$pilgrimageSector->name}}</a></li>
                                @endforeach
                            @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="footer-bottom-block">
                            <h3 class="footer-bottom-title">Global Holidays</h3>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <ul>
                                        @if(isset($global_sectors) && !empty($global_sectors))
                                            @foreach($global_sectors as $globalSector)
                                                @php $gname = preg_replace('/\W+/', '-', strtolower($globalSector->name)); @endphp
                                                <li class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><a href="{{route('global-holiday.sector',[$globalSector->id,$gname])}}">{{$globalSector->name}}</a></li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer Copyright Begins -->
                <div class="footer-copyright">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <p>&copy; All Rights Reserved</p>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                                <ul class="footer-payment">
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/comodo.jpg"/></a></li>
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/visa.jpg"/></a></li>
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/master-card.jpg"/></a></li>
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/ae.jpg"/></a></li>
                                    <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/paypal.jpg"/></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Footer Copyright Ends -->
            </div>
        </div>
    </div>
    
    */ ?>
    <!-- Footer Bottom Ends -->
</footer>
<!-- Footer Ends -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
    
    // Pure Javascript
document.getElementsByClassName("activity-details").onmouseover = function(){
    document.getElementsByClassName("activity-overlay").style.display = 'block';
}
document.getElementsByClassName("activity-details").onmouseout = function(){
    document.getElementsByClassName("activity-overlay").style.display= 'none';
}

    
    </script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ env('APP_PUBLIC_URL')}}new-theme/js/bootstrap.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}new-theme/assets/owl/js/owl.carousel.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}client/js/custom.js"></script>

            <!-- datatables -->
<script src="{{ env('APP_PUBLIC_URL')}}client/data-table/js/jquery.dataTables.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}client/data-table/js/dataTables.tableTools.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}client/data-table/js/bootstrap-dataTable.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#newslettersform").validate({
            rules: {
                newsletter_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                newsletter_email: {
                    required: "Please enter an email"
                }
            },
            errorPlacement: function(error, element) {
              if (element.attr("name") == "newsletter_email") {
                 error.insertAfter("#newsletter_btn");
              } else {
                 error.insertAfter(element);
              }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('.hd-carousel').owlCarousel({
            loop:true,
            margin:10,
            dots: true,
            responsiveClass:true,
            
            <!-- navText : ['<i class=" fa fa-caret-left" aria-hidden="true"></i>','<i class="  fa fa-caret-right" aria-hidden="true"></i>'], -->
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:false
                },
                1000:{
                    items:3,
                    nav:false,
                    loop:false,
                    margin:20
                    
                }
            }
        }); 

        $('.owl-slider').owlCarousel({
            margin: 30,
            loop: true,
            autowidth:true,
            autoplay:true,
            autoplayTimeout:10000,
            nav:true,
            autoplayHoverPause:true,
            items: 1,
            dots: false,
            navText : ['<i class=" fa fa-angle-left fa-2x" aria-hidden="true"></i>','<i class="    fa fa-angle-right fa-2x" aria-hidden="true"></i>'],
        });

        $("#detailslider").owlCarousel({
            loop:true,
            navigation : true,
            scrollPerPage : true,
            nav:true,
            autoplay:true,
            loop:true,
            dots:false,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });


        $("#footer-images").owlCarousel({
            items : 6,
             // Navigation
            navigation : true,
            navigationText : ["prev","next"],
            rewindNav : true,
            scrollPerPage : true,
              responsive: {
              0: {
                items: 2
              },
              768: {
                items: 2
              },
              1000: {
                items: 6
              }
            }
        });

    }); 
          
    $(function() {
        $('#ChangeToggle').click(function() {
            $('#navbar-hamburger').toggleClass('hidden');
            $('#navbar-close').toggleClass('hidden');  
        });
        $(".datepicker").datepicker({dateFormat: 'dd MM yy'});
        $("#birth_date").datepicker({dateFormat: 'dd MM yy',changeMonth: true,
        changeYear: true,yearRange: "-100:+0",});
    });

    $('.rediret-link').click(function() {
        var url = $(this).attr('href');
        window.location.href = url;
        window.location.reload(true);
        $("body").scrollTop(0);
    });
          
</script>
<script>

    $(".img-plus" ).click(function() {
        if (!$(this).hasClass('comadd') ) {
            var src = $(this).attr('src').replace("plus.png","substract.png");
            $(this).attr('src', src);
            //$(this).attr('src', 'http://localhost/lowcosttrip/public/new-theme/images/substract.png');
            $(this).addClass('comadd');
        } else  {
            var src = $(this).attr('src').replace("substract.png","plus.png");
            $(this).attr('src', src);
            $(this).removeClass('comadd');
        }
    });
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ad1ea204b401e45400e9fc3/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


<!-- <script>
		$('#navigation').slimmenu(
	{
		resizeWidth: '800',
		collapserTitle: 'Main Menu',
		animSpeed: 'medium',
		easingEffect: null,
		indentChildren: false,
		childrenIndenter: '&nbsp;'
	});
	</script> -->
	<!-- <script>
		$('#navigationnew').slimmenu(
	{
		resizeWidth: '800',
		collapserTitle: 'Main Menu',
		animSpeed: 'medium',
		easingEffect: null,
		indentChildren: false,
		childrenIndenter: '&nbsp;'
	});
	</script> -->