@extends('layouts.client')
<script>
    var cityData = <?= $cities ?>;
</script>
@section('content')
 
<section>
     <div class="dashboard-tabs">
        <div class="dashboard-tabs-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <h2 class="dashboard-title">Flight Bookings</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="dashboard-tabs-content">
            <div class="container"> 

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if(session()->has('message'))
                            <div class="alert {{ session()->get('alert-class') }}" style="margin-top: 30px;">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        @if(session()->has('log_error'))
                            <div class="alert alert-danger" style="margin-top: 30px;">
                                {{ session()->get('log_error') }}
                            </div>
                        @endif
                        <div class="dashboard-list-item " style="border:none"> 
                            <form name="searchForm" id="searchForm" action="" method="post" onsubmit="return false;">
                                    {{csrf_field()}}
                                    <div class="form-body">
                                        <?php //dd(isset($wallet_user_data) && $wallet_user_data=='');?>
                                        @if(isset($wallet_user_data) && $wallet_user_data!='')
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-bottom: 45px;">
                                            <div class="inner-breadcrumbs" style="background: #F9DDD2;">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="inner-breadcrumbs-block">
                                                                <div class="inner-breadcrumbs-page-title">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                            <div style="padding: 23px 0px;">
                                                                            <input type="checkbox" id="is_wallet" name="is_wallet" checked>
                                                                            <input type="hidden" id="wallet_amount" name="wallet_amount" value="{{(isset($wallet_user_data->current_balance) ? $wallet_user_data->current_balance :0)}}">
                                                                            <span>Wallet Balance</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                            <div style="padding: 23px 70px; text-align: right;">
                                                                            <span>&#8377 {{(isset($wallet_user_data->current_balance) ? $wallet_user_data->current_balance :0)}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group col-md-3">                                
                                                <label>From</label>
                                                <select class="form-control" id="select_form_city" name="from" required>
                                                    <option value="" disabled selected>Choose</option>
                                                    <?php foreach ($city as $key => $city_info) { 
                                                        if($city_info->city_code != '' && $city_info->city_code != NULL){ ?>
                                                            <option value="{{$city_info->city_code}}">{{$city_info->city_name}}</option>
                                                    <?php }
                                                    
                                                    } ?>
                                                </select>
                                            </div>                                
                                            <div class="form-group col-md-3">                                
                                                <label>To</label>
                                                <select class="form-control"  id="select_to_city"  name="to" required>
                                                    <option value="" disabled selected>Choose</option>
                                                     <?php foreach ($city as $key => $city_info) { 
                                                        if($city_info->city_code != '' && $city_info->city_code != NULL){ ?>
                                                            <option value="{{$city_info->city_code}}">{{$city_info->city_name}}</option>
                                                    <?php }
                                                    
                                                    } ?>
                                                   
                                                </select>
                                            </div>                                                            
                                            <div class="form-group col-md-3">                                
                                                <label>Depart</label>
                                                <input type="text" class="form-control" id="departdate" name="depart_date" value="<?= date('d M, Y') ?>" placeholder="eg: <?= date('d M, Y') ?>" readonly required>
                                            </div>    
                                            <div class="form-group col-md-3">                                
                                                <label>Passengers|Class</label>
                                                <select class="form-control" id="select_class_type" name="class" required>
                                                    <?php foreach (get_air_class() as $k => $v) { ?>
                                                        <option value="{{$k}}">{{ $v }}</option>
                                                    <?php } ?>                               
                                                </select>
                                            </div>                                                            
                                        </div>    
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                             <div class="form-group col-md-3">                                       
                                                <label>Adults</label>
                                                <input type="text" class="form-control" id="adults" name="adults" value="1" required  style="width: auto">
                                                
                                            </div>    
                                             <div class="form-group col-md-3">       
                                                <label>Child</label>
                                                <input type="text" class="form-control" id="child" name="child" value="0" required  style="width: auto">
                                                
                                            </div>      
                                             <div class="form-group col-md-3">       
                                                <label>Infants</label>
                                                <input type="text" class="form-control" id="infants" name="infants" value="0" required  style="width: auto">
                                            </div>                                                                                        
                                            <div class="form-group col-md-3">   
                                                <label>&nbsp;</label>
                                                <button type="submit" id="submit-button" class="form-control btn btn-danger">Search</button>
                                            </div>  
                                        </div>  
                                    </div>
                            </form>   
                        </div>
                    </div> 
                </div>
                <div class="container">
                    <div class="search-result"> 
                        
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script src="https://www.solodev.com/assets/pagination/jquery.twbsPagination.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $( "#departdate" ).datepicker({ dateFormat: 'd M, yy', minDate: 0   });
        $(document).on("submit", "#searchForm", function (event) {
            $th = $(this);
            var submit_text = $('#submit-button').text();
            var data = $('#searchForm').serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('submit.flight.book.form') }}",
                data: data,
                dataType: 'json',
                // async: false, 
                beforeSend: function (xhr) {
                    // $('#submit-button').attr('disabled', true).text('Please wait...');
                    $('#submit-button').attr('disabled', true).html('<i class="fa fa-refresh fa-spin"></i>');
                    $('.search-result').html('');
                },
                success: function (data, textStatus, jqXHR) {
                    $('#submit-button').removeAttr('disabled').text(submit_text);
                    if(data.error){
                        alert(data.message);
                    }else{                     
                        $('.search-result').html(data.html_data);
                        // setTimeout(function () {
                        $('#pagination-demo').twbsPagination({
                          totalPages: Math.ceil(($('#count-records').val() / 10)),
                          startPage: 1,
                          visiblePages: 5,
                          initiateStartPageClick: true,
                          href: false,
                          hrefVariable: 1,
                          first: 'First',
                          prev: 'Previous',
                          next: 'Next',
                          last: 'Last',
                          loop: false,
                          onPageClick: function (event, page) {
                            $('.page-active').removeClass('page-active');
                            $('#page'+page).addClass('page-active');
                          },
                          paginationClass: 'pagination',
                          nextClass: 'next',
                          prevClass: 'prev',
                          lastClass: 'last',
                          firstClass: 'first',
                          pageClass: 'page',
                          activeClass: 'active',
                          disabledClass: 'disabled'
                        });
                        // }, 1000);
                    }
                }
            });
        });

        $(document).on("click", ".book-now-button", function (event) {
            $th = $(this);
            var button_txt =  $th.text();
            var fromdata = $("#SearchFormData").val();          
            $.ajax({
                type: 'POST',
                url: "{{ route('flight.book.now') }}",
                data: {_token :"{{ csrf_token() }}" ,fromdata:fromdata,id: $(this).attr('data-id') },
                dataType: 'json',
                // async: false, 
                beforeSend: function (xhr) {
                    $th.attr('disabled', true).html('<i class="fa fa-refresh fa-spin"></i>');
                },
                success: function (data, textStatus, jqXHR) {
                    $th.removeAttr('disabled').text(button_txt);
                    if(data.error){
                        alert(data.message);
                    }else{                     
                        $('.search-result').html(data.html_data);
                    }

                    $( ".datepicker" ).datepicker({ dateFormat: 'd M, yy' });
                }
            });
        });



        $(document).on("click", ".more-details", function (event) {
            // $th.parents(".flight-row").find('.flightdetails').css('display','block');
            $th = $(this);
            if($th.parents(".flight-row").find('.flightdetails.open').length>0){
                $th.parents(".flight-row").find('.flightdetails').slideToggle();
            }else{ 
                var button_txt =  $th.text();
                var fromdata = $("#SearchFormData").val();          
                $.ajax({
                    type: 'POST',
                    url: "{{ route('flight.book.get.details') }}",
                    data: {_token :"{{ csrf_token() }}" ,fromdata:fromdata,id: $(this).attr('data-id') },
                    dataType: 'json',
                    // async: false, 
                    beforeSend: function (xhr) {
                        $th.attr('disabled', true).html('<i class="fa fa-refresh fa-spin"></i>');
                    },
                    success: function (data, textStatus, jqXHR) {
                        $th.removeAttr('disabled').text(button_txt);
                        if(data.error){
                            alert(data.message);
                        }else{                            
                            $th.parents(".flight-row").find('.flightdetails').html(data.html_data);
                            $th.parents(".flight-row").find('.flightdetails').slideDown();
                            $th.parents(".flight-row").find('.flightdetails').addClass('open');
                        }      
                    }
                });
            }
        });


        $(document).on("submit", "#bookingForm", function (event) {
            $th = $(this);
            var button_txt =  $th.text();
            var data = $('#bookingForm').serialize()+'&'+$.param({ 'flight_from': $('#select_form_city').val() })+'&'+$.param({ 'flight_to': $('#select_to_city').val() })+'&'+$.param({ 'depart_date': $('#departdate').val() })+'&'+$.param({ 'passengers_class': $('#select_class_type').val() })+'&'+$.param({ 'adults': $('#adults').val() })+'&'+$.param({ 'child': $('#child').val() })+'&'+$.param({ 'infants': $('#infants').val() });
            $.ajax({
                type: 'POST',
                url: "{{ route('submit.flight.order.now') }}",
                data: data,
                dataType: 'json',
                // async: false, 
                beforeSend: function (xhr) {
                    $th.attr('disabled', true).html('<i class="fa fa-refresh fa-spin"></i>');
                },
                success: function (data, textStatus, jqXHR) {
                    $th.removeAttr('disabled').text(button_txt);
                    if(data.error){
                        alert(data.message);
                    }else{  
                        window.location.href = data.url;                      
                        $('.search-result').html("");
                    }
                }
            });
        });



        $(document).on('change', '.meals-price', function(){   
            $meals_price = 0;
            $( ".meals-price" ).each(function() {
               if ($(this).is(":checked")) {
                    $meals_price = ($meals_price) +( parseFloat($(this).attr('data-price')));
               }
            });
            $total_price = parseFloat($('#total_price').val());
            $('#grand_total_price').val($total_price + $meals_price);
            $('.grand-total').text(($total_price + $meals_price).toFixed(2));
        });

        $(document).on('change', '.seats-price', function(){  
            $seats_price = 0;
            $( ".seats-price" ).each(function() {
               if ($(this).is(":checked")) {
                    $seats_price = ($seats_price) + (parseFloat($(this).attr('data-price')));
               }
            });
            $total_price = parseFloat($('#total_price').val());
           $('#grand_total_price').val($total_price + $seats_price);
           $('.grand-total').text(($total_price + $seats_price).toFixed(2));
        });

        $(document).on('change', '.buggage-price', function(){  

            $buggage_price = 0;
            $( ".buggage-price" ).each(function() {
               if ($(this).is(":checked")) {
                    $buggage_price = ($buggage_price) + (parseFloat($(this).attr('data-price')));
               }
            });
            $total_price = parseFloat($('#total_price').val());
            $('#grand_total_price').val($total_price + $buggage_price);
            $('.grand-total').text(($total_price + $buggage_price).toFixed(2));

        });





        $(document).on('click', '.closemoredetails', function(){                                   
            $(this).parents(".flightdetails").slideUp();
        });



    });
</script>
@endsection