@extends('layouts.client')

@section('content')
<script>
    var cityData = <?= $cities ?>;
</script>
<section>
     <style type="text/css">
         .last_border{
             border-bottom: 1px solid #cccccc !important;
         }
     </style>
    <!-- Page Breadcrumbs Begins -->
    <div class="inner-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="inner-breadcrumbs-block">
                        <div class="inner-breadcrumbs-nav">
                            <ul>
                                @if(isset($breadcrumbs) && !empty($breadcrumbs))
                                @foreach($breadcrumbs as $menuName=>$menuLink)
                                <li><a href="{{$menuLink}}"> {{$menuName}}</a></li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="inner-breadcrumbs-page-title">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <h2>{{$title}}</h2>
                                </div>
                                
                                <?php /*
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <p>{{$description}}</p>
                                </div>
                                */ ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="comparebtn"></div>
    <!-- Page Breadcrumbs Ends -->
    <!-- HOliday Listing Block Begins -->
    <div class="holiday-listing">
        <div class="container"> 
            <div id="Comparemsg" class="text-center" style="display: none;"></div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?php /*
                    <div class="row" style="display: none;">
                        <div class="holiday-list-search">
                            <form action="" method="post">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <div class="input-group">
                                        <select class="select-style" name="tour_budget" id="tour_budget" onchange="tour_filter();">
                                            <option value="">Budget</option>
                                            @foreach($tourBudget as $budgetId => $budgetName)
                                            <option value="{{$budgetId}}" {{ (isset($_GET['budget']) && $_GET['budget'] == $budgetId) ? 'selected' : '' }}>{{$budgetName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <div class="input-group">
                                        <select class="select-style" name="tour_duration" id="tour_duration" onchange="tour_filter();">
                                            <option value="">Duration</option>
                                            @foreach($tourDuration as $durationId => $durationName)
                                            <option value="{{$durationId}}" {{ (isset($_GET['duration']) && $_GET['duration'] == $durationId) ? 'selected' : '' }}>{{$durationName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <div class="input-group">
                                        <select class="select-style" name="tour_hotel" id="tour_hotel" onchange="tour_filter();">
                                            <option value="">Hotel Choice</option>
                                            @foreach($hotelChoice as $choiceId => $choiceName)
                                            <option value="{{$choiceId}}" {{ (isset($_GET['hotel']) && $_GET['hotel'] == $choiceId) ? 'selected' : '' }}>{{$choiceName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <div class="input-group">
                                        <select class="select-style" name="theme_type" id="theme_type" onchange="tour_filter();">
                                            <option value="">Themes</option>
                                            @foreach($SpecialtyTours as $theme)
                                            <option value="{{$theme->typeId}}" {{ (isset($_GET['theme_type']) && $_GET['theme_type'] == $theme->typeId) ? 'selected' : '' }}>{{$theme->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <div class="input-group">
                                        <select class="select-style-gray" name="short_by" id="short_by" onchange="tour_filter();">
                                            <option value="">Short By</option>
                                            @foreach($toursSortBy as $sortId => $sortName)
                                            <option value="{{$sortId}}" {{ (isset($_GET['short_by']) && $_GET['short_by'] == $sortId) ? 'selected' : '' }}>{{$sortName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    */ ?>
                    <?php if (isset($holidays) && count($holidays) > 0) { ?>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="infinite-scroll">
                                    @foreach($holidays as $key => $holiday)
                                    <div class="holiday-list-item">
                                        <div class="row no-margin">
                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-pad">
                                                @if(isset($holiday->image) && !empty($holiday->image))
                                                <div class="holiday-place-image">
                                                    <a href="{{route('holiday-detail',[$holiday->id,str_slug($holiday->tour_name,'-')])}}">
                                                        <img src="{{ env('AWS_BUCKET')}}{{$holiday->image}}" class="img-responsive"/>
                                                    </a>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 no-pad">
                                                <div class="holiday-tour-deatils">
                                                    <div class="holiday-name tour-title">
                                                        @if(isset($holiday->tour_name) && $holiday->tour_name!='')
                                                        <a href="{{route('holiday-detail',[$holiday->id,str_slug($holiday->tour_name,'-')])}}">
                                                            <h3>{{$holiday->tour_name}}</h3>
                                                        </a>
                                                        @endif
                                                        <div class="holiday-duration">
                                                            @if(isset($holiday->tour_nights) && $holiday->tour_nights!='')
                                                            <h4>{{$holiday->tour_nights}} Nights {{$holiday->tour_nights + 1}} Days <span> - </span> <span class="holiday-tour-rating"> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></h4>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <?php if(isset($holiday['cost']['amount']) && count($holiday['cost']['amount']) > 0){?>
                                                    <div class="holiday-packages">
                                                        @foreach($holiday['cost']['amount'] as $c => $cost)
                                                        @foreach($cost['rates'] as $r => $rate)
                                                        @if(isset($rate['sell']) && $rate['sell'] > 0)
                                                        <label class="radio-inline"><input type="radio" class="holydayHotelRadio" holyday="{{$holiday->id}}" package="{{$c}}" name="holydaycost{{$holiday->id}}" data-package_type="{{$c}}" {{($c==1)?"checked":""}}>{{$cost['name']}} <?php /* ({{($rate['sell'])-($rate['sell']*$holiday['cost']['dates']['discount'])/100}}) */ ?></label>
                                                        @endif
                                                        @break;
                                                        @endforeach
                                                        @endforeach
                                                    </div>
                                                    <?php } ?>
                                                    
                                                    <?php if (isset($holiday['hotel']) && count($holiday['hotel']) > 0) { ?>
                                                    <div class="holiday-packages-details">
                                                        <table class="table table-responsive hotel_{{$holiday->id}} last_border">
                                                            <tr class="hotel_label" >
                                                                <td><h4><strong>Location</strong></h4></td>
                                                                <td><h4><strong>Hotel</strong></h4></td>
                                                                <td><h4><strong>Nights</strong></h4></td>
                                                                <td><h4><strong>Rating</strong></h4></td>
                                                            </tr>
                                                            @foreach($holiday['hotel'] as $h => $hotel)
                                                            <tr class="{{($hotel->package_type!=1)?'hide':''}} package_{{$holiday->id}}{{$hotel->package_type}}">
                                                                <td><h4>{{$hotel->location}}</h4></td>
                                                                <td><h4>{{$hotel->hotel}}</h4></td>
                                                                <td><h4>{{$hotel->night}}N</h4></td>
                                                                <td><h4>{{$hotel->hotel_star}}<i class="fa fa-star"></i></h4></td>
                                                            </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="holiday-package-facilities">
                                                        <ul>
                                                            <?php
                                                                $features = array();
                                                                if (isset($holiday->feature_data) && $holiday->feature_data != '') {
                                                                    $features = explode(',', $holiday->feature_data);
                                                                }
                                                            ?>
                                                                @if(count($features)>0)
                                                                @foreach($features as $f_key => $feature)
                                                                <li><a href="#"><i class="fa fa-check"></i>{{$feature}}</a></li>
                                                                @endforeach
                                                                @endif
                                                        </ul>
                                                    </div>
                                                    <?php /* 
                                                    <div><i>Social share:</i></div>
                                                    <div class="social">
                                                        <a class="btn-floating btn-lg btn-fb social-share" href="https://www.facebook.com/sharer.php?u={{ urlencode(route('holiday-detail',[$holiday->id,str_slug($holiday->tour_name,'-')])) }}" type="button" role="button"><i class="fa fa-facebook"></i></a>

                                                        <a class="btn-floating btn-lg btn-tw social-share" href="https://twitter.com/intent/tweet?url={{ urlencode(route('holiday-detail',[$holiday->id,str_slug($holiday->tour_name,'-')])) }}" type="button" role="button"><i class="fa fa-twitter"></i></a>

                                                        <a class="btn-floating btn-lg btn-gplus social-share" href="https://plus.google.com/share?url={{ urlencode(route('holiday-detail',[$holiday->id,str_slug($holiday->tour_name,'-')])) }}" type="button" role="button"><i class="fa fa-google-plus"></i></a>

                                                        <a class="btn-floating btn-lg btn-li social-share" href="https://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(route('holiday-detail',[$holiday->id,str_slug($holiday->tour_name,'-')])) }}" type="button" role="button"><i class="fa fa-linkedin btn-primary" aria-hidden="true"></i></a>
                                                        
                                                        <a class="btn-floating btn-lg btn-pin social-share" href="https://pinterest.com/pin/create/button/?url={{ urlencode(route('holiday-detail',[$holiday->id,str_slug($holiday->tour_name,'-')])) }}" type="button" role="button"><i class="fa fa-pinterest btn-danger" aria-hidden="true"></i></a>
                                                    </div>
                                                    */?>
                                                    <!--Google +-->
                                                    <div class="holiday-managed-by">
                                                        <p>Holiday Package Supplied and Managed by Trip Cafe - Division of Low Cost Trip</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if(isset($holiday['cost']['amount']) && count($holiday['cost']['amount']) > 0){?>
                                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                <div class="holiday-tour-fare cost-{{$holiday->id}}">
                                                    <?php /*
                                                    @foreach($holiday['cost']['amount'] as $c => $cost)
                                                    @foreach($cost['rates'] as $r => $rate)
                                                    <div class="amount package_cost_{{$holiday->id}}{{$c}} {{($c!=1)?'hide':''}}">
                                                        @if(isset($rate['sell']) && $rate['sell'] > 0)

                                                        @if($holiday['cost']['dates']['discount']>0)
                                                        <div class="actual-fare">
                                                            <h4><img style="width: 15px" src="{{  isset($holiday['cost']['dates']['currency_icon']) ?  env('APP_PUBLIC_URL').'uploads/currency_icons/'.$holiday['cost']['dates']['currency_icon'] : '' }}">{{$rate['sell']}}</h4>
                                                        </div>
                                                        @endif
                                                        <div class="discount-fare">
                                                            <h2><img style="width: 30px"  src="{{  isset($holiday['cost']['dates']['currency_icon']) ?  env('APP_PUBLIC_URL').'uploads/currency_icons/'.$holiday['cost']['dates']['currency_icon'] : '' }}">{{($rate['sell'])-($rate['sell']*$holiday['cost']['dates']['discount'])/100}}</h2>
                                                        </div>
                                                        <div class="toal-persons">
                                                            <h4>{{$rate['name']['description']}}</h4>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    @break;
                                                    @endforeach
                                                    @endforeach
                                                    */ ?>
                                                    <div class="inquire">
                                                        <a href="{{route('inquiry-now',[$holiday->id,str_slug($holiday->tour_name,'-'),1])}}" class=" btn-holiday">Inquire Now</a>
                                                        <?php /*
                                                          <button type="submit" class=" btn-holiday ">Book Now</button>
                                                         */ ?>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="{{route('holiday-detail',[$holiday->id,str_slug($holiday->tour_name,'-')])}}" class=" btn-holiday">View Details</a>
                                                        <!-- <button type="submit" class=" btn-holiday ">View Details</button> -->
                                                    </div>
                                                    <div class="add-compare">
                                                        <a href="javascript:void(0);" data-id="{{$holiday->id}}" onclick="add_tour(this);">Add to Compare</a>
                                                        <?php /*
                                                          <a href="{{route('tour-compare')}}">Add to Compare</a>
                                                          <a href="#">Add to Compare</a> */ ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row" style="margin-top: -25px;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4  style="margin-bottom: 0;">We do not have any fixed departure in this sector right now, Please filled up below form for exiting affordable customize tours.</h4>
                                <br>
                                <br>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h3 style="color: #7f7f7f;margin: 0px;font-weight: 600;font-size: 16px;line-height: 16px;text-transform: uppercase;">Holiday</h3>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="nav nav-pills inquiry_tab">
                                    <li class="active holiday"><a data-toggle="pill" href="#holiday">Holiday</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="holiday" class="tab-pane fade active in">
                                        <div class="booking-block border-1">
                                            <div class="holiday-booking-flight booking-block-form">
                                                <form name="holiday_frm" id="holiday_frm" action="" method="post" onsubmit="return false;">
                                                {{csrf_field()}}
                                                    <div class="row booking-form-padding" style="display: none;">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                                            <ul class="validate-msg">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Departure City</label>
                                                                <input type="text" name="departure_city" class="form-control autocomplete" placeholder="Departure City">
                                                                <input type="hidden" name="departure_city_id">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Interested Date</label>
                                                                <input type="text" name="check_in_date" class="form-control datepicker" readonly="" placeholder="Interested Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Choose travel type</label>
                                                                <select name="travel_type" class="select-style">
                                                                    <option value="" selected>Choose travel type</option>
                                                                    @foreach(get_travel_type() as $key => $travel_type)
                                                                    <option value="{{ $key }}">{{ $travel_type}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Destination Details</label>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <td width="50%">Destination</td>
                                                                        <td width="30%">Number of Nights</td>
                                                                        <td width="20%">Action</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr class="add_btn_tr">
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-default btn-number destination_add_btn" data-type="plus" data-field="quant[1]">
                                                                                <span class="glyphicon glyphicon-plus"></span>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Activity Details</label>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <table class="table table-bordered activiti-table">
                                                                <thead class="append_new_row">
                                                                    <tr id="activity-header">
                                                                        <td width="30%">Country</td>
                                                                        <td width="50%">Activity</td>
                                                                        <td width="20%">Action</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody >
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-default btn-number add_tour_activites" data-type="plus" data-field="quant[1]">
                                                                                <span class="glyphicon glyphicon-plus"></span>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Number of Guests</label>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Adults</label>
                                                                <input type="text" name="adults" value="" class="form-control" placeholder="Adults">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Child</label>
                                                                <input type="text" name="child" class="form-control" placeholder="Child">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Infants</label>
                                                                <input type="text" name="infants" class="form-control" placeholder="Infants">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">Name</label>
                                                                <input type="text" name="contact_name" class="form-control" placeholder="Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                        <div class="input-group">
                                                             <label class="radio-inline nopadding">Contact No</label>
                                                            <input type="text" name="contact_no" class="form-control onlynumber" maxlength="10" placeholder="Contact No">
                                                        </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                        <div class="input-group">
                                                             <label class="radio-inline nopadding">Email Id</label>
                                                            <input type="text" name="email_id" class="form-control" placeholder="Email Id">
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <div class="input-group">
                                                                 <label class="radio-inline nopadding">More Details</label>
                                                                <textarea name="notes" class="form-control" placeholder="More Details"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <button class="btn btn-default btn-payment btn-desktop-right holiday_submit_btn">SUBMIT</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php //dd(get_travel_type());?>
                        </div>
                    <?php } ?>

                    <?php if (isset($holidays) && count($holidays) > 0) { ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                            {{ $holidays->appends(app('request')->except('page'))->links() }}
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>	
        </div>
    </div>
    <!-- Booking Block Ends -->
</section>
@endsection

@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--<script src="{{ env('APP_PUBLIC_URL')}}client/js/jquery.jscroll.min.js"></script>-->
<script type='text/javascript'>
    $(function () {
        window.loadMore = false;
        $(window).scroll(function () {
            if (isInView('.viewMore')) {
                if (window.loadMore) {
                    window.loadMore = false;
                    viewMoreHolyday();
                }
            }
        });
    //         $('ul.pagination').hide();
    //         $('.infinite-scroll').jscroll({
    //            autoTrigger: true,
    //            loadingHtml: '<img class="center-block" src="/images/loading.gif" alt="Loading..." />',
    //            padding: 0,
    //            nextSelector: '.pagination li.active + li a',
    //            contentSelector: 'div.infinite-scroll',
    //            callback: function() {
    //                $('ul.pagination').remove();
    //            }
    //        });

    $('input[type=radio][class=holydayHotelRadio]').change(function () {
            var holyday = $(this).attr('holyday');
            var package = $(this).attr('package');

            $('div.cost-' + holyday).find('div.amount').addClass('hide');
            $('div.cost-' + holyday).find('div.package_cost_' + holyday + package).removeClass('hide');

            $('table.hotel_' + holyday + ' tr:gt(0)').addClass('hide');
            $('table.hotel_' + holyday + ' tr.package_' + holyday + package).removeClass('hide');

            var tr_length = $('table.hotel_' + holyday + ' tr.package_' + holyday + package).length;
            if(tr_length==0){
                $('table.hotel_' + holyday + ' tr.hotel_label').addClass('hide');
            }else{
                $('table.hotel_' + holyday + ' tr.hotel_label').removeClass('hide');
            }

            var inquire = $(this).parents('div.holiday-list-item').find('div.inquire a');
            if(inquire.length){
                var oldLink = inquire.attr('href');
                oldLink = oldLink.substr(0, oldLink.lastIndexOf('/'));
                inquire.attr('href', oldLink + "/" + package);
            }
        });
        $('.holiday-list-item').each(function (i, obj) {
            if (!$(obj).find('input[type=radio][class=holydayHotelRadio]:first').prop('checked')) {
                $(obj).find('input[type=radio][class=holydayHotelRadio]:first').prop("checked", true).trigger("change");
            }
        });
    });

    function tour_filter() {
        var budget = $("#tour_budget").val();
        var duration = $("#tour_duration").val();
        var hotel = $("#tour_hotel").val();
        var theme_type = $("#theme_type").val();
        var short_by = $("#short_by").val();
        var url = "{{ Request::url() }}";
        window.location.href = url + '?budget=' + budget + '&duration=' + duration + '&hotel=' + hotel + '&theme_type=' + theme_type + '&short_by=' + short_by;
    }
</script>
<script type="text/javascript">
    var arrayFromPHP = <?php echo json_encode(session()->get('compare_tours')); ?>;
    if (arrayFromPHP.length > 0) {
        $('#comparebtn').html('<a class="comparebtn btn-holiday" href="{{ route("tour-compare") }}">Compare<span>(' + arrayFromPHP.length + ')</span</a>');
    } else {
        $('#comparebtn').hide();
    }
    function add_tour(ele) {
        var tour_id = $(ele).data('id');
        var package_id = $('input[name=holydaycost' + tour_id + ']:checked').data('package_type');
        $.ajax({
            url: '{{ route("add-to-compare") }}',
            type: 'POST',
            data: {tour_id: tour_id, package_id: package_id},
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
            success: function (data) {
                if (data.status == true) {
                    if (data.count > 0) {
                        $('#comparebtn').html('<a class="comparebtn btn-holiday" href="{{ route("tour-compare") }}">Compare<span>(' + data.count + ')</span</a>');
                    } else {
                        $('#comparebtn').hide();
                    }
                } else {
                    $('#Comparemsg').html(data.message).removeClass('alert alert-success').addClass("alert alert-danger").show();
                    setTimeout(function () {
                        $('#Comparemsg').hide();
                    }, 5000);
                }
            }
        });
    }
</script>
<script>
    $(document).ready(function () {
        $(document).on("click", ".add_tour_activites", function (event) {
            $.ajax({
                type: 'GET',
                url: "{{ route('get-activiti-html') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    $('.append_new_row').append( data.html );
                }
            });
        });

        $(document).on("change", ".country_id", function (event) {
            $th = $(this);
            $.ajax({
                type: 'POST',
                url: "{{ route('get-activiti-by-city') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id" :  $th.val(),
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Please wait..</option>');
                },
                success: function (data, textStatus, jqXHR) {
                    $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Choose Activity</option>');

                    $.map(data.activities, function (item) {
                        $th.closest('tr').find('td:eq(1) .tour_activity').append('<option value="' + item.id + '">' + item.title + '</option>');
                    });
                }
            });
        });
    }); 
</script>
@endsection
