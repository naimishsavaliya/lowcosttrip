<div class="item">
    <div class="holiday-hotel-carousel-item">
        <h3>{{$tour_hotel['night']}}N</h3>
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <div class="thumbnail">
                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/default-placeholder.png" width="210px" height="139px" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-7 col-lg-7 no-desktop-pad">
                <div class="hh-details">
                    <div class="hh-title">
                        <h4>{{$tour_hotel['hotel']}}</h4>
                    </div>
                    <div class="hh-rating">
                        <span class="holiday-tour-rating">
                        @if($tour_hotel['hotel_star']=='1')
                        <i class="fa fa-star"></i>
                        @elseif($tour_hotel['hotel_star']=='2')
                        <i class="fa fa-star"></i><i class="fa fa-star"></i>
                        @elseif($tour_hotel['hotel_star']=='3')
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        @elseif($tour_hotel['hotel_star']=='4')
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        @elseif($tour_hotel['hotel_star']=='5')
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        @else
                        'N/A';
                        @endif 
                        </span>
                    </div>
                    <div class="hh-location">
                        <h4>{{$tour_hotel['location']}}</h4>
                    </div>
                    <div class="hh-room">
                        <h4>Room Type: Deluxe Room</h4>
                    </div>
                    <?php /*
                    <div class="hh-package-facilities">
                        <ul>
                            <li><a href="#"><i class="fa fa-check"></i>Breakfast</a></li>
                            <li><a href="#"><i class="fa fa-check"></i>Lunch</a></li>
                            <li><a href="#"><i class="fa fa-check"></i>Dinner</a></li>
                        </ul>
                    </div>
                    */ ?>
                </div>
            </div>
        </div>
    </div>
</div>