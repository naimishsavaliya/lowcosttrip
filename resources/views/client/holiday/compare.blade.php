@extends('layouts.client')

@section('content')
<script>
	var cityData = '';
</script>
<style type="text/css">
	.max-height{
		max-height: 110px;
	}
</style>
<section>
    <div class="dashboard-tabs">
		<div class="dashboard-tabs-heading">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<h2 class="dashboard-title">Tour Comapre</h2>
					</div>
					<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
						<a href="{{route('empty-tour')}}" class="btn-holiday col-md-3 pull-right text-center">Empty Comapre</a>
					</div>
				</div>
			</div>
		</div>
    	<div class="dashboard-tabs-content">
            	<div class="container">
                	<div class="row">
                    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        	<div class="tab-content"> 
                            	<div id="tab-holidays" class="tab-pane fade in active">
                                	<div class="quote-tabs-content">
		                                <?php /*
		                                    <div class="row">
		                                        <div class="quote-booking-details">
		                                            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
		                                                <h2 class="quote-title">Comapre Tour</h2>
		                                            </div>
		                                        </div>
		                                    </div>
		                                 */ ?>   
	                                    <div class="row">
	                                    	<?php //dd($compare_data);
	                                    	$color = array("0" => "bg-violet", "1" => "bg-tangelo", "2" => "bg-vividblue")
	                                    	?>
	                                    	@if(count($compare_data)>0)
	                                    	@foreach($compare_data as $key => $compare)
											<div class="col-sm-4">
												<div class="boc-time">
													<h4 >{{date("d M,Y", strtotime($compare['tour']->created_at))}}</h4>
												</div>
												<div class="bo-compare">
													<div class="boc-main">
														<div class="boc-header {{$color[$key]}}">
															<div class="row">
																<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
																	<div class="boc-ti">
																		<img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/logo.jpg" width="114px"     height="50px" class="img-responsive"/>
																	</div>
																</div>
																<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 no-pad">
																		<h3 class="boc-travel-name">{{ (isset($compare['tour']->tour_name)) ? $compare['tour']->tour_name : '' }}</h3>
																	<p class="boc-travel-data">259 Trip Sold | 250 Review | 5 <i class="fa fa-star text-yellow"></i> </p>
																</div>
															</div>
														</div>
														<div class="boc-content">
															<div class="row">
																<div class="col-xs-6">
																	<div class="qto-number">
																		<h4>Tour code</h4>
																		<h3>{{ (isset($compare['tour']->tour_code)) ? $compare['tour']->tour_code : '' }}</h3>
																	</div>
																</div>
																<?php /*
																<div class="col-xs-6 pull-right">
																	<div class="text-right">
																		<div class="qto-cost">
																			<h4>Total Cost<br/>({{ (isset($compare['package']->name)) ? $compare['package']->name : '' }})</h4>
																			<h3 class="text-red">Rs.{{ (isset($compare['cost']->total_cost)) ? number_format((float)$compare['cost']->total_cost, 2, '.', '') : '' }}/-</h3>
																		</div>
																	</div>
																</div>
																*/ ?>
																<div class="col-xs-12">
																	<p>{{ (isset($compare['tour']->short_description)) ? substr($compare['tour']->short_description, 0, 150) : '' }}</p>
																</div>
																<div class="col-xs-10">
																	<div class="holiday-package-facilities">
																		<ul>
																			<?php
																			$features=array(); 
																			if(isset($compare['tour']->feature_data) && $compare['tour']->feature_data != ''){
																				$features=explode(',', $compare['tour']->feature_data);
																			}
																			?>
																		
																		@if(count($features)>0)
																		@foreach($features as $f_key => $feature)
																		<li><a href="#"><i class="fa fa-check"></i>{{$feature}}</a></li>
																		@endforeach
																		@endif
																		<?php /*
																		<li><a href="#"><i class="fa fa-check"></i>Flight</a></li>
																		<li><a href="#"><i class="fa fa-check"></i>Transfer</a></li>
																		<li><a href="#"><i class="fa fa-check"></i>Food</a></li>
																		<li><a href="#"><i class="fa fa-check"></i>Tour Manager</a></li>
																		*/ ?>
																		</ul>
																	</div>
																</div>
															</div>
															@if(isset($compare['tour_itinerary']) && $compare['tour_itinerary']->count()>0)
															<div class="row">
																<div class="owl-carousel owl-theme booking-compare-carousel">
																	@foreach($compare['tour_itinerary'] as $itinerary_key => $itinerary)
																	<div class="item">
																		<div class="">
																			<div class="col-xs-6">
																				<h3 class="boc-title">itinerary</h3>
																			</div>
																			<div class="col-xs-6 ">
																				<h3 class="boc-title pull-right">  <i class="fa fa-chevron-left"></i> Day {{$itinerary->tour_day}} <i class="fa fa-chevron-right"></i> </h3>
																			</div>
																		</div>
																		<div class="">
																			<div class="col-xs-12">
																				<div class="boc-itinerary">
																					<h5>{{$itinerary->title}}</h5>
																				@if($itinerary['image']!='' && file_exists(public_path('/uploads/itinerary/'.$itinerary['image'])))
																				<img class="max-height" src="{{ env('APP_PUBLIC_URL')}}uploads/itinerary/{{$itinerary['image']}}" width="100%" height="auto" />
																				@else
																				<img class="max-height" src="{{ env('APP_PUBLIC_URL')}}client/images/default-placeholder.png" width="100%" height="auto"/>
																				@endif
																					<p>{{$itinerary->description}}</p>
																				</div>
																			</div>
																		</div>
																	</div>
																	@endforeach
																	<?php /*
																	<div class="item">
																		<div class="">
																			<div class="col-xs-6">
																				<h3 class="boc-title">itinerary</h3>
																			</div>
																			<div class="col-xs-6 ">
																				<h3 class="boc-title pull-right">  <i class="fa fa-chevron-left"></i> Day 1 <i class="fa fa-chevron-right"></i> </h3>
																			</div>
																		</div>
																		<div class="">
																			<div class="col-xs-12">
																				<div class="boc-itinerary">
																					<h5>Arrival in Bangalore and Proceed to Mysore</h5>
																					<p>On arrival check in to hotel. After fresh ‘n' up proceed to visit Maharaja's Palace, Tipu Summer Palace, Mysore Zoo, and Brindawan garden.</p>
																				</div>
																			</div>
																		</div>
																	</div>
																	*/ ?>
																</div>
															</div>
															@endif
															@if(isset($compare['hotels']) && $compare['hotels']->count()>0)
															<div class="row">
																<div class="col-xs-12">
																	<div class="boc-hotel">
																		<h3 class="boc-title">Hotel</h3>
																		<table class="table table-responsive table-bordered table-bo">
																			<thead>
																				<tr>
																				<td>Location</td>
																				<td>Nights</td>
																				<td>Hotel Names</td>
																				</tr>
																			</thead>
																			<tbody>
																				@foreach($compare['hotels'] as $h => $tour_hotel)
																				<tr>
																				<td>{{$tour_hotel->location}}</td>
																				<td>{{$tour_hotel->night}}N</td>
																				<td>{{$tour_hotel->hotel}} <br> {{$tour_hotel->hotel_star}} <i class="fa fa-star text-yellow"></i></td>
																				</tr>
																				@endforeach
																				<?php /*
																				<tr>
																				<td>Mumbai</td>
																				<td>3</td>
																				<td>The Grand Palace<br> 4   <i class="fa fa-star text-yellow"></i></td>
																				</tr>
																				*/?>
																				</tbody>
																		</table>
																	</div>
																</div>
															</div>
															@endif
															<div class="row">
																<div class="col-xs-12">
																	<div class="boc-contact">
																		<a href="{{route('holiday-detail',[$compare['tour']->id,str_slug($compare['tour']->tour_name,'-')])}}" class=" btn btn-default btn-boc {{$color[$key]}}" target="_blank">View Details</a>
																		<?php /*
																		<button class="btn btn-default btn-boc bg-violet">View Details</button>
																		*/ ?>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach
											@endif
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
@endsection

@section('javascript')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
    // Pure Javascript
    document.getElementsByClassName("activity-details").onmouseover = function(){
        document.getElementsByClassName("activity-overlay").style.display = 'block';
    }
    document.getElementsByClassName("activity-details").onmouseout = function(){
        document.getElementsByClassName("activity-overlay").style.display= 'none';
    }
</script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ env('APP_PUBLIC_URL') }}client/js/owl.carousel.min.js"></script>
<script>
    var owl=$('.booking-compare-carousel').owlCarousel({
        loop:true,
        margin:0,
        dots: false,
        responsiveClass:true,
        nav:true,
        navigation : false,
        navText : ['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive:{
            0:{
                items:1,
                nav:true
            },
                600:{
                items:1,
                nav:true
            },
            1000:{
                items:1,
                nav:true,
                loop:false,
                margin:0
            }
        }
    });
	// $('.oc_select_next_0').on('click',function(){
	// 	$(this).css('background-color','#f3f3f3');
	// 	owl.trigger('next.owl.carousel');
	// });
	// $('.oc_select_prev_0').on('click',function(){
	// 	$(this).css('background-color','#f3f3f3');
	// 	owl.trigger('prev.owl.carousel');
	// });
</script>
@endsection