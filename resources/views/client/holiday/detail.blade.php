@extends('layouts.client')
@section('content')
<script type="text/javascript">
    cityData = '';
</script>
<section>
    <div class="detailslider-sec">
        <?php //$lines_arr = preg_split('/\n|\r/',$tour->inclusions);
        //dd(array_filter($lines_arr));
        //@if(isset($tour_image) && count($tour_image)>0)
        ?>
        @if(isset($tour_image) && count($tour_image)>0)
        <div id="detailslider" class="owl-carousel">
            @foreach($tour_image as $image_key => $image)
            <div class="item">
                <?php if(isset($image->filename) && $image->filename!=''){ ?>
                <img src="{{ env('AWS_BUCKET').$image->filename}}" class="img-responsive" style="width:auto">
                <?php }else{ ?>
                <img src="{{env('APP_PUBLIC_URL')}}client/images/no-banner.png" class="img-responsive" style="width: 1423px;height: 337px;" alt="•">
                <?php } ?> 
            </div>
            @endforeach
        </div>
        @else
        <div id="detailslider" class="owl-carousel">
            <div class="item">
                <img src="{{env('APP_PUBLIC_URL')}}client/images/no-banner.png" class="img-responsive" style="width: 1423px;height: 337px;" alt="•">
            </div>
        </div>
        @endif
        <?php /*
        <div id="detailslider">
            <div class="item">
                <img src="{{ env('APP_PUBLIC_URL')}}images/Banner_detail.jpg" class="img-responsive" style="width:auto">
            </div>
        </div>
        */ ?>
        <div class="sidetourinfo">
            @if(isset($tour->tour_code) && $tour->tour_code!='')
                <div class="toptitle">{{$tour->tour_code}}</div>
            @endif
            @if(isset($tour->tour_name) && $tour->tour_name!='')
                <div class="maintitle">{{$tour->tour_name}}</div>
            @endif
            @if(isset($tour->tour_nights) && $tour->tour_nights!='')
                <div class="subtitle">{{$tour->tour_nights}} Nights & {{$tour->tour_nights + 1}} Days</div>
            @endif
            <ul>
                <?php
                    $features = array();
                    if (isset($tour->feature_data) && $tour->feature_data != '') {
                        $features = explode(',', $tour->feature_data);
                    }
                ?>
                    @if(count($features)>0)
                    @foreach($features as $f_key => $feature)
                    <li><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Right_Icon.png" class="img-responsive" style="width:auto"> {{$feature}}</li>
                    @endforeach
                    @endif
            </ul>
            <?php /*
            <div class="bottomtitle">EX-MUMBAI</div>
            <div class="price">Rs. 145000/-</div>
            */ ?>
            <div class="bottombtn">
                <a class="booknw">Book Now</a>
                <a href="{{route('inquiry-now',[$tour->id,str_slug($tour->tour_name,'-'),1])}}" class="inqunw">Inquire Now</a>
            </div>
        </div>
    </div>
    <div class="topdetailmenu">
        <div class="container ">
            <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <li><a href="#overview">Overview</a></li>
                <li><a href="#ititnerary">Itinerary</a></li>
                <li><a href="#departuredate">Departure Date & cost</a></li>
                <li><a href="#inclusion">Inclusions</a></li>
                <li><a href="#exclusion">Exclusions</a></li>
                <li><a href="#inquirenw">Inquire Now</a></li>
            </ul>
        </div>
    </div>
    <div class="container tourdetailmain">
        <div class="leftfixbar">
            <div class="sidebox">
                <p><b>Need Lowcosttrip Help?</b><br>we are more than haapy to help you, kindly call or email us and we will assist you with best of our sevices.</p>
                <ul class="contactdetail">
                    <li><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Call_Icon.png" ><a class="contactinfo">81 828 38485</a></li>
                    <li><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Email.png" ><a class="contactinfo">info@lowcosttrip.com</a></li>
                </ul>
                <a href="{{route('inquiry-now',[$tour->id,str_slug($tour->tour_name,'-'),1])}}" class="inquirbtn">Inquire Now</a>
                <a class="bookbtn">Book Now</a>
            </div>
            <div class="sidebox">
                <p><b>Share Itinerary</b></p>
                <ul class="sharebtn">
                    <li><a><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Whats_App.png" ></a></li>
                    <li><a><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Messanger.png" ></a></li>
                    <li><a><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Share.png" ></a></li>
                    <li><a><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Email_2.png" ></a></li>
                    <li><a><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Linkdin.png" ></a></li>
                </ul>
            </div>
            @if(isset($tour->english_pdf) && $tour->english_pdf!='')
            <div class="sidebox">
                <p><b>Download Itinerary English PDF</b></p>
                <div class="pdfbox">
                    <a class="downpdfbtn" target="_blank" href="{{ (($tour->english_pdf != '') ? (env('AWS_BUCKET').$tour->english_pdf) : (env('AWS_BUCKET').$tour->english_pdf) )}}">Download PDF</a>
                    <img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/PDF_Icon.png" class="pdficn">
                </div>
            </div>
            @endif
            @if(isset($tour->gujarati_pdf) && $tour->gujarati_pdf!='')
            <div class="sidebox">
                <p><b>Download Itinerary Gujarati PDF</b></p>
                <div class="pdfbox">
                    <a class="downpdfbtn" target="_blank" href="{{ (($tour->gujarati_pdf != '') ? (env('AWS_BUCKET').$tour->gujarati_pdf) : (env('AWS_BUCKET').$tour->gujarati_pdf) )}}">Download PDF</a>
                    <img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/PDF_Icon.png" class="pdficn">
                </div>
            </div>
            @endif
        </div>
        
        <div class="row">
            @if(isset($tour->short_description) && $tour->short_description!='')
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="overview">
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 leftsec">
                    <p>{{$tour->short_description}}</p>

                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 rightsec" style="margin-top: 16px;">
                    <ul>
                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Hotel_Icon.png">
                            <div class="serviname">Hotels</div>
                        </li>
                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Food_Icon.png">
                            <div class="serviname">Food</div>
                        </li>
                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Sightseen_Icon.png">
                            <div class="serviname">Sightseeing</div>
                        </li>
                        <li>
                            <img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Transport_Icon.png">
                            <div class="serviname">Transport &
                                <br>Sightseeing</div>
                        </li>
                    <ul>
                </div>
            </div>
            @endif
            @if(isset($tour_itinerary) && $tour_itinerary->count()>0)
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="ititnerary">
                <div class="titlemain">
                    <div class="nam">Your Itinerary</div>
                    <div class="option">
                        <ul>
                            <li class="email"><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Email_Icon.png"><div class="opnm">Email</div></li>
                            <li class="print"><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/Print.png"><div class="opnm">Print</div></li>
                        </ul>
                    </div>
                </div>
                @foreach($tour_itinerary as $itineraryId=>$itinerary)
                <div class="dayititnerary">
                    <div class="dayname">Day {{(isset($itinerary->tour_day) ? $itinerary->tour_day :'')}}</div>
                    <div class="destination">Destination – {{(isset($itinerary->title) ? $itinerary->title :'')}} (1 Night)</div>
                    <div class="aboutext">
                        {{(isset($itinerary->description) ? $itinerary->description :'')}}
                    </div>
                </div>
                <?php /*
                <div class="services">
                    <div class="servbox">
                        <div class="servimg"><img src="http://103.13.242.103/~webdemo/lct/images/Hotel_Icon.png"></div>
                        <div class="servnm">Night Stay in Innsbruck</div>
                    </div>
                    <div class="servbox">
                        <div class="servimg"><img src="http://103.13.242.103/~webdemo/lct/images/Food_Icon.png"></div>
                        <div class="servnm">Lunch & Dinner</div>
                    </div>
                </div>
                */ ?>
                @endforeach
            </div>
            @endif
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="departuredate">
                <div class="titlemain"><div class="nam">Departure Date & Cost</div></div>
                <div id='calendar'></div>
            </div>
             @if(isset($tour->inclusions) && $tour->inclusions!='')
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="inclusion">
                <div class="titlemain"><div class="nam">Inclusions</div></div>
                <div class="containdetail">
                    <ul class="withright">
                        <li>{!! nl2br($tour->inclusions) !!}</li>
                    </ul>
                </div>
            </div>
            @endif
            @if(isset($tour->exclusions) && $tour->exclusions!='')
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="exclusion">
                <div class="titlemain"><div class="nam">Exclusions</div></div>
                <div class="containdetail">
                    <ul class="withright">
                        <li>{!! nl2br($tour->exclusions) !!}</li>
                    </ul>
                </div>
            </div>
            @endif
            @if(isset($tour->terms_conditions) && $tour->terms_conditions!='')
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="termcondition">
                <div class="titlemain"><div class="nam">Terms & Condition</div></div>
                <div class="containdetail">
                <p>{{$tour->terms_conditions}}</p>
                </div>
            </div>
            @endif
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="inquirenw">
                <div class="containdetail">
                    <a href="{{route('inquiry-now',[$tour->id,str_slug($tour->tour_name,'-'),1])}}" class="inqubtn">Inquire Now</a>
                </div>
            </div>
        </div>
        <br/>
        <br/>
    </div>
</section>
@endsection
@section('javascript')
<script src="{{ env('APP_PUBLIC_URL')}}new-theme/js/moment.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}new-theme/js/fullcalendar.min.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        $('body').scroll(function() {
            if($('body').scrollTop()>435){
                $('.leftfixbar').addClass('fixedbar');
            }else {
                $('.leftfixbar').removeClass('fixedbar');
            }
            if($('body').scrollTop()>2900){
                $('.leftfixbar').addClass('absolutebar');
                $('.leftfixbar').removeClass('fixedbar');
            }else{
                $('.leftfixbar').removeClass('absolutebar');
            }
        }); 
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultDate: '2019-01-12',
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
            events: [{
                title: 'Inr 14000',
                start: '2019-01-01'
            },
                {
                    title: 'Inr 19000',
                    start: '2019-01-07'
                },
                {
                    id: 999,
                    title: 'Inr 24000',
                    start: '2019-01-09T16:00:00'
                },
                {
                    id: 999,
                    title: 'Inr 44000',
                    start: '2019-01-16T16:00:00'
                },
                {
                    title: 'Inr 14000',
                    start: '2019-01-11'
                },
                {
                    title: 'Inr 14000',
                    start: '2019-01-12T10:30:00',
                },
                {
                    title: 'Inr 14000',
                    start: '2019-01-12T12:00:00'
                },
                {
                    title: 'Inr 14000',
                    start: '2019-01-12T14:30:00'
                },
                {
                    title: 'Inr 14000',
                    start: '2019-01-12T17:30:00'
                },
                {
                    title: 'Inr 14000',
                    start: '2019-01-12T20:00:00'
                },
                {
                    title: 'Inr 14000',
                    start: '2019-01-13T07:00:00'
                },
                {
                    title: 'Inr 14000',
                    url: 'http://google.com/',
                    start: '2019-01-28'
                }
            ]
        });
    }); 
</script>
@endsection