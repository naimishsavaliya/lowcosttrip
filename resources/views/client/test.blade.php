<!DOCTYPE html>
<html lang="en">
    <head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Title -->
    <title>Low Cost Trip</title>
    <!-- Favicon -->
    <link rel="icon" href="{{ env('APP_PUBLIC_URL')}}/favicon.ico" type="image/x-icon">
    <!-- FONTS -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}client/fonts/fa/css/font-awesome.min.css">
    <link href="{{ env('APP_PUBLIC_URL')}}client/fonts/soap/soap-icon.css" rel="stylesheet" type="text/css">
    <!--Bootstrap Css -->
    <link href="{{ env('APP_PUBLIC_URL')}}client/css/bootstrap.min.css" rel="stylesheet">
    <!-- External Css -->
    <link rel='stylesheet prefetch' href='assets/owl/css/owl.carousel.css'>
    <link href="{{ env('APP_PUBLIC_URL')}}client/css/media-queries.css" rel="stylesheet" type="text/css">
    <link href="{{ env('APP_PUBLIC_URL')}}client/test.css" rel="stylesheet" type="text/css">
    <link href="{{ env('APP_PUBLIC_URL')}}client/css/custom.css" rel="stylesheet" type="text/css">
    <link href="{{ env('APP_PUBLIC_URL')}}css/responseve.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href=" https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript">
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>;
        var APP_URL = "<?= env('APP_URL') ?>";
    </script>
    </head>
<script>
    var cityData = '';
</script>
<body>
    <header>
      <div class="hidden-xs">
        <div class="main-nav">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="logo"> 
                  <a class="navbar-brand" href="#">
                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/logotest.jpg" class="img-responsive">
                  </a> 
                </div>
              </div>
              <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <ul class="nav nav-tabs navbar-nav " id = "myTab">
                  <li class="active"><a href="#holiday-block" data-toggle = "tab">Holidays</a></li>
                  <li><a href="#" data-toggle = "tab">Hotels</a></li>
                  <li ><a href="#" data-toggle = "tab">Flights</a></li>
                  <li><a href="#" data-toggle = "tab">Visa</a></li>
                  <li><a href="" data-toggle = "tab">Login</a></li>
                  <li><a href="#"><i class="fa fa-phone"></i> +91 8182 828 282</a> </li>
                  <li><a href="#"><i class="fa fa-search"></i> </a> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="header-bottom">
          <div id = "myTabContent" class = "tab-content">
            <div  class = "tab-pane fade  in active" id="holiday-block">
            <!-- Search Block Begins -->
              <div class="search-block hotel-search">
                <div class="main-sub-nav">
                  <div class="container">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 col-lg-offset-3">
                        <nav>
                          <ul class="ul-reset">
                            <li class="droppable"> <a href='#'>Domestic Tours</a>
                              <div class='mega-menu'>
                                <div class="cf">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                                      <div class="row">
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Andaman & Nicobar</a></h4>
                                            <ul>
                                              <li><a href="#">Port Blair</a></li>
                                            </ul>
                                          </div>
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Goa</a></h4>
                                          </div>
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Gujarat</a></h4>
                                            <ul>
                                              <li><a href="#">Ahmedabad</a></li>
                                              <li><a href="#">Bhuj</a></li>
                                              <li><a href="#">Ahmedabad</a></li>
                                              <li><a href="#">Ahmedabad</a></li>
                                              <li><a href="#">Ahmedabad</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Himachal & Surroundings</a></h4>
                                            <ul>
                                              <li><a href="#">Amritsar</a></li>
                                              <li><a href="#">Chandigarh</a></li>
                                              <li><a href="#">Dalhousie</a></li>
                                              <li><a href="#">Dharamshala</a></li>
                                              <li><a href="#">Manali</a></li>
                                              <li><a href="#">Shimla</a></li>
                                            </ul>
                                          </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Kashmir</a></h4>
                                          </div>
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Kerala</a></h4>
                                            <ul>
                                              <li><a href="#">Cochin</a></li>
                                              <li><a href="#">Kumarakom</a></li>
                                              <li><a href="#">Kanyakumari</a></li>
                                              <li><a href="#">Munnar</a></li>
                                              <li><a href="#">Periyar</a></li>
                                              <li><a href="#">Thiruvananthapuram</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Leh Ladakh</a></h4>
                                            <ul>
                                              <li><a href="#">Kargil</a></li>
                                              <li><a href="#">Leh</a></li>
                                              <li><a href="#">Nubra Valley</a></li>
                                            </ul>
                                          </div> 
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Madhya Pradesh</a></h4>
                                            <ul>
                                              <li><a href="#">Bhopal</a></li>
                                              <li><a href="#">Gwalior</a></li>
                                              <li><a href="#">Indore</a></li>
                                              <li><a href="#">Jabalpur</a></li>
                                              <li><a href="#">Khajuraho</a></li>
                                              <li><a href="#">Mandu</a></li>
                                              <li><a href="#">Pachmarhi</a></li>
                                              <li><a href="#">Ujjain</a></li>
                                            </ul>
                                          </div>
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Maharashtra</a></h4>
                                          </div>
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">North India</a></h4>
                                            <ul>
                                              <li><a href="#">Allahabad</a></li>
                                              <li><a href="#">Agra</a></li>
                                              <li><a href="#">Delhi</a></li>
                                              <li><a href="#">Varanasi</a></li>
                                            </ul>
                                          </div> 
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">North East</a></h4>
                                            <ul>
                                              <li><a href="#">Agartala</a></li>
                                              <li><a href="#">Aizawl</a></li>
                                              <li><a href="#">Bomdila</a></li>
                                              <li><a href="#">Cherrapunjee</a></li>
                                              <li><a href="#">Guwahati</a></li>
                                              <li><a href="#">Imphal</a></li>
                                              <li><a href="#">Kohima</a></li>
                                              <li><a href="#">Shillong</a></li>
                                              <li><a href="#">Silchar</a></li>
                                              <li><a href="#">Tawang</a></li>
                                              <li><a href="#">Tezpur</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Odisha</a></h4>
                                            <ul>
                                              <li><a href="#">Bhubaneshwar</a></li>
                                              <li><a href="#">Puri</a></li>
                                            </ul>
                                          </div> 
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Rajasthan</a></h4>
                                            <ul>
                                              <li><a href="#">Ajmer</a></li>
                                              <li><a href="#">Bikaner</a></li>
                                              <li><a href="#">Jaipur</a></li>
                                              <li><a href="#">Jaisalmer</a></li>
                                              <li><a href="#">Jodhpur</a></li>
                                              <li><a href="#">Udaipur</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Sikkim Darjeeling</a></h4>
                                            <ul>
                                              <li><a href="#">Darjeeling</a></li>
                                              <li><a href="#">Gangtok</a></li>
                                              <li><a href="#">Kolkata</a></li>
                                              <li><a href="#">Lachung</a></li>
                                            </ul>
                                          </div> 
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">South India</a></h4>
                                            <ul>
                                              <li><a href="#">Badami</a></li>
                                              <li><a href="#">Bijapur</a></li>
                                              <li><a href="#">Coorg</a></li>
                                              <li><a href="#">Hampi</a></li>
                                              <li><a href="#">Hyderabad</a></li>
                                              <li><a href="#">Mysore</a></li>
                                              <li><a href="#">Ooty</a></li>
                                              <li><a href="#">Pondicherry</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Uttarakhand</a></h4>
                                            <ul>
                                              <li><a href="#">Haridwar</a></li>
                                              <li><a href="#">Mussoorie</a></li>
                                              <li><a href="#">Nainital</a></li>
                                              <li><a href="#">Rishikesh</a></li>
                                              <li><a href="#">Hyderabad</a></li>
                                              <li><a href="#">Mysore</a></li>
                                              <li><a href="#">Ooty</a></li>
                                              <li><a href="#">Pondicherry</a></li>
                                            </ul>
                                          </div> 
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="text-center">
                                          <a href="#" class="view-tours text-center">View Tours</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li class="droppable"> <a href='#'>International Tours</a>
                              <div class='mega-menu'>
                                <div class="cf">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                      <div class="row">
                                        <div class="col-xs-12 col-sm-16 col-md-16 col-lg-16">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Africa</a></h4>
                                            <ul>
                                              <li><a href="#">Cape Town</a></li>
                                              <li><a href="#">Johannesburg</a></li>
                                              <li><a href="#">Kenya</a></li>
                                              <li><a href="#">Masai Mara</a></li>
                                              <li><a href="#">South Africa</a></li>
                                              <li><a href="#">Victoria Falls</a></li>
                                              <li><a href="#">Zimbabwe</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">America</a></h4>
                                            <ul>
                                              <li><a href="#">Alaska</a></li>
                                              <li><a href="#">Canada</a></li>
                                              <li><a href="#">Chicago</a></li>
                                              <li><a href="#">Las Vegas</a></li>
                                              <li><a href="#">Los Angeles</a></li>
                                              <li><a href="#">New York</a></li>
                                              <li><a href="#">Niagara Falls</a></li>
                                              <li><a href="#">San Francisco</a></li>
                                              <li><a href="#">USA</a></li>
                                              <li><a href="#">Washington</a></li>
                                            </ul>
                                          </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-16 col-md-16 col-lg-16">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Antarctica</a></h4>
                                          </div>
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Australia</a></h4>
                                            <ul>
                                              <li><a href="#">Cairns</a></li>
                                              <li><a href="#">Canberra</a></li>
                                              <li><a href="#">Fiji</a></li>
                                              <li><a href="#">Gold Coast</a></li>
                                              <li><a href="#">Melbourne</a></li>
                                              <li><a href="#">Sydney</a></li>
                                              <li><a href="#">Wollongong</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Bhutan</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Europe</a></h4>
                                            <ul>
                                              <li><a href="#">Austria</a></li>
                                              <li><a href="#">Belgium</a></li>
                                              <li><a href="#">Croatia</a></li>
                                              <li><a href="#">Czech Republic</a></li>
                                              <li><a href="#">Denmark</a></li>
                                              <li><a href="#">England</a></li>
                                            </ul>
                                          </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-16 col-md-16 col-lg-16">
                                          <div class="dropdown-menu-item">
                                            <ul>
                                              <li><a href="#">Finland</a></li>
                                              <li><a href="#">France</a></li>
                                              <li><a href="#">Geneva</a></li>
                                              <li><a href="#">Germany</a></li>
                                              <li><a href="#">Greece</a></li>
                                              <li><a href="#">Hungary</a></li>
                                              <li><a href="#">Iceland</a></li>
                                              <li><a href="#">Interlaken</a></li>
                                              <li><a href="#">Italy</a></li>
                                              <li><a href="#">Netherlands</a></li>
                                              <li><a href="#">Norway</a></li>
                                              <li><a href="#">Paris</a></li>
                                              <li><a href="#">Russia</a></li>
                                              <li><a href="#">Scotland</a></li>
                                              <li><a href="#">Spain</a></li>
                                              <li><a href="#">Sweden</a></li>
                                              <li><a href="#">Switzerland</a></li>
                                              <li><a href="#">Vatican City</a></li>
                                            </ul>
                                          </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-16 col-md-16 col-lg-16">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Far East Asia</a></h4>
                                            <ul>
                                              <li><a href="#">Beijing</a></li>
                                              <li><a href="#">China</a></li>
                                              <li><a href="#">Hiroshima</a></li>
                                              <li><a href="#">Japan</a></li>
                                              <li><a href="#">Korea</a></li>
                                              <li><a href="#">Macao</a></li>
                                              <li><a href="#">Osaka</a></li>
                                              <li><a href="#">Shanghai</a></li>
                                              <li><a href="#">Shenzhen</a></li>
                                              <li><a href="#">Taiwan</a></li>
                                              <li><a href="#">Tokyo</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Maldives</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Mauritius</a></h4>
                                          </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-16 col-md-16 col-lg-16">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Middle East</a></h4>
                                            <ul>
                                              <li><a href="#">Abu Dhabi</a></li>
                                              <li><a href="#">Cairo</a></li>
                                              <li><a href="#">Dubai</a></li>
                                              <li><a href="#">Egypt</a></li>
                                              <li><a href="#">Israel</a></li>
                                              <li><a href="#">Jordan</a></li>
                                              <li><a href="#">Muscat</a></li>
                                              <li><a href="#">Oman</a></li>
                                              <li><a href="#">Petra</a></li>
                                              <li><a href="#">United Arab Emirates</a></li>
                                            </ul>
                                          </div>
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Nepal</a></h4>
                                          </div>
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">New Zealand</a></h4>
                                            <ul>
                                              <li><a href="#">Auckland</a></li>
                                              <li><a href="#">Christchurch</a></li>
                                              <li><a href="#">Queenstown</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Seychelles</a></h4>
                                          </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-16 col-md-16 col-lg-16">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">South America</a></h4>
                                            <ul>
                                              <li><a href="#">Argentina</a></li>
                                              <li><a href="#">Brazil</a></li>
                                            </ul>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">South East Asia</a></h4>
                                            <ul>
                                              <li><a href="#">Bali</a></li>
                                              <li><a href="#">Bangkok</a></li>
                                              <li><a href="#">Cambodia</a></li>
                                              <li><a href="#">Hong Kong</a></li>
                                              <li><a href="#">Krabi</a></li>
                                              <li><a href="#">Kuta</a></li>
                                              <li><a href="#">Langkawi</a></li>
                                              <li><a href="#">Malaysia</a></li>
                                              <li><a href="#">Pattaya</a></li>
                                              <li><a href="#">Phuket</a></li>
                                              <li><a href="#">Singapore</a></li>
                                              <li><a href="#">Thailand</a></li>
                                              <li><a href="#">Ubud</a></li>
                                              <li><a href="#">Vietnam</a></li>
                                            </ul>
                                          </div> 
                                        </div>
                                        <div class="col-xs-12 col-sm-16 col-md-16 col-lg-16">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Sri Lanka</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Bollywood Parks</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Cherry Blossom in Japan</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Eastern Europe</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Ferrari World</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Great Barrier Reef</a></h4>
                                          </div> 
                                        </div>
                                        <div class="col-xs-12 col-sm-16 col-md-16 col-lg-16">
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Great Ocean Road</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Great Wall of China</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Northern Lights</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Rio Carnival</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Scandinavia</a></h4>
                                          </div> 
                                          <div class="dropdown-menu-item">
                                            <h4><a href="#">Trans Siberian</a></h4>
                                          </div> 
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                          <div class="text-center">
                                            <a href="#" class="view-tours text-center">View Tours</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li><a href='#'>Group Tours</a></li>
                            <!-- .droppable -->
                            <li class="droppable"><a href='#'>Specialty Tours</a>
                              <div class='mega-menu'>
                                <div class="cf">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/1.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Festival Tours</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/2.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">
                                        Grandparents & Grandchildren Tours</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/3.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Honeymoon Special</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/4.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Jubilee Special Tours</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/5.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Luxury Tours</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/6.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Post Tour Holiday</a></h4>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/6.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Seniors Special</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/5.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Singles' Special</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/4.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Specially Abled</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/3.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Weekend Special</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/2.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Wildlife Tours</a></h4>
                                      </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                      <div class="megamenu-image-block">
                                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/menu/1.jpg" class="img-responsive clearfix">
                                        <h4><a href="" class="">Women's Special</a></h4>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>              
                            </li>
                            <li><a href="#">Deals</a></li>
                          </ul>
                        <!-- .container .ul-reset -->
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="holiday-bottom">
                  <div class="container">
                    <div class="row">
                      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-lg-offset-1">
                        <form role="search" method="get" action="#">
                          <div class="row">
                            <div>
                              <input type="text" placeholder="Search Your Destination" name="search" class=" search-dest">
                            </div>
                            <div>
                              <button type="submit" class=" btn-search ">Search</button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                        <div class="text-center">
                        <br>
                        <span class="search-span">or</span>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
                        <div class="row">
                          <div class="search-cust">
                          <a href="#" class="btn btn-default btn-customize">Build your package</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- Search Block Ends -->
            </div>
          </div>
        </div>
      </div> 
      <div class=" mobile-nav hidden-sm hidden-md hidden-lg">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span> 
              </button>
              <a class="navbar-brand" href="#"><img src="images/logo.jpg" class="img-responsive"></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
                <li class="active dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown" >Holidays</a>
                <ul class="dropdown-menu">
                <li><a href="#">Domestic Tours</a></li>
                <li><a href="#">International Tours</a></li>
                <li><a href="#">Group Tours</a></li>
                <li><a href="#">Tours By Price</a></li>
                <li><a href="#">Deals</a></li>
                </ul>
                </li>
                <li><a href="#">Hotels</a></li>
                <li><a href="#">Flights</a></li> 
                <li><a href="#">Visa</a></li> 
                <li><a href="#">Login</a></li> 
              </ul>
            </div>
          </div>
        </nav>
      </div> 
    </header>
    <section class="page-body">
            <div class="container-fluid">
                <div class="slider-banner">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                            <div class="owl-carousel owl-slider owl-theme">
                                <div class="item"> 
                                    <div class="custom_overlay_wrapper">
                                        <img src="http://naimish/lowcosttrip/public/uploads/home_banner/4887174214.jpg" class="img-responsive">
                                            <div class="custom_overlay">
                                                <div class="custom_overlay_inner ">
                                                <h4 class="banner-heading4">Dazzling</h4>
                                                <h2 class="banner-heading2">Dubai</h2>
                                                <span class="banner-price">Starts From: USD 495 (INR. 32,670) </span>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="custom_overlay_wrapper">
                                        <img src="http://naimish/lowcosttrip/public/uploads/home_banner/4887174214.jpg" class="img-responsive">
                                        <div class="custom_overlay">
                                            <div class="custom_overlay_inner ">
                                                <h4 class="banner-heading4">Dazzling</h4>
                                                <h2 class="banner-heading2">Dubai</h2>
                                                <span class="banner-price">Starts From: USD 495 (INR. 32,670) </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item"> 
                                    <div class="custom_overlay_wrapper">
                                        <img src="http://naimish/lowcosttrip/public/uploads/home_banner/4887174214.jpg" class="img-responsive">
                                        <div class="custom_overlay">
                                            <div class="custom_overlay_inner ">
                                                <h4 class="banner-heading4">Dazzling</h4>
                                                <h2 class="banner-heading2">Dubai</h2>
                                                <span class="banner-price">Starts From: USD 495 (INR. 32,670) </span>
                                            </div>
                                        </div>
                                    </div>             
                                </div>
                                <div class="item"> 
                                    <div class="custom_overlay_wrapper">
                                        <img src="http://naimish/lowcosttrip/public/uploads/home_banner/4887174214.jpg" class="img-responsive">
                                        <div class="custom_overlay">
                                            <div class="custom_overlay_inner ">
                                                <h4 class="banner-heading4">Dazzling</h4>
                                                <h2 class="banner-heading2">Dubai</h2>
                                                <span class="banner-price">Starts From: USD 495 (INR. 32,670) </span>
                                            </div>
                                        </div>
                                    </div>             
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <footer>
            <div class="subscribe">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a class="navbar-brand1" href="{{route('index')}}"><img src="{{ env('APP_PUBLIC_URL')}}client/images/header-logo.png" class="img-responsive1"></a>
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <div class="subscribe-text">
                                    <h2 >Get Our Special Deals</h2>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <form role="search" method="get" action="#">
                                    <div class="row">
                                        <input type="text" placeholder="Your Email" name="search" class="subscribe-email">
                                        <button type="submit" class="btn-subscribe "><i class="fa fa-paper-plane "></i> Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-top">
                <div class="container">
                    <div class="footer-top-blocks">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="footer-top-block">
                                    <h3 class="footer-top-title">About Us</h3>
                                    <p class="footer-top-desc">The vision is to help you plan your holidays, providing you world class experience at low cost and creating memories for life time. Giving the best of luxury at the lowest price and making you feel important.</p>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                <div class="footer-top-block">
                                    <h3 class="footer-top-title">Quick Link</h3>
                                    <div class="footer-top-links">
                                        <ul>
                                            <li><a href="{{route('indian-holiday')}}">Indian Holidays</a></li>
                                            <li><a href="{{route('global-holiday')}}">Global Holidays</a></li>
                                            <li><a href='{{env("APP_URL")}}home/customise-book'>Customise & Book</a></li>
                                            <li><a href="{{ route('payment') }}">Online Payment</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                <div class="footer-top-block">
                                    <h3 class="footer-top-title"> &nbsp; </h3>
                                    <div class="footer-top-links">
                                        <ul>
                                            <li><a href="{{ route('about') }}">About Us</a></li>
                                            <li><a href="{{ route('contact') }}">Contact</a></li>
                                            <li><a href="{{ route('faq') }}">Faq</a></li>
                                            <li><a href="{{ route('terms-conditions') }}">T &amp; C</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="footer-top-block">
                                    <h3 class="footer-top-title">Contact Us</h3>
                                    <div class="footer-address-block">
                                        <div class="row">
                                            <div class="col-xs-4 footer-address">
                                                <i class="fa fa-map-marker"></i> Address
                                            </div>
                                            <div class="col-xs-8">
                                                <p>1205, 12th Floor, Sai Indu Towers, L.B.S. Road, Bhandup (W), Mumbai - 400078.  </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4 footer-address">
                                                <i class="fa fa-envelope"></i> Email
                                            </div>
                                            <div class="col-xs-8">
                                                <p>info@lowcosttrip.com </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4 footer-address">
                                                <i class="fa fa-phone"></i> Phone
                                            </div>
                                            <div class="col-xs-8">
                                                <p>+91 81 82 83 84 85 </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="footer-copyright">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <p>&copy; All Rights Reserved</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                                        <ul class="footer-payment">
                                            <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/comodo.jpg"/></a></li>
                                            <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/visa.jpg"/></a></li>
                                            <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/master-card.jpg"/></a></li>
                                            <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/ae.jpg"/></a></li>
                                            <li><a href="#"><img src="{{ env('APP_PUBLIC_URL')}}client/images/payment/paypal.jpg"/></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="{{ env('APP_PUBLIC_URL')}}client/js/bootstrap.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}client/js/custom.js"></script>
<link rel="stylesheet" href="{{ env('APP_PUBLIC_URL')}}css/slimmenu.min.css" type="text/css">
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.slimmenu.min.js"></script>
<!-- datatables -->
<script src="{{ env('APP_PUBLIC_URL')}}/client/data-table/js/jquery.dataTables.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}/client/data-table/js/dataTables.tableTools.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}/client/data-table/js/bootstrap-dataTable.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
<script>
$(function () {
$(".datepicker").datepicker({dateFormat: 'dd MM yy'});
});
</script>
<script>
$(document).ready(function() {
  $('.owl-slider').owlCarousel({
     margin: 30,
    loop: true,
    autowidth:true,
     autoplay:true,
    autoplayTimeout:10000,
    nav:true,
    
    autoplayHoverPause:true,
    items: 1,
    dots: false,
     navText : ['<i class=" fa fa-angle-left fa-2x" aria-hidden="true"></i>','<i class="    fa fa-angle-right fa-2x" aria-hidden="true"></i>'],

  });
});
</script>
<script>
$('#navigation').slimmenu(
{
resizeWidth: '800',
collapserTitle: 'Main Menu',
animSpeed: 'medium',
easingEffect: null,
indentChildren: false,
childrenIndenter: '&nbsp;'
});
</script>
<script>
$('#navigationnew').slimmenu(
{
resizeWidth: '800',
collapserTitle: 'Main Menu',
animSpeed: 'medium',
easingEffect: null,
indentChildren: false,
childrenIndenter: '&nbsp;'
});
</script>
</html>