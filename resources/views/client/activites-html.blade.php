<tr>
    <td> 
	    <select id="country_id" class="select-style country_id" name=" activiti_details['city'][]">
	        <option value="" selected>Choose Country</option>
	        @if(isset($countrys))
	        @foreach($countrys as $country)
	        <option value="{{ $country->country_id }}" {{ (isset($city->country_id) && $city->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
	        @endforeach
	        @endif
	    </select>
    </td>
    <td>
    	<select class="select-style tour_activity" name=" activiti_details['activiti'][]">
	        <option value="" selected>Choose Activity</option>
	    </select>
    </td>
    <td>
        <button type="button" class="btn btn-default btn-number destination_delete_btn" data-type="plus" data-field="quant[1]"><span class="glyphicon glyphicon-minus"></span></button>
    </td>
</tr>
