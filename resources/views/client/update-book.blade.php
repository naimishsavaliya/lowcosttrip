@extends('layouts.client')
<script>
	var cityData = <?= $cities ?>;
</script>
@section('content')
<style type="text/css">
    .last_border{
        border-bottom: 1px solid #cccccc !important;
    }
</style>
<section>
   <div id = "myTabContent" class = "tab-content">
      <div  class="tab-pane fade in active" id="customizes-block" >
            <div class="customize">
                <div class="container">
                    <div class="row">
                        <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="tab-content">
									<div id="Tour" class="tab-pane fade active in">
										<div class="booking-block" >
											<div class="booking-block-title  p-t-b-5" style="border-bottom:none;">
												<h3>Tour Inquire</h3>
											</div>
											<div class="holiday-booking-flight booking-block-form" style="margin-top: -15px;">
												<form name="update_book_frm" id="update_book_frm" action="" method="post" onsubmit="return false;">
												{{csrf_field()}}
												<input type="hidden" name="id" value="{{ isset($lead->id) ? $lead->id : '' }}" />
												<input type="hidden" name="tour_id" value="{{ isset($lead->tour_id) ? $lead->tour_id : '' }}" />
												<div class="row booking-form-padding" style="display: none;">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
														<ul class="validate-msg">
														</ul>
													</div>
												</div>
													<!-- <div class="row booking-form-padding sub-title">
														<label class="radio-inline">Tour Details</label>
													</div> -->
													<?php if (isset($holiday) && !empty($holiday)) { ?>
													<div class="row booking-form-padding">
													    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													        <div class="infinite-scroll">
													            <div class="holiday-list-item">
													                <div class="row no-margin">
													                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-pad">
													                        <div class="holiday-place-image">
													                            @if(isset($holiday->image) && $holiday->image!='')
													                            <img src="{{ env('AWS_BUCKET')}}{{$holiday->image}}" class="img-responsive"/>
													                            @endif
													                        </div>
													                    </div>
													                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 no-pad">
													                        <div class="holiday-tour-deatils">
													                            <div class="holiday-name-duration">
													                                <div class="holiday-name tour-title">
													                                    @if(isset($holiday->tour_name) && $holiday->tour_name!='')
													                                    <h3>{{$holiday->tour_name}}</h3>
													                                    @endif
													                                </div>
													                                <div class="holiday-duration">
													                                    @if(isset($holiday->tour_nights) && $holiday->tour_nights!='')
													                                    <h4>{{$holiday->tour_nights}} Nights {{$holiday->tour_nights + 1}} Days <span> - </span> <span class="holiday-tour-rating"> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></h4>
													                                    @endif
													                                </div>
													                            </div>
													                            <?php if(isset($cost['amount']) && count($cost['amount']) > 0){?>
													                            <div class="holiday-packages">
													                            @foreach($cost['amount'] as $c => $cost_val)
													                            @foreach($cost_val['rates'] as $r => $rate)
													                            @if(isset($rate['sell']) && $rate['sell'] > 0)
													                            <label class="radio-inline"><input type="radio" class="holydayHotelRadio" holyday="{{$holiday->id}}" package="{{$c}}" name="holydaycost{{$holiday->id}}" data-package_type="{{$c}}" {{($c==1)?"checked":""}}>{{$cost_val['name']}} <?php /*
													                            ({{($rate['sell'])-($rate['sell']*$cost['dates']['discount'])/100}}) */?></label>
													                            @endif
													                            @break;
													                            @endforeach
													                            @endforeach
													                            </div>
													                            <?php } ?>
													                            <?php if (isset($hotel) && count($hotel) > 0) { ?>
													                            <div class="holiday-packages-details">
													                                <table class="table table-responsive hotel_{{$holiday->id}} last_border" style="margin-top: 10px;">
													                                <tr class="hotel_label">
													                                    <td><h4><strong>Location</strong></h4></td>
													                                    <td><h4><strong>Hotel</strong></h4></td>
													                                    <td><h4><strong>Nights</strong></h4></td>
													                                    <td><h4><strong>Rating</strong></h4></td>
													                                </tr>
													                                @foreach($hotel as $h => $hotel_val)
													                                <tr class="{{($hotel_val->package_type!=1)?'hide':''}} package_{{$holiday->id}}{{$hotel_val->package_type}}">
													                                    <td><h4>{{isset($hotel_val->location)?$hotel_val->location:''}}</h4></td>
													                                    <td><h4>{{isset($hotel_val->hotel)?$hotel_val->hotel:''}}</h4></td>
													                                    <td><h4>{{isset($hotel_val->night)?$hotel_val->night:''}}N</h4></td>
													                                    <td><h4>{{isset($hotel_val->hotel_star)?$hotel_val->hotel_star:''}}<i class="fa fa-star"></i></h4></td>
													                                </tr>
													                                @endforeach
													                                </table>
													                            </div>
													                            <?php } ?>
													                            
													                            <div class="holiday-package-facilities">
													                                <ul>
													                                    <?php
													                                        $features = array();
													                                        if (isset($holiday->feature_data) && $holiday->feature_data != '') {
													                                            $features = explode(',', $holiday->feature_data);
													                                        }
													                                    ?>
													                                        @if(count($features)>0)
													                                        @foreach($features as $f_key => $feature)
													                                        <li><a href="#"><i class="fa fa-check"></i>{{$feature}}</a></li>
													                                        @endforeach
													                                        @endif
													                                </ul>
													                            </div>
													                        </div>
													                    </div>
													                    <?php if(isset($cost['amount']) && count($cost['amount']) > 0){?>
													                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
													                        <div class="holiday-tour-fare cost-{{$holiday->id}}">
													                        	<?php  /*
													                            @foreach($cost['amount'] as $c => $cost_val)
													                            @foreach($cost_val['rates'] as $r => $rate)
													                            <div class="amount package_cost_{{$holiday->id}}{{$c}} {{($c!=1)?'hide':''}}">
													                                @if(isset($rate['sell']) && $rate['sell'] > 0)
													                                @if($cost['dates']['discount']>0)
													                                <div class="actual-fare">
													                                    <h4>{{$rate['sell']}}</h4>
													                                </div>
													                                @endif
													                                <div class="discount-fare" style="margin-top: -10px;">
													                                    <h2>{{($rate['sell'])-($rate['sell']*$cost['dates']['discount'])/100}}</h2>
													                                </div>
													                                <div class="toal-persons" style="margin-top: -25px;">
													                                    <h4>{{$rate['name']['description']}}</h4>
													                                </div>
													                                @endif
													                            </div>
													                            @break;
													                            @endforeach
													                            @endforeach
													                            */ ?>
													                        </div>
													                    </div>
													                    <?php } ?>
													                </div>
													            </div>
													        </div>
													    </div>
													</div>
													<?php } ?>
													<div class="row booking-form-padding sub-title">
													    <label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Name</label>
																<input type="text" name="contact_name" class="form-control" value="{{ (isset($lead->name)) ? $lead->name : '' }}" placeholder="Name">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Contact No</label>
																<input type="text" name="contact_no" class="form-control onlynumber" maxlength="10" value="{{ (isset($lead->phone)) ? $lead->phone : '' }}" placeholder="Contact No">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Email Id</label>
																<input type="text" name="email_id" class="form-control" value="{{ (isset($lead->email)) ? $lead->email : '' }}" placeholder="Email Id">
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
													    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
													        <div class="input-group">
													            <label class="radio-inline nopadding">Adults</label>
													            <input type="text" name="adults" class="form-control" value="{{ (isset($lead->adult)) ? $lead->adult : '' }}" placeholder="ADULTS" min="1" max="10">
													        </div>
													    </div>
													    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
													        <div class="input-group">
													            <label class="radio-inline nopadding">Child</label>
													            <input type="text" name="child" class="form-control" value="{{ isset($lead->child) ? $lead->child : '' }}" placeholder="Child" min="1" max="10">
													        </div>
													    </div>
													    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
													        <div class="input-group">
													            <label class="radio-inline nopadding">Infants</label>
													            <input type="text" name="infants" class="form-control" value="{{ isset($lead->infant) ? $lead->infant : '' }}"  placeholder="Infants" min="1" max="10">
													        </div>
													    </div>
													    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
													        <div class="input-group">
													            <label class="radio-inline nopadding">Interested Date</label>
													            <input type="text" name="interested_date" class="form-control datepicker" value="{{ isset($lead->interested_date) ? date('d F Y', strtotime($lead->interested_date)) : '' }}" readonly="" placeholder="Interested Date">
													        </div>
													    </div>
													    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
													        <div class="input-group">
													            <label class="radio-inline nopadding">Hotel Category</label>
													            <select name="hotel_category" class="select-style">
													                <option value="">Category</option>
													                <?php if(isset($cost['amount']) && count($cost['amount']) > 0){?>
													                @foreach($cost['amount'] as $c => $cost_val)
													                @foreach($cost_val['rates'] as $r => $rate)
													                @if(isset($rate['sell']) && $rate['sell'] > 0)
													                <?php /*
													                <option value="{{$c}}" {{($c==1)?"selected":""}}>{{$cost_val['name']}}</option>
													                */ ?>
													                <option value="{{$c}}" {{ (isset($lead->hotel_category) && $lead->hotel_category == $c) ? 'selected' : '' }}>{{$cost_val['name']}}</option>
													                @endif
													                @break;
													                @endforeach
													                @endforeach
													                <?php } ?>
													            </select>
													        </div>
													    </div>
													</div>
													
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
															<div class="input-group">
																<label class="radio-inline nopadding">More Details</label>
																<textarea name="notes" class="form-control" placeholder="More Details">{{ (isset($lead->notes)) ? $lead->notes : '' }}</textarea>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<button class="btn btn-default btn-payment btn-desktop-right tour_submit_btn">SUBMIT</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
   </div>
  
   <!-- Search Compare Book Block Ends -->
   <!-- Hot Deals Begins-->
   <?php /*
   <div class="hot-deals">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                     <h3 class="hot-deals-title"><span class="hot">Hot</span> Deals in Domestic Holidays</h3>
                     <div class="row">
                        <div class="deal-blocks">
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/deal-1.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Kerala Tour</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/kashmir.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Kerala Tour</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                           <a href="#" class=" text-center">View More</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                     <h3 class="hot-deals-title"><span class="hot">Hot</span>Deals in International Holidays</h3>
                     <div class="row">
                        <div class="deal-blocks">
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/europe.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Europe</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/dubai.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Dubai</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                           <a href="#" class=" text-center">View More</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   */?>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
	// Pure Javascript
	document.getElementsByClassName("activity-details").onmouseover = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'block';
	}
	document.getElementsByClassName("activity-details").onmouseout = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'none';
	}
</script>
<script type='text/javascript'>
    $(function () {

        $('input[type=radio][class=holydayHotelRadio]').change(function () {
            var holyday = $(this).attr('holyday');
            var package = $(this).attr('package');

            $('div.cost-' + holyday).find('div.amount').addClass('hide');
            $('div.cost-' + holyday).find('div.package_cost_' + holyday + package).removeClass('hide');

            $('table.hotel_' + holyday + ' tr:gt(0)').addClass('hide');
            $('table.hotel_' + holyday + ' tr.package_' + holyday + package).removeClass('hide');

            var tr_length = $('table.hotel_' + holyday + ' tr.package_' + holyday + package).length;
            if(tr_length==0){
                $('table.hotel_' + holyday + ' tr.hotel_label').addClass('hide');
            }else{
                $('table.hotel_' + holyday + ' tr.hotel_label').removeClass('hide');
            }

            var inquire = $(this).parents('div.holiday-list-item').find('div.inquire a');
            if(inquire.length){
                var oldLink = inquire.attr('href');
                oldLink = oldLink.substr(0, oldLink.lastIndexOf('/'));
                inquire.attr('href', oldLink + "/" + package);
            }
        });
        $('.holiday-list-item').each(function (i, obj) {
            if (!$(obj).find('input[type=radio][class=holydayHotelRadio]:first').prop('checked')) {
                $(obj).find('input[type=radio][class=holydayHotelRadio]:first').prop("checked", true).trigger("change");
            }
        });
    });

</script>
<?php /*
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
<script>
	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		dots: true,
		responsiveClass: true,
<!-- navText : ['<i class="	fa fa-caret-left" aria-hidden="true"></i>','<i class="	fa fa-caret-right" aria-hidden="true"></i>'], -->
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 2,
				nav: false
			},
			1000: {
				items: 3,
				nav: false,
				loop: false,
				margin: 20

			}
		}
	})
</script>
*/?>
@endsection