
@extends('layouts.client')

@section('content')
<script type="text/javascript">
    cityData = '';
</script>
<style type="text/css" media="screen">
  .text-danger{
    display: inline-block !important;
    font-size: 14px !important;
  }

  .error {
    color: #a94442;
    display: inline-block !important;
    font-size: 14px !important;
    font-weight: normal !important;
  }
  
</style>
<?php $active_tab = ((session()->has('tab')) ? session()->get('tab') : 'view_profile'); ?>
<section>
    <div class="dashboard-tabs">
        <div class="dashboard-tabs-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <h2 class="dashboard-title">{{ (($user_data) ? $user_data->first_name : '') }} {{ (($user_data) ? $user_data->last_name : '') }}</h2>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <ul class="nav nav-pills pull-right">
                            <li><a href="{{ route('user_dashboard') }}" title="Dashboard">Dashboard</a></li>
                            <li><a href="{{ route('client_logout') }}" title="Logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="dashboard-tabs-content">
          <div class="container">
            <div class="row" style="padding: 20px 0px;">
              @if(session()->has('success_msg'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{ session()->get('success_msg') }}
                    </div>
                </div>
              @endif
              @if(session()->has('error_msg'))
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        {{ session()->get('error_msg') }}
                    </div>
                </div>
              @endif
              <div class="container bootstrap snippet">
                <div class="row">
                  <div class="col-sm-3"><!--left col-->
                    <div class="text-center">
                      <?php if($user_data->image!=null){  ?>
                        <img src="{{ (($user_data) ? env('APP_PUBLIC_URL', public_path()).'/uploads/user_profile/'. $user_data->image: '') }}" class="avatar img-circle" alt="avatar"  style="width: 220px; height:220px">
                      <?php }else{ ?>
                        <img src="{{ env('APP_PUBLIC_URL')}}client/images/fpo_avatar.png" class="avatar img-circle" alt="avatar" style="width: 220px; height:220px"/>
                      <?php }?>
                    </div>
                  </div>
                  <div class="col-sm-9">
                    <ul class="nav nav-pills">
                      <li class="<?php echo (($active_tab == 'view_profile' ) ? 'active' :''); ?> "><a data-toggle="pill" href="#view_profile">View Profile</a></li>
                      <li class="<?php echo (($active_tab == 'edit_profile' ) ? 'active' :''); ?> "><a data-toggle="pill" href="#edit_profile">Edit Profile</a></li>
                      <li class="<?php echo (($active_tab == 'change_password' ) ? 'active' :''); ?> "><a data-toggle="pill" href="#change_password">Change password</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane fade <?php echo (($active_tab == 'view_profile' ) ? 'active in' :''); ?>" id="view_profile">
                      <hr>
                      <table class="table">
                        <tbody>
                          <tr>
                            <td width="25%">First name</td>
                            <td width="75%">{{(isset($user_data->first_name) ? $user_data->first_name :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Last name</td>
                            <td>{{(isset($user_data->last_name) ? $user_data->last_name :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Email</td>
                            <td>{{(isset($user_data->email) ? $user_data->email :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Mobile</td>
                            <td>{{(isset($user_data->phone_no) ? $user_data->phone_no :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Landline Number</td>
                            <td>{{(isset($user_data->landline_no) ? $user_data->landline_no :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Age</td>
                            <td>{{(isset($user_data->age) ? $user_data->age :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Gender</td>
                            <td>{{ (($user_data && isset($user_data->gender) && $user_data->gender != '') ? ($user_data->gender == 'M') ? 'Male' : 'Female' : '') }}</td> 
                          </tr>
                          @if(isset($user_data->birth_date) && $user_data->birth_date!=null)
                          <tr>
                            <td>Birth Date</td>
                            <td>{{date('d M Y', strtotime($user_data->birth_date))}}</td> 
                          </tr>
                          @else
                          <tr>
                            <td>Birth Date</td>
                            <td></td> 
                          </tr>
                          @endif
                          <tr>
                            <td>Timezone</td>
                            <td>{{(isset($user_data->time_zone) ? $user_data->time_zone :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Address</td>
                            <td>{{(isset($user_data->address) ? $user_data->address :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Country</td>
                            <td>{{(isset($user_data->country_name) ? $user_data->country_name :'')}}</td> 
                          </tr>
                          <tr>
                            <td>State</td>
                            <td>{{(isset($user_data->state_name) ? $user_data->state_name :'')}}</td> 
                          </tr>
                          <tr>
                            <td>City</td>
                            <td>{{(isset($user_data->city_name) ? $user_data->city_name :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Pin Code</td>
                            <td>{{(isset($user_data->pin_code) ? $user_data->pin_code :'')}}</td> 
                          </tr>
                          <tr>
                            <td>Brief Description</td>
                            <td>{{(isset($user_data->information) ? $user_data->information :'')}}</td> 
                          </tr>
                        </tbody>
                      </table>
                      <hr>
                      </div><!--/tab-pane-->
                      <div class="tab-pane fade <?php echo (($active_tab == 'edit_profile' ) ? 'active in' :''); ?>" id="edit_profile">
                      <h2></h2>
                      <hr>
                      <form action="{{ route('user.profile.save') }}" name="user_frm" id="user_frm" class="" method="post" enctype="multipart/form-data">
                      {{ csrf_field() }}
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="first_name"><h4>First name *</h4></label>
                              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name" value="{{ isset($users[0]->first_name) ? $users[0]->first_name : '' }}" title="enter your first name if any.">
                              <span class="text-danger">{{ $errors->first('first_name') }}</span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="last_name"><h4>Last name *</h4></label>
                              <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name" title="enter your last name if any." value="{{ isset($users[0]->last_name) ? $users[0]->last_name : '' }}" >
                              <span class="text-danger">{{ $errors->first('last_name') }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="email"><h4>Email *</h4></label>
                              <input type="text" class="form-control" name="email" id="email" placeholder="Enter email" title="enter your email."  value="{{ isset($users[0]->email) ? $users[0]->email : '' }}" {{ isset($users[0]->email) ? 'readonly' : '' }}>
                              <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="image"><h4>Profile</h4></label>
                              <input type="file" id="input-file-a" name="image"  class="form-control-file" data-default-file="{{  isset($users[0]->image) ?  env('APP_PUBLIC_URL').'uploads/user_profile/'.$users[0]->image : '' }}" />
                              <span class="text-danger"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="phone_no"><h4>Mobile *</h4></label>
                              <input type="text" class="form-control" name="phone_no" id="mobile" placeholder="Enter mobile number" title="enter your mobile." value="{{ isset($users[0]->phone_no) ? $users[0]->phone_no : '' }}">
                              <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="landline_no"><h4>Landline Number</h4></label>
                              <input type="text" class="form-control" name="landline_no" id="landline_no" placeholder="Enter landline number" title="enter your landline number." value="{{ isset($users[0]->landline_no) ? $users[0]->landline_no : '' }}">
                              <span class="text-danger">{{ $errors->first('landline_no') }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="birth_date"><h4>Birth Date *</h4></label>
                              <input type="text" id="birth_date" name="birth_date" class="form-control" readonly=""  autocomplete="off" value="{{ isset($users[0]->birth_date) ? date('d F Y',strtotime($users[0]->birth_date)) : '' }}"  >
                              <span class="text-danger"></span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="age"><h4>Age *</h4></label>
                              <input type="text" class="form-control" name="age" id="age" placeholder="Enter age" title="enter your age." value="{{ isset($users[0]->age) ? $users[0]->age : '' }}">
                              <span class="text-danger">{{ $errors->first('age') }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="gender"><h4>Choose Gender *</h4></label>
                              <select class="form-control select-style errow-size" name="gender">
                              <option value="" selected>Choose Gender</option>
                              <option value="M" {{ (isset($users[0]->gender) && $users[0]->gender == "M") ? 'selected' : '' }}>Male</option>
                              <option value="F" {{ (isset($users[0]->gender) && $users[0]->gender == "F") ? 'selected' : '' }}>Female</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="time_zone"><h4>Choose Timezone</h4></label>
                              <select class="form-control select-style errow-size" id="choose_timezone" name="time_zone">
                              <option value="" selected>Choose Timezone</option>
                              @if(isset($timezones))
                              @foreach($timezones as $t=>$zone)
                              <option value="{{ $t }}" {{ (isset($users[0]->time_zone) && $users[0]->time_zone == $t) ? 'selected' : '' }}>{{ $zone }}</option>
                              @endforeach
                              @endif
                              </select>
                              <span class="text-danger">{{ $errors->first('time_zone') }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="country_id"><h4>Choose Country *</h4></label>
                              <select class="form-control select-style errow-size"  name="country_id" id="country_id">
                              <option value="" selected>Choose Country</option>
                              @if(isset($countrys))
                              @foreach($countrys as $country)
                              <option value="{{ $country->country_id }}" {{ (isset($users[0]->country_id) && $users[0]->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
                              @endforeach
                              @endif
                              </select>
                              <span class="text-danger">{{ $errors->first('country_id') }}</span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="state_id"><h4>Choose State *</h4></label>
                              <select class="form-control select-style errow-size"  name="state_id" id="state_id">
                              <option value="" selected>Choose State</option>
                              @if(isset($states))
                              @foreach($states as $state)
                              <option value="{{ $state->state_id }}" {{ (isset($users[0]->state_id) && $users[0]->state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name}}</option>
                              @endforeach
                              @endif
                              </select>
                              <span class="text-danger">{{ $errors->first('state_id') }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="city_id"><h4>Choose City *</h4></label>
                              <select class="form-control select-style errow-size"  name="city_id" id="city_id">
                              <option value="" selected>Choose City</option>
                              @if(isset($cities))
                              @foreach($cities as $city)
                              <option value="{{ $city->city_id }}" {{ (isset($users[0]->city_id) && $users[0]->city_id == $city->city_id) ? 'selected' : '' }}>{{ $city->city_name}}</option>
                              @endforeach
                              @endif
                              </select>
                              <span class="text-danger">{{ $errors->first('city_id') }}</span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="pin_code"><h4>Pin Code *</h4></label>
                              <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Enter pin code" title="enter pin code." value="{{ isset($users[0]->pin_code) ? $users[0]->pin_code : '' }}" />
                              <span class="text-danger">{{ $errors->first('pin_code') }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="address"><h4>Address *</h4></label>
                              <textarea id="address" cols="30" rows="5" class="form-control" name="address" style="height:auto">{{ isset($users[0]->address) ? $users[0]->address : '' }}</textarea>
                              <span class="text-danger"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="information"><h4>Brief Description</h4></label>
                              <textarea id="information" cols="30" rows="5" class="form-control" name="information"  style="height:auto">{{ isset($users[0]->information) ? $users[0]->information : '' }}</textarea>
                              <span class="text-danger"></span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-12">
                          <br>
                          <button type="submit" class="btn btn-default btn-payment btn-desktop-right flight_submit_btn">SUBMIT</button>
                          </div>
                        </div>
                      </form>
                      </div><!--/tab-pane-->
                      <div class="tab-pane fade <?php echo (($active_tab == 'change_password' ) ? 'active in' :''); ?>" id="change_password">
                      <hr>
                      <form action="{{ route('user.change.password') }}" name="user_password" id="user_password" class="" method="post" enctype="multipart/form-data">
                      {{ csrf_field() }}
                        <div class="form-group">
                          <div class="col-xs-12">
                          <label class="radio-inline nopadding col-md-12"  for="current-password"><h4>Current password *</h4></label>
                          <input type="password" class="form-control col-md-12" value="{{ old('current-password')}}" name="current-password" id="current-password" placeholder="Current password" title="enter your current password.">
                          <span class="text-danger col-md-12" style="padding: 0px" >{{ $errors->first('current-password') }}{{ (session()->has('old_pass_error'))? session()->get('old_pass_error') : '' }}</span> 
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-12">
                          <label class="radio-inline nopadding col-md-12" for="password"><h4 style="margin-top: 15px">New password *</h4></label>
                          <input type="password" class="form-control col-md-12" value="{{ old('password')}}" name="password" id="password" placeholder="New password" title="enter your new password.">
                          <span class="text-danger col-md-12" style="padding: 0px" >{{ $errors->first('password') }}</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-12">
                          <label class="radio-inline nopadding col-md-12" for="password_confirmation"><h4 style="margin-top: 15px">Conform password *</h4></label>
                          <input type="password" class="form-control col-md-12" value="{{ old('password_confirmation')}}" name="password_confirmation" id="password_confirmation" placeholder="Conform password" title="enter your conform password.">
                          <span class="text-danger col-md-12" style="padding: 0px" >{{ $errors->first('password_confirmation') }}</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-12">
                          <br>
                          <button type="submit" class="btn btn-default btn-payment btn-desktop-right flight_submit_btn">SUBMIT</button>
                          </div>
                        </div>
                      </form>
                      </div>
                    </div><!--/tab-pane-->
                  </div><!--/tab-content-->
                </div><!--/col-9-->
              </div><!--/row-->

            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {


  $("form#user_frm").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            email: {
                required: true,
                email: true
            },
            phone_no: "required",
            age:"required",
            gender:"required",
            address: "required",
            country_id: "required",
            state_id: "required",
            city_id: "required",
            pin_code: "required",
            status: "required"
        },
        messages: {
            first_name: "Please enter firstname",
            last_name: "Please enter lastname",
            email: "Please enter a valid email address",
            phone_no: "Please enter phone number",
            age:"Please enter age",
            gender:"Please select gender",
            address: "Please enter address",
            country_id: "Please select country",
            state_id: "Please select state",
            city_id: "Please select city",
            pin_code: "Please enter pin code",
            status: "Please select status"
        },
        // errorPlacement: function (error, element) {
        //     if (element.attr("type") == "radio") {
        //         error.insertBefore(element);
        //     } else {
        //         error.insertAfter($(element).parents('div.input-field'));
        //     }
        // },
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
  });

  $("form#user_password").validate({
    rules: {
        password: {
            minlength: 6
        },
        password_confirmation: {
            equalTo: "#password"
        },
    },
    messages: {
        password: {
            minlength: "Your password must be at least 6 characters long"
        },
        password_confirmation: {
            equalTo: "Enter Confirm Password Same as Password"
        },
    },
    submitHandler: function (form) {
        form.submit();
    }
  });
     
    $('#country_id').change(function () {
        var country_id = $(this).val();
        var url = '{{ route("state.edit.profile", ":country_id") }}';
        url = url.replace(':country_id', country_id);
        $.ajax({
            url: url,
            type: 'GET',
            data: {country_id:country_id},
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#state_id')
                        .find('option')
                        .remove();
                $('#state_id').append('<option value="">Choose State</option>');
                $.map(data, function (item) {
                    $('#state_id').append('<option value="' + item.state_id + '">' + item.state_name + '</option>');
                });
                $("#state_id").trigger("chosen:updated");
                $("#state_id").trigger("liszt:updated");
            }
        });
        
    });
    $('#state_id').change(function () {
        var state_id = $(this).val();
        var url = '{{ route("city.edit.profile", ":state_id") }}';
        url = url.replace(':state_id', state_id);
        $.ajax({
            url: url,
            type: 'GET',
            data: {state_id:state_id},
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#city_id')
                        .find('option')
                        .remove();
                $('#city_id').append('<option value="">Choose City</option>');
                $.map(data, function (item) {
                    $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
                });
                $("#city_id").trigger("chosen:updated");
                $("#city_id").trigger("liszt:updated");
            }
        });
    });
});
</script>
@endsection
