@extends('layouts.client')
<script>
	var cityData = <?= $cities ?>;
</script>
@section('content')
<section>
   <div id = "myTabContent" class = "tab-content">
      <div  class="tab-pane fade in active" id="customizes-block" >
            <div class="customize">
                <div class="container">
                    <div class="row">
                        <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="tab-content">
									<div id="visa" class="tab-pane fade active in">
										<div class="booking-block  border-1">
											<div class="booking-block-title p-t-b-5">
												<h3>Visa Details</h3>
											</div>
											<div class="holiday-booking-flight booking-block-form">
												<form name="visa_frm_update" id="visa_frm_update" action="" method="post" onsubmit="return false;">
													{{csrf_field()}}
													<input type="hidden" name="id" value="{{ isset($lead->id) ? $lead->id : '' }}" />
													<div class="row booking-form-padding" style="display: none;">
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<ul class="validate-msg">

															</ul>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Nationality</label>
																<input type="text" name="where_live" class="form-control autocomplete" value="{{ isset($lead->fromCity->city_name) ? $lead->fromCity->city_name : old('where_live') }}" placeholder="Nationality">
																<input type="hidden" name="where_live_id" value="{{ isset($lead->from_city) ? $lead->from_city : old('where_live_id') }}">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Where do you want to go?</label>

																<input type="text" name="where_go" class="form-control autocomplete" value="{{ isset($lead->toCity->city_name) ? $lead->toCity->city_name : old('where_go') }}" placeholder="Where do you want to go?">
																<input type="hidden" name="where_go_id" value="{{ isset($lead->to_city) ? $lead->to_city : old('where_go_id') }}">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Travel Date</label>
																<input type="text" name="travel_date" class="form-control datepicker" value="{{ isset($lead->deprature_date) ? date('d F Y', strtotime($lead->deprature_date)) : '' }}" readonly="" placeholder="Travel Date">
															</div>	
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Visa Type</label>
																<select name="visa_type" class="select-style">
																	<option value="">Visa Type</option>
																	@foreach(get_visa_type() as $key => $visa_type)
																	<option value="{{$key}}" {{ (isset($lead->visa_type) && $lead->visa_type == $key) ? 'selected' : '' }}>{{$visa_type}}</option>
																	@endforeach
																</select>
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Number of Entries</label>
																<select name="number_of_entries" class="select-style">
																	<option value="">Number of Entries</option>
																	@foreach(get_number_of_entries() as $key => $visa_type)
																	<option value="{{$key}}" {{ (isset($lead->number_of_entries) && $lead->number_of_entries == $key) ? 'selected' : '' }}>{{$visa_type}}</option>
																	@endforeach
																</select>
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Duration Days</label>
																
																<input type="text" name="duration_days" class="form-control" value="{{ isset($lead->duration_days) ? $lead->duration_days : '' }}" placeholder="Duration Days" min="1" max="10">
															</div>	
														</div>
													</div>
													<div class="row booking-form-padding sub-title">
														<label class="radio-inline">Number of Guests</label>
													</div>
													<?php /*
													<div class="row booking-form-padding sub-title">
													<label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
													</div> 
													*/?>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<label class="radio-inline nopadding">Adults</label>
																<input type="text" name="adults" class="form-control" value="{{ (isset($lead->adult)) ? $lead->adult : '' }}" placeholder="Adults" >
															</div>
														</div>
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<label class="radio-inline nopadding">Child</label>
																<input type="text" name="child" class="form-control" value="{{ isset($lead->child) ? $lead->child : '' }}" placeholder="Kids" >
															</div>
														</div>
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<label class="radio-inline nopadding">Infants</label>
																<input type="text" name="infants" class="form-control" value="{{ isset($lead->infant) ? $lead->infant : '' }}" placeholder="Infants" >
															</div>
														</div>
													</div>

													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Name</label>
																<input type="text" name="contact_name" class="form-control" value="{{ (isset($lead->name)) ? $lead->name : '' }}" placeholder="Name">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Contact No</label>
																<input type="text" name="contact_no" class="form-control onlynumber" value="{{ (isset($lead->phone)) ? $lead->phone : '' }}" placeholder="Contact No" maxlength="10">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<label class="radio-inline nopadding">Email Id</label>
																<input type="text" name="email_id" class="form-control" value="{{ (isset($lead->email)) ? $lead->email : '' }}" placeholder="Email Id">
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
															<div class="input-group">
																<label class="radio-inline nopadding">More Details</label>
																<textarea name="notes" class="form-control" placeholder="More Details">{{ (isset($lead->notes)) ? $lead->notes : '' }}</textarea>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<button class="btn btn-default btn-payment btn-desktop-right visa_submit_btn">UPDATE</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
   </div>
   
</section>
@endsection
@section('javascript')
<script type="text/javascript">
	// Pure Javascript
	document.getElementsByClassName("activity-details").onmouseover = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'block';
	}
	document.getElementsByClassName("activity-details").onmouseout = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'none';
	}
</script>
<?php /*
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
<script>
	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		dots: true,
		responsiveClass: true,
<!-- navText : ['<i class="	fa fa-caret-left" aria-hidden="true"></i>','<i class="	fa fa-caret-right" aria-hidden="true"></i>'], -->
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 2,
				nav: false
			},
			1000: {
				items: 3,
				nav: false,
				loop: false,
				margin: 20

			}
		}
	})
</script>
*/?>
@endsection