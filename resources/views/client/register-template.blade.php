<html>
<head></head>
  <body>
    <div style="background:#ecf0f1;width: 500px;font-family: Arial, Helvetica, sans-serif;">
      <div style="width: 500px; background-color:#fefefe;font-family: Arial, Helvetica, sans-serif;">
        <div >
          <h4>Hello {{$first_name}} {{$last_name}}</h4>
          <br/>
          <p><a href="{{route('activation_user',[$activation_token])}}">Follow this link to activation your account.</a></p>
          <p style="margin-left: 10px;">Thank you for registering to the Low Cost Trip. 
            If you need any help please email the info@lowcosttrip.com or call them on  +91 81 82 83 84 85.
          </p>
        </div>
      </div>
    </div>
  </body>
</html>