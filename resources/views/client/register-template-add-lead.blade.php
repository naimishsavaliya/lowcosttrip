

<!DOCTYPE html>
<html lang="en" >
   <head>
      <meta charset="UTF-8">
   </head>
   <body>
      <head>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
      </head>
      <body style="width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
         <table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="margin:0; padding:0; width:100% !important; line-height: 100% !important; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;background-color: #F9FAFB;" width="100%">
            <tbody>
               <tr>
                  <td width="10" valign="top">&nbsp;</td>
                  <td valign="top" align="center">
                     <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; max-width: 600px; background-color: #FFF; border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;" id="contentTable">
                        <tbody>
                           <tr>
                              <td width="600" valign="top" align="center" style="border-collapse:collapse;">
                                 <table align="center" border="0" cellpadding="0" cellspacing="0" style="background: #F9FAFB;" width="100%">
                                    <tbody>
                                       <tr>
                                          <td align="center" valign="top">
                                             <div style="font-family: &quot;lato&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 28px;">&nbsp;</div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <table align="center" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #E0E4E8;" width="100%">
                                    <tbody>
                                       <tr>
                                          <td align="center" style="padding: 56px 56px 28px 56px;" valign="top">
                                             <a style="color: #3498DB; text-decoration: none;" href="https://lowcosttrip.com" target="_blank"><img style="border: 0;" alt="LCT" src="https://www.lowcosttrip.com/public/new-theme/images/logo.jpg">
                                             </a>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td align="center" style="padding: 0 56px 28px 56px;" valign="top">
                                             <div style="font-family: &quot;lato&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 28px;font-size: 20px; color: #333;">
                                                   Thank you for choosing <strong>Low Cost Trip</strong>.Your Login Email-id and password are bellow<br>
                                                  
                                                </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td align="center" style="padding: 0 56px 28px 124px;" valign="top">
                                             <div style="font-family: &quot;lato&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 28px;font-size: 20px; color: #333;text-align: left">
                                                   <strong>
                                                   Email-id: {{$user_info->email}}<br>
                                                   Password: {{$user_profile->phone_no}}
                                                   </strong> 
                                                </div>
                                          </td>
                                       </tr>
                                       <?php /*
                                       <tr>
                                          <td align="center" style="padding: 0 56px; padding-bottom: 56px;" valign="top">
                                             <div>
                                                <a href="#" style="background-color:#E15718;border-radius:50px;color:#ffffff;display:inline-block;font-family: 'lato', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:18px;font-weight: bold;line-height:40px;text-align:center;text-decoration:none;width:270px;-webkit-text-size-adjust:none;" target="_blank">Confirm subscription now</a>
                                             </div>
                                           
                                          </td>
                                       </tr> */ ?>
                                       <tr>
                                          <td align="center" bgcolor="#F9FAFB" style="padding: 28px 56px;" valign="top">
                                             <div style="font-family: &quot;lato&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 28px;font-size: 16px; color: #666666; font-weight: 900;">Invite your friends to subscribe:</div>
                                             <div style="font-family: &quot;lato&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 28px;font-size: 16px; color: #000;">
                                                <a style="color: #55ACEE;" target="_blank" href="#">Invite via Twitter</a>
                                                &nbsp;&nbsp;
                                                <a style="color: #3B5998;" target="_blank" href="#">Invite via Facebook</a>
                                             </div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
                  <td width="10" valign="top">&nbsp;</td>
               </tr>
            </tbody>
         </table>
   </body>
   </body>
</html>

