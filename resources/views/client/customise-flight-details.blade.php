<div class="cart">  
    <?php if(isset($response->Data->Cart->OrderItems)){ ?>  
    <form name="bookingForm" id="bookingForm" action="" method="post" onsubmit="return false;">
    {{csrf_field()}}
    <input type="hidden" name="SearchFormData" value="<?php echo ( isset($response->Data->SearchFormData) ? $response->Data->SearchFormData : ''); ?>">   
    <input type="hidden" name="CartData" value="<?php echo ( isset($response->Data->CartData) ? $response->Data->CartData : ''); ?>">   
    <input type="hidden" name="CartBookingId" value="<?php echo ( isset($response->Data->Cart->CartBookingId) ? $response->Data->Cart->CartBookingId : ''); ?>">           
    <div class='col-md-9 search-result'>
        <h3>Review</h3>
        <div class='cart-item'>            
            <ul>                              
                <?php 
                    $flights = $response->Data->Cart->OrderItems;
                    foreach($flights as $key => $row){                
                        $item = $row->FlightSegment; 
                        $item_PriceInfo = $response->Data->Cart->OrderItems[0]->PriceInfo;                             
                        if($key>0){ ?>
                        <li class="changeplane-span">
                            <?php                        
                            // Get waiting time 
                            $dteStart = new DateTime(date('Y-m-d H:i:s', strtotime( str_replace('/', '-',$flights[$key-1]->FlightSegment->ArrivalTime))));
                            $dteEnd   = new DateTime(date('Y-m-d H:i:s', strtotime( str_replace('/', '-',$item->DepartureTime) ))); 
                            $dteDiff  = $dteStart->diff($dteEnd); 
                            ?>
                            Plane change at <?php echo $item->DepartureAirport ?> | Waiting: <?php echo $dteDiff->format("%H").'h '.$dteDiff->format("%i").'m';  ?>
                        </li>
                        <?php } ?>
                        <h3><?php 
                            $date = new DateTime(str_replace("/","-", $item->DepartureTime));
                            echo $date->format('d M, Y');                            
                        ?></h3>
                        <li class="flight-row" id="row">
                            <div class="name">                        
                                <?php echo $item->OperatingAirline ?>
                                <span><?php echo $item->CarrierCode." ".$item->FlightNumber ?></span>
                            </div>    
                            <div class="flight-details">
                                <ul>
                                    <li>
                                        <span class="time"><?php
                                        $DepartureTime = str_replace("/", "-",$item->DepartureTime);
                                        echo date("H:i",strtotime($DepartureTime)); 
                                        
                                        ?></span>
                                        <?php echo $item->DepartureAirport ?>
                                    </li>
                                    <li>
                                        <span class="time"><?php 
                                            $ArrivalTime = str_replace("/", "-",$item->ArrivalTime);
                                            echo date("H:i",strtotime($ArrivalTime)); 
                                        ?></span>
                                        <?php echo $item->ArrivalAirport ?> 
                                    </li>
                                    <li>
                                        <?php 
                                        $minutes = $item->JourneyTime;
                                        $hours = floor($minutes / 60);
                                        $min = $minutes - ($hours * 60);
                                        $count_stop =(  (isset($item->StopoverCodes))  ? count($item->StopoverCodes) : 0);
                                        ?>
                                        <span class="time"><?php echo $hours."h ".$min.'m' ?></span>
                                        <?php echo $count_stop == 0 ? "Non stop" : $count_stop.' stop' ; ?>
                                    </li>                                    
                                </ul>
                            </div>                    
                        </li>                
                <?php } ?>
            </ul>                          
        </div>           
        <div class="cart-option">                        
            <div class="row">
                <div class="form-group col-md-4">                        
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" placeholder="Enter your email address" required>
                </div>    
                <div class="form-group col-md-4">                                        
                    <label>Mobile</label>
                    <input type="text" class="form-control" name="mobile" placeholder="Enter your mobile number" required>
                </div>                            
            </div>
            <?php $passengers = $response->Data->Cart->OrderItems[0]->Passengers;  ?>
            <ul class="nav nav-tabs">
                <?php $adults = 1;$child=1;$infants=1; 
                foreach($passengers as $key=>$passenger) { ?>                                    
                    <?php if($passenger->Type=="A"){ $name="adults".$adults; ?>
                            <li class="<?php echo $key==0 ? 'active' : '' ?>"><a href="#<?php echo $name; ?>" data-toggle="tab"><?php echo "Adults ".$adults; ?></a></li>
                    <?php $adults++; }else if($passenger->Type=="C"){ $name="child".$child; ?>
                            <li><a href="#<?php echo $name; ?>" data-toggle="tab"><?php echo "Child ".$child; ?></a></li>
                    <?php $child++; }else if($passenger->Type=="I"){ $name="infants".$infants; ?>
                            <li><a href="#<?php echo $name; ?>" data-toggle="tab"><?php echo "Infants ".$infants; ?></a></li>
                    <?php $infants++; } ?>                            
                <?php } ?>
            </ul>
            <div class="tab-content ">
                <?php $adults = 1;$child=1;$infants=1; 
                $age_required = $response->Data->Cart->IsRequiresAgeForAdult;
                foreach($passengers as $key=>$passenger) { ?>
                    <?php if($passenger->Type=="A"){ $name="adults".$adults; $adults++; $age_required=='Y' ? $col=4 : $col=3;  ?>
                    <?php }else if($passenger->Type=="C"){ $name="child".$child; $child++; $col=3;   ?>
                    <?php }else if($passenger->Type=="I"){ $name="infants".$infants; $infants++; $col=3;  } ?>
                    <div class="tab-pane <?php echo $key==0 ? 'active' : '' ?>" id="<?php echo $name ?>">                
                        <div class="row">                            
                            <div class="form-group col-md-<?php echo $col; ?>">                        
                                <input type="text" class="form-control" name="firstname[<?php echo $name ?>]" placeholder="First Name" required>
                            </div>    
                            <div class="form-group col-md-<?php echo $col; ?>">                                        
                                <input type="text" class="form-control" name="lastname[<?php echo $name ?>]" placeholder="Last Name" required>
                            </div>                
                            <?php if($passenger->Type=="C" || $passenger->Type=="I" || $age_required=="Y"){ ?>
                            <div class="form-group col-md-<?php echo $col; ?>">                                        
                                <input type="text" class="form-control date-field datepicker" name="dob[<?php echo $name ?>]" placeholder="Date Of Birth (DD/MM/YYY)" readonly required>
                            </div>                
                            <?php } ?>
                            <div class="form-group radio-group col-md-<?php echo $col; ?>">                                        
                                <label><input type="radio" checked name="sex[<?php echo $name ?>]" value="M">Male</label>
                                <label><input type="radio" name="sex[<?php echo $name ?>]" value="F">Female</label>
                            </div>                                                
                        </div>      
                        <?php $meals = $response->Data->Cart->OrderItems[0]->Prefrences->MealPrefrences;  
                        if(isset($meals[0]->PrefList[0]) && !empty($meals[0]->PrefList[0])){ ?>
                        <div class="row options-row">
                            <div class="col-md-12">                        
                                <h3>Meals</h3>
                                <ul>                                    
                                    <?php foreach($meals as $meal){ if($passenger->Type==$meal->PaxType){
                                        foreach($meal->PrefList as $row){
                                        ?>
                                    <li>
                                        <div class="col-md-6 descrition"><?php echo $row->Description; ?></div>
                                        <?php $r_m_p =  str_replace("," , "" , $row->Price); ?>
                                      
                                        <div class="col-md-3 price"><?php echo $r_m_p; ?></div>
                                        <div class="col-md-3"><input type="radio" class="meals-price" data-price ="<?php echo $r_m_p; ?>" name="meals[<?php echo $name ?>]" value="<?php echo $row->Code; ?>"></div> 

                                        <?php /*
                                        <?php $r_m_p = rand(10,99); ?>
                                        <div class="col-md-3 price"><?php echo $r_m_p; ?></div>
                                        <div class="col-md-3"><input type="radio" class="meals-price" data-price ="<?php echo $r_m_p; ?>" name="meals[<?php echo $name ?>]" value="<?php echo $row->Code; ?>"></div>  */ ?>


                                    </li>
                                    <?php } } } ?>
                                </ul>                                
                            </div>    
                        </div>    
                        <?php } ?>
                        <?php $seats = $response->Data->Cart->OrderItems[0]->Prefrences->SeatPrefrences;  
                        if(isset($seats[0]->PrefList[0]) && !empty($seats[0]->PrefList[0]) && $passenger->Type!="I"){ ?>                        
                        <div class="row options-row">
                            <div class="col-md-12">
                                <h3>Seats</h3>
                                <ul>                                
                                    <?php foreach($seats as $seat){ if($passenger->Type==$seat->PaxType){
                                        foreach($seat->PrefList as $row){
                                        ?>
                                    <li>
                                        <div class="col-md-6 descrition"><?php echo $row->Description; ?></div>
                                        <?php $r_s_p = str_replace("," , "" , $row->Price); ?>
                                       
                                        <div class="col-md-3 price"><?php echo $r_s_p; ?></div>
                                        <div class="col-md-3"><input type="radio" class="seats-price" data-price ="<?php echo $r_s_p; ?>" name="seats[<?php echo $name ?>]" value="<?php echo $row->Code; ?>"></div>
                                         <?php /*
                                        <?php $r_s_p = rand(10,99); ?>
                                        <div class="col-md-3 price"><?php echo $r_s_p; ?></div>
                                        <div class="col-md-3"><input type="radio" class="seats-price" data-price ="<?php echo $r_s_p; ?>" name="seats[<?php echo $name ?>]" value="<?php echo $row->Code; ?>"></div> */ ?>


                                    </li>
                                    <?php } } } ?>
                                </ul>                                
                            </div>    
                        </div>                            
                        <?php } ?>
                        <?php $buggages = $response->Data->Cart->OrderItems[0]->Prefrences->BaggagePrefrences;  
                        if(isset($buggages[0]->PrefList[0]) && !empty($buggages[0]->PrefList[0])){ ?>                                                
                        <div class="row options-row">
                            <div class="col-md-12">                        
                                <h3>Buggage</h3>
                                <ul>                                
                                    <?php foreach($buggages as $buggage){ if($passenger->Type==$buggage->PaxType){
                                        foreach($buggage->PrefList as $row){
                                        ?>
                                    <li>
                                        <div class="col-md-6 descrition"><?php echo $row->Description; ?></div>
                                        <?php $r_b_p = str_replace("," , "" , $row->Price); ?>
                                        
                                        <div class="col-md-3 price"><?php echo $r_b_p; ?></div>
                                        <div class="col-md-3"><input type="radio" class="buggage-price" data-price ="<?php echo  $r_b_p; ?>" name="buggage[<?php echo $name ?>]" value="<?php echo $row->Code; ?>"></div> 
                                        <?php /*
                                        <?php $r_b_p = rand(10,99); ?>
                                        <div class="col-md-3 price"><?php echo $r_b_p; ?></div>
                                        <div class="col-md-3"><input type="radio" class="buggage-price" data-price ="<?php echo $r_m_p; ?>" name="buggage[<?php echo $name ?>]" value="<?php echo $row->Code; ?>"></div> */ ?>
                                    </li>
                                    <?php } } } ?>
                                </ul>                                
                            </div>    
                        </div>                                                    
                        <?php } ?>
                    </div>                     
                <?php } ?>
            </div>               
        </div>    
    </div>
    <div class='col-md-3'>
        <h3>Summary</h3>
        <div class='cart-total'>        
            <?php 
                $base_price = $response->Data->Cart->PriceDetails->BasePrice; 
                $total_price = $response->Data->Cart->PriceDetails->TotalPrice;             
                $customtax = $response->Data->Cart->PriceDetails->CustomTaxDetails; 
            ?>
            <ul>
                <li>
                    <span class='label'>Base Fare</span>
                    <span class='value'><?php echo $base_price ?></span>
                </li>

                <li class="hide">
                    <span class='label'>Meals</span>
                    <span class='value meals-price'><?php echo '0.00' ?></span>
                </li>

                <li class="hide">
                    <span class='label'>Seats</span>
                    <span class='value seats-price'><?php echo '0.00' ?></span>
                </li>

               
                <div id="demo" class="collapse">
                    <?php $tax = 0; foreach ($customtax as $key => $value){
                        if(!empty($value) && $value != "0.0" &&   $key  != 'TGSTTax' ){
                            $tax +=  str_replace("," , "" , $value);
                        ?>
                        <li>
                            <span class="label"><?php echo $key ?></span>
                            <span class="value"><?php echo $value ?></span>
                        </li>      
                    <?php } } ?>
                </div>
                <li data-toggle="collapse" data-target="#demo">
                    <span class="label"><strong>Total TAX</strong></span>
                    <span class="value"><strong>{{ number_format( $tax,2 )}}</strong></span>
                </li> 

                <li>
                    <span class='label'><strong>Grand Total</strong></span>
                    <?php  $total_price =  str_replace("," , "" , $total_price); ?>
                    <strong><span class='value grand-total'><?php echo number_format($total_price , 2) ?></span></strong>
                </li>            
            </ul>
        </div>        
        <br><br>
        <button class="btn btn-primary" type="submit">Book Now</button>
    </div>
    <input type="hidden" id="total_price" value="{{ str_replace(',' , '' , $response->Data->Cart->PriceDetails->TotalPrice)}}">
    <input type="hidden" id="grand_total_price" name="total_price" value="{{$response->Data->Cart->PriceDetails->TotalPrice}}">
    </form>
    <?php  }else{ 
        echo "<h3 class='text-center text-danger' style='padding:15px'>Unable To Fetch Air Reprice Info</h3>";
    } ?>
</div> 