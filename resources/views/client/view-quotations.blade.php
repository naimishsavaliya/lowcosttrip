
@extends('layouts.client')

@section('content')
<script type="text/javascript">
    cityData = '';
</script>
<section>

    <style>

        .badge-bg-draft{background-color: #ffa000; border-color: #ffa000;}
        .badge-bg-sent{background-color: #0097a7; border-color: #0097a7;}
        .badge-bg-approved{background-color: #7cb342; border-color: #7cb342;}
        .badge-bg-rejected{background-color: #e53935; border-color: #e53935;}
    </style>
    <div class="dashboard-tabs">
        <div class="dashboard-tabs-heading">
            <div class="container">
                <div class="row">
                   
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <h2 class="dashboard-title">Quotations</h2>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <ul class="nav nav-pills pull-right">
                            <li><a href="{{ route('user_dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ route('user.profile.page') }}" title="Profile">Profile</a></li>
                            <li><a href="{{ route('client_logout') }}" title="Logout">Logout</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <input type="hidden" name="lead_id_for_quotation" id="lead_id_for_quotation" value="<?php echo $lead_info->id;?>">
        <div class="dashboard-tabs-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive" style="margin-top: 25px"> 
                              <?php /*      
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>

                                        <th>Sr. No</th>
                                        <th>Created Date</th>
                                        <th>Quotation Title</th>
                                        <th>Adult/Child/Infant</th>
                                        <th>Amount Agent</th>
                                        <th>Status</th>
                                        <th>Actions</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if (isset($quotation) && count($quotation) > 0) {
                                        foreach ($quotation as $k => $qto) {
                                            echo '<tr>';
                                            echo '<td>' . ($k + 1) . '</td>';
                                            echo '<td>' . date("d M Y", strtotime($qto->created_at)) . '</td>';
                                            echo '<td>' . $qto->subject . '</td>';
                                            echo '<td>' . $qto->adult . ' / ' . $qto->child . ' / ' . $qto->infant . '</td>';
                                            echo '<td>' . number_format($qto->total_amount, 2) . '</td>';
                                            echo '<td>';
                                            if ($qto->status_id == 1) {
                                                echo '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $qto->quaId . '" data-id="' . $qto->status_id . '" data-toggle="modal"  data-target="#quotation_update_status"><span class="text-warning">Draft</span></a>';
                                            } elseif ($qto->status_id == 2) {
                                                echo '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $qto->quaId . '" data-id="' . $qto->status_id . '" data-toggle="modal"  data-target="#quotation_update_status"><span class="text-info">Quote Sent</span></a>';
                                            } elseif ($qto->status_id == 3) {
                                                echo '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $qto->quaId . '" data-id="' . $qto->status_id . '" data-toggle="modal"  data-target="#quotation_update_status"><span class="text-success">Quote Approved</span></a>';
                                            } elseif ($qto->status_id == 4) {
                                                echo '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $qto->quaId . '" data-id="' . $qto->status_id . '" data-toggle="modal"  data-target="#quotation_update_status"><span class="text-danger">Quote Rejected</span></a>';
                                            } else {
                                                echo '-';
                                            }
//                                            $qto->quaId
                                            echo '</td>'; ?>
                                            <td><a href="{{route('quotation_info', [$qto->lead_id,$qto->quaId])}}" target="_blank" ><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                            <?php
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            */?>
                            
                            <table class="table table-bordered" id="quotation_tbl" style="margin-top: 20px;" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:25px;">ID</th>
                                        <th>Date</th>
                                        <th>Quotation Title</th>
                                        <th>Adult/Child/Infant</th>
                                        <th>Amount</th>
                                        <th>Quote By</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <!-- <tfoot>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <input placeholder="ID" type="text" class="form-control" colPos="0" style="width:80px;">
                                                <span class="md-input-bar "></span>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>
                                            <div class="form-group">
                                                <input placeholder="Title" type="text" class="form-control" colPos="2" style="width:80px;">
                                                <span class="md-input-bar "></span>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="form-group">
                                                <input placeholder="Agent" type="text" class="form-control" colPos="5" style="width:80px;">
                                                <span class="md-input-bar "></span>
                                            </div>
                                        </td>
                                        <td>
                                            <select class="form-control" data-uk-tooltip="{pos:'bottom'}" title="SChoose a Status" colPos="6" style="width:80px;">
                                                <option value="">Status</option>
                                                <option value="3">Quote Approved</option>
                                                <option value="4">Quote Rejected</option>
                                            </select>
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot> -->
                                <tbody></tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal" id="quotation_update_status">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Update Status</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <select class="form-control" id="quotation_status_id" title="Select Status...">
                            <option value="0">Select Status...</option>
                            <option value="3">Quote Approved</option>
                            <option value="4">Quote Rejected</option>
                        </select>
                    </div>
                </div>
                <input type="hidden" value="" id="quotation-change-status-row-id" />
            </div>
            <div class="modal-footer">
                <button type="button" id="quotation-status-dlg-btn" class="btn btn-info" >Submit</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div> 
@endsection

@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>

@endsection
