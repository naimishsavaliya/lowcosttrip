@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
    select.select-style{
        background-size: 6px 5px, 5px 5px, 3em 3.5em;
    }
</style>
<script type="text/javascript">
    cityData='';
</script>
<div class="container">
<br/>
@if(session()->has('reset_success_message'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('reset_success_message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('reset_error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session()->get('reset_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('RESET PASSWORD') }}</div>
                <div class="card-body">
                    <form method="POST" id="reset_frm" name="reset_frm" action="{{ route('reset-password-request') }}">
                        @csrf
                        <input type="hidden" name="user_id" id="user_id" value="{{ isset($userdata->id) ? $userdata->id : '' }}" />
                        <input type="hidden" name="forgot_token" id="forgot_token" value="{{ isset($forgot_token) ? $forgot_token : '' }}" />
                        <div class="form-group">
                            <input type="password" name="password" id="reg_password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <a class="btn btn-primary submit-btn" href="{{ route('client_login_form') }}">
                                    {{ __('BACK') }}
                                </a>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary submit-btn">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
    $("#reset_frm").validate({
        rules: {
            password: {
                required: true,
                minlength: 6
            },
            password_confirmation: {
                required: true,
                equalTo: "#reg_password"
            }
        },
        messages: {
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            password_confirmation: {
                required: "Please provide a confirm password",
                equalTo: "Enter Confirm Password Same as Password"
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});
</script>
@endsection
