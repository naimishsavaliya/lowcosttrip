@extends('layouts.client')

@section('content')
<style type="text/css">
.home-newsletter {
	padding: 80px 0;
	background: white;
}
.home-newsletter .single {
	max-width: 650px;
	margin: 0 auto;
	text-align: center;
	position: relative;
	z-index: 2; 
}
.home-newsletter .single h2 {
	font-size: 22px;
	color: black;
	text-transform: uppercase;
	margin-bottom: 40px; 
}
</style>
<script type="text/javascript">
    cityData='';
</script>
<section class="home-newsletter">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="single">
					<h2>Thank you for subscribing to our news latter</h2>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
@endsection
