@extends('layouts.client')

@section('content')
<section>
    <div class="container">
        <br/>
    <div class="row">
        <h1 class="contitle">Contact Us</h1>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="cntct-dtl-col ofc-dtl-wrapper">
                        <!-- <div class="cntct-dtl-box">
                            <p class="toll-txt">
                            <i class="fa fa-phone ico-ralign"></i>Toll Free</p>
                            <p class="toll-no">1800 22 7979 (For Indian Nationals)</p>
                        </div>  -->
                        <div class="cntct-dtl-box">
                            <p class="whatsapp-txt">
                                <i class="fa fa-whatsapp ico-ralign"></i>WhatsApp</p>
                            <p class="whatsapp-no">81 82 83 84 85 ( For Foreign Nationals)</p>
                        </div>  
                        <div class="cntct-dtl-box">
                            <p class="cntct-dtl-hdng"><i class="fa fa-envelope-o ico-ralign"></i>Email
                                <a href="mailto:info@lowcosttrip.com" class="mail_contact padd">info@lowcosttrip.com</a></p>
                        </div>
                        <div class="cntct-dtl-box">
                            <p class="cntct-dtl-hdng"><i class="fa fa-mobile-phone ico-ralign"></i>Info-Center Numbers</p>
                            <p class="cntct-dtl-dscrptn">
                                81 82 83 84 85
                            </p>
                        </div>
                        <div class="cntct-dtl-box">
                            <p class="cntct-dtl-hdng"><i class="fa fa-map-marker ico-ralign"></i>Address</p>
                            <p class="cntct-dtl-dscrptn">
                                IQ Comtech Pvt Ltd ,<br>		
                                1205,Sai Indu Tower ,<br>
                                Lbs marg, Bhandup, Mumbai - 78.
                            </p>
                        </div>
                        <div class="cntct-dtl-box">
                            <p class="cntct-dtl-hdng"><i class="fa fa-clock-o ico-ralign"></i>Office Timings</p>
                            <p class="cntct-dtl-dscrptn">
                                Monday-Saturday 10 AM to 7 PM
                            </p>
                        </div>
                        <?php /* 
                        <div class="cntct-dtl-box">
                            <p class="cntct-dtl-hdng"><i class="fa fa-handshake-o ico-ralign"></i>For Business Relations</p>
                        </div>
                        */ ?>
                        <div class="cntct-dtl-box">
                             <p class="cntct-dtl-hdng"><i class="fa fa-handshake-o ico-ralign"></i><a href="#contact_form" class="busn_rel fancybox" data-toggle="modal">Click Here For Business Association</a></p> 
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <?php /* 
                    <a href="#contact_form" class="busn_rel fancybox" data-toggle="modal">Click Here For <!-- Business Relations --> Business Association <i class="fa fa-user pull-right fs-20 p-r-20 m-t-5" aria-hidden="true"></i></a>
                    <!-----For business relation Popup start ------>
                    */ ?>

                    <div class="modal fade" id="contact_form" role="dialog" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog enquiry_modal" style="top: 10px">
                            <div class="modal-content">
                                <div class="modal-header blue_box_form">
                                    <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">×</button>
                                    <h4 class="modal-title c-f p-l-15">Connect To Grow With Us</h4>
                                </div>
                                <div class="modal-body pull-left full-width">
                                    <form class="m-t-12 pull-left full-width ng-pristine ng-valid" role="form">
                                        <div class="col-md-6">
                                            <div class="form-group advnce_autocomplete">
                                                <label for="txtWhere1" class="fs-12">
                                                    Full Name</label>
                                                <span role="status" class=""></span>
                                                <input name="Destination" class="form-control ui-autocomplete-input" searchtype="Advance" id="txtWhere1" placeholder="Enter Name" autocomplete="off" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group advnce_autocomplete">
                                                <label for="txtWhere2" class="fs-12">
                                                    Contact No</label>
                                                <span role="status" class=""></span>
                                                <input name="Destination" class="form-control ui-autocomplete-input" searchtype="Advance" id="txtWhere2" placeholder="Phone No" autocomplete="off" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group advnce_autocomplete">
                                                <label for="txtWhere3" class="fs-12">
                                                    Email Id</label>
                                                <span role="status" class=""></span>
                                                <input name="Destination" class="form-control ui-autocomplete-input" searchtype="Advance" id="txtWhere3" placeholder="Enter Email Id" autocomplete="off" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="pop_search_depart enquiry-form m-b-0">
                                                <label style="padding-top: 0px;"><b>
                                                        Subject</b></label>

                                                <div class="form-group">

                                                    <select name="Month" class="form-control" id="optMonth">

                                                        <option value="" selected="selected">Select Subject</option>

                                                        <option value="Ahmedabad">Business Relations: Destination Management</option>
                                                        <option value="Goa">Business Relations: Marketing Management</option>
                                                        <option value="Mumbai">Business Relations: Accounts Management</option>
                                                        <option value="Nashik">Business Relations: Human Resource Management</option>
                                                        <option value="Pune">Business Relations: Web/IT Management</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group advnce_autocomplete">
                                                <label class="fs-12"><b>Message</b></label>
                                                <textarea class="form-control ui-autocomplete-input" placeholder=""></textarea>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <div class="more_option p-t-0">
                                        <button class="btn btn-complete btn-cons">Submit</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-----For business relation Popup End ------>

                </div>
            </div>
        </div>

        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="cntct-map-col">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3768.8649152897347!2d72.93571881433957!3d19.15738948704126!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b86704c97351%3A0xd20ad6ad3ef020bd!2sSai+Indu+Tower!5e0!3m2!1sen!2sin!4v1541680027234" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
        <br/><br/>
    </div>
</section>


@endsection

@section('javascript')
<script type="text/javascript">

    // Pure Javascript
    document.getElementsByClassName("activity-details").onmouseover = function () {
        document.getElementsByClassName("activity-overlay").style.display = 'block';
    }
    document.getElementsByClassName("activity-details").onmouseout = function () {
        document.getElementsByClassName("activity-overlay").style.display = 'none';
    }


</script>
@endsection