@extends('layouts.client')
<script>
	var cityData = <?= $cities ?>;
</script>
@section('content')
<!-- <section> -->
<section class="page-body">



    <div class="container-fluid">
        <div class="slider-banner">
          @if(isset($home_banner) && $home_banner->count()>0)
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">

                    <div class="owl-carousel owl-slider owl-theme">
                       @foreach($home_banner as $bannerId=>$banner)
                          <div class="item">

                              <div class="custom_overlay_wrapper">
                                  @if($banner->image_1!='' && file_exists(public_path('/uploads/home_banner/'.$banner->image_1)))
                                     <a href="{{$banner->redirect_to}}" target="_blank">
                                        <img src="{{ env('APP_PUBLIC_URL')}}uploads/home_banner/{{$banner->image_1}}" class="img-responsive">
                                     </a>
                                  @else
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/default-placeholder.png" width="210px" height="139px"/>
                                  @endif
                                  <?php /*
                                  <div class="custom_overlay">
                                      <div class="custom_overlay_inner ">
                                          <h4 class="banner-heading4">Dazzling</h4>
                                          <h2 class="banner-heading2">Dubai</h2>
                                          <span class="banner-price">Starts From: USD 495 (INR. 32,670) </span>
                                      </div>
                                  </div> */ ?>
                              </div>

                          </div>
                         @endforeach
                         <?php /*
                        <div class="item">

                            <div class="custom_overlay_wrapper">
                                <img src="{{ env('APP_PUBLIC_URL')}}/new-theme/images/slider/banner-1.jpg" class="img-responsive">
                                <div class="custom_overlay">
                                    <div class="custom_overlay_inner ">
                                        <h4 class="banner-heading4">Dazzling</h4>
                                        <h2 class="banner-heading2">Dubai</h2>
                                        <span class="banner-price">Starts From: USD 495 (INR. 32,670) </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">

                            <div class="custom_overlay_wrapper">
                                <img src="{{ env('APP_PUBLIC_URL')}}/new-theme/images/slider/banner-1.jpg" class="img-responsive">
                                <div class="custom_overlay">
                                    <div class="custom_overlay_inner ">
                                        <h4 class="banner-heading4">Dazzling</h4>
                                        <h2 class="banner-heading2">Dubai</h2>
                                        <span class="banner-price">Starts From: USD 495 (INR. 32,670) </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">

                            <div class="custom_overlay_wrapper">
                                <img src="{{ env('APP_PUBLIC_URL')}}/new-theme/images/slider/banner-1.jpg" class="img-responsive">
                                <div class="custom_overlay">
                                    <div class="custom_overlay_inner ">
                                        <h4 class="banner-heading4">Dazzling</h4>
                                        <h2 class="banner-heading2">Dubai</h2>
                                        <span class="banner-price">Starts From: USD 495 (INR. 32,670) </span>
                                    </div>
                                </div>
                            </div>
                        </div> */ ?>
                    </div>

                </div>

            </div>
          @endif

        </div>
    </div>


   
        <div class="scb">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 ">
                        <h2 class="scb-title">Search • Compare • Book</h2>
                        <p class="scb-desc">One stop travel solution company with focus on personalized and group tours for Indian and overseas destination. Cut above the rest in services for Holidays packages, air tickets, hotels, forex, transfers and visa.</p>
                    </div>
                </div>
            </div>
        </div>
 



<?php /*
   <div id = "myTabContent" class = "tab-content">
      <!-- Holiday Block Begins -->
       
      <div  class="tab-pane fade" id="hotel-block" >
         <!-- Search Block Begins -->
         <div class="search-block hotel-search">
            <div class="container">
               <div class="row">
                  <form role="search" method="get" action="#">
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <input  type="text" class="form-control" name="email" placeholder="Destination">
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <input type="text" class="form-control"  placeholder="Check-In">
                                 <span class="form-control-feedback"><i class="fa fa-calendar"></i></span>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <input type="text" class="form-control" name="email" placeholder="Check-Out">
                                 <span class="form-control-feedback"><i class="fa fa-calendar"></i></span>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <select class="form-control">
                                    <option>Rooms</option>
                                    <option>1</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <select class="form-control">
                                    <option>Adults</option>
                                    <option>1</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <button type="submit" class="btn-hotel">Search</button>
                           </div>
                        </div>
                        <!--   <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                           <div class="row">
                               <div class="search-cust">
                               <span class="search-span">or</span>
                                <a href="#" class="btn btn-default btn-customize">Customize</a>
                                </div>
                               </div>
                               
                           
                           </div> -->
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <!-- Search Block Ends -->
         <!-- Banner Block Begins -->
         <div class="banner"></div>
         <div class="banner-main" >
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                           <div class="  banner-blocks">
                              <div class="banner-block-left">
                                 <div class="thumbnail">
                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-1.jpg" alt = "Generic placeholder thumbnail">
                                 </div>
                                 <div class="caption big-caption bg-orange orange-border">
                                    <div class="row">
                                       <div class="col-xs-6 big-caption-left">
                                          <h4>On International Tours</h4>
                                          <h2>
                                          Europe Tour Package</h2>
                                       </div>
                                       <div class="col-xs-6 big-caption-right">
                                          <h4>Starting From</h4>
                                          <h3>Rs. 29,999/- Per Person</h3>
                                       </div>
                                    </div>
                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/traingle-show.png" class="arrow  img-responsive"/>
                                 </div>
                                 <div class="off">
                                    <img src="{{ env('APP_PUBLIC_URL')}}client/images/off.png" class="img-responsive"/>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                           <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                 <div class="banner-blocks banner-block-right-top">
                                    <div class="thumbnail">
                                       <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-2.jpg" alt = "Generic placeholder thumbnail">
                                    </div>
                                    <div class="caption big-caption bg-blue">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <h4>Flat Rs. 800 OFF</h4>
                                             <h2>
                                             The Resort Bech Hotel</h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                 <div class="banner-blocks banner-block-right-bottom">
                                    <div class="thumbnail">
                                       <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-3.jpg" alt = "Generic placeholder thumbnail">
                                       <div class="pink-overlay bg-pink big-caption" >
                                          <div class="pink-skew">
                                             <h4>Goa Banana Boat</h4>
                                             <h2>
                                             Flat 15% Off</h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Banner Block Ends -->
      </div>
      <!-- Hotel Block Ends -->
      <!-- Flight Block Begins -->
      <div  class="tab-pane fade" id="flight-block" >
         <!-- Sub Menu Begins -->
         <!-- Sub Menu Ends -->
         <!-- Search Block Begins -->
         <div class="search-block flight-search">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <div class="row">
                        <div class="row">
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <input  type="text" class="form-control" name="email" placeholder="From">
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <input  type="text" class="form-control" name="email" placeholder="To">
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <input type="text" class="form-control" name="email" placeholder="Depart">
                                 <span class="form-control-feedback"><i class="fa fa-calendar"></i></span>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <select class="form-control">
                                    <option>Returns</option>
                                    <option>1</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <div class="input-group">
                                 <select class="form-control">
                                    <option>Adults</option>
                                    <option>1</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <button type="submit" class="btn-flight ">Search</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Search Block Ends -->
         <!-- Banner Block Begins -->
         <div class="banner"></div>
         <div class="banner-main" >
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                           <div class="row">
                              <div class="  banner-blocks">
                                 <div class="banner-block-left">
                                    <div class="thumbnail">
                                       <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-1.jpg" alt = "Generic placeholder thumbnail">
                                    </div>
                                    <div class="caption big-caption bg-orange orange-border">
                                       <div class="row">
                                          <div class="col-xs-6 big-caption-left">
                                             <h4>On International Tours</h4>
                                             <h2>
                                             Europe Tour Package</h2>
                                          </div>
                                          <div class="col-xs-6 big-caption-right">
                                             <h4>Starting From</h4>
                                             <h3>Rs. 29,999/- Per Person</h3>
                                          </div>
                                       </div>
                                       <img src="{{ env('APP_PUBLIC_URL')}}client/images/traingle-show.png" class="arrow  img-responsive"/>
                                    </div>
                                    <div class="off">
                                       <img src="{{ env('APP_PUBLIC_URL')}}client/images/off.png" class="img-responsive"/>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                           <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                 <div class="banner-blocks banner-block-right-top">
                                    <div class="thumbnail">
                                       <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-2.jpg" alt = "Generic placeholder thumbnail">
                                    </div>
                                    <div class="caption big-caption bg-blue">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <h4>Flat Rs. 800 OFF</h4>
                                             <h2>
                                             The Resort Bech Hotel</h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                 <div class="banner-blocks banner-block-right-bottom">
                                    <div class="thumbnail">
                                       <img src="{{ env('APP_PUBLIC_URL')}}client/images/banner/banner-3.jpg" alt = "Generic placeholder thumbnail">
                                       <div class="pink-overlay bg-pink big-caption" >
                                          <div class="pink-skew">
                                             <h4>Goa Banana Boat</h4>
                                             <h2>
                                             Flat 15% Off</h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Banner Block Ends -->
      </div>
      <!-- Flight Block Ends -->
      <!-- Activity Block Begins -->
      <div  class="tab-pane fade" id="activity-block" >
         <!-- Search Block Begins -->
         <div class="search-block">
            <div class="container">
               <div class="row">
                  <form role="search" method="get" action="#">
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                           <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                              <input type="text" placeholder=" Search For Cities , Activites ,Tours " name="search" class="activity-text">
                           </div>
                           <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                              <button type="submit" class="btn-activity ">Search</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <!-- Search Block Ends -->
            </div>
            <!-- Activity Block Ends -->
         </div>
      </div>
      <div  class="tab-pane fade" id="customizes-block" >
            <div class="customize">
                <div class="container">
                    <div class="row">
                        <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<!-- Nav tabs -->
								<ul class="nav nav-pills inquiry_tab">
									<li class="active"><a data-toggle="pill" href="#Flight">Flight</a></li>
									<li class=""><a data-toggle="pill" href="#Hotel">Hotel</a></li>
									<li class=""><a data-toggle="pill" href="#holiday">Holiday</a></li>
									<li class=""><a data-toggle="pill" href="#visa">Visa</a></li>
								</ul>
								<div class="tab-content">
									<div id="Flight" class="tab-pane fade active in">
										<form name="flight_frm" id="flight_frm" action="" method="post" onsubmit="return false;">
											{{csrf_field()}}
											<div class="booking-block">
												<div class="booking-block-title">
													<h3>Inquiry Details</h3>
												</div>
												<div class="holiday-booking-flight booking-block-form">
													<div class="row booking-form-padding" style="display: none;">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<ul class="validate-msg">

															</ul>
														</div>
													</div>
													<div class="row booking-form-padding">
														<label class="radio-inline">Flight Type</label>
														<label class="radio-inline"><input type="radio" name="flight_type" value="One Way" checked>One Way</label>
														<label class="radio-inline"><input type="radio" name="flight_type" value="Round Trip">Round Trip</label>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="from_city" class="form-control autocomplete" placeholder="From City">
																<input type="hidden" name="from_city_id">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="to_city" class="form-control autocomplete" placeholder="To City">
																<input type="hidden" name="to_city_id">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<select name="flight_class" class="select-style">
																	<option value="">Class</option>
																	@foreach(get_flight_class() as $key => $value)
																	<option value="{{$key}}">{{$value}}</option>
																	@endforeach
																</select>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="deprature_date" class="form-control datepicker" readonly="" placeholder="Deprature Date">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="return_date" class="form-control datepicker" readonly="" placeholder="Return Date">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">

														</div>
													</div>
													<div class="row booking-form-padding">
														<label class="radio-inline">Number of Guests</label>
													</div>
													<div class="row booking-form-padding col-lg-8">
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-minus"></span>
																	</button>
																</span>
																<input type="number" name="adults" value="" class="form-control input-number" placeholder="ADULTS" min="1" max="10">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-plus"></span>
																	</button>
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-minus"></span>
																	</button>
																</span>
																<input type="text" name="child" class="form-control input-number" placeholder="Child" min="1" max="10">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-plus"></span>
																	</button>
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-minus"></span>
																	</button>
																</span>
																<input type="text" name="infants" class="form-control input-number" placeholder="Infants" min="1" max="10">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-plus"></span>
																	</button>
																</span>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<label class="radio-inline">Contact Details</label>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="contact_name" class="form-control" placeholder="Name">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="contact_no" class="form-control onlynumber" maxlength="10" placeholder="Contact No">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="email_id" class="form-control" placeholder="Email Id">
															</div>
														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
															<div class="input-group">
																<textarea name="notes" class="form-control" placeholder="More Details"></textarea>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<button type="submit" class="btn btn-default btn-payment btn-desktop-right flight_submit_btn">SUBMIT</button>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div id="Hotel" class="tab-pane fade">
										<div class="booking-block">
											<div class="booking-block-title">
												<h3>Inquiry Details</h3>
											</div>
											<form name="hotel_frm" id="hotel_frm" action="" method="post" onsubmit="return false;">
												{{csrf_field()}}
												<div class="holiday-booking-flight booking-block-form">
													<div class="row booking-form-padding" style="display: none;">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<ul class="validate-msg">

															</ul>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="destination_city" class="form-control autocomplete" placeholder="Destination City">
																<input type="hidden" name="destination_city_id">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="deprature_date" class="form-control datepicker" readonly="" placeholder="Deprature Date">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="return_date" class="form-control datepicker" readonly="" placeholder="Return Date">
															</div>
														</div>
													</div>
													<div class="row booking-form-padding">
														<label class="radio-inline">Number of Guests</label>
													</div>
													<div class="row booking-form-padding col-lg-8">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="number_of_guests" class="form-control" placeholder="Number of Guests">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">

														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">

														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<label class="radio-inline">Contact Details</label>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="contact_name" class="form-control" placeholder="Name">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="contact_no" class="form-control onlynumber" placeholder="Contact No" maxlength="10">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="email_id" class="form-control" placeholder="Email Id">
															</div>
														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
															<div class="input-group">
																<textarea name="notes" class="form-control" placeholder="More Details"></textarea>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<button class="btn btn-default btn-payment btn-desktop-right hotel_submit_btn">SUBMIT</button>
													</div>
												</div>
											</form>
										</div>
									</div>
									<div id="holiday" class="tab-pane fade">
										<div class="booking-block">
											<div class="booking-block-title">
												<h3>Build Your Own Holiday Itinerary</h3>
											</div>
											<div class="holiday-booking-flight booking-block-form">
												<form name="holiday_frm" id="holiday_frm" action="" method="post" onsubmit="return false;">
												{{csrf_field()}}
												<div class="row booking-form-padding" style="display: none;">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<ul class="validate-msg">

														</ul>
													</div>
												</div>
												<div class="row booking-form-padding">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<input type="text" name="departure_city" class="form-control autocomplete" placeholder="Departure City">
															<input type="hidden" name="departure_city_id">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<input type="text" name="check_in_date" class="form-control datepicker" readonly="" placeholder="Check In Date">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<select name="travel_type" class="select-style">
																<option value="">Travel Type</option>
																@foreach(get_travel_type() as $key => $travel_type)
																<option value="{{$key}}">{{$travel_type}}</option>
																@endforeach
															</select>
														</div>
													</div>
												</div>
												<div class="row booking-form-padding">
													<label class="radio-inline">Destination Details</label>
												</div>
												<div class="row booking-form-padding">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<td width="50%">Destination</td>
																	<td width="30%">Number of Nights</td>
																	<td width="20%">Action</td>
																</tr>
															</thead>
															<tbody>
																<tr class="add_btn_tr">
																	<td></td>
																	<td></td>
																	<td>
																		<button type="button" class="btn btn-default btn-number destination_add_btn" data-type="plus" data-field="quant[1]">
																			<span class="glyphicon glyphicon-plus"></span>
																		</button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
												<div class="row booking-form-padding">
													<label class="radio-inline">Number of Guests</label>
												</div>
												<div class="row booking-form-padding col-lg-8">
													<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
														<div class="input-group">
															<span class="input-group-btn">
																<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																	<span class="glyphicon glyphicon-minus"></span>
																</button>
															</span>
															<input type="text" name="adults" class="form-control input-number" placeholder="ADULTS" min="1" max="10">
															<span class="input-group-btn">
																<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																	<span class="glyphicon glyphicon-plus"></span>
																</button>
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
														<div class="input-group">
															<span class="input-group-btn">
																<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																	<span class="glyphicon glyphicon-minus"></span>
																</button>
															</span>
															<input type="text" name="child" class="form-control input-number" placeholder="Child" min="1" max="10">
															<span class="input-group-btn">
																<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																	<span class="glyphicon glyphicon-plus"></span>
																</button>
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
														<div class="input-group">
															<span class="input-group-btn">
																<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																	<span class="glyphicon glyphicon-minus"></span>
																</button>
															</span>
															<input type="text" name="infants" class="form-control input-number" placeholder="Infants" min="1" max="10">
															<span class="input-group-btn">
																<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																	<span class="glyphicon glyphicon-plus"></span>
																</button>
															</span>
														</div>
													</div>
												</div>
												<div class="row booking-form-padding col-lg-12">
													<label class="radio-inline">Contact Details</label>
												</div>
												<div class="row booking-form-padding col-lg-12">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<input type="text" name="contact_name" class="form-control" placeholder="Name">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<input type="text" name="contact_no" class="form-control onlynumber" maxlength="10" placeholder="Contact No">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<input type="text" name="email_id" class="form-control" placeholder="Email Id">
														</div>
													</div>
												</div>
												<div class="row booking-form-padding col-lg-12">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
														<div class="input-group">
															<textarea name="notes" class="form-control" placeholder="More Details"></textarea>
														</div>
													</div>
												</div>
												<div class="row booking-form-padding col-lg-12">
													<button class="btn btn-default btn-payment btn-desktop-right holiday_submit_btn">SUBMIT</button>
												</div>
											</form>
											</div>
										</div>
									</div>
									<div id="visa" class="tab-pane fade">
										<div class="booking-block">
											<div class="booking-block-title">
												<h3>Inquiry Details</h3>
											</div>
											<div class="holiday-booking-flight booking-block-form">
												<form name="visa_frm" id="visa_frm" action="" method="post" onsubmit="return false;">
													{{csrf_field()}}
													<div class="row booking-form-padding" style="display: none;">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<ul class="validate-msg">

															</ul>
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="where_live" class="form-control autocomplete" placeholder="Where do you live?">
																<input type="hidden" name="where_live_id">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="where_go" class="form-control autocomplete" placeholder="Where do you want to go?">
																<input type="hidden" name="where_go_id">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="travel_date" class="form-control datepicker" placeholder="Travel Date">
															</div>	
														</div>
													</div>
													<div class="row booking-form-padding">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<select name="visa_type" class="select-style">
																	<option value="">Visa Type</option>
																	@foreach(get_visa_type() as $key => $visa_type)
																	<option value="{{$key}}">{{$visa_type}}</option>
																	@endforeach
																</select>
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<select name="number_of_entries" class="select-style">
																	<option value="">Number of Entries</option>
																	@foreach(get_number_of_entries() as $key => $visa_type)
																	<option value="{{$key}}">{{$visa_type}}</option>
																	@endforeach
																</select>
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-minus"></span>
																	</button>
																</span>
																<input type="text" name="duration_days" class="form-control input-number" placeholder="Duration Days" min="1" max="10">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-plus"></span>
																	</button>
																</span>
															</div>	
														</div>
													</div>
													<div class="row booking-form-padding">
														<label class="radio-inline">Number of Guests</label>
													</div>
													<div class="row booking-form-padding col-lg-8">
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-minus"></span>
																	</button>
																</span>
																<input type="text" name="adults" class="form-control input-number" placeholder="ADULTS" min="1" max="10">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-plus"></span>
																	</button>
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-minus"></span>
																	</button>
																</span>
																<input type="text" name="child" class="form-control input-number" placeholder="KIDS" min="1" max="10">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-plus"></span>
																	</button>
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
															<div class="input-group">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-minus"></span>
																	</button>
																</span>
																<input type="text" name="infants" class="form-control input-number" placeholder="INFANTS" min="1" max="10">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-plus"></span>
																	</button>
																</span>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<label class="radio-inline">Contact Details</label>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="contact_name" class="form-control" placeholder="Name">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="contact_no" class="form-control onlynumber" placeholder="Contact No" maxlength="10">
															</div>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
															<div class="input-group">
																<input type="text" name="email_id" class="form-control" placeholder="Email Id">
															</div>
														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
															<div class="input-group">
																<textarea name="notes" class="form-control" placeholder="More Details"></textarea>
															</div>
														</div>
													</div>
													<div class="row booking-form-padding col-lg-12">
														<button class="btn btn-default btn-payment btn-desktop-right visa_submit_btn">SUBMIT</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
   </div>
   <!-- Search Compare Book Block Begins -->
   <div class="scb">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 ">
               <h2 class="scb-title">SEARCH &diams; COMPARE &diams; BOOK</h2>
               <p class="scb-desc">Low cost trip (LCT) is a b2b as well as b2c online travel company. We are a one stop travel solution company. Focussing on holidays packages, air tickets, hotels, forex, transfers and visa.</p>
            </div>
         </div>
      </div>
   </div>

*/ ?>





   <!-- Search Compare Book Block Ends -->
   <!-- Hot Deals Begins-->
    <?php if (count($Hotdeal) > 0) { ?>
    @foreach($Hotdeal as $key => $HotValue)
    <?php if(isset($HotValue->hot_tour_data) && count($HotValue->hot_tour_data) > 0){ ?>
    <div class="hot-deals" style="margin-top: -45px">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-offset-2"> 
              <h3 class="hot-deals-title background-line col-xs-10"><span>Deals in {{ isset($HotValue->current_name) ? $HotValue->current_name : '' }}</span></h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="deal-blocks">
                @foreach($HotValue->hot_tour_data as $key1 => $tour_data)
                <a href="{{route('holiday-detail',[$tour_data->tour_id,str_slug($tour_data->tour_name,'-')])}}" target="_blank">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 pd-r-0">
                  <div class="thumbnail">
                    <img src="{{ env('AWS_BUCKET')}}{{$tour_data->image}}" alt="" class="img-responsive">
                    <div class="deal-duration">
                      <?php /*    
                      <a href="{{route('holiday-detail',[$tour_data->tour_id,str_slug($tour_data->tour_name,'-')])}}">
                      */ ?>
                      {{$tour_data->tour_nights + 1}} Days & {{$tour_data->tour_nights}}  Nights 
                       <?php /*
                      </a>
                       */ ?>
                    </div>
                    <div class="caption">
                      <div class="deal-desc">
                      <?php
                        $tour_name_val=$tour_data->tour_name; 
                        if(strlen($tour_data->tour_name)>=22){
                          $tour_name_val=substr($tour_data->tour_name,0,22)."..."; 
                        }
                      ?>
                      <span class="tour-title">{{$tour_name_val}}</span>
                      <?php /* //if(isset($tour_data['cost']['amount']) && $tour_data['cost']['amount']!=''){?>
                      @foreach($tour_data['cost']['amount'] as $c => $cost)
                      @foreach($cost['rates'] as $r => $rate)
                      <p class="amount {{($c!=1)?'hide':''}}">
                      @if(isset($rate['sell']) && $rate['sell'] > 0)
                      {{($rate['sell'])-($rate['sell']*$tour_data['cost']['dates']['discount'])/100}} (Per Pax Cost)
                      @endif
                      </p>
                      @break;
                      @endforeach
                      @endforeach
                      <?php //} */?>
                      </div>
                    </div>
                  </div>
                </div>
                </a>
                @endforeach
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                <a href="{{ isset($HotValue->url) ? $HotValue->url : '#' }}" class=" text-center" target="_blank">View More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    @endforeach
    <?php } ?>
    <?php if (isset($blog_posts) && count($blog_posts) > 0) { ?>
    <div class="hot-deals" style="margin-top: -45px">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-offset-2"> 
              <h3 class="hot-deals-title background-line col-xs-10"><span>Stories</span></h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="deal-blocks">
                @foreach($blog_posts as $key => $blog_postsValue)
                <a href="{{ isset($blog_postsValue->link) ? $blog_postsValue->link : '#' }}" target="_blank">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pd-r-0">
                  <div class="thumbnail">
                    <img src="{{$source_url[$key]}}" alt="" class="img-responsive">
                    <div class="deal-duration" style="bottom: 21.7%;">
                      {{ isset($blog_postsValue->date) ? date('d F Y', strtotime($blog_postsValue->date)) : '' }}
                    </div>
                    <div class="caption">
                      <div class="deal-desc">
                      <?php
                        $blog_title='';
                        if(isset($blog_postsValue->title->rendered) && $blog_postsValue->title->rendered!=''){
                        $blog_title=$blog_postsValue->title->rendered; 
                        if(strlen($blog_postsValue->title->rendered)>=22){
                          $blog_title=substr($blog_postsValue->title->rendered,0,22)."..."; 
                        }
                        }
                      ?>
                      <span class="tour-title">{{$blog_title}}</span>
                      <?php /*
                      <p>
                      @if(isset($blog_postsValue->content->rendered) && $blog_postsValue->content->rendered!='')
                      <?php echo strip_tags(substr($blog_postsValue->content->rendered,0,300)."..."); ?>
                      @endif
                      </p>
                      */ ?>
                      </div>
                    </div>
                  </div>
                </div>
                </a>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                <a href="{{ env('WP_URL')}}" class=" text-center" target="_blank">View More</a>
              </div>
            </div>
      </div>
    </div>
    <?php } ?>

    <div class="hot-deals" style="margin-top: -45px">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-offset-2"> 
              <h3 class="hot-deals-title background-line col-xs-10"><span>Why Low Cost Trip ? </span></h3>
            </div>
            <div class="clearfix"></div>
            <div class="row deal-blocks why-lct">
               <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                  <div><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/why-lct/Effectively planned FIT (Customised) & Group Tours.png" alt="" > </div> 
                  <div class="p-t-b-5">Effectively planned FIT (Customised) & Group Tours</div>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                  <div><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/why-lct/Attentions to details and specific requirements.png" alt="" > </div>
                  <div class="p-t-b-5">Attentions to details and specific requirements</div>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                  <div><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/why-lct/Multiple options for hotels & sight seeing.png" alt="" > </div>
                  <div class="p-t-b-5">Multiple options for hotels & sight seeing</div>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                  <div><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/why-lct/Economical as well as luxury tour packages.png" alt="" > </div>
                  <div class="p-t-b-5">Economical as well as luxury tour packages</div>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center">
                  <div><img src="{{ env('APP_PUBLIC_URL')}}new-theme/images/why-lct/Young, Dynamic & Experienced Travel Experts.png" alt="" > </div>
                  <div class="p-t-b-5">Young, Dynamic & Experienced Travel Experts</div>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php /*
    <div class="hot-deals" style="margin-top: -45px">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3 class="hot-deals-title"><span class="hot">Hot</span> Blog</h3>
            <div class="row">
              <div class="deal-blocks">
                <a href="" target="_blank">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="thumbnail">
                    <img src="{{ env('AWS_BUCKET')}}" alt="" class="img-responsive">
                    <div class="deal-duration">
                     10 Days & 10 Nights 
                    </div>
                    <div class="caption">
                      <div class="deal-desc">
                      <p class="amount {{($c!=1)?'hide':''}}">
                        100 (Per Pax Cost)
                      </p>
                      </div>
                    </div>
                  </div>
                </div>
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                <a href="#" class=" text-center" target="_blank">View More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  */ ?>
   <!-- Hot Deals Ends -->
   <?php 
      /*
      <div class="hotel-deals">
      
          <div class="container">
      
              <div class="row">
      
      
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      
                      <h2 class="hotel-deals-title">Hotel Deals</h2>
                      <div class="clearfix"></div>
      
                      <div class="owl-carousel owl-theme">
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/plam-dubai.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href="#">plam Hotel</a></h4>
                                                  <h5 class="hotel-location">Dubai</h5>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 1500/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/green-club.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">Green Club</a></h4>
                                                  <h5 class="hotel-location">Europe</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 2500/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/wave-hotel.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">Wave Hotel</a></h4>
                                                  <h5 class="hotel-location">Goa</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 1000/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/plam-dubai.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">plam Hotel</a></h4>
                                                  <h5 class="hotel-location">Dubai</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 1500/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/green-club.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">Green Club</a></h4>
                                                  <h5 class="hotel-location">Europe</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 2500/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/wave-hotel.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">Wave Hotel</a></h4>
                                                  <h5 class="hotel-location">Goa</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 1000/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/plam-dubai.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">plam Hotel</a></h4>
                                                  <h5 class="hotel-location">Dubai</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 1500/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/green-club.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href="#">Green Club</a></h4>
                                                  <h5 class="hotel-location">Europe</h5>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 2500/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/wave-hotel.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">Wave Hotel</a></h4>
                                                  <h5 class="hotel-location">Goa</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 1000/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/plam-dubai.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">plam Hotel</a></h4>
                                                  <h5 class="hotel-location">Dubai</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 1500/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/green-club.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">Green Club</a></h4>
                                                  <h5 class="hotel-location">Europe</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 2500/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/hotels/wave-hotel.jpg" alt="" class="img-responsive">
                                      <div class="caption">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <h4 class="hotel-name"><a href=#">Wave Hotel</a></h4>
                                                  <h5 class="hotel-location">Goa</h4>
      
                                              </div>
                                              <div class="col-xs-6 pull-right">
                                                  <div class="text-right">
                                                      <h6 class="hotel-time">Avg / Night</h6>
                                                      <h4 class="hotel-price">Rs 1000/-</h4>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 hotel-reviews">
                                                  <div class="row">
                                                      <div class="col-xs-6">
                                                          <p class="hotel-review-star">
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star fa-yellow"></span>
                                                              <span class="fa fa-star"></span>
                                                          </p>
      
                                                      </div>
                                                      <div class="col-xs-6 text-right">
                                                          <p class="hotel-review-count">270 reviews</p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12">
                                                  <p class="hotel-desc">Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta.</p>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-xs-12 text-right">
      
                                                  <div class="col-xs-6">
                                                      <a href="#" class="  btn btn-default btn-select">Select</a>
                                                  </div>
                                                  <div class="col-xs-6 ">
                                                      <a href="#" class="btn btn-default btn-view">View</a>
                                                  </div>
                                              </div>
                                          </div>
      
      
                                      </div>
      
                                  </div>
      
      
      
                              </div>
                          </div>
      
      
      
      
      
      
      
      
      
      
      
                      </div>
      
      
      
      
      
                  </div>
      
      
      
      
      
              </div>
      
      
          </div>
      
      
      
      </div>
      */
      ?>
   <!-- Hotel Deals Ends -->
   <!-- Feature Activites Begins -->
   <?php 
      /*
       <div class="feature-activities">
      
          <div class="container">
      
              <div class="row">
      
      
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      
                      <h2 class="hotel-deals-title">Feature Activities</h2>
                      <div class="clearfix"></div>
      
                      <div class="owl-carousel owl-theme">
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-1.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-2.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-3.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-1.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-2.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-3.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
      
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-1.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-2.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-3.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-1.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-2.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
      
                                  </div>
      
      
      
                              </div>
      
                          </div>
                          <div class="item">
                              <div class="col-xs-12">
                                  <div class="thumbnail">
                                      <img src="{{ env('APP_PUBLIC_URL')}}client/images/featured/featured-3.jpg" alt="" class="img-responsive">
                                      <div class="activity-overlay" id="ao">
                                          <div class="activity-location">
                                              <h4>Kashmir</h4>
                                          </div>
                                          <div class="activity-count">
                                              <h5>3 Activities</h5>
                                          </div>    
      
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <div class="activity-details" id="ad">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
      
                                      <div class="caption">
                                          <div class="activity-details">
                                              <div class="col-xs-12">
                                                  <div class="row">
                                                      <div class="col-xs-6 activity-name"  id="ad">
                                                          <a href="#">Half-Day Island Tour</a>
                                                      </div>
                                                      <div class="col-xs-6 activity-price text-right">
                                                          <h5>From</h5>
                                                          <h4 class="activity-price">Rs.1200</h4>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
            
                                  </div>
            
                              </div>
      
                          </div>
      
                      </div>
      
                  </div>
      
              </div>
            
          </div>
      
      </div> 
      
      */
      ?>
   <!-- Feature Activites Ends -->
</section>
@endsection
@section('javascript')
<script type="text/javascript">

	// Pure Javascript
	document.getElementsByClassName("activity-details").onmouseover = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'block';
	}
	document.getElementsByClassName("activity-details").onmouseout = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'none';
	}


</script>
<?php /*<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>*/ ?>
<script>
// 	$('.owl-carousel').owlCarousel({
// 		loop: true,
// 		margin: 10,
// 		dots: true,
// 		responsiveClass: true,
// <!-- navText : ['<i class="	fa fa-caret-left" aria-hidden="true"></i>','<i class="	fa fa-caret-right" aria-hidden="true"></i>'], -->
// 		responsive: {
// 			0: {
// 				items: 1,
// 				nav: true
// 			},
// 			600: {
// 				items: 2,
// 				nav: false
// 			},
// 			1000: {
// 				items: 3,
// 				nav: false,
// 				loop: false,
// 				margin: 20

// 			}
// 		}
// 	});
	// $('.top-banner-slide').owlCarousel({
	// 	responsiveClass: true,
	// 	responsive: {
	// 		0: {
	// 			items: 1,
	// 			nav: true
	// 		},
	// 		600: {
	// 			items: 1,
	// 			nav: true
	// 		},
	// 		1000: {
	// 			items: 1,
	// 			nav: true,
	// 			loop: false,
	// 			margin: 20,
	// 			dots: false
	// 		}
	// 	}
	// });
</script>
@endsection