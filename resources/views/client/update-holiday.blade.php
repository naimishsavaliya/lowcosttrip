@extends('layouts.client')
<script>
	var cityData = <?= $cities ?>;
</script>
@section('content')
<section>
   <div id = "myTabContent" class = "tab-content">
      <div  class="tab-pane fade in active" id="customizes-block" >
            <div class="customize">
                <div class="container">
                    <div class="row">
                        <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="tab-content">
									<div id="holiday" class="tab-pane fade active in">
										<div class="booking-block  border-1">
											<div class="booking-block-title  p-t-b-5">
												<h3>Holiday Details</h3>
											</div>
											<div class="holiday-booking-flight booking-block-form">
												<form name="holiday_frm_update" id="holiday_frm_update" action="" method="post" onsubmit="return false;">
												{{csrf_field()}}
												<input type="hidden" name="id" value="{{ isset($lead->id) ? $lead->id : '' }}" />
												<div class="row booking-form-padding" style="display: none;">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
														<ul class="validate-msg">

														</ul>
													</div>
												</div>
												<div class="row booking-form-padding">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<label class="radio-inline nopadding">Departure City</label>
															<input type="text" name="departure_city" class="form-control autocomplete" value="{{ isset($lead->departureCity->city_name) ? $lead->departureCity->city_name : old('from_city') }}" placeholder="Departure City">
															<input type="hidden" name="departure_city_id" value="{{ isset($lead->departure_city_id) ? $lead->departure_city_id : old('departure_city_id') }}">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<label class="radio-inline nopadding">Interested Date</label>
															<input type="text" name="check_in_date" class="form-control datepicker" value="{{ isset($lead->check_in_date) ? date('d F Y', strtotime($lead->check_in_date)) : '' }}" readonly="" placeholder="Interested Date">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<label class="radio-inline nopadding">Choose travel type</label>
															<select name="travel_type" class="select-style">
																<option value="" selected>Choose travel type</option>
									                            @foreach(get_travel_type() as $key => $travel_type)
									                            <option value="{{ $key }}" {{ (isset($lead->travel_type) && $lead->travel_type == $key) ? 'selected' : '' }}>{{ $travel_type}}</option>
									                            @endforeach
															</select>
														</div>
													</div>
												</div>
												<div class="row booking-form-padding sub-title">
													<label class="radio-inline">Destination Details</label>
												</div>
												<div class="row booking-form-padding">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<td width="50%">Destination</td>
																	<td width="30%">Number of Nights</td>
																	<td width="20%">Action</td>
																</tr>
															</thead>
															<tbody>
																@if(count($holiday_destination_details)>0)
																@foreach($holiday_destination_details as $key=>$holiday)
																<tr class="destination_row">
																	<td>
																		<input type="text" name="destination[city_name][]" class="form-control autocomplete"
																		value="{{( isset($holiday->destinationCity->city_name) ? $holiday->destinationCity->city_name : '')}}"
																		placeholder="Search by city name">
																		<input type="hidden" name="destination[city_id][]" value="{{ ( isset($holiday->destination_city_id) ? $holiday->destination_city_id: '')}}">
																	</td>
																	<td>
																		<div class="input-group" style="width:120px">
																			<span class="input-group-btn">
																				<button type="button" class="btn btn-default auto-incriment"  data-type="minus" data-field="destination[{{$key}}][number_of_nights]">
																				<span class="glyphicon glyphicon-minus"></span>
																				</button>
																			</span>
																			<input type="text" name="destination[number_of_nights][]" class="form-control" value="{{ ( isset($holiday->number_of_nights) ? $holiday->number_of_nights: '')}}" min="1" max="10">
																			<span class="input-group-btn">
																				<button type="button" class="btn btn-default auto-incriment" data-type="plus" data-field="destination[{{$key}}][number_of_nights]">
																				<span class="glyphicon glyphicon-plus"></span>
																				</button>
																			</span>
																		</div>
																	</td>
																	<td>
																		<button type="button" class="btn btn-default btn-number destination_delete_btn" data-type="plus" data-field="quant[1]">
																		<span class="glyphicon glyphicon-minus"></span>
																		</button>
																	</td>
																</tr>
																@endforeach
																@endif
																<tr class="add_btn_tr">
																	<td></td>
																	<td></td>
																	<td>
																		<button type="button" class="btn btn-default btn-number destination_add_btn" data-type="plus" data-field="quant[1]">
																			<span class="glyphicon glyphicon-plus"></span>
																		</button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>





												<div class="row booking-form-padding sub-title" >
                                                        <label class="radio-inline">Activity Details</label>
                                                    </div>
                                                    <div class="row booking-form-padding">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <table class="table table-bordered activiti-table">
                                                                <thead class="append_new_row">
                                                                    <tr id="activity-header">
                                                                        <td width="30%">Country</td>
                                                                        <td width="50%">Activity</td>
                                                                        <td width="20%">Action</td>
                                                                    </tr>

                                                                @if(count($holiday_activity_details)>0)
																@foreach($holiday_activity_details as $key=>$activity)
																<tr>
																	<td>
																		<select id="country_id" class="select-style country_id edit_country_id" data-id="{{ $key }}" name=" activiti_details['city'][]">
																        <option value="" selected>Choose Country</option>
																	        @if(isset($countrys))
																	        @foreach($countrys as $country)
																	        <option value="{{ $country->country_id }}" {{ (isset($activity->country_id) && $activity->country_id == $country->country_id) ? 'selected' : '' }}>{{ $country->country_name}}</option>
																	        @endforeach
																	        @endif
																	    </select>
	
																	</td>
																	<td>
																		<input type="hidden" class="activity_id_{{ $key }}" value="{{$activity->activiti_id}}">
																		<select class="select-style tour_activity edit_tour_activity_{{ $key }}" name=" activiti_details['activiti'][]">
																	        <option value="" selected>Choose Activity</option>
																	    </select>	
																	</td>
																	<td>
																        <button type="button" class="btn btn-default btn-number destination_delete_btn" data-type="plus" data-field="quant[1]"><span class="glyphicon glyphicon-minus"></span></button>
																    </td>
																</tr>
																@endforeach
																@endif


                                                                </thead>
                                                                <tbody >                                                      
                                                                    <tr >
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-default btn-number add_tour_activites" data-type="plus" data-field="quant[1]">
                                                                                <span class="glyphicon glyphicon-plus"></span>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>





												<div class="row booking-form-padding sub-title">
													<label class="radio-inline">Number of Guests</label>
												</div>
												<?php /*
													<div class="row booking-form-padding sub-title">
													<label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
													</div> 
													*/?>
												<div class="row booking-form-padding">
													<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
														<div class="input-group">
															<label class="radio-inline nopadding">Adults</label>
															<input type="text" name="adults" class="form-control" value="{{ (isset($lead->adult)) ? $lead->adult : '' }}" placeholder="Adults">
															
														</div>
													</div>
													<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
														<div class="input-group">
															<label class="radio-inline nopadding">Child</label>
															<input type="text" name="child" class="form-control" value="{{ isset($lead->child) ? $lead->child : '' }}" placeholder="Child">
															
														</div>
													</div>
													<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
														<div class="input-group">
															<label class="radio-inline nopadding">Infants</label>
															<input type="text" name="infants" class="form-control" value="{{ isset($lead->infant) ? $lead->infant : '' }}" placeholder="Infants">
															
														</div>
													</div>
												</div>
												 
												<div class="row booking-form-padding">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<label class="radio-inline nopadding">Name</label>
															<input type="text" name="contact_name" class="form-control" value="{{ (isset($lead->name)) ? $lead->name : '' }}" placeholder="Name">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<label class="radio-inline nopadding">Contact No</label>
															<input type="text" name="contact_no" class="form-control onlynumber" maxlength="10" value="{{ (isset($lead->phone)) ? $lead->phone : '' }}" placeholder="Contact No">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
														<div class="input-group">
															<label class="radio-inline nopadding">Email Id</label>
															<input type="text" name="email_id" class="form-control" value="{{ (isset($lead->email)) ? $lead->email : '' }}" placeholder="Email Id">
														</div>
													</div>
												</div>
												<div class="row booking-form-padding">
													<div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
														<div class="input-group">
															<label class="radio-inline nopadding">More Details</label>
															<textarea name="notes" class="form-control" placeholder="More Details">{{ (isset($lead->notes)) ? $lead->notes : '' }}</textarea>
														</div>
													</div>
												</div>
												<div class="row booking-form-padding">
													<button class="btn btn-default btn-payment btn-desktop-right holiday_submit_btn">UPDATE</button>
												</div>
											</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
   </div>
   
   <!-- Search Compare Book Block Ends -->
   <!-- Hot Deals Begins-->
   <?php /*
   <div class="hot-deals">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                     <h3 class="hot-deals-title"><span class="hot">Hot</span> Deals in Domestic Holidays</h3>
                     <div class="row">
                        <div class="deal-blocks">
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/deal-1.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Kerala Tour</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/kashmir.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Kerala Tour</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                           <a href="#" class=" text-center">View More</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                     <h3 class="hot-deals-title"><span class="hot">Hot</span>Deals in International Holidays</h3>
                     <div class="row">
                        <div class="deal-blocks">
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/europe.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Europe</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                              <div class="thumbnail">
                                 <img src="{{ env('APP_PUBLIC_URL')}}client/images/hot-deals/dubai.jpg" alt="" class="img-responsive">
                                 <div class="deal-duration">    <a href="#">11 Days & 10 Nights </a></div>
                                 <div class="caption">
                                    <div class="deal-desc">
                                       <a href="#">Dubai</a>
                                       <p>INR 59000.00 (Per Couple Cost) </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 view-more">
                           <a href="#" class=" text-center">View More</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php */?>

</section>
@endsection
@section('javascript')
<script type="text/javascript">

	// Pure Javascript
	document.getElementsByClassName("activity-details").onmouseover = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'block';
	}
	document.getElementsByClassName("activity-details").onmouseout = function () {
		document.getElementsByClassName("activity-overlay").style.display = 'none';
	}


</script>
<?php /*
<script src="{{ env('APP_PUBLIC_URL')}}client/assets/owl/js/owl.carousel.min.js"></script>
*/ ?>
<script>

	$(document).ready(function () {

        $(document).on("click", ".add_tour_activites", function (event) {
            $.ajax({
                type: 'GET',
                url: "{{ route('get-activiti-html') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                dataType: 'json',
                beforeSend: function (xhr) {

                },
                success: function (data, textStatus, jqXHR) {
                    $('.append_new_row').append( data.html );
                    // $(".append_new_row select").select2();
                }
            });
        });

        $(document).on("change", ".country_id", function (event) {
            $th = $(this);
            $.ajax({
                type: 'POST',
                url: "{{ route('get-activiti-by-city') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id" :  $th.val(),
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Please wait..</option>');
                },
                success: function (data, textStatus, jqXHR) {
                    $th.closest('tr').find('td:eq(1) .tour_activity').html('<option value="" selected>Choose Activity</option>');

                    $.map(data.activities, function (item) {
                        $th.closest('tr').find('td:eq(1) .tour_activity').append('<option value="' + item.id + '">' + item.title + '</option>');
                    });
                }
            });
        });


        $( ".edit_country_id" ).each(function() {
        	$th = $(this);
        	$c_id = $th.attr("data-id");
            var activity_class = '.edit_tour_activity_'+$c_id;
            var activity_id = '.activity_id_'+$c_id;
            $.ajax({
                type: 'POST',
                url: "{{ route('get-activiti-by-city') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id" :  $th.val(),
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $(activity_class).html('<option value="" selected>Please wait..</option>');
                },
                success: function (data, textStatus, jqXHR) {
                    $(activity_class).html('<option value="" selected>Choose Activity</option>');
                    $.map(data.activities, function (item) {
                    	var selected = ''
                    	if($(activity_id).val() == item.id){
                    		selected = 'selected';
                    	}
                        $(activity_class).append('<option '+selected+' value="' + item.id + '">' + item.title + '</option>');
                    });
                }
            });
		});


    }); 


// 	$('.owl-carousel').owlCarousel({
// 		loop: true,
// 		margin: 10,
// 		dots: true,
// 		responsiveClass: true,
// <!-- navText : ['<i class="	fa fa-caret-left" aria-hidden="true"></i>','<i class="	fa fa-caret-right" aria-hidden="true"></i>'], -->
// 		responsive: {
// 			0: {
// 				items: 1,
// 				nav: true
// 			},
// 			600: {
// 				items: 2,
// 				nav: false
// 			},
// 			1000: {
// 				items: 3,
// 				nav: false,
// 				loop: false,
// 				margin: 20

// 			}
// 		}
// 	})
</script>
@endsection