<?php  
$pg_no = 1; $i = 0; 
if(isset($result['AirSearchResult']) && count($result['AirSearchResult']) > 0){
    foreach ($result['AirSearchResult'] as $key => $flight) { ?>
    <?php if($i == 0){ ?>
        <div class="page" id="page{{($pg_no++)}}">
    <?php } $i++; ?>
            <div class="flight-row ng-scope" id="row" ng-repeat="flight in flights|itemsPerPage:10">
                <div class="icon">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                </div>
                <div class="name ng-binding">
                     {{$flight['name']}}
                    <span class="ng-binding ng-scope">{{$flight['CarrierCode']}}-{{$flight['FlightNumber']}}</span>
                </div>
                <div class="flight-details">
                    <ul>
                        <li>
                            <span class="time">{{$flight['DepartureTime']}}</span>
                            {{$flight['DepartureAirport']}}
                        </li>
                        <li>
                            <span class="time">{{$flight['ArrivalTime']}}</span>
                            {{$flight['ArrivalAirport']}}
                        </li>
                        <li>
                            <span class="time">{{$flight['JourneyTime']}}</span>
                            {{$flight['StopoverCodes']}} stop
                        </li>                                    
                    </ul>
                </div>
                <div class="price ng-binding">
                    Rs. {{$flight['price']}}
                </div>
                <div class="book-now">
                    <button type="button" data-id='{{$flight['OptionId']}}' class="btn btn-primary book-now-button">Book Now</button>
                    <a href="javascript:void" class="more-details" data-id='{{$flight['OptionId']}}' >Flight Details <i data-id='{{$flight['OptionId']}}' class="fa fa-angle-down" aria-hidden="true"></i></a>
                </div>                            
                <div class="flightdetails" style="display:none">
                    <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>
                </div>
            </div>
    <?php if($i == 10 || count($result['AirSearchResult']) == ($key+1)){  
        echo '</div>'; $i = 0;  
            } 
        } ?>
    <ul id="pagination-demo" class="pagination-lg pull-right"></ul>   
    <input type="hidden"  id="count-records"value="{{count($result['AirSearchResult'])}}">
    <input type="hidden"  id="SearchFormData" value="{{ $SearchFormData }}">
<?php }else{
    echo "<h3><center>Flight not available</center></h3>";
} ?>