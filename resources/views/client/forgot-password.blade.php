@extends('layouts.client')

@section('content')
<style type="text/css">
    .error{
        color: red;
    }
    select.select-style{
        background-size: 6px 5px, 5px 5px, 3em 3.5em;
    }
</style>
<script type="text/javascript">
    cityData='';
</script>
<div class="container">
<br/>
@if(session()->has('forgot_success_message'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session()->get('forgot_success_message') }}
            </div>
        </div>
    </div>
@endif
@if(session()->has('forgot_error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session()->get('forgot_error') }}
            </div>
        </div>
    </div>
@endif
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('FORGOT PASSWORD') }}</div>
                <div class="card-body">
                    <form method="POST" id="ForgotpasswordForm" name="ForgotpasswordForm" action="{{route('forgot-password-request')}}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Email" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <a class="btn btn-primary submit-btn" href="{{ route('client_login_form') }}">
                                    {{ __('BACK') }}
                                </a>
                                
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary submit-btn">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!--  dropify -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/bower_components/dropify/dist/js/dropify.min.js"></script>
<!--  form file input functions -->
<script src="{{ env('APP_PUBLIC_URL')}}theme/assets/js/pages/forms_file_input.min.js"></script>
<script src="{{ env('APP_PUBLIC_URL')}}js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(function () {
    $("#ForgotpasswordForm").validate({
        rules: {
            email: "required"
        },
        messages: {
            email: "Please enter email"
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});
</script>
@endsection
