@extends('layouts.client')
<script>
var cityData = <?= $cities ?>;
</script>
@section('content')
<style type="text/css">
    .last_border{
        border-bottom: 1px solid #cccccc !important;
    }
</style>
<section>
    <div id = "myTabContent" class = "tab-content">
        <div  class="tab-pane fade in active" id="customizes-block" >
            <div class="customize">
                <div class="container">
                    <div class="row">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="tab-content">
                                    <div id="Tour" class="tab-pane fade active in">
                                        <div class="booking-block">
                                            <div class="booking-block-title" style="border-bottom:none;">
                                                <h3>Tour Inquire</h3>
                                            </div>
                                            <div class="holiday-booking-flight booking-block-form" style="margin-top: -10px;">
                                                <form name="book_frm" id="book_frm" action="" method="post" onsubmit="return false;">
                                                {{csrf_field()}}
                                                    <input type="hidden" name="tour_id" value="{{ isset($holiday->id) ? $holiday->id : '' }}" />
                                                    <div class="row booking-form-padding" style="display: none;">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <ul class="validate-msg"></ul>
                                                        </div>
                                                    </div>
                                                    <!--												<div class="row booking-form-padding">
                                                        <label class="radio-inline">Tour Details</label>
                                                    </div>-->
                                                    <?php /*
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="infinite-scroll">
                                                                <div class="holiday-list-item">
                                                                    <div class="row no-margin">
                                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-pad">
                                                                            <div class="holiday-place-image">
                                                                                <img src="{{ env('AWS_BUCKET')}}{{$tour->image}}" class="img-responsive"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 no-pad">
                                                                            <div class="holiday-tour-deatils">
                                                                                <div class="holiday-name-duration">
                                                                                    <div class="holiday-name">
                                                                                        <h3>{{$tour->tour_name}}  ({{$tour->tour_code}})</h3>
                                                                                    </div>
                                                                                    <div class="holiday-duration">
                                                                                        <h4>{{$tour->tour_nights}} Nights {{$tour->tour_nights + 1}} Days</h4>
                                                                                    </div>
                                                                                    <div class="holiday-packages">
                                                                                    Rs.{{ (isset($cost->total_cost)) ? number_format((float)$cost->total_cost, 2, '.', '') : '' }}/-
                                                                                    </div>
                                                                                    <div class="holiday-description">
                                                                                    <p>{{ (isset($tour->short_description)) ? substr($tour->short_description, 0, 150) : '' }}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="holiday-package-facilities">
                                                                                    <ul>
                                                                                    <?php
                                                                                    $features = array();
                                                                                    if (isset($tour->feature_data) && $tour->feature_data != '') {
                                                                                    $features = explode(',', $tour->feature_data);
                                                                                    }
                                                                                    ?>
                                                                                    @if(count($features)>0)
                                                                                    @foreach($features as $f_key => $feature)
                                                                                    <li><a href="#"><i class="fa fa-check"></i>{{$feature}}</a></li>
                                                                                    @endforeach
                                                                                    @endif
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    */?>
                                                    <?php if (isset($holiday) && !empty($holiday)) { ?>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="infinite-scroll">
                                                                <div class="holiday-list-item">
                                                                    <div class="row no-margin">
                                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-pad">
                                                                            <div class="holiday-place-image">
                                                                                @if(isset($holiday->image) && $holiday->image!='')
                                                                                <img src="{{ env('AWS_BUCKET')}}{{$holiday->image}}" class="img-responsive"/>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 no-pad">
                                                                            <div class="holiday-tour-deatils">
                                                                                <div class="holiday-name-duration">
                                                                                    <div class="holiday-name tour-title">
                                                                                        @if(isset($holiday->tour_name) && $holiday->tour_name!='')
                                                                                        <h3>{{$holiday->tour_name}}</h3>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="holiday-duration">
                                                                                        @if(isset($holiday->tour_nights) && $holiday->tour_nights!='')
                                                                                        <h4>{{$holiday->tour_nights}} Nights {{$holiday->tour_nights + 1}} Days <span> - </span> <span class="holiday-tour-rating"> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></h4>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                                <?php if(isset($cost['amount']) && count($cost['amount']) > 0){?>
                                                                                <div class="holiday-packages">
                                                                                @foreach($cost['amount'] as $c => $cost_val)
                                                                                @foreach($cost_val['rates'] as $r => $rate)
                                                                                @if(isset($rate['sell']) && $rate['sell'] > 0)
                                                                                <label class="radio-inline"><input type="radio" class="holydayHotelRadio" holyday="{{$holiday->id}}" package="{{$c}}" name="holydaycost{{$holiday->id}}" data-package_type="{{$c}}" {{($c==1)?"checked":""}}>{{$cost_val['name']}} <?php /*
                                                                                ({{($rate['sell'])-($rate['sell']*$cost['dates']['discount'])/100}}) */?></label>
                                                                                @endif
                                                                                @break;
                                                                                @endforeach
                                                                                @endforeach
                                                                                </div>
                                                                                <?php } ?>
                                                                                <?php if (isset($hotel) && count($hotel) > 0) { ?>
                                                                                <div class="holiday-packages-details">
                                                                                    <table class="table table-responsive hotel_{{$holiday->id}} last_border" style="margin-top: 10px;">
                                                                                    <tr class="hotel_label">
                                                                                        <td><h4><strong>Location</strong></h4></td>
                                                                                        <td><h4><strong>Hotel</strong></h4></td>
                                                                                        <td><h4><strong>Nights</strong></h4></td>
                                                                                        <td><h4><strong>Rating</strong></h4></td>
                                                                                    </tr>
                                                                                    @foreach($hotel as $h => $hotel_val)
                                                                                    <tr class="{{($hotel_val->package_type!=1)?'hide':''}} package_{{$holiday->id}}{{$hotel_val->package_type}}">
                                                                                        <td><h4>{{isset($hotel_val->location)?$hotel_val->location:''}}</h4></td>
                                                                                        <td><h4>{{isset($hotel_val->hotel)?$hotel_val->hotel:''}}</h4></td>
                                                                                        <td><h4>{{isset($hotel_val->night)?$hotel_val->night:''}}N</h4></td>
                                                                                        <td><h4>{{isset($hotel_val->hotel_star)?$hotel_val->hotel_star:''}}<i class="fa fa-star"></i></h4></td>
                                                                                    </tr>
                                                                                    @endforeach
                                                                                    </table>
                                                                                </div>
                                                                                <?php } ?>
                                                                                
                                                                                <div class="holiday-package-facilities">
                                                                                    <ul>
                                                                                        <?php
                                                                                            $features = array();
                                                                                            if (isset($holiday->feature_data) && $holiday->feature_data != '') {
                                                                                                $features = explode(',', $holiday->feature_data);
                                                                                            }
                                                                                        ?>
                                                                                            @if(count($features)>0)
                                                                                            @foreach($features as $f_key => $feature)
                                                                                            <li><a href="#"><i class="fa fa-check"></i>{{$feature}}</a></li>
                                                                                            @endforeach
                                                                                            @endif
                                                                                    </ul>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <?php if(isset($cost['amount']) && count($cost['amount']) > 0){?>
                                                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                                            <div class="holiday-tour-fare cost-{{$holiday->id}}">
                                                                                <?php /*
                                                                                @foreach($cost['amount'] as $c => $cost_val)
                                                                                @foreach($cost_val['rates'] as $r => $rate)
                                                                                <div class="amount package_cost_{{$holiday->id}}{{$c}} {{($c!=1)?'hide':''}}">
                                                                                    @if(isset($rate['sell']) && $rate['sell'] > 0)
                                                                                    @if($cost['dates']['discount']>0)
                                                                                    <div class="actual-fare">
                                                                                        <h4><img style="width: 15px" src="{{  isset($cost['dates']['currency_icon']) ?  env('APP_PUBLIC_URL').'uploads/currency_icons/'.$cost['dates']['currency_icon'] : '' }}">{{$rate['sell']}}</h4>
                                                                                    </div>
                                                                                    @endif
                                                                                    <div class="discount-fare" style="margin-top: -10px;">
                                                                                        <h2><img style="width: 30px"  src="{{  isset($cost['dates']['currency_icon']) ?  env('APP_PUBLIC_URL').'uploads/currency_icons/'.$cost['dates']['currency_icon'] : '' }}">{{($rate['sell'])-($rate['sell']*$cost['dates']['discount'])/100}}</h2>
                                                                                    </div>
                                                                                    <div class="toal-persons" style="margin-top: -25px;">
                                                                                        <h4>{{$rate['name']['description']}}</h4>
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                @break;
                                                                                @endforeach
                                                                                @endforeach
                                                                                */?>
                                                                            </div>
                                                                        </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="row booking-form-padding sub-title">
                                                        <label class="radio-inline inquire-tour">Fill below detail to inquire about this tour :</label>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Name</label>
                                                                <input type="text" value="{{(isset($user_data->first_name) && isset($user_data->last_name) ? $user_data->first_name.' '.$user_data->last_name :'')}}" name="contact_name" class="form-control" placeholder="Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Contact No</label>
                                                                <input type="text"  value="{{(isset($user_data->phone_no) ? $user_data->phone_no :'')}}" name="contact_no" class="form-control" placeholder="Contact No">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Email Id</label>
                                                                <input type="text"  value="{{(isset($user_data->email) ? $user_data->email :'')}}"  name="email_id" class="form-control" placeholder="Email Id">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Adults</label>
                                                                <input type="text" name="adults" class="form-control" placeholder="Adults" min="1" max="10">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Child</label>
                                                                <input type="text" name="child" class="form-control" placeholder="Child" min="1" max="10">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Infants</label>
                                                                <input type="text" name="infants" class="form-control" placeholder="Infants" min="1" max="10">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Interested Date</label>
                                                                <input type="text" name="interested_date" class="form-control datepicker " readonly="" placeholder="Interested Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">Hotel Category</label>
                                                                <select name="hotel_category" class="select-style">
                                                                    <option value="">Category</option>
                                                                    <?php if(isset($cost['amount']) && count($cost['amount']) > 0){?>
                                                                    @foreach($cost['amount'] as $c => $cost_val)
                                                                    @foreach($cost_val['rates'] as $r => $rate)
                                                                    @if(isset($rate['sell']) && $rate['sell'] > 0)
                                                                    <?php /*
                                                                    <option value="{{$c}}" {{($c==1)?"selected":""}}>{{$cost_val['name']}}</option>
                                                                    */ ?>
                                                                    <option value="{{$c}}">{{$cost_val['name']}}</option>
                                                                    @endif
                                                                    @break;
                                                                    @endforeach
                                                                    @endforeach
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="row booking-form-padding col-lg-12 sub-title">
                                                    <label class="radio-inline">Contact Details</label>
                                                    </div> -->
                                                    
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12 ">
                                                            <div class="input-group">
                                                                <label class="radio-inline nopadding">More Details</label>
                                                                <textarea name="notes" class="form-control" placeholder="More Details"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row booking-form-padding col-lg-12">
                                                        <button class=" btn-holiday btn-desktop-right tour_submit_btn">SUBMIT</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection
@section('javascript')
<script type='text/javascript'>
    $(function () {

        $('input[type=radio][class=holydayHotelRadio]').change(function () {
            var holyday = $(this).attr('holyday');
            var package = $(this).attr('package');

            $('div.cost-' + holyday).find('div.amount').addClass('hide');
            $('div.cost-' + holyday).find('div.package_cost_' + holyday + package).removeClass('hide');

            $('table.hotel_' + holyday + ' tr:gt(0)').addClass('hide');
            $('table.hotel_' + holyday + ' tr.package_' + holyday + package).removeClass('hide');

            var tr_length = $('table.hotel_' + holyday + ' tr.package_' + holyday + package).length;
            if(tr_length==0){
                $('table.hotel_' + holyday + ' tr.hotel_label').addClass('hide');
            }else{
                $('table.hotel_' + holyday + ' tr.hotel_label').removeClass('hide');
            }

            var inquire = $(this).parents('div.holiday-list-item').find('div.inquire a');
            if(inquire.length){
                var oldLink = inquire.attr('href');
                oldLink = oldLink.substr(0, oldLink.lastIndexOf('/'));
                inquire.attr('href', oldLink + "/" + package);
            }
        });
        $('.holiday-list-item').each(function (i, obj) {
            if (!$(obj).find('input[type=radio][class=holydayHotelRadio]:first').prop('checked')) {
                $(obj).find('input[type=radio][class=holydayHotelRadio]:first').prop("checked", true).trigger("change");
            }
        });
    });

</script>
@endsection