@extends('layouts.client')

@section('content')
<script type="text/javascript">
    cityData = '';
</script>
<section>
    
    <div class="dashboard-tabs">
        <div class="dashboard-tabs-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <h2 class="dashboard-title">My Bookings</h2>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <ul class="nav nav-pills pull-right">
                            <li class="active"><a data-toggle="pill" href="#tab-lead">My Inquire </a></li>
                            <li><a data-toggle="pill" href="#lead-quotations">My Quotation</a></li>
                            <li><a data-toggle="pill" href="#conform-lead">My Confirmation</a></li>
                            <li><a data-toggle="pill" href="#wallet">Wallet</a></li>
                            <li><a href="{{ route('user.profile.page') }}" title="Profile">Profile</a></li>
                            <li><a href="{{ route('client_logout') }}" title="Logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="dashboard-tabs-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tab-content">
                            <div id="tab-lead" class="tab-pane fade active in">
                                <div class="dashboard-list-item " style="border:none">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="lead_tbl">
                                                <thead>
                                                    <tr>
                                                        <th style="width:25px;">Lead ID</th>
                                                        <th>Date</th>
                                                        <th>User Info</th>
                                                        <th>Interest</th>
                                                        <th>Managed By</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="lead-quotations" class="tab-pane fade">
                                <div class="dashboard-list-item " style="border:none">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="table-responsive">
                                             <input type="hidden" name="lead_id_for_quotation" id="lead_id_for_quotation" value="0">
                                             <table class="table table-bordered" id="quotation_tbl" style="margin-top: 20px;" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th width="10%">Quotation ID</th>
                                                        <th width="10%">Date</th>
                                                        <th width="15%">Quotation Title</th>
                                                        <th width="10%">Adult/Child/Infant</th>
                                                        <th width="10%">Amount</th>
                                                        <th width="15%">Agent</th>
                                                        <th width="10%">Status</th>
                                                        <th width="10%">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody> </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="conform-lead" class="tab-pane fade">
                                <div class="dashboard-list-item " style="border:none">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="conform-lead-table">
                                                <thead>
                                                    <tr>
                                                        <th style="width:25px;">Lead ID</th>
                                                        <th>Quotation ID</th>
                                                        <th>Date</th>
                                                        <th>User Info</th>
                                                        <th>Interest</th>
                                                        <th>Managed By</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody> </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="wallet" class="tab-pane fade">
                                <div class="dashboard-list-item " style="border:none">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="wallet-table">
                                                <thead>
                                                    <tr>
                                                        <th style="width:30px;">Date</th>
                                                        <th>Withdrawal</th>
                                                        <th>Deposit</th>
                                                        <th>Closing balance</th>
                                                    </tr>
                                                </thead>
                                                <tbody> </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal" id="quotation_update_status">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Update Status</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <select class="form-control" id="quotation_status_id" title="Select Status...">
                            <option value="0">Select Status...</option>
                            <option value="3">Quote Approved</option>
                            <option value="4">Quote Rejected</option>
                        </select>
                    </div>
                </div>
                <input type="hidden" value="" id="quotation-change-status-row-id" />
            </div>
            <div class="modal-footer">
                <button type="button" id="quotation-status-dlg-btn" class="btn btn-info" >Submit</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div> 
@endsection

@section('javascript')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
@endsection