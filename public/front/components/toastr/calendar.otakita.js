function Calendar(a, b, c) {
    this.dataEvent = a, this.month = isNaN(b) || null == b ? cal_current_date.getMonth() : b, this.year = isNaN(c) || null == c ? cal_current_date.getFullYear() : c, this.html = ""
}
cal_days_labels = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], cal_months_labels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], cal_days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], cal_current_date = new Date, String.prototype.paddingLeft = function (a) {
    return String(a + this).slice(-a.length)
}, _generateCalendar = function (a, b, c) {
    var d = new Date,
            e = new Date(c, b, 1),
            f = e.getDay(),
            g = cal_days_in_month[e.getMonth()],
            h = cal_days_in_month[e.getMonth() - 1 < 0 ? 11 : e.getMonth() - 1];
    (c % 4 == 0 && c % 100 != 0 || c % 400 == 0) && (1 == e.getMonth() && (g = 29), e.getMonth() - 1 == 1 && (h = 29));
    var i = '<div class="calendar-header">' + cal_months_labels[e.getMonth()] + " " + e.getFullYear() + '</div><div class="navigation">';
    a.canBack && (monthBack = e.getMonth() - 1 < 0 ? 11 : e.getMonth() - 1, yearBack = 11 == monthBack ? e.getFullYear() - 1 : e.getFullYear(), i += '<span class="calendar-nav" data-month="' + monthBack + '" data-year="' + yearBack + '">&lt; back</span>'), a.canNext && (monthNext = e.getMonth() + 1 == 12 ? 0 : e.getMonth() + 1, yearNext = 0 == monthNext ? e.getFullYear() + 1 : e.getFullYear(), i += ' <span class="calendar-nav" data-month="' + monthNext + '" data-year="' + yearNext + '">next &gt;</span>'), i += "</div>", i += '<table class="table-calendar">', i += "<tr>";
    for (var j = 0; j <= 6; j++)
        i += '<td class="table-calendar-header">', i += cal_days_labels[j], i += "</td>";
    i += "</tr><tr>";
    for (var k = 1, l = 1, j = 0; j < 24; j++) {
        for (var m = 0; m <= 6; m++) {
            var n = 0,
                    o = "",
                    p = "",
                    q = "",
                    r = "",
                    s = "";
            if (k <= g && (j > 0 || m >= f)) {
                var t = k,
                        u = e.getMonth() + 1,
                        v = e.getMonth(),
                        w = e.getFullYear();
                1 == k && (s = " " + cal_months_labels[e.getMonth()]), k++
            } else if (0 == j)
                var t = h - f + m + 1,
                        u = 0 == e.getMonth() ? 11 : e.getMonth(),
                        v = e.getMonth() - 1 < 0 ? 11 : e.getMonth() - 1,
                        w = e.getMonth() - 1 < 0 ? e.getFullYear() - 1 : e.getFullYear();
            else {
                var t = l,
                        u = e.getMonth() + 2 > 12 ? e.getMonth() + 2 > 13 ? 2 : 1 : e.getMonth() + 2,
                        v = e.getMonth() + 1 > 11 ? 0 : e.getMonth() + 1,
                        w = e.getMonth() + 1 > 11 ? e.getFullYear() + 1 : e.getFullYear();
                1 == l && (s = " " + cal_months_labels[e.getMonth() + 1 > 11 ? 0 : e.getMonth() + 1]), l++
            }
            var x = new Date(w, v, t),
                    y = x.getTime() - d.getTime(),
                    z = Math.ceil(y / 864e5);
            z < 0 && (q = "calendar-expired"), n = a["price" + x.getDay()];
            var A = a.event[u.toString().paddingLeft("00") + "-" + t.toString().paddingLeft("00") + "-" + w];
            "undefined" != typeof A && ("undefined" != typeof A.price && (n = A.price), "undefined" != typeof A.disabled && "" == q && (p = "calendar-disabled")), "" == q && (r = "calendar-active"), i += '<td class="cal_row"><div class="containingBlock ' + p + " " + o + " " + q + " " + r + '" ' + ("" == p ? 'title="Available"' : 'title="Unavailable"') + ' data-value="' + w + "-" + u.toString().paddingLeft("00") + "-" + t.toString().paddingLeft("00") + '">', i += '<div class="day">' + t + s + "</div>", i += '<div class="price">' + parseFloat(n).format(2, 3, ".", ",") + "</div>", i += "</div></td>"
        }
        i += "</tr><tr>"
    }
    return i += "</tr></table>"
}, Calendar.prototype.generateHTML = function () {
    var a = "";
    a += _generateCalendar(this.dataEvent, this.month, this.year), this.html = a
}, Calendar.prototype.next = function () {}, Calendar.prototype.back = function () {}, Calendar.prototype.getHTML = function () {
    return this.html
}, Number.prototype.format = function (a, b, c, d) {
    var e = "\\d(?=(\\d{" + (b || 3) + "})+" + (a > 0 ? "\\D" : "$") + ")",
            f = this.toFixed(Math.max(0, ~~a));
    return (d ? f.replace(".", d) : f).replace(new RegExp(e, "g"), "$&" + (c || ","))
};