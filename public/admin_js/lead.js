$(document).ready(function () {
	$("#tour_id").change(function () {
		if ($(this).val() == "") {
			var options = '<option value="">Confirm Departure Date</option>';
			$("#departure_date").html(options);
			return false;
		}
		$.ajax({
			type: "GET",
			url: ADMIN_URL + 'lead/get-departure-date/' + $(this).val(),
			dataType: 'json',
			beforeSend: function () {

			},
			success: function (data) {
				if (data.success == true) {
					var options = '<option value="">Confirm Departure Date</option>';
					$(data.departure_date).each(function (i, row) {
						options += '<option value="' + row.id + '">' + row.departure_date + '</option>';
					});
					$("#departure_date").html(options);
				} else {

				}
			},
		});
		return false;
	});
	$(".add-new-room").click(function () {
		$(".accomodation-wrap").append($("#accomodation").html());
		$(".accomodation-wrap").find(".accomodation-drop-down").addClass("required_field")
	});
	$("body").on("change", ".accomodation-drop-down", function () {
		var obj = $(this);
		if (obj.val() == "") {
			return false;
		}
		var tour_id = $("#tour_id").val();
		var total_element = $(".accomodation-drop-down").length - 2;
		var tour_type = $("#tour_type").val();
		$.ajax({
			type: "GET",
			url: ADMIN_URL + 'lead/get-accomodation-data/' + obj.val() + '/' + tour_id + '/' + tour_type + '/' + total_element,
//			dataType: 'json',
			beforeSend: function () {

			},
			success: function (data) {
				if (data) {
					obj.closest(".uk-form-row").next().html(data);
					calculation();
				} else {

				}
			},
		});
	});
	$("#tour_id, #departure_date, #tour_type").click(function () {
		if (parseInt($("#tour_id").val()) > 0 && parseInt($("#departure_date").val()) > 0 && parseInt($("#tour_type").val()) > 0) {
			$(".add-new-room").show();
		} else {
			$(".add-new-room").hide();
		}
	});

	$("body").on("change", ".child_age_group", function () {
		var amount = $('option:selected', this).attr('amount');
		$(this).parent().next().find(".amount_txt").val(amount);
		calculation();
	});
	$("body").on("change", "input[name=discount_amount]", function () {
		calculation();
	});
	$("body").on("change", "input[name=discount]", function () {
		calculation();
	});

	$("body").on("click", ".tmp-remove-room", function () {
		if (confirm("Are you sure to remove room?")) {
			$(this).closest(".customer-row").prev().remove();
			$(this).closest(".customer-row").prev().remove();
			$(this).closest(".customer-row").remove();
			calculation();
		}
	});
	$("body").on("click", ".added-remove-room", function () {
		if (confirm("Are you sure to remove room?")) {
			var obj = $(this);
			$.ajax({
				type: "GET",
				url: ADMIN_URL + 'lead/remove-room/' + obj.attr("acc_id"),
				beforeSend: function () {
					obj.prop("disabled", true).css("opacity", "0.5");
				},
				success: function (data) {
					if (data.success == true) {
						obj.closest(".customer-row").prev().remove();
						obj.closest(".customer-row").prev().remove();
						obj.closest(".customer-row").remove();
						obj.prop("disabled", false).css("opacity", "1");
						calculation();
					} else {
						obj.prop("disabled", false).css("opacity", "1");
					}
				},
			});
		}
	});
	$("body").on("click", ".save_confirm_lead", function () {
		var is_valid = true;
		$(".required_field").each(function () {
			$(this).removeClass("error-border");
			if ($(this).val() == "") {
				if (is_valid) {
					$(this).focus();
				}
				$(this).addClass("error-border");
				is_valid = false;
			}
		});
		if (!is_valid) {
			return false;
		}
		if ($(".accomodation-wrap").find(".uk-form-row").length == 0) {
			alert("Please add room");
			return false;
		}
		$.ajax({
			type: "POST",
			url: ADMIN_URL + 'lead/save-confirm-lead',
			data: $("#confirm_lead_frm").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$(".save_confirm_lead").prop("disabled", true).css("opacity", "0.5");
			},
			success: function (data) {
				if (data) {
					window.location.href = ADMIN_URL + 'lead';
				} else {
					$(".save_confirm_lead").prop("disabled", false).css("opacity", "1");
				}
			},
		});
	});

});
function calculation() {
	var sub_total1 = 0;
	$(".amount_txt").each(function () {
		if (parseInt($(this).val()) > 0) {
			sub_total1 = parseInt(sub_total1) + parseInt($(this).val());
		}
	});
	if (parseInt($("input[name=discount]").val()) > 0) {
		var discount_amount = (sub_total1 * $("input[name=discount]").val()) / 100;
		$("input[name=discount_amount]").val(discount_amount);
	}
	$("input[name=sub_total1]").val(sub_total1);
	var sub_total2 = parseInt(sub_total1) - parseInt($("input[name=discount_amount]").val());
	$("input[name=sub_total2]").val(sub_total2);
	var igst = (sub_total2 * 5) / 100;
	$("input[name=igst]").val(parseInt(igst));
	$("input[name=total_amount]").val(parseInt(sub_total2) + parseInt(igst));
}