$(function () {
    $('#country_id').change(function () {
        var country_id = $(this).val();
        var url = ADMIN_URL + 'home/getstate/' + country_id;

        $.ajax({
            type: 'GET',
            url: url,
            data: {
//                    "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#state_id')
                        .find('option')
                        .remove();
                $('#state_id').append('<option value="">Choose State</option>');
                $.map(data, function (item) {
                    $('#state_id').append('<option value="' + item.state_id + '">' + item.state_name + '</option>');
                });
                $("#state_id").trigger("chosen:updated");
                $("#state_id").trigger("liszt:updated");
            }
        });
    });

    $('#state_id').change(function () {
        var state_id = $(this).val();
        var url = ADMIN_URL + 'home/getcity/' + state_id;

        $.ajax({
            type: 'GET',
            url: url,
            data: {
//                    "_token": "{{ csrf_token() }}",
            },
            dataType: 'json',
            beforeSend: function (xhr) {

            },
            success: function (data, textStatus, jqXHR) {
                $('#city_id')
                        .find('option')
                        .remove();
                $('#city_id').append('<option value="">Choose City</option>');
                $.map(data, function (item) {
                    $('#city_id').append('<option value="' + item.city_id + '">' + item.city_name + '</option>');
                });
                $("#city_id").trigger("chosen:updated");
                $("#city_id").trigger("liszt:updated");
            }
        });
    });
});