function isInView(elem) {
	if (!$(".viewMore").length > 0) {
		return false;
	}
	var docViewTop = $(window).scrollTop();
	var docViewBottom = docViewTop + $(window).height();
	var elemTop = $(elem).offset().top;
	var elemBottom = elemTop + $(elem).height();

	return ((docViewTop < elemTop) && (docViewBottom > elemBottom));
}
$(document).ready(function () {

	// This button will increment the value
	$(document).on('click', '.auto-incriment', function(e){
     	if($(this).attr('data-type') == 'plus'){
	        e.preventDefault();
	        fieldName = $(this).attr('data-field');
	        var currentVal = parseInt($('input[name="'+fieldName+'"]').val());
	        if (!isNaN(currentVal)) {
	            $('input[name="'+fieldName+'"]').val(currentVal + 1);
	        } else {
	            $('input[name="'+fieldName+'"]').val(0);
	        }
	    }

	    if($(this).attr('data-type') == 'minus'){
	        e.preventDefault();
	        fieldName = $(this).attr('data-field');
	        var currentVal = parseInt($('input[name="'+fieldName+'"]').val());
	        if (!isNaN(currentVal) && currentVal > 0) {
	            $('input[name="'+fieldName+'"]').val(currentVal - 1);
	        } else {
	            $('input[name="'+fieldName+'"]').val(0);
	        }
	    }
    });
    
	$(".destination_add_btn").click(function () {
		var i = $(".destination_row").length;
		var html = '<tr class="destination_row">'
				+ '<td>'
				+ '<input type="text" name="destination[city_name][]" class="form-control autocomplete" placeholder="Search by city name">'
				+ '<input type="hidden" name="destination[city_id][]">'
				+ '</td>'
				+ '<td>'
				+ '<div class="input-group" style="width:120px">'
				+ '<span class="input-group-btn">'
				+ '<button type="button" class="btn btn-default auto-incriment"  data-type="minus" data-field="destination[number_of_nights][]">'
				+ '<span class="glyphicon glyphicon-minus"></span>'
				+ '</button>'
				+ '</span>'
				+ '<input type="text" name="destination[number_of_nights][]" value="1" class="form-control input-number" min="1" max="10">'
				+ '<span class="input-group-btn">'
				+ '<button type="button" class="btn btn-default auto-incriment" data-type="plus" data-field="destination[number_of_nights][]">'
				+ '<span class="glyphicon glyphicon-plus"></span>'
				+ '</button>'
				+ '</span>'
				+ '</div>'
				+ '</td>'
				+ '<td>'
				+ '<button type="button" class="btn btn-default btn-number destination_delete_btn" data-type="plus" data-field="quant[1]">'
				+ '<span class="glyphicon glyphicon-minus"></span>'
				+ '</button>'
				+ '</td>'
				+ '</tr>';
		$(html).insertBefore(".add_btn_tr");
		$(".autocomplete").autocomplete({
			source: cityData,
			minLength: 3,
			focus: function (event, ui) {
				event.preventDefault();
				$(this).val(ui.item.label);
			},
			select: function (event, ui) {
				event.preventDefault();
				$(this).val(ui.item.label);
				$(this).next().val(ui.item.value);
			}
		});
	});
	$("body").on("click", ".destination_delete_btn", function () {
		$(this).closest("tr").remove();
	});
	$("body").on("click", ".glyphicon-plus", function () {
		var obj = $(this).closest(".input-group").find(".input-number");
		if (obj.val() == '') {
			var total = parseInt(0) + parseInt(1);
		} else {
			var total = parseInt(obj.val()) + parseInt(1);
		}
//		$(".glyphicon-minus").parent("disabled", false);
		$(obj).val(total);
	});
	$("body").on("click", ".glyphicon-minus", function () {
		var obj = $(this).closest(".input-group").find(".input-number");
		if (obj.val() == 1) {
//			$(this).parent("disabled", true);
		}
		if (obj.val() == 0) {
			return false;
		} else {
			var total = parseInt(obj.val()) - parseInt(1);
		}
		console.log(total);
		$(obj).val(total);
	});
	$("#flight_frm").submit(function () {
		$.ajax({
			url: APP_URL + 'home/flight-data-store',
			type: "POST",
			data: $("#flight_frm").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#flight_frm .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".flight_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#flight_frm .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
					$('#flight_frm')[0].reset();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#flight_frm .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#flight_frm").offset().top
				}, 1000);
				$(".flight_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$("#hotel_frm").submit(function () {
		$.ajax({
			url: APP_URL + 'home/hotel-data-store',
			type: "POST",
			data: $("#hotel_frm").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#hotel_frm .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".hotel_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#hotel_frm .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
					$('#hotel_frm')[0].reset();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#hotel_frm .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#hotel_frm").offset().top
				}, 1000);
				$(".hotel_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$("#visa_frm").submit(function () {
		$.ajax({
			url: APP_URL + 'home/visa-data-store',
			type: "POST",
			data: $("#visa_frm").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#visa_frm .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".visa_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#visa_frm .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
					$('#visa_frm')[0].reset();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#visa_frm .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#visa_frm").offset().top
				}, 1000);
				$(".visa_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$("#holiday_frm").submit(function () {
		$.ajax({
			url: APP_URL + 'home/holiday-data-store',
			type: "POST",
			data: $("#holiday_frm").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#holiday_frm .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".holiday_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#holiday_frm .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
					$('#holiday_frm')[0].reset();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#holiday_frm .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#holiday_frm").offset().top
				}, 1000);
				$(".holiday_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$(".onlynumber").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
				// Allow: Ctrl+A, Command+A
						(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
						// Allow: home, end, left, right, down, up
								(e.keyCode >= 35 && e.keyCode <= 40)) {
					// let it happen, don't do anything
					return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			});
	$(".autocomplete").autocomplete({
		source: cityData,
		minLength: 3,
		focus: function (event, ui) {
			event.preventDefault();
			$(this).val(ui.item.label);
		},
		select: function (event, ui) {
			event.preventDefault();
			$(this).val(ui.item.label);
			$(this).next().val(ui.item.value);
		}
	});

	$("#flight_frm_update").submit(function () {
		$.ajax({
			url: APP_URL + 'home/flight-data-update',
			type: "POST",
			data: $("#flight_frm_update").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#flight_frm_update .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".flight_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#flight_frm_update .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#flight_frm_update .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#flight_frm_update").offset().top
				}, 1000);
				$(".flight_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$("#hotel_frm_update").submit(function () {
		$.ajax({
			url: APP_URL + 'home/hotel-data-update',
			type: "POST",
			data: $("#hotel_frm_update").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#hotel_frm_update .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".hotel_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#hotel_frm_update .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#hotel_frm_update .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#hotel_frm_update").offset().top
				}, 1000);
				$(".hotel_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$("#holiday_frm_update").submit(function () {
		$.ajax({
			url: APP_URL + 'home/holiday-data-update',
			type: "POST",
			data: $("#holiday_frm_update").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#holiday_frm_update .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".holiday_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#holiday_frm_update .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#holiday_frm_update .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#holiday_frm_update").offset().top
				}, 1000);
				$(".holiday_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$("#visa_frm_update").submit(function () {
		$.ajax({
			url: APP_URL + 'home/visa-data-update',
			type: "POST",
			data: $("#visa_frm_update").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#visa_frm_update .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".visa_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#visa_frm_update .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#visa_frm_update .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#visa_frm_update").offset().top
				}, 1000);
				$(".visa_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$("#book_frm").submit(function () {
		$.ajax({
			url: APP_URL + 'home/book-store',
			type: "POST",
			data: $("#book_frm").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#book_frm .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".tour_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#book_frm .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
					$('#book_frm')[0].reset();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#book_frm .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#book_frm").offset().top
				}, 1000);
				$(".tour_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
	$("#update_book_frm").submit(function () {
		$.ajax({
			url: APP_URL + 'home/book-data-update',
			type: "POST",
			data: $("#update_book_frm").serialize(),
			dataType: 'json',
			beforeSend: function () {
				$('#update_book_frm .validate-msg').html("").closest('.booking-form-padding').hide();
				$(".tour_submit_btn").attr("disabled", true).css("opacity", "0.4");
			},
			success: function (data) {
				if (data.success == true) {
					$('#update_book_frm .validate-msg').html('<div class="alert alert-success alert-dismissible fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> ' + data.msg + '</div>').removeClass('error-msg').addClass("success-msg").closest('.booking-form-padding').show();
				} else {
					var error_msg = '';
					$(data.errors).each(function (row, val) {
						error_msg += '<li>' + val[1] + '</li>';
					});
					$('#update_book_frm .validate-msg').html(error_msg).removeClass('success-msg').addClass("error-msg").closest('.booking-form-padding').show();
				}
				$('html, body').animate({
					scrollTop: $("#update_book_frm").offset().top
				}, 1000);
				$(".tour_submit_btn").attr("disabled", false).css("opacity", "1");
			},
		});
	});
    var lead_id_for_quotation= $("#lead_id_for_quotation").val();
    var quotationTable = $('#quotation_tbl').DataTable({

        processing: true,
        serverSide: true,
        pageLength: 20,
        lengthMenu: [ 10, 20, 50, 75, 100 ],
        destroy: true,
        order: [[ 1, "desc" ]],
        "ajax": {
            "url": APP_URL+'dashboard/get_quotation/'+lead_id_for_quotation,
            "dataType": "json",
            "type": "POST",
            "data": {_token: Laravel.csrfToken}, 
            beforeSend: function () {

                if (typeof quotationTable != 'undefined' && quotationTable.hasOwnProperty('settings')) {
                    quotationTable.settings()[0].jqXHR.abort();
                }
            }
        },
        "columns": [
        	{"data": "quatation_id", "name": "lead_quatation.quatation_id"},
        	// {"data": "quaId", "name": "lead_quatation.quaId"},
            {"data": "created_at", "name": "lead_quatation.created_at"},
            {"data": "subject", "name": "lead_quatation.subject"},
            {"data": "members", "name": "members"},
            {"data": "total_amount", "name": "lead_quatation.total_amount"},
            {"data": "full_name", "name": "full_name"},
            {"data": "status_id","name": "status_id"},
            {"data": "action", "sortable": false,"name": "action"}
        ]
    });

    $(quotationTable.table().container()).on('keyup change', 'tfoot input', function () {
        quotationTable
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });
    $(quotationTable.table().container()).on('change', 'tfoot select', function () {
        quotationTable
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

    $(document.body).on('click', '.quotation-change-status-btn', function () {
        var $obj = $(this);
        var row_id = $obj.attr('row-id');
        var status = $obj.attr('data-id');
        $('input#quotation-change-status-row-id').val(row_id);
        if (status) {
            $("select#quotation_status_id option[value='" + status + "']").attr("selected", "selected");
        }
    });

    $(document.body).on('click', '#quotation-status-dlg-btn', function () {
        var $obj = $(this);
        var btn_txt = $obj.text();
        var row_id = $('#quotation-change-status-row-id').val();
        var status = $('select#quotation_status_id').val();
        if (row_id == false && status == false) {
            return false;
        }
        $.ajax({
            type: 'get',
            "url": APP_URL + 'dashboard/change_status',
            data: {
                "_token": Laravel.csrfToken,
                "table": "lead_quatation",
                "id": row_id,
                "column_name": "quaId",
                "column_update": "status_id",
                "status": status
            },
            dataType: 'json',
            beforeSend: function (xhr) {
                $obj.prop('disabled', true);
                $obj.text('Wait...');
            },
            success: function (data, textStatus, jqXHR) {
            	$obj.text(btn_txt);
            	$obj.prop('disabled', false);
            	if (data.success) {
            	    quotationTable.draw();
            	}
            	$('select#quotation_status_id option:selected', this).removeAttr('selected');
            	$('#quotation_update_status').modal('hide');
            }
        });
    });

    $(document).on('change', '#quotation_info_status_id', function () {
    	if($('#quotation_info_status_id').val() == ''){
    		return false;
    	}
    	if (confirm('Are you sure to change status?')) {
	
            var $obj = $(this);
            var row_id = $('#quotation-change-status-row-id').val();
            var status = $('select#quotation_info_status_id').val();
            if (row_id == false && status == false) {
                return false;
            }
            $.ajax({
                type: 'get',
                "url": APP_URL + 'dashboard/change_status',
                data: {
                    "_token": Laravel.csrfToken,
                    "table": "lead_quatation",
                    "id": row_id,
                    "column_name": "quaId",
                    "column_update": "status_id",
                    "status": status
                },
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                	//console.log('HEllo');
                    location.reload(); 
                    // showNotify(data.msg_status, data.message);
                }
            });
        }
    });

    $(".quote_change_status").click(function(){
	    $("#quotation_info_status_id").toggle();
	    $(".quote_status_view").toggle();
	});
    
    var homebannerTable = $('#lead_tbl').DataTable({
        processing: true,
        serverSide: true,
        pageLength: 20,
        lengthMenu: [ 10, 20, 50, 75, 100 ],
        destroy: true,
        oSearch: { "bSmart": false, "bRegex": true },
        order: [[ 1, "desc" ]],
        "ajax": {
            "url":  APP_URL + 'user-lead/data',
            "dataType": "json",
            "type": "POST",
            "data": {_token: Laravel.csrfToken}, //, 'category_name': $("#Scategory_name").val()
            beforeSend: function () {

                if (typeof homebannerTable != 'undefined' && homebannerTable.hasOwnProperty('settings')) {
                    homebannerTable.settings()[0].jqXHR.abort();
                }
            }
        },
        "columns": [
			{"data": "lead_id", "name": "lead_id"},
			{"data": "created_at", "name": "created_at"},
			{"data": "user_info", "name": "user_info",render:function(data,type,row){
				return data.split('\n').join('<br>');
			}},
			{"data": "interested_in", "name": "interested_in"},
			{"data": "assign_to_user", "name": "assign_to_user"},
			{"data": "status_name", "name": "status_name"},
			{"data": "action", "sortable": false,"name": "action"}

        ]
    });
    $(homebannerTable.table().container()).on('keyup', 'tfoot input', function () {
        homebannerTable
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

    $(homebannerTable.table().container()).on('change', 'tfoot select', function () {
        homebannerTable
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

    var tour_conform_lead = $('#conform-lead-table').DataTable({
        processing: true,
        serverSide: true,
        pageLength: 20,
        lengthMenu: [ 10, 20, 50, 75, 100 ],
        destroy: true,
        oSearch: { "bSmart": false, "bRegex": true },
        order: [[ 1, "desc" ]],
        "ajax": {
            "url":  APP_URL + 'user-lead/conform-lead-data',
            "dataType": "json",
            "type": "POST",
            "data": {_token: Laravel.csrfToken}, //, 'category_name': $("#Scategory_name").val()
            beforeSend: function () {

                if (typeof tour_conform_lead != 'undefined' && tour_conform_lead.hasOwnProperty('settings')) {
                    tour_conform_lead.settings()[0].jqXHR.abort();
                }
            }
        },
        "columns": [
			{"data": "lead_id", "name": "lead_id"},
			{"data": "quatation_id", "name": "lead_quatation.quatation_id"},
			{"data": "created_at", "name": "created_at"},
			{"data": "user_info", "name": "user_info",render:function(data,type,row){
				return data.split('\n').join('<br>');
			}},
			{"data": "interested_in", "name": "interested_in"},
			{"data": "assign_to_user", "name": "assign_to_user"},
			{"data": "status_name", "name": "status_name"},
			{"data": "action", "sortable": false,"name": "action"}

        ]
    });


    $(tour_conform_lead.table().container()).on('keyup', 'tfoot input', function () {
        tour_conform_lead
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

    $(tour_conform_lead.table().container()).on('change', 'tfoot select', function () {
        tour_conform_lead
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

    var wallet = $('#wallet-table').DataTable({
        processing: true,
        serverSide: true,
        pageLength: 20,
        lengthMenu: [ 10, 20, 50, 75, 100 ],
        destroy: true,
        oSearch: { "bSmart": false, "bRegex": true },
        //order: [[ 0, "desc" ]],
        "ajax": {
            "url":  APP_URL + 'wallet/data',
            "dataType": "json",
            "type": "POST",
            "data": {_token: Laravel.csrfToken}, //, 'category_name': $("#Scategory_name").val()
            beforeSend: function () {
                if (typeof wallet != 'undefined' && wallet.hasOwnProperty('settings')) {
                    wallet.settings()[0].jqXHR.abort();
                }
            }
        },
        "columns": [
			{"data": "created_at", "name": "created_at"},
			{"data": "debit", "name": "debit"},
			{"data": "credit", "name": "credit"},
			{"data": "current_balance", "name": "current_balance"}
        ]
    });


    $(wallet.table().container()).on('keyup', 'tfoot input', function () {
        wallet
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });

    $(wallet.table().container()).on('change', 'tfoot select', function () {
        wallet
                .column($(this).attr('colPos'))
                .search(this.value)
                .draw();
    });


	$(document).on('click', '.social-share', function(event){
		var popupMeta = {
		    width: 400,
		    height: 400
		}
	    event.preventDefault();

	    var vPosition = Math.floor(($(window).width() - popupMeta.width) / 2),
	        hPosition = Math.floor(($(window).height() - popupMeta.height) / 2);

	    var url = $(this).attr('href');
	    var popup = window.open(url, 'Social Share',
	        'width='+popupMeta.width+',height='+popupMeta.height+
	        ',left='+vPosition+',top='+hPosition+
	        ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

	    if (popup) {
	        popup.focus();
	        return false;
	    }
	});

	$(document).on('change', 'input[type=radio][name=amount_type]', function(event){
		var val = $(this).val();
		if(val == 'custom_amount'){
			 $("#custome_amount").attr("readonly", false); 
		}else{
			 $("#custome_amount").attr("readonly", true); 
		}

		var amt = $(this).closest("div").find(".amount").val();
        var online_charge = $('#online_change').val();
        var servicetax =  ( ((parseFloat(amt) * parseFloat(online_charge)) / 100 ) +  parseFloat(amt) ).toFixed(2);
        if(servicetax > 0){
            $("#gross_amount").val(servicetax);
        }
	});
	$(document).on('change', '.amount', function(event){
        var amt = $(this).val();
        var online_charge = $('#online_change').val();
        var servicetax =  ( ((parseFloat(amt) * parseFloat(online_charge)) / 100 ) +  parseFloat(amt) ).toFixed(2);
        if(servicetax > 0){
            $("#gross_amount").val(servicetax);
        }
    });
    $('button.back-button').on('click', function(e){
	    e.preventDefault();
	    window.history.back();
	});

            

	
	
});