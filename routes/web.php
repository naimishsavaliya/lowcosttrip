<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/home', function () {
    return redirect('admin/home');
});
Route::get('/', 'HomeController@index')->name('index');
Route::get('/testing', 'HomeController@testing')->name('testing');
Route::post('/home/flight-data-store', 'HomeController@flight_data_store');
Route::get('/home/customise-book', 'HomeController@customise_book');

Route::get('/home/flight-booking', 'HomeController@flight_booking')->name('flight.booking');
Route::post('/home/flight-booking-submit', 'HomeController@flight_book_submit')->name('submit.flight.book.form');
Route::post('/home/flight-booking-book-now', 'HomeController@flight_book_now')->name('flight.book.now');
Route::post('/home/flight-order-now', 'HomeController@flight_order_now')->name('submit.flight.order.now');
Route::post('/home/flight-details', 'HomeController@flight_detsils')->name('flight.book.get.details');
Route::post('/home/flight-payment-response', 'HomeController@flight_payment_response')->name('flight.payment.response');


Route::get('/home/get-activiti-html/', 'HomeController@get_activiti_html')->name('get-activiti-html');
Route::post('/home/get-activiti-by-city/', 'HomeController@get_activiti_by_city')->name('get-activiti-by-city');

Route::post('/home/hotel-data-store', 'HomeController@hotel_data_store');
Route::post('/home/visa-data-store', 'HomeController@visa_data_store');
Route::post('/home/holiday-data-store', 'HomeController@holiday_data_store');

Route::post('/home/flight-data-update', 'HomeController@flight_data_edit');
Route::post('/home/hotel-data-update', 'HomeController@hotel_data_edit');
Route::post('/home/holiday-data-update', 'HomeController@holiday_data_edit');
Route::post('/home/visa-data-update', 'HomeController@visa_data_edit');

Route::post('/home/book-store', 'HomeController@book_store');
Route::post('/home/book-data-update', 'HomeController@book_data_edit');

//Route::post('/dashboard/get_quotation', 'HomeController@get_quotation');

Route::post('/dashboard/get_quotation/{lead_id}', [
    'uses' => 'client\DashboardController@get_quotation'
]);

Route::get('/cron/follow-up', [
    'uses' => 'admin\CronController@cron_followup'
])->name('cron_followup');

//Route::post('/dashboard/change_status/', [
//    'uses' => 'DashboardController@change_status'
//]);
//
//Route::post('/dashboard/change_status/', [
//    'uses' => 'DashboardController@change_status'
//])->name('test_con');

Route::get('/dashboard/change_status', 'client\DashboardController@change_status');


Route::get('/admin', 'admin\LoginController@index');
Auth::routes();

Route::post('/agent-register', 
    'Auth\AgentRegisterController@register')
->name('agent-register');

$admin = Request::segment(1);
Route::group(['prefix' => $admin, 'namespace' => 'admin', 'middleware' => ['auth', 'checkUserType:Admin|Manager|Supplier']], function($admin) {

    Route::get('/images', [
        'uses' => 'UploadimagesController@create'
    ])->name('images.create');
    Route::post('/images-save', [
        'uses' => 'UploadimagesController@store'
    ])->name('images.store');
    Route::post('/images-delete', [
        'uses' => 'UploadimagesController@destroy'
    ])->name('images.destroy');
    Route::get('/images-show', [
        'uses' => 'UploadimagesController@index'
    ])->name('images.index');



    /*     * *******************************************************************
     *                                                                   *
     *                             HOME                             *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/home', [
        'uses' => 'HomeController@index'
    ])->name('home');

    Route::get('/home/getstate/{country_id}', [
        'uses' => 'HomeController@getStateByCountry'
    ])->name('state_by_country');

    Route::get('/home/getcity/{state_id}', [
        'uses' => 'HomeController@getCityByState'
    ])->name('city_by_state');

    Route::get('/home/change-status/', [
        'uses' => 'HomeController@change_status'
    ])->name('change_status');

    Route::post('/admin/chart', [
        'uses' => 'HomeController@chart'
    ])->name('admin.chart');



    /*     * *******************************************************************
     *                                                                   *
     *                             CUSTOMERS                             *
     *                                                                   *
     * ******************************************************************* */
    Route::get('/customer', [
        'uses' => 'CustomerController@index'
    ])->name('customer.index');

    Route::post('/customer/data', [
        'uses' => 'CustomerController@data'
    ])->name('customer.data');




    Route::get('/customer/delete/{id}', [
        'uses' => 'CustomerController@destroy'
    ])->name('customer.delete');

    Route::get('/customer/edit/{id}', [
        'uses' => 'CustomerController@edit'
    ])->name('customer.edit');

    Route::get('/customer/wallet/{id}', [
            'uses' => 'CustomerController@wallet'
        ])->name('customer.wallet');

    Route::post('/customer/wallet/data/{id}', [
        'uses' => 'CustomerController@wallet_data'
    ])->name('customer.wallet.data');

    Route::post('/customer/store', [
        'uses' => 'CustomerController@store'
    ])->name('customer.store');

    Route::get('/customer/add', [
        'uses' => 'CustomerController@create'
    ])->name('customer.add');

    Route::get('/customer/view/{id}', [
        'uses' => 'CustomerController@show'
    ])->name('customer.view');

    Route::get('/customer/booking/{id}', [
        'uses' => 'CustomerController@booking'
    ]);

    Route::get('/customer/booking-history/{id}', [
        'uses' => 'CustomerController@pastbooking'
    ]);

    Route::get('/customer/type', [
        'uses' => 'CustomerController@type'
    ])->name('customer.type');

    Route::post('/customer/type/data', [
        'uses' => 'CustomerController@type_data'
    ])->name('customer.type.data');

    Route::post('/customer/type/store', [
        'uses' => 'CustomerController@type_store'
    ])->name('customer.type.store');

    Route::get('/customer/type/add', [
        'uses' => 'CustomerController@type_add'
    ])->name('customer.type.add');

    Route::get('/customer/type/edit/{id}', [
        'uses' => 'CustomerController@type_edit'
    ])->name('customer.type.edit');

    Route::get('/customer/type/delete/{id}', [
        'uses' => 'CustomerController@type_destroy'
    ])->name('customer.type.delete');

    /*     * *******************************************************************
     *                                                                   *
     *                             USER                                  *
     *                                                                   *
     * ******************************************************************* */
    Route::get('/user', [
        'uses' => 'UserController@index'
    ])->name('user.index');

    Route::post('/user/data', [
        'uses' => 'UserController@data'
    ])->name('user.data');

    Route::get('/user/add', [
        'uses' => 'UserController@create'
    ])->name('user.add');

    Route::get('/user/edit/{id}', [
        'uses' => 'UserController@edit'
    ])->name('user.edit');
    
    Route::post('/user/store', [
        'uses' => 'UserController@store'
    ])->name('user.store');
    Route::get('/user/delete/{id}', [
        'uses' => 'UserController@destroy'
    ])->name('user.delete');
    Route::get('/user/profile/{id}', [
        'uses' => 'UserController@profile'
    ]);
    Route::get('/user/change-role/{id}', [
        'uses' => 'UserController@changerole'
    ])->name('user.change.role');
    Route::post('/user/role/update/', [
        'uses' => 'UserController@role_save'
    ])->name('user.role.update');



    /*     * *******************************************************************
     *                                                                   *
     *                             ROLE                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/role', [
        'uses' => 'RoleController@index'
    ])->name('role.index');

    Route::get('/role/add', [
        'uses' => 'RoleController@create'
    ])->name('role.add');

    Route::post('/role/store', [
        'uses' => 'RoleController@store'
    ])->name('role.store');

    Route::get('/role/role-permission/{id}', [
        'uses' => 'RoleController@role_permission'
    ]);
    // Route::post('/role/data', [
    //     'uses' => 'RoleController@data'
    // ]);

    Route::post('/role/data', [
        'uses' => 'RoleController@data'
    ])->name('role.data');
    
    Route::post('/role/update/{id}', [
        'uses' => 'RoleController@update'
    ]);


    /*     * *******************************************************************
     *                                                                   *
     *                             Agent                             *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/agent', [
        'uses' => 'AgentController@index'
    ])->name('agent.index');
    Route::post('/agent/data', [
        'uses' => 'AgentController@agent_data'
    ])->name('agent.data');
    Route::get('/agent/view/{id}', [
        'uses' => 'AgentController@show'
    ])->name('agent.view');
    Route::get('/agent/add', [
        'uses' => 'AgentController@create'
    ])->name('agent.add');
    Route::get('/agent/edit/{id}', [
        'uses' => 'AgentController@edit'
    ])->name('agent.edit');
    Route::get('/agent/delete/{id}', [
        'uses' => 'AgentController@destroy'
    ])->name('agent.delete');
    Route::post('/agent/store', [
        'uses' => 'AgentController@store'
    ])->name('agent.store');
    Route::get('/agent/subscription', [
        'uses' => 'AgentController@subscription'
    ])->name('agent.subscription');
    Route::get('/agent/subscription_add', [
        'uses' => 'AgentController@subscription_add'
    ])->name('agent.subscription.add');
    Route::get('/agent/subscription-edit/{id}', [
        'uses' => 'AgentController@subscription_edit'
    ])->name('agent.subscription.edit');
    Route::post('/agent/subscription/store', [
        'uses' => 'AgentController@subscription_store'
    ])->name('agent.subscription.store');
    Route::post('/agent/subscription-data', [
        'uses' => 'AgentController@subscription_data'
    ])->name('agent.subscription.data');
    Route::get('/agent/subscription-delete/{id}', [
        'uses' => 'AgentController@subscription_destroy'
    ])->name('agent.subscription.delete');

    Route::get('/agent/notes/{agentid}', [
        'uses' => 'AgentController@notes'
    ])->name('agent.notes');
    Route::get('/agent/notes/add/{agentid}', [
        'uses' => 'AgentController@notes_add'
    ])->name('agent.notes.add');
    Route::get('/agent/notes/edit/{agentid}/{id}', [
        'uses' => 'AgentController@notes_edit'
    ])->name('agent.notes.edit');
    Route::get('/agent/notes/delete/{agentid}/{id}', [
        'uses' => 'AgentController@notes_delete'
    ])->name('agent.notes.delete');

    Route::get('/agent/document/add/{agentid}', [
        'uses' => 'AgentController@document_add'
    ])->name('agent.document.add');
    Route::get('/agent/document/edit/{agentid}/{id}', [
        'uses' => 'AgentController@document_edit'
    ])->name('agent.document.edit');
    /*     * *******************************************************************
     *                                                                   *
     *                             Sectore                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/sector', [
        'uses' => 'SectorController@index'
    ])->name('sector.index');
    Route::post('/sector/data', [
        'uses' => 'SectorController@data'
    ]);

    Route::get('/sector/add', [
        'uses' => 'SectorController@create'
    ])->name('sector.add');

    Route::get('/sector/edit/{id}', [
        'uses' => 'SectorController@edit'
    ])->name('sector.edit');

    Route::post('/sector/save', [
        'uses' => 'SectorController@save'
    ])->name('sector.save');

    Route::get('/sector/delete/{id}', [
        'uses' => 'SectorController@destroy'
    ])->name('sector.delete');

    /*     * *******************************************************************
     *                                                                   *
     *                             Tour Info                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/tourinfo', [
        'uses' => 'TourInfoController@index'
    ])->name('tourinfo.index');
    Route::post('/tourinfo/data', [
        'uses' => 'TourInfoController@data'
    ]);

    Route::get('/tourinfo/add', [
        'uses' => 'TourInfoController@create'
    ])->name('tourinfo.add');

    Route::get('/tourinfo/edit/{id}', [
        'uses' => 'TourInfoController@edit'
    ])->name('tourinfo.edit');

    Route::post('/tourinfo/save', [
        'uses' => 'TourInfoController@save'
    ])->name('tourinfo.save');

    Route::get('/tourinfo/delete/{id}', [
        'uses' => 'TourInfoController@destroy'
    ])->name('tourinfo.delete');

    Route::post('/tourinfo/get-data', [
        'uses' => 'TourInfoController@get_data'
    ])->name('get.single.data.ajax');

    //    Route::post('/sector/store', [
//        'uses' => 'SectorController@store'
//    ]);

    /*     * *******************************************************************
     *                                                                   *
     *                             Destination                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/destination', [
        'uses' => 'DestinationController@index'
    ])->name('destination.index');

    Route::post('/destination/data', [
        'uses' => 'DestinationController@data'
    ])->name('destination.data');

    Route::post('/destination/save', [
        'uses' => 'DestinationController@save'
    ])->name('destination.save');

    Route::get('/destination/add', [
        'uses' => 'DestinationController@create'
    ])->name('destination.add');
    Route::get('/destination/edit/{id}', [
        'uses' => 'DestinationController@edit'
    ])->name('destination.edit');
    Route::get('/destination/delete/{id}', [
        'uses' => 'DestinationController@destroy'
    ])->name('destination.delete');

    /*     * *******************************************************************
     *                                                                   *
     *                             Hotels                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/hotel', [
        'uses' => 'HotelController@index'
    ])->name('hotel.index');
    Route::post('/hotel/data', [
        'uses' => 'HotelController@data'
    ])->name('hotel.data');
    Route::post('/hotel/store', [
        'uses' => 'HotelController@store'
    ])->name('hotel.store');
    Route::get('/hotel/add', [
        'uses' => 'HotelController@create'
    ])->name('hotel.add');
    Route::get('/hotel/edit/{id}', [
        'uses' => 'HotelController@edit'
    ])->name('hotel.edit');
    Route::get('/hotel/delete/{id}', [
        'uses' => 'HotelController@destroy'
    ])->name('hotel.delete');
    Route::get('/hotel/detail/{id}', [
        'uses' => 'HotelController@detail'
    ])->name('hotel.detail');
    Route::post('/hotel/dropzon-image', [
        'uses' => 'HotelController@dropzonImageSave'
    ])->name('hotel.images.store');
    Route::get('/hotel/dropzon-image/get/{id}', [
        'uses' => 'HotelController@dropzonImageFetch'
    ])->name('hotel.images.fetch');
    Route::post('/hotel/images-delete', [
        'uses' => 'HotelController@dropzonImageDelete'
    ])->name('hotel.images.destroy');

    Route::get('/hotel/room/{id}', [
        'uses' => 'HotelController@room'
    ])->name('hotel.room');
    Route::post('/hotel/room-data', [
        'uses' => 'HotelController@room_data'
    ]);
    Route::post('/hotel/room-store', [
        'uses' => 'HotelController@room_store'
    ]);
    Route::get('/hotel/room-add', [
        'uses' => 'HotelController@room_create'
    ])->name('hotel.room.add');
    Route::get('/hotel/room-edit/{id}', [
        'uses' => 'HotelController@room_edit'
    ]);
    Route::get('/hotel/room-delete/{id}', [
        'uses' => 'HotelController@room_destroy'
    ]);
    Route::get('/hotel/room-price/{id}', [
        'uses' => 'HotelController@room_price'
    ]);

    //Room Type
    Route::get('/hotel/room-type', [
        'uses' => 'HotelController@room_type'
    ])->name('room.types');
    Route::post('/hotel/room-type/data', [
        'uses' => 'HotelController@room_type_data'
    ])->name('room.types.data');
    Route::get('/hotel/room-type/add', [
        'uses' => 'HotelController@room_type_add'
    ])->name('room.type.add');
    Route::get('/hotel/room-type/edit/{id}', [
        'uses' => 'HotelController@room_type_edit'
    ])->name('room.type.edit');
    Route::post('/hotel/room-type/store', [
        'uses' => 'HotelController@room_type_store'
    ])->name('room.type.store');
    Route::get('/hotel/room-type/delete/{id}', [
        'uses' => 'HotelController@room_type_destroy'
    ])->name('room.type.delete');

    Route::get('/hotel/room-price-type', [
        'uses' => 'HotelController@room_price_type'
    ])->name('room.price.type');
    Route::post('/hotel/room-price-type/data', [
        'uses' => 'HotelController@room_price_data'
    ])->name('room.price.type.data');
    Route::get('/hotel/room-price-type/add', [
        'uses' => 'HotelController@room_price_add'
    ])->name('room.price.type.add');
    Route::get('/hotel/room-price-type/edit/{id}', [
        'uses' => 'HotelController@room_price_edit'
    ])->name('room.price.type.edit');
    Route::post('/hotel/room-price-type/store', [
        'uses' => 'HotelController@room_price_store'
    ])->name('room.price.type.store');
    Route::get('/hotel/room-price-type/delete/{id}', [
        'uses' => 'HotelController@room_price_destroy'
    ])->name('room.price.type.delete');

    Route::get('/hotel/lead_room_booking/{id}', [
        'uses' => 'HotelController@lead_room_booking'
    ]);
    Route::get('/hotel/lead_room_buy/{id}', [
        'uses' => 'HotelController@lead_room_buy'
    ]);
    Route::get('/hotel/room_inventory/{id}', [
        'uses' => 'HotelController@room_inventory'
    ])->name('hotel.room.invntory');
    Route::get('/hotel/view_booking/{id}', [
        'uses' => 'HotelController@view_booking'
    ])->name('hotel.view.booking');
    Route::get('/hotel/view_booking_calendar/{id}', [
        'uses' => 'HotelController@view_booking_calendar'
    ])->name('hotel.view.calendar');
    Route::get('/hotel/discount/{id}', [
        'uses' => 'HotelController@discount'
    ])->name('hotel.discount');
    Route::get('/hotel/manage_discount/{id}', [
        'uses' => 'HotelController@manage_discount'
    ]);
    Route::get('/hotel/room_booking', [
        'uses' => 'HotelController@room_booking'
    ])->name('hotel.room.booking');
    Route::get('/hotel/room_detail/{id}', [
        'uses' => 'HotelController@room_detail'
    ])->name('hotel.room.detail');
    Route::get('/hotel/room_book/{id}', [
        'uses' => 'HotelController@room_book'
    ])->name('hotel.room.book');


    /*     * *******************************************************************
     *                                                                   *
     *                             Tour                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/tour', [
        'uses' => 'TourController@index'
    ])->name('tour.index');
    Route::get('/tour/view/{id}', [
        'uses' => 'TourController@show'
    ]);
    Route::post('/tour/data', [
        'uses' => 'TourController@data'
    ])->name('tour.data');
    Route::post('/tour/store', [
        'uses' => 'TourController@store'
    ])->name('tour.save');
    Route::get('/tour/add', [
        'uses' => 'TourController@create'
    ])->name('tour.add');
    Route::get('/tour/edit/{id}', [
        'uses' => 'TourController@edit'
    ])->name('tour.edit');
    Route::get('/tour/delete/{id}', [
        'uses' => 'TourController@destroy'
    ])->name('tour.delete');
    Route::get('/tour/detail/{id}', [
        'uses' => 'TourController@detail'
    ])->name('tour.detail');
    Route::post('/tour/images-save', [
        'uses' => 'TourController@dropzonImageSave'
    ])->name('tour.images.store');
    Route::post('/tour/images-delete', [
        'uses' => 'TourController@dropzonImageDelete'
    ])->name('tour.images.destroy');
    Route::get('/tour/get-images/{id}', [
        'uses' => 'TourController@dropzonImageFetch'
    ])->name('tour.images.fetch');

    Route::get('/tour/change-status/', [
        'uses' => 'TourController@change_status'
    ])->name('tour.change_status');


    /*     * *******************************************************************
     *                                                                   *
     *                             Flight                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/flight/manage-flight-booking', [
        'uses' => 'FlightsController@index'
    ])->name('manage.flight.booking');

    Route::post('/flight/flights-data', [
        'uses' => 'FlightsController@FlightsData'
    ])->name('flights.data');
    Route::get('/flight/view/{id}', [
        'uses' => 'FlightsController@view'
    ])->name('flights.book.view');


    

    /////////////////// Tour itinerary ////////////
    Route::post('/tour/itinerary_store', [
        'uses' => 'TourController@itinerary_store'
    ])->name('tour.itinerary.store');
    Route::get('/tour/itinerary_delete/{id}', [
        'uses' => 'TourController@itinerary_destroy'
    ])->name('tour.itinerary.delete');

    /////////////////// Tour departure date ////////////
    Route::post('/tour/departure_store', [
        'uses' => 'TourController@departure_store'
    ])->name('tour.departure.store');
    Route::get('/tour/get_departuredates/{currentYear}/{tourId}', [
        'uses' => 'TourController@get_departuredates'
    ])->name('tour.get.departuredates');

    ///////////////////Tour FAQ ////////////////
    Route::post('/tour/faq_store', [
        'uses' => 'TourController@faq_store'
    ])->name('tour.faq.store');
    Route::get('/tour/faq/delete/{id}', [
        'uses' => 'TourController@faq_destroy'
    ])->name('tour.faq.delete');
    Route::get('/tour/faq/detail/{id}', [
        'uses' => 'TourController@faq_detail'
    ])->name('tour.faq.detail');

    /////////////////// Tour Cost/////////////////////
    Route::get('/tour/cost-manage/{id}', [
        'uses' => 'TourController@cost_manage'
    ])->name('tour.cost.manage');
    Route::get('/tour/cost-edit/{tourId}/{costId}', [
        'uses' => 'TourController@cost_edit'
    ])->name('tour.cost.edit');
    Route::post('/tour/cost_store', [
        'uses' => 'TourController@cost_store'
    ])->name('tour.cost.store');
    Route::post('/tour/cost_update', [
        'uses' => 'TourController@cost_update'
    ])->name('tour.cost.update');
    Route::get('/tour/cost_delete/{id}', [
        'uses' => 'TourController@cost_destroy'
    ])->name('tour.cost.delete');

    ////////////////// Tour Hotel
    Route::post('/tour/hotel_store', [
        'uses' => 'TourController@hotel_store'
    ])->name('tour.hotel.store');

    Route::get('/tour/hotel/{id}', [
        'uses' => 'TourController@hotel'
    ]);
    Route::get('/tour/departure/{id}', [
        'uses' => 'TourController@departure'
    ]);
    //////////// Tour Quotation //////
    Route::post('/tour/tour_quotation_data', [
        'uses' => 'TourController@tour_quotation_data'
    ])->name('tour.quotation.data');



    Route::get('/tour/graphics/{id}', [
        'uses' => 'TourController@graphics'
    ]);



    Route::get('/tour/discount/{id}', [
        'uses' => 'TourController@discount_list'
    ]);
    Route::get('/tour/itinerary/{id}', [
        'uses' => 'TourController@itinerary'
    ]);
    Route::get('/tour/add_discount/{id}', [
        'uses' => 'TourController@discount_create'
    ]);
    Route::get('/tour/edit_discount/{id}', [
        'uses' => 'TourController@discount_edit'
    ]);
    Route::get('/tour/delete_discount/{id}', [
        'uses' => 'TourController@discount_destroy'
    ]);
    Route::get('/tour/details/{id}/{date}', [
        'uses' => 'TourController@details'
    ]);
    Route::get('/tour/payment/{id}/{date}', [
        'uses' => 'TourController@payment'
    ]);
    Route::get('/tour/hotel_chart/{id}/{date}', [
        'uses' => 'TourController@hotel_chart'
    ]);
    Route::get('/tour/price-type', [
        'uses' => 'TourController@price_type'
    ])->name('tour.price.type');
    Route::get('/tour/price-type/add', [
        'uses' => 'TourController@price_type_add'
    ]);
    Route::get('/tour/price-type/edit/{id}', [
        'uses' => 'TourController@price_type_edit'
    ]);
    Route::get('/tour/price-type/delete/{id}', [
        'uses' => 'TourController@price_type_destroy'
    ]);

    Route::get('/tour/feature-list', [
        'uses' => 'TourController@feature_list'
    ])->name('tour.features');
    Route::post('/tour/feature-list/data', [
        'uses' => 'TourController@feature_list_data'
    ])->name('tour.features.data');
    Route::get('/tour/feature-list/add', [
        'uses' => 'TourController@feature_add'
    ])->name('tour.features.add');
    Route::get('/tour/feature-list/edit/{id}', [
        'uses' => 'TourController@feature_edit'
    ])->name('tour.features.edit');
    Route::post('/tour/feature-list/store', [
        'uses' => 'TourController@feature_store'
    ])->name('tour.features.store');
    Route::get('/tour/feature-list/delete/{id}', [
        'uses' => 'TourController@feature_destroy'
    ])->name('tour.features.delete');



    //Tour Type
    Route::get('/tour/type', [
        'uses' => 'TourController@tour_type'
    ])->name('tour.type.index');
    Route::post('/tour/type/data', [
        'uses' => 'TourController@tour_type_data'
    ])->name('tour.type.data');
    Route::get('/tour/type/add', [
        'uses' => 'TourController@tour_type_add'
    ])->name('tour.type.add');
    Route::get('/tour/type/edit/{id}', [
        'uses' => 'TourController@tour_type_edit'
    ])->name('tour.type.edit');
    Route::post('/tour/type/store', [
        'uses' => 'TourController@tour_type_store'
    ])->name('tour.type.store');
    Route::get('/tour/type/delete/{id}', [
        'uses' => 'TourController@tour_destroy'
    ])->name('tour.type.delete');

    //Tour package type
    Route::get('/tour/package/type', [
        'uses' => 'TourController@tour_category'
    ])->name('tour.category.index');
    Route::post('/tour/package/type/data', [
        'uses' => 'TourController@tour_category_data'
    ])->name('tour.category.data');
    Route::get('/tour/package/type/add', [
        'uses' => 'TourController@tour_category_add'
    ])->name('tour.category.add');
    Route::get('/tour/package/type/edit/{id}', [
        'uses' => 'TourController@tour_category_edit'
    ])->name('tour.category.edit');
    Route::post('/tour/package/type/store', [
        'uses' => 'TourController@tour_category_store'
    ])->name('tour.category.store');
    Route::get('/tour/package/type/delete/{id}', [
        'uses' => 'TourController@tour_category_destroy'
    ])->name('tour.category.delete');

    Route::get('/tour/booking', [
        'uses' => 'TourController@tour_booking'
    ])->name('tour.booking');

    Route::post('/tour/read-name', [
        'uses' => 'TourController@read_name'
    ])->name('tour.name.read');

    Route::post('/tour/read-name-json', [
        'uses' => 'TourController@read_name_json'
    ])->name('tour.name.read.json');

    /*     * *******************************************************************
     *                                                                   *
     *                             Activities                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/domestic-holidays', [
        'uses' => 'TourController@domestic_holiday_list'
    ])->name('tour.domestic-holidays');

    Route::post('/domestic-holidays/data', [
        'uses' => 'TourController@domestic_holiday_list_data'
    ])->name('tour.domestic-holidays.data');

    Route::get('/domestic-holidays/add', [
        'uses' => 'TourController@domestic_holiday_create'
    ])->name('tour.domestic-holidays.add');

    Route::post('/domestic-holidays/store', [
        'uses' => 'TourController@domestic_holiday_store'
    ])->name('domestic-holidays.store');

    Route::get('/domestic-holidays/delete/{id}', [
        'uses' => 'TourController@domestic_holiday_destroy'
    ])->name('tour.domestic-holidays.delete');

    Route::get('/domestic-holidays/edit/{id}', [
        'uses' => 'TourController@domestic_holiday_edit'
    ])->name('tour.domestic-holidays.edit');

    Route::post('/domestic-holidays/update', [
        'uses' => 'TourController@domestic_holiday_update'
    ])->name('domestic-holidays.update');

 




    Route::get('/book-activities', [
        'uses' => 'BookActivitiesController@index'
    ])->name('book-activities.view');

    Route::post('/book-activities/data', [
        'uses' => 'BookActivitiesController@data'
    ])->name('book-activities.data');

    Route::get('/book-activities/add', [
        'uses' => 'BookActivitiesController@create'
    ])->name('book-activities.add');

    Route::post('/book-activities/store', [
        'uses' => 'BookActivitiesController@store'
    ])->name('book-activities.store');

    Route::get('/book-activities/edit/{id}', [
        'uses' => 'BookActivitiesController@edit'
    ])->name('book-activities.edit');

    Route::post('/book-activities/update', [
        'uses' => 'BookActivitiesController@update'
    ])->name('book-activities.update');

    Route::get('/book-activities/delete/{id}', [
        'uses' => 'BookActivitiesController@destroy'
    ])->name('book-activities.delete');



    /*     * *******************************************************************
     *                                                                   *
     *                             Activities                              *
     *                                                                   *
     * ******************************************************************* */    

    Route::get('/international-holidays', [
        'uses' => 'TourController@international_holiday_list'
    ])->name('tour.international-holidays');

    Route::post('/international-holidays/data', [
        'uses' => 'TourController@international_holiday_list_data'
    ])->name('tour.international-holidays.data');

    Route::get('/international-holidays/add', [
        'uses' => 'TourController@international_holiday_create'
    ])->name('tour.international-holidays.add');

    Route::post('/international-holidays/store', [
        'uses' => 'TourController@international_holiday_store'
    ])->name('international-holidays.store');

    Route::get('/international-holidays/delete/{id}', [
        'uses' => 'TourController@international_holiday_destroy'
    ])->name('tour.international-holidays.delete');

    Route::get('/international-holidays/edit/{id}', [
        'uses' => 'TourController@international_holiday_edit'
    ])->name('tour.international-holidays.edit');

    Route::post('/international-holidays/update', [
        'uses' => 'TourController@international_holiday_update'
    ])->name('international-holidays.update');


    /*     * *******************************************************************
     *                                                                   *
     *                             Activities                              *
     *                                                                   *
     * ******************************************************************* */
    Route::get('/featured-tours', [
        'uses' => 'TourController@featured_tour_list'
    ])->name('tour.featured-tours');

    Route::post('/featured-tours/data', [
        'uses' => 'TourController@featured_tour_list_data'
    ])->name('tour.featured-tours.data');

    Route::get('/featured-tours/add', [
        'uses' => 'TourController@featured_tour_create'
    ])->name('tour.featured-tours.add');

    Route::post('/featured-tours/store', [
        'uses' => 'TourController@featured_tour_store'
    ])->name('featured-tours.store');

    Route::get('/featured-tours/delete/{id}', [
        'uses' => 'TourController@featured_tour_destroy'
    ])->name('tour.featured-tours.delete');

    Route::get('/featured-tours/edit/{id}', [
        'uses' => 'TourController@featured_tour_edit'
    ])->name('tour.featured-tours.edit');

    Route::post('/featured-tours/update', [
        'uses' => 'TourController@featured_tour_update'
    ])->name('featured-tours.update');

    /*     * *******************************************************************
     *                                                                   *
     *                             Activities                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/hot-deals-tours', [
        'uses' => 'TourController@hot_deal_tour_list'
    ])->name('tour.hot-deals-tours');

    Route::post('/hot-deals-tours/data', [
        'uses' => 'TourController@hot_deal_tour_list_data'
    ])->name('tour.hot-deals-tours.data');

    Route::get('/hot-deals-tours/add', [
        'uses' => 'TourController@hot_deal_tour_create'
    ])->name('tour.hot-deals-tours.add');

    Route::post('/hot-deals-tours/store', [
        'uses' => 'TourController@hot_deal_tour_store'
    ])->name('hot-deals-tours.store');

    Route::get('/hot-deals-tours/delete/{id}', [
        'uses' => 'TourController@hot_deal_tour_destroy'
    ])->name('tour.hot-deals-tours.delete');

    Route::get('/hot-deals-tours/edit/{id}', [
        'uses' => 'TourController@hot_deal_tour_edit'
    ])->name('tour.hot-deals-tours.edit');

    Route::post('/hot-deals-tours/update', [
        'uses' => 'TourController@hot_deal_tour_update'
    ])->name('hot-deals-tours.update');

    /*     * *******************************************************************
     *                                                                   *
     *                             Activities                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/new-arrivals', [
        'uses' => 'TourController@new_arrival_list'
    ])->name('tour.new-arrivals');

    Route::post('/new-arrivals/data', [
        'uses' => 'TourController@new_arrival_list_data'
    ])->name('tour.new-arrivals.data');

    Route::get('/new-arrivals/add', [
        'uses' => 'TourController@new_arrival_create'
    ])->name('tour.new-arrivals.add');

    Route::post('/new-arrivals/store', [
        'uses' => 'TourController@new_arrival_store'
    ])->name('new-arrivals.store');

    Route::get('/new-arrivals/delete/{id}', [
        'uses' => 'TourController@new_arrival_destroy'
    ])->name('tour.new-arrivals.delete');

    Route::get('/new-arrivals/edit/{id}', [
        'uses' => 'TourController@new_arrival_edit'
    ])->name('tour.new-arrivals.edit');

    Route::post('/new-arrivals/update', [
        'uses' => 'TourController@new_arrival_update'
    ])->name('new-arrivals.update');


    /*     * *******************************************************************
     *                                                                   *
     *                             Activities                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/featured-destinations', [
        'uses' => 'TourController@featured_destination_list'
    ])->name('tour.featured-destinations');

    Route::post('/featured-destinations/data', [
        'uses' => 'TourController@featured_destination_list_data'
    ])->name('tour.featured-destinations.data');

    Route::get('/featured-destinations/add', [
        'uses' => 'TourController@featured_destination_create'
    ])->name('tour.featured-destinations.add');

    Route::post('/featured-destinations/store', [
        'uses' => 'TourController@featured_destination_store'
    ])->name('featured-destinations.store');

    Route::get('/featured-destinations/delete/{id}', [
        'uses' => 'TourController@featured_destination_destroy'
    ])->name('tour.featured-destinations.delete');

    Route::get('/featured-destinations/edit/{id}', [
        'uses' => 'TourController@featured_destination_edit'
    ])->name('tour.featured-destinations.edit');

    Route::post('/featured-destinations/update', [
        'uses' => 'TourController@featured_destination_update'
    ])->name('featured-destinations.update');


    /*     * *******************************************************************
     *                                                                   *
     *                             Activities                              *
     *                                                                   *
     * ******************************************************************* */



    Route::get('/activities', [
        'uses' => 'ActivitiesController@index'
    ])->name('activities.index');
    Route::post('/activities/data', [
        'uses' => 'ActivitiesController@data'
    ]);
    Route::post('/activities/store', [
        'uses' => 'ActivitiesController@store'
    ]);
    Route::get('/activities/add', [
        'uses' => 'ActivitiesController@create'
    ])->name('activities.add');

    Route::get('/activities/edit/{id}', [
        'uses' => 'ActivitiesController@edit'
    ]);
    Route::get('/activities/delete/{id}', [
        'uses' => 'ActivitiesController@destroy'
    ]);
    //activity type
    Route::get('activities/type', [
        'uses' => 'ActivitiesController@category'
    ])->name('activities.type');
    Route::post('/activities/type/data', [
        'uses' => 'ActivitiesController@category_data'
    ])->name('activities.type_data');
    Route::post('/activities/type_store', [
        'uses' => 'ActivitiesController@category_store'
    ])->name('activities.type_store');
    Route::get('activities/type/add', [
        'uses' => 'ActivitiesController@category_create'
    ])->name('activities.type.add');
    Route::get('activities/type/edit/{id}', [
        'uses' => 'ActivitiesController@category_edit'
    ])->name('activities.type.edit');
    Route::get('activities/type/delete/{id}', [
        'uses' => 'ActivitiesController@category_destroy'
    ])->name('activities.type.delete');

    Route::get('activities/inventory', [
        'uses' => 'ActivitiesController@activity_inventory'
    ])->name('activities.inventory');
    Route::get('activities/inventory/add', [
        'uses' => 'ActivitiesController@activity_inventory_add'
    ]);
    Route::get('activities/inventory/edit/{id}', [
        'uses' => 'ActivitiesController@activity_inventory_edit'
    ]);
    Route::get('activities/inventory/delete/{id}', [
        'uses' => 'ActivitiesController@activity_inventory_desctroy'
    ]);

    Route::get('activities/view/{id}', [
        'uses' => 'ActivitiesController@show'
    ])->name('activity.view');

    Route::get('activities/detail/{id}', [
        'uses' => 'ActivitiesController@detail'
    ])->name('activities.detail');

    //Rate Type
    Route::get('rates/type', [
        'uses' => 'ActivitiesController@rates_type'
    ])->name('rate.type.index');
    Route::post('rates/type/data', [
        'uses' => 'ActivitiesController@rates_data'
    ])->name('rate.type.data');
    Route::post('rates/type/store', [
        'uses' => 'ActivitiesController@rate_type_store'
    ])->name('rate.type.store');
    Route::get('rates/type/add', [
        'uses' => 'ActivitiesController@rates_type_add'
    ])->name('rate.type.add');
    Route::get('rates/type/edit/{id}', [
        'uses' => 'ActivitiesController@rate_type_edit'
    ])->name('rate.type.edit');
    Route::get('rates/type/delete/{id}', [
        'uses' => 'ActivitiesController@rate_type_destroy'
    ])->name('rate.type.delete');

    Route::get('activities/inventory/{id}', [
        'uses' => 'ActivitiesController@inventory'
    ])->name('activity.inventory');


//    Manage activity booking
    Route::get('activities/bookings', [
        'uses' => 'ActivitiesController@bookings'
    ])->name('activities.mange.booking.index');
    Route::get('activities/bookings/add', [
        'uses' => 'ActivitiesController@booking_add'
    ])->name('activities.booking.add');

    /*     * *******************************************************************
     *                                                                   *
     *                             Lead                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/lead', [
        'uses' => 'LeadController@index'
    ])->name('lead.index');
    Route::post('/lead/data', [
        'uses' => 'LeadController@data'
    ])->name('lead.data');

    Route::get('/lead/assign-to-user/', [
        'uses' => 'LeadController@assign_to_user'
    ])->name('assign.to.user');

    Route::post('/lead/store', [
        'uses' => 'LeadController@store'
    ])->name('lead.store');
    Route::get('/lead/add', [
        'uses' => 'LeadController@create'
    ])->name('lead.add');
    Route::get('/lead/edit/{id}', [
        'uses' => 'LeadController@edit'
    ])->name('lead.edit');
    Route::get('/lead/cancelled', [
        'uses' => 'LeadController@cancelled'
    ]);
    Route::get('/lead/confirmed', [
        'uses' => 'LeadController@confirmed'
    ]);
    Route::get('/lead/confirm/{id}', [
        'uses' => 'LeadController@confirm'
    ])->name('lead.confirm');
    Route::get('/lead/get-departure-date/{tour_id}', [
        'uses' => 'LeadController@get_departure_date'
    ]);
    Route::get('/lead/get-accomodation-data/{package_id}/{tour_id}/{tour_type}/{total_element}', [
        'uses' => 'LeadController@get_accomodation_data'
    ]);
    Route::get('/lead/remove-room/{acc_id}', [
        'uses' => 'LeadController@remove_room'
    ]);
    Route::post('/lead/save-confirm-lead', [
        'uses' => 'LeadController@save_confirm_lead'
    ]);
    Route::get('/lead/delete/{id}', [
        'uses' => 'LeadController@destroy'
    ])->name('lead.delete');
    Route::get('/lead/detail/{id}', [
        'uses' => 'LeadController@detail'
    ])->name('lead.view');

    Route::get('/lead/notes', [
        'uses' => 'LeadController@notes_list'
    ])->name('lead.notes.list');
    Route::get('/lead/notes/add/{lead_id}', [
        'uses' => 'LeadController@notes_add'
    ])->name('lead.note.add');
    Route::get('/lead/notes/edit/{lead_id}/{id}', [
        'uses' => 'LeadController@notes_edit'
    ])->name('lead.note.edit');
    Route::post('/lead/notes/store', [
        'uses' => 'LeadController@notes_store'
    ])->name('lead.notes.store');
    Route::post('/lead/notes/data/{lead_id}', [
        'uses' => 'LeadController@notes_data'
    ])->name('lead.notes.data');
    Route::get('/lead/notes/delete/{lead_id}/{id}', [
        'uses' => 'LeadController@notes_destroy'
    ])->name('lead.notes.delete');

    //Leads Document route
    Route::get('/lead/documents/add/{lead_id}', [
        'uses' => 'LeadController@documents_add'
    ])->name('lead.document.add');
    Route::any('/lead/documents/data/{lead_id}', [
        'uses' => 'LeadController@documents_data'
    ])->name('lead.document.data');
    Route::get('/lead/documents/edit/{lead_id}/{id}', [
        'uses' => 'LeadController@documents_edit'
    ])->name('lead.document.edit');
    Route::post('/lead/documents/store', [
        'uses' => 'LeadController@documents_store'
    ])->name('lead.document.store');
    Route::get('/lead/documents/delete/{id}', [
        'uses' => 'LeadController@documents_delete'
    ])->name('lead.document.delete');

    //Lead voucher
    Route::post('/lead/voucher/data/{lead_id}', [
        'uses' => 'LeadController@voucher_data'
    ])->name('lead.voucher.data');
    Route::get('/lead/voucher/add/{lead_id}', [
        'uses' => 'LeadController@voucher_add'
    ])->name('lead.voucher.add');
    Route::get('/lead/voucher/edit/{id}', [
        'uses' => 'LeadController@voucher_edit'
    ])->name('lead.voucher.edit');
    Route::get('/lead/voucher/delete/{id}', [
        'uses' => 'LeadController@voucher_delete'
    ])->name('lead.voucher.delete');
    Route::post('/lead/voucher/store', [
        'uses' => 'LeadController@voucher_store'
    ])->name('voucher.store');
    Route::get('/lead/voucher/generate-pdf/{id}', [
        'uses' => 'LeadController@generate_pdf'
    ])->name('lead.voucher.pdf');


    //Lead Payment route
    Route::get('/lead/payment/{lead_id}', [
        'uses' => 'LeadController@documents_add'
    ])->name('lead.payment.view');

    //Lead Status
    Route::get('/lead/status', [
        'uses' => 'LeadController@status_list'
    ])->name('lead.status.list');
    Route::post('/lead/status/data', [
        'uses' => 'LeadController@status_data'
    ])->name('lead.status.data');
    Route::get('/lead/status/add', [
        'uses' => 'LeadController@status_add'
    ])->name('lead.status.add');
    Route::get('/lead/status/edit/{id}', [
        'uses' => 'LeadController@status_edit'
    ])->name('lead.status.edit');
    Route::post('/lead/status/store', [
        'uses' => 'LeadController@status_store'
    ])->name('lead.status.store');
    Route::get('/lead/status/delete/{id}', [
        'uses' => 'LeadController@status_destroy'
    ])->name('lead.status.delete');
    Route::get('/lead/lead_detail/{id}', [
        'uses' => 'LeadController@lead_detail'
    ])->name('lead.lead_detail');
    Route::get('/lead/payment', [
        'uses' => 'LeadController@payment'
    ])->name('lead.payment');
    Route::get('/lead/payment_manage', [
        'uses' => 'LeadController@payment_manage'
    ])->name('lead.payment.manage');
    
    Route::get('/lead/detial-for-quatation/{lead_no}', [
        'uses' => 'LeadController@lead_detials'
    ])->name('lead.get.detial');
    

    Route::post('/lead/lead_payment_data/{lead_id}', [
        'uses' => 'LeadController@lead_payment_data'
    ])->name('lead_payment.data');

    Route::get('/lead/new_payment/add/{lead_id}', [
        'uses' => 'LeadController@new_payment'
    ])->name('lead.payment.add_new_payment');

    Route::post('/lead/payment/store', [
        'uses' => 'LeadController@leadpayment_store'
    ])->name('lead_payment.store');

    Route::get('/lead/customer_discount/add/{lead_id}', [
        'uses' => 'LeadController@customer_discount'
    ])->name('lead.payment.add_customer_discount');

    Route::post('/lead/customer_discount/store', [
        'uses' => 'LeadController@customer_discount_store'
    ])->name('customerdiscount.store');

    Route::get('/lead/additional_services/add/{lead_id}', [
        'uses' => 'LeadController@additional_services'
    ])->name('lead.payment.add_additional_services');

    Route::post('/lead/additional_services/store', [
        'uses' => 'LeadController@additional_services_store'
    ])->name('additional_services.store');

    Route::get('/lead/payment/edit/{id}/{type}', [
           'uses' => 'LeadController@lead_payment_edit'
    ])->name('lead_payment.edit');

    Route::get('/lead/send-proforma-service/{id}', [
           'uses' => 'LeadController@send_proforma_service'
    ])->name('send.proforma.service');

    Route::get('/lead/print-receipt/{id}/{type}', [
           'uses' => 'QuotationController@print_receipt'
    ])->name('lead.print_receipt');

    Route::post('/lead/payment/update', [
        'uses' => 'LeadController@leadpayment_update'
    ])->name('lead_payment.update');

    Route::get('/lead/get-activiti-html/', 'LeadController@get_activiti_html')->name('lead.get-activiti-html');

    Route::post('/lead/get-activiti-by-city/', 'LeadController@get_activiti_by_city')->name('lead.get-activiti-by-city');
    
    // Route::get('/lead/payment/edit/{id}/{type}', [
    //        'uses' => 'LeadController@new_payment_edit'
    //    ])->name('lead_payment.edit');

    // Route::get('/lead/discount/edit/{id}/{type}', [
    //        'uses' => 'LeadController@customer_discount_edit'
    //    ])->name('lead_discount.edit');

    // Route::get('/lead/service/edit/{id}/{type}', [
    //        'uses' => 'LeadController@additional_services_edit'
    //    ])->name('lead_service.edit');

    
    
    
    
    Route::any('/lead/itinerary-quatation/{tour_id}', [
        'uses' => 'LeadController@itinerary_quotation'
    ])->name('tour.itinerary.quotation');
    /*     * ********Quotation******** */
    Route::get('/quotation', [
        'uses' => 'QuotationController@index'
    ]);
    Route::get('/quotation/add', [
        'uses' => 'QuotationController@create'
    ]);

    Route::POST('/quotation/send-mail-to-user', [
        'uses' => 'QuotationController@send_mail_to_user'
    ])->name('quot.send.mail.to.user');

    Route::get('/quotation/edit/{lead_id}/{id}', [
        'uses' => 'QuotationController@edit'
    ])->name('quotation.edit');
    Route::get('/quotation/delete/{id}', [
        'uses' => 'QuotationController@destroy'
    ])->name('quotation.delete');
    Route::get('/quotation/view/{lead_id}/{quotation_id}', [
        'uses' => 'QuotationController@show'
    ])->name('quotation.view');
    Route::post('/quotation/data/{lead_id}', [
        'uses' => 'QuotationController@data'
    ])->name('quotation.data');

    Route::get('/quotation/change-status/', [
        'uses' => 'QuotationController@change_status'
    ])->name('quotation_change_status');

    Route::get('/quotation/print-proforma/{id}', [
        'uses' => 'QuotationController@print_proforma'
    ])->name('quote.proforma.print');

    /*     * *******************************************************************
     *                                                                   *
     *                             Website                              *
     *                                                                   *
     * ******************************************************************* */
    //website home banner
    Route::get('/website/homebanner', [
        'uses' => 'WebsiteController@homebanner'
    ])->name('website.banner.list');
    Route::post('/website/homebanner/data', [
        'uses' => 'WebsiteController@homebanner_data'
    ])->name('website.banner.data');
    Route::get('/website/homebanner-add', [
        'uses' => 'WebsiteController@homebanner_add'
    ])->name('website.banner.add');
    Route::get('/website/homebanner-edit/{id}', [
        'uses' => 'WebsiteController@homebanner_edit'
    ])->name('website.banner.edit');
    Route::post('/website/homebanner/store', [
        'uses' => 'WebsiteController@homebanner_store'
    ])->name('website.banner.store');
    Route::get('/website/homebanner-delete/{id}', [
        'uses' => 'WebsiteController@homebanner_delete'
    ])->name('website.banner.delete');

    Route::get('/website/featured-tours', [
        'uses' => 'WebsiteController@featured_tours'
    ])->name('website.featured.tour');
    Route::get('/website/featured-tours-add', [
        'uses' => 'WebsiteController@featured_tours_add'
    ])->name('website.featured.tour.add');
    Route::get('/website/featured-tours-edit/{id}', [
        'uses' => 'WebsiteController@featured_tours_edit'
    ])->name('website.featured.tour.edit');
    Route::post('/website/featured-tours-store', [
        'uses' => 'WebsiteController@featured_tours_store'
    ])->name('website.featured.tour.store');
    Route::post('/website/featured-tours-data', [
        'uses' => 'WebsiteController@featured_tours_data'
    ])->name('website.featured.tour.data');
    Route::get('/website/featured-tours-delete/{id}', [
        'uses' => 'WebsiteController@featured_tours_delete'
    ])->name('website.featured.tour.dalete');

    Route::get('/website/featured-destinations', [
        'uses' => 'WebsiteController@featured_destinations'
    ])->name('website.feature.destination');
    Route::get('/website/featured-destinations-add', [
        'uses' => 'WebsiteController@featured_destinations_add'
    ]);
    Route::get('/website/featured-destinations-edit/{id}', [
        'uses' => 'WebsiteController@featured_destinations_edit'
    ]);
    Route::get('/website/featured-destinations-delete/{id}', [
        'uses' => 'WebsiteController@featured_destinations_delete'
    ]);

    Route::get('/website/hot-tours', [
        'uses' => 'WebsiteController@hot_tours'
    ])->name('website.hottour.index');
    Route::get('/website/hot-tours-add', [
        'uses' => 'WebsiteController@hot_tours_add'
    ])->name('website.hottour.add');
    Route::get('/website/hot-tours-edit/{id}', [
        'uses' => 'WebsiteController@hot_tours_edit'
    ])->name('website.hottour.edit');
    Route::post('/website/hot-tours-save', [
        'uses' => 'WebsiteController@hot_tours_store'
    ])->name('website.hottour.store');
    Route::post('/website/hot-tours-data', [
        'uses' => 'WebsiteController@hot_tours_data'
    ])->name('website.hottour.data');
    Route::get('/website/hot-tours-delete/{id}', [
        'uses' => 'WebsiteController@hot_tours_delete'
    ])->name('website.hottour.delete');

    Route::get('/website/international-tours', [
        'uses' => 'WebsiteController@international_tours'
    ])->name('website.international.tour');
    Route::get('/website/international-tours-add', [
        'uses' => 'WebsiteController@international_tours_add'
    ])->name('website.international.add');
    Route::get('/website/international-tours-edit/{id}', [
        'uses' => 'WebsiteController@international_tours_edit'
    ])->name('website.international.edit');
    Route::get('/website/international-tours-delete/{id}', [
        'uses' => 'WebsiteController@international_tours_delete'
    ])->name('website.international.delete');
    Route::post('/website/international-tours-store', [
        'uses' => 'WebsiteController@international_tours_store'
    ])->name('website.international.store');
    Route::post('/website/international-tours-data', [
        'uses' => 'WebsiteController@international_tours_data'
    ])->name('website.international.data');


    Route::post('/website/data', [
        'uses' => 'WebsiteController@data'
    ]);
    Route::post('/website/store', [
        'uses' => 'WebsiteController@store'
    ]);
    Route::get('/website/add', [
        'uses' => 'WebsiteController@create'
    ]);
    Route::get('/website/edit/{id}', [
        'uses' => 'WebsiteController@edit'
    ]);
    Route::get('/website/delete/{id}', [
        'uses' => 'WebsiteController@destroy'
    ]);

    //Hotel Promotion Routing
    Route::get('/website/hotel-prmoted', [
        'uses' => 'WebsiteController@hotel_promoted'
    ])->name('website.hotel.promoted');
    Route::post('/website/hotel-prmoted/data', [
        'uses' => 'WebsiteController@hotel_promoted_data'
    ])->name('website.hotel.promoted.data');
    Route::get('/website/hotel-prmoted/add', [
        'uses' => 'WebsiteController@hotel_promoted_add'
    ])->name('website.hotel.promoted.add');
    Route::get('/website/hotel-prmoted/edit/{id}', [
        'uses' => 'WebsiteController@hotel_promoted_edit'
    ])->name('website.hotel.promoted.edit');
    Route::post('/website/hotel-prmoted/store', [
        'uses' => 'WebsiteController@hotel_promoted_store'
    ])->name('website.hotel.promoted.store');
    Route::get('/website/hotel-prmoted/delete/{id}', [
        'uses' => 'WebsiteController@hotel_promoted_destroy'
    ])->name('website.hotel.promoted.delete');

    //Hotel Best Deal Routing
    Route::get('/website/hotel-best-deal', [
        'uses' => 'WebsiteController@hotel_bestdeal'
    ])->name('website.hotel.bestdeal');
    Route::get('/website/hotel-best-deal/add', [
        'uses' => 'WebsiteController@hotel_bestdeal_add'
    ])->name('website.hotel.bestdeal.add');
    Route::get('/website/hotel-best-deal/edit/{id}', [
        'uses' => 'WebsiteController@hotel_bestdeal_edit'
    ])->name('website.hotel.bestdeal.edit');
    Route::get('/website/hotel-best-deal/delete/{id}', [
        'uses' => 'WebsiteController@hotel_bestdeal_destroy'
    ])->name('website.hotel.bestdeal.delete');

    //Hotel Featured Routing
    Route::get('/website/hotel-featured', [
        'uses' => 'WebsiteController@hotel_featured'
    ])->name('website.hotel.featured');
    Route::post('/website/hotel-featured/data', [
        'uses' => 'WebsiteController@hotel_featured_data'
    ])->name('website.hotel.featured.data');
    Route::get('/website/hotel-featured/add', [
        'uses' => 'WebsiteController@hotel_featured_add'
    ])->name('website.hotel.featured.add');
    Route::get('/website/hotel-featured/edit/{id}', [
        'uses' => 'WebsiteController@hotel_featured_edit'
    ])->name('website.hotel.featured.edit');
    Route::post('/website/hotel-featured/store', [
        'uses' => 'WebsiteController@hotel_featured_store'
    ])->name('website.hotel.featued.store');
    Route::get('/website/hotel-featured/delelete/{id}', [
        'uses' => 'WebsiteController@hotel_featured_destroy'
    ])->name('website.hotel.featured.delete');

    // Hotel Activity Deal
    Route::get('/website/activity-deal', [
        'uses' => 'WebsiteController@activity_deal'
    ])->name('website.activity.deal');
    Route::get('/website/activity-deal/add', [
        'uses' => 'WebsiteController@activity_deal_add'
    ])->name('website.activity.deal.add');
    Route::get('/website/activity-deal/edit/{id}', [
        'uses' => 'WebsiteController@activity_deal_edit'
    ])->name('website.activity.deal.edit');
    Route::get('/website/activity-deal/delete/{id}', [
        'uses' => 'WebsiteController@activity_deal_destroy'
    ])->name('website.activity.deal.delete');

    //Flight Deal List
    Route::get('/website/flight-deal', [
        'uses' => 'WebsiteController@flight_deal'
    ])->name('website.flight.deal');
    Route::get('/website/flight-deal/add', [
        'uses' => 'WebsiteController@flight_deal_add'
    ])->name('website.flight.deal.add');
    Route::get('/website/flight-deal/edit/{id}', [
        'uses' => 'WebsiteController@flight_deal_edit'
    ])->name('website.flight.deal.edit');
    Route::get('/website/flight-deal/delete/{id}', [
        'uses' => 'WebsiteController@flight_deal_destroy'
    ])->name('website.flight.deal.delete');

    //Manage popup
    Route::get('/website/popup', [
        'uses' => 'WebsiteController@popup'
    ])->name('website.popup');
    Route::post('/website/popup/data', [
        'uses' => 'WebsiteController@popup_data'
    ])->name('website.popup.data');
    Route::get('/website/popup/add', [
        'uses' => 'WebsiteController@popup_add'
    ])->name('website.popup.add');
    Route::get('/website/popup/edit/{id}', [
        'uses' => 'WebsiteController@popup_edit'
    ])->name('website.popup.edit');
    Route::post('/website/popup/store', [
        'uses' => 'WebsiteController@popup_store'
    ])->name('website.popup.store');
    Route::get('/website/popup/delete/{id}', [
        'uses' => 'WebsiteController@popup_destroy'
    ])->name('website.popup.delete');

    //Manage Gallery
    Route::get('/website/gallery-album', [
        'uses' => 'WebsiteController@gallery'
    ])->name('website.gallery');
    Route::post('/website/gallery-album/data', [
        'uses' => 'WebsiteController@gallery_data'
    ])->name('website.gallery.data');
    Route::get('/website/gallery-album/add', [
        'uses' => 'WebsiteController@gallery_add'
    ])->name('website.gallery.add');
    Route::get('/website/gallery-album/edit/{id}', [
        'uses' => 'WebsiteController@gallery_edit'
    ])->name('website.gallery.edit');
    Route::post('/website/gallery-album/store', [
        'uses' => 'WebsiteController@gallery_store'
    ])->name('website.gallery.store');
    Route::get('/website/gallery-album/delete/{id}', [
        'uses' => 'WebsiteController@gallery_destroy'
    ])->name('website.gallery.delete');

    Route::get('/website/manage-album', [
        'uses' => 'WebsiteController@manage_album'
    ])->name('website.manage.album');
    Route::get('/website/manage-album/add', [
        'uses' => 'WebsiteController@manage_album_add'
    ])->name('website.manage.album.add');
    Route::get('/website/manage-album/edit/{id}', [
        'uses' => 'WebsiteController@manage_album_edit'
    ])->name('website.manage.album.edit');
    Route::post('/website/manage-album/store', [
        'uses' => 'WebsiteController@manage_album_store'
    ])->name('website.manage.album.store');
    Route::post('/website/manage-album/data', [
        'uses' => 'WebsiteController@manage_album_data'
    ])->name('website.manage.album.data');
    Route::get('/website/manage-album/delete/{id}', [
        'uses' => 'WebsiteController@manage_album_destroy'
    ])->name('website.manage.album.delete');

    Route::post('/webiste/album/images-save', [
        'uses' => 'WebsiteController@albumImageSave'
    ])->name('album.images.store');
    Route::post('/webiste/album/images-delete', [
        'uses' => 'WebsiteController@albumImageDelete'
    ])->name('album.images.destroy');
    Route::get('/tour/album/get-images/{id}', [
        'uses' => 'WebsiteController@albumImageFetch'
    ])->name('album.images.fetch');


    //Manage Push Notification
    Route::get('/website/push-notification', [
        'uses' => 'WebsiteController@pushnotification'
    ])->name('website.pushnotification');
    Route::get('/website/push-notification/add', [
        'uses' => 'WebsiteController@pushnotification_add'
    ])->name('website.pushnotification.add');
    Route::get('/website/push-notification/edit/{id}', [
        'uses' => 'WebsiteController@pushnotification_edit'
    ])->name('website.pushnotification.edit');
    Route::get('/website/push-notification/delete/{id}', [
        'uses' => 'WebsiteController@pushnotification_destroy'
    ])->name('website.pushnotification.delete');

    //Search Keywords
    Route::get('/website/search-keywords', [
        'uses' => 'WebsiteController@search_keywords'
    ])->name('website.search_keywords');

    Route::get('/website/tour-section-name', [
        'uses' => 'WebsiteController@tour_section_name'
    ])->name('website.tour.section.name.list');

    Route::post('/website/tour-section-name/data', [
        'uses' => 'WebsiteController@tour_section_name_data'
    ])->name('website.tour.section.name.data');

    Route::get('/website/tour-section-name-edit/{id}', [
        'uses' => 'WebsiteController@tour_section_name_edit'
    ])->name('website.tour.section.name.edit');
    Route::post('/website/tour-section-name/update', [
        'uses' => 'WebsiteController@tour_section_name_update'
    ])->name('website.tour.section.name.update');


    // Route::get('/domestic-holidays/edit/{id}', [
    //     'uses' => 'TourController@domestic_holiday_edit'
    // ])->name('tour.domestic-holidays.edit');

    // Route::post('/domestic-holidays/update', [
    //     'uses' => 'TourController@domestic_holiday_update'
    // ])->name('domestic-holidays.update');

    /*     * *******************************************************************
     *                                                                   *
     *                             Testimonial                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/testimonial', [
        'uses' => 'TestimonialController@index'
    ])->name('website.testimonial');
    Route::post('/testimonial/data', [
        'uses' => 'TestimonialController@data'
    ])->name('testimonial.data');
    Route::get('/testimonial/add', [
        'uses' => 'TestimonialController@create'
    ])->name('testimonial.add');
    Route::get('/testimonial/edit/{id}', [
        'uses' => 'TestimonialController@edit'
    ])->name('testimonial.edit');
    Route::post('/testimonial/store', [
        'uses' => 'TestimonialController@store'
    ])->name('testimonial.store');
    Route::get('/testimonial/delete/{id}', [
        'uses' => 'TestimonialController@destroy'
    ])->name('testimonial.delete');
    /*     * *******************************************************************
     *                                                                   *
     *                             video                              *
     *                                                                   *
     * ******************************************************************* */

    //video category
    Route::get('/video/category', [
        'uses' => 'VideoController@index'
    ])->name('video.category.index');
    Route::post('/video/category/data', [
        'uses' => 'VideoController@data'
    ])->name('video.category.data');
    Route::post('/video/store', [
        'uses' => 'VideoController@store'
    ])->name('video.category.store');
    Route::get('/video/category/add', [
        'uses' => 'VideoController@create'
    ])->name('video.category.add');
    Route::get('/video/category/edit/{id}', [
        'uses' => 'VideoController@edit'
    ])->name('video.category.edit');
    Route::get('/video/category/delete/{id}', [
        'uses' => 'VideoController@destroy'
    ])->name('video.category.delete');

    //video album
    Route::get('/video/album', [
        'uses' => 'VideoController@album'
    ])->name('video.album.index');
    Route::post('/video/album/store', [
        'uses' => 'VideoController@album_store'
    ])->name('video.album.store');
    Route::post('/video/album/data', [
        'uses' => 'VideoController@album_data'
    ])->name('website.video.album.data');
    Route::get('/video/album/add', [
        'uses' => 'VideoController@album_create'
    ])->name('video.album.add');
    Route::get('/video/album/edit/{id}', [
        'uses' => 'VideoController@album_edit'
    ])->name('video.album.edit');
    Route::get('/video/album/delete/{id}', [
        'uses' => 'VideoController@album_destroy'
    ])->name('video.album.delete');

    /*     * ******** Hotel Facilities ****** */
    Route::get('/hotel-facilities', [
        'uses' => 'HotelfacilitiesController@index'
    ])->name('hotel-facilities.index');

    Route::post('/hotel-facilities/data', [
        'uses' => 'HotelfacilitiesController@data'
    ])->name('hotel-facilities.data');

    Route::get('/hotel-facilitie/add', [
        'uses' => 'HotelfacilitiesController@create'
    ])->name('hotel-facilitie.add');

    Route::get('/hotel-facilitie/edit/{id}', [
        'uses' => 'HotelfacilitiesController@edit'
    ])->name('hotel-facilitie.edit');
    Route::post('/hotel-facilitie/store', [
        'uses' => 'HotelfacilitiesController@save'
    ])->name('hotel-facilities.store');

    Route::get('/hotel-facilitie/delete/{id}', [
        'uses' => 'HotelfacilitiesController@destroy'
    ])->name('hotel-facilitie.delete');


    /*     * ***********Room Facilities*************** */
    Route::get('/room-facilities', [
        'uses' => 'HotelfacilitiesController@room_index'
    ])->name('room.facilities');
    Route::post('/room-facilities/data', [
        'uses' => 'HotelfacilitiesController@room_data'
    ])->name('room-facilities.data');
    Route::get('/room-facilities/add', [
        'uses' => 'HotelfacilitiesController@room_add'
    ])->name('room.facilities.add');
    Route::get('/room-facilities/edit/{id}', [
        'uses' => 'HotelfacilitiesController@room_edit'
    ])->name('room.facilities.edit');
    Route::post('/room-facilities/store', [
        'uses' => 'HotelfacilitiesController@room_store'
    ])->name('room.facilities.store');
    Route::get('/room-facilities/delete/{id}', [
        'uses' => 'HotelfacilitiesController@room_destroy'
    ])->name('room.facilities.delete');

    /*     * ******Hotel Category */
    Route::get('/hotel/type/', [
        'uses' => 'HotelController@category'
    ])->name('hotel.category.index');
    Route::get('/hotel/type/add', [
        'uses' => 'HotelController@category_add'
    ])->name('hotel.category.add');
    Route::get('/hotel/type/edit/{id}', [
        'uses' => 'HotelController@category_edit'
    ])->name('hotel.category.edit');
    Route::get('/hotel/type/delete/{id}', [
        'uses' => 'HotelController@category_destroy'
    ])->name('hotel.category.delete');
    Route::post('/hotel/type/store/', [
        'uses' => 'HotelController@category_store'
    ])->name('hotel.category.store');
    Route::post('/hotel/type/data', [
        'uses' => 'HotelController@category_data'
    ])->name('hotel.type.data');

    /*     * **********Email Template************* */
    Route::get('/email-template/', [
        'uses' => 'EmailtemplateController@index'
    ])->name('email.template.index');
    Route::get('/email-template/add', [
        'uses' => 'EmailtemplateController@create'
    ])->name('email.template.add');
    Route::get('/email-template/edit/{id}', [
        'uses' => 'EmailtemplateController@edit'
    ])->name('email.template.edit');
    Route::get('/email-template/delete/{id}', [
        'uses' => 'EmailtemplateController@destroy'
    ])->name('email.template.delete');

    /*     * ******** Suppliers ****** */
    Route::get('/suppliers', [
        'uses' => 'SupplierController@index'
    ])->name('supplier.index');
    Route::get('/suppliers/view/{id}', [
        'uses' => 'SupplierController@show'
    ])->name('supplier.view');
    Route::post('/suppliers', [
        'uses' => 'SupplierController@data'
    ])->name('supplier.data');
    Route::get('/supplier/add', [
        'uses' => 'SupplierController@create'
    ])->name('supplier.add');
    Route::get('/supplier/edit/{id}', [
        'uses' => 'SupplierController@edit'
    ])->name('supplier.edit');
    Route::post('/supplier/store', [
        'uses' => 'SupplierController@store'
    ])->name('supplier.store');
    Route::get('/supplier/delete/{id}', [
        'uses' => 'SupplierController@destroy'
    ])->name('supplier.delete');

    //Supplier type
    Route::get('supplier/type', [
        'uses' => 'SupplierController@supplier_type'
    ])->name('supplier.type.index');
    Route::get('suppliertype/add', [
        'uses' => 'SupplierController@supplier_type_add'
    ])->name('supplier.type.add');
    Route::post('suppliertype/add', [
        'uses' => 'SupplierController@supplier_type_data'
    ])->name('supplier.type.data');
    Route::get('suppliertype/edit/{id}', [
        'uses' => 'SupplierController@supplier_type_edit'
    ])->name('supplier.type.edit');
    Route::post('suppliertype/store', [
        'uses' => 'SupplierController@supplier_type_store'
    ])->name('supplier.type.store');
    Route::get('suppliertype/delete/{id}', [
        'uses' => 'SupplierController@supplier_type_destroy'
    ])->name('supplier.type.delete');

    Route::get('/supplier/notes/{agentid}', [
        'uses' => 'SupplierController@notes'
    ])->name('supplier.notes');
    Route::get('/supplier/notes/add/{supid}', [
        'uses' => 'SupplierController@notes_add'
    ])->name('supplier.notes.add');
    Route::get('/supplier/notes/edit/{supid}/{id}', [
        'uses' => 'SupplierController@notes_edit'
    ])->name('supplier.notes.edit');
    Route::get('/supplier/notes/delete/{supid}/{id}', [
        'uses' => 'SupplierController@notes_delete'
    ])->name('supplier.notes.delete');

    Route::get('/supplier/document/add/{supid}', [
        'uses' => 'SupplierController@document_add'
    ])->name('supplier.document.add');
    Route::get('/supplier/document/edit/{supid}/{id}', [
        'uses' => 'SupplierController@document_edit'
    ])->name('supplier.document.edit');

    /*     * ******** Discount ******* */
    Route::get('/discount', [
        'uses' => 'DiscountController@index'
    ])->name('discount.index');
    Route::post('/discount/data', [
        'uses' => 'DiscountController@data'
    ])->name('discount.data');
    Route::get('/discount/add', [
        'uses' => 'DiscountController@create'
    ])->name('discount.add');
    Route::get('/discount/edit/{id}', [
        'uses' => 'DiscountController@edit'
    ])->name('discount.edit');
    Route::post('/discount/store', [
        'uses' => 'DiscountController@store'
    ])->name('discount.store');
    Route::get('/discount/delete/{id}', [
        'uses' => 'DiscountController@destroy'
    ])->name('discount.delete');

    /*     * ********Inventory********** */
    Route::get('/inventory', [
        'uses' => 'InventoryController@index'
    ])->name('inventory.index');
    Route::get('/inventory/add', [
        'uses' => 'InventoryController@create'
    ]);
    Route::get('/inventory/edit/{id}', [
        'uses' => 'InventoryController@edit'
    ]);
    Route::get('/inventory/delete/{id}', [
        'uses' => 'InventoryController@destroy'
    ]);
    Route::get('/inventory/history/{id}', [
        'uses' => 'InventoryController@history'
    ]);
    Route::get('/room/inventory', [
        'uses' => 'InventoryController@room_inventory'
    ]);
    Route::get('/room/calendar-price/update', [
        'uses' => 'InventoryController@room_price_update'
    ]);
    Route::get('/room/calendar-price/get-room', [
        'uses' => 'InventoryController@get_room'
    ]);
    Route::get('/room/calendar-price/get-navigate', [
        'uses' => 'InventoryController@get_navigate'
    ]);

    /*     * ******Buy Inventory*********** */
    Route::get('hotel/view/{id}', [
        'uses' => 'BuyinventoryController@hotel_view'
    ]);

    Route::get('hotel-rooms', [
        'uses' => 'BuyinventoryController@index'
    ]);
    Route::get('buy-hotel-rooms/{id}', [
        'uses' => 'BuyinventoryController@buy_rooms'
    ]);
    Route::get('hotel/book/{id}', [
        'uses' => 'BuyinventoryController@booking'
    ]);

    /*     * ***********Certificates************** */
    Route::get('certificate', [
        'uses' => 'CertificateController@index'
    ])->name('certificates.index');
    Route::post('certificate/data', [
        'uses' => 'CertificateController@data'
    ])->name('certificates.data');
    Route::get('certificate/add', [
        'uses' => 'CertificateController@create'
    ])->name('certificates.add');
    Route::get('certificate/edit/{id}', [
        'uses' => 'CertificateController@edit'
    ])->name('certificates.edit');
    Route::post('certificate/store}', [
        'uses' => 'CertificateController@store'
    ])->name('certificates.store');
    Route::get('certificate/delete/{id}', [
        'uses' => 'CertificateController@destroy'
    ])->name('certificates.delete');



    /*     * *******************************************************************
     *                                                                   *
     *                             Country                              *
     *                                                                   *
     * ******************************************************************* */

    /////////////////// Country//////////////
    Route::get('/country', [
        'uses' => 'CountryController@index'
    ])->name('country.index');
    Route::post('/country/data', [
        'uses' => 'CountryController@data'
    ])->name('country.data');
    Route::get('/country/add', [
        'uses' => 'CountryController@create'
    ])->name('country.add');
    Route::post('/country/save', [
        'uses' => 'CountryController@store'
    ])->name('country.save');
    Route::get('/country/edit/{id}', [
        'uses' => 'CountryController@edit'
    ])->name('country.edit');
    Route::get('/country/delete/{id}', [
        'uses' => 'CountryController@destroy'
    ])->name('country.delete');
////////////////////////// state ////////////////
    Route::post('/state/data', [
        'uses' => 'CountryController@statedata'
    ])->name('state.data');
    Route::get('/state/add/', [
        'uses' => 'CountryController@state_add'
    ])->name('state.add');
    Route::post('/state/save', [
        'uses' => 'CountryController@state_store'
    ])->name('state.save');
    Route::get('/state/edit/{id}', [
        'uses' => 'CountryController@state_edit'
    ])->name('state.edit');
    Route::get('/state/delete/{id}', [
        'uses' => 'CountryController@state_delete'
    ])->name('state.delete');
////////////////////////// state ////////////////
    Route::post('/city/data', [
        'uses' => 'CountryController@citydata'
    ])->name('city.data');
    Route::post('/city/save', [
        'uses' => 'CountryController@city_store'
    ])->name('city.save');
    Route::get('/city/add', [
        'uses' => 'CountryController@city_add'
    ])->name('city.add');
    Route::get('/city/edit/{id}', [
        'uses' => 'CountryController@city_edit'
    ])->name('city.edit');
    Route::get('/city/delete/{id}', [
        'uses' => 'CountryController@city_destroy'
    ])->name('city.delete');




    /*     * *******************************************************************
     *                                                                   *
     *                             Quotation                              *
     *                                                                   *
     * ******************************************************************* */

    Route::get('/quotation', [
        'uses' => 'QuotationController@index'
    ])->name('quotation.index');
    Route::post('/quotation/data', [
        'uses' => 'QuotationController@data'
    ]);

    Route::get('/quotation/add/{lead_id}', [
        'uses' => 'QuotationController@create'
    ])->name('quotation.add');

    Route::get('/quotation/edit/{id}', [
        'uses' => 'QuotationController@edit'
    ]);

    Route::any('/quotation/save/{lead_id}', [
        'uses' => 'QuotationController@save'
    ])->name('quotation.save');

    Route::get('/quotation/delete/{id}', [
        'uses' => 'QuotationController@destroy'
    ])->name('quotation.delete');

    Route::get('/currency', [
        'uses' => 'CurrencymasterController@index'
    ])->name('currency.index');
    Route::post('/currency/data', [
        'uses' => 'CurrencymasterController@data'
    ])->name('currency.data');
    Route::get('/currency/add', [
        'uses' => 'CurrencymasterController@create'
    ])->name('currency.add');
    Route::get('/currency/edit/{id}', [
        'uses' => 'CurrencymasterController@edit'
    ])->name('currency.edit');
    Route::post('/currency/store', [
        'uses' => 'CurrencymasterController@store'
    ])->name('currency.store');
    Route::get('/currency/delete/{id}', [
        'uses' => 'CurrencymasterController@destroy'
    ])->name('currency.delete');
});

Route::post('/admin/user/login', 'admin\UserController@login');
Route::post('/admin/login/login', 'admin\LoginController@login');
Route::get('/admin/logout', 'admin\LogoutController@index');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('posts', 'PostController');
});


//////////////////////////////////// Front HTML pages /////////////////////////////////
//
Route::get('/about', [
    'uses' => 'client\AboutController@index'
])->name('about');
Route::get('/faq', [
    'uses' => 'client\FaqController@index'
])->name('faq');
Route::get('/terms-conditions', [
    'uses' => 'client\FaqController@terms_conditions'
])->name('terms-conditions');
Route::get('/contact', [
    'uses' => 'client\ContactController@index'
])->name('contact');

// Route::get('/hot-domestic-holidays', [
//     'uses' => 'client\HolidayController@hot_domestic_holidays'
// ])->name('hot-domestic-holiday');

// Route::get('/hot-international-holidays', [
//     'uses' => 'client\HolidayController@hot_international_holidays'
// ])->name('hot-international-holiday');

////////////// domestic holiday///////////
Route::get('/indian-holidays', [
    'uses' => 'client\HolidayController@domestic_holidays'
])->name('indian-holiday');

Route::get('/indian-holidays/{sectorId}/{sectorName}', [
    'uses' => 'client\HolidayController@sector_holidays'
])->name('indian-holiday.sector');

/////////// global holidays///////////////
Route::get('/global-holidays', [
    'uses' => 'client\HolidayController@global_holidays'
])->name('global-holiday');

Route::get('/global-holidays/{sectorId}/{sectorName}', [
    'uses' => 'client\HolidayController@global_sector_holidays'
])->name('global-holiday.sector');

/////////// Holidays By Theme///////////////
Route::get('/speciality-tours', [
    'uses' => 'client\HolidayController@specialty_tours'
])->name('speciality-tours');

Route::get('/speciality-tours/{typeId}/{CategoryName}', [
    'uses' => 'client\HolidayController@specialty_tours_category'
])->name('speciality-tours.category');

Route::get('/cruise-tours', [
    'uses' => 'client\HolidayController@cruise_tours'
])->name('cruise-tours');

// Route::get('/holidays-by-theme', [
//     'uses' => 'client\HolidayController@theme_holidays'
// ])->name('holidays-by-theme');

// Route::get('/holidays-by-theme/{sectorId}/{sectorName}', [
//     'uses' => 'client\HolidayController@theme_holidays_sector'
// ])->name('holidays-by-theme.sector');

// Route::get('/holidays-by-theme/{categoryId}/{categoryName}', [
//     'uses' => 'client\HolidayController@theme_holidays'
// ])->name('holidays-by-theme.category');

Route::get('/holiday-detail/{tourId}/{tourName}', [
    'uses' => 'client\HolidayController@holiday_detail'
])->name('holiday-detail');

Route::POST('/holiday-package-detail/', [
    'uses' => 'client\HolidayController@holiday_package_detail'
])->name('holiday-package-detail');

Route::get('/tour-compare', [
    'uses' => 'client\HolidayController@compare_tour'
])->name('tour-compare');

Route::get('/empty-tour', [
    'uses' => 'client\HolidayController@empty_tour'
])->name('empty-tour');

Route::POST('/add-to-compare/', [
    'uses' => 'client\HolidayController@add_to_compare'
])->name('add-to-compare');

Route::get('/login', [
    'uses' => 'client\LoginController@index'
])->name('client_login_form');

Route::get('/agent-login', [
    'uses' => 'client\AgentLoginController@index'
])->name('agent_login_form');


Route::POST('client_login', 'client\LoginController@login')->name('client_login');
Route::POST('agent_login', 'client\AgentLoginController@login')->name('agent_login');

// Route::get('login/google', 'client\LoginController@redirectToProvider')->name('google.login');
// Route::get('login/google/callback', 'client\LoginController@handleProviderCallback');

// Route::get('login/facebook', 'client\LoginController@FacebookLogin')->name('facebook.login');
// Route::get('login/facebook/callback', 'client\LoginController@FacebookResponse');


// Route::get('login/twitter', 'client\LoginController@TwitterLogin')->name('twitter.login');
// Route::get('login/callback/twitter', 'client\LoginController@TwitterResponse');



Route::get('login/callback/{provider}', 'client\LoginController@callback');
Route::get('login/redirect/{provider}/{type}', 'client\LoginController@redirect');

Route::get('/forgot-password', [
    'uses' => 'client\LoginController@forgot_password'
])->name('forgot_password');

Route::POST('/forgot-password-request', 'client\LoginController@forgot_password_request')->name('forgot-password-request');

Route::get('/reset-password/{token}', [
    'uses' => 'client\LoginController@reset_password'
])->name('reset_password');

Route::POST('/reset-password-request', 'client\LoginController@reset_password_request')->name('reset-password-request');

// Agent Forgot Password
Route::get('/agent-forgot-password', [
    'uses' => 'client\AgentLoginController@forgot_password'
])->name('agent_forgot_password');

Route::POST('/agent-forgot-password-request', 'client\AgentLoginController@forgot_password_request')->name('agent-forgot-password-request');

Route::get('/agent-reset-password/{token}', [
    'uses' => 'client\AgentLoginController@reset_password'
])->name('agent_reset_password');

Route::POST('/agent-reset-password-request', 'client\AgentLoginController@reset_password_request')->name('agent-reset-password-request');

// Route::POST('forgot_password_request', 'client\LoginController@forgot_password_request')->name('forgot_password_request');

 Route::get('/activation-user/{token}', [
    'uses' => 'client\LoginController@activation_user'
])->name('activation_user');



Route::get('/getstate/{country_id}', [
    'uses' => 'client\LoginController@getStateByCountry'
])->name('state_by_country_front');

Route::get('/getcity/{state_id}', [
    'uses' => 'client\LoginController@getCityByState'
])->name('city_by_state_front');

Route::POST('/duplicate-email/', [
    'uses' => 'client\LoginController@duplicate_email'
])->name('duplicate_email');

Route::POST('/duplicate-phone_no/', [
    'uses' => 'client\LoginController@duplicate_phone_no'
])->name('duplicate_phone_no');

Route::get('/logout', [
    'uses' => 'client\LogoutController@index'
])->name('client_logout');

Route::get('/inquiry-now/{tourId}/{tourName}/{packageType}', [
    'uses' => 'HomeController@inquiry_now'
])->name('inquiry-now');

Route::POST('/newsletter', [
    'uses' => 'client\NewsletterController@index'
])->name('newsletter');

Route::get('/newsletter-suc', [
    'uses' => 'client\NewsletterController@newsletter_suc'
])->name('newsletter-suc');

// Route::POST('/add-tour/', [
//     'uses' => 'client\HolidayController@add_tour'
// ])->name('add-tour');

Route::group(['namespace' => 'client', 'middleware' => ['checkUserType:Customer|Agent']], function() {
    Route::get('/dashboard', [
        'uses' => 'DashboardController@index'
    ])->name('user_dashboard');

    Route::post('/user-lead/data', [
        'uses' => 'DashboardController@data'
    ])->name('user.lead.data');

    Route::post('/wallet/data', [
        'uses' => 'DashboardController@wallet_data'
    ])->name('wallet.data');

    Route::post('/user-lead/conform-lead-data', [
        'uses' => 'DashboardController@conform_lead_data'
    ])->name('conform.lead.data');

    Route::get('/update-flight/{leadId}', [
        'uses' => 'DashboardController@update_flight'
    ])->name('update-flight');

    Route::get('/update-hotel/{leadId}', [
        'uses' => 'DashboardController@update_hotel'
    ])->name('update-hotel');

    Route::get('/update-holiday/{leadId}', [
        'uses' => 'DashboardController@update_holiday'
    ])->name('update-holiday');

    Route::get('/update-visa/{leadId}', [
        'uses' => 'DashboardController@update_visa'
    ])->name('update-visa');

    Route::get('/update-tour/{leadId}', [
        'uses' => 'DashboardController@update_tour'
    ])->name('update-tour');
    
    Route::get('/view-quotation/{leadId}', [
    'uses' => 'DashboardController@view_quotation'
    ])->name('view-quotation');
    
    Route::get('/quotation-info/{lead_id}/{quot_id}', [
        'uses' => 'DashboardController@quotation_info'
    ])->name('quotation_info');

    Route::get('/mail-quotation/{leadId}/{qty_id}', [
        'uses' => 'DashboardController@mail_quotation'
    ])->name('mail-quotation');

    Route::get('/quotation/make-payment/{lead_id}/{quot_id}', [
        'uses' => 'DashboardController@make_payment'
    ])->name('quotation.make.payment');

    Route::get('/pay-now/', [
        'uses' => 'client\PaymentController@pay_now'
    ])->name('quotation.pay-now');

    Route::get('/user-profile/', [
        'uses' => 'DashboardController@user_profile'
    ])->name('user.profile.page');

    Route::post('/user-profile-save/', [
        'uses' => 'DashboardController@store'
    ])->name('user.profile.save');


    Route::post('/change-password/', [
        'uses' => 'DashboardController@change_password'
    ])->name('user.change.password');


    Route::get('/get-state/{country_id}', [
        'uses' => 'DashboardController@getStateByCountry'
    ])->name('state.edit.profile');

    Route::get('/get-city/{state_id}', [
        'uses' => 'DashboardController@getCityByState'
    ])->name('city.edit.profile');

});



Route::get('/payment/user-form', [
    'uses' => 'client\PaymentController@user_form'
])->name('user-form');

Route::get('/payment', [
    'uses' => 'client\PaymentController@index'
])->name('payment');

Route::POST('/payment/form_submit', [
    'uses' => 'client\PaymentController@form_submit'
])->name('form_submit');

Route::POST('/payment/quotation-payment', [
    'uses' => 'client\PaymentController@quotation_payment'
])->name('make.quotation.payment');

Route::POST('/check-booking-id/', [
    'uses' => 'client\PaymentController@check_booking_id'
])->name('check-booking-id');

Route::POST('/check-otp/', [
    'uses' => 'client\PaymentController@check_otp'
])->name('check-otp');


Route::get('/payment-info/{leadId}', [
    'uses' => 'client\PaymentController@payment_info'
])->name('payment-info');

Route::POST('/atom', [
    'uses' => 'client\PaymentController@sendToAtom'
])->name('send_to_atom');

Route::post('/response', [
    'uses' => 'client\PaymentController@atomResponse'
])->name('atom_response');

Route::get('/payment-response/{tra_id}', [
    'uses' => 'client\PaymentController@payment_response'
])->name('payment-response');

Route::post('/user-response', [
    'uses' => 'client\PaymentController@user_response'
])->name('user-response');

Route::get('/pay-now/{leadId}/{qty_id}', [
    'uses' => 'client\PaymentController@pay_now'
])->name('quotation.pay-now');

Route::get('/user-payment-response/{tra_id}', [
    'uses' => 'client\PaymentController@user_payment_response'
])->name('user-payment-response');

Route::get('/tour-filter', [
    'uses' => 'client\HolidayController@tour_filter'
])->name('tour_filter');
 
//
//////////////////////////////////// Front HTML pages end /////////////////////////////////
