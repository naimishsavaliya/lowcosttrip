<?php

use Illuminate\Database\Seeder;

class HomeTourSectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home_tour_sections')->insert([
            [
                'primary_name'  => 'Domestic Holidays',
                'current_name'  => 'Domestic Holidays'
            ],
            [
                'primary_name'  => 'International Holidays',
                'current_name'  => 'International Holidays'
            ],
            [
                'primary_name'  => 'Featured Tours',
                'current_name'  => 'Featured Tours'
            ],
            [
                'primary_name'  => 'Hot Deals Tours',
                'current_name'  => 'Hot Deals Tours'
            ],
            [
                'primary_name'  => 'New Arrival Tours',
                'current_name'  => 'New Arrival Tours'
            ],
            [
                'primary_name'  => 'Featured Destination Tours',
                'current_name'  => 'Featured Destination Tours'
            ]
     	]);
    }
}
