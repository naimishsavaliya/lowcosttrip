<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('hotel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temp_id')->nullable();
            $table->string('name', 255)->nullable();
            $table->integer('destination_id')->nullable();
            $table->tinyInteger('star')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('pincode')->nullable();
            $table->text('address')->nullable();
            $table->smallInteger('timezone')->nullable();
            $table->integer('mobile')->nullable();
            $table->string('landline', 25)->nullable();
            $table->string('fax', 25)->nullable();
            $table->string('email', 55)->nullable();
            $table->string('manager_name', 155)->nullable();
            $table->string('reference_no', 25)->nullable();
            $table->text('map_code')->nullable();
            $table->string('check_in_time', 5)->nullable();
            $table->string('check_out_time', 5)->nullable();
            $table->string('tags', 255)->nullable();
            $table->longText('brief_description')->nullable();
            $table->longText('hotel_policies')->nullable();
            $table->string('facilities', 255)->nullable();
            $table->string('hotel_logo', 255)->nullable();
            $table->string('photos', 255)->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('hotel');
    }

}
