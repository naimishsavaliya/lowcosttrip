<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('receipt_no')->nullable();
            $table->integer('lead_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('tour_id')->unsigned()->nullable();
            $table->string('type_of_payment')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('payment_mode')->nullable();
            $table->decimal('paid_amount',10,2)->nullable();
            $table->string('receipt_name')->nullable();
            $table->text('payment_note')->nullable();
            $table->timestamp('paid_date')->nullable();
            $table->string('additional_services_name')->nullable();
            $table->decimal('additional_service_fees',10,2)->nullable();
            $table->text('additional_services_notes')->nullable();
            $table->decimal('discount_amount',10,2)->nullable();
            $table->text('discount_notes')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('merchant_transaction_id')->nullable();
            $table->string('bank_transaction_id')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('auth_code')->nullable();
            $table->text('description')->nullable();
            $table->integer('booking_id')->unsigned()->nullable();
            $table->string('ref_no')->nullable();
            $table->decimal('cancel_amount',10,2)->nullable();
            $table->decimal('refund_amount',10,2)->nullable();
            $table->decimal('total_amount',10,2)->nullable();
            $table->decimal('balance',10,2)->nullable();
            $table->string('payment_currency',15)->nullable();
            $table->integer('credit_note_id')->unsigned()->nullable();
            $table->enum('active',['Y', 'N'])->default('N')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_payment');
    }
}
