<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCnfLeadAutoNoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cnf_lead_auto_no', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
		DB::update("ALTER TABLE cnf_lead_auto_no AUTO_INCREMENT = 10001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cnf_lead_auto_no');
    }
}
