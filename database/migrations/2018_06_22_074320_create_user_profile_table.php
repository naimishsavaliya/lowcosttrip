<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('manager_id')->nullable();
            $table->integer('subscription_id')->nullable();
            $table->string('title')->nullable();
            $table->text('address')->nullable();
            $table->integer('country_id')->default(0);
            $table->integer('state_id')->default(0);
            $table->integer('city_id')->default(0);
            $table->string('pin_code', 255);
            $table->integer('age')->default(0);
            $table->string('time_zone', 255);
            $table->string('phone_no', 255);
            $table->string('landline_no', 255);
            $table->enum('gender', array('M', 'F'))->nullable()->comment('M=male, F=female');
            $table->date('birth_date');
            $table->text('information')->nullable();
            $table->string('image', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users_profile');
    }

}
