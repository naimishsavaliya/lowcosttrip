<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('lead', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id')->nullable();
            $table->string('lead_id', 55)->nullable();
            $table->integer('customer_type_id')->nullable();
            $table->integer('lead_source')->nullable();
            $table->string('name', 255)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('phone', 25)->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('pincode')->nullable();
            $table->integer('interested_in')->nullable();
            $table->date('interested_date')->nullable();
            $table->tinyInteger('domestic_international')->nullable();
            $table->tinyInteger('group_customised')->nullable();
            $table->integer('adult')->nullable();
            $table->integer('child')->nullable();
            $table->integer('infant')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('assign_to', 55)->nullable();
            $table->integer('confirm_by')->nullable();
            $table->integer('total_amount')->nullable();
            $table->integer('payed_amount')->nullable();

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('lead');
    }

}
