<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('discount', function (Blueprint $table) {
            $table->increments('desId');
            $table->string('discount_for', 255);
            $table->string('discount_code', 255);
            $table->boolean('type')->comment('1=fix, 2=percentage');
            $table->float('amount', 8, 2)->nullable();
            $table->float('max_discount_amount', 8, 2)->nullable();
            $table->float('min_cart_amount', 8, 2)->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->text('valid_on')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('discount');
    }

}
