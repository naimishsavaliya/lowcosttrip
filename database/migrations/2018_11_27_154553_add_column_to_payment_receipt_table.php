<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToPaymentReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_receipt', function (Blueprint $table) {
             $table->string('type')->nullable();
        });
        Schema::table('lead_quatation', function (Blueprint $table) {
             $table->integer('quatation_id')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_receipt', function (Blueprint $table) {
            //
        });
    }
}
