<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightPassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_passengers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flight_books_id')->nullable();
            $table->string('passengers_type', 255)->nullable();
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('gender',10)->nullable();
            $table->string('meals',255)->nullable();
            $table->float('meals_price',10,2)->nullable();
            $table->string('seats',255)->nullable();
            $table->float('seats_price',10,2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_passengers');
    }
}
