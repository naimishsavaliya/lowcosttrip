<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToLeadQuatationPerticularTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_quatation_perticular', function (Blueprint $table) {
            $table->decimal('gst',10,2)->nullable();
            $table->decimal('cgst',10,2)->nullable();
            $table->decimal('sgst',10,2)->nullable();
            $table->decimal('igst',10,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_quatation_perticular', function (Blueprint $table) {
            //
        });
    }
}
