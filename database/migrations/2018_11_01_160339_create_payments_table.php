<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_name');
            $table->string('mobile_number')->nullable();
            $table->string('email')->nullable();
            $table->string('tour_code')->nullable();
            $table->integer('currency_code')->unsigned()->nullable();
            $table->float('amount',8,2)->nullable();
            $table->float('gross_amount',8,2)->nullable();
            $table->string('refered_by')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
