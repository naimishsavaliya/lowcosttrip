<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourImagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tour_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temp_id');
            $table->integer('tour_id')->defualt(0)->nullable();
            $table->string('filename', 1000);
            $table->string('resized_name', 1000);
            $table->string('original_name', 1000);
            $table->string('file_size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tour_images');
    }

}
