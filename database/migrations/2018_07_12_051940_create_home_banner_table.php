<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeBannerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('home_banners', function (Blueprint $table) {
            $table->increments('banId');
            $table->string('title', 255);
            $table->string('tooltip', 255)->nullable();
            $table->smallInteger('banner_opt')->default(0)->comment('1=1 Image, 2=2 Image, 3=3 Image');
            $table->smallInteger('banner_cat')->default(0)->comment('1=Holidays, 2=Flights, 3=Hotels, 4=Activity');
            $table->string('redirect_to')->nullable();
            $table->string('image_1', 255)->nullable();
            $table->string('image_2', 255)->nullable();
            $table->string('image_3', 255)->nullable();
            $table->text('description')->nullable();
            $table->integer('sort_ord')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('home_banners');
    }

}
