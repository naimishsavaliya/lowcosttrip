<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationPerticularTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('quotation_perticular', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotation_id')->nullable();
            $table->string('item_no', 20)->nullable();
            $table->string('item_title', 255)->nullable();
            $table->integer('qty')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('discount')->nullable();
            $table->integer('line_total')->nullable();
            $table->integer('item_id')->nullable();
            $table->string('item_type', 25)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('quotation_perticular');
    }

}
