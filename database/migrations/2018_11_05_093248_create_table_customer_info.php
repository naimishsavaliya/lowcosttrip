<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomerInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_info', function (Blueprint $table) {
            $table->increments('id');
			$table->integer("lead_id")->nullable();
			$table->string("name")->nullable();
			$table->string("accomodation_id")->nullable();
			$table->date("birth_date")->nullable();
			$table->date("anniversary_date")->nullable();
			$table->string("mobile_no")->nullable();
			$table->string("email")->nullable();
			$table->string("passport_no")->nullable();
			$table->date("issue_date")->nullable();
			$table->string("issue_place")->nullable();
			$table->date("passport_exp_date")->nullable();
			$table->enum("gender",['M','F'])->nullable();
			$table->tinyInteger('child_age_group')->nullable();
			$table->unsignedDecimal('amount', 12, 2)->nullable();
            $table->timestamps();
			$table->integer("created_by")->nullable();
			$table->integer("updated_by")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_info');
    }
}
