<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAgeColumnStructureProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_profile', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users_profile` CHANGE `age` `age` INT(11) NULL DEFAULT NULL;");
            DB::statement("ALTER TABLE `users_profile` CHANGE `birth_date` `birth_date` VARCHAR(50) NULL DEFAULT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_profile', function (Blueprint $table) {
            //
        });
    }
}
