<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsToPaymentResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_response', function (Blueprint $table) {
            $table->tinyInteger('is_payment_type')->comment('1=>Bank,2=>Wallet,3=>Both')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_response', function (Blueprint $table) {
            $table->dropColumn('is_payment_type');
        });
    }
}
