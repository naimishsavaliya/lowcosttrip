<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebPopupTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('web_popup', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('redirect_link', 1000);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('image', 255)->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('web_popup');
    }

}
