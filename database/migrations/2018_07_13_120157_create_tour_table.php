<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tour', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('agent_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('tour_name', 255)->nullable();
            $table->string('tour_code', 55)->nullable();
            $table->tinyInteger('tour_nights')->nullable();
            $table->integer('domestic_international')->nullable();
            $table->string('tour_type', 25)->nullable();
            $table->tinyInteger('public_private')->nullable();
            $table->integer('sector')->nullable();
            $table->tinyInteger('group_customized')->nullable();
            $table->tinyInteger('minimum_tourist')->nullable();
            $table->tinyInteger('maximum_tourist')->nullable();
            $table->integer('online_booking_minimum_amount')->nullable();
            $table->string('feature', 55)->nullable();
            $table->string('starting_point', 255)->nullable();
            $table->string('ending_point', 255)->nullable();
            $table->string('key_point', 255)->nullable();
            $table->text('short_description')->nullable();
            $table->longText('long_description')->nullable();
            $table->longText('inclusions')->nullable();
            $table->longText('exclusions')->nullable();
            $table->longText('tour_tips')->nullable();
            $table->longText('cancellation_policy')->nullable();
            $table->longText('keywords')->nullable();
            $table->longText('terms_conditions')->nullable();
            $table->integer('tour_status')->nullable();
            $table->tinyInteger('tour_step')->nullable();
            $table->string('image', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tour');
    }

}
