<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddfieldsLeadTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('lead', function (Blueprint $table) {
			if (!Schema::hasColumn('lead', 'sub_total1')) {
				$table->unsignedDecimal('sub_total1', 12, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'sub_total2')) {
				$table->unsignedDecimal('sub_total2', 12, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'discount')) {
				$table->unsignedDecimal('discount', 6, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'discount_amount')) {
				$table->unsignedDecimal('discount_amount', 12, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'IGST')) {
				$table->unsignedDecimal('IGST', 6, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'CGST')) {
				$table->unsignedDecimal('CGST', 6, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'SGST')) {
				$table->unsignedDecimal('SGST', 6, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'IGST_amount')) {
				$table->unsignedDecimal('IGST_amount', 12, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'CGST_amount')) {
				$table->unsignedDecimal('CGST_amount', 12, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'SGST_amount')) {
				$table->unsignedDecimal('SGST_amount', 12, 2)->nullable();
			}
			if (!Schema::hasColumn('lead', 'tour_id')) {
				$table->integer('tour_id')->nullable();
			}
			if (!Schema::hasColumn('lead', 'departure_date_id')) {
				$table->integer('departure_date_id')->nullable();
			}
			if (!Schema::hasColumn('lead', 'tour_type')) {
				$table->integer('tour_type')->nullable();
			}
			if (!Schema::hasColumn('lead', 'no_of_room')) {
				$table->integer('no_of_room')->nullable();
			}
			if (!Schema::hasColumn('lead', 'invoice_name')) {
				$table->string('invoice_name')->nullable();
			}
			if (!Schema::hasColumn('lead', 'cnf_lead_id')) {
				$table->string('cnf_lead_id')->nullable();
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}

}
