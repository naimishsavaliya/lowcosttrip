<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsToPaymentResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_response', function (Blueprint $table) {
            $table->float('wallet_amount',10,2)->nullable();
            $table->tinyInteger('is_wallet')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_response', function (Blueprint $table) {
            $table->dropColumn('wallet_amount');
            $table->dropColumn('is_wallet');
        });
    }
}
