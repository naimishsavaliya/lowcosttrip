<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadPaymentTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('lead_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id')->nullable();
            $table->integer('lead_id')->nullable();
            $table->string('title', 255)->nullable();
            $table->string('payment_type', 20)->nullable()->comment('Debit or credit');
            $table->integer('amount')->nullable();
            $table->string('payment_mode', 25)->nullable()->comment('Cash, cheque, RTGS, IMPS');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('lead_payment');
    }

}
