<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_response', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lead_id')->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->string('status')->nullable();
            $table->float('paid_amount',10,2)->nullable();
            $table->dateTime('paid_date')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('merchant_transaction_id')->nullable();
            $table->string('bank_transaction_id')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('auth_code')->nullable();
            $table->text('description')->nullable();
            $table->string('user_name')->nullable();
            $table->string('user_email')->nullable();
            $table->string('user_mobile_no')->nullable();
            $table->text('user_address')->nullable();
            $table->text('all_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_response');
    }
}
