<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsToLeadPackagesHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_packages_hotels', function (Blueprint $table) {
            $table->text('inclusions')->nullable();
            $table->text('exclusions')->nullable();
            $table->text('rooming_details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_packages_hotels', function (Blueprint $table) {
            //
        });
    }
}
