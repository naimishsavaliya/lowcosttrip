<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourCostAmountTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tour_cost_amount', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cost_id')->nullable();
            $table->integer('tour_id')->nullable();
            $table->integer('rate_id')->nullable();
            $table->integer('package_type_id')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('sell')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tour_cost_amount');
    }

}
