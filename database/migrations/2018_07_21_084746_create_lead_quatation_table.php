<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadQuatationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('lead_quatation', function (Blueprint $table) {
            $table->increments('quaId');
            $table->integer('lead_id')->default(0);
            $table->integer('tour_id')->nullable();
            $table->integer('status_id')->default(0);
            $table->dateTime('quatation_date')->nullable();
            $table->dateTime('validup_date')->nullable();
            $table->text('address')->nullable();
            $table->integer('country_id')->default(0);
            $table->integer('state_id')->default(0);
            $table->integer('city_id')->default(0);
            $table->string('pin_code', 255);
            $table->string('subject', 255);
            $table->integer('adult')->nullable();
            $table->integer('child')->nullable();
            $table->integer('infant')->nullable();
            $table->text('inclusion')->nullable();
            $table->text('exclusion')->nullable();
            $table->text('terms_condition')->nullable();
            $table->integer('total_amount')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('lead_quatation');
    }

}
