<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supId');
            $table->integer('sup_type')->default(0);
//            $table->string('first_name', 255);
//            $table->string('last_name', 255);
            $table->string('camapnay_name', 255);
            $table->string('contact_no', 30);
            $table->string('office_no', 30);
            $table->string('email', 100);
            $table->text('address');
            $table->string('gst_number', 255);
            $table->string('gst_name', 255);
            $table->text('gst_address');
            $table->integer('country_id')->default(0);
            $table->integer('state_id')->default(0);
            $table->integer('city_id')->default(0);
            $table->string('pin_code', 255);
            $table->string('login_email', 100);
            $table->string('password', 100);
            $table->string('register_mobile', 100);
            $table->string('notes', 255);
            $table->string('logo', 255);
            $table->integer('created_by')->nullable();
            $table->integer('verified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('suppliers');
    }

}
