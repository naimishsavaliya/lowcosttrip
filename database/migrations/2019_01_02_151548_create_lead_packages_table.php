<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id')->nullable();
            $table->string('guest_name', 255)->nullable();
            $table->string('no_of_guests', 255)->nullable();
            $table->integer('package_id')->nullable();
            $table->string('meal_plan', 255)->nullable();
            $table->string('book_through', 255)->nullable();
            $table->string('issued_by', 255)->nullable();
            $table->string('voucher_number', 255)->nullable();
            $table->dateTime('issue_date')->nullable();
            $table->string('em_name_1', 255)->nullable();
            $table->string('em_name_2', 255)->nullable();
            $table->string('em_name_3', 255)->nullable();
            $table->string('em_number_1', 255)->nullable();
            $table->string('em_number_2', 255)->nullable();
            $table->string('em_number_3', 255)->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_packages');
    }
}
