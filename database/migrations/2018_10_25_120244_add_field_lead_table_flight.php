<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldLeadTableFlight extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('lead', function (Blueprint $table) {
			$table->string('flight_type', 50)->nullable();
			$table->integer('from_city')->nullable();
			$table->integer('to_city')->nullable();
			$table->integer('flight_class')->nullable();
			$table->date('deprature_date')->nullable();
			$table->date('return_date')->nullable();
			$table->integer('customer_id')->nullable();
			$table->text('notes')->nullable();
			$table->integer('destination_city_id')->nullable();
			$table->integer('duration_days')->nullable();
			$table->integer('visa_type')->nullable();
			$table->integer('number_of_entries')->nullable();
			$table->integer('departure_city_id')->nullable();
			$table->integer('travel_type')->nullable();
			$table->integer('confirm_lead_id')->nullable();
			$table->date('check_in_date')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}

}
