<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name', 255)->after('id');
            $table->string('last_name', 255)->after('first_name');
            $table->tinyInteger('status')->default(0)->nullable()->after('remember_token');
            $table->integer('role_id')->default(0)->after('status');
            $table->integer('created_by')->nullable()->after('role_id');
            $table->integer('updated_by')->nullable()->after('created_by');
            $table->softDeletes();
            $table->dropColumn('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
        });
    }
}
