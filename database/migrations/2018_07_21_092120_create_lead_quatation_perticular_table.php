<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadQuatationPerticularTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('lead_quatation_perticular', function (Blueprint $table) {
            $table->increments('perId');
            $table->integer('lead_id')->default(0);
            $table->integer('quotation_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->string('item_type', 50)->nullable();
            $table->integer('tour_id')->nullable();
            $table->integer('item_no')->default(0);
            $table->string('item_title', 255)->nullable();
            $table->integer('qty')->default(0);
            $table->integer('cost')->default(0);
            $table->integer('discount')->default(0);
            $table->tinyInteger('status')->default(0)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('lead_quatation_perticular');
    }

}
