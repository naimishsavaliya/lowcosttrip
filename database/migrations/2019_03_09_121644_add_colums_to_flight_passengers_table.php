<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsToFlightPassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flight_passengers', function (Blueprint $table) {
            $table->string('buggage',255)->nullable();
            $table->float('buggage_price',10,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flight_passengers', function (Blueprint $table) {
            $table->dropColumn('buggage');
            $table->dropColumn('buggage_price');
            
        });
    }
}
