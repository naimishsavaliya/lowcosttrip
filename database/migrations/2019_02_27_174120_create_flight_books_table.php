<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('flight_from', 255)->nullable();
            $table->string('flight_to', 255)->nullable();
            $table->date('depart_date')->nullable();
            $table->string('passengers_class', 255)->nullable();
            $table->integer('adults')->nullable();
            $table->integer('child')->nullable();
            $table->integer('infants')->nullable();
            $table->string('email', 255)->nullable();
            $table->string('mobile', 255)->nullable();
            $table->float('base_fare', 10,2)->nullable();
            $table->float('udfcharge', 10,2)->nullable();
            $table->float('congestioncharge', 10,2)->nullable();
            $table->float('igstax', 10,2)->nullable();
            $table->float('tgsttax', 10,2)->nullable();
            $table->float('airporttax', 10,2)->nullable();
            $table->float('fuelSurcharge', 10,2)->nullable();
            $table->float('grand_total', 10,2)->nullable();
            $table->text('api_response')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_books');
    }
}
