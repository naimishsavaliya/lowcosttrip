<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightReviewInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_review_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flight_books_id')->nullable();
            $table->string('flight_name',255)->nullable();
            $table->string('flight_number',255)->nullable();
            $table->string('carrier_code',255)->nullable();

            $table->string('drrival_airport', 100)->nullable();
            $table->timestamp('departure_time')->nullable();

            $table->string('aeparture_airport', 100)->nullable();
            $table->timestamp('arrival_time')->nullable();

            $table->string('time_duration',255)->nullable();
            $table->string('flight_type',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_review_info');
    }
}
