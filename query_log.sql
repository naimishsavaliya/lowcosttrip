    -- Date: 20-07-2018 By: Kishan
ALTER TABLE `users_profile` ADD `subscription_id` INT(11) NULL DEFAULT NULL AFTER `user_id`;
ALTER TABLE `users_profile` ADD `title` VARCHAR(50) NULL DEFAULT NULL AFTER `subscription_id`;
ALTER TABLE `users_profile`  ADD `manager_id` INT NULL DEFAULT NULL  AFTER `user_id`;

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2018-05-10 00:32:35', '2018-05-10 00:32:35'),
(2, 'Manager', 'web', '2018-05-10 00:32:35', '2018-05-10 00:32:35'),
(3, 'Supplier', 'web', '2018-07-17 04:29:35', '2018-07-17 04:29:35'),
(4, 'Agent', 'web', '2018-07-17 07:01:38', '2018-07-17 07:01:38');

-- Date: 20-07-2018 By: Imran
ALTER TABLE `users_profile` ADD `sector_id` VARCHAR(255) NULL AFTER `image`;
ALTER TABLE `users_profile` CHANGE `sector_id` `sector_id` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Manager has permission to this sectors';
ALTER TABLE `tour_cost_amount` CHANGE `hotel_type_id` `package_type_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `tour_cost_amount` ADD `tour_id` INT(11) NULL AFTER `cost_id`;

-- Date: 24-07-2018 By: Kishan
ALTER TABLE `lead_quatation` CHANGE `quatation_date` `quatation_date` DATE NULL DEFAULT NULL;
ALTER TABLE `lead_quatation` CHANGE `validup_date` `validup_date` DATE NULL DEFAULT NULL;

-- Date: 25-07-2018 By: Kishan
ALTER TABLE `home_banners` CHANGE `image` `image_1` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `home_banners`  ADD `image_2` VARCHAR(255) NULL DEFAULT NULL  AFTER `image_1`,  ADD `image_3` VARCHAR(255) NULL DEFAULT NULL  AFTER `image_2`;

-- Date: 25-07-2018 By: Imran
ALTER TABLE `sector` ADD `created_by` INT(11) NULL AFTER `status`, ADD `updated_by` INT(11) NULL AFTER `created_by`;
ALTER TABLE `tour` CHANGE `maximam_tourist` `maximum_tourist` TINYINT(4) NULL DEFAULT NULL;
ALTER TABLE `tour` ADD `tour_step` TINYINT(4) NULL AFTER `tour_status`;

-- Date: 25-07-2018 By: Kishan
ALTER TABLE `lead_quatation_perticular`  ADD `quotation_id` INT(11) NULL DEFAULT NULL  AFTER `lead_id`;

-- Date: 26-07-2018 By: Kishan
ALTER TABLE `lead_quatation`  ADD `tour_id` INT NULL DEFAULT NULL  AFTER `quaId`;

-- Date: 27-07-2018 By: Kishan
ALTER TABLE `lead_quatation_perticular`  ADD `tour_id` INT NULL DEFAULT NULL  AFTER `quotation_id`;

-- Date: 28-07-2018 By: Kishan
ALTER TABLE `users_profile`  ADD `company_name` VARCHAR(255) NULL DEFAULT NULL  AFTER `sector_id`,  ADD `website` VARCHAR(255) NULL DEFAULT NULL  AFTER `company_name`,  ADD `skype_id` VARCHAR(255) NULL DEFAULT NULL  AFTER `website`,  ADD `designation` VARCHAR(255) NULL DEFAULT NULL  AFTER `skype_id`,  ADD `agent_type` INT(3) NULL DEFAULT NULL  AFTER `designation`,  ADD `how_old_agency` VARCHAR(10) NULL DEFAULT NULL  AFTER `agent_type`,  ADD `booking_per_month` INT(11) NULL DEFAULT NULL  AFTER `how_old_agency`,  ADD `no_of_employee` INT(11) NULL DEFAULT NULL  AFTER `booking_per_month`,  ADD `year_of_registration` INT(11) NULL DEFAULT NULL  AFTER `no_of_employee`,  ADD `compnay_type` INT(11) NULL DEFAULT NULL  AFTER `year_of_registration`,  ADD `where_hear_about` INT(11) NULL DEFAULT NULL  AFTER `compnay_type`,  ADD `use_any_travel_web` BOOLEAN NOT NULL  AFTER `where_hear_about`,  ADD `destination_ids` VARCHAR(255) NULL DEFAULT NULL  AFTER `use_any_travel_web`,  ADD `current_travellers_regions` INT(11) NULL DEFAULT NULL  AFTER `destination_ids`,  ADD `affiliations_id` INT(11) NULL DEFAULT NULL  AFTER `current_travellers_regions`;
ALTER TABLE `users_profile`  ADD `contact_name_1` VARCHAR(255) NULL DEFAULT NULL  AFTER `affiliations_id`,  ADD `contact_email_1` VARCHAR(255) NULL DEFAULT NULL  AFTER `contact_name_1`,  ADD `contact_phone_1` VARCHAR(50) NULL DEFAULT NULL  AFTER `contact_email_1`,  ADD `contact_name_2` VARCHAR(255) NULL DEFAULT NULL  AFTER `contact_phone_1`,  ADD `contact_email_2` VARCHAR(255) NULL DEFAULT NULL  AFTER `contact_name_2`,  ADD `contact_phone_2` VARCHAR(50) NULL DEFAULT NULL  AFTER `contact_email_2`,  ADD `contact_name_3` VARCHAR(255) NULL DEFAULT NULL  AFTER `contact_phone_2`,  ADD `contact_email_3` VARCHAR(255) NULL DEFAULT NULL  AFTER `contact_name_3`,  ADD `contact_phone_3` VARCHAR(50) NULL DEFAULT NULL  AFTER `contact_email_3`;
ALTER TABLE `users_profile`  ADD `products_services` VARCHAR(255) NULL DEFAULT NULL  AFTER `destination_ids`;

-- Date: 30-07-2018 By: Kishan
ALTER TABLE `lead_quatation_perticular` CHANGE `tour_id` `item_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `lead_quatation_perticular`  ADD `item_type` VARCHAR(50) NULL DEFAULT NULL  AFTER `item_id`;
ALTER TABLE `lead_quatation`  ADD `total_amount` INT(11) NULL DEFAULT NULL  AFTER `terms_condition`;

-- Date: 03-08-2018 By: Kishan
ALTER TABLE `tour`  ADD `image` VARCHAR(255) NULL DEFAULT NULL  AFTER `tour_step`;

-- Date: 08-08-2018 By: Kishan
ALTER TABLE `hotel`  ADD `status` TINYINT NULL DEFAULT '0'  AFTER `photos`;
ALTER TABLE `hotel`  ADD `temp_id` INT NULL DEFAULT NULL  AFTER `id`;
ALTER TABLE `hotel` CHANGE `mobile` `mobile` VARCHAR(50) NULL DEFAULT NULL;
