<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activitytype extends Model {

    protected $table = 'master_activity_type';

}
