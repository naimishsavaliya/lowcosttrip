<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model {

    protected $table = "lead";

    public function leadQuotation(){
    	return $this->hasMany(Leadquatation::class);
    }

    public function fromCity(){
    	return $this->belongsTo(City::class,'from_city','city_id');
    }

    public function toCity(){
    	return $this->belongsTo(City::class,'to_city','city_id');
    }

    public function City(){
    	return $this->belongsTo(City::class,'city_id','city_id');
    }

    public function State(){
    	return $this->belongsTo(State::class,'state_id','state_id');
    }

    public function Country(){
    	return $this->belongsTo(Country::class,'country_id','country_id');
    }

    public function destinationCity(){
    	return $this->belongsTo(City::class,'destination_city_id','city_id');
    }

    public function departureCity(){
    	return $this->belongsTo(City::class,'departure_city_id','city_id');
    }


}
