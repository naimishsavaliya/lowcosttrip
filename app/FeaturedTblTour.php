<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturedTblTour extends Model {

    protected $table = 'featured_tbl_tours';

}
