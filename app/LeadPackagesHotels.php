<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadPackagesHotels extends Model {
    protected $table = "lead_packages_hotels";
}
