<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturedDestinationTour extends Model {

    protected $table = 'featured_destination_tours';

}
