<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hoteltype extends Model {

    protected $table = 'master_hotel_type';

}
