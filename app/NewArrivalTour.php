<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewArrivalTour extends Model {

    protected $table = 'new_arrival_tours';

}
