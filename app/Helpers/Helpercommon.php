<?php

function get_flight_class($id = false) {
	$arr = [1 => "Business/First Class", 2 => "Economy Class"];
	if ($id) {
		return $arr[$id];
	}
	return $arr;
}
 
function get_air_class($id = false) {
	$arr = [ "A" => "Any","G" => 'Economy', "R" => 'Refundable Economy', "E" => 'Full Fare Economy', "W" => 'Premium Economy', "B" => 'Business', "F" => 'First'];
	if ($id) {
		return $arr[$id];
	}
	return $arr;
}

function get_visa_type($id = false) {
	$arr = [1 => "Tourism", 2 => "Vacation", 3 => "Pleasure visitor", 4 => "Business visa", 5 => "Residence Visa", 6 => "Journalist Visa", 7 => "Medical Visa", 8 => "Student Visa"];
	if ($id) {
		return $arr[$id];
	}
	return $arr;
}

function get_number_of_entries($id = false) {
	$arr = [1 => "Single Entry", 2 => "Double Entries", 3 => "Multiple Entries"];
	if ($id) {
		return $arr[$id];
	}
	return $arr;
}

function get_travel_type($id = false) {
	$arr = [1 => "Group Tours", 2 => "Speciality Tours", 3 => "FIT Tours"];
	if ($id) {
		return $arr[$id];
	}
	return $arr;
}

function get_accomodation($id = false) {
	$arr = ["1:0" => "1 Adult in 1 Room", "2:0" => "2 Adults in 1 Room", "3:0" => "3 Adults in 1 Room", "2:1" => "2 Adults + 1 Child"];
	if ($id) {
		return $arr[$id];
	}
	return $arr;
}

function get_gender($id = false) {
	$arr = ["M" => "Male", "F" => "Female"];
	if ($id) {
		return $arr[$id];
	}
	return $arr;
}

function get_child_age_group($id = false) {
	$arr = [1 => "Child (5 to 11 years) with bed", 2 => "Child (5 to 11 years) without bed", 3 => "Child (Infant)"];
	if ($id) {
		return $arr[$id];
	}
	return $arr;
}

function get_rate_id($id) {
	$arr = [1 => 2, 2 => 3, 3 => 4, 4 => 5, 5 => 6, 6 => 1, 7 => 9];
	if ($id) {
		return $arr[$id];
	}
	return 0;
}
function amount_format($amount) {
	return number_format($amount, 2, ".", "");
}

function number_to_word( $num = '', $cur_type = '')
{
    $num = str_replace(array(',', ' '), '' , trim($num));
    if(! $num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
    );
    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ( $tens < 20 ) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
        } else {
            $tens = (int)($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return   (($cur_type != '')? $cur_type : 'Rupees ' ).ucwords((implode(' ', $words))).' Only';
}
