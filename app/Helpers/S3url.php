<?php

namespace App\Helpers;

class S3url {
    
    /*
     * Return Full image with s3
     */
    public static function s3_get_image($image) {
        return \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->url($image);
    }

}
