<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookActivities extends Model
{
    protected $table = 'book_activities';
}
