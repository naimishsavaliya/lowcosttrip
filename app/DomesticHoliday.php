<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomesticHoliday extends Model {

    protected $table = 'domestic_holidays';

}
