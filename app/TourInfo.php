<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourInfo extends Model
{
    
    protected $table = 'incs_excs_tcs';
    
    //fillable fields
    // protected $fillable = ['name', 'description'];
    
    
    //custom timestamps name
    // const CREATED_AT = 'created_at';
    // const UPDATED_AT = 'updated_at';
}
