<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationPayment extends Model
{
    protected $table = 'quotation_payments';
}
