<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightBooks extends Model
{
    protected $table = 'flight_books';
}
