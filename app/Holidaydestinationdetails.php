<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holidaydestinationdetails extends Model {

    protected $table = "holiday_destination_details";

    public function destinationCity(){
    	return $this->belongsTo(City::class,'destination_city_id','city_id');
    }

}
