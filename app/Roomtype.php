<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roomtype extends Model {

    protected $table = 'master_room_type';

}
