<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends \Spatie\Permission\Models\Role {

    protected $table = 'roles';
    //fillable fields
    protected $fillable = ['name', 'guard_name'];

    //custom timestamps name
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

}
