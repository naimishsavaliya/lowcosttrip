<?php

namespace App\Http\Middleware;

use Closure;

class checkUserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        if(\Auth::user()){
            $role  = explode('|',$role);
            if(in_array(\Auth::user()->role->name, $role)){
                return $next($request);
            }
            else{
                if(\Auth::user()->role->name=="Customer")
                    return redirect()->route('user_dashboard');
                else
                    return redirect('admin');
            }
        }
        else{
            return redirect()->route('client_login_form');
        }
    }
}
