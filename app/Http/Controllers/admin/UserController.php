<?php

namespace App\Http\Controllers\admin;

use App\User;
use App\Role;
use App\Permission;
use App\Authorizable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ControllerAbstract;
use Session;
use DB;
use Validator;
use Hash;

class UserController extends ControllerAbstract {

    use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Admin User'
            )
        );
        return view('admin/user/user', $data);
    }

    /*
     * Get Certificates data in JSON
     */

    public function data(Request $request) {
        return datatables()->of(User::select('users.id', DB::raw("CONCAT(users.first_name,' ',users.last_name) as full_name"), 'users.email', 'roles.name as role_name', 'users.status', 'users.created_at')
                                ->where('deleted_at', null)
                                ->where('users.role_id', '!=', '5')
                                ->leftjoin('roles', 'roles.id', '=', 'users.role_id')
                        )
                        // ->editColumn('role_id', function ($model) {
                            // if ($model->role_id == 1) {
                            //     return "Admin";
                            // } else if ($model->role_id == 2) {
                            //     return "Manager";
                            // }
                        // })
                        ->editColumn('users.created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->filterColumn('full_name', function($query, $keyword) {
                            $query->whereRaw("CONCAT(users.first_name,' ',users.last_name) like ?", ["%{$keyword}%"]);
                        })
                        ->filterColumn('role_name', function($query, $keyword) {
                            $query->whereRaw("roles.name like ?", ["%{$keyword}%"]);
                        })
                        // ->filterColumn('role_id', function ($query, $keyword) use ($request) {
                            // if (isset($request['search']) && $request['search']['value'] != '') {
                            //     $keyword = $request['search']['value'];
                            // }
                            // if(isset($keyword) && $keyword!=''){
                            //     if($keyword=='Admin'){
                            //         $query->where("role_id",1);
                            //     }else if ($keyword=='Manager') {
                            //        $query->where("role_id",2);
                            //     }
                            // }
                        //     if (isset($request['search']) && $request['search']['value'] != '') {
                        //         $keyword = $request['search']['value'];
                        //     }
                        //     $roleid_in = $this->getCommonLib()->getusermoduleroleinid($keyword);
                        //     if(count($roleid_in) >0){
                        //         $query->where("role_id",implode(',',$roleid_in), 'in',false );
                        //     }
                        // })
                        ->addColumn('action', '')
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['timezones'] = $this->getCommonLib()->getTimezoneData();
        $data['sectors'] = $this->getSectorLib()->getSector(array('id', 'name'), array('status' => 1));
        $data['roles'] = $this->getRoleLib()->getRole(array('id', 'name'));

        $data['breadcrumb'] = array(
            array(
                'link' => route('user.index'),
                'title' => 'Manage Admin User'
            ),
            array(
                'title' => 'Add User'
            )
        );
        return view('admin/user/user-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $userId = "";
        $userData = $request->all();
        if (!empty($userData['id'])) {
            $userId = $userData['id'];
        }

        //validate user data
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required',
            'landline_no' => 'required',
            'address' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'time_zone' => 'required',
            'pin_code' => 'required',
            'role_id' => 'required',
            'status' => 'required',
        ];
        if (empty($userData['id'])) {
            $rules['email'] = 'required|email|unique:users';
            $rules['password'] = "required|min:6";
            $rules['confirm_password'] = "required|same:password|min:6";
        }
        $messages = [
            'phone_no.required' => 'The phone number field is required.',
            'landline_no.required' => 'The landline number field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
//                return redirect('/admin/student/edit/' . $request->id)->withErrors($validator)->withInput();
                return redirect()->route('user.edit', $userId)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('user.add')->withErrors($validator)->withInput();
//                return redirect('/admin/student/add')->withErrors($validator)->withInput();
            }
        } else {
            //get User Data
            $data['first_name'] = $userData['first_name'];
            $data['last_name'] = $userData['last_name'];
            if (empty($userId)) {
                $data['email'] = $userData['email'];
                $data['password'] = Hash::make($userData['password']);
            }
            $data['role_id'] = $userData['role_id'];
            $data['status'] = $userData['status'];

            $profileData = array(
                'address' => $userData['address'],
                'country_id' => $userData['country_id'],
                'state_id' => $userData['state_id'],
                'city_id' => $userData['city_id'],
                'pin_code' => $userData['pin_code'],
                'time_zone' => $userData['time_zone'],
                'phone_no' => $userData['phone_no'],
                'landline_no' => $userData['landline_no'],
                'gender' => $userData['gender'],
                'age' => $userData['age'],
                'information' => $userData['information'],
            );
            $sectors_ids = isset($request->sector_id) ? $request->sector_id : array();
            if (count($sectors_ids) > 0) {
                $profileData['sector_id'] = implode(',', $sectors_ids);
            }
            if ($request->role_id == 1) {
                $profileData['sector_id'] = '';
            }

            if (!empty($userData['birth_date'])) {
                $profileData['birth_date'] = date('Y-m-d', strtotime($userData['birth_date']));
            }


            if ($userData['id'] > 0) {
                $this->getUserLib()->updateUser($data, $userData['id']);
                $this->getUserprofileLib()->updateUserprofile($profileData, $userData['id']);
                Session::flash('success_msg', 'User has been updated successfully!');
            } else {
                $userId = $this->getUserLib()->addUser($data);
                $profileData['user_id'] = $userId;
                $this->getUserprofileLib()->addUserprofile($profileData);
                Session::flash('success_msg', 'User has been saved successfully!');
            }
            $user = $this->getUserLib()->getUserById($userId);
            $this->syncPermissions($request, $user);

            //Updoad image and update in db
            $image = $request->file('image');
            if (!empty($image)) {
                $imagename = $userId . '_user.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/user_profile');
                $image->move($destinationPath, $imagename);
                $profilepicData['image'] = $imagename;
                $this->getUserprofileLib()->updateUserprofile($profilepicData, $userId);
            }
        }

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['users'] = $this->getUserLib()->getUserWithProfile($id, array('users.*', 'users_profile.*', 'users.id as user_id'));
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['states'] = $this->getStateLib()->getStateByCountry($data['users'][0]->country_id, array('state_id', 'state_name'));
        $data['cities'] = $this->getCityLib()->getCityByState($data['users'][0]->state_id, array('city_id', 'city_name'));
        $data['roles'] = $this->getRoleLib()->getRole(array('id', 'name'));
        $data['timezones'] = $this->getCommonLib()->getTimezoneData();
        $data['sectors'] = $this->getSectorLib()->getSector(array('id', 'name'), array('status' => 1));
        $data['breadcrumb'] = array(
            array(
                'link' => route('user.index'),
                'title' => 'Manage Admin User'
            ),
            array(
                'title' => 'Edit User'
            )
        );

        return view('admin/user/user-manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->getUserLib()->deleteUser($id);
        if ($deleted) {
            Session::flash('success_msg', 'User has been deleted successfully!');
        } else {
            Session::flash('error_msg', "User can't deleted!");
        }
        return redirect()->route('user.index');
    }

    public function profile(User $user) {
        $data = array();
        return view('admin/user-profile', $data);
    }

    public function role() {
        $data = array();
        return view('admin/user-role', $data);
    }

    public function role_add() {
        $data = array();
        return view('admin/user-role', $data);
    }

    public function role_edit(User $user) {
        $data = array();
        return view('admin/user-role-manage', $data);
    }

    public function role_delete(User $user) {
        $data = array("deleted" => true);
        return view('admin/user-role', $data);
    }

    public function role_permission(User $user) {
        $data = array();
        return view('admin/user-role-permission', $data);
    }

    public function message() {
        $data = array();
        return view('admin/user-message', $data);
    }

    public function compose() {
        $data = array();
        return view('admin/user-compose', $data);
    }

    public function payment_due() {
        $data = array();
        return view('admin/user-payment-due', $data);
    }

    public function login(Request $request) {
        $response = array();
        $userdata = array(
            'email' => $request->email,
            'password' => $request->password
        );

        // attempt to do the login
        if ($res = Auth::attempt($userdata)) {
//            print_r($_SESSION);
            sleep(5);
            $response["success"] = true;
            $response["msg"] = "Login";
        } else {
            $response["success"] = false;
            $response["msg"] = "Invalid Login!";
        }
        echo json_encode($response);
    }

    public function set_permission(Request $request, $id) {
        $user = User::findOrFail($id);
        $this->syncPermissions($request, $user);
    }

    private function syncPermissions(Request $request, $user) {
// Get the submitted roles
        $roles = $request->role_id;
//	$permissions = $request->get('permissions', []);
// Get the roles
        $roles = Role::find($roles);

// check for current role changes
        if (!$user->hasAllRoles($roles)) {
// reset all direct permissions for user
//	$user->permissions()->sync([]);
        } else {
// handle permissions
//	$user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }

    public function changerole($id) {
        $data['user_id'] = $id;
        $data['roles'] = $this->getRoleLib()->getRole(array('id', 'name'));
        $data['users'] = $this->getUserLib()->getUserById($id, array('role_id'));

        $data['breadcrumb'] = array(
            array(
                'link' => route('user.index'),
                'title' => 'Manage Admin User'
            ),
            array(
                'title' => 'Edit User Role'
            )
        );
        return view('admin/user/change-role', $data);
    }

    public function role_save(Request $request) {
        //validate user data
        $rules = [
            'role_id' => 'required',
        ];

        $messages = [
            'role_id.required' => 'Please select user role.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('user.change.role', $request->id)->withErrors($validator)->withInput();
        } else {
            //set User Data
            $data['role_id'] = $request->role_id;

            if ($request->role_id > 0) {
                //update user role
                $this->getUserLib()->updateUser($data, $request->id);
                Session::flash('success_msg', 'User role has been updated successfully!');
            }
        }

        return redirect()->route('user.index');
    }

}
