<?php

namespace App\Http\Controllers\admin;

use App\Agent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use App\User;
use App\Role;
use App\Permission;
use App\Authorizable;
use DB;
use Session;
use Validator;
use Hash;
use File;

class AgentController extends ControllerAbstract {

    use Authorizable;

    private $agent_role = 4;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['subscription_pkg'] = $this->getSubscriptionpackageLib()->getSubscriptionpackage(array('packId', 'name'));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Travel Agents'
            )
        );
        return view('admin/agent/agent', $data);
    }

    public function agent_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\User::select('users.id', DB::raw("CONCAT(users.first_name,' ',users.last_name) as full_name"), 'users.email', 'users.role_id', 'users.status', 'users.created_at', 'users_profile.phone_no', 'city.city_name', 'subscription_package.name')
                                    ->leftjoin('users_profile', 'users.id', '=', 'users_profile.user_id')
                                    ->leftjoin('city', 'users_profile.city_id', '=', 'city.city_id')
                                    ->leftjoin('subscription_package', 'users_profile.subscription_id', '=', 'subscription_package.packId')
                                    ->where('users.deleted_at', null)
                                    ->where('users.role_id', 4)
                            )
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->filterColumn('full_name', function($query, $keyword) {
                                $query->whereRaw("CONCAT(first_name,' ',last_name) like ?", ["%{$keyword}%"]);
                            })
                            ->addColumn('note', 'Follow Up Note')
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['timezones'] = $this->getCommonLib()->getTimezoneData();
        $data['subscription_pkg'] = $this->getSubscriptionpackageLib()->getSubscriptionpackage(array('packId', 'name'));
        $data['managers'] = $this->getUserLib()->getUsersByRole(2, array('id', 'first_name', 'last_name'));

        $data['agent_types'] = $this->getCommonLib()->getAgentType();
        $data['booking_per_months'] = $this->getCommonLib()->getBookingPerMonth();
        $data['no_of_employees'] = $this->getCommonLib()->getTotalEmployees();
        $data['year_of_registation'] = $this->getCommonLib()->getYearOfRegistation();
        $data['company_types'] = $this->getCommonLib()->getCompanyType();
        $data['hear_about_us'] = $this->getCommonLib()->getHearAboutUS();
        $data['destinations'] = $this->getDestinationsLib()->getDestination(array('desId', 'name'));
        $data['affiliations'] = $this->getCommonLib()->getAffiliationsType();
        $data['product_services'] = $this->getCommonLib()->getProductServices();

        $data['breadcrumb'] = array(
            array(
                'link' => route('agent.index'),
                'title' => 'Manage Travel Agents'
            ),
            array(
                'title' => 'Add Travel Agent'
            )
        );
        return view('admin/agent/agent-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate user data
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => "required|email|unique:users,email,$request->id",
            'phone_no' => 'required',
            'landline_no' => 'required',
            'address' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'time_zone' => 'required',
            'pin_code' => 'required',
            'subscription_id' => 'required',
            'manger_id' => 'required',
            'status' => 'required',
        ];
        if (!isset($request->id)) {
//            $rules['email'] = 'required|email|unique:users';
            $rules['password'] = "required|min:6";
            $rules['confirm_password'] = "required|same:password|min:6";
        }
        $messages = [
            'phone_no.required' => 'The phone number field is required',
            'landline_no.required' => 'The landline number field is required',
            'subscription_id.required' => 'Please select subscription package',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('agent.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('agent.add')->withErrors($validator)->withInput();
            }
        } else {
            //get User Data
            $data['first_name'] = $request->first_name;
            $data['last_name'] = $request->last_name;
            if (!isset($request->id)) {
                $data['email'] = $request->email;
                $data['password'] = Hash::make($request->password);
            }
            $data['role_id'] = $this->agent_role;
            $data['status'] = $request->status;

            $profileData = array(
                'title' => $request->title,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'state_id' => $request->state_id,
                'city_id' => $request->city_id,
                'pin_code' => $request->pin_code,
                'time_zone' => $request->time_zone,
                'phone_no' => $request->phone_no,
                'landline_no' => $request->landline_no,
                'gender' => $request->gender,
                'age' => $request->age,
                'subscription_id' => $request->subscription_id,
                'manager_id' => $request->manger_id,
                'information' => $request->information,
                'company_name' => $request->company_name,
                'website' => $request->website,
                'skype_id' => $request->skype_id,
                'designation' => $request->designation,
                'agent_type' => $request->agent_type,
                'how_old_agency' => $request->how_old_agency,
                'booking_per_month' => $request->booking_per_month,
                'no_of_employee' => $request->no_of_employee,
                'year_of_registration' => $request->year_of_registration,
                'compnay_type' => $request->compnay_type,
                'where_hear_about' => $request->where_hear_about,
                'use_any_travel_web' => $request->use_any_travel_web,
                'current_travellers_regions' => $request->current_travellers_regions,
                'affiliations_id' => $request->affiliations_id,
                'contact_name_1' => $request->contact_name_1,
                'contact_name_2' => $request->contact_name_2,
                'contact_name_3' => $request->contact_name_3,
                'contact_email_1' => $request->contact_email_1,
                'contact_email_2' => $request->contact_email_2,
                'contact_email_3' => $request->contact_email_3,
                'contact_phone_1' => $request->contact_phone_1,
                'contact_phone_2' => $request->contact_phone_2,
                'contact_phone_3' => $request->contact_phone_3,
            );
            if (isset($request->destination_ids) && count($request->destination_ids) > 0) {
                $profileData['destination_ids'] = implode(',', $request->destination_ids);
            }
            if (isset($request->products_services) && count($request->products_services) > 0) {
                $profileData['products_services'] = implode(',', $request->products_services);
            }

            if ($request->id > 0) {
                $userId = $request->id;
                $this->getUserLib()->updateUser($data, $request->id);
                $this->getUserprofileLib()->updateUserprofile($profileData, $request->id);
                Session::flash('success_msg', 'User has been updated successfully!');
            } else {
                $userId = $this->getUserLib()->addUser($data);
                $profileData['user_id'] = $userId;
                $this->getUserprofileLib()->addUserprofile($profileData);
                Session::flash('success_msg', 'User has been saved successfully!');
            }
            $user = $this->getUserLib()->getUserById($userId);
            $this->syncPermissions($request, $user);

            //Updoad image and update in db
            $image = $request->file('image');
            if (!empty($image)) {
                $imagename = $userId . '_user.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/user_profile');
                $image->move($destinationPath, $imagename);
                $profilepicData['image'] = $imagename;
                $this->getUserprofileLib()->updateUserprofile($profilepicData, $userId);
            }

            //upload Agent Documents
            $document_path = public_path() . '/uploads/documents/' . $userId;
            if (!File::exists($document_path)) {
                File::makeDirectory($document_path, $mode = 0777, true, true);
            }
            for ($doc_counter = 1; $doc_counter <= 6; $doc_counter++) {
                $doc_ids = $request->document_hidden[$doc_counter - 1];
                $document = $request->file('document_' . $doc_counter);
                if (!empty($document)) {
                    $doc_arr = array();
                    $doc_orignal_name = $document->getClientOriginalName();
                    $doc_name = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . '.' . $document->getClientOriginalExtension();
                    $document->move($document_path, $doc_name);
                    $doc_arr['doc_file_name'] = $doc_name;
                    if (count($doc_arr)) {
                        if ($doc_ids > 0) {
                            $old_docs = $this->getAgentdocumentLib()->getAgentdocumentById($doc_ids);
                            if (count($old_docs) > 0) {
                                if (File::exists($document_path . '/' . $old_docs->doc_file_name)) {
                                    File::delete($document_path . '/' . $old_docs->doc_file_name);
                                }
                            }
                            $saveDocuments = $this->getAgentdocumentLib()->updateAgentdocument($doc_arr, $doc_ids);
                        } else {
                            $doc_arr['agent_id'] = $userId;
                            $saveDocuments = $this->getAgentdocumentLib()->addAgentdocument($doc_arr);
                        }
                    }
                }
            }
        }
        return redirect()->route('agent.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent) {
        $data = array();
        return view('admin/agent/agent-view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['agent_id'] = $id;
        $data['users'] = $this->getUserLib()->getUserWithProfile($id);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['states'] = $this->getStateLib()->getStateByCountry($data['users'][0]->country_id, array('state_id', 'state_name'));
        $data['cities'] = $this->getCityLib()->getCityByState($data['users'][0]->state_id, array('city_id', 'city_name'));
        $data['timezones'] = $this->getCommonLib()->getTimezoneData();
        $data['subscription_pkg'] = $this->getSubscriptionpackageLib()->getSubscriptionpackage(array('packId', 'name'));
        $data['managers'] = $this->getUserLib()->getUsersByRole(2, array('id', 'first_name', 'last_name'));
        $data['documents'] = $this->getAgentdocumentLib()->getAgentdocument($id);

        $data['agent_types'] = $this->getCommonLib()->getAgentType();
        $data['booking_per_months'] = $this->getCommonLib()->getBookingPerMonth();
        $data['no_of_employees'] = $this->getCommonLib()->getTotalEmployees();
        $data['year_of_registation'] = $this->getCommonLib()->getYearOfRegistation();
        $data['company_types'] = $this->getCommonLib()->getCompanyType();
        $data['hear_about_us'] = $this->getCommonLib()->getHearAboutUS();
        $data['destinations'] = $this->getDestinationsLib()->getDestination(array('desId', 'name'));
        $data['affiliations'] = $this->getCommonLib()->getAffiliationsType();
        $data['product_services'] = $this->getCommonLib()->getProductServices();
        $data['breadcrumb'] = array(
            array(
                'link' => route('agent.index'),
                'title' => 'Manage Travel Agents'
            ),
            array(
                'title' => 'Edit Travel Agent'
            )
        );
        return view('admin/agent/agent-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->getUserLib()->deleteUser($id);
        if ($deleted) {
            Session::flash('success_msg', 'Travel Agent has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Travel Agent package can't deleted!");
        }
        return redirect()->route('agent.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function subscription(Agent $agent) {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Subscription Package'
            )
        );
        return view('admin/agent/agent-subscription', $data);
    }

    /*
     * Subscription package data in Json
     */

    public function subscription_data(Request $request) {
        return datatables()->of(\App\Subscriptionpackage::select('packId', 'name', 'amount', 'status', 'created_at')
                                ->where('deleted_at', null)
                        )
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->addColumn('action', '')
                        ->addColumn('no_of_leads', '2')
                        ->addColumn('no_of_subscription', '3')
                        ->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function subscription_add(Agent $agent) {
        $data['breadcrumb'] = array(
            array(
                'link' => route('agent.subscription'),
                'title' => 'Manage Subscription Package'
            ),
            array(
                'title' => 'Add Subscription Package'
            )
        );
        return view('admin/agent/agent-subscription-add', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function subscription_edit($id) {
        $data['package'] = $this->getSubscriptionpackageLib()->getSubscriptionpackageById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('agent.subscription'),
                'title' => 'Manage Subscription Package'
            ),
            array(
                'title' => 'Edit Subscription Package'
            )
        );
        return view('admin/agent/agent-subscription-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function subscription_store(Request $request) {
        //validate user data
        $rules = [
            'name' => 'required',
            'amount' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter package name',
            'amount.required' => 'Please enter package amount',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('agent.subscription.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('agent.subscription.add')->withErrors($validator)->withInput();
            }
        } else {
            //get Suscription Data
            $data['name'] = $request->name;
            $data['amount'] = $request->amount;
            $data['status'] = $request->status;

            if ($request->id > 0) {
                $this->getSubscriptionpackageLib()->updateSubscriptionpackage($data, $request->id);
                Session::flash('success_msg', 'Subscription package has been updated successfully!');
            } else {
                $this->getSubscriptionpackageLib()->addSubscriptionpackage($data);
                Session::flash('success_msg', 'Subscription package has been saved successfully!');
            }
        }
        return redirect()->route('agent.subscription');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function subscription_destroy($id) {
        $deleted = $this->getSubscriptionpackageLib()->deleteSubscriptionpackage($id);
        if ($deleted) {
            Session::flash('success_msg', 'Subscription package has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Subscription package can't deleted!");
        }
        return redirect()->route('agent.subscription');
    }

    /*
     * Agents Note list
     */

    public function notes($agentid) {
        $data = array();
        return view('admin/agent/agent_notes', $data);
    }

    /*
     * Agent Note Add
     */

    public function notes_add($agentid) {
        $data = array();
        return view('admin/agent/note_create', $data);
    }

    /*
     * Agent Note 
     * editAdd
     */

    public function notes_edit($agentid, $id) {
        $data = array();
        return view('admin/agent/note_create', $data);
    }

    /*
     * Agent Note 
     * editAdd
     */

    public function notes_delete($agentid, $id) {
        $data = array();
        return view('admin/agent/agent_notes', $data);
    }

    /*
     * Agent Dcoument Add
     */

    public function document_add($agentid) {
        $data = array();
        return view('admin/agent/manage-document', $data);
    }

    /*
     * Agent Dcoument Add
     */

    public function document_edit($agentid, $id) {
        $data = array();
        return view('admin/agent/manage-document', $data);
    }

    public function set_permission(Request $request, $id) {
        $user = User::findOrFail($id);
        $this->syncPermissions($request, $user);
    }

    private function syncPermissions(Request $request, $user) {
// Get the submitted roles
        $roles = $this->agent_role;
//	$permissions = $request->get('permissions', []);
// Get the roles
        $roles = Role::find($roles);

// check for current role changes
        if (!$user->hasAllRoles($roles)) {
// reset all direct permissions for user
//	$user->permissions()->sync([]);
        } else {
// handle permissions
//	$user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }

}
