<?php

namespace App\Http\Controllers\admin;

use App\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Validator;
use Session;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;


class CronController extends ControllerAbstract {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function cron_followup() {
		$datetime = date("Y-m-d H:i");
		$lead_notes = $this->getLeadnotesLib()->get_followup_data_for_cron($datetime);
		//dd(count($lead_notes));
		if(count($lead_notes) > 0){
			foreach ($lead_notes as $key => $value) {
				if(isset($value['lead_id']) && $value['lead_id']!=''){
					$cron_data=$this->getLeadLib()->get_lead_data_for_cron($value['lead_id']);
					if($cron_data){
						$lead_id  	=  isset($cron_data->lead_id)?$cron_data->lead_id:'';
						$lead_title  	=  isset($cron_data->lead_title)?$cron_data->lead_title:'';
						$email  	=  isset($cron_data->email)?$cron_data->email:'';
						$full_name  	=  isset($cron_data->full_name)?$cron_data->full_name:'';
						$reminder_note  	=  isset($value['description'])?$value['description']:'';
						
						if($email!=''){
							$data = array('username'=>$full_name,'lead_title'=>$lead_title,'email' => $email,'lead_id'=>$lead_id,'reminder_datetime'=>date("Y-m-d H:i:s"),'reminder_note'=>$reminder_note);
							
							Mail::send('followup-template', ['username'=>$data['username'],'lead_title'=>$lead_title,'email'=>$data['email'],'lead_id'=>$data['lead_id'],'reminder_datetime'=>$data['reminder_datetime'],'reminder_note'=>$data['reminder_note']], function($message) use ($data) {
							   $message->to($data['email'], 'Low Cost Trip')->subject
							      ('Follow up reminder for lead #'.$data['lead_id']);
							   $message->from('info@lowcosttrip.com','Low Cost Trip');
							});
						}
					}
				}
			}
			if (Mail::failures()) {
				echo 'Could not send email, please try again later.';
		    }else{
		    	echo 'Send email.';
		    }
		}else{
			echo 'No record found';
		}
		exit();
	}

	// public function cron_followup() {
	// 	$datetime = date("Y-m-d H:i");
	// 	$lead_id=[];
	// 	$lead_notes = $this->getLeadnotesLib()->get_followup_data_for_cron($datetime);
	// 	if(count($lead_notes) > 0){
	// 		foreach ($lead_notes as $key => $value) {
	// 			$lead_id[] = $value['lead_id'];
	// 		}
	// 		if(!empty($lead_id)){
	// 			$cron_data=$this->getLeadLib()->get_lead_data_for_cron($lead_id);
	// 			if(count($cron_data) > 0){
	// 				foreach ($cron_data as $key1 => $row) {
	// 					$name  	=  isset($row['name'])?$row['name']:'';
	// 					$email  	=  isset($row['email'])?$row['email']:'';
	// 					$lead_no  	=  isset($row['lead_id'])?$row['lead_id']:'';
	// 					if($email!=''){
	// 						$data = array('name'=>$name,'email' => $email,'lead_no'=>$lead_no,'reminder_datetime'=>date("Y-m-d H:i:s"));
	// 						dd($data);
	// 						// Mail::send([], $data, function($message)  use ($data )  {
	// 						//      $message->to($data['email'], 'Low Cost Trip')->subject
	// 						//         ('Follow up reminder');
	// 						//      $message->from('info@lowcosttrip.com','Low Cost Trip');
	// 						// });

	// 						// Mail::send('followup-template', ['email'=>$data['email'],'lead_no'=>$data['lead_no']], function($message) use ($data) {
	// 						//    $message->to($data['email'], 'Low Cost Trip')->subject
	// 						//       ('Follow up reminder for lead');
	// 						//    $message->from('info@lowcosttrip.com','Low Cost Trip');
	// 						// });
	// 					}
	// 				}
	// 				if (Mail::failures()) {
	// 					echo 'Could not send email, please try again later.';
	// 			    }else{
	// 			    	echo 'Send email.';
	// 			    }
	// 			}else{
	// 				echo 'No record found';
	// 			}
	// 		}else{
	// 			echo 'No record found';
	// 		}
	// 	}else{
	// 		echo 'No record found';
	// 	}
	// 	exit();
	// }

	// public function cron_followup() {
	// 	$datetime = date("Y-m-d H:i");
	// 	$datetime = "2019-01-01 12:09:00";
	// 	$lead_id=[];
	// 	//$email=[];
	// 	$lead_notes = $this->getLeadnotesLib()->get_followup_data_for_cron($datetime);
	// 	if(count($lead_notes) > 0){
	// 		foreach ($lead_notes as $key => $value) {
	// 			$lead_id[] = $value['lead_id'];
	// 		}
	// 		if(!empty($lead_id)){
	// 			$cron_data=$this->getLeadLib()->get_lead_data_for_cron($lead_id);
	// 			if(count($cron_data) > 0){
	// 				// foreach ($cron_data as $key1 => $row) {
	// 				// 	$email[]  =  isset($row['email'])?$row['email']:'';
	// 				// }

	// 				foreach ($cron_data as $key1 => $row) {
	// 					$email  =  isset($row['email'])?$row['email']:'';
	// 					if($email!=''){
	// 						// Mail::send('client/register-template', ['email'=>$data['email'],'first_name'=>$data['first_name'],'last_name'=>$data['last_name'],'activation_token'=>$activation_token], function($message) use ($data) {
	// 						//    $message->to($data['email'], 'Low Cost Trip')->subject
	// 						//       ('Thank you for registering low cost trip');
	// 						//    $message->from('info@lowcosttrip.com','Low Cost Trip');
	// 						// });

	// 						$data = array('email' => $email);
	// 						Mail::send([], $data, function($message)  use ($data )  {
	// 						     $message->to($data['email'], 'Low Cost Trip')->subject
	// 						        ('Follow-up');
	// 						     $message->from('info@lowcosttrip.com','Low Cost Trip');
	// 						});
	// 					}
	// 				}
	// 				if (Mail::failures()) {
	// 					echo 'Could not send email, please try again later.';
	// 			    }else{
	// 			    	echo 'Send email.';
	// 			    }
	// 			}else{
	// 				echo 'No record found';
	// 			}
	// 		}else{
	// 			echo 'No record found';
	// 		}
	// 	}else{
	// 		echo 'No record found';
	// 	}
	// 	exit();
	// }

}



