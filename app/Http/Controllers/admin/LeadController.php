<?php

namespace App\Http\Controllers\admin;

use App\Lead;
use App\CustomerPaymentlead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ControllerAbstract;
use App\Leadstatus;
use Validator;
use Session;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;


class LeadController extends ControllerAbstract {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//dd('Hello');
		$data['intrested_ins'] = $this->getCommonLib()->getLeadIntrestedin();
		$data['lead_sources'] = $this->getCommonLib()->getLeadSources();
		$data['lead_places_type'] = $this->getCommonLib()->getLeadIsPlaceType();
		$data['group_customised'] = $this->getCommonLib()->getLeadCustomizedOpt();
		$data['lead_status'] = $this->getLeadstatusmasterLib()->getLeadstatusmaster(array('typeId', 'name'));
		$data['admin_user'] = $this->getUserLib()->getAdminUser();
		$data['breadcrumb'] = array(
			array(
				'title' => 'Manage Leads'
			)
		);
		return view('admin/lead/lead', $data);
	}

	/*
	 * Return Lead data in JSON
	 */

	public function data(Request $request) {
		if ($request->ajax()) {
			$input = $request->all();
			if ($keyword = $input['search']['value']) {
			}

			$query = \App\Lead::select('lead.id', 
					'lead.lead_id',
					'lead.created_at',
					'lead.name', 
					'lead.email', 
					'lead.phone', 
					'lead.interested_in', 
					'lead.domestic_international',
					'lead.lead_source', 
					'lead.assign_to', 
					'lead.confirm_by',
					'master_lead_status.name as status_name', 'lead.status_id',
					\DB::raw("GROUP_CONCAT(CONCAT_WS(' ', users.first_name, users.last_name)) as assign_to_user"))
					->leftjoin('master_lead_status', 'lead.status_id', '=', 'master_lead_status.typeId')
					->leftJoin('users', function($join) {
						$join->whereRaw(\DB::raw("find_in_set(users.id, lead.assign_to)"));
					})
					->where('lead.deleted_at', null)
					->groupBy('lead.id');

			// if (\Auth::user()->role_id == 3 || \Auth::user()->role_id == 4) {
			// 	$query->where('lead.created_by', \Auth::user()->id);
			// }
			if (\Auth::user()->role_id != 1) {
				$query->where('lead.assign_to', \Auth::user()->id);
			}
			

			return datatables()->of($query)
				->editColumn('interested_in', function ($model) {
					if (isset($model->interested_in)) {
						return $this->getCommonLib()->getLeadIntrestedin($model->interested_in);
					} else {
						return '';
					}
				})
				->editColumn('domestic_international', function ($model) {
					if (isset($model->domestic_international)) {
						return $this->getCommonLib()->getLocationTourType($model->domestic_international);
					} else {
						return '';
					}
				})
				->editColumn('lead_source', function ($model) {
					if (isset($model->lead_source)) {
						return $this->getCommonLib()->getLeadSources($model->lead_source);
					} else {
						return '';
					}
				})
				->editColumn('created_at', function ($model) {
					return date("d M Y", strtotime($model->created_at));
				})

				->filterColumn('assign_to_user', function($query, $keyword) {
                    $query->whereRaw("CONCAT(users.first_name,' ',users.last_name) like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('user_info', function($query, $keyword) {
                    $query->whereRaw("CONCAT(lead.name, '\n', lead.email, '\n', lead.phone) like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('status_name', function($query, $keyword) {
                    $query->whereRaw("master_lead_status.name like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(lead.created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                })
                ->filterColumn('interested_in', function ($query, $keyword) use ($request) {
                    if (isset($request['search']) && $request['search']['value'] != '') {
                        $keyword = $request['search']['value'];
                    }
                    $interested_in = $this->getCommonLib()->getLeadIntrestedinid($keyword);
                    if(count($interested_in) >0){
                        $query->where("interested_in",implode(',',$interested_in), 'in',false );
                    }
                })
                ->filterColumn('lead_source', function ($query, $keyword) use ($request) {
                    if (isset($request['search']) && $request['search']['value'] != '') {
                        $keyword = $request['search']['value'];
                    }
                    $lead_source = $this->getCommonLib()->getLeadSources($keyword, 1);
                    if(count($lead_source) >0){
                        $query->where("lead_source",implode(',',$lead_source), 'in',false );
                    }
                })

				->addColumn('action', '')
				->toJson();

		}
	}

	public function assign_to_user(Request $request)	{
        $response['success'] = false;
        if ($request->ajax()) {
            try {
                $table = 'lead';
                $id = $request->id;
                $assign_user_id = $request->assign_user_id;
                $column_name = $request->column_name;
                
                $update_columns = "assign_to";
                if(isset($request->column_update)){
                    $update_columns = $request->column_update;
                }
                
                //update query fire
                DB::table($table)
                        ->where($column_name, $id)
                        ->update([$update_columns => $assign_user_id]);
                $response['success'] = true;
                $response['msg_status'] = 'success';
                $response['message'] = "Assign to has been changed successfully";
            } catch (Exception $e) {
                $response['success'] = false;
                $response['msg_status'] = 'warning';
                $response['message'] = "Something went wrong, please try again";
            }
        }
        exit(json_encode($response));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$data['customer_type'] = $this->getCustomertypemasterLib()->getCustomertypemaster(array('typeId', 'name'));
		$data['intrested_ins'] = $this->getCommonLib()->getLeadIntrestedin();
		$data['lead_sources'] = $this->getCommonLib()->getLeadSources();
		$data['lead_places_type'] = $this->getCommonLib()->getLeadIsPlaceType();
		$data['group_customised'] = $this->getCommonLib()->getLeadCustomizedOpt();
		$data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
		$cities_arr = array();
		$cities_for_holidays = $this->getCityLib()->getCity(['city_id', 'city_name']);
		foreach ($cities_for_holidays as $key => $value) {
		    $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
		}
		$data['cities_for_holidays'] = json_encode($cities_arr);
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'title' => 'Add Lead'
			)
		);
		return view('admin/lead/lead-manage', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$rules = [
			'intrested_in' => 'required',
			'lead_name' => 'required',
			'email_id' => 'required',
			'phone_no' => 'required',
		];
		$messages = [
			'intrested_in.required' => 'Please select interested in',
			'lead_name.required' => 'Please enter lead name',
			'email_id.required' => 'Please enter email address',
			'phone_no.required' => 'Please enter phone number',
		];

		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()) {
			if (isset($request->id)) {
				return redirect()->route('lead.edit', $request->id)->withErrors($validator)->withInput();
			} else {
				return redirect()->route('lead.add')->withErrors($validator)->withInput();
			}
		} else {
			$data['customer_type_id'] = $request->customer_type;
			$data['interested_in'] = $request->intrested_in;
			$data['lead_source'] = $request->lead_source;
			if (!empty($request->intrested_date)) {
				$data['interested_date'] = date('Y-m-d', strtotime($request->intrested_date));
			}
			$data['name'] = $request->lead_name;
			$data['domestic_international'] = $request->domestic_international;
			$data['email'] = $request->email_id;
			$data['phone'] = $request->phone_no;
			$data['adult'] = $request->no_adults;
			$data['child'] = $request->no_child;
			$data['infant'] = $request->no_infants;
			$data['country_id'] = $request->country_id;
			$data['state_id'] = $request->state_id;
			$data['city_id'] = $request->city_id;
			$data['pincode'] = $request->pin_code;
			$data['notes'] = $request->notes;
			$data['status_id'] = 1;

			//Flight & visa Interested In Type Start

			if(isset($request->from_city) && isset($request->from_city_id)){
				$data['from_city'] = $request->from_city_id;
			}

			if(isset($request->to_city) && isset($request->to_city_id)){
				$data['to_city'] = $request->to_city_id;
			}

			//Flight & visa Interested In Type End
			
			//Flight Interested In Type Start

			if(isset($request->flight_class)){
				$data['flight_class'] = $request->flight_class;
			}

			if(isset($request->flight_type)){
				$data['flight_type'] = $request->flight_type;
			}

			//Flight Interested In Type End

			//Flight & hotel Interested In Type Start

			if(isset($request->deprature_date)){
				$data['deprature_date'] = date('Y-m-d', strtotime($request->deprature_date));
			}
			if(isset($request->return_date)){
				$data['return_date'] = date('Y-m-d', strtotime($request->return_date));
			}

			//Flight & hotel Interested In Type End

			//Hotel Interested In Type Start

			if(isset($request->destination_city) && isset($request->destination_city_id) ){
				$data['destination_city_id'] = $request->destination_city_id;
			}

			if(isset($request->number_of_nights)){
				$data['number_of_nights'] = $request->number_of_nights;
			}

			//Hotel Interested In Type End

			//visa Interested In Type Start

			if(isset($request->visa_type)){
				$data['visa_type'] = $request->visa_type;
			}

			if(isset($request->number_of_entries)){
				$data['number_of_entries'] = $request->number_of_entries;
			}

			if(isset($request->travel_date)){
				$data['deprature_date'] =  date('Y-m-d', strtotime($request->travel_date));
			}

			if(isset($request->duration_days)){
				$data['duration_days'] = $request->duration_days;
			}			

			//visa Interested In Type End

			//Holiday Interested In Type start
			
			if(isset($request->departure_city) && isset($request->departure_city_id) ){
				$data['departure_city_id'] = $request->departure_city_id;
			}
			
			if(isset($request->check_in_date)){
				$data['check_in_date'] = date('Y-m-d', strtotime($request->check_in_date));
			}

			if(isset($request->travel_type)){
				$data['travel_type'] = $request->travel_type;
			}

			//Holiday Interested In Type End

			if (isset($request->agent_id) && count($request->agent_id) > 0) {
				$data['assign_to'] = implode(',', $request->agent_id);
			}

			//$data['group_customised'] = $request->group_customised;
			if ($request->id > 0) {
				//$this->getLeadLib()->updateLead($data, $request->id);
				$lead_id = $this->getLeadLib()->updateLead($data,$request->id);
				if(isset($request->intrested_in) && $request->intrested_in==7){
					$this->getHolidaydestinationdetailsLib()->deleteHolidaydestinationdetails_lead(['lead_id'=>$request->id]);
					if(!empty($request->destination) && count($request->destination) > 0) {
						foreach ($request->destination as $key => $value) {
							if(isset($value['city_id'])){
								$destinationData = array(
									"lead_id" => isset($request->id)?$request->id:'',
									"destination_city_id" => isset($value['city_id'])?$value['city_id']:'',
									"number_of_nights" => isset($value['number_of_nights'])?$value['number_of_nights']:''
								);
								$this->getHolidaydestinationdetailsLib()->addHolidaydestinationdetails($destinationData);
							}
						}
					}

					$this->getHolidayactivitiesLib()->deleteHolidayactivities_lead(['lead_id'=>$request->id]);
					if(!empty($request->activiti_details) && count($request->activiti_details) > 0) {
						for ($i=0; $i < count($request->activiti_details["'city'"]) ; $i++) { 
							if($request->activiti_details["'city'"][$i] != '' &&  $request->activiti_details["'activiti'"][$i] != ''){
								$data_activity = array(
									"lead_id"    => isset($request->id)?$request->id:'',
									"country_id"  => isset($request->activiti_details["'city'"][$i])?$request->activiti_details["'city'"][$i]:'',
									"activiti_id" => isset($request->activiti_details["'activiti'"][$i])?$request->activiti_details["'activiti'"][$i]:''
								);
								$this->getHolidayactivitiesLib()->addHolidayactivities($data_activity);
							}
						}
					}
				}else{
					$this->getHolidaydestinationdetailsLib()->deleteHolidaydestinationdetails_lead(['lead_id'=>$request->id]);
					$this->getHolidayactivitiesLib()->deleteHolidayactivities_lead(['lead_id'=>$request->id]);
				}
				
				Session::flash('success_msg', 'Lead has been updated successfully!');
			} else {
				$user = $this->getUserLib()->getUserByEmailOrPhone($request->email_id, $request->phone_no);
				if (!empty($user->id)) {
					$customer_id = $user->id;
				} else {
					$name_arr = explode(" ", $request->lead_name);
					$last_name = "";
					foreach ($name_arr as $key => $value) {
						if ($key == 0) {
							$first_name = $value;
						} else {
							$last_name .= " " . $value;
						}
					}
					$userData = array(
						"first_name" => $first_name,
						"last_name" => trim($last_name, " "),
						"email"     => $request->email_id,
						"password"  => Hash::make($request->phone_no),
						"role_id" => 5,
						'status'  => 1,
						'is_new_password' => 1,
						'forgot_token' => str_random(10),
					);
					$customer_id = $this->getUserLib()->addUser($userData);
					$profileData = array();
					$profileData['user_id'] = $customer_id;
					$profileData['phone_no'] = $request->phone_no;
					$this->getUserprofileLib()->addUserprofile($profileData);
					$this->send_mail_to_user($customer_id);
				}
				
				$data['customer_id'] = $customer_id;

				$leadId = $this->getLeadLib()->addLead($data);
				if(isset($request->intrested_in) && $request->intrested_in==7){
					if(!empty($request->destination) && count($request->destination) > 0) {
						foreach ($request->destination as $key => $value) {
							// $destinationData = array(
							// 	"lead_id" => $leadId,
							// 	"destination_city_id" => $value['city_id'],
							// 	"number_of_nights" => $value['number_of_nights'],
							// );
							// $this->getHolidaydestinationdetailsLib()->addHolidaydestinationdetails($destinationData);

							if(isset($value['city_id'])){
								$destinationData = array(
									"lead_id" => $leadId,
									"destination_city_id" => isset($value['city_id'])?$value['city_id']:'',
									"number_of_nights" => isset($value['number_of_nights'])?$value['number_of_nights']:''
								);
								$this->getHolidaydestinationdetailsLib()->addHolidaydestinationdetails($destinationData);
							}
						}
					}

					if(!empty($request->activiti_details) && count($request->activiti_details) > 0) {
						for ($i=0; $i < count($request->activiti_details["'city'"]) ; $i++) { 
							if($request->activiti_details["'city'"][$i] != '' &&  $request->activiti_details["'activiti'"][$i] != ''){
								$data_activity = array(
									"lead_id"    => $leadId,
									"country_id"  => isset($request->activiti_details["'city'"][$i])?$request->activiti_details["'city'"][$i]:'',
									"activiti_id" => isset($request->activiti_details["'activiti'"][$i])?$request->activiti_details["'activiti'"][$i]:''
								);
								$this->getHolidayactivitiesLib()->addHolidayactivities($data_activity);
							}
						}
					}
				}
//                $updatelead['lead_id'] = "LCT" . $leadId;
//                $this->getLeadLib()->updateLead($updatelead, $leadId);
				Session::flash('success_msg', 'Lead has been saved successfully!');
			}
		}
		return redirect()->route('lead.index');
	}

	public function lead_detail($id) {
		$data['quotation_status'] = $this->getCommonLib()->getQuotationStatus();
		$data['lead'] = $this->getLeadLib()->getLeadHeaderInfoById($id);

		$data['type_of_payment'] = $this->getCommonLib()->type_of_payment();
		$data['paymentType'] = $this->getCommonLib()->paymentType();
		$data['paymentMode'] = $this->getCommonLib()->paymentMode();
		
		$data['customer_type'] = $this->getCustomertypemasterLib()->getCustomertypemaster(array('typeId', 'name'));
		$data['intrested_ins'] = $this->getCommonLib()->getLeadIntrestedin();
		$data['lead_sources'] = $this->getCommonLib()->getLeadSources();
		$data['lead_places_type'] = $this->getCommonLib()->getLeadIsPlaceType();
		$data['group_customised'] = $this->getCommonLib()->getLeadCustomizedOpt();
		$data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
		$data['states'] = $this->getStateLib()->getStateByCountry($data['lead']->country_id, array('state_id', 'state_name'));
		$data['cities'] = $this->getCityLib()->getCityByState($data['lead']->state_id, array('city_id', 'city_name'));

		$cities_arr = array();
		$cities_for_holidays = $this->getCityLib()->getCity(['city_id', 'city_name']);
		foreach ($cities_for_holidays as $key => $value) {
		    $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
		}
		$data['cities_for_holidays'] = json_encode($cities_arr);

		$lead_info = $this->getLeadLib()->getLeadById($id, array('name'));
		$data['from_city'] = $this->getCityLib()->getCityById($data['lead']->from_city);
		$data['to_city'] = $this->getCityLib()->getCityById($data['lead']->to_city);
		$data['destination_city'] = $this->getCityLib()->getCityById($data['lead']->destination_city_id);
		$data['departure_city'] = $this->getCityLib()->getCityById($data['lead']->departure_city_id);
		$data['holiday_destination_details'] = $this->getHolidaydestinationdetailsLib()->getHolidaydestinationdetails(array('holiday_destination_details.*'),['lead_id'=>$id]);
		$data['holiday_activity_details'] = $this->getHolidayactivitiesLib()->getHolidayactivities(array('holiday_activities.*'),['lead_id'=>$id]);
		if (\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2) {
			$data['manager_users'] = $this->getUserLib()->getUsersByRole(4, array('id', 'first_name', 'last_name'));
		}
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'title' => isset($lead_info->name) ? $lead_info->name.' (Lead #'.$data['lead']->lead_id.') in '.$data['lead']->status.' Status' : 'Manage Lead'
			),
		);
		return view('admin/lead/lead-detail', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Lead  $lead
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Lead $lead) {
		$data = array();
		return view('admin/lead/lead-manage', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Lead  $lead
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Lead $lead) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Lead  $lead
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$response['success'] = false;
		$deleted = $this->getLeadLib()->deleteLead($id);
		if ($deleted) {
			$response['success'] = true;
			$response['message'] = 'Lead note has been deleted successfully!';
			$response['msg_status'] = 'success';
		} else {
			$response['message'] = "Lead note can't deleted successfully!";
			$response['msg_status'] = 'warning';
		}
		exit(json_encode($response));
	}

	public function cancelled() {
		$data = array();
		return view('admin/lead/lead', $data);
	}

	public function confirmed() {
		$data = array();
		return view('admin/lead/lead-confirmed', $data);
	}

	public function detail() {
		$data = array();
		return view('admin/lead-detail', $data);
	}

	public function notes_list() {
		$data = array();
		return view('admin/lead/lead_notes', $data);
	}

	// public function notes_data($lead_id, Request $request) {
	// 	if ($request->ajax()) {
	// 		return datatables()->of(\App\Leadnotes::select('lead_notes.notId', 'lead_notes.lead_id', 'lead_notes.title', 'lead_notes.description', 'lead_notes.followup_date', 'lead_notes.status', 'lead_notes.created_at', \DB::raw("CONCAT(users.first_name,' ',users.last_name) as full_name"))
	// 								->leftjoin('users', 'lead_notes.created_by', '=', 'users.id')
	// 								->where('lead_notes.deleted_at', null)
	// 								->where('lead_notes.lead_id', $lead_id))
	// 						->editColumn('followup_date', function ($model) {
	// 							return date("d M Y h:i A", strtotime($model->followup_date));
	// 						})
	// 						->editColumn('created_at', function ($model) {
	// 							return date("d M Y", strtotime($model->created_at));
	// 						})
	// 						->editColumn('description', function ($model) {
	// 							return mb_strimwidth($model->description, 0, 100, "...");
	// 						})
	// 						->filterColumn('created_at', function($query, $keyword) {
	// 							$search_date = date('Y-m-d', strtotime($keyword));
	// 							$query->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
	// 						})
	// 						->filterColumn('followup_date', function($query, $keyword) {
	// 							$search_date = date('Y-m-d', strtotime($keyword));
	// 							$query->whereRaw("DATE_FORMAT(followup_date, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
	// 						})
	// 						->addColumn('action', '')
	// 						->rawColumns(['status', 'action'])
	// 						->toJson();
	// 	}
	// }

	public function notes_data($lead_id, Request $request) {
		if ($request->ajax()) {
			return datatables()->of(\App\Leadnotes::select('lead_notes.notId', 
				'lead_notes.lead_id', 
				'lead_notes.title', 
				'lead_notes.description',
				'lead_notes.followup_date', 
				'lead_notes.status', 
				'lead_notes.created_at', 
				\DB::raw("CONCAT(users.first_name,' ',users.last_name) as full_name"))
				->leftjoin('users', 'lead_notes.created_by', '=', 'users.id')
				->where('lead_notes.deleted_at', null)
				->where('lead_notes.lead_id', $lead_id))
				->editColumn('followup_date', function ($model) {
					return date("d M Y h:i A", strtotime($model->followup_date));
				})
				->editColumn('created_at', function ($model) {
					return date("d M Y", strtotime($model->created_at));
				})

				->editColumn('description', function ($model) {
					return mb_strimwidth($model->description, 0, 100, "...");
				})
				->filterColumn('full_name', function($query, $keyword) {
				    $query->whereRaw("CONCAT(users.first_name,' ',users.last_name) like ?", ["%{$keyword}%"]);
				})
				->filterColumn('created_at', function ($query, $keyword) {
				    $query->whereRaw("DATE_FORMAT(lead_notes.created_at,'%d %b %Y') like ?", ["%$keyword%"]);
				})
				->filterColumn('followup_date', function($query, $keyword) {
					$query->whereRaw("DATE_FORMAT(lead_notes.followup_date,'%d %b %Y %h:%i %p') like ?", ["%$keyword%"]);
				})
				->addColumn('action', '')
				->rawColumns(['status', 'action'])
				->toJson();
		}
	}

	public function notes_add($lead_id) {
            $lead_info = $this->getLeadLib()->getLeadById($lead_id);
            $data['breadcrumb'] = array(
		array(
                    'link' => route('lead.index'),
                    'title' => 'Manage Leads'
		),
		array(
                    'link' => route('lead.lead_detail', $lead_id),
                    'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
		),
		array(
                    'title' => 'Add Follow Up Notes'
		)
            );
            return view('admin/lead/note_create', $data);
	}

	public function notes_edit($lead_id, $id) {
		$data['lead_note'] = $this->getLeadnotesLib()->getLeadnoteById($id);
		$lead_info = $this->getLeadLib()->getLeadById($lead_id);
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' => route('lead.lead_detail', $lead_id . '#followup'),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
			),
			array(
				'title' => 'Edit Follow Up Notes'
			)
		);
		return view('admin/lead/note_create', $data);
	}

	public function notes_store(Request $request) {
		//validate certificate data
		$rules = [
			'title' => 'required',
			'description' => 'required',
			'status' => 'required',
		];

		$messages = [
			'title.required' => 'Please enter note title',
			'description.required' => 'Please enter description',
			'status.required' => 'Please select status',
		];

		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()) {
			if (isset($request->id)) {
				return redirect()->route('lead.note.edit', $request->id)->withErrors($validator)->withInput();
			} else {
				return redirect()->route('lead.note.add', $request->lead_id)->withErrors($validator)->withInput();
			}
		} else {
			$data['lead_id'] = $request->lead_id;
			$data['title'] = $request->title;
			$data['description'] = $request->description;
			$data['status'] = $request->status;
			if (!empty($request->followup_date)) {
				$data['followup_date'] = date('Y-m-d H:i:s', strtotime($request->followup_date));
			}

			if (isset($request->id)) {
				$this->getLeadnotesLib()->updateLeadnote($data, $request->id);
				Session::flash('success_msg', 'Lead note has been updated successfully!');
			} else {
				$this->getLeadnotesLib()->addLeadnote($data);
				Session::flash('success_msg', 'Lead note has been saved successfully!');
			}
		}
		return redirect()->route('lead.lead_detail', $request->lead_id . '#followup');
	}

	public function notes_destroy($lead_id, $id) {
		$deleted = $this->getLeadnotesLib()->deleteLeadnote($id);
		if ($deleted) {
			Session::flash('success_msg', 'Lead note has been deleted successfully!');
		} else {
			Session::flash('error_msg', "Lead note can't deleted successfully!");
		}
		return redirect()->route('lead.lead_detail', $lead_id . '#followup');
	}

	public function status_list() {
		$data['breadcrumb'] = array(
			array(
				'title' => 'Manage Lead Status'
			)
		);
		return view('admin/lead/status_list', $data);
	}

	/*
	 * Lead status data in JSON
	 */

	public function status_data(Request $request) {
		if ($request->ajax()) {
			return datatables()->of(Leadstatus::select('typeId', 'name', 'description', 'status')
									->where('deleted_at', null)
							)
							->editColumn('description', function ($model) {
								return mb_strimwidth($model->description, 0, 250, "...");
							})
							//->rawColumns(['status', 'action'])
							->addColumn('action', '')
							->toJson();
		}
	}

	public function status_add() {
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.status.list'),
				'title' => 'Manage Lead Status'
			),
			array(
				'title' => 'Add Lead Status'
			)
		);
		return view('admin/lead/status_create', $data);
	}

	public function status_edit($id) {
		$data['lead_status'] = $this->getLeadstatusmasterLib()->getLeadstatusmasterById($id);
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.status.list'),
				'title' => 'Manage Lead Status'
			),
			array(
				'title' => 'Edit Lead Status'
			)
		);
		return view('admin/lead/status_create', $data);
	}

	/*
	 * Lead status store
	 */

	public function status_store(Request $request) {
		$rules = [
			'name' => 'required',
			'status' => 'required',
		];

		$messages = [
			'name.required' => 'Please enter lead status',
			'status.required' => 'Please select status',
		];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails()) {
			if (isset($request->id)) {
				return redirect()->route('lead.status.edit', $request->id)->withErrors($validator)->withInput();
			} else {
				return redirect()->route('lead.status.add')->withErrors($validator)->withInput();
			}
		} else {
			$leadstatusData = array();
			$leadstatusData['name'] = $request->name;
			$leadstatusData['description'] = $request->description;
			$leadstatusData['status'] = $request->status;
			if (isset($request->id)) {
				$id = $request->id;
				$this->getLeadstatusmasterLib()->updateLeadstatusmaster($leadstatusData, $id);
				Session::flash('success_msg', 'Lead status has been updated successfully!');
			} else {
				$this->getLeadstatusmasterLib()->addLeadstatusmaster($leadstatusData);
				Session::flash('success_msg', 'Lead status  has been saved successfully!');
			}
			return redirect(route('lead.status.list'));
		}
	}

	public function status_destroy($id) {
		$deleted = $this->getLeadstatusmasterLib()->deleteLeadstatusmaster($id);
		if ($deleted) {
			Session::flash('success_msg', 'Lead status has been deleted successfully!');
		} else {
			Session::flash('error_msg', "Lead status can't deleted!");
		}
		return redirect(route('lead.status.list'));
	}

	public function payment() {
		$data = array();
		return view('admin/lead/payment', $data);
	}

	public function payment_manage() {
		$data = array();
		return view('admin/lead/payment-manage', $data);
	}


        
	/*
	 * Lead Document Data
	 */

	public function documents_data($lead_id, Request $request) {
//        echo $lead_id;die;
		if ($request->ajax()) {
			return datatables()->of(\App\Leaddocuments::select('docId', 'lead_id', 'name', 'document', 'description', 'status', 'created_at')
									->where('deleted_at', null)
									->where('lead_id', $lead_id))
							->editColumn('created_at', function ($model) {
								return date("d M Y", strtotime($model->created_at));
							})
							->editColumn('description', function ($model) {
								return mb_strimwidth($model->description, 0, 250, "...");
							})
							->filterColumn('created_at', function($query, $keyword) {
								$search_date = date('Y-m-d', strtotime($keyword));
								$query->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
							})
//                            ->editColumn('document', function ($model) use($lead_id) {
//                                return url('/public/uploads/lead_document/' . $lead_id) . '/' . $model->document;
//                            })
							->addColumn('action', '')
							->rawColumns(['document'])
							->toJson();
		}
	}

	public function documents_add($lead_id) {
		$lead_info = $this->getLeadLib()->getLeadById($lead_id);
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' => route('lead.lead_detail', $lead_id . '#documents'),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
			),
			array(
				'title' => 'Add Document'
			)
		);
		return view('additional_services', $data);
	}

	public function documents_edit($lead_id, $id) {
		$lead_info = $this->getLeadLib()->getLeadById($lead_id, array('name'));
		$data['document'] = $this->getLeadDocumentLib()->getLeadDocumentById($id);
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' => route('lead.lead_detail', $lead_id . '#documents'),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
			),
			array(
				'title' => 'Edit Document'
			)
		);
		return view('admin/lead/document-manage', $data);
	}

	public function documents_store(Request $request) {
		//validate certificate data
		$rules = [
			'name' => 'required',
			'status' => 'required',
		];

		$messages = [
			'name.required' => 'Please enter document name',
			'status.required' => 'Please select status',
		];

		if (isset($request->id) == false) {
			$rules['document'] = 'required';
			$messages['document.required'] = 'Please select document';
		}

		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()) {
			if (isset($request->id)) {
				return redirect()->route('lead.document.edit', array($request->lead_id, $request->id))->withErrors($validator)->withInput();
			} else {
				return redirect()->route('lead.document.add', $request->lead_id)->withErrors($validator)->withInput();
			}
		} else {
			$data['lead_id'] = $request->lead_id;
			$data['name'] = $request->name;
			$data['description'] = $request->description;
			$data['status'] = $request->status;

			//upload Agent Documents
			$document_path = public_path() . '/uploads/lead_document/' . $request->lead_id;
			if (\File::exists($document_path) == false) {
				\File::makeDirectory($document_path, $mode = 0777, true, true);
			}
			//Upload destination image
			$document = $request->file('document');
			if (!empty($document)) {
				$document_name = time() . '.' . $document->getClientOriginalExtension();
				$document->move($document_path, $document_name);
				$data['document'] = $document_name;
			}

			if (isset($request->id)) {
				$this->getLeadDocumentLib()->updateLeadDocument($data, $request->id);
				Session::flash('success_msg', 'Lead document has been updated successfully!');
			} else {
				$this->getLeadDocumentLib()->addLeadDocument($data);
				Session::flash('success_msg', 'Lead document has been saved successfully!');
			}
		}
		return redirect()->route('lead.lead_detail', $request->lead_id . '#documents');
	}

	/*
	 * Document Delte
	 */

	public function documents_delete($id) {
		$response['success'] = false;
		$deleted = $this->getLeadDocumentLib()->deleteLeadDocument($id);
		if ($deleted) {
			$response['success'] = true;
			$response['message'] = 'Lead document has been deleted successfully!';
			$response['msg_status'] = 'success';
		} else {
			$response['message'] = "Lead document can't deleted successfully!";
			$response['msg_status'] = 'warning';
		}
		exit(json_encode($response));
	}


	/*
	* Voucher Information 
	*/

	public function voucher_data($lead_id, Request $request) {
		if ($request->ajax()) {	
			$input = $request->all();
			if ($keyword = $input['search']['value']) {
			}

			$query = \App\LeadPackages::select('lead_packages.id', 
					'lead_packages.guest_name',
					'lead_packages.voucher_number',
					'master_tour_package_type.name', 
					'lead_packages.no_of_guests')
					->leftjoin('master_tour_package_type', 'lead_packages.package_id', '=', 'master_tour_package_type.typeId')
					->where('lead_packages.deleted_at', null)
					->groupBy('lead_packages.id');
			return datatables()->of($query)
				// ->editColumn('created_at', function ($model) {
				// 	return date("d M Y", strtotime($model->created_at));
				// })
			 	->filterColumn('name', function($query, $keyword) {
                    $query->whereRaw("master_tour_package_type.name like ?", ["%{$keyword}%"]);
                })
				->addColumn('action', function($voucher_data) {
    				$action = '';
					$action .= '<a href="'.route('lead.voucher.edit',[$voucher_data->id]).'" title="Edit" class=""><i class="md-icon material-icons">&#xE254;</i></a>';

					$action .= '<a href="' .route('lead.voucher.delete',[$voucher_data->id]). '" title="Delete" onclick="return confirm(\'Are you sure to delete?\');" ><i class="md-icon material-icons" data-type="plus">&#xe872;</i></a>';

					$action .= '<a href="'.route('lead.voucher.pdf',[$voucher_data->id]).'" target="_blank" title="Print" class=""><i class="md-icon material-icons">&#xe555;</i></a>';
    				return $action;
	            })
				->toJson();
		}
	}

	public function generate_pdf($id='') {
		$data['lead_package'] = $lead_package = $this->getLeadPackagesLib()->getLeadPackagesById($id);
		$data['lead_package_meta'] = $this->getLeadPackagesHotelsLib()->getLeadPackagesHotels('*', array('lead_package_id' => $lead_package->id));
		$data['lead'] = $this->getLeadLib()->getLeadById($lead_package->lead_id);
		if($lead_package->quotation_id > 0){
        	$data['itinarary'] = $this->getLeadItinararyLib()->getLeadItinararyById($lead_package->lead_id, $lead_package->quotation_id);
        }
		$contents = view('admin/lead/voucher-pdf', $data)->render();
		// echo $contents;die;
		$pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($contents );
        return $pdf->download($lead_package->voucher_number.'.pdf');
        // return $pdf->stream('invoice.pdf');
	}


	public function voucher_add($lead_id) {
		$data['lead'] = $lead_info = $this->getLeadLib()->getLeadById($lead_id);
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' => route('lead.lead_detail', $lead_id . '#voucher'),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
			),
			array(
				'title' => 'Add Voucher'
			)
		);
		$data['package_type'] = $this->getTourpackagetypemasterLib()->getTourpackagetypemaster();
		return view('admin/lead/manage-voucher', $data);
	}

	public function voucher_edit($id) {
		$data['lead_package'] = $lead_package = $this->getLeadPackagesLib()->getLeadPackagesById($id);
		$data['lead_package_meta'] = $this->getLeadPackagesHotelsLib()->getLeadPackagesHotels('*', array('lead_package_id' => $lead_package->id));
		$data['lead'] = $lead_info = $this->getLeadLib()->getLeadById($lead_package->lead_id);
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' => route('lead.lead_detail', $lead_info->id . '#voucher'),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
			),
			array(
				'title' => 'Add Voucher'
			)
		);
		$data['package_type'] = $this->getTourpackagetypemasterLib()->getTourpackagetypemaster();
		$data['quote_info']   = $this->getLeadQuatationLib()->getLeadQuatationRecordById($lead_package->quotation_id, ['quatation_id']);
		return view('admin/lead/manage-voucher', $data);
	}

	public function voucher_store(Request $request) {
		//validate certificate data
	 
		$rules = [
			'guest_name' => 'required',
			'package_id' => 'required',
			'no_of_guests' => 'required',
			'voucher_number' => 'required',
			'issue_date' => 'required',
		];

		$messages = [
			'guest_name.required' => 'Please enter guest name',
			'package_id.required' => 'Please select package type',
			'no_of_guests.required' => 'Please enter no of guests',
			'voucher_number.required' => 'Please enter voucher number',
			'issue_date.required' => 'Please enter issue date',
		];

		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()) {
			if (isset($request->id)) {
				return redirect()->route('lead.voucher.add', $request->id)->withErrors($validator)->withInput();
			} else {
				return redirect()->route('lead.voucher.add', $request->lead_id)->withErrors($validator)->withInput();
			}
		} else {
			$data['lead_id'] = $request->lead_id;
			$data['guest_name'] = $request->guest_name;
			$data['no_of_guests'] = $request->no_of_guests;
			$data['package_id'] = $request->package_id;
			$data['meal_plan'] = $request->meal_plan;
			$data['book_through'] = $request->book_through;
			$data['issued_by'] = $request->issued_by;
			$data['voucher_number'] = $request->voucher_number;
			$data['issue_date'] = date('Y-m-d H:i:s', strtotime($request->issue_date));
			$data['em_name_1'] = $request->em_name_1;
			$data['em_name_2'] = $request->em_name_2;
			$data['em_name_3'] = $request->em_name_3;
			$data['em_number_1'] = $request->em_number_1;
			$data['em_number_2'] = $request->em_number_2;
			$data['em_number_3'] = $request->em_number_3;
			$data['note'] = $request->note;
			$quote_data = $this->getLeadQuatationLib()->getQuotationByNumber($request->quotation_id,['quaId']);
			$data['quotation_id'] = (( isset($quote_data)  && isset($quote_data->quaId)) ? $quote_data->quaId : 0); 
			// date('Y-m-d H:i:s', strtotime($request->followup_date));
			if (isset($request->id)) {
				$this->getLeadPackagesLib()->update($data, $request->id);
				$this->getLeadPackagesHotelsLib()->delete_by_package_id( $request->id );
				for($i=0; $i < count($request->hotel_name); $i++){
                    $hote_data = array(
                        'lead_package_id'  => $request->id,
                        'lead_id'	       => $request->lead_id,
                        'hotel_name'       => $request->hotel_name[$i],
                        'location'         => $request->location[$i],
                        'check_in'         => date('Y-m-d H:i:s', strtotime($request->check_in[$i])),
                        'check_out'        => date('Y-m-d H:i:s', strtotime($request->check_out[$i])),
                        'number_of_nights' => $request->number_of_nights[$i],
                        'rooming_details'  => $request->rooming_details[$i],
                        'inclusions'       => $request->inclusions[$i],
                        'exclusions'       => $request->exclusions[$i],
                    );
					$id = $this->getLeadPackagesHotelsLib()->add($hote_data);
                }
				Session::flash('success_msg', 'Voucher has been updated successfully!');
			} else {
				$id = $this->getLeadPackagesLib()->add($data);
				for($i=0; $i < count($request->hotel_name); $i++){
                    $hote_data = array(
                        'lead_package_id'  => $id,
                        'lead_id'	       => $request->lead_id,
                        'hotel_name'       => $request->hotel_name[$i],
                        'location'         => $request->location[$i],
                        'check_in'         => date('Y-m-d H:i:s', strtotime($request->check_in[$i])),
                        'check_out'        => date('Y-m-d H:i:s', strtotime($request->check_out[$i])),
                        'number_of_nights' => $request->number_of_nights[$i],
                        'rooming_details'  => $request->rooming_details[$i],
                        'inclusions'       => $request->inclusions[$i],
                        'exclusions'       => $request->exclusions[$i],
                    );
					$id = $this->getLeadPackagesHotelsLib()->add($hote_data);
                }
				Session::flash('success_msg', 'Voucher has been saved successfully!');
			}
		}
		return redirect()->route('lead.lead_detail', $request->lead_id . '#voucher');
	}

	function voucher_delete($id)
	{
		$data['lead_package'] = $lead_package = $this->getLeadPackagesLib()->getLeadPackagesById($id);
        $deleted = $this->getLeadPackagesLib()->delete( $id );
        $deleted = 1;
        if ($deleted) {
            $this->getLeadPackagesHotelsLib()->delete_by_package_id( $id );
			Session::flash('success_msg', 'Voucher has been deleted successfully!');
        } else {
			Session::flash('error_msg', "Voucher can't deleted successfully!");
        }

        return redirect()->route('lead.lead_detail', $lead_package->lead_id . '#voucher');
	}


	/*
	 * Get Lead detail by LeadNo
	 */

	public function lead_detials($lead_no, Request $request) {
		$response['success'] = false;
		if ($request->ajax()) {
			$lead_detail = $this->getLeadLib()->getLeadByNuber($lead_no, array('id', 'adult', 'child', 'infant', 'country_id', 'state_id', 'city_id', 'pincode'));
			if (!empty($lead_detail)) {
				$response['success'] = true;
				$response['data']['adult'] = isset($lead_detail->adult) ? $lead_detail->adult : 0;
				$response['data']['child'] = isset($lead_detail->child) ? $lead_detail->child : 0;
				$response['data']['infant'] = isset($lead_detail->infant) ? $lead_detail->infant : 0;
				$response['data']['country_id'] = isset($lead_detail->country_id) ? $lead_detail->country_id : 0;
				$response['data']['state_id'] = isset($lead_detail->state_id) ? $lead_detail->state_id : 0;
				$response['data']['city_id'] = isset($lead_detail->city_id) ? $lead_detail->city_id : 0;
				$response['data']['pincode'] = isset($lead_detail->pincode) ? $lead_detail->pincode : '';
			}
		}
		exit(json_encode($response));
	}

	public function itinerary_quotation($tour_id, Request $request) {
		$response['success'] = false;
		if ($request->ajax()) {
			$response['success'] = true;
			$itinerarys = $this->getTourItineraryLib()->getItineraryByTour($tour_id);
			$tour_info  = $this->getTourLib()->getTourById($tour_id);
			$contents = view('admin/lead/itinerary-rows', ['itinerarys' => $itinerarys])->render();
			$response['contents'] = $contents;
			$response['inclusions'] = (isset($tour_info->inclusions) ? $tour_info->inclusions : '');
			$response['exclusions'] = (isset($tour_info->exclusions) ? $tour_info->exclusions : '');
			$response['terms_conditions'] = (isset($tour_info->terms_conditions) ? $tour_info->terms_conditions : '');
		}
		exit(json_encode($response));
	}

	public function confirm($id) {
		$data = array();
		$data['breadcrumb'] = array(
			array(
				'title' => 'Confirm Leads'
			)
		);
		$data['tours'] = $this->getTourLib()->getTour(['id', 'tour_code']);
		$data['lead'] = $this->getLeadLib()->getLeadById($id);
		if (empty($data['lead']->cnf_lead_id)) {
			$data['lead']->cnf_lead_id = "CNF" . $this->getCnfLeadAutoNoLib()->getNextAutoID();
		}
		$data['tour_type'] = $this->getTourpackagetypemasterLib()->getTourpackagetypemaster(['typeId', 'name']);
		$data['departure_date'] = $this->getTourDepartureLib()->getTourDeparture(['id', 'departure_date'], ['tour_id' => $data['lead']->tour_id]);
		$data['accomodation'] = $this->getAccomodationLib()->getAccomodation("*", ['lead_id' => $id]);
		$data['accomodation_view'] = "";
		if (count($data['accomodation']) > 0) {
			foreach ($data['accomodation'] as $key => $value) {
				$data['accomodation'][$key]->customer = $this->getCustomerinfoLib()->getCustomerinfo("*", ['accomodation_id' => $value->id]);
			}
			$data['accomodation_view'] = view('admin/lead/accomodation-data-edit', $data);
		}
		return view('admin/lead/confirm-lead', $data);
	}

	public function save_confirm_lead(Request $request) {
//		print_r($request->all());die;
		$lead_id = $request->lead_id;
		if (!empty($request->customer)) {
			$this->getAccomodationLib()->deleteAccomodationByLeadId($lead_id);
			$this->getCustomerinfoLib()->deleteCustomerinfoByLeadId($lead_id);
			foreach ($request->customer as $key => $customer) {
				if (!empty($request->accomodation[$key])) {
					$request->accomodation[$key];
					$accomodationData = array(
						"lead_id" => $lead_id,
						"accomodation" => $request->accomodation[$key],
					);
					$accomodation_id = $this->getAccomodationLib()->addAccomodation($accomodationData);
					foreach ($customer as $key => $value) {
						$customerData = array(
							"lead_id" => $lead_id,
							"accomodation_id" => $accomodation_id,
							"name" => $value['name'],
							"birth_date" => (!empty($value['birth_date'])) ? date("Y-m-d", strtotime($value['birth_date'])) : NULL,
							"anniversary_date" => (!empty($value['anniversary_date'])) ? date("Y-m-d", strtotime($value['anniversary_date'])) : NULL,
							"mobile_no" => $value['mobile_no'],
							"email" => $value['email'],
							"passport_no" => $value['passport_no'],
							"issue_date" => (!empty($value['passport_issue_date'])) ? date("Y-m-d", strtotime($value['passport_issue_date'])) : NULL,
							"issue_place" => $value['passport_issue_place'],
							"passport_exp_date" => (!empty($value['passport_exp_date'])) ? date("Y-m-d", strtotime($value['passport_exp_date'])) : NULL,
							"gender" => $value['gender'],
						);
						if (!empty($value['amount'])) {
							$customerData['amount'] = $value['amount'];
						}
						if (!empty($value['child_age_group'])) {
							$customerData['child_age_group'] = $value['child_age_group'];
						}
						$this->getCustomerinfoLib()->addCustomerinfo($customerData);
					}
				}
			}
		}
		$lead = $this->getLeadLib()->getLeadById($lead_id);
		$leadData = array(
			"status_id" => $request->status_id,
			"tour_id" => $request->tour_id,
			"adult" => $request->adult,
			"child" => $request->child,
			"departure_date_id" => $request->departure_date,
			"tour_type" => $request->tour_type,
			"no_of_room" => $request->total_room,
			"invoice_name" => $request->invoice_name,
			"sub_total1" => $request->sub_total1,
			"discount" => $request->discount,
			"discount_amount" => $request->discount_amount,
			"sub_total2" => $request->sub_total2,
			"IGST_amount" => $request->igst,
			"total_amount" => $request->total_amount,
		);

		if (empty($lead->cnf_lead_id)) {
			$cnf_lead_id = $this->getCnfLeadAutoNoLib()->addCnfLeadAutoNo();
			$leadData['cnf_lead_id'] = "CNF" . $cnf_lead_id;
		}
		$this->getLeadLib()->updateLead($leadData, $lead_id);
		$response['success'] = true;
		return response()->json($response);
	}

	public function remove_room($acc_id) {
		$this->getAccomodationLib()->deleteAccomodation($acc_id);
		$this->getCustomerinfoLib()->deleteCustomerinfoByAccId($acc_id);
		$response['success'] = true;
		return response()->json($response);
	}

	public function get_departure_date($tour_id) {
		$departure_date = $this->getTourDepartureLib()->getTourDeparture(['id', 'departure_date'], ['tour_id' => $tour_id]);
		return response()->json(['success' => true, 'departure_date' => $departure_date]);
	}

	public function get_accomodation_data($package_id, $tour_id, $tour_type, $total_element) {
		$data = array();
		$arr = explode(":", $package_id);
		$data['total_adult'] = $arr[0];
		$data['total_child'] = $arr[1];
		$data['total_element'] = $total_element;
//		print_r($tour_cost);
//		die;
		$cost[0] = "0.00";
		$cost[1] = "0.00";
		$cost[2] = "0.00";
		$cost[3] = "0.00";
		if ($data['total_adult'] == 1) {
			$rate_id = get_rate_id($data['total_adult']);
			$tour_cost = $this->getTourCostAmountLib()->getTourCostAmount("*", ['tour_id' => $tour_id, 'package_type_id' => $tour_type, 'rate_id' => $rate_id]);
			if (!empty($tour_cost[0]->cost)) {
				$cost[0] = amount_format($tour_cost[0]->cost);
			}
		} elseif ($data['total_adult'] == 2) {
			$rate_id = get_rate_id($data['total_adult']);
			$tour_cost = $this->getTourCostAmountLib()->getTourCostAmount("*", ['tour_id' => $tour_id, 'package_type_id' => $tour_type, 'rate_id' => $rate_id]);
			if (!empty($tour_cost[0]->cost)) {
				$cost[1] = amount_format($tour_cost[0]->cost);
			}
		} elseif ($data['total_adult'] == 3) {
			$rate_id = get_rate_id(2);
			$tour_cost = $this->getTourCostAmountLib()->getTourCostAmount("*", ['tour_id' => $tour_id, 'package_type_id' => $tour_type, 'rate_id' => $rate_id]);
			if (!empty($tour_cost[0]->cost)) {
				$cost[1] = amount_format($tour_cost[0]->cost);
			}
			$rate_id = get_rate_id(3);
			$tour_cost = $this->getTourCostAmountLib()->getTourCostAmount("*", ['tour_id' => $tour_id, 'package_type_id' => $tour_type, 'rate_id' => $rate_id]);
			if (!empty($tour_cost[0]->cost)) {
				$cost[2] = amount_format($tour_cost[0]->cost);
			}
		} elseif ($data['total_adult'] == 4) {
			$rate_id = get_rate_id(2);
			$tour_cost = $this->getTourCostAmountLib()->getTourCostAmount("*", ['tour_id' => $tour_id, 'package_type_id' => $tour_type, 'rate_id' => $rate_id]);
			if (!empty($tour_cost[0]->cost)) {
				$cost[1] = amount_format($tour_cost[0]->cost);
			}
			$rate_id = get_rate_id(3);
			$tour_cost = $this->getTourCostAmountLib()->getTourCostAmount("*", ['tour_id' => $tour_id, 'package_type_id' => $tour_type, 'rate_id' => $rate_id]);
			if (!empty($tour_cost[0]->cost)) {
				$cost[2] = amount_format($tour_cost[0]->cost);
				$cost[3] = amount_format($tour_cost[0]->cost);
			}
		}
		$child_cost[1] = "0.00";
		$child_cost[2] = "0.00";
		$child_cost[3] = "0.00";
		if ($data['total_child'] > 0) {
			$rate_id = get_rate_id(6);
			$tour_cost = $this->getTourCostAmountLib()->getTourCostAmount("*", ['tour_id' => $tour_id, 'package_type_id' => $tour_type, 'rate_id' => $rate_id]);
			if (!empty($tour_cost[0]->cost)) {
				$child_cost[1] = amount_format($tour_cost[0]->cost);
			}
			$rate_id = get_rate_id(7);
			$tour_cost = $this->getTourCostAmountLib()->getTourCostAmount("*", ['tour_id' => $tour_id, 'package_type_id' => $tour_type, 'rate_id' => $rate_id]);
			if (!empty($tour_cost[0]->cost)) {
				$child_cost[2] = amount_format($tour_cost[0]->cost);
			}
		}
		$data['cost'] = $cost;
		$data['child_cost'] = $child_cost;
		return view('admin/lead/accomodation-data', $data)->render();
	}

	public function new_payment($lead_id) {
		$lead_info 		= $this->getLeadLib()->getLeadById($lead_id);
		//dd($lead_info);
		$data['type_of_payment'] = $this->getCommonLib()->type_of_payment();
		$data['paymentType'] = $this->getCommonLib()->paymentType();
		$data['paymentMode'] = $this->getCommonLib()->paymentMode();
		$data['lead']	=	$lead_info;
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' => route('lead.lead_detail', $lead_id . '#Add payment'),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Add payment'
			),
			array(
				'title' => 'Add new payment'
			)
		);
		return view('admin/lead/new-payment', $data);
	}

	public function customer_discount($lead_id) {
		$lead_info 		= $this->getLeadLib()->getLeadById($lead_id);
		$data['lead']	=	$lead_info;
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' => route('lead.lead_detail', $lead_id),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
			),
			array(
				'title' => 'Add Customer Discount'
			)
		);
		return view('admin/lead/customer-discount', $data);
	}
        
    public function additional_services($lead_id) {
		$lead_info 		= $this->getLeadLib()->getLeadById($lead_id);
		$data['lead']	=	$lead_info;
		$data['breadcrumb'] = array(
			array(
				'link' 	=> route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' 	=> route('lead.lead_detail', $lead_id),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
			),
			array(
				'title' => 'Add Additional Services'
			)
		);
		return view('admin/lead/additional-services', $data);
	}

	public function leadpayment_store(Request $request) {
		$rules = [
			'type_of_payment' 		=> 'required',
			'payment_mode' 			=> 'required',
			'payment_type'		 	=> 'required',
			'amount' 				=> 'required',
			'receipt_name' 			=> 'required',
			'payment_note' 			=> 'required'
		];

		$messages = [
			'type_of_payment.required' 		=> 'Please select type of payment',
			'payment_mode.required' 		=> 'Please select payment detail',
			'payment_type.required' 		=> 'Please select payment type',
			'amount.required' 				=> 'Please enter amount',
			'receipt_name.required' 		=> 'Please enter receipt name',
			'payment_note.required' 		=> 'Please enter note'
		];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails()) {
			return redirect()->route('lead.payment.add_new_payment', $request->lead_id)->withErrors($validator)->withInput();
		} else {
			$records = array(
				"lead_id" 				=> $request->lead_id,
				"type_of_payment" 		=> $request->type_of_payment,
				"payment_type" 			=> $request->payment_type,
				"payment_mode" 			=> $request->payment_mode,
				"paid_amount" 			=> $request->amount,
				"receipt_name" 			=> $request->receipt_name,
				"payment_note" 			=> $request->payment_note,
				"paid_date" 			=> date("Y-m-d H:i:s"),
				"type" 					=> "payment",
				"receipt_no"			=>$this->getReceipLib()->getReceiptNumber()
			);

			//$lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord();
			$lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($request->lead_id);
			
			// if(!empty($lastRecord)){
			// 	if(isset($lastRecord->balance) && $lastRecord->balance!=null){
			// 		$records['balance']=$lastRecord->balance-$request->amount;
			// 	}
			// }
			
			$crbalance=null;
			if(!empty($lastRecord)){
				if(isset($lastRecord->balance) && $lastRecord->balance!=null){
					$records['balance']=$lastRecord->balance-$request->amount;
				}
			}else{
					$records['balance']=$crbalance-$request->amount;
			}
			
			//dd($records);
			if($request->customer_id!=null){
				$records['customer_id'] = $request->customer_id;
			}

			if($request->tour_id!=null){
				$records['tour_id'] = $request->tour_id;
			}
			//dd($records);
			$last_id = $this->getCustomerPaymentLib()->addCustomerPayment($records);
			if($last_id){
				$this->getReceipLib()->updateReceiptNumber();
				Session::flash('success_msg', 'Payment inserted successfully.');	
			}else{
				Session::flash('error_msg', 'Payment inserted unsuccessfully.');
			}
			return redirect()->route('lead.lead_detail', $request->lead_id . '#payment');
		}
	}

	public function customer_discount_store(Request $request) {
		$rules = [
			'discount_title' 		=> 'required',
			'discount_amount' 		=> 'required',
			'discount_notes' 		=> 'required'
		];

		$messages = [
			'discount_title.required' 		=> 'Please enter title',
			'discount_amount.required' 		=> 'Please enter amount',
			'discount_notes.required' 		=> 'Please enter note'
		];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails()) {
			return redirect()->route('lead.payment.add_customer_discount', $request->lead_id)->withErrors($validator)->withInput();
		} else {
			$records = array(
				"lead_id" 				=> $request->lead_id,
				"discount_title" 		=> $request->discount_title,
				"discount_amount" 		=> $request->discount_amount,
				"discount_notes" 		=> $request->discount_notes,
				"type" 					=> "discount"
			);
			if($request->customer_id!=null){
				$records['customer_id'] = $request->customer_id;
			}

			if($request->tour_id!=null){
				$records['tour_id'] = $request->tour_id;
			}

			//$lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord();
			$lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($request->lead_id);
			

			// if(!empty($lastRecord)){
			// 	if(isset($lastRecord->balance) && $lastRecord->balance!=null){
			// 		$records['balance']=$lastRecord->balance-$request->discount_amount;
			// 	}
			// }

			$crbalance=null;
			if(!empty($lastRecord)){
				if(isset($lastRecord->balance) && $lastRecord->balance!=null){
					$records['balance']=$lastRecord->balance-$request->discount_amount;
				}
			}else{
				$records['balance']=$crbalance-$request->discount_amount;
			}

			//dd($records);
			$last_id = $this->getCustomerPaymentLib()->addCustomerPayment($records);
			if($last_id){
				Session::flash('success_msg', 'Discount inserted successfully.');	
			}else{
				Session::flash('error_msg', 'Discount inserted unsuccessfully.');
			}
			return redirect()->route('lead.lead_detail', $request->lead_id . '#payment');
		}
	}

	public function additional_services_store(Request $request) {
		$rules = [
			'additional_services_name' 		=> 'required',
			'additional_service_fees' 		=> 'required',
			'additional_services_notes' 	=> 'required'
		];

		$messages = [
			'additional_services_name.required' 		=> 'Please select service',
			'additional_service_fees.required' 		=> 'Please enter fee',
			'additional_services_notes.required' 	=> 'Please enter note'
		];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails()) {
			return redirect()->route('lead.payment.add_additional_services', $request->lead_id)->withErrors($validator)->withInput();
		} else {
			$records = array(
				"lead_id" 						=> $request->lead_id,
				"additional_services_name" 		=> $request->additional_services_name,
				"additional_service_fees" 		=> $request->additional_service_fees,
				"additional_services_notes" 	=> $request->additional_services_notes,
				"type" 							=> "service"
			);
			if($request->customer_id!=null){
				$records['customer_id'] = $request->customer_id;
			}

			if($request->tour_id!=null){
				$records['tour_id'] = $request->tour_id;
			}

			//$lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord();
			$lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($request->lead_id);

			// if(!empty($lastRecord)){
			// 	if(isset($lastRecord->balance) && $lastRecord->balance!=null){
			// 		$records['balance']=$lastRecord->balance+$request->additional_service_fees;
			// 	}
			// }

			$crbalance=null;
			if(!empty($lastRecord)){
				if(isset($lastRecord->balance) && $lastRecord->balance!=null){
					$records['balance']=$lastRecord->balance+$request->additional_service_fees;
				}
			}else{
				$records['balance']=$crbalance+$request->additional_service_fees;
			}
			
			//dd($records);
			$last_id = $this->getCustomerPaymentLib()->addCustomerPayment($records);
			if($last_id){
				Session::flash('success_msg', 'Additional services inserted successfully.');	
			}else{
				Session::flash('error_msg', 'Additional services inserted unsuccessfully.');
			}
			return redirect()->route('lead.lead_detail', $request->lead_id . '#payment');
		}
	}

	public function lead_payment_data($lead_id, Request $request) {
	    return datatables()->of(CustomerPaymentlead::selectRaw("id,
	    	IFNULL(receipt_no,'-') as receipt_no,
	    	IFNULL(additional_services_name,'-') as additional_services_name,
	    	IFNULL(discount_title,'-') as discount_title,
	    	IFNULL(lead_id,'-') as lead_id,
	    	IFNULL(customer_id,'-') as customer_id,
	    	IFNULL(tour_id,'-') as tour_id,
	    	IFNULL(quotation_id,'-') as quotation_id,
	    	IFNULL(paid_amount,'-') as paid_amount,
	    	IFNULL(additional_service_fees,'0.00') as additional_service_fees,
	    	IFNULL(discount_amount,'0.00') as discount_amount,
	    	IFNULL(total_amount,'0.00') as total_amount,
	    	IFNULL(cancel_amount,'-') as cancel_amount,
	    	IFNULL(refund_amount,'-') as refund_amount,
	    	IFNULL(balance,'-') as balance,
	    	created_at,
	    	IFNULL(payment_mode,'-') as payment_mode,
	    	IFNULL(payment_note,'-') as payment_note,
	    	type")
	    	->where('lead_id', $lead_id)
	    	->where('deleted_at', null)
	    	)
	    	->addColumn('action', function($CustomerPaymentlead) {
	    		if($CustomerPaymentlead->type=='payment'){
	    			return '<a href="'.route('lead_payment.edit',[$CustomerPaymentlead->id,$CustomerPaymentlead->type]).'" title="Edit" class=""><i class="md-icon material-icons">&#xE254;</i></a>&nbsp;<a href="'.route('lead.print_receipt',[$CustomerPaymentlead->id,$CustomerPaymentlead->type]).'" target="_blank" title="Print" class=""><i class="material-icons">print</i></a>';
	    		}elseif ($CustomerPaymentlead->type=='discount') {
	    			return '<a href="'.route('lead_payment.edit',[$CustomerPaymentlead->id,$CustomerPaymentlead->type]).'" title="Edit" class=""><i class="md-icon material-icons">&#xE254;</i></a>';
	    		}elseif ($CustomerPaymentlead->type=='service') {
	    			return '<a href="'.route('lead_payment.edit',[$CustomerPaymentlead->id,$CustomerPaymentlead->type]).'" title="Edit" class=""><i class="md-icon material-icons">&#xE254;</i></a>&nbsp;<a href="'.route('send.proforma.service',[$CustomerPaymentlead->id]).'" title="Send email" class=""><i class="material-icons">email</i></a>';
	    		}else{
	    			return '<a href="'.route('quote.proforma.print',[$CustomerPaymentlead->quotation_id]).'" target="_blank" title="Print" class=""><i class="material-icons">print</i></a>';
	    		}
            })
            ->editColumn('created_at', function ($model) {
            	return date("d M Y", strtotime($model->created_at));
            })
            ->editColumn('receipt_no', function ($CustomerPaymentlead) {
            	if($CustomerPaymentlead->type=='payment'){
            		return $CustomerPaymentlead->receipt_no;
            	}elseif ($CustomerPaymentlead->type=='discount') {
            		return $CustomerPaymentlead->discount_title;
            	}elseif ($CustomerPaymentlead->type=='service') {
            		return $CustomerPaymentlead->additional_services_name;
            	}else{
            		$quotation_id = $this->getLeadQuatationLib()->getLeadQuatationRecordById($CustomerPaymentlead->quotation_id);
            		$invoice = $this->getInvoiceLib()->getInvoice('proforma', $CustomerPaymentlead->lead_id, $quotation_id->quaId);
            		$receipt_no = '';
            		if($invoice){
            			$receipt_no = $invoice->receipt_number;
            		}
            		return "Proferma invoice number:".$receipt_no." for quotation number:".$quotation_id->quatation_id;
            	}
            })
            ->editColumn('additional_service_fees', function ($CustomerPaymentlead) {
            	if($CustomerPaymentlead->type=='payment'){
            		return '-';
            	}elseif ($CustomerPaymentlead->type=='discount') {
            		return '-';
            	}elseif ($CustomerPaymentlead->type=='service') {
            		return $CustomerPaymentlead->additional_service_fees;
            	}else{
            		return $CustomerPaymentlead->total_amount;
            	}
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %b %Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('receipt_no', function ($query, $keyword) {
                    $query->whereRaw("receipt_no like ?", ["%$keyword%"]);
            })
	        ->toJson();

	}

	public function lead_payment_edit($id,$type) {
		$payment_info 		= $this->getCustomerPaymentLib()->getCustomerPaymentById($id);
		if(isset($payment_info->lead_id)){
			$lead_info 		= $this->getLeadLib()->getLeadById($payment_info->lead_id);
		}
		$data['type_of_payment'] = $this->getCommonLib()->type_of_payment();
		$data['paymentType'] = $this->getCommonLib()->paymentType();
		$data['paymentMode'] = $this->getCommonLib()->paymentMode();
		$data['payment_info']	=	$payment_info;
		$data['breadcrumb'] = array(
			array(
				'link' => route('lead.index'),
				'title' => 'Manage Leads'
			),
			array(
				'link' 	=> route('lead.lead_detail', $payment_info->lead_id),
				'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
			),
			array(
				'title' => 'Edit payment'
			)
		);
		if($type=='payment'){
			return view('admin/lead/new-payment-edit', $data);
		}elseif ($type=='discount') {
			return view('admin/lead/customer-discount-edit', $data);
		}elseif ($type=='service') {
			return view('admin/lead/additional-services-edit', $data);
		}else{
			return redirect()->route('lead.lead_detail', $payment_info->lead_id);
		}	
	}

	public function leadpayment_update(Request $request) {
		$payment_info 		= $this->getCustomerPaymentLib()->getCustomerPaymentById($request->id);
		if(isset($payment_info->type)){
			if($payment_info->type=='payment'){
				$rules = [
					'type_of_payment' 		=> 'required',
					'payment_mode' 			=> 'required',
					'payment_type'		 	=> 'required',
					'amount' 				=> 'required',
					'receipt_name' 			=> 'required',
					'payment_note' 			=> 'required'
				];

				$messages = [
					'type_of_payment.required' 		=> 'Please select type of payment',
					'payment_mode.required' 		=> 'Please select payment detail',
					'payment_type.required' 		=> 'Please select payment type',
					'amount.required' 				=> 'Please enter amount',
					'receipt_name.required' 		=> 'Please enter receipt name',
					'payment_note.required' 		=> 'Please enter note'
				];
			}elseif ($payment_info->type=='discount') {
				$rules = [
					'discount_title' 		=> 'required',
					'discount_amount' 		=> 'required',
					'discount_notes' 		=> 'required'
				];

				$messages = [
					'discount_title.required' 		=> 'Please enter title',
					'discount_amount.required' 		=> 'Please enter amount',
					'discount_notes.required' 		=> 'Please enter note'
				];
			}elseif ($payment_info->type=='service') {
				$rules = [
					'additional_services_name' 		=> 'required',
					'additional_service_fees' 		=> 'required',
					'additional_services_notes' 	=> 'required'
				];

				$messages = [
					'additional_services_name.required' 	=> 'Please select service',
					'additional_service_fees.required' 		=> 'Please enter fee',
					'additional_services_notes.required' 	=> 'Please enter note'
				];
			}else{
				return redirect()->route('lead.lead_detail', $payment_info->lead_id);
			}
			$validator = Validator::make($request->all(), $rules, $messages);
			if ($validator->fails()) {
				if($payment_info->type=='payment'){
					return redirect()->route('lead_payment.edit',[$request->id,$payment_info->type])->withErrors($validator)->withInput();
				}elseif ($payment_info->type=='discount') {
					return redirect()->route('lead_payment.edit',[$request->id,$payment_info->type])->withErrors($validator)->withInput();
				}elseif ($payment_info->type=='service') {
					return redirect()->route('lead_payment.edit',[$request->id,$payment_info->type])->withErrors($validator)->withInput();
				}else{
					return redirect()->route('lead.lead_detail', $payment_info->lead_id);
				}
			} else {
				if($payment_info->type=='payment'){
					$records = array(
						"type_of_payment" 		=> $request->type_of_payment,
						"payment_type" 			=> $request->payment_type,
						"payment_mode" 			=> $request->payment_mode,
						"paid_amount" 			=> $request->amount,
						"receipt_name" 			=> $request->receipt_name,
						"payment_note" 			=> $request->payment_note
					);

					//Start
					$current_balance=null;

					$PaymentData=$this->getCustomerPaymentLib()->getCustomerPaymentById($request->id);
					if(!empty($PaymentData)){
						if($request->amount!=$PaymentData->paid_amount){
							//$BalnceRecord=$this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at);
							$BalnceRecord=$this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
							if(!empty($BalnceRecord)){
								if(isset($BalnceRecord->balance) && $BalnceRecord->balance!=null){
									$current_balance = $PaymentData->paid_amount-$request->amount;
									$records['balance']=$BalnceRecord->balance-$request->amount;
								}
							}
						}

						if(isset($PaymentData->created_at)){
							//$AllDownRecord=$this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at);
							$AllDownRecord=$this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
							if(!empty($AllDownRecord)){
								foreach ($AllDownRecord as $key => $value) {
									$cur = $value->balance+$current_balance;
									$this->getCustomerPaymentLib()->updateCustomerPayment(['balance'=>$cur],$value->id);
								}
							}
						}
					}

					$last_id=$this->getCustomerPaymentLib()->updateCustomerPayment($records,$request->id);
					if($last_id){
						Session::flash('success_msg', 'Payment updated successfully.');	
					}else{
						Session::flash('error_msg', 'Payment updated unsuccessfully.');
					}
					return redirect()->route('lead.lead_detail', $payment_info->lead_id . '#payment');
				}elseif ($payment_info->type=='discount') {
					$records = array(
						"discount_title" 		=> $request->discount_title,
						"discount_amount" 		=> $request->discount_amount,
						"discount_notes" 		=> $request->discount_notes
					);

					//Start
					$current_balance=null;

					$PaymentData=$this->getCustomerPaymentLib()->getCustomerPaymentById($request->id);
					if(!empty($PaymentData)){
						if($request->discount_amount!=$PaymentData->discount_amount){
							//$BalnceRecord=$this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at);
							$BalnceRecord=$this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
							if(!empty($BalnceRecord)){
								if(isset($BalnceRecord->balance) && $BalnceRecord->balance!=null){
									$current_balance = $PaymentData->discount_amount-$request->discount_amount;
									$records['balance']=$BalnceRecord->balance-$request->discount_amount;
								}
							}
						}
						if(isset($PaymentData->created_at)){
							//$AllDownRecord=$this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at);
							$AllDownRecord=$this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
							if(!empty($AllDownRecord)){
								foreach ($AllDownRecord as $key => $value) {
									$cur = $value->balance+$current_balance;
									$this->getCustomerPaymentLib()->updateCustomerPayment(['balance'=>$cur],$value->id);
								}
							}
						}
					}

					$last_id=$this->getCustomerPaymentLib()->updateCustomerPayment($records,$request->id);
					if($last_id){
						Session::flash('success_msg', 'Discount updated successfully.');	
					}else{
						Session::flash('error_msg', 'Discount updated unsuccessfully.');
					}
					return redirect()->route('lead.lead_detail', $payment_info->lead_id . '#payment');
				}elseif ($payment_info->type=='service') {
					$records = array(
						"additional_services_name" 		=> $request->additional_services_name,
						"additional_service_fees" 		=> $request->additional_service_fees,
						"additional_services_notes" 	=> $request->additional_services_notes
					);
					//Start
					$current_balance=null;

					$PaymentData=$this->getCustomerPaymentLib()->getCustomerPaymentById($request->id);
					if(!empty($PaymentData)){
						if($request->additional_service_fees!=$PaymentData->additional_service_fees){
							//$BalnceRecord=$this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at);
							$BalnceRecord=$this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
							if(!empty($BalnceRecord)){
								if(isset($BalnceRecord->balance) && $BalnceRecord->balance!=null){
									$current_balance = $request->additional_service_fees-$PaymentData->additional_service_fees;
									$records['balance']=$BalnceRecord->balance+$request->additional_service_fees;
								}
							}
						}
						//dd($records);
						if(isset($PaymentData->created_at)){
							//$AllDownRecord=$this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at);
							$AllDownRecord=$this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
							if(!empty($AllDownRecord)){
								foreach ($AllDownRecord as $key => $value) {
									$cur = $value->balance+$current_balance;
									$this->getCustomerPaymentLib()->updateCustomerPayment(['balance'=>$cur],$value->id);
								}
							}
						}
					}

					$last_id=$this->getCustomerPaymentLib()->updateCustomerPayment($records,$request->id);
					if($last_id){
						Session::flash('success_msg', 'Additional services updated successfully.');	
					}else{
						Session::flash('error_msg', 'Additional services updated unsuccessfully.');
					}
					return redirect()->route('lead.lead_detail', $payment_info->lead_id . '#payment');
				}else{
					return redirect()->route('lead.lead_detail', $payment_info->lead_id);
				}
			}
		}else{
			return redirect()->route('lead.lead_detail', $payment_info->lead_id);
		}
	}

	public function send_proforma_service($id){
		$data['payment_info'] = $payment_info = $this->getCustomerPaymentLib()->getCustomerPaymentById($id);
		$data['lead_info'] = $lead = $this->getLeadLib()->getLeadInfo($payment_info->lead_id);
		if ($data['lead_info']) {

	        $invoice = $this->getInvoiceLib()->getInvoice('proforma', $payment_info->lead_id);
	        if (isset($invoice) && $invoice != null) {
	            $receipt_no = $invoice->receipt_number;
	        } else {
	            $receipt_no = $this->getReceipLib()->getProformaReceiptNumber();
	            $inset_data = array(
	                'lead_id' => $payment_info->lead_id,
	                'type' => 'proforma',
	                'customer_id' => ( (isset($lead->customer_id) && $lead->customer_id > 0 ) ? $lead->customer_id : ''),
	                'receipt_number' => $receipt_no,
	                'quatation_id' => $quotation_id,
	                'created_at' => date("Y-m-d H:i:s")
	            );
	            $invoice = $this->getInvoiceLib()->insertInvoice($inset_data);
	            $this->getReceipLib()->updateProformaReceiptNumber();
	        }
	        $data['receipt_no'] = $receipt_no;
	        $contents = view('admin/quotation/service-quotation-template', $data);
	        $data = array();
	        $pdf = \App::make('dompdf.wrapper');
	        $pdf->loadHTML($contents);
	        $file_name = isset($lead->name) ? $lead->name . time() . '.pdf' : time() . '.pdf';
	        $path = public_path('pdf');
	        $file = public_path() . '/pdf/' . $file_name;
	        // dd($path);
	        if(!file_exists($path)) {
	            File::makeDirectory($path, $mode = 0777, true, true);
	        }
	        $pdf->save($file);

	        $data = array('email' => $lead->email, 'file' => $file);
	        Mail::send([], $data, function($message)  use ($data )  {
	             $message->to($data['email'], 'Low Cost Trip')->subject
	                ('Low cost trip');
	             $message->attach($data['file']);
	             $message->from('info@lowcosttrip.com','Low cost trip');
	        });
	        Session::flash('success_msg', 'Proferma invoice mail sent successfully!');
			return redirect()->route('lead.lead_detail', $payment_info->lead_id . '#payment');
        } else {
            return redirect()->route('lead.lead_detail', $payment_info->lead_id . '#payment');
        }
	}

    public function get_activiti_html() { 
    	$data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
		$view = \View::make('admin/lead/activites-html', $data);
    	$return['html'] = $view->render();
    	$return['success'] = true;
    	return response()->json($return);
    }

    public function get_activiti_by_city(Request $request){
        $return['activities'] = $this->getBookActivitiesLib()->getActivitiesByCity($request->id); 
        $return['success'] = true;
        return response()->json($return);
    }
    public function send_mail_to_user($user_id)
    {
    	$data['user_info'] = $this->getUserLib()->getUserById($user_id);
    	$data['user_profile'] = $this->getUserprofileLib()->getUserprofileById($user_id);
    	$d = array('email' => $data['user_info']->email );
        Mail::send('client/register-template-add-lead', $data, function($message)  use ($d )  {
             $message->to($d['email'], 'Low Cost Trip')->subject
                ('Thank you for choosing low cost trip');
             $message->from('info@lowcosttrip.com','Low Cost Trip');
        });
    }

}
