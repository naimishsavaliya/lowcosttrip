<?php

namespace App\Http\Controllers\admin;

use App\User;
use App\Role;
use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ControllerAbstract;
use App\Authorizable;
use Session;


class RoleController extends ControllerAbstract {
    use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage User Role'
            )
        );
        return view('admin/user-role', $data);
    }

    public function data(Request $request) {
        return datatables()->of(Role::select('id', 'name', 'created_at'))
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->filterColumn('created_at', function ($query, $keyword) {
                            $query->whereRaw("DATE_FORMAT(created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                        })
                        ->addColumn('action', '')
                        //->rawColumns(['status', 'action'])
                        ->toJson();
    }

    public function create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('role.index'),
                'title' => 'Manage User Role'
            ),
            array(
                'title' => 'Add Role'
            )
        );
        return view('admin/user-role-manage', $data);
    }

    public function store(Request $request) {
        //validate post data
        $this->validate($request, [
            'name' => 'required',
            'guard_name' => 'required'
        ]);

        //get post data
        $roleData = $request->all();

        //update post data
        Role::create($roleData);

        //store status message
        Session::flash('success_msg', 'Role created successfully!');

        return redirect()->route('role.index');
    }

    public function role_permission($id) {
        $role = Role::find($id);
        $permissions = Permission::all();
        $breadcrumb = array(
            array(
                'link' => route('role.index'),
                'title' => 'Manage User Role'
            ),
            array(
                'title' => 'Permissions'
            )
        );
        return view('admin/user/role-permission', compact('role', 'permissions', 'breadcrumb'));
    }

    public function update(Request $request, $id) {
        if ($role = Role::findOrFail($id)) {
            // admin role has everything
            $msg = $role->name . ' permissions has been updated.';
            if ($role->name === 'Admin') {
                $role->syncPermissions(Permission::all());
                return redirect('/admin/role/role-permission/' . $id)->with('success', $msg);
            }
            $permissions = $request->get('permissions', []);
            $role->syncPermissions($permissions);
        } else {
            $msg = 'Role with id ' . $id . ' not found.';
        }
        return redirect('/admin/role/role-permission/' . $id)->with('success', $msg);
    }

}
