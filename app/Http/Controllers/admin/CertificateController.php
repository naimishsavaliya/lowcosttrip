<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Certificates;
use App\Http\Controllers\ControllerAbstract;
use Session;
use Validator;

class CertificateController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Certificates'
            )
        );
        return view('admin/certificate/index', $data);
    }

    /*
     * Get Certificates data in JSON
     */

    public function data(Request $request) {
        return datatables()->of(Certificates::select('cerId', 
            'name', 'image', 'description', 'status', 'created_at')
                                ->where('deleted_at', null))
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->editColumn('image', function ($model) {
                            return \App\Helpers\S3url::s3_get_image($model->image);
                        })
                        ->addColumn('action', '')
                        //->rawColumns(['description', 'status', 'action'])
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('certificates.index'),
                'title' => 'Manage Certificates'
            ),
            array(
                'title' => 'Add Certificate'
            )
        );
        return view('admin/certificate/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate certificate data
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter certificate name',
            'status.required' => 'Please select  status',
        ];

//        if (isset($request->id) == false) {
//            $rules['currency_icon'] = 'required';
//            $messages['currency_icon.required'] = 'Please upload certificate image';
//        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('certificates.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('certificates.add')->withErrors($validator)->withInput();
            }
        } else {
            //get sector data
            $certificateData = $request->all();

            $data = array(
                'name' => $certificateData['name'],
                'description' => $certificateData['description'],
                'status' => $certificateData['status'],
            );


            $file = $request->file('image');
            if (!empty($file)) {
                $imagename = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $file->getClientOriginalExtension();
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'certificates/' . $imagename, file_get_contents($file->getRealPath())
                );
                $data['image'] = "certificates/" . $imagename;
            }

            if ($certificateData['id'] > 0) {
                $this->getCertificatesLib()->updateCertificates($data, $certificateData['id']);
                Session::flash('success_msg', 'Certificate has been updated successfully!');
            } else {
                $this->getCertificatesLib()->addCertificates($data);
                Session::flash('success_msg', 'Certificate has been saved successfully!');
            }
        }

        return redirect()->route('certificates.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['certificate'] = $this->getCertificatesLib()->getCertificatesById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('certificates.index'),
                'title' => 'Manage Certificates'
            ),
            array(
                'title' => 'Edit Certificate'
            )
        );

        return view('admin/certificate/create', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->getCertificatesLib()->deleteCertificates($id);
        if ($deleted) {
            Session::flash('success_msg', 'Certificate has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Certificate can't deleted successfully!");
        }
        return redirect()->route('certificates.index');
    }

}
