<?php

namespace App\Http\Controllers\admin;

use App\hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use App\Hoteltype;
use App\Roomtype;
use Validator;
use Session;
use DB;

class FlightsController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Flights Bookings'
            )
        );
        return view('admin/flights/index', $data);
    }

    public function FlightsData(Request $request) {
        if ($request->ajax()) {
            $input = $request->all();
            if ($keyword = $input['search']['value']) {
            }

            $query = \App\FlightBooks::select('flight_books.id', 
                    'flight_books.email', 
                    'flight_books.mobile', 
                    'flight_books.depart_date', 
                    'flight_books.passengers_class', 
                    'c1.city_name as flight_from',
                    'c2.city_name as flight_to' ,
                    \DB::raw("CONCAT(flight_books.adults, '/', flight_books.child, '/', flight_books.infants) as passengers"))
                    ->leftjoin('city as c1', 'flight_books.flight_from', '=', 'c1.city_code')
                    ->leftjoin('city as c2', 'flight_books.flight_to', '=', 'c2.city_code')
                    
                    ->groupBy('flight_books.id');
 
            return datatables()->of($query)
                
                ->editColumn('depart_date', function ($model) {
                    return date("d M Y", strtotime($model->depart_date));
                })

                ->editColumn('passengers_class', function($passengers_class) {
                    foreach (get_air_class() as $val => $cls) {
                       if($passengers_class->passengers_class== $val )
                          return $cls;
                    }
                  })

                ->filterColumn('passengers', function($query, $keyword) {
                    $query->whereRaw("CONCAT(flight_books.adults, '/', flight_books.child, '/', flight_books.infants) like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('flight_from', function($query, $keyword) {
                    $query->whereRaw("c1.city_name like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('flight_to', function($query, $keyword) {
                    $query->whereRaw("c2.city_name like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('depart_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(flight_books.created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                })
                ->filterColumn('passengers_class', function ($query, $keyword) {
                     foreach (get_air_class() as $val => $status) {
                          if (strstr(strtolower($status), strtolower($keyword) ) !== false) {
                            $query->whereRaw("flight_books.passengers_class like ?", ["%$val%"]);
                          }
                      }
                })
                ->addColumn('action', '')
                ->toJson();
        }
    }

    public function view($id) {
        $data['flight_books']        = $this->getFlightBooksLib()->getFlightBooksById($id);
        $data['flight_payment_info'] = $this->getFlightBooksLib()->getFlightBooksPayment($data['flight_books']->payment_id);
        $data['flight_review_info']  = $this->getFlightReviewInfoLib()->getFlightReviewByFlightId($id);
        $data['flight_passengers']   = $this->getFlightPassengersLib()->getFlightPassengersByFlightId($id);
        // dd( $data);
        $data['breadcrumb'] = array(
            array(
                'link' => route('manage.flight.booking'),
                'title' => 'Manage Flights Bookings'
            ),
            array(
                'title' => 'View Booking Info'
            )
        );
        return view('admin/flights/booking-info', $data);
    }


}
