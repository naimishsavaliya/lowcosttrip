<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Discount;
use App\Http\Controllers\ControllerAbstract;
use Validator;
use Session;

class DiscountController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Discount Codes'
            )
        );
        $data['discount_for'] = $this->getCommonLib()->getDiscountFor();
        return view('admin/discount/index', $data);
    }

    /*
     * Get Discount Data in JSON
     */

    public function data(Request $request) {
        return datatables()->of(Discount::select('desId', 'discount_for', 'discount_code', 'type', 'amount', 'min_cart_amount', 'max_discount_amount', 'start_date', 'end_date', 'status', 'created_at')
                                ->where('deleted_at', null))
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->editColumn('start_date', function ($model) {
                            return date("d M y", strtotime($model->start_date));
                        })
                        ->editColumn('end_date', function ($model) {
                            return date("d M y", strtotime($model->end_date));
                        })
                        ->editColumn('type', function ($model) {
                            if ($model->type == 1) {
                                return "Fix";
                            }
                            if ($model->type == 2) {
                                return "Percentage";
                            }
                        })
                        ->editColumn('discount_for', function ($model) {
                            $discount_for_str = '';
                            $disc_arr = explode(',', $model->discount_for);
                            if (count($disc_arr) > 0) {
                                foreach ($disc_arr as $d_key => $ds_key) {
                                    if ($d_key == 0) {
                                        $discount_for_str .= $this->getCommonLib()->getDiscountFor($ds_key);
                                    } else {
                                        $discount_for_str .= ', ' . $this->getCommonLib()->getDiscountFor($ds_key);
                                    }
                                }
                            }
                            return $discount_for_str;
                        })
                        ->filterColumn('start_date', function($query, $keyword) {
                            $start_date = date('Y-m-d', strtotime($keyword));
                            $query->whereRaw("DATE_FORMAT(start_date, '%Y-%m-%d') >= '" . $start_date . "'");
                        })
                        ->filterColumn('end_date', function($query, $end_date) {
                            $end_date = date('Y-m-d', strtotime($end_date));
                            $query->whereRaw("DATE_FORMAT(end_date, '%Y-%m-%d') <= '" . $end_date . "'");
                        })
                        ->addColumn('action', '')
                        ->rawColumns(['status', 'action'])
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['discount_for'] = $this->getCommonLib()->getDiscountFor();
        $data['breadcrumb'] = array(
            array(
                'link' => route('discount.index'),
                'title' => 'Manage Discount Codes'
            ),
            array(
                'title' => 'Add Discount'
            )
        );
        return view('admin/discount/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = [
            'discount_for' => 'required',
            'discount_code' => 'required',
            'type' => 'required',
            'amount' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'discount_for' => 'Please select discount for',
            'discount_code' => 'Please enter discount code',
            'type' => 'Please select discount type',
            'amount' => 'Please enter amount',
            'start_date' => 'Please select start date',
            'end_date' => 'Please select end date',
            'status' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('discount.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('discount.add')->withErrors($validator)->withInput();
            }
        } else {

            //get Facilities data
            $discountData = $request->all();

            $data = array(
                'discount_for' => implode(',', $discountData['discount_for']),
                'discount_code' => $discountData['discount_code'],
                'type' => $discountData['type'],
                'amount' => $discountData['amount'],
                'max_discount_amount' => $discountData['max_discount_amount'],
                'min_cart_amount' => $discountData['min_cart_amount'],
                'start_date' => date('Y-m-d H:i:s', strtotime($discountData['start_date'])),
                'end_date' => date('Y-m-d H:i:s', strtotime($discountData['end_date'])),
                'valid_on' => $discountData['valid_on'],
                'status' => $discountData['status'],
            );

            if ($discountData['id'] > 0) {
                $this->getDiscountLib()->updateDiscount($data, $discountData['id']);
                Session::flash('success_msg', 'Discount updated successfully!');
            } else {
                $this->getDiscountLib()->addDiscount($data);
                Session::flash('success_msg', 'Discount saved successfully!');
            }
        }

        return redirect()->route('discount.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['discount'] = $this->getDiscountLib()->getDiscountById($id);
        $data['discount_for'] = $this->getCommonLib()->getDiscountFor();
        $data['breadcrumb'] = array(
            array(
                'link' => route('discount.index'),
                'title' => 'Manage Discount Codes'
            ),
            array(
                'title' => 'Edit Discount'
            )
        );
        return view('admin/discount/create', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getDiscountLib()->deleteDiscount($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Discount has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Discount Tour can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

}
