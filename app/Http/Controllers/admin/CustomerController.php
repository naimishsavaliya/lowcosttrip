<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ControllerAbstract;
use App\Customertype;
use Session;
use DB;
use Validator;
use Hash;

class CustomerController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        return view('admin/customer/index', $data);
    }

    public function data(Request $request) {
        return datatables()->of(User::select('id', DB::raw("CONCAT(first_name,' ',last_name) as full_name"), 'email','status')
            ->where('deleted_at', null)
            ->where('role_id', 5))
            ->filterColumn('full_name', function($query, $keyword) {
                $query->whereRaw("CONCAT(first_name,' ',last_name) like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', '')
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['timezones'] = $this->getCommonLib()->getTimezoneData();
        $data['breadcrumb'] = array(
            array(
                'link' => route('customer.index'),
                'title' => 'Manage Customer'
            ),
            array(
                'title' => 'Add Customer'
            )
        );
        return view('admin/customer/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $userId = "";
        $userData = $request->all();
        if (!empty($userData['id'])) {
            $userId = $userData['id'];
        }

        if (empty($userData['id'])) {
            $rules = [
                'first_name'    => 'required|string|max:255',
                'last_name'     => 'required|string|max:255',
                'email'         => 'required|string|email|max:255|unique:users',
                'phone_no'      => 'required|string|max:20|unique:users_profile',
                'password'      =>'required|min:6',
                'confirm_password'=>'required|same:password|min:6',
                'age'           => 'required',
                'gender'        => 'required',
                'address'       => 'required',
                'country_id'    => 'required',
                'state_id'      => 'required',
                'city_id'       => 'required',
                'pin_code'      => 'required',
                'status'        => 'required'
            ];
        }else{
            $rules = [
                'first_name'    => 'required|string|max:255',
                'last_name'     => 'required|string|max:255',
                'email'         => 'required|string|email|max:255',
                'phone_no'      => 'required|string|max:20',
                'age'           => 'required',
                'gender'        => 'required',
                'address'       => 'required',
                'country_id'    => 'required',
                'state_id'      => 'required',
                'city_id'       => 'required',
                'pin_code'      => 'required',
                'status'        => 'required'
            ];
        }
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('customer.edit', $userId)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('customer.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['first_name']     = $userData['first_name'];
            $data['last_name']      = $userData['last_name'];
            if (empty($userId)) {
                $data['email']      = $userData['email'];
            }

            if($userData['password']){
               $data['password']       = Hash::make($userData['password']);
            }

            $data['status']         = $userData['status'];

            if (empty($userId)) {
                 $profileData['phone_no']      = $userData['phone_no'];
            }

            $profileData = array(
                'landline_no'       => $userData['landline_no'],
                'age'               => $userData['age'],
                'gender'            => $userData['gender'],
                'time_zone'         => $userData['time_zone'],
                'address'           => $userData['address'],
                'country_id'        => $userData['country_id'],
                'state_id'          => $userData['state_id'],
                'city_id'           => $userData['city_id'],
                'pin_code'          => $userData['pin_code'],
                'information'       => $userData['information']
            );

            if (!empty($userData['birth_date'])) {
                $profileData['birth_date'] = date('Y-m-d', strtotime($userData['birth_date']));
            }

            if ($userData['id'] > 0) {
                $this->getUserLib()->updateUser($data, $userData['id']);
                $this->getUserprofileLib()->updateUserprofile($profileData, $userData['id']);
                Session::flash('success_msg', 'Customer has been updated successfully!');
            } else {
                $userId = $this->getUserLib()->addUser($data);
                $profileData['user_id'] = $userId;
                $this->getUserprofileLib()->addUserprofile($profileData);
                Session::flash('success_msg', 'Customer has been saved successfully!');
            }
            $user = $this->getUserLib()->getUserById($userId);
            //Updoad image and update in db
            $image = $request->file('image');
            if (!empty($image)) {
                $imagename = $userId . '_user.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/user_profile');
                $image->move($destinationPath, $imagename);
                $profilepicData['image'] = $imagename;
                $this->getUserprofileLib()->updateUserprofile($profilepicData, $userId);
            }
        }

        return redirect()->route('customer.index');
            
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array();
        return view('admin/customer/view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['users'] = $this->getUserLib()->getUserWithProfile($id);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        if($data['users'][0]->country_id != NULL && $data['users'][0]->country_id > 0){
        $data['states'] = $this->getStateLib()->getStateByCountry($data['users'][0]->country_id, array('state_id', 'state_name'));
        }
        if($data['users'][0]->state_id != NULL && $data['users'][0]->state_id > 0){
        $data['cities'] = $this->getCityLib()->getCityByState($data['users'][0]->state_id, array('city_id', 'city_name'));
        }
        $data['timezones'] = $this->getCommonLib()->getTimezoneData();
        $data['breadcrumb'] = array(
            array(
                'link' => route('customer.index'),
                'title' => 'Manage Customer'
            ),
            array(
                'title' => 'Edit Customer'
            )
        );
        return view('admin/customer/create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->getUserLib()->deleteUser($id);
        if ($deleted) {
            Session::flash('success_msg', 'Customer has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Customer can't deleted!");
        }
        return redirect()->route('customer.index');
    }

    /*
     * @param int @id
     * Customer Booking view
     */

    public function booking($id) {
        $data = array();
        return view('admin/customer/booking', $data);
    }

    /*
     * @param int @id
     * Customer Past Booking view
     */

    public function pastbooking($id) {
        $data = array();
        return view('admin/customer/pastbooking', $data);
    }

    /*
     * Customer Type List
     */

    public function type() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Customer Type'
            )
        );
        return view('admin/customer/type',$data);
    }

    /*
     * Customer data in JSON
     */

    public function type_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(Customertype::select('typeId', 'name', 'description', 'status')
                                    ->where('deleted_at', null)
                            )
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 250, "...");
                            })
                            ->addColumn('action', '')
                            //->rawColumns(['status', 'action'])
                            ->toJson();
        }
    }

    /*
     * Customer Type Add
     */

    public function type_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('customer.type'),
                'title' => 'Manage Customer Type'
            ),
            array(
                'title' => 'Add Customer Type'
            )
        );
        return view('admin/customer/type_add',$data);
    }

    /*
     * Customer Type Edit
     */

    public function type_edit($id) {
        $data['customer_type'] = $this->getCustomertypemasterLib()->getCustomertypemasterById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('customer.type'),
                'title' => 'Manage Customer Type'
            ),
            array(
                'title' => 'Edit Customer Type'
            )
        );
        return view('admin/customer/type_add', $data);
    }

    /*
     * Customer type store
     */

    public function type_store(Request $request) {
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter customer type',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('customer.type.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('customer.type.add')->withErrors($validator)->withInput();
            }
        } else {
            $customerData = array();
            $customerData['name'] = $request->name;
            $customerData['description'] = $request->description;
            $customerData['status'] = $request->status;
            if (isset($request->id)) {
                $id = $request->id;
                $this->getCustomertypemasterLib()->updateCustomertypemaster($customerData, $id);
                Session::flash('success_msg', 'Customer type has been updated successfully!');
            } else {
                $this->getCustomertypemasterLib()->addCustomertypemaster($customerData);
                Session::flash('success_msg', 'Customer type has been saved successfully!');
            }
            return redirect(route('customer.type'));
        }
    }

    /*
     * Customer type Delete
     */

    public function type_destroy($id) {
        $deleted = $this->getCustomertypemasterLib()->deleteCustomertypemaster($id);
        if ($deleted) {
            Session::flash('success_msg', 'Customer type has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Customer type can't deleted!");
        }
        return redirect(route('customer.type'));
    }

    public function wallet($id) {
        $user = $this->getUserLib()->getUserById($id);
        $full_name='';
        if(isset($user->first_name) && $user->first_name!=''){
            $full_name.=$user->first_name;
        }
        if(isset($user->last_name) && $user->last_name!=''){
            $full_name.=' '.$user->last_name;
        }
        $data['breadcrumb'] = array(
            array(
                'link' => route('customer.index'),
                'title' => 'Manage Customer'
            ),
            array(
                'link' => route('customer.wallet',[$id]),
                'title' => 'Manage Wallet'
            ),
            array(
                'title' => $full_name
            )
        );
        return view('admin/customer/wallet-index', $data);
    }

    public function wallet_data($id, Request $request) {
        if ($request->ajax()) {
            $query = \App\Wallet::select('id','user_id','created_at','credit','debit','current_balance')
                        ->where('user_id', $id)
                        ->orderBy('id', 'DESC');
            return datatables()->of($query)
                ->editColumn('created_at', function ($model) {
                    return date("d M Y", strtotime($model->created_at));
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                })
                ->toJson();
        }
    }

}
