<?php

namespace App\Http\Controllers\admin;

use App\Tour;
use App\DomesticHoliday;
use App\InternationalHoliday;
use App\FeaturedTblTour;
use App\HotDealTour;
use App\NewArrivalTour;
use App\FeaturedDestinationTour;
use App\Leadquatation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use App\Tourtype;
use App\Tourpackagetype;
use Validator;
use Session;
use App\Authorizable;
use DB;

class TourController extends ControllerAbstract {

    use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        echo Session::get('sector_id');    
//        echo Session::get('sector_id');    
//        $data12 = Session::all();
//        print_r($data12);
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Tour Package',
            )
        );
        $data['tour_group'] = $this->getCommonLib()->getTourGroup();
        $userSector = Session::get('sector_id');
        $data['userSector'] = array();
        if ($userSector != '') {
            $data['userSector'] = explode(',', \Session::get('sector_id'));
        }
        return view('admin/tour/tour', $data);
    }

    public function detail(Tour $tour, $id) {
        // $actualtouravailable = $this->getTourLib()->checkTour($id);
        // if($actualtouravailable>0){
        //   dd('IF');
        // }else{
        //   abort(404);
        // }
        // dd('Hello');
        $data = array();
        $data['tour_location_type'] = $this->getCommonLib()->getLocationTourType();
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['tour_display'] = $this->getCommonLib()->getTourDisplay();
        $data['sectors'] = $this->getSectorLib()->getSector(array('id', 'name'), array('status' => 1));
        $data['tour_group'] = $this->getCommonLib()->getTourGroup();
        $data['tour_feature'] = $this->getFeaturetourmasterLib()->getFeaturetourmaster(array('typeId', 'name'));
        $data['tour_itinerary'] = $this->getTourItineraryLib()->getTourInerary("*", array('tour_id' => $id));
        $data['tour_departure'] = $this->getTourDepartureLib()->getTourDeparture(array('id', 'departure_date'), array('tour_id' => $id), date('Y'));
        $data['tour'] = $this->getTourLib()->getTourById($id);
        $data['tour_header'] = $this->getTourLib()->getTourHeader($id);
        $data['tour_faq'] = $this->getTourFAQLib()->getTourFAQ("*", array('tour_id' => $id));
        $data['tour_rate_master'] = $this->getRatetypemasterLib()->getRatetypemaster(array('typeId', 'name'));
        $data['tour_package_type'] = $this->getTourpackagetypemasterLib()->getTourpackagetypemaster(array('typeId', 'name'));
        $data['tour_cost'] = $this->getTourCostLib()->getTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $id));
        $data['tour_cost_amount'] = $this->getTourCostAmountLib()->getTourCostAmount(array('id', 'cost_id', 'rate_id', 'package_type_id', 'cost', 'sell'), array('tour_id' => $id));
        $data['tour_history'] = $this->getTourHistoryLib()->getTourHistory(array('tour_history.type', 'tour_history.message', 'tour_history.created_at'), array('tour_id' => $id));
        $data['tour_hotels'] = $tour_hotels = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $id));
        $data['quotation_status'] = $this->getCommonLib()->getQuotationStatus();

        $tourCostData = array();
        $tourData = array();
        $rateMaster = array();
        foreach ($data['tour_rate_master'] as $rate) {
            $rateMaster[$rate->typeId] = $rate->name;
        }
        $packageType = array();
        foreach ($data['tour_package_type'] as $type) {
            $packageType[$type->typeId] = $type->name;
//            $tourCostData['rate'][$type->typeId];
        }
        foreach ($data['tour_cost'] as $key => $cost) {
            $tourCostData[$cost->id] = array("from_date" => $cost->from_date, "to_date" => $cost->to_date, "discount" => $cost->discount);
            $tourData[$cost->id]['from_date'] = $cost->from_date;
            $tourData[$cost->id]['to_date'] = $cost->to_date;
            $tourData[$cost->id]['discount'] = $cost->discount;
            $tourData[$cost->id]['rates'] = $rateMaster;
            foreach ($rateMaster as $r => $raeM) {
                $tourData[$cost->id]['rates'][$r] = array('type' => $raeM, 'amount' => $packageType);
            }
        }
        foreach ($data['tour_cost_amount'] as $key => $amount) {
            if (!isset($packageType[$amount->package_type_id]) || !isset($rateMaster[$amount->rate_id])) {
                continue;
            }
            if (!isset($tourCostData[$amount->cost_id]['rates'][$amount->rate_id])) {
                $tourCostData[$amount->cost_id]['rates'][$amount->rate_id] = array();
                $tourCostData[$amount->cost_id]['rates'][$amount->rate_id]['type'] = $rateMaster[$amount->rate_id];
            }
            $tourCostData[$amount->cost_id]['rates'][$amount->rate_id]['amount'][] = array('cost' => $amount->cost, "sell" => $amount->sell, 'name' => $packageType[$amount->package_type_id], 'id' => $amount->id);
            //////////////tour data ///////////////
            $costArr = $tourData[$amount->cost_id]['rates'][$amount->rate_id]['amount'][$amount->package_type_id];
            $tourData[$amount->cost_id]['rates'][$amount->rate_id]['amount'][$amount->package_type_id] = array('name' => $costArr, 'cost' => $amount->cost, "sell" => $amount->sell);
            //////////////tour data ///////////////
        }
//        $data['tourCostData'] = $tourCostData;
        $data['tourCostData'] = $tourData;
        return view('admin/tour/tour-detail', $data);
    }

    public function data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(Tour::select('id', 
                'tour_code', 
                'tour_name',
                'domestic_international', 
                DB::raw("CONCAT(tour_nights,' ','Nights') as nights"),
                DB::raw("IFNULL(supplier_id,'-') as supplier_id"), 
                'tour_status')->where('deleted_at', null))
                ->editColumn('domestic_international', function ($model) {
                    return $this->getCommonLib()->getLocationTourType($model->domestic_international);
                })
                ->filterColumn('domestic_international', function ($query, $keyword) use ($request) {
                    if (isset($request['search']) && $request['search']['value'] != '') {
                        $keyword = $request['search']['value'];
                    }
                    $domestic_international_id = $this->getCommonLib()->getLocationTourTypeForDatatable($keyword);
                    if(count($domestic_international_id) >0){
                        $query->where("domestic_international",implode(',',$domestic_international_id), 'in',false );
                    }
                })
                ->filterColumn('nights', function($query, $keyword) {
                    $query->whereRaw("CONCAT(tour_nights,' ','Nights') like ?", ["%{$keyword}%"]);
                })
                ->addColumn('total_lead', '-')
                ->addColumn('total_confirmed_lead', '-')
                ->addColumn('total_travelers', '-')
                ->addColumn('action', '')
                ->toJson();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Tour Package',
                'link' => route('tour.index'),
            ),
            array(
                'title' => 'Add Tour'
            )
        );
        $data['tour_location_type'] = $this->getCommonLib()->getLocationTourType();
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['tour_display'] = $this->getCommonLib()->getTourDisplay();
        $data['sectors'] = $this->getSectorLib()->getSector(array('id', 'name'), array('status' => 1));
        $data['tour_group'] = $this->getCommonLib()->getTourGroup();
        $data['tour_feature'] = $this->getFeaturetourmasterLib()->getFeaturetourmaster(array('typeId', 'name'));
        return view('admin/tour/tour-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'tour_name' => 'required',
            'tour_code' => 'required',
            'tour_nights' => 'required'
            // 'domestic_international' => 'required',
            // 'tour_type' => 'required',
            // 'public_private' => 'required',
            // 'sector' => 'required',
            // 'group_customized' => 'required',
        ];

        $messages = [
            'tour_name' => 'Please enter tour name',
            'tour_code' => 'Please enter tour code',
            'tour_nights' => 'Please enter number of nights'
            // 'domestic_international' => 'Please select type',
            // 'tour_type' => 'Please select category',
            // 'public_private' => 'Please select public/private',
            // 'sector' => 'Please select sector',
            // 'group_customized' => 'Please select group/customized',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('tour.detail', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('tour.add')->withErrors($validator)->withInput();
            }
        } else {
            //get sector data
            $tourData = $request->all();

            $data = array(
                'tour_name' => $tourData['tour_name'],
                'tour_code' => $tourData['tour_code'],
                'tour_nights' => $tourData['tour_nights'],
                'domestic_international' => $tourData['domestic_international'],
                //'tour_type' => $tourData['tour_type'],
                'public_private' => $tourData['public_private'],
                'sector' => $tourData['sector'],
                'group_customized' => $tourData['group_customized'],
                'minimum_tourist' => $tourData['minimum_tourist'],
                'maximum_tourist' => $tourData['maximum_tourist'],
                'online_booking_minimum_amount' => $tourData['online_booking_minimum_amount'],
                //'feature' => implode(',', $tourData['feature']),
                'starting_point' => $tourData['starting_point'],
                'ending_point' => $tourData['ending_point'],
                'key_point' => $tourData['key_point'],
                'short_description' => $tourData['short_description'],
                'long_description' => $tourData['long_description'],
                'inclusions' => $tourData['inclusions'],
                'exclusions' => $tourData['exclusions'],
                'tour_tips' => $tourData['tour_tips'],
                'cancellation_policy' => $tourData['cancellation_policy'],
                'keywords' => $tourData['keywords'],
                'terms_conditions' => $tourData['terms_conditions'],
                'tour_step' => '2',
                'tour_status' => $tourData['tour_status'],
            );
            if (\Auth::user()->role_id == 3) {
                $data['supplier_id'] = \Auth::id();
            } elseif (\Auth::user()->role_id == 4) {
                $data['agent_id'] = \Auth::id();
            }

            

            $file = $request->file('image');
            if (!empty($file)) {
                $imagename = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $file->getClientOriginalExtension();
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'tour/' . $imagename, file_get_contents($file->getRealPath())
                );
                $data['image'] = "tour/" . $imagename;
            }

            if(!empty($tourData['feature'])){
                $data['feature']=implode(',', $tourData['feature']);
            }

            if(!empty($tourData['tour_type'])){
                $data['tour_type']=implode(',', $tourData['tour_type']);
            }

            $full_size_image = $request->file('full_size_image');
            if (!empty($full_size_image)) {
                $imagename = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $full_size_image->getClientOriginalExtension();
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'tour/' . $imagename, file_get_contents($full_size_image->getRealPath())
                );
                $data['full_size_image'] = "tour/" . $imagename;
            }

            $english_pdf = $request->file('english_pdf');
            if (!empty($english_pdf)) {
                $englishpdfname = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $english_pdf->getClientOriginalExtension();
                
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'tour/pdf/' . $englishpdfname, file_get_contents($english_pdf->getRealPath())
                );
                $data['english_pdf'] = "tour/pdf/" . $englishpdfname;
            }

            $gujarati_pdf = $request->file('gujarati_pdf');
            if (!empty($gujarati_pdf)) {
                $gujaratipdfname = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $gujarati_pdf->getClientOriginalExtension();
                
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'tour/pdf/' . $gujaratipdfname, file_get_contents($gujarati_pdf->getRealPath())
                );
                $data['gujarati_pdf'] = "tour/pdf/" . $gujaratipdfname;
            }

            if ($tourData['id'] > 0) {
                $tourId = $tourData['id'];
                $this->getTourLib()->updateTour($data, $tourData['id']);
                $historyData = array('tour_id' => $tourData['id'], 'message' => 'Tour data updated.', 'type' => 'done');
                $this->getTourHistoryLib()->addTourHistory($historyData);
                Session::flash('success_msg', 'Tour has been updated successfully!');
            } else {
                $tourId = $this->getTourLib()->addTour($data);
                $historyData = array('tour_id' => $tourId, 'message' => 'Tour added.', 'type' => 'add');
                $this->getTourHistoryLib()->addTourHistory($historyData);
                Session::flash('success_msg', 'Tour has been saved successfully!');
            }
            //Update TourID in "tour_image" TABLE
            \DB::table('tour_images')
                    ->where('temp_id', $request->temp_id)
                    ->update(['tour_id' => $tourId]);
            if ($tourData['id'] > 0) {
                return redirect()->route('tour.detail', $tourData['id']);
            } else {
                return redirect()->route('tour.index');
            }
        }

        return redirect()->route('tour.index');
    }

    /*
     * Fetch Tour Images
     */

    public function dropzonImageFetch($id, Request $request) {
        if ($request->ajax()) {
            $images = \DB::table('tour_images')
                            ->where('tour_id', $id)->get();

            $imageAnswer = [];
            foreach ($images as $image) {
                $imageAnswer[] = [
                    'temp_id' => $image->temp_id,
                    'original' => $image->original_name,
                    'server' => \App\Helpers\S3url::s3_get_image($image->filename),
                    'size' => $image->file_size
                ];
            }

            return response()->json([
                        'images' => $imageAnswer
            ]);
        }
    }

    /*
     * Store Tour Multiple images
     */

    public function dropzonImageSave(Request $request) {
        if ($request->ajax()) {
            $photos_path = "tours/";
            $photos = $request->file('file');

            if (!is_array($photos)) {
                $photos = [$photos];
            }

            if (!is_dir($photos_path)) {
                mkdir($photos_path, 0777);
            }

            for ($i = 0; $i < count($photos); $i++) {
                $photo = $photos[$i];
                $name = sha1(date('YmdHis') . str_random(30));
                $save_name = $name . '.' . $photo->getClientOriginalExtension();
                $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

//                Image::make($photo)
//                        ->resize(250, null, function ($constraints) {
//                            $constraints->aspectRatio();
//                        })
//                        ->save($photos_path . '/' . $resize_name);
//                $photo->move($photos_path, $save_name);
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        $photos_path . $save_name, file_get_contents($photo->getRealPath())
                );

                $upload = new \App\Tourimages();
                $upload->temp_id = $request->tour_temp_id;
                $upload->filename = $photos_path . $save_name;
                $upload->resized_name = $photos_path . $resize_name;
                $upload->original_name = basename($photo->getClientOriginalName());
                $upload->file_size = $photo->getClientSize();
                $upload->save();
            }
            return Response::json([
                        'message' => 'Image saved Successfully'
                            ], 200);
        }
    }

    public function dropzonImageDelete(Request $request) {
        if ($request->ajax()) {
            $photos_path = "tours/";
            $filename = $request->id;
            $tour_temp_id = $request->tour_temp_id;
            $uploaded_image = \DB::table('tour_images')
                    ->where('original_name', basename($filename))
                    //->where('temp_id', $tour_temp_id)
                    ->first();

            if (empty($uploaded_image)) {
                return Response::json(['message' => 'Sorry file does not exist'], 400);
            }

            $file_path = $uploaded_image->filename;
            $resized_file = \App\Helpers\S3url::s3_get_image($uploaded_image->resized_name);


            if (\App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->has($file_path)) {
                \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->delete($file_path);
            }

//            if (file_exists($resized_file)) {
//                unlink($resized_file);
//            }

            if (!empty($uploaded_image)) {
                \DB::table('tour_images')->where('id', $uploaded_image->id)->delete();
            }

            return Response::json(['message' => 'File successfully delete'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array();
        return view('admin/tour/tour_view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour $tour) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Tour $tour) {
    //     $data = array("deleted" => true);
    //     return view('admin/tour/tour', $data);
    // }

    public function destroy($id) {
        $response['success'] = false;
        $deleted = $this->getTourLib()->deleteTour($id);
        if ($deleted) {
            $this->getTourCostLib()->deleteTour($id);
            $this->getTourCostAmountLib()->deleteTour($id);
            $this->getTourDepartureLib()->deleteTour($id);
            $this->getTourFAQLib()->deleteTour($id);
            $this->getTourHistoryLib()->deleteTour($id);
            $this->getTourHotelLib()->deleteTour($id);
            $this->getTourItineraryLib()->deleteTour($id);
            $response['success'] = true;
            $response['message'] = 'Tour has been deleted successfully!';
            $response['msg_status'] = 'success';
        } else {
            $response['message'] = "Tour can't deleted successfully!";
            $response['msg_status'] = 'warning';
        }
        exit(json_encode($response));
    }

    ////////////////////////////////////////////////////// Tour Itinerary //////////////////////////////////////////////////////////////////////

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function itinerary_store(Request $request) {
        $tourData = $request->all();
        for ($k = 0; $k < count($tourData['tour']['tour_day']); $k++) {
            if (!isset($tourData['cnt'][$k])) {
                continue;
            }
            $cnt = $tourData['cnt'][$k];
            $itineraryImageName = "";
            if (isset($tourData['image'][$k])) {
                $itineraryImg = $tourData['image'][$k];
                if ($itineraryImg) {
                    $itineraryImageName = time() . rand(11, 999) . '.' . $itineraryImg->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/itinerary');
                    $itineraryImg->move($destinationPath, $itineraryImageName);
//                @unlink(public_path('/uploads/itinerary/') . $request->old_image . $cnt);
                }
            }
            $data = array(
                'tour_id' => $tourData['id'],
                'tour_day' => $tourData['tour']['tour_day'][$k],
                'title' => $tourData['tour']['title'][$k],
                'description' => $tourData['tour']['description'][$k],
                'image' => $itineraryImageName,
            );
            $this->getTourItineraryLib()->addTourInerary($data);
        }
        $historyData = array('tour_id' => $tourData['id'], 'message' => 'Tour itinerary added.', 'type' => 'add');
        $this->getTourHistoryLib()->addTourHistory($historyData);
        Session::flash('success_msg', 'Tour itinerary has been added successfully!');
        return redirect()->route('tour.detail', $tourData['id'] . "#itinerary");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function itinerary_destroy($id) {
        $itinerary = $this->getTourItineraryLib()->getTourIneraryById($id);
        $deleted = $this->getTourItineraryLib()->deleteTourInerary($id);
        if ($deleted) {
            $historyData = array('tour_id' => $itinerary['tour_id'], 'message' => 'Tour itinerary deleted.', 'type' => 'remove');
            $this->getTourHistoryLib()->addTourHistory($historyData);
            echo json_encode(array("status" => "ok"));
        } else {
            echo json_encode(array("status" => "error"));
        }
    }

    ///////////////////////////////////////////// Tour departure date /////////////////////////////////////////

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function departure_store(Request $request) {
        $tourData = $request->all();
        $tour_departure = $this->getTourDepartureLib()->getTourDeparture(array('id', 'departure_date'), array('tour_id' => $tourData['tourId']), $tourData['currentYear']);
        $yearDate = array();
        foreach ($tour_departure As $departure) {
            $yearDate[$departure['departure_date']] = $departure['id'];
        }
        foreach ($tourData['tourDepartureDate'] as $months) {
            foreach ($months as $date) {
                $tourDeptDetesAD = substr($date, 0, 24);
                $tourDeparturedate = date('Y-m-d', strtotime($tourDeptDetesAD));
                if (array_key_exists($tourDeparturedate, $yearDate)) {
                    unset($yearDate[$tourDeparturedate]);
                    continue;
                }
                $dateData = array('tour_id' => $tourData['tourId'], 'departure_date' => $tourDeparturedate);
                $this->getTourDepartureLib()->addTourDeparture($dateData);
            }
        }
        foreach ($yearDate as $removedate) {
            $this->getTourDepartureLib()->deleteTourDeparture($removedate);
        }
        echo json_encode(array('success' => "ok"));
    }

    function get_departuredates($currentYear, $tourId) {
        $tour_departure = $this->getTourDepartureLib()->getTourDeparture(array('departure_date'), array('tour_id' => $tourId), $currentYear);
        $dateArray['dateList'] = array();
        foreach ($tour_departure As $departure) {
            $dateArray['dateList'][] = $departure['departure_date'];
        }
        echo json_encode($dateArray);
    }

    ///////////////////////////////////////// Tour FAQ /////////////////////////////////////////////////////
    public function faq_store(Request $request) {
        $rules = [
            'question' => 'required',
            'answer' => 'required',
        ];

        $messages = [
            'question' => 'Please enter question',
            'answer' => 'Please enter answer',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('tour.detail', $request->id . "#faq")->withErrors($validator)->withInput();
            }
        } else {
            //get sector data
            $tourData = $request->all();

            $data = array(
                'tour_id' => $tourData['id'],
                'question' => $tourData['question'],
                'answer' => $tourData['answer'],
                'status' => $tourData['status'],
            );

            if (isset($tourData['faq_id']) && $tourData['faq_id'] > 0) {
                $this->getTourFAQLib()->updateTourFAQ($data, $tourData['faq_id']);
                $historyData = array('tour_id' => $tourData['id'], 'message' => 'Tour FAQ updated.', 'type' => 'done');
                $this->getTourHistoryLib()->addTourHistory($historyData);
                Session::flash('success_msg', 'Tour FAQ has been updated successfully!');
                return redirect()->route('tour.detail', $tourData['id'] . "#faq");
            } else {
                $tourId = $this->getTourFAQLib()->addTourFAQ($data);
                $historyData = array('tour_id' => $tourData['id'], 'message' => 'Tour FAQ added.', 'type' => 'add');
                $this->getTourHistoryLib()->addTourHistory($historyData);
                Session::flash('success_msg', 'Tour FAQ has been saved successfully!');
                return redirect()->route('tour.detail', $tourData['id'] . "#faq");
            }
        }

        return redirect()->route('tour.index');
    }

    public function faq_destroy($id) {
        $itinerary = $this->getTourFAQLib()->getTourFAQById($id);
        $deleted = $this->getTourFAQLib()->deleteTourFAQ($id);
        if ($deleted) {
            $historyData = array('tour_id' => $itinerary['tour_id'], 'message' => 'Tour FAQ deleted.', 'type' => 'remove');
            $this->getTourHistoryLib()->addTourHistory($historyData);
            echo json_encode(array("status" => "ok"));
        } else {
            echo json_encode(array("status" => "error"));
        }
    }

    public function faq_detail($id) {
        $faq = $this->getTourFAQLib()->getTourFAQById($id);
        if ($faq) {
            echo json_encode($faq);
        } else {
            echo json_encode(array());
        }
    }

    ///////////////////   Tour Cost /////////////////////////////////////////////

    public function cost_manage($id) {
        $data = array();
        $data['tour'] = $this->getTourLib()->getTourById($id);
        $data['tour_rate_master'] = $this->getRatetypemasterLib()->getRatetypemaster(array('typeId', 'name'));
        $data['tour_package_type'] = $this->getTourpackagetypemasterLib()->getTourpackagetypemaster(array('typeId', 'name'));
        $data['currencys'] = $this->getCurrencymasterLib()->getCurrencymaster(array('currId', 'currency_name'));

        return view('admin/tour/tour-cost-manage', $data);
    }

    public function cost_store(Request $request) {
        $tourData = $request->all();

        $c = 0;

        foreach ($tourData['date_from'] as $dates) {
            $data = array(
                'tour_id' => $tourData['id'],
                'from_date' => date('Y-m-d', strtotime($tourData['date_from'][$c])),
                'to_date' => date('Y-m-d', strtotime($tourData['date_to'][$c])),
                'discount' => $tourData['discount'][$c],
                'currency_id' => $tourData['currency_id'][$c],
            );
            $costId = $this->getTourCostLib()->addTourCost($data);

            $cLoop = '__' . $c;
            foreach ($tourData['cost'][$cLoop] as $k => $cost) {
                $ratePackageIds = explode('-', $k);
                $tourCost = $cost[0];
                $costData = array(
                    'cost_id' => $costId,
                    'tour_id' => $tourData['id'],
                    'rate_id' => $ratePackageIds[0],
                    'package_type_id' => $ratePackageIds[1],
                    'cost' => $tourCost,
                    'sell' => $tourData['sell'][$cLoop][$k],
                );
                $this->getTourCostAmountLib()->addTourCostAmount($costData);
            }
            $c++;
        }
        $historyData = array('tour_id' => $tourData['id'], 'message' => 'Tour Cost added successfully.', 'type' => 'add');
        $this->getTourHistoryLib()->addTourHistory($historyData);
        Session::flash('success_msg', 'Tour Cost has been added successfully!');
        return redirect()->route('tour.detail', $tourData['id'] . "#cost");
    }

    public function cost_update(Request $request) {
        $tourData = $request->all();
        $costId = $tourData['cost_id'];


        $data = array(
            'tour_id' => $tourData['id'],
            'from_date' => date('Y-m-d', strtotime($tourData['date_from'][$costId])),
            'to_date' => date('Y-m-d', strtotime($tourData['date_to'][$costId])),
            'discount' => $tourData['discount'][$costId],
            'currency_id' => $tourData['currency_id'][$costId],

        );
        $this->getTourCostLib()->updateTourCost($data, $costId);

        foreach ($tourData['cost'] as $k => $cost) {
            $costData = array(
                'cost' => $cost,
                'sell' => $tourData['sell'][$k],
            );
            $this->getTourCostAmountLib()->updateTourCostAmount($costData, $k);
        }
        if (isset($tourData['cost_new'])) {
            foreach ($tourData['cost_new'] as $k => $cost) {
                $ratePackageIds = explode('-', $k);
                $costData = array(
                    'cost_id' => $costId,
                    'tour_id' => $tourData['id'],
                    'rate_id' => $ratePackageIds[0],
                    'package_type_id' => $ratePackageIds[1],
                    'cost' => $cost,
                    'sell' => $tourData['sell_new'][$k],
                );
                $this->getTourCostAmountLib()->addTourCostAmount($costData);
            }
        }
        $historyData = array('tour_id' => $tourData['id'], 'message' => 'Tour Cost updated successfully.', 'type' => 'done');
        $this->getTourHistoryLib()->addTourHistory($historyData);
        Session::flash('success_msg', 'Tour Cost has been updated successfully!');
        return redirect()->route('tour.detail', $tourData['id'] . "#cost");
    }

    public function cost_edit($tourId, $costId) {
        $data = array();
        $data['tour'] = $this->getTourLib()->getTourById($tourId);
        $data['cost'] = $this->getTourCostLib()->getTourCost(array('id', 'from_date', 'to_date', 'discount', 'currency_id'), array('id' => $costId));
        $data['amount'] = $this->getTourCostAmountLib()->getTourCostAmount(array('id', 'cost_id', 'tour_id', 'rate_id', 'package_type_id', 'cost', 'sell'), array('cost_id' => $costId));

        $data['tour_rate_master'] = $this->getRatetypemasterLib()->getRatetypemaster(array('typeId', 'name'));
        $data['tour_package_type'] = $this->getTourpackagetypemasterLib()->getTourpackagetypemaster(array('typeId', 'name'));
        $data['currencys'] = $this->getCurrencymasterLib()->getCurrencymaster(array('currId', 'currency_name'));


        $tourCostData = array();
        foreach ($data['amount'] as $key => $amount) {
            $tourCostData[$amount->rate_id][$amount->package_type_id] = array('cost' => $amount->cost, "sell" => $amount->sell, 'id' => $amount->id);
        }
        $data['tourCostData'] = $tourCostData;
        return view('admin/tour/tour-cost-edit', $data);
    }

    public function cost_destroy($id) {
        $cost = $this->getTourCostLib()->getTourCostById($id);
        $deleted = $this->getTourCostLib()->deleteTourCost($id);
        if ($deleted) {
            $this->getTourCostAmountLib()->deleteTourCostAmountByCost($id);
            $historyData = array('tour_id' => $cost['tour_id'], 'message' => 'Tour Cost deleted.', 'type' => 'remove');
            $this->getTourHistoryLib()->addTourHistory($historyData);
            echo json_encode(array("status" => "ok"));
        } else {
            echo json_encode(array("status" => "error"));
        }
    }

    ////////////////////////////////////////////////////// Tour Hotel //////////////////////////////////////////////////////////////////////

    public function hotel_store(Request $request) {
        $tourData = $request->all();
        if (isset($tourData['update']) && count($tourData['update']) > 0) {
            $hotels = $this->getTourHotelLib()->getTourHotel(array('id'), array('tour_id' => $tourData['id']));
            $hotelRemove = array();
            foreach ($hotels as $tHot) {
                $hotelRemove[] = $tHot->id;
            }
            foreach ($tourData['update'] as $hotelId => $thotel) {
                $data = array(
                    'tour_id' => $tourData['id'],
                    'location' => $thotel['location'],
                    'hotel' => $thotel['hotel'],
                    'hotel_star' => $thotel['hotel_star'],
                    'night' => $thotel['night'],
                    'package_type' => $thotel['package_type'],
                );
                if ($thotel['location'] != '' || $thotel['hotel']) {
                    $this->getTourHotelLib()->updateTourHotel($data, $hotelId);
                }
                unset($hotelRemove[array_search($hotelId, $hotelRemove)]);
            }
//            $historyData = array('tour_id' => $tourData['id'], 'message' => 'Tour hotel updated', 'type' => 'add');
//            $this->getTourHistoryLib()->addTourHistory($historyData);
        }
        $addedh = 0;
        for ($k = 0; $k < count($tourData['package_type']); $k++) {
            $data = array(
                'tour_id' => $tourData['id'],
                'location' => $tourData['location'][$k],
                'hotel' => $tourData['hotel'][$k],
                'hotel_star' => $tourData['hotel_star'][$k],
                'night' => $tourData['night'][$k],
                'package_type' => $tourData['package_type'][$k],
            );
            if ($tourData['location'][$k] != '' || $tourData['hotel'][$k]) {
                $this->getTourHotelLib()->addTourHotel($data);
                $addedh++;
            }
        }
        if (isset($hotelRemove) && count($hotelRemove)) {
            foreach ($hotelRemove as $hotelId) {
                $this->getTourHotelLib()->deleteTourHotel($hotelId);
            }
            $historyData = array('tour_id' => $cost['tour_id'], 'message' => 'Tour hotel deleted', 'type' => 'remove');
            $this->getTourHistoryLib()->addTourHistory($historyData);
        }
        if ($addedh > 0) {
            $historyData = array('tour_id' => $tourData['id'], 'message' => 'Tour hotel added', 'type' => 'add');
            $this->getTourHistoryLib()->addTourHistory($historyData);
        }
        Session::flash('success_msg', 'Tour hotel has been added successfully!');
        return redirect()->route('tour.detail', $tourData['id'] . "#hotel");
    }

    /////////////////////////// Tour Quotation ///////////////

//     public function tour_quotation_data(Request $request) {
//         if ($request->ajax()) {
//             $query = Leadquatation::select('lead_quatation.quaId', 'lead_quatation.quatation_date', 'lead_quatation.validup_date', 'lead_quatation.subject', 'lead_quatation.adult', 'lead_quatation.child', 'lead_quatation.infant', 'lead_quatation.total_amount', 'lead_quatation.created_by', 'lead_quatation.status_id', 'lead_quatation.lead_id')->where('lead_quatation.deleted_at', null);
//             if (\Auth::user()->role_id == 3 || \Auth::user()->role_id == 4) {
//                 $query->where('created_by', Auth::user()->id);
//             }
//             if (\Auth::user()->role_id == 2) {
// //                $query->whereIn('sector', explode(',', \Session::get('sector_id')));
//             }
//             $query->leftJoin('lead_quatation_perticular', 'lead_quatation.quaId', '=', 'lead_quatation_perticular.quotation_id');
//             $query->where('lead_quatation_perticular.item_id', $request->tourId);
//             $query->groupBy('lead_quatation.quaId');
//             return datatables()->of($query)
//                             ->editColumn('status_id', function ($model) {
//                                 return $this->getCommonLib()->getQuotationStatus($model->status_id);
//                             })
//                             ->editColumn('quatation_date', function ($model) {
//                                 return date('d M Y', strtotime($model->quatation_date));
//                             })
//                             ->editColumn('validup_date', function ($model) {
//                                 return date('d M Y', strtotime($model->validup_date));
//                             })
//                             ->editColumn('adult', function ($model) {
//                                 $str_member = '';
//                                 if (isset($model->adult)) {
//                                     $str_member .= $model->adult . '/';
//                                 } else {
//                                     $str_member .= '0/';
//                                 }
//                                 if (isset($model->child)) {
//                                     $str_member .= $model->child . '/';
//                                 } else {
//                                     $str_member .= '0/';
//                                 }
//                                 if (isset($model->infant)) {
//                                     $str_member .= $model->infant;
//                                 } else {
//                                     $str_member .= '0';
//                                 }
//                                 return $str_member;
//                             })
//                             ->filterColumn('lead_quatation.quatation_date', function($query, $keyword) {
//                                 $search_date = date('Y-m-d', strtotime($keyword));
//                                 $query->whereRaw("DATE_FORMAT(lead_quatation.quatation_date, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
//                             })
//                             ->filterColumn('lead_quatation.validup_date', function($query, $keyword) {
//                                 $search_date = date('Y-m-d', strtotime($keyword));
//                                 $query->whereRaw("DATE_FORMAT(lead_quatation.validup_date, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
//                             })
//                             ->addColumn('action', '')
// //                            ->rawColumns(['description', 'status', 'action'])
//                             ->toJson();
//         }
//     }

    public function tour_quotation_data(Request $request) {
        if ($request->ajax()) {
            $query = Leadquatation::select('lead_quatation.quaId',
                'lead_quatation.quatation_id',
                'lead_quatation.quatation_date', 
                'lead_quatation.validup_date', 
                'lead_quatation.subject',
                DB::raw("CONCAT(lead_quatation.adult,'/',lead_quatation.child,'/',lead_quatation.infant) as members"),
                DB::raw("FORMAT(lead_quatation.total_amount,2) as total_amount"),   
                'lead_quatation.created_by', 
                'lead_quatation.status_id', 
                'lead_quatation.lead_id')->where('lead_quatation.deleted_at', null);
                if (\Auth::user()->role_id == 3 || \Auth::user()->role_id == 4) {
                    $query->where('created_by', Auth::user()->id);
                }
                $query->leftJoin('lead_quatation_perticular', 'lead_quatation.quaId', '=', 'lead_quatation_perticular.quotation_id');
                $query->where('lead_quatation_perticular.item_id', $request->tourId);
                $query->groupBy('lead_quatation.quaId');
                $query->orderBy('lead_quatation.quaId', 'DESC');
            return datatables()->of($query)
                ->editColumn('status_id', function ($model) {
                    return $this->getCommonLib()->getQuotationStatus($model->status_id);
                })
                ->editColumn('quatation_date', function ($model) {
                    return date('d M Y', strtotime($model->quatation_date));
                })
                ->editColumn('validup_date', function ($model) {
                    return date('d M Y', strtotime($model->validup_date));
                })
                ->filterColumn('quatation_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(lead_quatation.quatation_date,'%d %b %Y') like ?", ["%$keyword%"]);
                })
                ->filterColumn('validup_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(lead_quatation.validup_date,'%d %b %Y') like ?", ["%$keyword%"]);
                })
                ->filterColumn('members', function($query, $keyword) {
                    $query->whereRaw("CONCAT(lead_quatation.adult,'/',lead_quatation.child,'/',lead_quatation.infant) like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('total_amount', function($query, $keyword) {
                    $query->whereRaw("FORMAT(lead_quatation.total_amount,2) like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('status_id', function ($query, $keyword) use ($request) {
                    if (isset($request['search']) && $request['search']['value'] != '') {
                        $keyword = $request['search']['value'];
                    }
                    $quotationStatus = $this->getCommonLib()->getQuotationStatus($keyword, 1);
                    if(count($quotationStatus) >0){
                        $query->where("status_id",implode(',',$quotationStatus), 'in',false );
                    }
                })
                ->addColumn('action', '')
                ->toJson();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function hotel(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-hotel', $data);
    }

    public function departure(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-departure', $data);
    }

    public function graphics(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-graphics', $data);
    }

    public function discount_list(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-discount-list', $data);
    }

    public function discount_create(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-discount-manage', $data);
    }

    public function discount_edit(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-discount-manage', $data);
    }

    public function discount_destroy(Tour $tour) {
        $data = array("deleted" => true);
        return view('admin/tour/tour-discount-list', $data);
    }

    public function details(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-details', $data);
    }

    public function payment(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-payment', $data);
    }

    public function hotel_chart(Tour $tour) {
        $data = array();
        return view('admin/tour/tour-hotel-chart', $data);
    }

    public function itinerary($id) {
        $data = array();
        return view('admin/tour/itinerary_manage', $data);
    }

    public function price_type() {
        $data = array();
        return view('admin/tour/price_type', $data);
    }

    public function price_type_add() {
        $data = array();
        return view('admin/tour/price_type_manage', $data);
    }

    public function price_type_edit($id) {
        $data = array();
        return view('admin/tour/price_type_manage', $data);
    }

    public function price_type_destroy($id) {
        $data = array("deleted" => true);
        return view('admin/tour/price_type', $data);
    }

    ///////////////////////////////////// Tour hotel feature master //////////////////////////////////////////////////////////
    public function feature_list() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Tour Features'
            )
        );
        return view('admin/tour/feature_list', $data);
    }

    public function feature_list_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Featuretour::select('typeId', 'name', 'description', 'status')
                                    ->where('deleted_at', null)
                            )
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 250, "...");
                            })
                            ->addColumn('action', '')
                            //->rawColumns(['status', 'action'])
                            ->toJson();
        }
    }

    public function feature_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.features'),
                'title' => 'Manage Tour Features'
            ),
            array(
                'title' => 'Add Tour Features'
            )
        );
        return view('admin/tour/feature_manage', $data);
    }

    public function feature_edit($id) {
        $data['type'] = $this->getFeaturetourmasterLib()->getFeaturetourmasterById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.features'),
                'title' => 'Manage Tour Features'
            ),
            array(
                'title' => 'Edit Tour Features'
            )
        );
        return view('admin/tour/feature_manage', $data);
    }

    /*
     * Tour feature save
     */

    public function feature_store(Request $request) {
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter feature tour',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('tour.features.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('tour.features.add')->withErrors($validator)->withInput();
            }
        } else {
            //set Hotel type Data
            $data['name'] = $request->name;
            $data['description'] = $request->description;
            $data['status'] = $request->status;

            if ($request->id > 0) {
                $this->getFeaturetourmasterLib()->updateFeaturetourmaster($data, $request->id);
                Session::flash('success_msg', 'Feature tour has been updated successfully!');
            } else {
                $typeId = $this->getFeaturetourmasterLib()->addFeaturetourmaster($data);
                Session::flash('success_msg', 'Feature tour has been saved successfully!');
            }
        }
        return redirect()->route('tour.features');
    }

    public function feature_destroy($id) {
        $deleted = $this->getFeaturetourmasterLib()->deleteFeaturetourmaster($id);
        if ($deleted) {
            Session::flash('success_msg', 'Feature tour has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Feature tour can't deleted!");
        }
        return redirect()->route('tour.features');
    }

    ////////////////////////////////// Tour Type master //////////////////////////////////////////////////
    /*
     * Tour type Listing Page
     */

    public function tour_type() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Tour Type'
            )
        );
        return view('admin/tour/toure_list', $data);
    }

    /*
     * Tour type Listing Page
     */

    public function tour_type_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.type.index'),
                'title' => 'Manage Tour Type'
            ),
            array(
                'title' => 'Add Tour Type'
            )
        );
        return view('admin/tour/toure_type_edit', $data);
    }

    /*
     * Tour type Data In JSON
     */

    public function tour_type_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(Tourtype::select('typeId', 'name', 'description', 'status')
                                    ->where('deleted_at', null)
                            )
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 250, "...");
                            })
                            //->rawColumns(['status', 'action'])
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /*
     * Tour type edit
     */

    public function tour_type_edit($id) {
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtypeById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.type.index'),
                'title' => 'Manage Tour Type'
            ),
            array(
                'title' => 'Edit Tour Type'
            )
        );

        return view('admin/tour/toure_type_edit', $data);
    }

    /*
     * Tour Type store IN DB
     */

    public function tour_type_store(Request $request) {
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter hotel type',
            'status.required' => 'Please select currency status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('tour.type.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('tour.type.add')->withErrors($validator)->withInput();
            }
        } else {
            //set Hotel type Data
            $data['name'] = $request->name;
            $data['description'] = $request->description;
            $data['status'] = $request->status;

            $image = $request->file('logo');
            if (!empty($image)) {
                $imagename = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/tour-category');
                $image->move($destinationPath, $imagename);
                $data['logo'] = $imagename;
            }
            if ($request->id > 0) {
                $this->getTourtypemasterLib()->updateTourtype($data, $request->id);
                Session::flash('success_msg', 'Tour type has been updated successfully!');
            } else {
                $typeId = $this->getTourtypemasterLib()->addTourtype($data);
                Session::flash('success_msg', 'Tour type has been saved successfully!');
            }
        }
        return redirect()->route('tour.type.index');
    }

    /*
     * Tour type delete
     */

    public function tour_destroy($id) {
        $deleted = $this->getTourtypemasterLib()->deleteTourtype($id);
        if ($deleted) {
            Session::flash('success_msg', 'Tour type has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Tour type can't deleted!");
        }
        return redirect()->route('tour.type.index');
    }

    //////////////////////////// tour category master ////////////////////////////////////////////////////////////////////////
    /*
     * Tour Category list
     */

    public function tour_category() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Tour Package Type'
            )
        );
        return view('admin/tour/toure_category_list', $data);
    }

    /*
     * Tour Category data in JSON
     */

    public function tour_category_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(Tourpackagetype::select('typeId', 'name', 'description', 'status')
                                    ->where('deleted_at', null)
                            )
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 250, "...");
                            })
                            ->addColumn('action', '')
                            //->rawColumns(['status', 'action'])
                            ->toJson();
        }
    }

    /*
     * Tour Category add
     */

    public function tour_category_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.category.index'),
                'title' => 'Manage Tour Package Type'
            ),
            array(
                'title' => 'Add Package Type'
            )
        );
        return view('admin/tour/toure_category_manage', $data);
    }

    /*
     * Tour Category edit
     */

    public function tour_category_edit($id) {
        $data['type'] = $this->getTourpackagetypemasterLib()->getTourpackagetypemasterById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.category.index'),
                'title' => 'Manage Tour Package Type'
            ),
            array(
                'title' => 'Edit Package Type'
            )
        );
        return view('admin/tour/toure_category_manage', $data);
    }

    /*
     * Tour Category save
     */

    public function tour_category_store(Request $request) {
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter tour package type',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('tour.category.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('tour.category.add')->withErrors($validator)->withInput();
            }
        } else {
            $tourpackageData = array();
            $tourpackageData['name'] = $request->name;
            $tourpackageData['description'] = $request->description;
            $tourpackageData['status'] = $request->status;
            if (isset($request->id)) {
                $id = $request->id;
                $this->getTourpackagetypemasterLib()->updateTourpackagetypemaster($tourpackageData, $id);
                Session::flash('success_msg', 'Tour package type has been updated successfully!');
            } else {
                $this->getTourpackagetypemasterLib()->addTourpackagetypemaster($tourpackageData);
                Session::flash('success_msg', 'Tour package type has been saved successfully!');
            }
            return redirect(route('tour.category.index'));
        }
    }

    /*
     * Tour Category edit
     */

    public function tour_category_destroy($id) {
        $deleted = $this->getTourpackagetypemasterLib()->deleteTourpackagetypemaster($id);
        if ($deleted) {
            Session::flash('success_msg', 'Tour package type has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Tour package type can't deleted!");
        }
        return redirect(route('tour.category.index'));
    }

    public function tour_booking() {
        $data = array();
        return view('admin/tour/tour_booking', $data);
    }

    /*
     * Read tour name for autocomplate
     */

    public function read_name(Request $request) {
        if ($request->ajax()) {
            $html = '';
            $keyword = isset($request->keyword) ? trim($request->keyword) : '';
            $tours = $this->getTourLib()->getTournameAutocomplate(array('id', 'tour_name'), $keyword);
            if (count($tours) > 0) {
                $html .= '<ul id="country-list">';
                foreach ($tours as $tour) {
                    $html .= '<li onClick="selectTour(\'' . $tour->tour_name . '\', ' . $tour->id . ');">' . $tour->tour_name . '</li>';
                }
                $html .= '</ul>';
            }
            echo $html;
            die;
        }
    }

    public function read_name_json(Request $request) {
        if ($request->ajax()) {
            $html = '';
            $keyword = isset($request->keyword) ? trim($request->keyword) : '';
            $tours = $this->getTourLib()->getTournameAutocomplate(array('id', 'tour_name'), $keyword);
            if (count($tours) > 0) {
                $html .= '<ul id="country-list">';
                foreach ($tours as $tour) {
                    $html .= '<li onClick="selectTourDynamic(this,\'' . $tour->tour_name . '\', ' . $tour->id . ',\'tour\');">' . $tour->tour_name . '</li>';
                }
                $html .= '</ul>';
            }
            echo $html;
            die;
        }
    }

    /*
     * domestic holiday
     */
    public function domestic_holiday_list() {
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Domestic Holidays'
            )
        );
        return view('admin/domestic_holiday/index', $data);
    }

    public function domestic_holiday_list_data(Request $request) {
        return datatables()->of(
            DomesticHoliday::select('domestic_holidays.id',
                'domestic_holidays.tour_id',
                'domestic_holidays.title',
                'domestic_holidays.status',
                'tour.tour_name')
            ->leftjoin('tour', 'tour.id', '=', 'domestic_holidays.tour_id')
            ->where('domestic_holidays.deleted_at', null)
            )
            ->filterColumn('tour_name', function($query, $keyword) {
                $query->whereRaw("tour_name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', '')
            ->toJson();
    }

    public function domestic_holiday_create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.domestic-holidays'),
                'title' => 'Manage Domestic Holidays'
            ),
            array(
                'title' => 'Add Domestic Holiday'
            )
        );
        $data['tour'] = $this->getTourLib()->getAll_DomesticTourData();
        //$data['tour'] = $this->getTourLib()->getAllTourData(1);
        return view('admin/domestic_holiday/create', $data);
    }

    public function domestic_holiday_store(Request $request) {
        $rules = [
            'title'       => 'required',
            'tour_id'     => 'required'
        ];

        $messages = [
            'title.required'      => 'Please enter title',
            'tour_id.required'    => 'Please select tour'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('tour.domestic-holidays.add')->withErrors($validator)->withInput();
        } else {
            $end_date=null;
            if($request->end_date!=null){
                $end_date=date('Y-m-d', strtotime($request->end_date));
            }

            $start_date=null;
            if($request->start_date!=null){
                $start_date=date('Y-m-d', strtotime($request->start_date));
            }
            
            $records = array(
                "title"               => $request->title,
                "tour_id"             => $request->tour_id,
                "start_date"          => $start_date,
                "end_date"            => $end_date,
                "status"              => $request->status
            );
            $last_id = $this->getDomesticHolidayLib()->addDomesticHoliday($records);
            if($last_id){
                Session::flash('success_msg', 'Domestic holiday inserted successfully.');   
            }else{
                Session::flash('error_msg', 'Domestic holiday inserted unsuccessfully.');
            }
            return redirect()->route('tour.domestic-holidays');
        }
    }

    public function domestic_holiday_destroy($id) {
        $deleted = $this->getDomesticHolidayLib()->deleteDomesticHoliday($id);
        if ($deleted) {
            Session::flash('success_msg', 'Domestic Holiday has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Domestic Holiday can't deleted!");
        }
        return redirect()->route('tour.domestic-holidays');
    }

    public function domestic_holiday_edit($id) {
        $data['domestic_data']  =   $this->getDomesticHolidayLib()->getDomesticHolidayById($id);
        //$data['tour'] = $this->getTourLib()->getAllTourData(1);
        $data['tour'] = $this->getTourLib()->getAll_DomesticTourData();
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.domestic-holidays'),
                'title' => 'Manage Domestic Holidays'
            ),
            array(
                'title' => 'Edit Domestic Holiday'
            )
        );
        return view('admin/domestic_holiday/domestic-holiday-edit', $data); 
    }

    public function domestic_holiday_update(Request $request) {
        if(isset($request->id)){
            $rules = [
                'title'       => 'required',
                'tour_id'     => 'required'
            ];

            $messages = [
                'title.required'      => 'Please enter title',
                'tour_id.required'    => 'Please select tour'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->route('tour.domestic-holidays.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                $end_date=null;
                if($request->end_date!=null){
                    $end_date=date('Y-m-d', strtotime($request->end_date));
                }

                $start_date=null;
                if($request->start_date!=null){
                    $start_date=date('Y-m-d', strtotime($request->start_date));
                }

                $records = array(
                    "title"               => $request->title,
                    "tour_id"             => $request->tour_id,
                    "start_date"          => $start_date,
                    "end_date"            => $end_date,
                    "status"              => $request->status
                );
                $update=$this->getDomesticHolidayLib()->updateDomesticHoliday($records, $request->id);
                if($update){
                    Session::flash('success_msg', 'Domestic holiday updated successfully.');   
                }else{
                    Session::flash('error_msg', 'Domestic holiday updated unsuccessfully.');
                }
                return redirect()->route('tour.domestic-holidays');
            }
        }else{
            return redirect()->route('tour.domestic-holidays');
        }

    }

    /*
     * international holiday
     */

    public function international_holiday_list() {
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage International Holidays'
            )
        );
        return view('admin/international_holiday/index', $data);
    }

    public function international_holiday_list_data(Request $request) {
        return datatables()->of(
            InternationalHoliday::select('international_holidays.id',
                'international_holidays.tour_id',
                'international_holidays.title',
                'international_holidays.status',
                'tour.tour_name')
            ->leftjoin('tour', 'tour.id', '=', 'international_holidays.tour_id')
            ->where('international_holidays.deleted_at', null)
            )
            ->filterColumn('tour_name', function($query, $keyword) {
                $query->whereRaw("tour_name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', '')
            ->toJson();
    }

    public function international_holiday_create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.international-holidays'),
                'title' => 'Manage International Holidays'
            ),
            array(
                'title' => 'Add International Holiday'
            )
        );
        //$data['tour'] = $this->getTourLib()->getAllTourData(2);
        $data['tour'] = $this->getTourLib()->getAll_InternationalTourData();
        //dd($data);
        return view('admin/international_holiday/create', $data);
    }

    public function international_holiday_store(Request $request) {
        $rules = [
            'title'       => 'required',
            'tour_id'     => 'required'
        ];

        $messages = [
            'title.required'      => 'Please enter title',
            'tour_id.required'    => 'Please select tour'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('tour.international-holidays.add')->withErrors($validator)->withInput();
        } else {
            // $records = array(
            //     "title"               => $request->title,
            //     "tour_id"             => $request->tour_id,
            //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
            //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
            //     "status"              => $request->status
            // );

            $end_date=null;
            if($request->end_date!=null){
                $end_date=date('Y-m-d', strtotime($request->end_date));
            }

            $start_date=null;
            if($request->start_date!=null){
                $start_date=date('Y-m-d', strtotime($request->start_date));
            }

            $records = array(
                "title"               => $request->title,
                "tour_id"             => $request->tour_id,
                "start_date"          => $start_date,
                "end_date"            => $end_date,
                "status"              => $request->status
            );

            $last_id = $this->getInternationalHolidayLib()->addInternationalHoliday($records);
            if($last_id){
                Session::flash('success_msg', 'International holiday inserted successfully.');   
            }else{
                Session::flash('error_msg', 'International holiday inserted unsuccessfully.');
            }
            return redirect()->route('tour.international-holidays');
        }
    }

    public function international_holiday_destroy($id) {
        $deleted = $this->getInternationalHolidayLib()->deleteInternationalHoliday($id);
        if ($deleted) {
            Session::flash('success_msg', 'International Holiday has been deleted successfully!');
        } else {
            Session::flash('error_msg', "International Holiday can't deleted!");
        }
        return redirect()->route('tour.international-holidays');
    }

    public function international_holiday_edit($id) {
        $data['international_data'] =   $this->getInternationalHolidayLib()->getInternationalHolidayById($id);
        $data['tour'] = $this->getTourLib()->getAll_InternationalTourData();
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.international-holidays'),
                'title' => 'Manage International Holidays'
            ),
            array(
                'title' => 'Edit International Holiday'
            )
        );
        return view('admin/international_holiday/international-holiday-edit', $data);   
    }

    public function international_holiday_update(Request $request) {
        if(isset($request->id)){
            $rules = [
                'title'       => 'required',
                'tour_id'     => 'required'
            ];

            $messages = [
                'title.required'      => 'Please enter title',
                'tour_id.required'    => 'Please select tour'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->route('tour.international-holidays.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                // $records = array(
                //     "title"               => $request->title,
                //     "tour_id"             => $request->tour_id,
                //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
                //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
                //     "status"              => $request->status
                // );

                $end_date=null;
                if($request->end_date!=null){
                    $end_date=date('Y-m-d', strtotime($request->end_date));
                }

                $start_date=null;
                if($request->start_date!=null){
                    $start_date=date('Y-m-d', strtotime($request->start_date));
                }

                $records = array(
                    "title"               => $request->title,
                    "tour_id"             => $request->tour_id,
                    "start_date"          => $start_date,
                    "end_date"            => $end_date,
                    "status"              => $request->status
                );

                $update=$this->getInternationalHolidayLib()->updateInternationalHoliday($records, $request->id);
                if($update){
                    Session::flash('success_msg', 'International holiday updated successfully.');   
                }else{
                    Session::flash('error_msg', 'International holiday updated unsuccessfully.');
                }
                return redirect()->route('tour.international-holidays');
            }
        }else{
            return redirect()->route('tour.international-holidays');
        }
    }

    /*
     * featured tours
     */

    public function featured_tour_list() {
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Featured Tours'
            )
        );
        return view('admin/featured_tour/index', $data);
    }

    public function featured_tour_list_data(Request $request) {
        return datatables()->of(
            FeaturedTblTour::select('featured_tbl_tours.id',
                'featured_tbl_tours.tour_id',
                'featured_tbl_tours.title',
                'featured_tbl_tours.status',
                'tour.tour_name')
            ->leftjoin('tour', 'tour.id', '=', 'featured_tbl_tours.tour_id')
            ->where('featured_tbl_tours.deleted_at', null)
            )
            ->filterColumn('tour_name', function($query, $keyword) {
                $query->whereRaw("tour_name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', '')
            ->toJson();
    }

    public function featured_tour_create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.featured-tours'),
                'title' => 'Manage Featured Tours'
            ),
            array(
                'title' => 'Add Featured Tour'
            )
        );
        $data['tour'] = $this->getTourLib()->getAllTourData();
        return view('admin/featured_tour/create', $data);
    }

    public function featured_tour_store(Request $request) {
        $rules = [
            'title'       => 'required',
            'tour_id'     => 'required'
        ];

        $messages = [
            'title.required'      => 'Please enter title',
            'tour_id.required'    => 'Please select tour'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('tour.featured-tours.add')->withErrors($validator)->withInput();
        } else {
            // $records = array(
            //     "title"               => $request->title,
            //     "tour_id"             => $request->tour_id,
            //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
            //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
            //     "status"              => $request->status
            // );

            $end_date=null;
            if($request->end_date!=null){
                $end_date=date('Y-m-d', strtotime($request->end_date));
            }

            $start_date=null;
            if($request->start_date!=null){
                $start_date=date('Y-m-d', strtotime($request->start_date));
            }

            $records = array(
                "title"               => $request->title,
                "tour_id"             => $request->tour_id,
                "start_date"          => $start_date,
                "end_date"            => $end_date,
                "status"              => $request->status
            );

            $last_id = $this->getFeaturedTblTourLib()->addFeaturedTour($records);
            if($last_id){
                Session::flash('success_msg', 'Featured Tour inserted successfully.');   
            }else{
                Session::flash('error_msg', 'Featured Tour inserted unsuccessfully.');
            }
            return redirect()->route('tour.featured-tours');
        }
    }

    public function featured_tour_destroy($id) {
        $deleted = $this->getFeaturedTblTourLib()->deleteFeaturedTour($id);
        if ($deleted) {
            Session::flash('success_msg', 'Featured Tour has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Featured Tour can't deleted!");
        }
        return redirect()->route('tour.featured-tours');
    }

    public function featured_tour_edit($id) {
        $data['featured_tour_data'] =   $this->getFeaturedTblTourLib()->getFeaturedTourById($id);
        $data['tour'] = $this->getTourLib()->getAllTourData();
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.featured-tours'),
                'title' => 'Manage Featured Tours'
            ),
            array(
                'title' => 'Edit Featured Tour'
            )
        );
        return view('admin/featured_tour/featured-tour-edit', $data);   
    }

    public function featured_tour_update(Request $request) {
        if(isset($request->id)){
            $rules = [
                'title'       => 'required',
                'tour_id'     => 'required'
            ];

            $messages = [
                'title.required'      => 'Please enter title',
                'tour_id.required'    => 'Please select tour'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->route('tour.featured-tours.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                // $records = array(
                //     "title"               => $request->title,
                //     "tour_id"             => $request->tour_id,
                //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
                //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
                //     "status"              => $request->status
                // );

                $end_date=null;
                if($request->end_date!=null){
                    $end_date=date('Y-m-d', strtotime($request->end_date));
                }

                $start_date=null;
                if($request->start_date!=null){
                    $start_date=date('Y-m-d', strtotime($request->start_date));
                }

                $records = array(
                    "title"               => $request->title,
                    "tour_id"             => $request->tour_id,
                    "start_date"          => $start_date,
                    "end_date"            => $end_date,
                    "status"              => $request->status
                );

                $update=$this->getFeaturedTblTourLib()->updateFeaturedTour($records, $request->id);
                if($update){
                    Session::flash('success_msg', 'Featured Tour updated successfully.');   
                }else{
                    Session::flash('error_msg', 'Featured Tour updated unsuccessfully.');
                }
                return redirect()->route('tour.featured-tours');
            }
        }else{
            return redirect()->route('tour.featured-tours');
        }
    }


    /*
     * Hot Deals Tours
     */

    public function hot_deal_tour_list() {
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Hot Deal Tours'
            )
        );
        return view('admin/hot_deal_tour/index', $data);
    }

    public function hot_deal_tour_list_data(Request $request) {
        return datatables()->of(
            HotDealTour::select('hot_deal_tours.id',
                'hot_deal_tours.tour_id',
                'hot_deal_tours.title',
                'hot_deal_tours.status',
                'tour.tour_name')
            ->leftjoin('tour', 'tour.id', '=', 'hot_deal_tours.tour_id')
            ->where('hot_deal_tours.deleted_at', null)
            )
            ->filterColumn('tour_name', function($query, $keyword) {
                $query->whereRaw("tour_name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', '')
            ->toJson();
    }

    public function hot_deal_tour_create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.hot-deals-tours'),
                'title' => 'Manage Hot Deal Tours'
            ),
            array(
                'title' => 'Add Hot Deal Tour'
            )
        );
        $data['tour'] = $this->getTourLib()->getAllTourData();
        return view('admin/hot_deal_tour/create', $data);
    }

    public function hot_deal_tour_store(Request $request) {
        $rules = [
            'title'       => 'required',
            'tour_id'     => 'required'
        ];

        $messages = [
            'title.required'      => 'Please enter title',
            'tour_id.required'    => 'Please select tour'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('tour.hot-deals-tours.add')->withErrors($validator)->withInput();
        } else {
            // $records = array(
            //     "title"               => $request->title,
            //     "tour_id"             => $request->tour_id,
            //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
            //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
            //     "status"              => $request->status
            // );

            $end_date=null;
            if($request->end_date!=null){
                $end_date=date('Y-m-d', strtotime($request->end_date));
            }

            $start_date=null;
            if($request->start_date!=null){
                $start_date=date('Y-m-d', strtotime($request->start_date));
            }

            $records = array(
                "title"               => $request->title,
                "tour_id"             => $request->tour_id,
                "start_date"          => $start_date,
                "end_date"            => $end_date,
                "status"              => $request->status
            );

            $last_id = $this->getHotDealTourLib()->addHotDealTour($records);
            if($last_id){
                Session::flash('success_msg', 'Hot Deal Tour inserted successfully.');   
            }else{
                Session::flash('error_msg', 'Hot Deal Tour inserted unsuccessfully.');
            }
            return redirect()->route('tour.hot-deals-tours');
        }
    }

    public function hot_deal_tour_destroy($id) {
        $deleted = $this->getHotDealTourLib()->deleteHotDealTour($id);
        if ($deleted) {
            Session::flash('success_msg', 'Hot Deal Tour has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Hot Deal Tour can't deleted!");
        }
        return redirect()->route('tour.hot-deals-tours');
    }

    public function hot_deal_tour_edit($id) {
        $data['hot_deal_tour_data'] =   $this->getHotDealTourLib()->getHotDealTourById($id);
        $data['tour'] = $this->getTourLib()->getAllTourData();
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.hot-deals-tours'),
                'title' => 'Manage Hot Deal Tours'
            ),
            array(
                'title' => 'Edit Hot Deal Tour'
            )
        );
        return view('admin/hot_deal_tour/hot-deal-tour-edit', $data);   
    }

    public function hot_deal_tour_update(Request $request) {
        if(isset($request->id)){
            $rules = [
                'title'       => 'required',
                'tour_id'     => 'required'
            ];

            $messages = [
                'title.required'      => 'Please enter title',
                'tour_id.required'    => 'Please select tour'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->route('tour.international-holidays.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                // $records = array(
                //     "title"               => $request->title,
                //     "tour_id"             => $request->tour_id,
                //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
                //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
                //     "status"              => $request->status
                // );

                $end_date=null;
                if($request->end_date!=null){
                    $end_date=date('Y-m-d', strtotime($request->end_date));
                }

                $start_date=null;
                if($request->start_date!=null){
                    $start_date=date('Y-m-d', strtotime($request->start_date));
                }

                $records = array(
                    "title"               => $request->title,
                    "tour_id"             => $request->tour_id,
                    "start_date"          => $start_date,
                    "end_date"            => $end_date,
                    "status"              => $request->status
                );

                $update=$this->getHotDealTourLib()->updateHotDealTour($records, $request->id);
                if($update){
                    Session::flash('success_msg', 'Hot Deal Tour updated successfully.');   
                }else{
                    Session::flash('error_msg', 'Hot Deal Tour updated unsuccessfully.');
                }
                return redirect()->route('tour.hot-deals-tours');
            }
        }else{
            return redirect()->route('tour.hot-deals-tours');
        }
    }

    /*
     * New Arrival Tours
     */
    public function new_arrival_list() {
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage New Arrival'
            )
        );
        return view('admin/new_arrival_tour/index', $data);
    }

    public function new_arrival_list_data(Request $request) {
        return datatables()->of(
            NewArrivalTour::select('new_arrival_tours.id',
                'new_arrival_tours.tour_id',
                'new_arrival_tours.title',
                'new_arrival_tours.status',
                'tour.tour_name')
            ->leftjoin('tour', 'tour.id', '=', 'new_arrival_tours.tour_id')
            ->where('new_arrival_tours.deleted_at', null)
            )
            ->filterColumn('tour_name', function($query, $keyword) {
                $query->whereRaw("tour_name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', '')
            ->toJson();
    }

    public function new_arrival_create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.new-arrivals'),
                'title' => 'Manage New Arrival'
            ),
            array(
                'title' => 'Add New Arrival'
            )
        );
        $data['tour'] = $this->getTourLib()->getAllTourData();
        return view('admin/new_arrival_tour/create', $data);
    }

    public function new_arrival_store(Request $request) {
        $rules = [
            'title'       => 'required',
            'tour_id'     => 'required'
        ];

        $messages = [
            'title.required'      => 'Please enter title',
            'tour_id.required'    => 'Please select tour'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('tour.new-arrivals.add')->withErrors($validator)->withInput();
        } else {
            // $records = array(
            //     "title"               => $request->title,
            //     "tour_id"             => $request->tour_id,
            //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
            //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
            //     "status"              => $request->status
            // );

            $end_date=null;
            if($request->end_date!=null){
                $end_date=date('Y-m-d', strtotime($request->end_date));
            }

            $start_date=null;
            if($request->start_date!=null){
                $start_date=date('Y-m-d', strtotime($request->start_date));
            }

            $records = array(
                "title"               => $request->title,
                "tour_id"             => $request->tour_id,
                "start_date"          => $start_date,
                "end_date"            => $end_date,
                "status"              => $request->status
            );

            $last_id = $this->getNewArrivalTourLib()->addNewArrivalTour($records);
            if($last_id){
                Session::flash('success_msg', 'New Arrival Tour inserted successfully.');   
            }else{
                Session::flash('error_msg', 'New Arrival Tour inserted unsuccessfully.');
            }
            return redirect()->route('tour.new-arrivals');
        }
    }

    public function new_arrival_destroy($id) {
        $deleted = $this->getNewArrivalTourLib()->deleteNewArrivalTour($id);
        if ($deleted) {
            Session::flash('success_msg', 'New Arrival Tour has been deleted successfully!');
        } else {
            Session::flash('error_msg', "New Arrival Tour can't deleted!");
        }
        return redirect()->route('tour.new-arrivals');
    }

    public function new_arrival_edit($id) {
        $data['new_arrival_data'] =   $this->getNewArrivalTourLib()->getNewArrivalTourById($id);
        $data['tour'] = $this->getTourLib()->getAllTourData();
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.new-arrivals'),
                'title' => 'Manage New Arrival'
            ),
            array(
                'title' => 'Edit New Arrival'
            )
        );
        return view('admin/new_arrival_tour/new-arrival-tour-edit', $data);   
    }

    public function new_arrival_update(Request $request) {
        if(isset($request->id)){
            $rules = [
                'title'       => 'required',
                'tour_id'     => 'required'
            ];

            $messages = [
                'title.required'      => 'Please enter title',
                'tour_id.required'    => 'Please select tour'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->route('tour.new-arrivals.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                // $records = array(
                //     "title"               => $request->title,
                //     "tour_id"             => $request->tour_id,
                //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
                //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
                //     "status"              => $request->status
                // );

                $end_date=null;
                if($request->end_date!=null){
                    $end_date=date('Y-m-d', strtotime($request->end_date));
                }

                $start_date=null;
                if($request->start_date!=null){
                    $start_date=date('Y-m-d', strtotime($request->start_date));
                }

                $records = array(
                    "title"               => $request->title,
                    "tour_id"             => $request->tour_id,
                    "start_date"          => $start_date,
                    "end_date"            => $end_date,
                    "status"              => $request->status
                );

                $update=$this->getNewArrivalTourLib()->updateNewArrivalTour($records, $request->id);
                if($update){
                    Session::flash('success_msg', 'New Arrival Tour updated successfully.');   
                }else{
                    Session::flash('error_msg', 'New Arrival Tour updated unsuccessfully.');
                }
                return redirect()->route('tour.new-arrivals');
            }
        }else{
            return redirect()->route('tour.new-arrivals');
        }
    }

    /*
     * Featured Destination Tours
     */

    public function featured_destination_list() {
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Featured Destinations'
            )
        );
        return view('admin/featured_destination_tour/index', $data);
    }

    public function featured_destination_list_data(Request $request) {
        return datatables()->of(
            FeaturedDestinationTour::select('featured_destination_tours.id',
                'featured_destination_tours.tour_id',
                'featured_destination_tours.title',
                'featured_destination_tours.status',
                'tour.tour_name')
            ->leftjoin('tour', 'tour.id', '=', 'featured_destination_tours.tour_id')
            ->where('featured_destination_tours.deleted_at', null)
            )
            ->filterColumn('tour_name', function($query, $keyword) {
                $query->whereRaw("tour_name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', '')
            ->toJson();
    }

    public function featured_destination_create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.featured-destinations'),
                'title' => 'Manage Featured Destinations'
            ),
            array(
                'title' => 'Add Featured Destination'
            )
        );
        $data['tour'] = $this->getTourLib()->getAllTourData();
        return view('admin/featured_destination_tour/create', $data);
    }

    public function featured_destination_store(Request $request) {
        $rules = [
            'title'       => 'required',
            'tour_id'     => 'required'
        ];

        $messages = [
            'title.required'      => 'Please enter title',
            'tour_id.required'    => 'Please select tour'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route('tour.featured-destinations.add')->withErrors($validator)->withInput();
        } else {
            // $records = array(
            //     "title"               => $request->title,
            //     "tour_id"             => $request->tour_id,
            //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
            //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
            //     "status"              => $request->status
            // );

            $end_date=null;
            if($request->end_date!=null){
                $end_date=date('Y-m-d', strtotime($request->end_date));
            }

            $start_date=null;
            if($request->start_date!=null){
                $start_date=date('Y-m-d', strtotime($request->start_date));
            }

            $records = array(
                "title"               => $request->title,
                "tour_id"             => $request->tour_id,
                "start_date"          => $start_date,
                "end_date"            => $end_date,
                "status"              => $request->status
            );

            $last_id = $this->getFeaturedDestinationTourLib()->addFeaturedDestinationTour($records);
            if($last_id){
                Session::flash('success_msg', 'Featured destination inserted successfully.');   
            }else{
                Session::flash('error_msg', 'Featured destination inserted unsuccessfully.');
            }
            return redirect()->route('tour.featured-destinations');
        }
    }

    public function featured_destination_destroy($id) {
        $deleted = $this->getFeaturedDestinationTourLib()->deleteFeaturedDestinationTour($id);
        if ($deleted) {
            Session::flash('success_msg', 'Featured destination has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Featured destination can't deleted!");
        }
        return redirect()->route('tour.featured-destinations');
    }

    public function featured_destination_edit($id) {
        $data['featured_destination_data'] =   $this->getFeaturedDestinationTourLib()->getFeaturedDestinationTourById($id);
        $data['tour'] = $this->getTourLib()->getAllTourData();
        $data['breadcrumb'] = array(
            array(
                'link' => route('tour.featured-destinations'),
                'title' => 'Manage Featured Destinations'
            ),
            array(
                'title' => 'Edit Featured Destination'
            )
        );
        return view('admin/featured_destination_tour/featured-destination-tour-edit', $data);   
    }

    public function featured_destination_update(Request $request) {
        if(isset($request->id)){
            $rules = [
                'title'       => 'required',
                'tour_id'     => 'required'
            ];

            $messages = [
                'title.required'      => 'Please enter title',
                'tour_id.required'    => 'Please select tour'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->route('tour.featured-destinations.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                // $records = array(
                //     "title"               => $request->title,
                //     "tour_id"             => $request->tour_id,
                //     "start_date"          => date('Y-m-d', strtotime($request->start_date)),
                //     "end_date"            => date('Y-m-d', strtotime($request->end_date)),
                //     "status"              => $request->status
                // );

                $end_date=null;
                if($request->end_date!=null){
                    $end_date=date('Y-m-d', strtotime($request->end_date));
                }

                $start_date=null;
                if($request->start_date!=null){
                    $start_date=date('Y-m-d', strtotime($request->start_date));
                }

                $records = array(
                    "title"               => $request->title,
                    "tour_id"             => $request->tour_id,
                    "start_date"          => $start_date,
                    "end_date"            => $end_date,
                    "status"              => $request->status
                );
                
                $update=$this->getFeaturedDestinationTourLib()->updateFeaturedDestinationTour($records, $request->id);
                if($update){
                    Session::flash('success_msg', 'Featured destination updated successfully.');   
                }else{
                    Session::flash('error_msg', 'Featured destination updated unsuccessfully.');
                }
                return redirect()->route('tour.featured-destinations');
            }
        }else{
            return redirect()->route('tour.featured-destinations');
        }
    }

    public function change_status(Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            try {
                $table = $request->table;
                $id = $request->id;
                $status = $request->status;
                $column_name = $request->column_name;
                
                $update_columns = "tour_status";
                if(isset($request->column_update)){
                    $update_columns = $request->column_update;
                }
                
                //update query fire
                DB::table($table)
                        ->where($column_name, $id)
                        ->update([$update_columns => $status]);
                $response['success'] = true;
                $response['msg_status'] = 'success';
                $response['message'] = "Status has been changed successfully";
            } catch (Exception $e) {
                $response['success'] = false;
                $response['msg_status'] = 'warning';
                $response['message'] = "Something went wrong, please try again";
            }
        }
        exit(json_encode($response));
    }

}
