<?php

namespace App\Http\Controllers\admin;

//use App\tourinfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TourInfo;
use App\Http\Controllers\ControllerAbstract;
use Session;
use Validator;

class TourInfoController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Tour Info Master'
            )
        );
        return view('admin/tour-info/tourinfo', $data);
    }

    public function data(Request $request) {
        return datatables()->of(TourInfo::select('incs_excs_tcs.id', 'incs_excs_tcs.title', 'incs_excs_tcs.inclusions', 'incs_excs_tcs.exclusions', 'incs_excs_tcs.terms_conditions', 'incs_excs_tcs.status', 'incs_excs_tcs.created_at' )
            ->where('incs_excs_tcs.deleted_at', null))
            ->editColumn('created_at', function ($model) {
                return date("d M Y", strtotime($model->created_at));
            })
            ->addColumn('action', '')
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('tourinfo.index'),
                'title' => 'Manage Tour Info Master'
            ),
            array(
                'title' => 'Add Tour Info'
            )
        );
        return view('admin/tour-info/tourinfo-manage', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tourinfo  $tourinfo
     * @return \Illuminate\Http\Response
     */
    public function show(tourinfo $tourinfo) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tourinfo  $tourinfo
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['tourinfo'] = $this->getTourInfoLib()->getTourInfoById($id);
        
        $data['breadcrumb'] = array(
            array(
                'link' => route('tourinfo.index'),
                'title' => 'Manage Tour Info Master'
            ),
            array(
                'title' => 'Edit Tour Info'
            )
        );

        return view('admin/tour-info/tourinfo-manage', $data);
    }

       /**
     * get the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tourinfo  $tourinfo
     * @return \Illuminate\Http\Response
     */

    public function get_data(Request $request) {
        $data['info'] = $this->getTourInfoLib()->getTourInfoById($request->id); 
        $response['success'] = true;
        $response['info']  = $data['info']; 
        exit(json_encode($response));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tourinfo  $tourinfo
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request) {
        //validate tourinfo data

        $rules = [
            'title' => 'required',
            'inclusions' => 'required',
            'exclusions' => 'required',
            'terms_conditions' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'title' => 'Please enter title',
            'inclusions' => 'Please enter inclusions',
            'exclusions' => 'Please enter exclusions',
            'terms_conditions' => 'Please enter terms conditions',
            'status' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('tourinfo.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('tourinfo.add')->withErrors($validator)->withInput();
            }
        } else {
            //get tourinfo data
            $tourinfoData = $request->all();

            $data = array(
                'title' => $tourinfoData['title'], 
                'inclusions' => $tourinfoData['inclusions'],
                'exclusions' => $tourinfoData['exclusions'],
                'terms_conditions' => $tourinfoData['terms_conditions'],
                'status' => $tourinfoData['status'],
            );

             //Updoad image and update in db
            if ($tourinfoData['id'] > 0) {
                $secID  = $tourinfoData['id'] ;
                $this->getTourInfoLib()->updateTourInfo($data, $tourinfoData['id']);
                Session::flash('success_msg', 'Tour Info has been updated successfully!');
            } else {
                $secID = $this->getTourInfoLib()->addTourInfo($data);
                Session::flash('success_msg', 'Tour Info has been saved successfully!');
            }

        }

        return redirect()->route('tourinfo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tourinfo  $tourinfo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = array("deleted" => true);
        $deleted = $this->getTourInfoLib()->deleteTourInfo($id);
        if ($deleted) {
            Session::flash('success_msg', 'Tour Info deleted successfully!');
        } else {
            Session::flash('error_msg', "Tour Info can't deleted successfully!");
        }
        return redirect()->route('tourinfo.index');
    }

}
