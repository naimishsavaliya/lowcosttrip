<?php

namespace App\Http\Controllers\admin;

use App\Destination;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Destinations;
use App\Http\Controllers\ControllerAbstract;
use Validator;
use Session;

class DestinationController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Destination'
            )
        );
        return view('admin/destinations/destination', $data);
    }

    /*
     * Return destination data in JSON
     */

    public function data(Request $request) {
        return datatables()->of(Destinations::select('destinations.desId as desId',
                            'destinations.name as destinations_name',
                            'destinations.sector_id as sector_id', 
                            'destinations.status as status', 
                            'destinations.created_at as created_at',
                            'sector.name as sector_name',
                            'country.country_name as country_name', 
                            'state.state_name as state_name', 
                            'city.city_name as city_name')
                                ->join("sector", "sector.id", "=", "destinations.sector_id", "left")
                                ->join("country", "country.country_id", "=", "destinations.country_id", "left")
                                ->join("state", "state.state_id", "=", "destinations.state_id", "left")
                                ->join("city", "city.city_id", "=", "destinations.city_id", "left")
                                ->where('destinations.deleted_at', null))
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->addColumn('action', '')
                        //->rawColumns(['status', 'action'])
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['sectors'] = $this->getSectorLib()->getSector(array('id', 'name'), array(), 1);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        
        $data['breadcrumb'] = array(
            array(
                'link' => route('destination.index'),
                'title' => 'Manage Destination'
            ),
            array(
                'title' => 'Add Destination'
            )
        );

        return view('admin/destinations/destination-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function show(Destination $destination) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['sectors'] = $this->getSectorLib()->getSector(array('id', 'name'));
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));

        $data['destinations'] = $this->getDestinationsLib()->getDestinationById($id);
        $data['states'] = $this->getStateLib()->getStateByCountry($data['destinations']['country_id'], array('state_id', 'state_name'));
        $data['cities'] = $this->getCityLib()->getCityByState($data['destinations']['state_id'], array('city_id', 'city_name'));
        
        $data['breadcrumb'] = array(
            array(
                'link' => route('destination.index'),
                'title' => 'Manage Destination'
            ),
            array(
                'title' => 'Edit Destination'
            )
        );
        
        return view('admin/destinations/destination-manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request) {
        //validate destination data
        $rules = [
            'name' => 'required',
            'sector_id' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name' => 'Please enter destination name',
            'sector_id' => 'Please select sector',
            'country_id' => 'Please select country',
            'state_id' => 'Please select state',
            'city_id' => 'Please select city',
            'status' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('destination.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('destination.add')->withErrors($validator)->withInput();
            }
        } else {
            //get destination data
            $destinationData = $request->all();

            $data = array(
                'name' => $destinationData['name'],
                'description' => $destinationData['description'],
                'weather_description' => $destinationData['weather_description'],
                'country_id' => $destinationData['country_id'],
                'sector_id' => $destinationData['sector_id'],
                'state_id' => $destinationData['state_id'],
                'city_id' => $destinationData['city_id'],
                'status' => $destinationData['status'],
            );

            if ($destinationData['id'] > 0) {
                $this->getDestinationsLib()->updateDestination($data, $destinationData['id']);
                Session::flash('success_msg', 'Destination has been updated successfully!');
            } else {
                $this->getDestinationsLib()->addDestination($data);
                Session::flash('success_msg', 'Destination has been saved successfully!');
            }
        }

        return redirect()->route('destination.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->getDestinationsLib()->deleteDestination($id);
        if ($deleted) {
            Session::flash('success_msg', 'Destination deleted successfully!');
        } else {
            Session::flash('error_msg', "Destination can't deleted successfully!");
        }
        return redirect()->route('destination.index');
    }

}
