<?php

namespace App\Http\Controllers\admin;

use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use DB;
use File;

class QuotationController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        return view('admin/quotation/quotation', $data);
    }

    /*
     * Quotation Data
     */

    // public function data($lead_id, Request $request) {
    //     if ($request->ajax()) {
    //         return datatables()->of(\App\Leadquatation::select('lead_quatation.quaId', 'lead_quatation.quatation_id','lead_quatation.lead_id', 'lead_quatation.subject', 'lead_quatation.adult as members', 'lead_quatation.child', 'lead_quatation.infant', 'lead_quatation.total_amount', 'lead_quatation.created_at', 
    //             // \DB::raw("CONCAT(users.first_name,' ',users.last_name) as full_name"), 
    //             'lead_quatation.status_id')
    //                                 ->leftjoin('users', 'lead_quatation.created_by', '=', 'users.id')
    //                                 ->where('lead_quatation.deleted_at', null)
    //                                 ->where('lead_quatation.lead_id', $lead_id))
    //                         ->editColumn('created_at', function ($model) {
    //                             return date("d M Y", strtotime($model->created_at));
    //                         })
    //                         ->editColumn('subject', function ($model) {
    //                             return mb_strimwidth($model->subject, 0, 100, "...");
    //                         })
    //                         ->editColumn('total_amount', function ($model) {
    //                             return number_format($model->total_amount, 2);
    //                         })
    //                         ->editColumn('members', function ($model) {
    //                             $str_member = '';
    //                             if (isset($model->members)) {
    //                                 $str_member .= $model->members . '/';
    //                             } else {
    //                                 $str_member .= '0/';
    //                             }
    //                             if (isset($model->child)) {
    //                                 $str_member .= $model->child . '/';
    //                             } else {
    //                                 $str_member .= '0/';
    //                             }
    //                             if (isset($model->infant)) {
    //                                 $str_member .= $model->infant;
    //                             } else {
    //                                 $str_member .= '0';
    //                             }
    //                             return $str_member;
    //                         })
    //                         ->filterColumn('lead_quatation.created_at', function($query, $keyword) {
    //                             $search_date = date('Y-m-d', strtotime($keyword));
    //                             $query->whereRaw("DATE_FORMAT(lead_quatation.created_at, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
    //                         })
    //                         ->filterColumn('full_name', function($query, $keyword) {
    //                             $query->whereRaw("CONCAT(users.first_name,' ',users.last_name) like ?", ["%{$keyword}%"]);
    //                         })
    //                         ->editColumn('status_id', function($Leadquatation) {
    //                             if ($Leadquatation->status_id == 1) {
    //                                 return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $Leadquatation->quaId . '" data-id="' . $Leadquatation->status_id . '" data-uk-modal="{target:\'#quotation_update_status\'}"><span class="uk-badge uk-badge-warning">Draft</span></a>';
    //                             } elseif ($Leadquatation->status_id == 2) {
    //                                 return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $Leadquatation->quaId . '" data-id="' . $Leadquatation->status_id . '" data-uk-modal="{target:\'#quotation_update_status\'}"><span class="uk-badge uk-badge-info">Quote Sent</span></a>';
    //                             } elseif ($Leadquatation->status_id == 3) {
    //                                 return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $Leadquatation->quaId . '" data-id="' . $Leadquatation->status_id . '" data-uk-modal="{target:\'#quotation_update_status\'}"><span class="uk-badge uk-badge-success">Quote Approved</span></a>';
    //                             } elseif ($Leadquatation->status_id == 4) {
    //                                 return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $Leadquatation->quaId . '" data-id="' . $Leadquatation->status_id . '" data-uk-modal="{target:\'#quotation_update_status\'}"><span class="uk-badge uk-badge-danger">Quote Rejected</span></a>';
    //                             } else {
    //                                 return '-';
    //                             }
    //                         })
    //                         ->addColumn('action', '')
    //                         ->rawColumns(['status_id', 'action'])
    //                         ->toJson();
    //     }
    // }

    public function data($lead_id, Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Leadquatation::select('quaId', 
                'quatation_id',
                'lead_id',
                'subject',
                DB::raw("CONCAT(adult,'/',child,'/',infant) as members"),
                DB::raw("FORMAT(total_amount,2) as total_amount"), 
                'created_at', 
                'status_id')
                ->where('deleted_at', null)
                ->where('lead_id', $lead_id))
                ->editColumn('created_at', function ($model) {
                    return date("d M Y", strtotime($model->created_at));
                })
                ->editColumn('subject', function ($model) {
                    return mb_strimwidth($model->subject, 0, 100, "...");
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                })
                ->filterColumn('members', function($query, $keyword) {
                    $query->whereRaw("CONCAT(adult,'/',child,'/',infant) like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('total_amount', function($query, $keyword) {
                    $query->whereRaw("FORMAT(total_amount,2) like ?", ["%{$keyword}%"]);
                })
                ->editColumn('status_id', function($Leadquatation) {
                    if ($Leadquatation->status_id == 1) {
                        return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $Leadquatation->quaId . '" data-id="' . $Leadquatation->status_id . '" data-uk-modal="{target:\'#quotation_update_status\'}"><span class="uk-badge uk-badge-warning">Draft</span></a>';
                    } elseif ($Leadquatation->status_id == 2) {
                        return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $Leadquatation->quaId . '" data-id="' . $Leadquatation->status_id . '" data-uk-modal="{target:\'#quotation_update_status\'}"><span class="uk-badge uk-badge-info">Quote Sent</span></a>';
                    } elseif ($Leadquatation->status_id == 3) {
                        return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $Leadquatation->quaId . '" data-id="' . $Leadquatation->status_id . '" data-uk-modal="{target:\'#quotation_update_status\'}"><span class="uk-badge uk-badge-success">Quote Approved</span></a>';
                    } elseif ($Leadquatation->status_id == 4) {
                        return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="' . $Leadquatation->quaId . '" data-id="' . $Leadquatation->status_id . '" data-uk-modal="{target:\'#quotation_update_status\'}"><span class="uk-badge uk-badge-danger">Quote Rejected</span></a>';
                    } else {
                        return '-';
                    }
                })
                ->addColumn('action', '')
                ->rawColumns(['status_id', 'action'])
                ->toJson();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lead_id) {
        $data['quotation_status'] = $this->getCommonLib()->getQuotationStatus();
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['lead_info'] = $lead_info = $this->getLeadLib()->getLeadById($lead_id);
        $data['states'] = $this->getStateLib()->getStateByCountry($lead_info->country_id, array('state_id', 'state_name'));
        $data['cities'] = $this->getCityLib()->getCityByState($lead_info->state_id, array('city_id', 'city_name'));
        $data['inclusion_exclusion'] = $this->getTourInfoLib()->getTourInfo();
        $data['quotation_currency'] = $this->getCurrencymasterLib()->getCurrencymaster(array('currId', 'currency_name', 'code', 'symbol'));


        if($lead_info->tour_id > 0){
            $data['tour_info'] = $this->getTourLib()->getTourById($lead_info->tour_id);
        }
        // dd($data['tour_info']);
        $data['breadcrumb'] = array(
            array(
                'link' => route('lead.index'),
                'title' => 'Manage Leads'
            ),
            array(
                'link' => route('lead.lead_detail', $lead_id . '#quotation'),
                'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
            ),
            array(
                'title' => 'Add Quotation'
            )
        );
        return view('admin/quotation/quotation-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save($lead_id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {

            $itinarary_path = public_path() . '/uploads/itinerary';
            if (\File::exists($itinarary_path) == false) {
                \File::makeDirectory($itinarary_path, $mode = 0777, true, true);
            }

            $formdata_str = $request->data;
            parse_str($formdata_str, $formdata);
            $itenary_arr = json_decode($request->itinarary);


            $quotation_data['tour_id'] = $formdata['tour_id'];
            $quotation_data['subject'] = $formdata['subject'];
            $quotation_data['status_id'] = $formdata['quotation_status'];
            $quotation_data['adult'] = $formdata['no_adults'];
            $quotation_data['infant'] = $formdata['no_infants'];
            $quotation_data['child'] = $formdata['no_child'];
            $quotation_data['quatation_date'] = date('Y-m-d', strtotime($formdata['quotation_date']));
            $quotation_data['validup_date'] = date('Y-m-d', strtotime($formdata['valid_date']));
            //$quotation_data['address'] = $formdata['address'];
            //$quotation_data['country_id'] = $formdata['country_id'];
            $quotation_data['state_id'] = $formdata['state_id'];
            //$quotation_data['city_id'] = $formdata['city_id'];
            //$quotation_data['pin_code'] = $formdata['pin_code'];
            $quotation_data['inclusion'] = $formdata['inclusions'];
            $quotation_data['exclusion'] = $formdata['exclusions'];
            $quotation_data['terms_condition'] = $formdata['terms_conditions'];
            $quotation_data['inclusion_exclusion'] = $formdata['inclusion_exclusion'];
            $quotation_data['quotation_currency'] = $formdata['quotation_currency'];
            $quotation_data['total_amount'] = round($formdata['total_line_amount']);
            $particular_arr = $formdata['particular'];
            $state_info = $this->getStateLib()->getStateById($formdata['state_id']);
            $company_state = strtolower(env('COMPANY_STATE', 'gujarat'));
            $is_current_state = 'no';
            if (  isset($state_info->state_name) && $company_state == strtolower($state_info->state_name)) {
                $is_current_state = 'yes';
            }
            if (isset($formdata['id']) && $formdata['id'] > 0) {
                $quation_id = $formdata['id'];

                $this->getLeadQuatationLib()->updateLeadQuatation($quotation_data, $quation_id);

                //Start
                if ($quotation_data['status_id'] == 3) {
                    $quotation = $this->getLeadQuatationLib()->getLeadQuatationRecordById($quation_id);
                    $customerpayment = $this->getCustomerPaymentLib()->chechQuotationCustomerPayment($quation_id);
                    if (!empty($customerpayment)) {
                        $current_balance = null;
                        $records = array();
                        if ($quotation->total_amount != $customerpayment->total_amount) {
                            //$BalnceRecord = $this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($customerpayment->created_at);
                            $BalnceRecord=$this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($customerpayment->created_at,$customerpayment->lead_id);
                            if (!empty($BalnceRecord)) {
                                if (isset($BalnceRecord->balance) && $BalnceRecord->balance != null) {
                                    $current_balance = $quotation->total_amount - $customerpayment->total_amount;
                                    $records['balance'] = $BalnceRecord->balance + $quotation->total_amount;
                                    $records['total_amount'] = $quotation->total_amount;
                                }
                            } else {
                                $records['balance'] = $quotation->total_amount;
                                $records['total_amount'] = $quotation->total_amount;
                            }
                        }
                        if (isset($customerpayment->created_at)) {
                            //$AllDownRecord = $this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($customerpayment->created_at);
                            $AllDownRecord=$this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($customerpayment->created_at,$customerpayment->lead_id);
                            if (!empty($AllDownRecord)) {
                                foreach ($AllDownRecord as $key => $value) {
                                    $cur = $value->balance + $current_balance;
                                    $this->getCustomerPaymentLib()->updateCustomerPayment(['balance' => $cur], $value->id);
                                }
                            }
                        }
                        if (!empty($records)) {
                            $this->getCustomerPaymentLib()->updateCustomerPaymentQuotation($records, $quation_id);
                        }
                    } else {
                        $records = array(
                            "quotation_id" => $quation_id
                        );
                        //$lastRecord = $this->getCustomerPaymentLib()->getCustomerPaymentlastRecord();
                        $lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($quotation->lead_id);
                        if ($lastRecord) {
                            if (isset($lastRecord->balance) && $lastRecord->balance != null) {
                                if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                                    $records['balance'] = $quotation->total_amount + $lastRecord->balance;
                                }
                            }
                        } else {
                            $records['balance'] = $quotation->total_amount;
                        }

                        if (isset($quotation->lead_id) && $quotation->lead_id != null) {
                            $lead = $this->getLeadLib()->getLeadById($quotation->lead_id);
                            $records['lead_id'] = $quotation->lead_id;
                            if (isset($lead->customer_id) && $lead->customer_id != null) {
                                $records['customer_id'] = $lead->customer_id;
                            }
                        }
                        if (isset($quotation->tour_id) && $quotation->tour_id != null) {
                            $records['tour_id'] = $quotation->tour_id;
                        }
                        if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                            $records['total_amount'] = $quotation->total_amount;
                        }
                        $this->getCustomerPaymentLib()->addCustomerPayment($records);
                    }
                } else {
                    $current_balance = null;
                    $PaymentData = $this->getCustomerPaymentLib()->chechQuotationCustomerPayment($quation_id);
                    if (!empty($PaymentData)) {
                        if (isset($PaymentData->created_at)) {
                            //$BalnceRecord = $this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at);
                            $BalnceRecord=$this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
                            if (!empty($BalnceRecord)) {
                                if (isset($BalnceRecord->balance) && $BalnceRecord->balance != null) {
                                    $current_balance = $BalnceRecord->balance - $PaymentData->balance;
                                }
                            }

                            //$AllDownRecord = $this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at);
                            $AllDownRecord=$this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
                            if (!empty($AllDownRecord)) {
                                foreach ($AllDownRecord as $key => $value) {
                                    $cur = $value->balance + $current_balance;
                                    $this->getCustomerPaymentLib()->updateCustomerPayment(['balance' => $cur], $value->id);
                                }
                            }
                        }
                        $this->getCustomerPaymentLib()->deleteCustomerPaymentQuotation($quation_id);
                    }
                }
                //End

                $db_particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quation_id, array('perId'));
                $db_itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quation_id, array('itiId'));

                $db_particular_ids = array();
                if (count($db_particular) > 0) {
                    foreach ($db_particular as $p_val) {
                        $db_particular_ids[$p_val->perId] = $p_val->perId;
                    }
                }

                $db_itinarary_ids = array();
                if (count($db_itinarary) > 0) {
                    foreach ($db_itinarary as $i_val) {
                        $db_itinarary_ids[$i_val->itiId] = $i_val->itiId;
                    }
                }

                if (count($particular_arr) > 0) {
                    foreach ($particular_arr as $par_key => $par_value) {
                        $particular_data = array();
                        $particular_data['item_type'] = $par_value['item_type'];
                        $particular_data['description'] = $par_value['description'];
                        $particular_data['item_id'] = $par_value['item_id'];
                        $particular_data['item_no'] = ((int)$par_key+1);
                        $particular_data['item_title'] = $par_value['item_title'];
                        $particular_data['qty'] = $par_value['qty'];

                        $particular_data['gst'] = round($par_value['gst']);
                        $particular_data['cgst'] = (($is_current_state == 'yes') ? ( $par_value['gst'] / 2 ) : 0 );
                        $particular_data['sgst'] = (($is_current_state == 'yes') ? ( $par_value['gst'] / 2 ) : 0 );
                        $particular_data['igst'] = (($is_current_state == 'no') ? $par_value['gst'] : 0 );


                        $particular_data['cost'] = round($par_value['cost']);
                        $particular_data['discount'] = $par_value['discount'];

                        if (isset($par_value['per_id']) && $par_value['per_id'] > 0) {
                            $this->getLeadParticularLib()->updateLeadParticular($particular_data, $par_value['per_id']);
                            $particular_id = $par_value['per_id'];
                            //unset from  $db_particular_ids array
                            unset($db_particular_ids[$particular_id]);
                        } else {
                            $particular_data['lead_id'] = $lead_id;
                            $particular_data['quotation_id'] = $quation_id;

                            $particular_id = $this->getLeadParticularLib()->addLeadParticular($particular_data);
                        }
                    }
                }

                if (count($itenary_arr) > 0) {
                    foreach ($itenary_arr as $iti_key => $ite_value) {
                        $itinary_data = array();
                        $itinary_data['day'] = $ite_value->day;
                        $itinary_data['title'] = $ite_value->title;
                        $itinary_data['description'] = $ite_value->description;

                        //Upload destination image
                        $image = '';
                        $image = $request->file('itenary_image_' . $iti_key);
                        if (!empty($image)) {
                            $image_name = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . '.' . $image->getClientOriginalExtension();
                            $image->move($itinarary_path, $image_name);
                            $itinary_data['image'] = $image_name;
                        }
                        if (isset($ite_value->id) && $ite_value->id > 0) {
                            $this->getLeadItinararyLib()->updateLeadItinarary($itinary_data, $ite_value->id);
                            //unset from  $db_particular_ids array
                            unset($db_itinarary_ids[$ite_value->id]);
                        } else {
                            $itinary_data['lead_id'] = $lead_id;
                            $itinary_data['quotation_id'] = $quation_id;

                            $itinarary_id = $this->getLeadItinararyLib()->addLeadItinarary($itinary_data);
                        }
                    }
                }

                //delete quotation particular
                if (count($db_particular_ids) > 0) {
                    $this->getLeadParticularLib()->deleteMulLeadParticular($db_particular_ids);
                }

                //delete quotation itinarary
                if (count($db_itinarary_ids) > 0) {
                    $this->getLeadItinararyLib()->deleteMulLeadItinarary($db_itinarary_ids);
                }

                $response['success'] = true;
                $response['quotation_id'] = $quation_id;
                $response['lead_id'] = $lead_id;
                $response['msg_status'] = 'success';
                $response['message'] = "Quotation has been saved successfuly";
            } else {
                try {


                    $quotation_data['lead_id'] = $lead_id;
                    $quation_id = $this->getLeadQuatationLib()->addLeadQuatation($quotation_data);

                    //Start
                    if ($quation_id) {
                        $quotation = $this->getLeadQuatationLib()->getLeadQuatationRecordById($quation_id);
                        if ($quotation_data['status_id'] == 3) {
                            $records = array("quotation_id" => $quation_id);
                            //$lastRecord = $this->getCustomerPaymentLib()->getCustomerPaymentlastRecord();
                            $lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($quotation->lead_id);
                            if ($lastRecord) {
                                if (isset($lastRecord->balance) && $lastRecord->balance != null) {
                                    if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                                        $records['balance'] = $quotation->total_amount + $lastRecord->balance;
                                    }
                                }
                            } else {
                                $records['balance'] = $quotation->total_amount;
                            }
                            if (isset($quotation->lead_id) && $quotation->lead_id != null) {
                                $lead = $this->getLeadLib()->getLeadById($quotation->lead_id);
                                $records['lead_id'] = $quotation->lead_id;
                                if (isset($lead->customer_id) && $lead->customer_id != null) {
                                    $records['customer_id'] = $lead->customer_id;
                                }
                            }
                            if (isset($quotation->tour_id) && $quotation->tour_id != null) {
                                $records['tour_id'] = $quotation->tour_id;
                            }
                            if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                                $records['total_amount'] = $quotation->total_amount;
                            }
                            $this->getCustomerPaymentLib()->addCustomerPayment($records);
                        }
                    }
                    //End

                    if (count($particular_arr) > 0) {
                        foreach ($particular_arr as $par_key => $par_value) {

                            $particular_data = array();
                            $particular_data['lead_id'] = $lead_id;
                            $particular_data['quotation_id'] = $quation_id;
                            $particular_data['item_no'] = ((int)$par_key+1);
                            $particular_data['item_type'] = $par_value['item_type'];
                            $particular_data['description'] = $par_value['description'];
                            $particular_data['item_id'] = $par_value['item_id'];
                            $particular_data['item_title'] = $par_value['item_title'];
                            $particular_data['qty'] = $par_value['qty'];

                            $particular_data['gst'] = round($par_value['gst']);
                            $particular_data['cgst'] = (($is_current_state == 'yes') ? ( $par_value['gst'] / 2 ) : 0 );
                            $particular_data['sgst'] = (($is_current_state == 'yes') ? ( $par_value['gst'] / 2 ) : 0 );
                            $particular_data['igst'] = (($is_current_state == 'no') ? $par_value['gst'] : 0 );

                            $particular_data['cost'] = round($par_value['cost']);
                            $particular_data['discount'] = $par_value['discount'];
//                            print_r($particular_data); die;
                            $particular_id = $this->getLeadParticularLib()->addLeadParticular($particular_data);
                        }
                    }

                    if (count($itenary_arr) > 0) {

                        foreach ($itenary_arr as $iti_key => $ite_value) {
                            $itinary_data['lead_id'] = $lead_id;
                            $itinary_data['quotation_id'] = $quation_id;
                            $itinary_data['day'] = $ite_value->day;
                            $itinary_data['title'] = $ite_value->title;
                            $itinary_data['description'] = $ite_value->description;

                            //Upload destination image
                            $image = $request->file('itenary_image_' . $iti_key);
                            if (!empty($image)) {
                                $image_name = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . '.' . $image->getClientOriginalExtension();
                                $image->move($itinarary_path, $image_name);
                                $itinary_data['image'] = $image_name;
                            }
                            $itinarary_id = $this->getLeadItinararyLib()->addLeadItinarary($itinary_data);
                        }
                    }
                    $response['success'] = true;
                    $response['quotation_id'] = $quation_id;
                    $response['lead_id'] = $lead_id;
                    $response['msg_status'] = 'success';
                    $response['message'] = "Quotation has been saved successfuly";
                } catch (Exception $exc) {
                    $response['success'] = false;
                    $response['msg_status'] = 'warning';
                    $response['message'] = "Quotation can't saved successfuly";
                }
            }
        }
        exit(json_encode($response));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lead_id, $quotation_id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $lead = $this->getLeadLib()->getLeadInfo($lead_id);
            $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quotation_id);
            $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
            $quotation->quotation_status = $quotation_satus;

            $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quotation_id);
            $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quotation_id);

            $contents = view('admin/quotation/quotation-view', ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary])->render();
            $response['success'] = true;
            $response['contents'] = $contents;
        }
        exit(json_encode($response));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lead_id, $id) {
        $data['quotation'] = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $id);
        $data['tour'] = $this->getTourLib()->getTourById($data['quotation']->tour_id, array('id', 'tour_name'));
        $data['particular'] = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $id);
        $data['itinerarys'] = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $id);
        $data['quotation_status'] = $this->getCommonLib()->getQuotationStatus();
        $data['lead_info'] = $lead_info = $this->getLeadLib()->getLeadById($lead_id);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['states'] = $this->getStateLib()->getStateByCountry($lead_info->country_id, array('state_id', 'state_name'));
        $data['cities'] = $this->getCityLib()->getCityByState($lead_info->state_id, array('city_id', 'city_name'));
         $data['inclusion_exclusion'] = $this->getTourInfoLib()->getTourInfo();
         $data['quotation_currency'] = $this->getCurrencymasterLib()->getCurrencymaster(array('currId', 'currency_name', 'code', 'symbol'));

        $data['breadcrumb'] = array(
            array(
                'link' => route('lead.index'),
                'title' => 'Manage Leads'
            ),
            array(
                'link' => route('lead.lead_detail', $lead_id . '#quotation'),
                'title' => isset($lead_info->name) ? $lead_info->name : 'Manage Lead'
            ),
            array(
                'title' => 'Edit Quotation'
            )
        );
        return view('admin/quotation/quotation-manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $response['success'] = false;
        $deleted = $this->getLeadQuatationLib()->deleteLeadQuatation($id);
        if ($deleted) {
            $response['success'] = true;
            $response['message'] = 'Quotation has been deleted successfully!';
            $response['msg_status'] = 'success';
        } else {
            $response['message'] = "Quotation can't deleted successfully!";
            $response['msg_status'] = 'warning';
        }
        exit(json_encode($response));
    }

    public function print_receipt($id, $type) {
        $payment_info = $this->getCustomerPaymentLib()->getCustomerPaymentById($id);
        $paymentData = $quotation = $particular = $itinarary = $transaction_data = array();
        if($payment_info &&  $payment_info->payment_mode == 'Online'){
            $transaction_data = $this->getPaymentLib()->getPaymentLeadNo($payment_info->transaction_id);

            if ($transaction_data) {
                if(isset($transaction_data->transaction_id) && $transaction_data->transaction_id!=''){
                    $paymentData = $this->getCustomerPaymentLib()->getQuotationId($transaction_data->transaction_id);
                    if(!empty($paymentData)){
                        if(isset($paymentData->lead_id) && isset($paymentData->quotation_id)){
                            $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($paymentData->lead_id, $paymentData->quotation_id);
                            $particular = $this->getLeadParticularLib()->getLeadParticularById($paymentData->lead_id, $paymentData->quotation_id);
                            $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($paymentData->lead_id, $paymentData->quotation_id);
                        }  
                    }
                }
                $data['payment_data'] = $paymentData;
                $data['transaction_data'] = $transaction_data;
                $data['quotation'] = $quotation;
                $data['particular'] = $particular;
                $data['itinarary'] = $itinarary;
                $lead_number_info = $this->getLeadLib()->getLeadByNuber($transaction_data->lead_id);
                $data['lead_info'] = $lead_info = $this->getLeadLib()->getLeadInfo($lead_number_info->id);
                if ($data['lead_info']) {
                    // $data['domestic_sectors'] = Cache::pull('domestic_sectors');
                    // $data['global_sectors'] = Cache::pull('global_sectors');
                    // $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
                    // $data['HolidaysTheme'] = Cache::pull('HolidaysTheme');

                    $html = view('client/payment/payment-receipt-template',['payment_data'=>$paymentData,'transaction_data'=>$transaction_data,'quotation'=>$quotation,'particular'=>$particular,'itinarary'=>$itinarary,'lead_info'=>$lead_info])->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($html);
                    return $pdf->stream();
                    return $pdf->download('invoice.pdf');
                } else {
                    return redirect()->route('lead.lead_detail', $payment_info->lead_id . '#payment');
                }
            } else {
                return redirect()->route('lead.lead_detail', $payment_info->lead_id . '#payment');
            }
        }elseif($payment_info &&  $payment_info->type == 'payment'){
            $data['lead_info'] = $this->getLeadLib()->getLeadInfo($payment_info->lead_id);
            $data['payment_info'] =  $payment_info;
            if ($data['lead_info']) {
                $html = view('client/payment/offline-payment-receipt-template',$data)->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($html);
                return $pdf->stream();
                return $pdf->download('invoice.pdf');
            } else {
                return redirect()->route('lead.lead_detail', $payment_info->lead_id . '#payment');
            }
        }
    }

    public function change_status(Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            try {
                $table = $request->table;
                $id = $request->id;
                $status = $request->status;
                $column_name = $request->column_name;

                $update_columns = "status";
                if (isset($request->column_update)) {
                    $update_columns = $request->column_update;
                }

                //update query fire
                DB::table($table)
                        ->where($column_name, $id)
                        ->update([$update_columns => $status]);
                if ($status == 3) {
                    $quotation = $this->getLeadQuatationLib()->getLeadQuatationRecordById($id);
                    $customerpayment = $this->getCustomerPaymentLib()->chechQuotationCustomerPayment($id);
                    $records = array(
                        "quotation_id" => $id
                    );
                    //$lastRecord = $this->getCustomerPaymentLib()->getCustomerPaymentlastRecord();
                    // if ($lastRecord) {
                    //     if (isset($lastRecord->balance) && $lastRecord->balance != null) {
                    //         if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                    //             $records['balance'] = $quotation->total_amount + $lastRecord->balance;
                    //         }
                    //     }
                    // } else {
                    //     $records['balance'] = $quotation->total_amount;
                    // }

                    // if (isset($quotation->lead_id) && $quotation->lead_id != null) {
                    //     $lead = $this->getLeadLib()->getLeadById($quotation->lead_id);
                    //     $records['lead_id'] = $quotation->lead_id;
                    //     if (isset($lead->customer_id) && $lead->customer_id != null) {
                    //         $records['customer_id'] = $lead->customer_id;
                    //     }
                    // }
                    // if (isset($quotation->tour_id) && $quotation->tour_id != null) {
                    //     $records['tour_id'] = $quotation->tour_id;
                    // }
                    // if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                    //     $records['total_amount'] = $quotation->total_amount;
                    // }

                    if(!empty($quotation)){
                        if(isset($quotation->lead_id) && $quotation->lead_id != null){
                            $lead = $this->getLeadLib()->getLeadById($quotation->lead_id);
                            $records['lead_id'] = $quotation->lead_id;
                            if (isset($lead->customer_id) && $lead->customer_id != null) {
                                $records['customer_id'] = $lead->customer_id;
                            }
                            $lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($quotation->lead_id);
                            if (!empty($lastRecord)) {
                                if (isset($lastRecord->balance) && $lastRecord->balance != null) {
                                    if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                                        $records['balance'] = $quotation->total_amount + $lastRecord->balance;
                                    }
                                }
                            } else {
                                $records['balance'] = $quotation->total_amount;
                            }
                        }

                        if (isset($quotation->tour_id) && $quotation->tour_id != null) {
                            $records['tour_id'] = $quotation->tour_id;
                        }
                        if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                            $records['total_amount'] = $quotation->total_amount;
                        } 
                    }

                    if (empty($customerpayment)) {
                        $this->getCustomerPaymentLib()->addCustomerPayment($records);
                    }

                    //   echo $this->show($quotation->lead_id, $quotation->quaId);

                    $lead_id = $quotation->lead_id;
                    $quotation_id = $quotation->quaId;
                    $data['lead'] = $lead = $this->getLeadLib()->getLeadInfo($lead_id);
                    $data['quotation'] = $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quotation_id);
//                            $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
//                            $quotation->quotation_status = $quotation_satus;

                    $data['particular'] = $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quotation_id);
                    $data['itinarary'] = $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quotation_id);
                    $invoice = $this->getInvoiceLib()->getInvoice('proforma', $lead->id, $quotation_id);
                    if (isset($invoice) && $invoice != null) {
                        $receipt_no = $invoice->receipt_number;
                    } else {
                        $receipt_no = $this->getReceipLib()->getProformaReceiptNumber();
                        $inset_data = array(
                            'lead_id' => $lead->id,
                            'type' => 'proforma',
                            'customer_id' => ( (isset($lead->customer_id) && $lead->customer_id > 0 ) ? $lead->customer_id : ''),
                            'receipt_number' => $receipt_no,
                            'quatation_id' => $quotation_id,
                            'created_at' => date("Y-m-d H:i:s")
                        );
                        $invoice = $this->getInvoiceLib()->insertInvoice($inset_data);
                        $this->getReceipLib()->updateProformaReceiptNumber();
                    }

                    $data['receipt_no'] =  $receipt_no; 
                    $data['user_info']= $user_info = $this->getUserLib()->getUserFullProfile($lead->customer_id);
                    // echo $contents = view('admin/quotation/quotation-email-template', $data)->render();die;
                    $d = array('email' => $lead->email, 'quot_id' => $quotation->quatation_id );
                    Mail::send('admin/quotation/quotation-email-template', $data, function($message)  use ($d )  {
                         $message->to($d['email'], 'Low Cost Trip (#'.$d['quot_id'].')')->subject
                            ('Low cost trip (#'.$d['quot_id'].')');
                         $message->from('info@lowcosttrip.com','Low Cost Trip');
                    });

                    // $contents = view('admin/quotation/quotation-template', ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary, 'receipt_no' => $receipt_no])->render();
                    // $data = array();
                    // $pdf = \App::make('dompdf.wrapper');
                    // $pdf->loadHTML($contents);
                    // $file_name = isset($lead->name) ? $lead->name . time() . '.pdf' : time() . '.pdf';
                    // $path = public_path() . '/pdf/';
                    // $file = public_path() . '/pdf/' . $file_name;
                    // if (!file_exists($path)) {
                    //     File::makeDirectory($path, $mode = 0777, true, true);
                    // }
                    // $pdf->save($file);
                    
                    // $data = array('email' => $lead->email, 'file' => $file, 'quot_id' => $quotation['quatation_id'] );
                    // Mail::send([], $data, function($message)  use ($data )  {
                    //      $message->to($data['email'], 'Low cost trip (#'.$data['quot_id'].')')->subject
                    //         ('Low cost trip (#'.$data['quot_id'].')');
                    //      $message->attach($data['file']);
                    //      $message->from('info@lowcosttrip.com','Low Cost Trip');
                    // });

                } else {
                    //Start
                    $current_balance = null;
                    $PaymentData = $this->getCustomerPaymentLib()->chechQuotationCustomerPayment($id);
                    if (!empty($PaymentData)) {
                        if (isset($PaymentData->created_at)) {
                            //$BalnceRecord = $this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at);
                            $BalnceRecord = $this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
                            if (!empty($BalnceRecord)) {
                                if (isset($BalnceRecord->balance) && $BalnceRecord->balance != null) {
                                    $current_balance = $BalnceRecord->balance - $PaymentData->balance;
                                }
                            }

                            //$AllDownRecord = $this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at);
                            $AllDownRecord = $this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
                            if (!empty($AllDownRecord)) {
                                foreach ($AllDownRecord as $key => $value) {
                                    $cur = $value->balance + $current_balance;
                                    $this->getCustomerPaymentLib()->updateCustomerPayment(['balance' => $cur], $value->id);
                                }
                            }
                        }
                        $this->getCustomerPaymentLib()->deleteCustomerPaymentQuotation($id);
                    }
                }
                $response['success'] = true;
                $response['msg_status'] = 'success';
                $response['message'] = "Status has been changed successfully";
            } catch (Exception $e) {
                $response['success'] = false;
                $response['msg_status'] = 'warning';
                $response['message'] = "Something went wrong, please try again";
            }
        }
        exit(json_encode($response));
    }

    function send_mail_to_user(Request $request)
    {
        $input = $request->all();
        if($input['email_id'] != ''){
            DB::table('lead_quatation')
                    ->where('quaId', $input['quotation_id'])
                    ->update(['status_id' => '2']);
            $quotation = $this->getLeadQuatationLib()->getLeadQuatationRecordById($input['quotation_id']);
            $lead_id = $quotation->lead_id;
            $quot_id = $quotation->quaId;
            $data['lead'] = $lead = $this->getLeadLib()->getLeadInfo($lead_id);
            $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quot_id); 
            $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
            $quotation->quotation_status = $quotation_satus;
            $data['quotation'] = $quotation;
            
            $data['particular'] = $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quot_id);
            $data['itinarary'] = $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quot_id);
            // ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary]
            $receipt_no = '';
            if($quotation->status_id == 3){
                $invoice = $this->getInvoiceLib()->getInvoice('proforma', $lead->id, $quot_id);
                if (isset($invoice) && $invoice != null) {
                    $receipt_no = $invoice->receipt_number;
                } else {
                    $receipt_no = $this->getReceipLib()->getProformaReceiptNumber();
                    $inset_data = array(
                        'lead_id' => $lead->id,
                        'type' => 'proforma',
                        'customer_id' => ( (isset($lead->customer_id) && $lead->customer_id > 0 ) ? $lead->customer_id : ''),
                        'receipt_number' => $receipt_no,
                        'quatation_id' => $quot_id,
                        'created_at' => date("Y-m-d H:i:s")
                    );
                    $invoice = $this->getInvoiceLib()->insertInvoice($inset_data);
                    $this->getReceipLib()->updateProformaReceiptNumber();
                }
            }
            $data['receipt_no'] =  $receipt_no; 
            $data['draft_message'] =  $input['message'];
            $data['user_name'] =  $input['name'];
            $data['user_info']= $user_info = $this->getUserLib()->getUserFullProfile($lead->customer_id);
            $d = array('email' =>$input['email_id'] , "subject" =>$input['subject'], 'cc' =>$input['email_cc'], 'quot_id' => $quotation->quatation_id );
            Mail::send('admin/quotation/quotation-email-template', $data, function($message)  use ($d )  {
                //$message->setBody('Hi, welcome user!'); // assuming text/plain
                $message->to($d['email'], $d['subject'].' (#'.$d['quot_id'].')');
                if($d['cc'] != ''){
                    $message->cc($d['cc']);
                }
                $message->subject($d['subject'].' (#'.$d['quot_id'].')');
                 // $message->attach($data['file']);
                $message->from('info@lowcosttrip.com','Low Cost Trip');
            });
        }

        return redirect()->route('lead.lead_detail', $lead_id . '#quotation');
         
    
    }

    public function print_proforma($id) {
        $quotation = $this->getLeadQuatationLib()->getLeadQuatationRecordById($id);
        $customerpayment = $this->getCustomerPaymentLib()->chechQuotationCustomerPayment($id);
        $records = array(
            "quotation_id" => $id
        );
        $lead_id = $quotation->lead_id;
        $quotation_id = $quotation->quaId;
        $lead = $this->getLeadLib()->getLeadInfo($lead_id);
        $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quotation_id);
        $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quotation_id);
        $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quotation_id);
        $invoice = $this->getInvoiceLib()->getInvoice('proforma', $lead->id, $quotation_id);
        $receipt_no = '';
        if (isset($invoice) && $invoice != null) {
            $receipt_no = $invoice->receipt_number;
        }
        $contents = view('admin/quotation/quotation-template', ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary, 'receipt_no' => $receipt_no])->render();
        $data = array();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        return $pdf->stream();
    }

}
