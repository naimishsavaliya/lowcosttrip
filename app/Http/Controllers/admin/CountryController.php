<?php

namespace App\Http\Controllers\admin;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use App\State;
use App\City;
use Validator;
use Session;

class CountryController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage City / State / Country'
            )
        );
        return view('admin/countries/index', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request) {

        return datatables()->of(Country::select('country_id', 'country_name', 'sortname', 'phonecode', 'status')->where('deleted_at', null))
                        ->addColumn('action', '')
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('country.index'),
                'title' => 'Manage City / State / Country'
            ),
            array(
                'title' => 'Add Country'
            )
        );
        return view('admin/countries/country-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'country_name' => 'required',
            'status' => 'required',
            'sortname' => 'required',
            'phonecode' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter country',
            'status.required' => 'Please select status',
            'sortname.required' => 'Please enter sortname',
            'phonecode.required' => 'Please enter phonecode',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('country.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('country.add')->withErrors($validator)->withInput();
            }
        } else {
            //set country Data
            $data['country_name'] = $request->country_name;
            $data['sortname'] = $request->sortname;
            $data['phonecode'] = $request->phonecode;
            $data['status'] = $request->status;

            if ($request->id > 0) {
                $this->getCountryLib()->updateCountry($data, $request->id);
                Session::flash('success_msg', 'Country has been updated successfully!');
            } else {
                $typeId = $this->getCountryLib()->addCountry($data);
                Session::flash('success_msg', 'Country has been saved successfully!');
            }
        }
        return redirect()->route('country.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country, $id) {
        $data['country'] = $this->getCountryLib()->getCountryById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('country.index'),
                'title' => 'Manage City / State / Country'
            ),
            array(
                'title' => 'Edit Country'
            )
        );

        return view('admin/countries/country-manage', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country, $id) {
        $deleted = $this->getCountryLib()->deleteCountry($id);
        if ($deleted) {
            Session::flash('success_msg', 'Country has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Country can't deleted!");
        }
        return redirect()->route('country.index');
    }

    /*
     * State list
     */

    public function statedata(Request $request) {

        return datatables()->of(State::select('state.state_id', 'state.state_name', 'country.country_name', 'state.status')->join('country', 'state.country_id', '=', 'country.country_id')->where('state.deleted_at', null))
                        ->addColumn('action', '')
                        ->toJson();
    }

    /*
     * State Add
     */

    public function state_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('country.index'),
                'title' => 'Manage City / State / Country'
            ),
            array(
                'title' => 'Add State'
            )
        );

        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        return view('admin/countries/state-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function state_store(Request $request) {
        $rules = [
            'country_id' => 'required',
            'state_name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'country_id.required' => 'Please select country',
            'state_name.required' => 'Please enter state name',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('state.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('state.add')->withErrors($validator)->withInput();
            }
        } else {
            //set Data
            $data['country_id'] = $request->country_id;
            $data['state_name'] = $request->state_name;
            $data['status'] = $request->status;

            if ($request->id > 0) {
                $this->getStateLib()->updateState($data, $request->id);
                Session::flash('success_msg', 'state has been updated successfully!');
            } else {
                $typeId = $this->getStateLib()->addState($data);
                Session::flash('success_msg', 'state has been saved successfully!');
            }
        }
        return redirect()->route('country.index');
    }

    /*
     * State Edit
     */

    public function state_edit($id) {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['state'] = $this->getStateLib()->getStateById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('country.index'),
                'title' => 'Manage City / State / Country'
            ),
            array(
                'title' => 'Edit State'
            )
        );

        return view('admin/countries/state-manage', $data);
    }

    /*
     * State Delete
     */

    public function state_delete($id) {
        $deleted = $this->getStateLib()->deleteState($id);
        if ($deleted) {
            Session::flash('success_msg', 'state has been deleted successfully!');
        } else {
            Session::flash('error_msg', "state can't deleted!");
        }
        return redirect()->route('country.index');
    }

    /*
     * City list
     */

    public function citydata(Request $request) {

        return datatables()->of(City::select('city.city_id', 'city.city_name', 'state.state_name', 'country.country_name', 'city.status')->join('state', 'city.state_id', '=', 'state.state_id')->join('country', 'state.country_id', '=', 'country.country_id')->where('city.deleted_at', null))
                        ->addColumn('action', '')
                        ->toJson();
    }

    /*
     * City Add
     */

    public function city_add() {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('country.index'),
                'title' => 'Manage City / State / Country'
            ),
            array(
                'title' => 'Add City'
            )
        );
        return view('admin/countries/city-manage', $data);
    }

    /*
     * City Edit
     */

    public function city_edit($id) {
        $data['city'] = $this->getCityLib()->getCityById($id);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['states'] = $this->getStateLib()->getStateByCountry($data['city']->country_id, array('state_id', 'state_name'));

        $data['breadcrumb'] = array(
            array(
                'link' => route('country.index'),
                'title' => 'Manage City / State / Country'
            ),
            array(
                'title' => 'Edit City'
            )
        );

        return view('admin/countries/city-manage', $data);
    }

    /*
     * Store city
     */

    public function city_store(Request $request) {
        $rules = [
            'country_id' => 'required',
            'state_id' => 'required',
            'city_name' => 'required',
            'city_code' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'country_id.required' => 'Please select country',
            'state_id.required' => 'Please select state',
            'city_name.required' => 'Please enter city name',
            'city_code.required' => 'Please enter city code',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('city.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('city.add')->withErrors($validator)->withInput();
            }
        } else {
            //set Data
            $data['country_id'] = $request->country_id;
            $data['state_id'] = $request->state_id;
            $data['city_name'] = $request->city_name;
            $data['city_code'] = $request->city_code;
            $data['status'] = $request->status;

            if ($request->id > 0) {
                $this->getCityLib()->updateCity($data, $request->id);
                Session::flash('success_msg', 'City has been updated successfully!');
            } else {
                $typeId = $this->getCityLib()->addCity($data);
                Session::flash('success_msg', 'City has been saved successfully!');
            }
        }
        return redirect()->route('country.index');
    }

    /*
     * City Delete
     */

    public function city_destroy($id) {
        $deleted = $this->getCityLib()->deleteCity($id);
        if ($deleted) {
            Session::flash('success_msg', 'City has been deleted successfully!');
        } else {
            Session::flash('error_msg', "City can't deleted!");
        }
        return redirect()->route('country.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country) {
        
    }

}
