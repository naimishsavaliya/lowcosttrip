<?php

namespace App\Http\Controllers\admin;

use App\Website;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Session;
use Validator;
use File;

class WebsiteController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function show(Website $website) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function edit(Website $website) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Website $website) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function destroy(Website $website) {
        //
    }

    public function homebanner() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Home Page Banners'
            )
        );
        return view('admin/website/website-homebanner', $data);
    }

    public function homebanner_add() {
        $data['banner_option'] = $this->getCommonLib()->getHomepagebanneroption();
        $data['banner_category'] = $this->getCommonLib()->getHomepagebannercategory();

        $data['breadcrumb'] = array(
            array(
                'link' => route('website.banner.list'),
                'title' => 'Manage Home Page Banners'
            ),
            array(
                'title' => 'Add Banners'
            )
        );

        return view('admin/website/website-homebanner-manage', $data);
    }

    public function homebanner_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Homebanner::select('banId', 'title', 'tooltip', 'banner_opt', 'banner_cat', 'redirect_to', 'status', 'created_at')
                                    ->where('deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->addColumn('action', '')
                            //->rawColumns(['description', 'status', 'action'])
                            ->toJson();
        }
    }

    public function homebanner_edit($id) {
        $data['banner_option'] = $this->getCommonLib()->getHomepagebanneroption();
        $data['banner_category'] = $this->getCommonLib()->getHomepagebannercategory();
        $data['home_banner'] = $this->getHomebannerLib()->getHomebannerById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('website.banner.list'),
                'title' => 'Manage Home Page Banners'
            ),
            array(
                'title' => 'Edit Banners'
            )
        );

        return view('admin/website/website-homebanner-manage', $data);
    }

    /*
     * Home page banner Save.s
     */

    public function homebanner_store(Request $request) {
        //validate certificate data
        $rules = [
            'name' => 'required',
            'banner_option' => 'required',
            'banner_category' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter banner title',
            'banner_option.required' => 'Please select banner option',
            'banner_category.required' => 'Please select banner category',
            'status.required' => 'Please select  status',
        ];

//        if (isset($request->id) == false) {
//            $rules['image_1'] = 'required';
//            $messages['image.required'] = 'Please upload banner image';
//        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.banner.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.banner.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['title'] = $request->name;
            $data['tooltip'] = $request->tooltip;
            $data['banner_opt'] = $request->banner_option;
            $data['banner_cat'] = $request->banner_category;
            $data['redirect_to'] = $request->redirect_to;
            $data['description'] = $request->description;
            $data['sort_ord'] = $request->sort_ord;
            $data['status'] = $request->status;

            //Upload Home banner image1
            $image1 = $request->file('image_1');
            $destinationPath = public_path('/uploads/home_banner');
            if (!empty($image1)) {
                $imagename1 = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . '.' . $image1->getClientOriginalExtension();
                $image1->move($destinationPath, $imagename1);
                $data['image_1'] = $imagename1;
            }

            //Upload Home banner image2
            $image2 = $request->file('image_2');
            if (!empty($image2)) {
                $imagename2 = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . '.' . $image2->getClientOriginalExtension();
                $image2->move($destinationPath, $imagename2);
                $data['image_2'] = $imagename2;
            }

            //Upload Home banner image3
            $image3 = $request->file('image_3');
            if (!empty($image3)) {
                $imagename3 = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . '.' . $image3->getClientOriginalExtension();
                $image3->move($destinationPath, $imagename3);
                $data['image_3'] = $imagename3;
            }

            if (isset($request->id)) {
                $get_banner = $this->getHomebannerLib()->getHomebannerById($request->id, array('banId', 'image_1', 'image_2', 'image_3'));
                if ($request->banner_option == 1) {
                    $data['image_2'] = '';
                    $data['image_3'] = '';
                    $image_old_2 = $destinationPath . '/' . $get_banner->image_2;
                    if (File::exists($image_old_2)) {
                        File::delete($image_old_2);
                    }

                    $image_old_3 = $destinationPath . '/' . $get_banner->image_3;
                    if (File::exists($image_old_3)) {
                        File::delete($image_old_3);
                    }
                }
                if ($request->banner_option == 2) {
                    $data['image_3'] = '';

                    $image_old_3 = $destinationPath . '/' . $get_banner->image_3;
                    if (File::exists($image_old_3)) {
                        File::delete($image_old_3);
                    }
                }
            }

            if (isset($request->id)) {
                $this->getHomebannerLib()->updateHomebanner($data, $request->id);
                Session::flash('success_msg', 'Home Banner has been updated successfully!');
            } else {
                $this->getHomebannerLib()->addHomebanner($data);
                Session::flash('success_msg', 'Home Banner has been saved successfully!');
            }
        }
        return redirect()->route('website.banner.list');
    }

    public function homebanner_delete($id) {
        $deleted = $this->getHomebannerLib()->deleteHomebanner($id);
        if ($deleted) {
            Session::flash('success_msg', 'Home Banner has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Home Banner can't deleted successfully!");
        }
        return redirect()->route('website.banner.list');
    }

    public function featured_tours() {
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Featured Tours'
            )
        );
        return view('admin/website/website-featured-tours', $data);
    }

    /*
     * Featured Tour Data
     * Json
     */

    public function featured_tours_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Featuredtour::select('featured_tour.id', 'featured_tour.sort_no', 'featured_tour.status', 'featured_tour.created_at', 'tour.tour_code', 'tour.image', 'master_tour_type.name')
                                    ->leftjoin('tour', 'featured_tour.tour_id', '=', 'tour.id')
                                    ->leftjoin('master_tour_type', 'featured_tour.type_id', '=', 'master_tour_type.typeId')
                                    ->where('featured_tour.deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('image', function ($model) {
                                return \App\Helpers\S3url::s3_get_image($model->image);
                            })
                            ->filterColumn('featured_tour.created_at', function($query, $keyword) {
                                $search_date = date('Y-m-d', strtotime($keyword));
                                $query->whereRaw("DATE_FORMAT(featured_tour.created_at, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
                            })
                            ->addColumn('action', '')
                            ->rawColumns(['image'])
                            ->toJson();
        }
    }

    public function featured_tours_add() {
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['tours'] = $this->getTourLib()->getTour(array('id', 'tour_name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.featured.tour'),
                'title' => 'Manage Featured Tours'
            ),
            array(
                'title' => 'Add Featured Tour'
            )
        );
        return view('admin/website/website-featured-tours-manage', $data);
    }

    public function featured_tours_edit($id) {
        $data['featured_tour'] = $this->getFeaturedTourLib()->getFeaturedtourById($id);
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['tours'] = $this->getTourLib()->getTour(array('id', 'tour_name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.featured.tour'),
                'title' => 'Manage Featured Tours'
            ),
            array(
                'title' => 'Edit Featured Tour'
            )
        );
        return view('admin/website/website-featured-tours-manage', $data);
    }

    /*
     * Store Featured Store
     * DB
     */

    public function featured_tours_store(Request $request) {
        //validate certificate data
        $rules = [
            'type_id' => 'required',
            'tour_id' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'type_id.required' => 'Please select tour type',
            'tour_id.required' => 'Please select tour',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.featured.tour.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.featured.tour.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['type_id'] = $request->type_id;
            $data['tour_id'] = $request->tour_id;
            $data['status'] = $request->status;

            if (isset($request->id)) {
                $this->getFeaturedTourLib()->updateFeaturedtour($data, $request->id);
                Session::flash('success_msg', 'Feature Tour has been updated successfully!');
            } else {
                $this->getFeaturedTourLib()->addFeaturedtour($data);
                Session::flash('success_msg', 'Feature Tour has been saved successfully!');
            }
        }
        return redirect()->route('website.featured.tour');
    }

    public function featured_tours_delete($id) {
        $deleted = $this->getHotTourLib()->deleteHottours($id);
        if ($deleted) {
            Session::flash('success_msg', 'Hot Banner has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Hot Tour can't deleted successfully!");
        }
        return redirect()->route('website.banner.list');
    }

    public function featured_destinations() {
        $data = array();
        return view('admin/website/website-featured-destinations', $data);
    }

    public function featured_destinations_add() {
        $data = array();
        return view('admin/website/website-featured-destinations-manage', $data);
    }

    public function featured_destinations_edit(Request $request, Website $website) {
        $data = array();
        return view('admin/website/website-featured-destinations-manage', $data);
    }

    public function featured_destinations_delete(Website $website) {
        $data = array("deleted" => true);
        return view('admin/website/website-featured-destinations', $data);
    }

    public function hot_tours() {
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Hot Tours'
            )
        );
        return view('admin/website/website-hot-tours', $data);
    }

    public function hot_tours_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Hottours::select('hot_tours.id', 'hot_tours.name', 'hot_tours.sort_no', 'hot_tours.status', 'hot_tours.created_at', 'tour.tour_code', 'tour.image', 'master_tour_type.name as title')
                                    ->leftjoin('tour', 'hot_tours.tour_id', '=', 'tour.id')
                                    ->leftjoin('master_tour_type', 'hot_tours.type_id', '=', 'master_tour_type.typeId')
                                    ->where('hot_tours.deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('image', function ($model) {
                                return \App\Helpers\S3url::s3_get_image($model->image);
                            })
                            ->filterColumn('hot_tours.created_at', function($query, $keyword) {
                                $search_date = date('Y-m-d', strtotime($keyword));
                                $query->whereRaw("DATE_FORMAT(hot_tours.created_at, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
                            })
                            ->addColumn('action', '')
                            ->rawColumns(['image'])
                            ->toJson();
        }
    }

    public function hot_tours_add() {
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['tours'] = $this->getTourLib()->getTour(array('id', 'tour_name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.hottour.index'),
                'title' => 'Manage Hot Tours'
            ),
            array(
                'title' => 'Add Featured Tour'
            )
        );
        return view('admin/website/website-hot-tours-manage', $data);
    }

    public function hot_tours_edit($id) {
        $data['featured_tour'] = $this->getHotTourLib()->getHottoursById($id);
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['tours'] = $this->getTourLib()->getTour(array('id', 'tour_name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.hottour.index'),
                'title' => 'Manage Hot Tours'
            ),
            array(
                'title' => 'Edit Featured Tour'
            )
        );
        return view('admin/website/website-hot-tours-manage', $data);
    }

    public function hot_tours_store(Request $request) {
        //validate certificate data
        $rules = [
            'name' => 'required',
            'type_id' => 'required',
            'tour_id' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter name',
            'type_id.required' => 'Please select tour type',
            'tour_id.required' => 'Please select tour',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.hottour.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.hottour.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['name'] = $request->name;
            $data['type_id'] = $request->type_id;
            $data['tour_id'] = $request->tour_id;
            $data['status'] = $request->status;

            if (isset($request->id)) {
                $this->getHotTourLib()->updateHottours($data, $request->id);
                Session::flash('success_msg', 'Hot Tour has been updated successfully!');
            } else {
                $this->getHotTourLib()->addHottours($data);
                Session::flash('success_msg', 'Hot Tour has been saved successfully!');
            }
        }
        return redirect()->route('website.hottour.index');
    }

    public function hot_tours_delete($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getHotTourLib()->deleteHottours($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Hot Tour has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Hot Tour can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    public function international_tours() {
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Internatioanl Tour'
            )
        );
        return view('admin/website/website-international-tours', $data);
    }

    public function international_tours_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Internationaltour::select('international_tours.id', 'international_tours.name', 'international_tours.sort_no', 'international_tours.status', 'international_tours.created_at', 'tour.tour_code', 'tour.image', 'master_tour_type.name as title')
                                    ->leftjoin('tour', 'international_tours.tour_id', '=', 'tour.id')
                                    ->leftjoin('master_tour_type', 'international_tours.type_id', '=', 'master_tour_type.typeId')
                                    ->where('international_tours.deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('image', function ($model) {
                                return \App\Helpers\S3url::s3_get_image($model->image);
                            })
                            ->filterColumn('international_tours.created_at', function($query, $keyword) {
                                $search_date = date('Y-m-d', strtotime($keyword));
                                $query->whereRaw("DATE_FORMAT(international_tours.created_at, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
                            })
                            ->addColumn('action', '')
                            ->rawColumns(['image'])
                            ->toJson();
        }
    }

    public function international_tours_add() {
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['tours'] = $this->getTourLib()->getTour(array('id', 'tour_name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.international.tour'),
                'title' => 'Manage Internatioanl Tour'
            ),
            array(
                'title' => 'Add Internatioanl Tour'
            )
        );
        return view('admin/website/website-international-tours-manage', $data);
    }

    public function international_tours_edit($id) {
        $data['featured_tour'] = $this->getInternationalTourLib()->getInternationaltoursById($id);
        $data['tour_type'] = $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name'));
        $data['tours'] = $this->getTourLib()->getTour(array('id', 'tour_name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.international.tour'),
                'title' => 'Manage Internatioanl Tour'
            ),
            array(
                'title' => 'Edit Internatioanl Tour'
            )
        );
        return view('admin/website/website-international-tours-manage', $data);
    }

    public function international_tours_store(Request $request) {
        //validate certificate data
        $rules = [
            'name' => 'required',
            'type_id' => 'required',
            'tour_id' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter name',
            'type_id.required' => 'Please select tour type',
            'tour_id.required' => 'Please select tour',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.international.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.international.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['name'] = $request->name;
            $data['type_id'] = $request->type_id;
            $data['tour_id'] = $request->tour_id;
            $data['status'] = $request->status;

            if (isset($request->id)) {
                $this->getInternationalTourLib()->updateInternationaltours($data, $request->id);
                Session::flash('success_msg', 'International Tour has been updated successfully!');
            } else {
                $this->getInternationalTourLib()->addInternationaltours($data);
                Session::flash('success_msg', 'International Tour has been saved successfully!');
            }
        }
        return redirect()->route('website.international.tour');
    }

    public function international_tours_delete($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getInternationalTourLib()->deleteInternationaltours($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'International Tour has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "International Tour can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    /*
     * Hotel Prmoted List
     */

    public function hotel_promoted() {
        $data['hotels'] = $this->getHotelLib()->getHotel(array('id', 'name'), array('status' => 1));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Hotel Promoted'
            )
        );
        return view('admin/website/website-prmoted', $data);
    }

    /*
     * Return Hotel Promoted Data
     * JSON
     */

    public function hotel_promoted_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Hotelpromote::select('hotel_promote.id', 'hotel_promote.name', 'hotel.name as hotel_name', 'hotel_promote.start_date', 'hotel_promote.end_date', 'hotel_promote.status', 'hotel_promote.created_at')
                                    ->leftjoin('hotel', 'hotel_promote.hotel_id', '=', 'hotel.id')
                                    ->where('hotel_promote.deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('start_date', function ($model) {
                                if (!empty($model->start_date)) {
                                    return date("d M Y", strtotime($model->start_date));
                                } else {
                                    return '';
                                }
                            })
                            ->editColumn('end_date', function ($model) {
                                if (!empty($model->end_date)) {
                                    return date("d M Y", strtotime($model->end_date));
                                } else {
                                    return '';
                                }
                            })
                            ->filterColumn('hotel_promote.start_date', function($query, $keyword) {
                                $start_date = date('Y-m-d', strtotime($keyword));
                                $query->whereRaw("DATE_FORMAT(hotel_promote.start_date, '%Y-%m-%d') >= '" . $start_date . "'");
                            })
                            ->filterColumn('hotel_promote.end_date', function($query, $end_date) {
                                $end_date = date('Y-m-d', strtotime($end_date));
                                $query->whereRaw("DATE_FORMAT(hotel_promote.end_date, '%Y-%m-%d') <= '" . $end_date . "'");
                            })
                            ->addColumn('action', '')
//                            ->rawColumns(['status', 'action'])
                            ->toJson();
        }
    }

    /*
     * Hotel Prmoted Add
     */

    public function hotel_promoted_add() {
        $data['hotels'] = $this->getHotelLib()->getHotel(array('id', 'name'), array('status' => 1));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.hotel.promoted'),
                'title' => 'Manage Hotel Promoted'
            ),
            array(
                'title' => 'Add Hotel Promoted'
            )
        );
        return view('admin/website/website-prmoted-mange', $data);
    }

    /*
     * Hotel Prmoted Edit
     */

    public function hotel_promoted_edit($id) {
        $data['hotelpromote'] = $this->getHotelPromoteLib()->getHotelpromoteById($id);
        $data['hotels'] = $this->getHotelLib()->getHotel(array('id', 'name'), array('status' => 1));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.hotel.promoted'),
                'title' => 'Manage Hotel Promoted'
            ),
            array(
                'title' => 'Edit Hotel Promoted'
            )
        );
        return view('admin/website/website-prmoted-mange', $data);
    }

    /*
     * Hotel Promoted Store
     */

    public function hotel_promoted_store(Request $request) {
        $rules = [
            'hotel_id' => 'required',
            'name' => 'required',
            'link' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'hotel_id.required' => 'Please select hotel',
            'name.required' => 'Please enter name',
            'link.required' => 'Please enter url',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.hotel.promoted.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.hotel.promoted.add')->withErrors($validator)->withInput();
            }
        } else {

            $data = array(
                'hotel_id' => $request->hotel_id,
                'name' => $request->name,
                'sequence' => $request->sequence,
                'link' => $request->link,
                'status' => $request->status,
            );

            if (isset($request->start_date) && !empty($request->start_date)) {
                $data['start_date'] = date('Y-m-d', strtotime($request->start_date));
            }
            if (isset($request->end_date) && !empty($request->end_date)) {
                $data['end_date'] = date('Y-m-d', strtotime($request->end_date));
            }

            $file = $request->file('image');
            if (!empty($file)) {
                $imagename = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $file->getClientOriginalExtension();
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'hotel-promote/' . $imagename, file_get_contents($file->getRealPath())
                );
                $data['image'] = "hotel-promote/" . $imagename;
            }

            if ($request->id > 0) {
                $this->getHotelPromoteLib()->updateHotelpromote($data, $request->id);
                Session::flash('success_msg', 'Hotel Promoted updated successfully!');
            } else {
                $this->getHotelPromoteLib()->addHotelpromote($data);
                Session::flash('success_msg', 'Hotel Promoted saved successfully!');
            }
        }

        return redirect()->route('website.hotel.promoted');
    }

    /*
     * Hotel Prmoted Delete
     */

    public function hotel_promoted_destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getHotelPromoteLib()->deleteHotelpromote($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Hotel Promoted has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Hotel Promoted can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    /*
     * Hotel best deal list
     */

    public function hotel_bestdeal() {
        $data = array();
        return view('admin/website/hotel-bestdeal', $data);
    }

    /*
     * Hotel best deal Add
     */

    public function hotel_bestdeal_add() {
        $data = array();
        return view('admin/website/hotel-bestdeal-mange', $data);
    }

    /*
     * Hotel best deal Edit
     */

    public function hotel_bestdeal_edit($id) {
        $data = array();
        return view('admin/website/hotel-bestdeal-mange', $data);
    }

    /*
     * Hotel best deal Delete
     */

    public function hotel_bestdeal_destroy($id) {
        $data = array();
        return view('admin/website/hotel-bestdeal', $data);
    }

    /*
     * Hotel Featured List
     */

    public function hotel_featured() {
        $data['hotels'] = $this->getHotelLib()->getHotel(array('id', 'name'), array('status' => 1));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Featured Hotel'
            )
        );
        return view('admin/website/hotel-featured', $data);
    }

    /*
     * Return Featured Hotel Data
     */

    public function hotel_featured_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Hotelfeatured::select('hotel_featured.id', 'hotel_featured.name', 'hotel.name as hotel_name', 'hotel_featured.start_date', 'hotel_featured.end_date', 'hotel_featured.status', 'hotel_featured.created_at')
                                    ->leftjoin('hotel', 'hotel_featured.hotel_id', '=', 'hotel.id')
                                    ->where('hotel_featured.deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('start_date', function ($model) {
                                if (!empty($model->start_date)) {
                                    return date("d M Y", strtotime($model->start_date));
                                } else {
                                    return '';
                                }
                            })
                            ->editColumn('end_date', function ($model) {
                                if (!empty($model->end_date)) {
                                    return date("d M Y", strtotime($model->end_date));
                                } else {
                                    return '';
                                }
                            })
                            ->filterColumn('hotel_featured.start_date', function($query, $keyword) {
                                $start_date = date('Y-m-d', strtotime($keyword));
                                $query->whereRaw("DATE_FORMAT(hotel_featured.start_date, '%Y-%m-%d') >= '" . $start_date . "'");
                            })
                            ->filterColumn('hotel_featured.end_date', function($query, $end_date) {
                                $end_date = date('Y-m-d', strtotime($end_date));
                                $query->whereRaw("DATE_FORMAT(hotel_featured.end_date, '%Y-%m-%d') <= '" . $end_date . "'");
                            })
                            ->addColumn('action', '')
//                            ->rawColumns(['status', 'action'])
                            ->toJson();
        }
    }

    /*
     * Hotel Featured Add
     */

    public function hotel_featured_add() {
        $data['hotels'] = $this->getHotelLib()->getHotel(array('id', 'name'), array('status' => 1));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.hotel.featured'),
                'title' => 'Manage Featured Hotel'
            ),
            array(
                'title' => 'Add Featured Hotel'
            )
        );
        return view('admin/website/hotel-featured-manage', $data);
    }

    /*
     * Hotel Featured Edit
     */

    public function hotel_featured_edit($id) {
        $data['hotelpromote'] = $this->getHotelFeaturedLib()->getHotelfeaturedById($id);
        $data['hotels'] = $this->getHotelLib()->getHotel(array('id', 'name'), array('status' => 1));
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.hotel.featured'),
                'title' => 'Manage Featured Hotel'
            ),
            array(
                'title' => 'Edit Featured Hotel'
            )
        );
        return view('admin/website/hotel-featured-manage', $data);
    }

    /*
     * Store featured hotel
     */

    public function hotel_featured_store(Request $request) {
        $rules = [
            'hotel_id' => 'required',
            'name' => 'required',
            'link' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'hotel_id.required' => 'Please select hotel',
            'name.required' => 'Please enter name',
            'link.required' => 'Please enter url',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.hotel.featured.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.hotel.featured.add')->withErrors($validator)->withInput();
            }
        } else {

            $data = array(
                'hotel_id' => $request->hotel_id,
                'name' => $request->name,
                'sequence' => $request->sequence,
                'link' => $request->link,
                'status' => $request->status,
            );

            if (isset($request->start_date) && !empty($request->start_date)) {
                $data['start_date'] = date('Y-m-d', strtotime($request->start_date));
            }
            if (isset($request->end_date) && !empty($request->end_date)) {
                $data['end_date'] = date('Y-m-d', strtotime($request->end_date));
            }

            $file = $request->file('image');
            if (!empty($file)) {
                $imagename = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $file->getClientOriginalExtension();
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'hotel-featured/' . $imagename, file_get_contents($file->getRealPath())
                );
                $data['image'] = "hotel-featured/" . $imagename;
            }

            if ($request->id > 0) {
                $this->getHotelFeaturedLib()->updateHotelfeatured($data, $request->id);
                Session::flash('success_msg', 'Hotel Featured updated successfully!');
            } else {
                $this->getHotelFeaturedLib()->addHotelfeatured($data);
                Session::flash('success_msg', 'Hotel Featured saved successfully!');
            }
        }

        return redirect()->route('website.hotel.featured');
    }

    /*
     * Hotel Featured Delete
     */

    public function hotel_featured_destroy($id) {
        $data = array();
        return view('admin/website/hotel-featured', $data);
    }

    /*
     * Activity Deal List
     */

    public function activity_deal() {
        $data = array();
        return view('admin/website/activity-deal', $data);
    }

    /*
     * Activity Deal Add
     */

    public function activity_deal_add() {
        $data = array();
        return view('admin/website/activity-deal-manage', $data);
    }

    /*
     * Activity Deal Edit
     */

    public function activity_deal_edit($id) {
        $data = array();
        return view('admin/website/activity-deal-manage', $data);
    }

    /*
     * Activity Deal Delete
     */

    public function activity_deal_destroy($id) {
        $data = array();
        return view('admin/website/activity-deal', $data);
    }

    /*
     * Flight Deal List
     */

    public function flight_deal() {
        $data = array();
        return view('admin/website/flight-deal', $data);
    }

    /*
     * Flight Deal Add
     */

    public function flight_deal_add() {
        $data = array();
        return view('admin/website/flight-deal-manage', $data);
    }

    /*
     * Flight Deal Edit
     */

    public function flight_deal_edit($id) {
        $data = array();
        return view('admin/website/flight-deal-manage', $data);
    }

    /*
     * Flight Deal De;ete
     */

    public function flight_deal_destroy($id) {
        $data = array();
        return view('admin/website/flight-deal', $data);
    }

    /*
     * Popup List
     */

    public function popup() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Pop Up'
            )
        );
        return view('admin/website/popup', $data);
    }

    /*
     * 
     */

    public function popup_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Webpopup::select('id', 'name', 'redirect_link', 'start_date', 'end_date', 'status', 'created_at')
                                    ->where('deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->filterColumn('start_date', function($query, $keyword) {
                                $start_date = date('Y-m-d', strtotime($keyword));
                                $query->whereRaw("DATE_FORMAT(start_date, '%Y-%m-%d') >= '" . $start_date . "'");
                            })
                            ->filterColumn('end_date', function($query, $end_date) {
                                $end_date = date('Y-m-d', strtotime($end_date));
                                $query->whereRaw("DATE_FORMAT(end_date, '%Y-%m-%d') <= '" . $end_date . "'");
                            })
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /*
     * Popup Add
     */

    public function popup_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.popup'),
                'title' => 'Manage Pop Up'
            ),
            array(
                'title' => 'Add Pop Up'
            )
        );
        return view('admin/website/popup-manage', $data);
    }

    /*
     * Popup Edit
     */

    public function popup_edit($id) {
        $data['popup'] = $this->getWebpopupLib()->getWebpopupById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.popup'),
                'title' => 'Manage Pop Up'
            ),
            array(
                'title' => 'Edit Pop Up'
            )
        );
        return view('admin/website/popup-manage', $data);
    }

    /*
     * Popup store
     */

    public function popup_store(Request $request) {
        //validate certificate data
        $rules = [
            'name' => 'required',
            'redirect_link' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter name',
            'redirect_link.required' => 'Please enter redirect url',
            'status.required' => 'Please select  status',
        ];

        if (isset($request->id) == false) {
            $rules['image'] = 'required';
            $messages['image.required'] = 'Please upload image';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.popup.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.popup.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['name'] = $request->name;
            $data['redirect_link'] = $request->redirect_link;
            $data['status'] = $request->status;
            if (!empty($request->start_date)) {
                $data['start_date'] = date('Y-m-d', strtotime($request->start_date));
            }
            if (!empty($request->end_date)) {
                $data['end_date'] = date('Y-m-d', strtotime($request->end_date));
            }
            $file = $request->file('image');
            if (!empty($file)) {
                $imagename = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $file->getClientOriginalExtension();
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'popup/' . $imagename, file_get_contents($file->getRealPath())
                );
                $data['image'] = "popup/" . $imagename;
            }
            if ($request->id > 0) {
                $this->getWebpopupLib()->updateWebpopup($data, $request->id);
                Session::flash('success_msg', 'Web popup has been updated successfully!');
            } else {
                $this->getWebpopupLib()->addWebpopup($data);
                Session::flash('success_msg', 'Web popup has been saved successfully!');
            }
        }
        return redirect()->route('website.popup');
    }

    /*
     * Popup Delete
     */

    public function popup_destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getWebpopupLib()->deleteWebpopup($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Web Pop-up has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Web Pop-up can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    /*
     * Gallery List
     */

    public function gallery() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Album Category'
            )
        );
        return view('admin/website/gallery', $data);
    }

    /*
     * 
     */

    public function gallery_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Albumcategory::select('albId', 'title', 'image', 'status', 'created_at')
                                    ->where('deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('image', function ($model) {
                                return url('/public/uploads/album_category') . '/' . $model->image;
                            })
                            ->addColumn('action', '')
                            ->rawColumns(['status', 'action'])
                            ->toJson();
        }
    }

    /*
     * Gallery Add
     */

    public function gallery_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.gallery'),
                'title' => 'Manage Album Category'
            ),
            array(
                'title' => 'Add Album Category'
            )
        );
        return view('admin/website/gallery-manage', $data);
    }

    /*
     * Gallery Edit
     */

    public function gallery_edit($id) {
        $data['album_category'] = $this->getAlbumcategoryLib()->getAlbumcategoryById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('website.gallery'),
                'title' => 'Manage Album Category'
            ),
            array(
                'title' => 'Edit Album Category'
            )
        );

        return view('admin/website/gallery-manage', $data);
    }

    /*
     * Gallery category store
     */

    public function gallery_store(Request $request) {
        //validate certificate data
        $rules = [
            'title' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'title.required' => 'Please enter album category title',
            'status.required' => 'Please select status',
        ];

        if (isset($request->id) == false) {
            $rules['image'] = 'required';
            $messages['image.required'] = 'Please upload album image';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.gallery.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.gallery.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['title'] = $request->title;
            $data['status'] = $request->status;

            //Upload destination image
            $image = $request->file('image');
            if (!empty($image)) {
                $imagename = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/album_category');
                $image->move($destinationPath, $imagename);
                $data['image'] = $imagename;
            }

            if (isset($request->id)) {
                $this->getAlbumcategoryLib()->updateAlbumcategory($data, $request->id);
                Session::flash('success_msg', 'Album category has been updated successfully!');
            } else {
                $this->getAlbumcategoryLib()->addAlbumcategory($data);
                Session::flash('success_msg', 'Album category has been saved successfully!');
            }
        }
        return redirect()->route('website.gallery');
    }

    /*
     * Gallery Delete
     */

    public function gallery_destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getAlbumcategoryLib()->deleteAlbumcategory($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Album category has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Album category can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    /*
     * Push Notification List
     */

    public function pushnotification() {
        $data = array();
        return view('admin/website/pushnotification', $data);
    }

    /*
     * Push Notification Add
     */

    public function pushnotification_add() {
        $data = array();
        return view('admin/website/pushnotification-manage', $data);
    }

    /*
     * Push Notification Edit
     */

    public function pushnotification_edit($id) {
        $data = array();
        return view('admin/website/pushnotification-manage', $data);
    }

    /*
     * Push Notification Delete
     */

    public function pushnotification_destroy($id) {
        $data = array();
        return view('admin/website/pushnotification', $data);
    }

    /*
     * Search Key Words
     */

    public function search_keywords() {
        $data = array();
        return view('admin/website/search-keywords', $data);
    }

    /*
     * Manage Album
     */

    public function manage_album() {
        $data['album_category'] = $this->getAlbumcategoryLib()->getAlbumcategory(array('albId', 'title'));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Album',
            )
        );
        return view('admin/website/manage-album', $data);
    }

    /*
     * Return manage album data
     */

    public function manage_album_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Albums::select('albums.albId', 'albums.title', 'albums.customer_name', 'albums.album_date', 'albums.status', 'albums.created_at', \DB::raw("GROUP_CONCAT(album_category.title) as category"))
                                    ->leftJoin('album_category', function($join) {
                                        $join->whereRaw(\DB::raw("find_in_set(album_category.albId, albums.album_cat)"));
                                    })
                                    ->where('albums.deleted_at', null)
                                    ->groupBy('albums.albId'))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('album_date', function ($model) {
                                return date("d M Y", strtotime($model->album_date));
                            })
                            ->filterColumn('albums.album_date', function($query, $keyword) {
                                $search_date = date('Y-m-d', strtotime($keyword));
                                $query->whereRaw("DATE_FORMAT(albums.album_date, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
                            })
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /*
     * Manage Album Add
     */

    public function manage_album_add() {
        $data['album_category'] = $this->getAlbumcategoryLib()->getAlbumcategory(array('albId', 'title'));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Album',
                'link' => route('website.manage.album'),
            ),
            array(
                'title' => 'Add Album'
            )
        );
        return view('admin/website/manage-album-add', $data);
    }

    /*
     * Manage Album Edit
     */

    public function manage_album_edit($id) {
        $data['album'] = $this->getAlbumsLib()->getAlbumsById($id);
        $data['album_category'] = $this->getAlbumcategoryLib()->getAlbumcategory(array('albId', 'title'));
        $album_catArr = explode(',', $data['album']->album_cat);
        $data['album_cat_arr'] = $album_catArr;
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Album',
                'link' => route('website.manage.album'),
            ),
            array(
                'title' => 'Edit Album'
            )
        );
        return view('admin/website/manage-album-add', $data);
    }

    public function manage_album_store(Request $request) {
        //validate certificate data
        $rules = [
            'title' => 'required',
            'category_id' => 'required',
            'customer_name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'title.required' => 'Please enter album category title',
            'category_id.required' => 'Please select album category',
            'customer_name.required' => 'Please enter customer name',
            'status.required' => 'Please select status',
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('website.manage.album.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('website.manage.album.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['title'] = $request->title;
            if (count($request->category_id) > 0) {
                $data['album_cat'] = implode(',', $request->category_id);
            }
            if (!empty($request->album_date)) {
                $data['album_date'] = date('Y-m-d', strtotime($request->album_date));
            }
            $data['customer_name'] = $request->customer_name;
            $data['status'] = $request->status;

            //Upload destination image
            $image = $request->file('images');
            if (!empty($image)) {
                $imagename = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/albums');
                $image->move($destinationPath, $imagename);
                $data['image'] = $imagename;
            }

            if (isset($request->id)) {
                $album_id = $request->id;
                $this->getAlbumsLib()->updateAlbums($data, $album_id);
                Session::flash('success_msg', 'Album has been updated successfully!');
            } else {
                $album_id = $this->getAlbumsLib()->addAlbums($data);
                Session::flash('success_msg', 'Album has been saved successfully!');
            }

            //Update AlbumID in "album_images" TABLE
            \DB::table('album_images')
                    ->where('temp_id', $request->temp_id)
                    ->update(['album_id' => $album_id]);
        }
        return redirect()->route('website.manage.album');
    }

    /*
     * 
     */

    public function albumImageSave(Request $request) {
        if ($request->ajax()) {
            $photos_path = "albumimages/";
            $photos = $request->file('file');

            if (!is_array($photos)) {
                $photos = [$photos];
            }

            if (!is_dir($photos_path)) {
                mkdir($photos_path, 0777);
            }

            for ($i = 0; $i < count($photos); $i++) {
                $photo = $photos[$i];
                $name = substr(number_format(time() * rand(), 0, '', ''), 0, 10);
                $save_name = $name . '.' . $photo->getClientOriginalExtension();
                $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

//                Image::make($photo)
//                        ->resize(250, null, function ($constraints) {
//                            $constraints->aspectRatio();
//                        })
//                        ->save($photos_path . '/' . $resize_name);
//                $photo->move($photos_path, $save_name);
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        $photos_path . $save_name, file_get_contents($photo->getRealPath())
                );

                $upload = new \App\Albumimages();
                $upload->temp_id = $request->tour_temp_id;
                $upload->filename = $photos_path . $save_name;
                $upload->resized_name = $photos_path . $resize_name;
                $upload->original_name = basename($photo->getClientOriginalName());
                $upload->file_size = $photo->getClientSize();
                $upload->save();
            }
            return \Response::json([
                        'message' => 'Image saved Successfully'
                            ], 200);
        }
    }

    public function albumImageFetch($id, Request $request) {
        if ($request->ajax()) {
            $images = \DB::table('album_images')
                            ->where('album_id', $id)->get();

            $imageAnswer = [];
            foreach ($images as $image) {
                $imageAnswer[] = [
                    'original' => $image->original_name,
                    'server' => \App\Helpers\S3url::s3_get_image($image->filename),
                    'size' => $image->file_size
                ];
            }

            return \response()->json([
                        'images' => $imageAnswer
            ]);
        }
    }

    /*
     * Album images delete
     */

    public function albumImageDelete(Request $request) {
        if ($request->ajax()) {
            $photos_path = "albumimages/";
            $filename = $request->id;
            $tour_temp_id = $request->tour_temp_id;
            $uploaded_image = \DB::table('album_images')
                    ->where('original_name', basename($filename))
                    ->where('temp_id', $tour_temp_id)
                    ->first();

            if (empty($uploaded_image)) {
                return \Response::json(['message' => 'Sorry file does not exist'], 400);
            }

            $file_path = $uploaded_image->filename;
            $resized_file = $uploaded_image->resized_name;


            if (\App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->has($file_path)) {
                \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->delete($file_path);
            }

//            if (file_exists($resized_file)) {
//                unlink($resized_file);
//            }

            if (!empty($uploaded_image)) {
                \DB::table('album_images')->where('id', $uploaded_image->id)->delete();
            }

            return \Response::json(['message' => 'File successfully delete'], 200);
        }
    }

    /*
     * Manage Album destroy
     */

    public function manage_album_destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getAlbumsLib()->deleteAlbums($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Album has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Album can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    public function dropzone_upload(Request $request) {
        if ($request->ajax()) {
            $photos = $request->file('file');

            if (!is_array($photos)) {
                $photos = [$photos];
            }
            $destinationPath = public_path('/uploads/albums');
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0777);
            }

            for ($i = 0; $i < count($photos); $i++) {
                $photo = $photos[$i];
                $name = sha1(date('YmdHis') . str_random(30));
                $save_name = $name . '.' . $photo->getClientOriginalExtension();
                $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

//                Image::make($photo)
//                        ->resize(250, null, function ($constraints) {
//                            $constraints->aspectRatio();
//                        })
//                        ->save($this->photos_path . '/' . $resize_name);

                $photo->move($destinationPath, $save_name);

//                $upload = new Upload();
//                $upload->filename = $save_name;
//                $upload->resized_name = $resize_name;
//                $upload->original_name = basename($photo->getClientOriginalName());
//                $upload->save();
            }
//            return Response::json([
//                        'message' => 'Image saved Successfully'
//                            ], 200);
        }
        exit();
    }

    public function dropzone_image_delete(Request $request) {
        $filename = $request->id;
        $uploaded_image = Upload::where('original_name', basename($filename))->first();

        if (empty($uploaded_image)) {
            return Response::json(['message' => 'Sorry file does not exist'], 400);
        }
        $destinationPath = public_path('/uploads/albums');
        $file_path = $destinationPath . '/' . $uploaded_image->filename;
        $resized_file = $destinationPath . '/' . $uploaded_image->resized_name;

        if (file_exists($file_path)) {
            unlink($file_path);
        }

        if (file_exists($resized_file)) {
            unlink($resized_file);
        }

        if (!empty($uploaded_image)) {
            $uploaded_image->delete();
        }

//        return Response::json(['message' => 'File successfully delete'], 200);
    }

    public function tour_section_name() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Home Page Section Name'
            )
        );
        return view('admin/website/website-tour-section-name', $data);
    }

    public function tour_section_name_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Hometoursection::select('id', 'primary_name', 'current_name', 'status'))
                ->addColumn('action', '')
                ->toJson();
        }
    }

    public function tour_section_name_edit($id) {
        $data['hometoursection_data'] =   $this->getHometoursectionLib()->getHomeTourSectionNameById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.tour.section.name.list'),
                'title' => 'Manage Home Page Section Name'
            ),
            array(
                'title' => 'Edit Home Page Section Name'
            )
        );
        return view('admin/website/website-tour-section-name-edit', $data);   
    }

    public function tour_section_name_update(Request $request) {
        if(isset($request->id)){
            $rules = [
                'current_name'       => 'required',
                'url'               => 'required'
            ];

            $messages = [
                'current_name.required'      => 'Please enter Current Name',
                'url.required'               => 'Please enter url'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->route('website.tour.section.name.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                $records = array(
                    "current_name"        => $request->current_name,
                    "url"                 => $request->url,
                    "status"              => $request->status
                );
                $update=$this->getHometoursectionLib()->updateHometoursection($records, $request->id);
                if($update){
                    Session::flash('success_msg', 'Home page section name updated successfully.');   
                }else{
                    Session::flash('error_msg', 'Home page section name updated unsuccessfully.');
                }
                return redirect()->route('website.tour.section.name.list');
            }
        }else{
            return redirect()->route('website.tour.section.name.list');
        }
    }

}
