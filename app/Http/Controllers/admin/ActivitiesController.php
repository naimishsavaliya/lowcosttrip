<?php

namespace App\Http\Controllers\admin;

use App\Activities;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ControllerAbstract;
use App\Activitytype;
use App\Ratetype;
use Session;

class ActivitiesController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        return view('admin/activitie/activities', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        return view('admin/activitie/activities-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activities  $activities
     * @return \Illuminate\Http\Response
     */
    public function show(Activities $activities) {
        $data = array();
        return view('admin/activitie/activities-view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activities  $activities
     * @return \Illuminate\Http\Response
     */
    public function edit(Activities $activities) {
        $data = array();
        return view('admin/activitie/activities-manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activities  $activities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activities $activities) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activities  $activities
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activities $activities) {
        //
    }

    /*
     * Activities category list
     */

    public function category() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Activity Type'
            )
        );

        return view('admin/activitie/category', $data);
    }

    /*
     * Category data
     */

    public function category_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(Activitytype::select('typeId', 'name', 'description', 'status')
                                    ->where('deleted_at', null)
                            )
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 250, "...");
                            })
                            //->rawColumns(['status', 'action'])
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /*
     * Activities category create
     */

    public function category_create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('activities.type'),
                'title' => 'Manage Activity Type'
            ),
            array(
                'title' => 'Add Activity Type'
            )
        );
        return view('admin/activitie/category_create', $data);
    }

    public function category_store(Request $request) {
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter activity type',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('activities.type.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('activities.type.add')->withErrors($validator)->withInput();
            }
        } else {
            $categoryData = array();
            $categoryData['name'] = $request->name;
            $categoryData['description'] = $request->description;
            $categoryData['status'] = $request->status;
            if (isset($request->id)) {
                $id = $request->id;
                $this->getActivitytypemasterLib()->updateActivitytypemaster($categoryData, $id);
                Session::flash('success_msg', 'Activity type has been updated successfully!');
            } else {
                $this->getActivitytypemasterLib()->addActivitytypemaster($categoryData);
                Session::flash('success_msg', 'Activity type has been saved successfully!');
            }
            return redirect(route('activities.type'));
        }
    }

    /*
     * Activities category id
     */

    public function category_edit($id) {
        $data['type'] = $this->getActivitytypemasterLib()->getActivitytypemasterById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('activities.type'),
                'title' => 'Manage Activity Type'
            ),
            array(
                'title' => 'Edit Activity Type'
            )
        );

        return view('admin/activitie/category_create', $data);
    }

    /*
     * Activities category desctroy
     */

    public function category_destroy($id) {
        $deleted = $this->getActivitytypemasterLib()->deleteActivitytypemaster($id);
        if ($deleted) {
            Session::flash('success_msg', 'Activity type has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Activity type can't deleted!");
        }
        return redirect(route('activities.type'));
    }

    /*
     * Activities inventory List
     */

    public function activity_inventory() {
        $data = array();
        return view('admin/activitie/activitie_inventory', $data);
    }

    /*
     * Activities inventory Add
     */

    public function activity_inventory_add() {
        $data = array();
        return view('admin/activitie/activitie_inventory_add', $data);
    }

    /*
     * Activities inventory edit
     */

    public function activity_inventory_edit($id) {
        $data = array();
        return view('admin/activitie/activitie_inventory_add', $data);
    }

    /*
     * Activities inventory delete
     */

    public function activity_inventory_desctroy($id) {
        $data = array();
        return view('admin/activitie/activitie_inventory', $data);
    }

    /*
     * Manage Rate Type
     */

    public function rates_type() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Tour Rate Type'
            )
        );
        return view('admin/activitie/rate-type', $data);
    }

    /*
     * Rate Type data in JSON
     */

    public function rates_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(Ratetype::select('typeId', 'name', 'description', 'status')
                                    ->where('deleted_at', null)
                            )
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 250, "...");
                            })
                            //->rawColumns(['status', 'action'])
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /*
     * Manage Rate Type Add
     */

    public function rates_type_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('rate.type.index'),
                'title' => 'Manage Tour Rate Type'
            ),
            array(
                'title' => 'Add Rate Type'
            )
        );
        return view('admin/activitie/rate-type-manage', $data);
    }

    /*
     * Activities discount add
     */

    public function rate_type_edit($id) {
        $data['type'] = $this->getRatetypemasterLib()->getRatetypemasterById($id);
        
        $data['breadcrumb'] = array(
            array(
                'link' => route('rate.type.index'),
                'title' => 'Manage Tour Rate Type'
            ),
            array(
                'title' => 'Edit Rate Type'
            )
        );
        
        return view('admin/activitie/rate-type-manage', $data);
    }

    /*
     * Rate type store
     */

    public function rate_type_store(Request $request) {
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter rate type',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('rate.type.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('rate.type.add')->withErrors($validator)->withInput();
            }
        } else {
            $ratetypeData = array();
            $ratetypeData['name'] = $request->name;
            $ratetypeData['description'] = $request->description;
            $ratetypeData['status'] = $request->status;
            if (isset($request->id)) {
                $id = $request->id;
                $this->getRatetypemasterLib()->updateRatetypemaster($ratetypeData, $id);
                Session::flash('success_msg', 'Rate type has been updated successfully!');
            } else {
                $this->getRatetypemasterLib()->addRatetypemaster($ratetypeData);
                Session::flash('success_msg', 'Rate type has been saved successfully!');
            }
            return redirect(route('rate.type.index'));
        }
    }

    /*
     * Delete rate type
     */

    public function rate_type_destroy($id) {
        $deleted = $this->getRatetypemasterLib()->deleteRatetypemaster($id);
        if ($deleted) {
            Session::flash('success_msg', 'Rate type has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Rate type can't deleted!");
        }
        return redirect(route('rate.type.index'));
    }

    public function detail($id) {
        $data = array();
        return view('admin/activitie/detail', $data);
    }

    public function inventory($id) {
        $data = array();
        return view('admin/activitie/inventory', $data);
    }

    public function bookings() {
        $data = array();
        return view('admin/activitie/booking-list', $data);
    }

    public function booking_add() {
        $data = array();
        return view('admin/activitie/booking-add', $data);
    }

}
