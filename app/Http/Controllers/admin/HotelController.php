<?php

namespace App\Http\Controllers\admin;

use App\hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use App\Hoteltype;
use App\Roomtype;
use Validator;
use Session;
use DB;

class HotelController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Hotel'
            )
        );
        return view('admin/hotel/hotel', $data);
    }

    /*
     * Return hotel data
     */

    public function data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Hotel::select('hotel.id', 
                'hotel.name', 
                'hotel.email', 
                'hotel.mobile',
                DB::raw("CONCAT(hotel.email, '\n', hotel.mobile, '\n') as hotel_info"),
                'hotel.star', 
                'hotel.status', 
                'hotel.created_at', 
                'country.country_name', 
                'state.state_name', 
                'city.city_name', 
                DB::raw("CONCAT(country.country_name, '\n', state.state_name, '\n',city.city_name) as location"))
                ->join("country", "country.country_id", "=", "hotel.country_id", "left")
                ->join("state", "state.state_id", "=", "hotel.state_id", "left")
                ->join("city", "city.city_id", "=", "hotel.city_id", "left")
                ->where('hotel.deleted_at', null))
                ->editColumn('created_at', function ($model) {
                    return date("d M Y", strtotime($model->created_at));
                })
                // ->editColumn('country_name', function ($model) {
                //     return $model->country_name . '/' . $model->state_name . '<br />' . $model->city_name;
                // })
                // ->editColumn('email', function ($model) {
                //     return $model->email . '<br />' . $model->mobile;
                // })
                ->filterColumn('location', function($query, $keyword) {
                    $query->whereRaw("CONCAT(country.country_name, '\n', state.state_name, '\n',city.city_name) like ?", ["%{$keyword}%"]);
                })
                ->filterColumn('hotel_info', function($query, $keyword) {
                    $query->whereRaw("CONCAT(hotel.email, '\n', hotel.mobile, '\n') like ?", ["%{$keyword}%"]);
                })
                // ->filterColumn('hotel.email', function($query, $keyword) {
                //     $search_arr = explode('/', $keyword);
                //     $email = trim($search_arr[0]);
                //     if (count($search_arr) > 1) {
                //         $mobile = trim($search_arr[1]);
                //         $query->whereRaw("hotel.email like ?", ["%{$email}%"]);
                //         $query->whereRaw("hotel.mobile like ?", ["%{$mobile}%"]);
                //     } else {
                //         $query->whereRaw("hotel.email like ?", ["%{$email}%"]);
                //     }
                // })
                ->addColumn('total_agent', '0')
                ->addColumn('total_inventory', '0')
                ->addColumn('total_booking', '0')
                ->addColumn('action', '')
                ->rawColumns(['country_name', 'email'])
                ->toJson();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['destinations'] = $this->getDestinationsLib()->getDestination(array('desId', 'name'));
        $data['facilities'] = $this->getHotelfacilitiesLib()->getHotelfacilities(array('facId', 'name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('hotel.index'),
                'title' => 'Manage Hotel'
            ),
            array(
                'title' => 'Add Hotel'
            )
        );
        return view('admin/hotel/hotel-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate certificate data
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'pincode' => 'required',
            'mobile' => 'required',
            'email' => 'required',
            'check_in_time' => 'required',
            'check_out_time' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter hotel name',
            'address.required' => 'Please enter address',
            'country_id.required' => 'Please select country',
            'state_id.required' => 'Please select state',
            'city_id.required' => 'Please select city',
            'pincode.required' => 'Please enter pincode',
            'mobile.required' => 'Please enter mobile number',
            'email.required' => 'Please enter email',
            'check_in_time.required' => 'Please select check-in time',
            'check_out_time.required' => 'Please select check-out time',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('hotel.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('hotel.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['temp_id'] = $request->temp_id;
            $data['name'] = $request->name;
            $data['destination_id'] = $request->destination_id;
            $data['star'] = $request->star;
            $data['address'] = $request->address;
            $data['country_id'] = $request->country_id;
            $data['state_id'] = $request->state_id;
            $data['city_id'] = $request->city_id;
            $data['pincode'] = $request->pincode;
            $data['mobile'] = $request->mobile;
            $data['landline'] = $request->landline;
            // $data['fax'] = $request->fax;
            $data['email'] = $request->email;
            $data['manager_name'] = $request->manager_name;
            $data['reference_no'] = $request->reference_no;
            $data['map_code'] = $request->map_code;
            $data['check_in_time'] = $request->check_in_time;
            $data['check_out_time'] = $request->check_out_time;
            $data['tags'] = $request->tags;
            $data['brief_description'] = $request->brief_description;
            $data['hotel_policies'] = $request->hotel_policies;
            if (count($request->facilities) > 0) {
                $data['facilities'] = implode(',', $request->facilities);
            }
            $data['status'] = $request->status;

            $file = $request->file('hotel_logo');
            if (!empty($file)) {
                $imagename = substr(number_format(time() * rand(), 0, '', ''), 0, 10) . "." . $file->getClientOriginalExtension();
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        'hotel_logo/' . $imagename, file_get_contents($file->getRealPath())
                );
                $data['hotel_logo'] = "hotel_logo/" . $imagename;
            }
//            print_r($data);die;

            if ($request->id > 0) {
                $hotel_id = $request->id;
                $this->getHotelLib()->updateHotel($data, $request->id);
                Session::flash('success_msg', 'Hotel has been updated successfully!');
            } else {
                $hotel_id = $this->getHotelLib()->addHotel($data);
                Session::flash('success_msg', 'Hotel has been saved successfully!');
            }

            //Update TourID in "hotel_image" TABLE
            \DB::table('hotel_image')
                    ->where('temp_id', $request->temp_id)
                    ->update(['hotel_id' => $hotel_id]);

//            if ($request->id > 0) {
//                return redirect()->route('hotel.detail', $request->id);
//            } else {
            return redirect()->route('hotel.index');
//            }
        }
        return redirect()->route('hotel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(hotel $hotel) {
        
    }

    public function detail(hotel $hotel) {
        $data = array();
        return view('admin/hotel/hotel-detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['hotel'] = $this->getHotelLib()->getHotelById($id);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['states'] = $this->getStateLib()->getStateByCountry($data['hotel']->country_id, array('state_id', 'state_name'));
        $data['cities'] = $this->getCityLib()->getCityByState($data['hotel']->state_id, array('city_id', 'city_name'));
        $data['destinations'] = $this->getDestinationsLib()->getDestination(array('desId', 'name'));
        $data['facilities'] = $this->getHotelfacilitiesLib()->getHotelfacilities(array('facId', 'name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('hotel.index'),
                'title' => 'Manage Hotel'
            ),
            array(
                'title' => 'Edit Hotel'
            )
        );
        return view('admin/hotel/hotel-manage', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getHotelLib()->deleteHotel($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Hot Tour has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Hot Tour can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    /*
     * Store Tour Multiple images
     */

    public function dropzonImageSave(Request $request) {
        if ($request->ajax()) {
            $photos_path = "hotel-images/";
            $photos = $request->file('file');

            if (!is_array($photos)) {
                $photos = [$photos];
            }

            if (!is_dir($photos_path)) {
                mkdir($photos_path, 0777);
            }

            for ($i = 0; $i < count($photos); $i++) {
                $photo = $photos[$i];
                $name = sha1(date('YmdHis') . str_random(30));
                $save_name = $name . '.' . $photo->getClientOriginalExtension();
                $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

//                Image::make($photo)
//                        ->resize(250, null, function ($constraints) {
//                            $constraints->aspectRatio();
//                        })
//                        ->save($photos_path . '/' . $resize_name);
//                $photo->move($photos_path, $save_name);
                $uploaded = \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->put(
                        $photos_path . $save_name, file_get_contents($photo->getRealPath())
                );

                $upload = new \App\Hotelimage();
                $upload->temp_id = $request->hotel_temp_id;
                if (isset($request->hotel_id) && !empty($request->hotel_id)) {
                    $upload->hotel_id = $request->hotel_id;
                }
                $upload->filename = $photos_path . $save_name;
                $upload->resized_name = $photos_path . $resize_name;
                $upload->original_name = basename($photo->getClientOriginalName());
                $upload->file_size = $photo->getClientSize();
                $upload->save();
            }
            return \Response::json([
                        'message' => 'Image saved Successfully'
                            ], 200);
        }
    }

    /*
     * Fetch Tour Images
     */

    public function dropzonImageFetch($id, Request $request) {
        if ($request->ajax()) {
            $images = \DB::table('hotel_image')
                            ->where('hotel_id', $id)->get();

            $imageAnswer = [];
            foreach ($images as $image) {
                $imageAnswer[] = [
                    'original' => $image->original_name,
                    'server' => \App\Helpers\S3url::s3_get_image($image->filename),
                    'size' => $image->file_size
                ];
            }

            return \response()->json([
                        'images' => $imageAnswer
            ]);
        }
    }

    /*
     * Delete Hotel Images
     */

    public function dropzonImageDelete(Request $request) {
        if ($request->ajax()) {
            $photos_path = "hotel-images/";
            $filename = $request->id;
            $hotel_temp_id = $request->hotel_temp_id;
            $uploaded_image = \DB::table('hotel_image')
                    ->where('original_name', basename($filename))
                    ->where('temp_id', $hotel_temp_id)
                    ->first();

            if (empty($uploaded_image)) {
                return \Response::json(['message' => 'Sorry file does not exist'], 400);
            }

            $file_path = $uploaded_image->filename;
            $resized_file = \App\Helpers\S3url::s3_get_image($uploaded_image->resized_name);


            if (\App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->has($file_path)) {
                \App\Libraries\S3::getFilesystem(env('AWS_S3_BUCKET'))->delete($file_path);
            }

//            if (file_exists($resized_file)) {
//                unlink($resized_file);
//            }

            if (!empty($uploaded_image)) {
                \DB::table('hotel_image')->where('id', $uploaded_image->id)->delete();
            }

            return \Response::json(['message' => 'File successfully delete'], 200);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function room() {
        $data = array();
        return view('admin/hotel/hotel-room', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function room_create() {
        $data = array();
        return view('admin/hotel/hotel-room-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function room_store(Request $request) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function room_edit(hotel $hotel) {
        $data = array();
        return view('admin/hotel/hotel-room-manage', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function room_price(hotel $hotel) {
        $data = array();
        return view('admin/hotel-room-price', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function room_destroy(hotel $hotel) {
        $data = array("deleted" => true);
        return view('admin/hotel-room', $data);
    }

    /*
     * Hotel Room Types
     */

    public function room_type() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Room Type'
            )
        );
        return view('admin/hotel/room_type', $data);
    }

    /*
     * Hotel Room Types Data
     * Return data IN JSON
     */

    public function room_type_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(Roomtype::select('typeId', 'name', 'description', 'status')
                                    ->where('deleted_at', null)
                            )
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 250, "...");
                            })
                            //->rawColumns(['status', 'action'])
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /*
     * Hotel Room Type Add
     */

    public function room_type_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('room.types'),
                'title' => 'Manage Room Type'
            ),
            array(
                'title' => 'Add Room Type'
            )
        );
        return view('admin/hotel/room_type_create', $data);
    }

    /*
     * Hotel room type save
     */

    public function room_type_store(Request $request) {
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter hotel type',
            'status.required' => 'Please select currency status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('room.type.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('room.type.add')->withErrors($validator)->withInput();
            }
        } else {
            //set Hotel type Data
            $data['name'] = $request->name;
            $data['description'] = $request->description;
            $data['status'] = $request->status;

            if ($request->id > 0) {
                $this->getRoomtypemasterLib()->updateRoomtype($data, $request->id);
                Session::flash('success_msg', 'Room type has been updated successfully!');
            } else {
                $typeId = $this->getRoomtypemasterLib()->addRoomtype($data);
                Session::flash('success_msg', 'Room type has been saved successfully!');
            }
        }
        return redirect()->route('room.types');
    }

    /*
     * Hotel Room Type Edit
     */

    public function room_type_edit($id) {
        $data['room_type'] = $this->getRoomtypemasterLib()->getRoomtypeById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('room.types'),
                'title' => 'Manage Room Type'
            ),
            array(
                'title' => 'Edit Room Type'
            )
        );

        return view('admin/hotel/room_type_create', $data);
    }

    /*
     * Hotel Room Type Delete
     */

    public function room_type_destroy($id) {
        $deleted = $this->getRoomtypemasterLib()->deleteRoomtype($id);
        if ($deleted) {
            Session::flash('success_msg', 'Room type has been deleted successfully!');
        } else {
            Session::flash('error_msg', "room type can't deleted!");
        }
        return redirect()->route('room.types');
    }

    /*
     * Hotel Room Price Type
     */

    public function room_price_type() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Room Price Type'
            )
        );
        return view('admin/hotel/room_price_type', $data);
    }

    /*
     * Return Room Price Data
     * JSON
     */

    public function room_price_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Roompricetype::select('id', 'name', 'description', 'status', 'created_at')
                                    ->where('deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 100, "...");
                            })
                            ->addColumn('action', '')
                            //->rawColumns(['status', 'action'])
                            ->toJson();
        }
    }

    /*
     * Hotel Room Price Type Add
     */

    public function room_price_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('room.price.type'),
                'title' => 'Manage Room Price Type'
            ),
            array(
                'title' => 'Add Room Type'
            )
        );
        return view('admin/hotel/room_price_type_create', $data);
    }

    /*
     * Hotel Room Price Type Edit
     */

    public function room_price_edit($id) {
        $data['roomprice'] = $this->getRoomPriceTypeLib()->getRoomPriceTypeById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('room.price.type'),
                'title' => 'Manage Room Price Type'
            ),
            array(
                'title' => 'Edit Room Type'
            )
        );
        return view('admin/hotel/room_price_type_create', $data);
    }

    /*
     * Store room price type
     */

    public function room_price_store(Request $request) {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter name',
            'description.required' => 'Please enter redirect url',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('room.price.type.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('room.price.type.add')->withErrors($validator)->withInput();
            }
        } else {
            //get Facilities data
            $facilitiesData = $request->all();
            $data = array(
                'name' => $facilitiesData['name'],
                'description' => $facilitiesData['description'],
                'status' => $facilitiesData['status'],
            );

            if ($facilitiesData['id'] > 0) {
                $this->getRoomPriceTypeLib()->updateRoomPriceType($data, $facilitiesData['id']);
                Session::flash('success_msg', 'Price type updated successfully!');
            } else {
                $this->getRoomPriceTypeLib()->addRoomPriceType($data);
                Session::flash('success_msg', 'Price type saved successfully!');
            }
        }

        return redirect()->route('room.price.type');
    }

    /*
     * Hotel Room Price Type Delete
     */

    public function room_price_destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getRoomPriceTypeLib()->deleteRoomPriceType($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Room price has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Room price can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    /*
     * Hotel Category List
     */

    public function category() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Hotel Type'
            )
        );
        return view('admin/hotel/hotel_category', $data);
    }

    /*
     * 
     */

    public function category_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(Hoteltype::select('typeId', 'name', 'description', 'status')
                                    ->where('deleted_at', null)
                            )
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 250, "...");
                            })
                            // ->rawColumns(['status', 'action'])
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /*
     * Hotel Category Add
     */

    public function category_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('hotel.category.index'),
                'title' => 'Manage Hotel Type'
            ),
            array(
                'title' => 'Add Hotel Type'
            )
        );
        return view('admin/hotel/hotel_category_add', $data);
    }

    /*
     * Hotel Category Edit
     */

    public function category_edit($id) {
        $data['hotel_type'] = $this->getHoteltypemasterLib()->getHoteltypeById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('hotel.category.index'),
                'title' => 'Manage Hotel Type'
            ),
            array(
                'title' => 'Edit Hotel Type'
            )
        );

        return view('admin/hotel/hotel_category_add', $data);
    }

    /*
     * Saved Hotel type in to DB
     */

    public function category_store(Request $request) {
        $rules = [
            'name' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter hotel type',
            'status.required' => 'Please select currency status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('hotel.category.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('hotel.category.add')->withErrors($validator)->withInput();
            }
        } else {
            //set Hotel type Data
            $data['name'] = $request->name;
            $data['description'] = $request->description;
            $data['status'] = $request->status;

            if ($request->id > 0) {
                $this->getHoteltypemasterLib()->updateHoteltype($data, $request->id);
                Session::flash('success_msg', 'Hotel type has been updated successfully!');
            } else {
                $typeId = $this->getHoteltypemasterLib()->addHoteltype($data);
                Session::flash('success_msg', 'Hotel type has been saved successfully!');
            }
        }
        return redirect()->route('hotel.category.index');
    }

    /*
     * Hotel Category Delete
     */

    public function category_destroy($id) {
        $deleted = $this->getHoteltypemasterLib()->deleteHoteltype($id);
        if ($deleted) {
            Session::flash('success_msg', 'Hotel type has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Hotel type can't deleted!");
        }
        return redirect()->route('hotel.category.index');
    }

    public function lead_room_booking() {
        $data = array();
        return view('admin/hotel/lead_room_booking', $data);
    }

    public function lead_room_buy() {
        //dd('Hello');
        $data = array();
        return view('admin/hotel/lead_room_buy', $data);
    }

    public function room_inventory() {
        $data = array();
        return view('admin/hotel/room_inventory', $data);
    }

    public function view_booking() {
        $data = array();
        return view('admin/hotel/view_booking', $data);
    }

    public function view_booking_calendar() {
        $data = array();
        return view('admin/hotel/view_booking_calendar', $data);
    }

    public function discount() {
        $data = array();
        return view('admin/hotel/discount', $data);
    }

    public function manage_discount() {
        $data = array();
        return view('admin/hotel/manage_discount', $data);
    }

    public function room_booking() {
        $data = array();
        return view('admin/hotel/room_booking', $data);
    }

    public function room_detail($id) {
        $data = array();
        return view('admin/hotel/room-detail', $data);
    }

    public function room_book($id) {
        $data = array();
        return view('admin/hotel/room-book', $data);
    }

}
