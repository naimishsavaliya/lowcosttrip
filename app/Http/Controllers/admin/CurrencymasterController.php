<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use App\Currencymaster;
use Validator;
use Session;

class CurrencymasterController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Currency'
            )
        );
        return view('admin/currency/index', $data);
    }

    /*
     * Currency table data 
     * Return JSON
     */

    public function data(Request $request) {
        return datatables()->of(Currencymaster::select('currId', 'currency_name', 'currency_value', 'currency_master.status', 'currency_master.created_at', 'currId as action', 'country.country_name')
                                ->join('country', 'country.country_id', '=', 'currency_master.country_id')
                                ->where('currency_master.deleted_at', null)
                        )
                        ->addColumn('action', '')
//                        ->rawColumns(['status', 'action'])
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));

        $data['breadcrumb'] = array(
            array(
                'link' => route('currency.index'),
                'title' => 'Manage Currency'
            ),
            array(
                'title' => 'Add Currency'
            )
        );

        return view('admin/currency/manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate user data
        $rules = [
            'name' => 'required',
            'currency_value' => 'required',
            'country_id' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter currency name',
            'currency_value.required' => 'Please enter currency value',
            'country_id.required' => 'Please select country',
            'status.required' => 'Please select currency status',
        ];

        if (isset($request->id) == false) {
//            $rules['currency_icon'] = 'required';
//            $messages['currency_icon.required'] = 'Please upload currency icon';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('currency.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('currency.add')->withErrors($validator)->withInput();
            }
        } else {
            //set Currency master Data
            $data['currency_name'] = $request->name;
            $data['currency_value'] = $request->currency_value;
            $data['country_id'] = $request->country_id;
            $data['description'] = $request->description;
            $data['code'] = $request->code;
            $data['symbol'] = $request->symbol;
            $data['status'] = $request->status;


            //Updoad Currency icon and save in db
            $image = $request->file('currency_icon');
            if (!empty($image)) {
                $imagename = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/currency_icons');
                $image->move($destinationPath, $imagename);
                $data['currency_icon'] = $imagename;
            }

            if ($request->id > 0) {
                $this->getCurrencymasterLib()->updateCurrencymaster($data, $request->id);
                Session::flash('success_msg', 'Currency has been updated successfully!');
            } else {
                $userId = $this->getCurrencymasterLib()->addCurrencymaster($data);
                Session::flash('success_msg', 'Currency has been saved successfully!');
            }
        }

        return redirect()->route('currency.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['currency'] = $this->getCurrencymasterLib()->getCurrencymasterById($id);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));

        $data['breadcrumb'] = array(
            array(
                'link' => route('currency.index'),
                'title' => 'Manage Currency'
            ),
            array(
                'title' => 'Edit Currency'
            )
        );

        return view('admin/currency/manage', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->getCurrencymasterLib()->deleteCurrencymaster($id);
        if ($deleted) {
            Session::flash('success_msg', 'Currency has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Currency can't deleted!");
        }
        return redirect()->route('currency.index');
    }

}
