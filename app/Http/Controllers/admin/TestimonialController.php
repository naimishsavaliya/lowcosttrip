<?php

namespace App\Http\Controllers\admin;

use App\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;

class TestimonialController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Testimonial'
            )
        );
        return view('admin/testimonial/testimonial', $data);
    }

    /*
     * Testimonial Return Data
     * Json
     */

    public function data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Testimonial::select('id', 'name', 'email', 'tour_code', 'description', 'sort_no', 'status', 'created_at')
                                    ->where('deleted_at', null))
                            ->editColumn('description', function ($model) {
                                return mb_strimwidth($model->description, 0, 100, "...");
                            })
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->addColumn('action', '')
                            ->rawColumns(['description', 'status', 'action'])
                            ->toJson();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.testimonial'),
                'title' => 'Manage Testimonial'
            ),
            array(
                'title' => 'Add Testimonial'
            )
        );
        return view('admin/testimonial/testimonial-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate certificate data
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'tour_code' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter name',
            'email.required' => 'Please enter email',
            'tour_code.required' => 'Please enter tour code',
            'status.required' => 'Please select status',
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('testimonial.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('testimonial.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['tour_code'] = $request->tour_code;
            $data['description'] = $request->description;
            $data['status'] = $request->status;

            if (isset($request->id)) {
                $this->getTestimonialLib()->updateTestimonial($data, $request->id);
                \Session::flash('success_msg', 'Testimonial has been updated successfully!');
            } else {
                $this->getTestimonialLib()->addTestimonial($data);
                \Session::flash('success_msg', 'Testimonial has been saved successfully!');
            }
        }
        return redirect()->route('website.testimonial');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['testimonial'] = $this->getTestimonialLib()->getTestimonialById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('website.testimonial'),
                'title' => 'Manage Testimonial'
            ),
            array(
                'title' => 'Edit Testimonial'
            )
        );
        return view('admin/testimonial/testimonial-manage', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getTestimonialLib()->deleteTestimonial($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Testimonial has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Testimonial can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

}
