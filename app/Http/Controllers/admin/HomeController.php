<?php

namespace App\Http\Controllers\admin;

use App\Home;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use DB;

class HomeController extends ControllerAbstract {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index() {
    //     $data = array();
    //     $data['intrested_ins'] = $this->getCommonLib()->getLeadIntrestedin();
    //     $lead_status = $this->getLeadstatusmasterLib()->getLeadstatusmaster(array('typeId', 'name'));
    //     $lead_data = $this->getLeadLib()->getLead(array('id', 'status_id', 'interested_in'));
    //     $data['status'] = $data['leads'] = array();
    //     foreach ($lead_status as $lead_s) {
    //         $data['status'][$lead_s->typeId] = $lead_s->name;
    //     }
    //     foreach ($lead_data as $lead) {
    //         $data['leads'][$lead->interested_in][$lead->status_id][] = $lead->id;
    //     }
    //     return view('admin/home', $data);
    // }

    public function index() {
        $data = array();
        $data['intrested_ins'] = $this->getCommonLib()->getLeadIntrestedin();
        $lead_status = $this->getLeadstatusmasterLib()->getLeadstatusmaster(array('typeId', 'name'));
        $data['status'] = $data['leads'] = array();
        foreach ($lead_status as $lead_s) {
            $data['status'][$lead_s->typeId] = $lead_s->name;
        }
        if(isset($_GET['start_date']) && $_GET['start_date']!=''){
            $start_date=date('Y-m-d', strtotime($_GET['start_date']));
        }
        if(isset($_GET['end_date']) && $_GET['end_date']!=''){
            $end_date=date('Y-m-d', strtotime($_GET['end_date']));
        }
        if(isset($start_date) && isset($end_date)){
            $lead_data = $this->getLeadLib()->getleadfilter($start_date,$end_date);
        }else{
            $lead_data = $this->getLeadLib()->getLead(array('id', 'status_id', 'interested_in'));
        }
        if(!empty($lead_data)){
            foreach ($lead_data as $lead) {
                $data['leads'][$lead->interested_in][$lead->status_id][] = $lead->id;
            }  
        }
        return view('admin/home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function show(Home $home) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function edit(Home $home) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Home $home) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function destroy(Home $home) {
        //
    }

    public function getStateByCountry($country_id) {
        $states = $this->getStateLib()->getStateByCountry($country_id, array('state_id', 'state_name'));
        exit(json_encode($states));
    }

    public function getCityByState($state_id) {
        $cities = $this->getCityLib()->getCityByState($state_id, array('city_id', 'city_name'));
        exit(json_encode($cities));
    }

    public function change_status(Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            try {
                $table = $request->table;
                $id = $request->id;
                $status = $request->status;
                $column_name = $request->column_name;
                
                $update_columns = "status";
                if(isset($request->column_update)){
                    $update_columns = $request->column_update;
                }
                
                //update query fire
                DB::table($table)
                        ->where($column_name, $id)
                        ->update([$update_columns => $status]);
                $response['success'] = true;
                $response['msg_status'] = 'success';
                $response['message'] = "Status has been changed successfully";
            } catch (Exception $e) {
                $response['success'] = false;
                $response['msg_status'] = 'warning';
                $response['message'] = "Something went wrong, please try again";
            }
        }
        exit(json_encode($response));
    }


    public function chart(Request $request)
    {
        $response['success'] = false;
        if ($request->ajax()) {

            
            $intrested_ins = $this->getCommonLib()->getLeadIntrestedin();

            $color = array('2' => '#188ae2', '3' => '#ff5b5b', '4' => '#10c469', '7' => '#ffa000', '8' => '#17a2b8');
            
            /* Days Month */ 
            $intrested_in = $request->intrested_in;
            $month = $request->month;
            $month_year = explode('-', $month);
            $days = cal_days_in_month(CAL_GREGORIAN, $month_year[0], $month_year[1]);
            for ($m=1; $m<=$days; $m++) {
                $months[] = $m;
            }

            $interested_in = $request->intrested_in;
            $lead_data = $this->getLeadLib()->getLeadCounter($request->status, $interested_in, $month);
            $lead_info = array();
            foreach ($lead_data as $lead) {
               $m = date("d", strtotime($lead->created_at));
               $lead_info[$lead->interested_in][$m][] = $lead->id;
            }

            $intrested_ins = (($interested_in > 0) ? array($interested_in => $intrested_ins[$interested_in] ) : $intrested_ins);

            $final_data = array();
            foreach ($intrested_ins as $intres_id => $intrested) {
                $rcolor= '#'.str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
                $chart_data = array();
                $chart_data['label'] = "$intrested";
                $chart_data['backgroundColor'] =  (( isset( $color[$intres_id] ) )? $color[$intres_id] : $rcolor);
                $chart_data['borderColor'] = (( isset( $color[$intres_id] ) )? $color[$intres_id] : $rcolor);
                $month_counter = array();
                for ($m=1; $m<=$days; $m++) {
                    if($m < 9){
                        $m = '0'.$m;
                    }
                    $month_counter[] = (isset($lead_info[$intres_id][$m]) ? count($lead_info[$intres_id][$m]) : 0);
                }
                $chart_data['data'] = $month_counter;
                $chart_data['fill'] = false;
                $final_data[]  = $chart_data;
            }
            $response['success'] = true;
            $response['month'] = $months;
            $response['chart_data'] = $final_data;
        }
        exit(json_encode($response));
    }

}
