<?php

namespace App\Http\Controllers\admin;

use App\BookActivities;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Validator;
use Session;
use Illuminate\Support\Facades\Auth;

class BookActivitiesController extends ControllerAbstract
{

   public function index() {
        $data = array();
        $data['breadcrumb'] = array(
            array(
                'title' => 'Tours Activities'
            )
        );
        return view('admin/BookActivities/view-book-activities', $data);
    }

    public function data(Request $request) {
        return datatables()->of(
            BookActivities::select('book_activities.id',
                'country.country_name',
                'book_activities.title',
                'book_activities.description',
                'book_activities.status')
            ->leftjoin('country', 'book_activities.country_id', '=', 'country.country_id')
            )
            ->filterColumn('country_name', function($query, $keyword) {
                $query->whereRaw("country_name like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', '')
            ->toJson();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['breadcrumb'] = array(
            array(
                'link' => route('book-activities.view'),
                'title' => 'Manage Activitie'
            ),
            array(
                'title' => 'Add Activitie'
            )
        );

        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        return view('admin/BookActivities/manage-book-activities', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'country_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'country_id.required' => 'Please select country',
            'title.required' => 'Please enter title',
            'description.required' => 'Please enter description',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('state.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('state.add')->withErrors($validator)->withInput();
            }
        } else {
            //set Data
            $data['country_id']  = $request->country_id;
            $data['title']       = $request->title;
            $data['description'] = $request->description;
            $data['status']      = $request->status;

            if ($request->id > 0) {
                $this->getBookActivitiesLib()->updateBookActivities($data, $request->id);
                Session::flash('success_msg', 'Activitie has been updated successfully!');
            } else {
                $typeId = $this->getBookActivitiesLib()->addBookActivities($data);
                Session::flash('success_msg', 'Activitie has been saved successfully!');
            }
        }
        return redirect()->route('book-activities.view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['activities'] = $this->getBookActivitiesLib()->getBookActivitiesById($id);
        $data['countrys']   = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('book-activities.view'),
                'title' => 'Tours Activities'
            ),
            array(
                'title' => 'Edit Activities'
            )
        );
        return view('admin/BookActivities/manage-book-activities', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->getBookActivitiesLib()->deleteBookActivities($id);
        if ($deleted) {
            Session::flash('success_msg', 'Tours Activities has been deleted successfully!');
        } else {
            Session::flash('error_msg', "Tours Activities can't deleted successfully!");
        }
        return redirect()->route('book-activities.view');
    }


    
}
