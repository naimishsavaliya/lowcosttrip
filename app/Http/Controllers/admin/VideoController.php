<?php

namespace App\Http\Controllers\admin;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Session;
use Validator;

class VideoController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Video Category'
            )
        );
        return view('admin/website/video', $data);
    }

    public function data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Videocategory::select('catId', 'title', 'image', 'status', 'created_at')
                                    ->where('deleted_at', null))
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('image', function ($model) {
                                return url('/public/uploads/video_category') . '/' . $model->image;
                            })
                            ->addColumn('action', '')
                            ->rawColumns(['status', 'action'])
                            ->toJson();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('video.category.index'),
                'title' => 'Manage Video Category'
            ),
            array(
                'title' => 'Add Video Category'
            )
        );
        return view('admin/website/video-manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate certificate data
        $rules = [
            'title' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'title.required' => 'Please enter video category title',
            'status.required' => 'Please select status',
        ];

        if (isset($request->id) == false) {
            $rules['image'] = 'required';
            $messages['image.required'] = 'Please upload video image';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('video.category.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('video.category.add')->withErrors($validator)->withInput();
            }
        } else {
            $data['title'] = $request->title;
            $data['status'] = $request->status;

            //Upload destination image
            $image = $request->file('image');
            if (!empty($image)) {
                $imagename = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/video_category');
                $image->move($destinationPath, $imagename);
                $data['image'] = $imagename;
            }

            if (isset($request->id)) {
                $this->getVideocategoryLib()->updateVideocategory($data, $request->id);
                Session::flash('success_msg', 'Video category has been updated successfully!');
            } else {
                $this->getVideocategoryLib()->addVideocategory($data);
                Session::flash('success_msg', 'Video category has been saved successfully!');
            }
        }
        return redirect()->route('video.category.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['album_category'] = $this->getVideocategoryLib()->getVideocategoryById($id);

        $data['breadcrumb'] = array(
            array(
                'link' => route('video.category.index'),
                'title' => 'Manage Video Category'
            ),
            array(
                'title' => 'Edit Video Category'
            )
        );

        return view('admin/website/video-manage', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getVideocategoryLib()->deleteVideocategory($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Video category has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Video category can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    /*
     * Video album manage
     */

    public function album() {
        $data['video_category'] = $this->getVideocategoryLib()->getVideocategory(array('catId', 'title'));
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Video Album'
            )
        );

        return view('admin/website/video-album', $data);
    }

    /*
     * Video Album Data
     * Json
     */

    public function album_data(Request $request) {
        if ($request->ajax()) {
            return datatables()->of(\App\Albumvideo::select('album_video.vidId', 'album_video.title', 'album_video.video_url', 'album_video.customer_name', 'album_video.album_date', 'album_video.status', 'album_video.created_at', \DB::raw("GROUP_CONCAT(category_video.title) as category"))
                                    ->leftJoin('category_video', function($join) {
                                        $join->whereRaw(\DB::raw("find_in_set(category_video.catId, album_video.cat_ids)"));
                                    })
                                    ->where('album_video.deleted_at', null)
                                    ->groupBy('album_video.vidId'))
                            ->editColumn('album_video', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('album_date', function ($model) {
                                return date("d M Y", strtotime($model->album_date));
                            })
                            ->filterColumn('album_video.album_date', function($query, $keyword) {
                                $search_date = date('Y-m-d', strtotime($keyword));
                                $query->whereRaw("DATE_FORMAT(album_video.album_date, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
                            })
                            ->addColumn('action', '')
                            ->toJson();
        }
    }

    /*
     * Video album create
     */

    public function album_create() {
        $data['video_category'] = $this->getVideocategoryLib()->getVideocategory(array('catId', 'title'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('video.album.index'),
                'title' => 'Manage Video Album'
            ),
            array(
                'title' => 'Add Video Album'
            )
        );
        return view('admin/website/video-album-manage', $data);
    }

    /*
     * Video album edit
     */

    public function album_edit($id) {
        $data['album_video'] = $this->getAlbumVideoLib()->getAlbumvideoById($id);
        $data['video_category'] = $this->getVideocategoryLib()->getVideocategory(array('catId', 'title'));
        $data['breadcrumb'] = array(
            array(
                'link' => route('video.album.index'),
                'title' => 'Manage Video Album'
            ),
            array(
                'title' => 'Edit Video Album'
            )
        );
        return view('admin/website/video-album-manage', $data);
    }

    /*
     * Video album store
     */

    public function album_store(Request $request) {
        //validate certificate data
        $rules = [
            'title' => 'required',
            'video_url' => 'required',
            'category_ids' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'title.required' => 'Please enter title',
            'video_url.required' => 'Please enter video url',
            'category_ids.required' => 'Please select category',
            'status.required' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('video.album.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('video.album.add')->withErrors($validator)->withInput();
            }
        } else {

            $data['title'] = $request->title;
            $data['video_url'] = $request->video_url;
            if (count($request->category_ids) > 0) {
                $data['cat_ids'] = implode(',', $request->category_ids);
            }
            if (!empty($request->album_date)) {
                $data['album_date'] = date('Y-m-d', strtotime($request->album_date));
            }
            $data['customer_name'] = $request->customer_name;
            $data['status'] = $request->status;

            if (isset($request->id)) {
                $this->getAlbumVideoLib()->updateAlbumvideo($data, $request->id);
                Session::flash('success_msg', 'Video album has been updated successfully!');
            } else {
                $this->getAlbumVideoLib()->addAlbumvideo($data);
                Session::flash('success_msg', 'Video album has been saved successfully!');
            }
        }
        return redirect()->route('video.album.index');
    }

    /*
     * Video album delete
     */

    public function album_destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getAlbumVideoLib()->deleteAlbumvideo($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Video album has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Video album can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

}
