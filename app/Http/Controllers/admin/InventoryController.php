<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InventoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        return view('admin/inventory/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        return view('admin/inventory/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = array();
        return view('admin/inventory/create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = array();
        return view('admin/inventory/index', $data);
    }

    public function history($id) {
        $data = array();
        return view('admin/inventory/history', $data);
    }

    public function room_inventory() {
        $data = array();
        return view('admin/inventory/room_inventory', $data);
    }

    public function room_price_update() {
        $response["status"] = "false";
        $response["message"] = "Room not found.";
        exit(json_encode($response));
    }

    public function get_room() {
//        $response["status"] = "false";
//        $response["message"] = "Room not found.";

        $response = array(
            'status' => 'true',
            'dataEvent' =>
            array(
                'event' =>
                array(
                    '06-27-2018' =>
                    array(
                        'disabled' => true,
                        'price' => '33.90',
                    ),
                    '07-07-2018' =>
                    array(
                        'price' => '43.00',
                    ),
                ),
                'price0' => '29.00',
                'price1' => '29.00',
                'price2' => '29.00',
                'price3' => '29.00',
                'price4' => '29.00',
                'price5' => '39.00',
                'price6' => '39.00',
                'canBack' => false,
                'canNext' => true,
            ),
            'month' => '05',
            'year' => '2018',
        );

        exit(json_encode($response));
    }

    public function get_navigate() {
        $response["status"] = "false";
        $response["message"] = "Room not found.";
        exit(json_encode($response));
    }

}
