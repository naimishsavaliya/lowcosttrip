<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Session;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function index() {
        return view('admin/login');
    }

    public function login(Request $request) {
        $response = array();
        $userdata = array(
            'email' => $request->email,
            'password' => $request->password
        );

        // attempt to do the login
        if ($res = Auth::attempt($userdata)) {
//            print_r($_SESSION);
//            sleep(5);
            $response["success"] = true;
            $response["msg"] = "Login";
            if (\Auth::user()->role_id != 0 && \Auth::user()->role_id != 1) {
                $user_data = DB::table('users_profile')->select('manager_id','subscription_id', 'sector_id', 'image')->where('user_id', \Auth::user()->id)->first();
                Session::put('manager_id', $user_data->manager_id);
                Session::put('subscription_id', $user_data->subscription_id);
                Session::put('sector_id', $user_data->sector_id);
                Session::put('image', $user_data->image);
            }
            return redirect()->route('home');
        } else {
            return redirect('/admin')->withErrors(['email' => 'Login id or password incorrect.'])->withInput();
            $response["success"] = false;
            $response["msg"] = "Invalid Login!";
        }
        echo json_encode($response);
    }

}
