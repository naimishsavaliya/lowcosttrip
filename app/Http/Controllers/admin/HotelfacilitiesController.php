<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hotelfaclitie;
use App\Http\Controllers\ControllerAbstract;
use Session;
use Validator;

class HotelfacilitiesController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Hotel Facility'
            )
        );
        return view('admin/hotel_facilties/index', $data);
    }

    public function data(Request $request) {
        return datatables()->of(Hotelfaclitie::select('facId', 'name', 'status', 'created_at')
                                ->where('deleted_at', null))
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->filterColumn('created_at', function ($query, $keyword) {
                            $query->whereRaw("DATE_FORMAT(created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                        })
                        ->addColumn('action', '')
                        //->rawColumns(['status', 'action'])
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('hotel-facilities.index'),
                'title' => 'Manage Hotel Facility'
            ),
            array(
                'title' => 'Add Hotel Facility'
            )
        );
        return view('admin/hotel_facilties/manage', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request) {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter name',
            'description.required' => 'Please enter redirect url',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('hotel-facilitie.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('hotel-facilitie.add')->withErrors($validator)->withInput();
            }
        } else {
            //get Facilities data
            $facilitiesData = $request->all();
            $data = array(
                'name' => $facilitiesData['name'],
                'description' => $facilitiesData['description'],
                'status' => $facilitiesData['status'],
            );

            if ($facilitiesData['id'] > 0) {
                $this->getHotelfacilitiesLib()->updateHotelfacilities($data, $facilitiesData['id']);
                Session::flash('success_msg', 'Hotel Facilities updated successfully!');
            } else {
                $this->getHotelfacilitiesLib()->addHotelfacilities($data);
                Session::flash('success_msg', 'Hotel Facilities saved successfully!');
            }
        }

        return redirect()->route('hotel-facilities.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['facilities'] = $this->getHotelfacilitiesLib()->getHotelfacilitiesById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('hotel-facilities.index'),
                'title' => 'Manage Hotel Facility'
            ),
            array(
                'title' => 'Edit Hotel Facility'
            )
        );
        return view('admin/hotel_facilties/manage', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getHotelfacilitiesLib()->deleteHotelfacilities($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Web Pop-up has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Web Pop-up can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

    /*
     * Hotel Room Facilities
     */

    public function room_index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Room Facility'
            )
        );
        return view('admin/hotel_facilties/room_index', $data);
    }
    
    /*
     * Hotel Room Facilities Data
     */
    public function room_data(Request $request) {
        return datatables()->of(\App\Roomfacilitis::select('facId', 'name', 'status', 'created_at')
                                ->where('deleted_at', null))
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->filterColumn('created_at', function ($query, $keyword) {
                            $query->whereRaw("DATE_FORMAT(created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                        })
                        ->addColumn('action', '')
                        //->rawColumns(['status', 'action'])
                        ->toJson();
    }

    
    /*
     * Hotel Room Facility Add
     */

    public function room_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('room.facilities'),
                'title' => 'Manage Room Facility'
            ),
            array(
                'title' => 'Add Room Facility'
            )
        );
        return view('admin/hotel_facilties/room_facility_manage', $data);
    }

    /*
     * Hotel Room Facility Edit
     */

    public function room_edit($id) {
        $data['facilities'] = $this->getRoomFacilitiesLib()->getRoomFacilitiesById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('room.facilities'),
                'title' => 'Manage Room Facility'
            ),
            array(
                'title' => 'Edit Room Facility'
            )
        );
        return view('admin/hotel_facilties/room_facility_manage', $data);
    }
    
    /*
     * Store Room Facilities
     */
    public function room_store(Request $request){
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name.required' => 'Please enter name',
            'description.required' => 'Please enter redirect url',
            'status.required' => 'Please select  status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('room.facilities.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('room.facilities.add')->withErrors($validator)->withInput();
            }
        } else {
            //get Facilities data
            $facilitiesData = $request->all();
            $data = array(
                'name' => $facilitiesData['name'],
                'description' => $facilitiesData['description'],
                'status' => $facilitiesData['status'],
            );

            if ($facilitiesData['id'] > 0) {
                $this->getRoomFacilitiesLib()->updateRoomFacilities($data, $facilitiesData['id']);
                Session::flash('success_msg', 'Room Facilities updated successfully!');
            } else {
                $this->getRoomFacilitiesLib()->addRoomFacilities($data);
                Session::flash('success_msg', 'Room Facilities saved successfully!');
            }
        }

        return redirect()->route('room.facilities');
    }

    /*
     * Hotel Room Facility Delete
     */

    public function room_destroy($id, Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            $deleted = $this->getRoomFacilitiesLib()->deleteRoomFacilities($id);
            if ($deleted) {
                $response['success'] = true;
                $response['message'] = 'Room Facilities has been deleted successfully!';
                $response['msg_status'] = 'success';
            } else {
                $response['message'] = "Room Facilities can't deleted!";
                $response['msg_status'] = 'warning';
            }
        }
        exit(json_encode($response));
    }

}
