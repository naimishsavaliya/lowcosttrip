<?php

namespace App\Http\Controllers\admin;

//use App\sector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sectors;
use App\Http\Controllers\ControllerAbstract;
use Session;
use Validator;

class SectorController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Sector Master'
            )
        );
        return view('admin/sector/sector', $data);
    }

    public function data(Request $request) {
        return datatables()->of(Sectors::select('sector.id', 'sector.name', 'sector.category_id', 'country.country_name', 'state.state_name', 'sector.status', 'sector.created_at', 'country.country_id', 'state.state_id')
                                ->join('country', 'country.country_id', '=', 'sector.country_id')
                                ->join('state', 'state.state_id', '=', 'sector.state_id')
                                ->where('sector.deleted_at', null))
                        ->editColumn('category_id', function ($model) {
                            if ($model->category_id == 1) {
                                return "Domestic";
                            } else if ($model->category_id == 2) {
                                return "International";
                            } else if ($model->category_id == 3) {
                                return "Pilgrimage";
                            }else{
                                return "-";
                            }
                        })
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->filterColumn('category_id', function ($query, $keyword) use ($request) {
                            if (isset($request['search']) && $request['search']['value'] != '') {
                                $keyword = $request['search']['value'];
                            }
                            $category_id = $this->getCommonLib()->getsectorCategoryId($keyword);
                            if(count($category_id) >0){
                                $query->where("category_id",implode(',',$category_id), 'in',false );
                            }
                        })
                        ->addColumn('action', '')
//                        ->rawColumns(['status', 'action'])
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));

        $data['breadcrumb'] = array(
            array(
                'link' => route('sector.index'),
                'title' => 'Manage Sector Master'
            ),
            array(
                'title' => 'Add Sector'
            )
        );

        return view('admin/sector/sector-manage', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function show(sector $sector) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['sector'] = $this->getSectorLib()->getSectorById($id);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['states'] = $this->getStateLib()->getStateByCountry($data['sector']->country_id, array('state_id', 'state_name'));

        $data['breadcrumb'] = array(
            array(
                'link' => route('sector.index'),
                'title' => 'Manage Sector Master'
            ),
            array(
                'title' => 'Edit Sector'
            )
        );

        return view('admin/sector/sector-manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request) {
        //validate sector data

        $rules = [
            'name' => 'required',
            'category_id' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'name' => 'Please enter sector name',
            'category_id' => 'Please select category',
            'country_id' => 'Please select country',
            'state_id' => 'Please select state',
            'status' => 'Please select status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (isset($request->id)) {
                return redirect()->route('sector.edit', $request->id)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('sector.add')->withErrors($validator)->withInput();
            }
        } else {
            //get sector data
            $sectorData = $request->all();

            $data = array(
                'name' => $sectorData['name'],
                'description' => $sectorData['description'],
                'category_id' => $sectorData['category_id'],
                'country_id' => $sectorData['country_id'],
                'state_id' => $sectorData['state_id'],
                'status' => $sectorData['status'],
            );

             //Updoad image and update in db
            if ($sectorData['id'] > 0) {
                $secID  = $sectorData['id'] ;
                $this->getSectorLib()->updateSector($data, $sectorData['id']);
                Session::flash('success_msg', 'Sector has been updated successfully!');
            } else {
                $secID = $this->getSectorLib()->addSector($data);
                Session::flash('success_msg', 'Sector has been saved successfully!');
            }

            $image = $request->file('logo');
            if (!empty($image) && $secID > 0) {
                $imagename = $secID . '_sector.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/sector-logo');
                $image->move($destinationPath, $imagename);
                $image_data['logo'] = $imagename;
                $this->getSectorLib()->updateSector($image_data, $sectorData['id']);
            }
        }

        return redirect()->route('sector.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = array("deleted" => true);
        $deleted = $this->getSectorLib()->deleteSector($id);
        if ($deleted) {
            Session::flash('success_msg', 'Sector deleted successfully!');
        } else {
            Session::flash('error_msg', "Sector can't deleted successfully!");
        }
        return redirect()->route('sector.index');
    }

}
