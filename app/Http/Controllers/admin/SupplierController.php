<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use App\Suppliertype;
use App\User;
use Session;
use DB;

class SupplierController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        return view('admin/suppliers/index', $data);
    }

    /*
     * Retrun Supplier Data in JSON
     */

    public function data(request $request) {
        return datatables()->of(User::select('users.id', 'users.email', 'users.created_at', DB::raw("CONCAT(users.first_name,' ',users.last_name) as full_name"), 'suppliers.contact_no', 'users.id as action', 'users.id as notes','country.country_name','supplier_type.type')
                                ->join('suppliers', 'suppliers.supId', '=', 'users.id')
                                ->join('country', 'country.country_id', '=', 'suppliers.country_id')
                                ->join('supplier_type', 'supplier_type.supTypeId', '=', 'suppliers.sup_type')
                                ->where('users.deleted_at', null)
                                ->where('role_id', 3)
                        )
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->editColumn('notes', function ($model) {
                            return '<div data-uk-tooltip="{cls:"long-text"}" title="Ad amet assumenda deserunt doloremque ea enim fuga illum magni, molestiae placeat praesentium quae recusandae rerum sed, vel. Animi architecto consectetur numquam! <br /> 23/July/2018 20:12:06">non, bibendum sed, est. Nunc laoreet 8<//div>';
                        })
                        ->editColumn('action', function ($model) {
                            $supp_view = route('supplier.view', $model->id);
                            $status_target = "{target:'#update_status'}";
                            $cfrm_msg = "'Are you sure to delete?'";

                            $action = '';
                            $action .= '<div class="uk-button-dropdown" data-uk-dropdown="{pos:bottom-right}">';
                            $action .= '<button class="md-btn"><i class="material-icons">settings</i> <i class="material-icons">&#xE313;</i></button>';
                            $action .= '<div class="uk-dropdown">';
                            $action .= '<ul class="uk-nav uk-nav-dropdown">';
                            $action .= '<li><a href="' . $supp_view . '" title="View Agent" class="">View</a></li>';
                            $action .= '<li><a href="' . $supp_view . '" title="Edit Agent" class="">Edit</a></li>';
                            $action .= '<li><a data-uk-modal="' . $status_target . '"> Update Status</a></li>';
                            $action .= '<li><a href="' . $supp_view . '#followup" title="Follow-Up Notes" class="">Follow-Up Notes</a></li>';
                            $action .= '<li><a href="' . $supp_view . '#documents" title="Manage Documents" class="">Manage Documents</a></li>';
                            $action .= '<li><a href="' . $supp_view . '#invoice" title="Manage Invoice" class="">Manage Invoice</a></li>';
                            $action .= '<li><a href="' . $supp_view . '#ledger" title="Manage Leger" class="">Manage Ledger</a></li>';
                            $action .= '<li><a href="' . $supp_view . '" onclick="return confirm(' . $cfrm_msg . ');" title="Delete Agent" class="">Delete</a></li>';
                            $action .= '</ul>';
                            $action .= '</div>';
                            $action .= '</div>';
                            return $action;
                        })
//                        ->addColumn('notes', '')
                        ->rawColumns(['status', 'action','notes'])
                        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
//        $data['states'] = $this->getStateLib()->getStateByCountry($data['supplier']->country_id, array('state_id', 'state_name'));
//        $data['cities'] = $this->getCityLib()->getCityByState($data['supplier']->state_id, array('city_id', 'city_name'));
        $data['certificates'] = $this->getCertificatesLib()->getCertificates(array('cerId', 'name'));
        $data['supplier_types'] = $this->getSuppliertypeLib()->getSuppliertype(array('supTypeId', 'type'));
        return view('admin/suppliers/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
//            'email' => 'required|email|unique:users',
        ]);

        $requestData = $request->all();


        $userData = array(
            'first_name' => $requestData['first_name'],
            'last_name' => $requestData['last_name'],
            'email' => $requestData['email'],
            'password' => bcrypt($requestData['password']),
            'role_id' => $requestData['user_type'],
            'status' => $requestData['status'],
        );

        $supplierData = array(
            'sup_type' => $requestData['user_type'],
            'camapnay_name' => $requestData['camapnay_name'],
            'contact_no' => $requestData['contact_no'],
            'office_no' => $requestData['office_no'],
            'supp_email' => $requestData['supp_email'],
            'address' => $requestData['address'],
            'gst_number' => $requestData['gst_number'],
            'gst_name' => $requestData['gst_name'],
            'gst_address' => $requestData['gst_address'],
            'country_id' => $requestData['country_id'],
            'state_id' => $requestData['state_id'],
            'city_id' => $requestData['city_id'],
            'pin_code' => $requestData['pin_code'],
            'register_mobile' => $requestData['register_mobile'],
            'notes' => $requestData['notes'],
            'logo' => 'test',
        );

        if ($requestData['id'] > 0) {
            $this->getUserLib()->updateUser($userData, $requestData['id']);
            $this->getSupplierLib()->updateSupplier($supplierData, $requestData['id']);
            Session::flash('success_msg', 'User updated successfully!');
        } else {
            $user_id = $this->getUserLib()->addUser($userData);
            $supplierData['supId'] = $user_id;
            $this->getSupplierLib()->addSupplier($supplierData);
            Session::flash('success_msg', 'User saved successfully!');
        }
        return redirect()->route('supplier.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data['supid'] = $id;
        $data['supplier'] = $this->getUserLib()->getSupplierWithProfile($id, array('users.*', 'users.id as user_id', 'suppliers.*'));
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['states'] = $this->getStateLib()->getStateByCountry($data['supplier']->country_id, array('state_id', 'state_name'));
        $data['cities'] = $this->getCityLib()->getCityByState($data['supplier']->state_id, array('city_id', 'city_name'));
        $data['certificates'] = $this->getCertificatesLib()->getCertificates(array('cerId', 'name'));
        $data['supplier_types'] = $this->getSuppliertypeLib()->getSuppliertype(array('supTypeId', 'type'));
        return view('admin/suppliers/view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['supplier'] = $this->getUserLib()->getSupplierWithProfile($id, array('users.*', 'users.id as user_id', 'suppliers.*'));
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        $data['states'] = $this->getStateLib()->getStateByCountry($data['supplier']->country_id, array('state_id', 'state_name'));
        $data['cities'] = $this->getCityLib()->getCityByState($data['supplier']->state_id, array('city_id', 'city_name'));
        $data['certificates'] = $this->getCertificatesLib()->getCertificates(array('cerId', 'name'));
        $data['supplier_types'] = $this->getSuppliertypeLib()->getSuppliertype(array('supTypeId', 'type'));
        return view('admin/suppliers/create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->getSupplierLib()->deleteSupplier($id);
        if ($deleted) {
            Session::flash('success_msg', 'Supplier deleted successfully!');
        } else {
            Session::flash('error_msg', "Supplier can't deleted!");
        }
        return redirect()->route('user.index');
    }

    /*
     * Supplier Type List
     */

    public function supplier_type() {
        $data['breadcrumb'] = array(
            array(
                'title' => 'Manage Supplier Type'
            )
        );
        return view('admin/suppliers/type_list', $data);
    }

    /*
     * Suppplier type data
     */

    public function supplier_type_data() {
        return datatables()->of(Suppliertype::select('supTypeId', 'type', 'description', 'status', 'created_at')
                                ->where('deleted_at', null)
                        )
                        ->editColumn('created_at', function ($model) {
                            return date("d M Y", strtotime($model->created_at));
                        })
                        ->addColumn('action', '')
                        //->rawColumns(['status', 'action'])
                        ->toJson();
    }

    /*
     * Supplier Type Add
     */

    public function supplier_type_add() {
        $data['breadcrumb'] = array(
            array(
                'link' => route('supplier.type.index'),
                'title' => 'Manage Supplier Type'
            ),
            array(
                'title' => 'Add Supplier Type'
            )
        );
        return view('admin/suppliers/supplier_type_add', $data);
    }

    /*
     * Supplier Type Edit
     */

    public function supplier_type_edit($id) {
        $data['supplier_type'] = $this->getSuppliertypeLib()->getSuppliertypeById($id);
        $data['breadcrumb'] = array(
            array(
                'link' => route('supplier.type.index'),
                'title' => 'Manage Supplier Type'
            ),
            array(
                'title' => 'Edit Supplier Type'
            )
        );
        return view('admin/suppliers/supplier_type_add', $data);
    }

    /*
     * 
     */

    public function supplier_type_store(Request $request) {
        //validate Supplier Type data
        $this->validate($request, [
            'type' => 'required',
        ]);

        //get Supplier Type data
        $suppliertypeData = $request->all();
        $data = array(
            'type' => $suppliertypeData['type'],
            'description' => $suppliertypeData['description'],
            'status' => $suppliertypeData['status'],
        );


        if ($suppliertypeData['id'] > 0) {
            $this->getSuppliertypeLib()->updateSuppliertype($data, $suppliertypeData['id']);
            Session::flash('success_msg', 'Supplier Type updated successfully!');
        } else {
            $this->getSuppliertypeLib()->addSuppliertype($data);
            Session::flash('success_msg', 'Supplier Type saved successfully!');
        }

        return redirect()->route('supplier.type.index');
    }

    /*
     * Supplier Type Delete
     */

    public function supplier_type_destroy($id) {
        $deleted = $this->getSuppliertypeLib()->deleteSuppliertype($id);
        if ($deleted) {
            Session::flash('success_msg', 'Supplier Type deleted successfully!');
        } else {
            Session::flash('error_msg', "Supplier Type can't deleted!");
        }
        return redirect()->route('supplier.type.index');
    }

    /*
     * Supplier Note list
     */

    public function notes($supid) {
        $data['supid'] = $supid;
        return view('admin/suppliers/agent_notes', $data);
    }

    /*
     * Supplier Note Add
     */

    public function notes_add($supid) {
        $data['supid'] = $supid;
        return view('admin/suppliers/note_create', $data);
    }

    /*
     * Supplier Note 
     * editAdd
     */

    public function notes_edit($supid, $id) {
        $data['supid'] = $supid;
        return view('admin/suppliers/note_create', $data);
    }

    /*
     * Supplier Note 
     * editAdd
     */

    public function notes_delete($supid, $id) {
        $data['supid'] = $supid;
        return view('admin/suppliers/agent_notes', $data);
    }

    /*
     * Supplier Dcoument Add
     */

    public function document_add($supid) {
        $data = array();
        return view('admin/suppliers/manage-document', $data);
    }

    /*
     * Supplier Dcoument Add
     */

    public function document_edit($supid, $id) {
        $data['supid'] = $supid;
        return view('admin/suppliers/manage-document', $data);
    }

}
