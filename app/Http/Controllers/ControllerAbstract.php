<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\S3;

//use Illuminate\Support\Collection as Collection;
//use Illuminate\Support\Facades\Auth;

abstract class ControllerAbstract extends Controller {

    /**
     * pagination params
     *
     * @var array
     */
    public static $paginationParams = array(
        'page' => null,
        'itemsPerPage' => null,
    );

    /**
     * request object
     *
     * @var \Illuminate\Http\Request;
     */
    protected $_request = null;

    /**
     * filter params
     *
     * @var array
     */
    protected $_filterParams = array();

    /**
     * pagination params
     *
     * @var array
     */
    protected $_paginationParams = array();

    /**
     * request params
     *
     * @var array
     */
    protected $_requestParams = array();

    /**
     * route params
     *
     * @var array
     */
    protected $_routeParams = array();

    /**
     * category Lib
     *
     * @var \App\Libraries\CategoryLib
     */
    private $_categoryLib = null;
    /**
     * payment Lib
     *
     * @var \App\Libraries\PaymentLib
     */
    private $_paymentLib = null;

    /**
     * quotationpayment lib 
     *
     * @var \App\Libraries\QuotationPaymentLib
     */
    private $_quotationpaymentlib = null;

    /**
     * location Lib
     *
     * @var \App\Libraries\LocationLib
     */
    private $_locationLib = null;

    /**
     * country Lib
     *
     * @var \App\Libraries\CountryLib
     */
    private $_countryLib = null;

    /**
     * state Lib
     *
     * @var \App\Libraries\StateLib
     */
    private $_stateLib = null;

    /**
     * city Lib
     *
     * @var \App\Libraries\CityLib
     */
    private $_cityLib = null;

    /**
     * theme Lib
     *
     * @var \App\Libraries\ThemeLib
     */
    private $_themeLib = null;

    /**
     * level Lib
     *
     * @var \App\Libraries\LevelLib
     */
    private $_levelLib = null;

    /**
     * course Lib
     *
     * @var \App\Libraries\CourseLib
     */
    private $_courseLib = null;

    /**
     * chapter Lib
     *
     * @var \App\Libraries\ChapterLib
     */
    private $_chapterLib = null;

    /**
     * chapter Lib
     *
     * @var \App\Libraries\SectorLib
     */
    private $_sectorLib = null;

    /**
     * chapter Lib
     *
     * @var \App\Libraries\SectorLib
     */
    private $_tourinfoLib = null;

    /**
     * Destinations Lib
     *
     * @var \App\Libraries\DestinationsLib
     */
    private $_destinationsLib = null;

    /**
     * HotelFacilities Lib
     *
     * @var \App\Libraries\HotelfacilitiesLib
     */
    private $_hotelfacilitiesLib = null;

    /**
     * Certificates Lib
     *
     * @var \App\Libraries\certificatesLib
     */
    private $_certificatesLib = null;

    /**
     * Discount Lib
     *
     * @var \App\Libraries\discountLib
     */
    private $_discountLib = null;

    /**
     * User Lib
     *
     * @var \App\Libraries\userLib
     */
    private $_userLib = null;

    /**
     * accomodation Lib
     *
     * @var \App\Libraries\AccomodationLib
     */
    private $_accomodationLib = null;
	
    /**
     * Customer info Lib
     *
     * @var \App\Libraries\CustomerinfoLib
     */
    private $_customerinfoLib = null;

	/**
     * cnfleadautono Lib
     *
     * @var \App\Libraries\CnfLeadAutoNoLib
     */
    private $_cnfleadautonoLib = null;
	
    /**
     * Userprofile Lib
     *
     * @var \App\Libraries\userprofileLib
     */
    private $_userprofileLib = null;

    /**
     * Role Lib
     *
     * @var \App\Libraries\RoleLib
     */
    private $_roleLib = null;

    /**
     * Suppliertype Lib
     *
     * @var \App\Libraries\SuppliertypeLib
     */
    private $_suppliertypeLib = null;

    /**
     * Supplier Lib
     *
     * @var \App\Libraries\SupplierLib
     */
    private $_supplierLib = null;

    /**
     * Common Data Lib
     *
     * @var \App\Libraries\CommonLib
     */
    private $_commonLib = null;

    /**
     * Currency Master Lib
     *
     * @var \App\Libraries\CurrencymasterLib
     */
    private $_currencymasterLib = null;

    /**
     * Hotel Type Master Lib
     *
     * @var \App\Libraries\HoteltypeLib
     */
    private $_hoteltypemasterLib = null;

    /**
     * Room Type Master Lib
     *
     * @var \App\Libraries\RoomltypeLib
     */
    private $_roomtypemasterLib = null;

    /**
     * Tour Type Master Lib
     *
     * @var \App\Libraries\TourltypeLib
     */
    private $_tourtypemasterLib = null;

    /**
     * Activity Type Master Lib
     *
     * @var \App\Libraries\ActivitytypeLib
     */
    private $_activitytypemasterLib = null;

    /**
     * Lead Status Master Lib
     *
     * @var \App\Libraries\LeadstatusLib
     */
    private $_leadstatusmasterLib = null;

    /**
     * Customer type Master Lib
     *
     * @var \App\Libraries\CustomertypeLib
     */
    private $_customertypemasterLib = null;

    /**
     * Tour package type Master Lib
     *
     * @var \App\Libraries\Tourpackagetype
     */
    private $_tourpackagetypemasterLib = null;

    /**
     * Tour package type Master Lib
     *
     * @var \App\Libraries\Tourpackagetype
     */
    private $_ratetypemasterLib = null;

    /**
     * Feature tour Master Lib
     *
     * @var \App\Libraries\FeaturetourLib
     */
    private $_featuretourmasterLib = null;

    /**
     * Home Banner Lib
     *
     * @var \App\Libraries\HomebannerLib
     */
    private $_homebannerLib = null;

    private $_hometoursectionLib = null;

    /**
     * Album Category Lib
     *
     * @var \App\Libraries\AlbumcategoryLib
     */
    private $_albumcategoryLib = null;

    /**
     * Albums Lib
     *
     * @var \App\Libraries\AlbumsLib
     */
    private $_albumsLib = null;

    /**
     * videocategoryLib
     *
     * @var \App\Libraries\videocategoryLib
     */
    private $_videocategoryLib = null;

    /**
     * tourLib
     *
     * @var \App\Libraries\tourLib
     */
    private $_tourLib = null;

    /**
     * tourhistoryLib
     *
     * @var \App\Libraries\tourhistoryLib
     */
    private $_tourhistoryLib = null;

    /**
     * TourItineraryLib
     *
     * @var \App\Libraries\TourItineraryLib
     */
    private $_tourItineraryLib = null;

    /**
     * TourDepartureLib
     *
     * @var \App\Libraries\TourDepartureLib
     */
    private $_tourDepartureLib = null;

    /**
     * TourFAQLib
     *
     * @var \App\Libraries\TourFAQLib
     */
    private $_tourFAQLib = null;

    /**
     * TourCost
     *
     * @var \App\Libraries\TourCost
     */
    private $_tourCostLib = null;

    /**
     * TourCostAmount
     *
     * @var \App\Libraries\TourCostAmount
     */
    private $_tourCostAmountLib = null;

    /**
     * TourHotelt
     *
     * @var \App\Libraries\TourHotel
     */
    private $_tourHotelLib = null;

    /**
     * SubscriptionpackageLib
     *
     * @var \App\Libraries\SubscriptionpackageLib
     */
    private $_subscriptionpackageLib = null;

    /**
     * get Lead Lib
     *
     * @var \App\Libraries\LeadLib
     */
    private $_leadLib = null;

    private $_customerpaymentLib = null;


    private $_receiptLib = null;
    
    private $_invoiceLib = null;

    /**
     * get Agent Document Lib
     *
     * @var \App\Libraries\Agentdocument
     */
    private $_agentdocumentLib = null;

    /**
     * get Leadnotes Lib
     *
     * @var \App\Libraries\LeadnotesLib
     */
    private $_leadnotesLib = null;

    /**
     * get Leadnotes Lib
     *
     * @var \App\Libraries\LeadPackagesHotelsLib
     */
    private $_leadpackageshotelsLib = null;

    /**
     * get Leadnotes Lib
     *
     * @var \App\Libraries\LeadPackagesLib
     */
    private $_leadpackagesLib = null;

    /**
     * get LeadDocument Lib
     *
     * @var \App\Libraries\LeadDocumentLib
     */
    private $_leaddocumentLib = null;

    /**
     * get LeadQuatation Lib
     *
     * @var \App\Libraries\LeadQuatationLib
     */
    private $_leadquatationLib = null;

    /**
     * get Leadquatationitinarary Lib
     *
     * @var \App\Libraries\LeadquatationitinararyLib
     */
    private $_leaditinararyLib = null;

    /**
     * get Leadquatationparticular Lib
     *
     * @var \App\Libraries\LeadquatationitparticularLib
     */
    private $_leadiparticularLib = null;

    /**
     * get Featuredtour Lib
     *
     * @var \App\Libraries\FeaturedtourLib
     */
    private $_featuredtourLib = null;

    /**
     * get Hottours Lib
     *
     * @var \App\Libraries\HottoursLib
     */
    private $_hottoursLib = null;

    /**
     * get International Lib
     *
     * @var \App\Libraries\internatioanltoursLib
     */
    private $_internatioanltoursLib = null;

    /**
     * get Discountcode Lib
     *
     * @var \App\Libraries\discountcodeLib
     */
    private $_discountcodeLib = null;

    /**
     * get AlbumVideo Lib
     *
     * @var \App\Libraries\AlbumvideoLib
     */
    private $_albumvideoLib = null;

    /**
     * get Webpopup Lib
     *
     * @var \App\Libraries\WebpopupLib
     */
    private $_webpopLib = null;

    /**
     * get Testimonial Lib
     *
     * @var \App\Libraries\TestimonialLib
     */
    private $_testimonialLib = null;

    /**
     * get Roomfacilitie Lib
     *
     * @var \App\Libraries\Roomfacilities
     */
    private $_roomfacilitiesLib = null;

    /**
     * get Hotel Lib
     *
     * @var \App\Libraries\HotelLib
     */
    private $_hotelLib = null;
    
    /**
     * get RoomPriceType Lib
     *
     * @var \App\Libraries\HotelLib
     */
    private $_roompricetypeLib = null;
    
    /**
     * get Hotelpromote Lib
     *
     * @var \App\Libraries\HotelpromoteLib
     */
    private $_hotelpromoteLib = null;
    
    /**
     * get Hotelfeatured Lib
     *
     * @var \App\Libraries\HotelpromoteLib
     */
    private $_hotelfeaturedLib = null;
	
	/**
     * get holiday destination details Lib
     *
     * @var \App\Libraries\HolidaydestinationdetailsLib
     */
    private $_holidaydestinationdetailsLib = null;

	private $_holidayactivitiesLib = null;

    private $_domesticHolidayLib = null;

    private $_internationalHolidayLib = null;

    private $_featuredTblTourLib = null;

    private $_hotDealTourLib = null;

    private $_newArrivalTourLib = null;

    private $_featuredDestinationTourLib = null;

    private $_bookActivitiesLib = null;

    private $_newsletterLib = null;

    

    private $_flightReviewInfoLib = null;

    private $_flightPassengersLib = null;

    private $_flightBooksLib = null;

    private $_walletLib = null;



	/*     * *******************************************************************
     *                                                                   *
     * PUBLIC MAIN FUNCTIONS                                             *
     *                                                                   *
     * ******************************************************************* */

    /**
     * constructor
     * @param Request $request
     * @throws UndefinedVersionException
     */
    public function __construct(Request $request) {
        $this->_init();
    }

    /*     * *******************************************************************
     *                                                                   *
     * PUBLIC HELPER FUNCTIONS                                           *
     *                                                                   *
     * ******************************************************************* */

    /**
     * get request object
     *
     * @return \Illuminate\Http\Request;
     */
    public function getRequest(): \Illuminate\Http\Request {
        return $this->_request;
    }

    /**
     * get api version
     *
     * @return string
     */
    /* public function getApiVersion()
      {
      return $this->_apiVersion;
      } */

    /**
     * set filter params
     *
     * @param array $val
     * @return \App\Http\Controllers\Api\ControllerAbstract
     */
    public function setFilterParams(array $val = array()): \App\Http\Controllers\Api\ControllerAbstract {
        $this->_filterParams = $val;
        return $this;
    }

    /**
     * merge filter params
     *
     * @param array $val
     * @return \App\Http\Controllers\Api\ControllerAbstract
     */
    public function mergeFilterParams(array $val = array()) {
        return $this->setFilterParams(array_merge($this->getFilterParams(), $val));
    }

    /**
     * get filter params
     *
     * @return array
     */
    public function getFilterParams(): array {
        return $this->_filterParams;
    }

    /**
     * set pagination params
     *
     * @param array $val
     * @return \App\Http\Controllers\Api\ControllerAbstract
     */
    public function setPaginationParams(array $val = array()): \App\Http\Controllers\Api\ControllerAbstract {
        $this->_paginationParams = $val;
        return $this;
    }

    /**
     * get pagination params
     *
     * @return array
     */
    public function getPaginationParams(): array {
        return $this->_paginationParams;
    }

    /**
     * set request params
     *
     * @param array $val
     * @return \App\Http\Controllers\Api\ControllerAbstract
     */
    public function setRequestParams(array $val = array()): \App\Http\Controllers\Api\ControllerAbstract {
        $this->_requestParams = $val;
        return $this;
    }

    /**
     * get request params
     *
     * @return array
     */
    public function getRequestParams(): array {
        return $this->_requestParams;
    }

    /**
     * set route params
     *
     * @param array $val
     * @return \App\Http\Controllers\Api\ControllerAbstract
     */
    public function setRouteParams(array $val = array()): \App\Http\Controllers\Api\ControllerAbstract {
        $this->_routeParams = $val;
        return $this;
    }

    /**
     * get route params
     *
     * @return array
     */
    public function getRouteParams(): array {
        return $this->_routeParams;
    }

    /**
     * get Category lib
     *
     * @return \App\Libraries\CategoryLib
     */
    public final function getCategoryLib() {
        if (!($this->_categoryLib instanceof \App\Libraries\CategoryLib)) {
            $this->_categoryLib = new \App\Libraries\CategoryLib();
        }
        return $this->_categoryLib;
    }

    /**
     * get Payment lib
     *
     * @return \App\Libraries\PaymentLib
     */
    public final function getPaymentLib() {
        if (!($this->_paymentLib instanceof \App\Libraries\PaymentLib)) {
            $this->_paymentLib = new \App\Libraries\PaymentLib();
        }
        return $this->_paymentLib;
    }
    /**
     * get Payment lib
     *
     * @return \App\Libraries\QuotationPaymentLib
     */
    public final function getQuotationPaymentLib() {
        if (!($this->_quotationpaymentlib instanceof \App\Libraries\QuotationPaymentLib)) {
            $this->_quotationpaymentlib = new \App\Libraries\QuotationPaymentLib();
        }
        return $this->_quotationpaymentlib;
    }
	
    /**
     * get Holiday destination details lib
     *
     * @return \App\Libraries\HolidaydestinationdetailsLib
     */
    public final function getHolidaydestinationdetailsLib() {
        if(!($this->_holidaydestinationdetailsLib instanceof \App\Libraries\HolidaydestinationdetailsLib)) {
            $this->_holidaydestinationdetailsLib = New \App\Libraries\HolidaydestinationdetailsLib();
        }
        return $this->_holidaydestinationdetailsLib;
    }
	/**
     * get Holiday destination details lib
     *
     * @return \App\Libraries\HolidayactivitiesLib
     */
	public final function getHolidayactivitiesLib() {
		if(!($this->_holidayactivitiesLib instanceof \App\Libraries\HolidayactivitiesLib)) {
			$this->_holidayactivitiesLib = New \App\Libraries\HolidayactivitiesLib();
		}
		return $this->_holidayactivitiesLib;
	}

    /**
     * get Location lib
     *
     * @return \App\Libraries\LocationLib
     */
    public final function getLocationLib() {
        if (!($this->_locationLib instanceof \App\Libraries\LocationLib)) {
            $this->_locationLib = new \App\Libraries\LocationLib();
        }
        return $this->_locationLib;
    }

    /**
     * get Country lib
     *
     * @return \App\Libraries\CountryLib
     */
    public final function getCountryLib() {
        if (!($this->_countryLib instanceof \App\Libraries\CountryLib)) {
            $this->_countryLib = new \App\Libraries\CountryLib();
        }
        return $this->_countryLib;
    }

    /**
     * get State lib
     *
     * @return \App\Libraries\StateLib
     */
    public final function getStateLib() {
        if (!($this->_stateLib instanceof \App\Libraries\StateLib)) {
            $this->_stateLib = new \App\Libraries\StateLib();
        }
        return $this->_stateLib;
    }

    /**
     * get City lib
     *
     * @return \App\Libraries\CityLib
     */
    public final function getCityLib() {
        if (!($this->_cityLib instanceof \App\Libraries\CityLib)) {
            $this->_cityLib = new \App\Libraries\CityLib();
        }
        return $this->_cityLib;
    }

    /**
     * get Theme lib
     *
     * @return \App\Libraries\ThemeLib
     */
    public final function getThemeLib() {
        if (!($this->_themeLib instanceof \App\Libraries\ThemeLib)) {
            $this->_themeLib = new \App\Libraries\ThemeLib();
        }
        return $this->_themeLib;
    }

    /**
     * get level lib
     *
     * @return \App\Libraries\LevelLib
     */
    public final function getLevelLib() {
        if (!($this->_levelLib instanceof \App\Libraries\LevelLib)) {
            $this->_levelLib = new \App\Libraries\LevelLib();
        }
        return $this->_levelLib;
    }

    /**
     * get course lib
     *
     * @return \App\Libraries\CourseLib
     */
    public final function getCourseLib() {
        if (!($this->_courseLib instanceof \App\Libraries\CourseLib)) {
            $this->_courseLib = new \App\Libraries\CourseLib();
        }
        return $this->_courseLib;
    }

    /**
     * get course lib
     *
     * @return \App\Libraries\CourseLib
     */
    public final function getChapterLib() {
        if (!($this->_chapterLib instanceof \App\Libraries\ChapterLib)) {
            $this->_chapterLib = new \App\Libraries\ChapterLib();
        }
        return $this->_chapterLib;
    }

    /**
     * get course lib
     *
     * @return \App\Libraries\DestinationsLib
     */
    public final function getDestinationsLib() {
        if (!($this->_destinationsLib instanceof \App\Libraries\DestinationsLib)) {
            $this->_destinationsLib = new \App\Libraries\DestinationsLib();
        }
        return $this->_destinationsLib;
    }

    /**
     * get course lib
     *
     * @return \App\Libraries\HotelfacilitiesLib
     */
    public final function getHotelfacilitiesLib() {
        if (!($this->_hotelfacilitiesLib instanceof \App\Libraries\HotelfacilitiesLib)) {
            $this->_hotelfacilitiesLib = new \App\Libraries\HotelfacilitiesLib();
        }
        return $this->_hotelfacilitiesLib;
    }

    /**
     * get course lib
     *
     * @return \App\Libraries\SectorLib
     */
    public final function getSectorLib() {
        if (!($this->_sectorLib instanceof \App\Libraries\SectorLib)) {
            $this->_sectorLib = new \App\Libraries\SectorLib();
        }
        return $this->_sectorLib;
    }

    /**
     * get course lib
     *
     * @return \App\Libraries\SectorLib
     */
    public final function getTourInfoLib() {
        if (!($this->_tourinfoLib instanceof \App\Libraries\TourInfoLib)) {
            $this->_tourinfoLib = new \App\Libraries\TourInfoLib();
        }
        return $this->_tourinfoLib;
    }

    /**
     * get course lib
     *
     * @return \App\Libraries\SectorLib
     */
    public final function getCertificatesLib() {
        if (!($this->_certificatesLib instanceof \App\Libraries\CertificatesLib)) {
            $this->_certificatesLib = new \App\Libraries\CertificatesLib();
        }
        return $this->_certificatesLib;
    }

    /**
     * get discount lib
     *
     * @return \App\Libraries\DiscountLib
     */
    public final function getDiscountLib() {
        if (!($this->_discountLib instanceof \App\Libraries\DiscountLib)) {
            $this->_discountLib = new \App\Libraries\DiscountLib();
        }
        return $this->_discountLib;
    }

    /**
     * get User lib
     *
     * @return \App\Libraries\UserLib
     */
    public final function getUserLib() {
        if (!($this->_userLib instanceof \App\Libraries\UserLib)) {
            $this->_userLib = new \App\Libraries\UserLib();
        }
        return $this->_userLib;
    }

    /**
     * get Customer info lib
     *
     * @return \App\Libraries\CustomerinfoLib
     */
    public final function getCustomerinfoLib() {
        if (!($this->_customerinfoLib instanceof \App\Libraries\CustomerinfoLib)) {
            $this->_customerinfoLib = new \App\Libraries\CustomerinfoLib();
        }
        return $this->_customerinfoLib;
    }
	
	/**
     * get CnfLeadAutoNo lib
     *
     * @return \App\Libraries\CnfLeadAutoNoLib
     */
    public final function getCnfLeadAutoNoLib() {
        if (!($this->_cnfleadautonoLib instanceof \App\Libraries\CnfLeadAutoNoLib)) {
            $this->_cnfleadautonoLib = new \App\Libraries\CnfLeadAutoNoLib();
        }
        return $this->_cnfleadautonoLib;
    }
	
	/**
     * get Accomodation lib
     *
     * @return \App\Libraries\AccomodationLib
     */
    public final function getAccomodationLib() {
        if (!($this->_accomodationLib instanceof \App\Libraries\AccomodationLib)) {
            $this->_accomodationLib = new \App\Libraries\AccomodationLib();
        }
        return $this->_accomodationLib;
    }

    /**
     * get Userprofile lib
     *
     * @return \App\Libraries\UserprofileLib
     */
    public final function getUserprofileLib() {
        if (!($this->_userprofileLib instanceof \App\Libraries\UserprofileLib)) {
            $this->_userprofileLib = new \App\Libraries\UserprofileLib();
        }
        return $this->_userprofileLib;
    }

    /**
     * get Role lib
     *
     * @return \App\Libraries\RoleLib
     */
    public final function getRoleLib() {
        if (!($this->_roleLib instanceof \App\Libraries\RoleLib)) {
            $this->_roleLib = new \App\Libraries\RoleLib();
        }
        return $this->_roleLib;
    }

    /**
     * get Supplier lib
     *
     * @return \App\Libraries\SuppliertypeLib
     */
    public final function getSuppliertypeLib() {
        if (!($this->_suppliertypeLib instanceof \App\Libraries\SuppliertypeLib)) {
            $this->_suppliertypeLib = new \App\Libraries\SuppliertypeLib();
        }
        return $this->_suppliertypeLib;
    }

    /**
     * get Supplier lib
     *
     * @return \App\Libraries\SupplierLib
     */
    public final function getSupplierLib() {
        if (!($this->_supplierLib instanceof \App\Libraries\SupplierLib)) {
            $this->_supplierLib = new \App\Libraries\SupplierLib();
        }
        return $this->_supplierLib;
    }

    /**
     * get Supplier lib
     *
     * @return \App\Libraries\SupplierLib
     */
    public final function getCommonLib() {
        if (!($this->_commonLib instanceof \App\Libraries\CommonLib)) {
            $this->_commonLib = new \App\Libraries\CommonLib();
        }
        return $this->_commonLib;
    }

    /**
     * get Supplier lib
     *
     * @return \App\Libraries\CurrencymasterLib
     */
    public final function getCurrencymasterLib() {
        if (!($this->_currencymasterLib instanceof \App\Libraries\CurrencymasterLib)) {
            $this->_currencymasterLib = new \App\Libraries\CurrencymasterLib();
        }
        return $this->_currencymasterLib;
    }

    /**
     * get Hoteltype lib
     *
     * @return \App\Libraries\HoteltypeLibs
     */
    public final function getHoteltypemasterLib() {
        if (!($this->_hoteltypemasterLib instanceof \App\Libraries\HoteltypeLib)) {
            $this->_hoteltypemasterLib = new \App\Libraries\HoteltypeLib();
        }
        return $this->_hoteltypemasterLib;
    }

    /**
     * get Room Type lib
     *
     * @return \App\Libraries\HoteltypeLibs
     */
    public final function getRoomtypemasterLib() {
        if (!($this->_roomtypemasterLib instanceof \App\Libraries\RoomtypeLib)) {
            $this->_roomtypemasterLib = new \App\Libraries\RoomtypeLib();
        }
        return $this->_roomtypemasterLib;
    }

    /**
     * get Room Type lib
     *
     * @return \App\Libraries\HoteltypeLibs
     */
    public final function getTourtypemasterLib() {
        if (!($this->_tourtypemasterLib instanceof \App\Libraries\TourtypeLib)) {
            $this->_tourtypemasterLib = new \App\Libraries\TourtypeLib();
        }
        return $this->_tourtypemasterLib;
    }

    /**
     * get Activity Type lib
     *
     * @return \App\Libraries\ActivitytypeLibs
     */
    public final function getActivitytypemasterLib() {
        if (!($this->_activitytypemasterLib instanceof \App\Libraries\ActivitytypeLib)) {
            $this->_activitytypemasterLib = new \App\Libraries\ActivitytypeLib();
        }
        return $this->_activitytypemasterLib;
    }

    /**
     * get Lead status lib
     *
     * @return \App\Libraries\LeadstatusLibs
     */
    public final function getLeadstatusmasterLib() {
        if (!($this->_leadstatusmasterLib instanceof \App\Libraries\LeadstatusLib)) {
            $this->_leadstatusmasterLib = new \App\Libraries\LeadstatusLib();
        }
        return $this->_leadstatusmasterLib;
    }

    /**
     * get Customer type lib
     *
     * @return \App\Libraries\CustomertypeLib
     */
    public final function getCustomertypemasterLib() {
        if (!($this->_customertypemasterLib instanceof \App\Libraries\CustomertypeLib)) {
            $this->_customertypemasterLib = new \App\Libraries\CustomertypeLib();
        }
        return $this->_customertypemasterLib;
    }

    /**
     * get Tour package type lib
     *
     * @return \App\Libraries\TourpackagetypeLib
     */
    public final function getTourpackagetypemasterLib() {
        if (!($this->_tourpackagetypemasterLib instanceof \App\Libraries\TourpackagetypeLib)) {
            $this->_tourpackagetypemasterLib = new \App\Libraries\TourpackagetypeLib();
        }
        return $this->_tourpackagetypemasterLib;
    }

    /**
     * get Rate type lib
     *
     * @return \App\Libraries\RatetypeLib
     */
    public final function getRatetypemasterLib() {
        if (!($this->_ratetypemasterLib instanceof \App\Libraries\RatetypeLib)) {
            $this->_ratetypemasterLib = new \App\Libraries\RatetypeLib();
        }
        return $this->_ratetypemasterLib;
    }

    /**
     * get Rate type lib
     *
     * @return \App\Libraries\RatetypeLib
     */
    public final function getFeaturetourmasterLib() {
        if (!($this->_featuretourmasterLib instanceof \App\Libraries\FeaturetourLib)) {
            $this->_featuretourmasterLib = new \App\Libraries\FeaturetourLib();
        }
        return $this->_featuretourmasterLib;
    }

    /**
     * get Home banner lib
     *
     * @return \App\Libraries\HomebannerLib
     */
    public final function getHomebannerLib() {
        if (!($this->_homebannerLib instanceof \App\Libraries\HomebannerLib)) {
            $this->_homebannerLib = new \App\Libraries\HomebannerLib();
        }
        return $this->_homebannerLib;
    }

    public final function getHometoursectionLib() {
        if (!($this->_hometoursectionLib instanceof \App\Libraries\HometoursectionLib)) {
            $this->_hometoursectionLib = new \App\Libraries\HometoursectionLib();
        }
        return $this->_hometoursectionLib;
    }

    /**
     * get Home banner lib
     *
     * @return \App\Libraries\HomebannerLib
     */
    public final function getAlbumcategoryLib() {
        if (!($this->_albumcategoryLib instanceof \App\Libraries\AlbumcategoryLib)) {
            $this->_albumcategoryLib = new \App\Libraries\AlbumcategoryLib();
        }
        return $this->_albumcategoryLib;
    }

    /**
     * get Home banner lib
     *
     * @return \App\Libraries\HomebannerLib
     */
    public final function getAlbumsLib() {
        if (!($this->_albumsLib instanceof \App\Libraries\AlbumsLib)) {
            $this->_albumsLib = new \App\Libraries\AlbumsLib();
        }
        return $this->_albumsLib;
    }

    /**
     * get Home banner lib
     *
     * @return \App\Libraries\HomebannerLib
     */
    public final function getVideocategoryLib() {
        if (!($this->_videocategoryLib instanceof \App\Libraries\VideocategoryLib)) {
            $this->_videocategoryLib = new \App\Libraries\VideocategoryLib();
        }
        return $this->_videocategoryLib;
    }

    /**
     * get tour lib
     *
     * @return \App\Libraries\TourLib
     */
    public final function getTourLib() {
        if (!($this->_tourLib instanceof \App\Libraries\TourLib)) {
            $this->_tourLib = new \App\Libraries\TourLib();
        }
        return $this->_tourLib;
    }

    /**
     * get tourhistoryLib
     *
     * @return \App\Libraries\tourhistoryLib
     */
    public final function getTourHistoryLib() {
        if (!($this->_tourhistoryLib instanceof \App\Libraries\TourHistoryLib)) {
            $this->_tourhistoryLib = new \App\Libraries\TourHistoryLib();
        }
        return $this->_tourhistoryLib;
    }

    /**
     * get TourItineraryLib
     *
     * @return \App\Libraries\TourItineraryLib
     */
    public final function getTourItineraryLib() {
        if (!($this->_tourItineraryLib instanceof \App\Libraries\TourItineraryLib)) {
            $this->_tourItineraryLib = new \App\Libraries\TourItineraryLib();
        }
        return $this->_tourItineraryLib;
    }

    /**
     * get TourDepartureLib
     *
     * @return \App\Libraries\TourDepartureLib
     */
    public final function getTourDepartureLib() {
        if (!($this->_tourDepartureLib instanceof \App\Libraries\TourDepartureLib)) {
            $this->_tourDepartureLib = new \App\Libraries\TourDepartureLib();
        }
        return $this->_tourDepartureLib;
    }

    /**
     * get TourFAQLib
     *
     * @return \App\Libraries\TourFAQLib
     */
    public final function getTourFAQLib() {
        if (!($this->_tourFAQLib instanceof \App\Libraries\TourFAQLib)) {
            $this->_tourFAQLib = new \App\Libraries\TourFAQLib();
        }
        return $this->_tourFAQLib;
    }

    /**
     * get getTourCostLib
     *
     * @return \App\Libraries\getTourCostLib
     */
    public final function getTourCostLib() {
        if (!($this->_tourCostLib instanceof \App\Libraries\TourCostLib)) {
            $this->_tourCostLib = new \App\Libraries\TourCostLib();
        }
        return $this->_tourCostLib;
    }

    /**
     * get getTourCostAmountLib
     *
     * @return \App\Libraries\getTourCostAmountLib
     */
    public final function getTourCostAmountLib() {
        if (!($this->_tourCostAmountLib instanceof \App\Libraries\TourCostAmountLib)) {
            $this->_tourCostAmountLib = new \App\Libraries\TourCostAmountLib();
        }
        return $this->_tourCostAmountLib;
    }

    /**
     * get getTourHotelLib
     *
     * @return \App\Libraries\getTourHotelLib
     */
    public final function getTourHotelLib() {
        if (!($this->_tourHotelLib instanceof \App\Libraries\TourHotelLib)) {
            $this->_tourHotelLib = new \App\Libraries\TourHotelLib();
        }
        return $this->_tourHotelLib;
    }

    /**
     * get getSubscriptionpackageLib lib
     *
     * @return \App\Libraries\getSubscriptionpackageLib
     */
    public final function getSubscriptionpackageLib() {
        if (!($this->_subscriptionpackageLib instanceof \App\Libraries\SubscriptionpackageLib)) {
            $this->_subscriptionpackageLib = new \App\Libraries\SubscriptionpackageLib();
        }
        return $this->_subscriptionpackageLib;
    }

    /**
     * get Lead lib
     *
     * @return \App\Libraries\getLeadLib
     */
    public final function getLeadLib() {
        if (!($this->_leadLib instanceof \App\Libraries\LeadLib)) {
            $this->_leadLib = new \App\Libraries\LeadLib();
        }
        return $this->_leadLib;
    }

    /**
     * get Agent Document lib
     *
     * @return \App\Libraries\getAgentdocumentLib
     */
    public final function getAgentdocumentLib() {
        if (!($this->_agentdocumentLib instanceof \App\Libraries\AgentdocumentLib)) {
            $this->_agentdocumentLib = new \App\Libraries\AgentdocumentLib();
        }
        return $this->_agentdocumentLib;
    }

    /**
     * get Leadnotes lib
     *
     * @return \App\Libraries\getLeadnotesLib
     */
    public final function getLeadnotesLib() {
        if (!($this->_leadnotesLib instanceof \App\Libraries\LeadnotesLib)) {
            $this->_leadnotesLib = new \App\Libraries\LeadnotesLib();
        }
        return $this->_leadnotesLib;
    }


    /**
     * get Leadnotes lib
     *
     * @return \App\Libraries\LeadPackagesHotels
     */
    
    
    public final function getLeadPackagesHotelsLib() {
        if (!($this->_leadpackageshotelsLib instanceof \App\Libraries\LeadPackagesHotelsLib)) {
            $this->_leadpackageshotelsLib = new \App\Libraries\LeadPackagesHotelsLib();
        }
        return $this->_leadpackageshotelsLib;
    }



    /**
     * get Leadnotes lib
     *
     * @return \App\Libraries\LeadPackages
     */
    public final function getLeadPackagesLib() {
        if (!($this->_leadpackagesLib instanceof \App\Libraries\LeadPackagesLib)) {
            $this->_leadpackagesLib = new \App\Libraries\LeadPackagesLib();
        }
        return $this->_leadpackagesLib;
    }


    /**
     * get Leadnotes lib
     *
     * @return \App\Libraries\getLeadnotesLib
     */
    public final function getLeadDocumentLib() {
        if (!($this->_leaddocumentLib instanceof \App\Libraries\LeaddocumentLib)) {
            $this->_leaddocumentLib = new \App\Libraries\LeaddocumentLib();
        }
        return $this->_leaddocumentLib;
    }

    /**
     * get Lead Quatation lib
     *
     * @return \App\Libraries\LeadquatationLib
     */
    public final function getLeadQuatationLib() {
        if (!($this->_leadquatationLib instanceof \App\Libraries\LeadquatationLib)) {
            $this->_leadquatationLib = new \App\Libraries\LeadquatationLib();
        }
        return $this->_leadquatationLib;
    }

    /**
     * get Lead Itinarary lib
     *
     * @return \App\Libraries\LeadquatationLib
     */
    public final function getLeadItinararyLib() {
        if (!($this->_leaditinararyLib instanceof \App\Libraries\LeadquatationitinararyLib)) {
            $this->_leaditinararyLib = new \App\Libraries\LeadquatationitinararyLib();
        }
        return $this->_leaditinararyLib;
    }

    /**
     * get Lead Particular lib
     *
     * @return \App\Libraries\LeadparticularLib
     */
    public final function getLeadParticularLib() {
        if (!($this->_leadiparticularLib instanceof \App\Libraries\LeadparticularLib)) {
            $this->_leadiparticularLib = new \App\Libraries\LeadparticularLib();
        }
        return $this->_leadiparticularLib;
    }

    /**
     * get Featured Tours lib
     *
     * @return \App\Libraries\FeaturedtourLib
     */
    public final function getFeaturedTourLib() {
        if (!($this->_featuredtourLib instanceof \App\Libraries\FeaturedtourLib)) {
            $this->_featuredtourLib = new \App\Libraries\FeaturedtourLib();
        }
        return $this->_featuredtourLib;
    }

    /**
     * get Hottours lib
     *
     * @return \App\Libraries\HottoursLib
     */
    public final function getHotTourLib() {
        if (!($this->_hottoursLib instanceof \App\Libraries\HottoursLib)) {
            $this->_hottoursLib = new \App\Libraries\HottoursLib();
        }
        return $this->_hottoursLib;
    }

    /**
     * get Internatioanl lib
     *
     * @return \App\Libraries\InternatioanlLib
     */
    public final function getInternationalTourLib() {
        if (!($this->_internatioanltoursLib instanceof \App\Libraries\InternationaltourLib)) {
            $this->_internatioanltoursLib = new \App\Libraries\InternationaltourLib();
        }
        return $this->_internatioanltoursLib;
    }

    /**
     * get Discountcode lib
     *
     * @return \App\Libraries\DiscountcodeLib
     */
    public final function getDiscountCodeLib() {
        if (!($this->_discountcodeLib instanceof \App\Libraries\DiscountcodeLib)) {
            $this->_discountcodeLib = new \App\Libraries\DiscountcodeLib();
        }
        return $this->_discountcodeLib;
    }

    /**
     * get Albumvideo lib
     *
     * @return \App\Libraries\Albumvido
     */
    public final function getAlbumVideoLib() {
        if (!($this->_albumvideoLib instanceof \App\Libraries\AlbumvideoLib)) {
            $this->_albumvideoLib = new \App\Libraries\AlbumvideoLib();
        }
        return $this->_albumvideoLib;
    }

    /**
     * get Testimonial lib
     *
     * @return \App\Libraries\TestimonialLib
     */
    public final function getTestimonialLib() {
        if (!($this->_testimonialLib instanceof \App\Libraries\TestimonialLib)) {
            $this->_testimonialLib = new \App\Libraries\TestimonialLib();
        }
        return $this->_testimonialLib;
    }

    /**
     * get Testimonial lib
     *
     * @return \App\Libraries\TestimonialLib
     */
    public final function getWebpopupLib() {
        if (!($this->_webpopLib instanceof \App\Libraries\WebpopupLib)) {
            $this->_webpopLib = new \App\Libraries\WebpopupLib();
        }
        return $this->_webpopLib;
    }

    /**
     * get Testimonial lib
     *
     * @return \App\Libraries\TestimonialLib
     */
    public final function getRoomFacilitiesLib() {
        if (!($this->_roomfacilitiesLib instanceof \App\Libraries\RoomfacilitiesLib)) {
            $this->_roomfacilitiesLib = new \App\Libraries\RoomfacilitiesLib();
        }
        return $this->_roomfacilitiesLib;
    }

    /**
     * get Testimonial lib
     *
     * @return \App\Libraries\TestimonialLib
     */
    public final function getHotelLib() {
        if (!($this->_hotelLib instanceof \App\Libraries\HotelLib)) {
            $this->_hotelLib = new \App\Libraries\HotelLib();
        }
        return $this->_hotelLib;
    }
    
    /**
     * get RoomPriceType lib
     *
     * @return \App\Libraries\RoomPriceTypeLib
     */
    public final function getRoomPriceTypeLib() {
        if (!($this->_roompricetypeLib instanceof \App\Libraries\RoompricetypeLib)) {
            $this->_roompricetypeLib = new \App\Libraries\RoompricetypeLib();
        }
        return $this->_roompricetypeLib;
    }
    
    /**
     * get RoomPriceType lib
     *
     * @return \App\Libraries\RoomPriceTypeLib
     */
    public final function getHotelPromoteLib() {
        if (!($this->_hotelpromoteLib instanceof \App\Libraries\HotelpromoteLib)) {
            $this->_hotelpromoteLib = new \App\Libraries\HotelpromoteLib();
        }
        return $this->_hotelpromoteLib;
    }
    
    /**
     * get RoomPriceType lib
     *
     * @return \App\Libraries\RoomPriceTypeLib
     */
    public final function getHotelFeaturedLib() {
        if (!($this->_hotelfeaturedLib instanceof \App\Libraries\HotelfeaturedLib)) {
            $this->_hotelfeaturedLib = new \App\Libraries\HotelfeaturedLib();
        }
        return $this->_hotelfeaturedLib;
    }

    public final function getCustomerPaymentLib() {
        if (!($this->_customerpaymentLib instanceof \App\Libraries\CustomerPaymentLib)) {
            $this->_customerpaymentLib = new \App\Libraries\CustomerPaymentLib();
        }
        return $this->_customerpaymentLib;
    }
    
    /**
     * get Receipt Lib
     *
     * @return \App\Libraries\getReceiptLib
     */
    public final function getReceipLib() {
        if (!($this->_receiptLib instanceof \App\Libraries\ReceiptLib)) {
            $this->_receiptLib = new \App\Libraries\ReceiptLib();
        }
        return $this->_receiptLib;
    }
    
    /**
     * get  prefer  Lib
     *
     * @return \App\Libraries\InvoiceLib
     */
    public final function getInvoiceLib() {
        if (!($this->_invoiceLib instanceof \App\Libraries\InvoiceLib)) {
            $this->_invoiceLib = new \App\Libraries\InvoiceLib();
        }
        return $this->_invoiceLib;
    }

    public final function getDomesticHolidayLib() {
        if (!($this->_domesticHolidayLib instanceof \App\Libraries\DomesticHolidayLib)) {
            $this->_domesticHolidayLib = new \App\Libraries\DomesticHolidayLib();
        }
        return $this->_domesticHolidayLib;
    }

    public final function getInternationalHolidayLib() {
        if (!($this->_internationalHolidayLib instanceof \App\Libraries\InternationalHolidayLib)) {
            $this->_internationalHolidayLib = new \App\Libraries\InternationalHolidayLib();
        }
        return $this->_internationalHolidayLib;
    }

    /*     * *******************************************************************
     *                                                                   *
     * New FUNCTIONS                                               *
     *                                                                   *
     * ******************************************************************* */

    public final function getFeaturedTblTourLib() {
        if (!($this->_featuredTblTourLib instanceof \App\Libraries\FeaturedTblTourLib)) {
            $this->_featuredTblTourLib = new \App\Libraries\FeaturedTblTourLib();
        }
        return $this->_featuredTblTourLib;
    }

    public final function getHotDealTourLib() {
        if (!($this->_hotDealTourLib instanceof \App\Libraries\HotDealTourLib)) {
            $this->_hotDealTourLib = new \App\Libraries\HotDealTourLib();
        }
        return $this->_hotDealTourLib;
    }

    public final function getNewArrivalTourLib() {
        if (!($this->_newArrivalTourLib instanceof \App\Libraries\NewArrivalTourLib)) {
            $this->_newArrivalTourLib = new \App\Libraries\NewArrivalTourLib();
        }
        return $this->_newArrivalTourLib;
    }

    public final function getFeaturedDestinationTourLib() {
        if (!($this->_featuredDestinationTourLib instanceof \App\Libraries\FeaturedDestinationTourLib)) {
            $this->_featuredDestinationTourLib = new \App\Libraries\FeaturedDestinationTourLib();
        }
        return $this->_featuredDestinationTourLib;
    }

    /*     * *******************************************************************
     *                                                                   *
     * PROTECTED FUNCTIONS                                               *
     *                                                                   *
     * ******************************************************************* */



    

    /*     * *******************************************************************
     *                                                                   *
     * Book Activities                                               *
     *                                                                   *
     * ******************************************************************* */

    public final function getBookActivitiesLib() {
        if (!($this->_bookActivitiesLib instanceof \App\Libraries\BookActivitiesLib)) {
            $this->_bookActivitiesLib = new \App\Libraries\BookActivitiesLib();
        }
        return $this->_bookActivitiesLib;
    }

    public final function getNewsletterLib() {
        if (!($this->_newsletterLib instanceof \App\Libraries\NewsletterLib)) {
            $this->_newsletterLib = new \App\Libraries\NewsletterLib();
        }
        return $this->_newsletterLib;
    }

    

    /*     * *******************************************************************
     *                                                                   *
     * Flight Booking                                                    *
     *                                                                   *
     * ******************************************************************* */

    public final function getFlightReviewInfoLib() {
        if (!($this->_flightReviewInfoLib instanceof \App\Libraries\FlightReviewInfoLib)) {
            $this->_flightReviewInfoLib = new \App\Libraries\FlightReviewInfoLib();
        }
        return $this->_flightReviewInfoLib;
    }

    public final function getFlightPassengersLib() {
        if (!($this->_flightPassengersLib instanceof \App\Libraries\FlightPassengersLib)) {
            $this->_flightPassengersLib = new \App\Libraries\FlightPassengersLib();
        }
        return $this->_flightPassengersLib;
    }

    public final function getFlightBooksLib() {
        if (!($this->_flightBooksLib instanceof \App\Libraries\FlightBooksLib)) {
            $this->_flightBooksLib = new \App\Libraries\FlightBooksLib();
        }
        return $this->_flightBooksLib;
    }

    /**
     * get User lib
     *
     * @return \App\Libraries\WalletLib
     */
    public final function getWalletLib() {
        if (!($this->_walletLib instanceof \App\Libraries\WalletLib)) {
            $this->_walletLib = new \App\Libraries\WalletLib();
        }
        return $this->_walletLib;
    }




    /**
     * initialize some vars
     *
     * @return \App\Http\Controllers\ControllerAbstract
     */
    protected function _init() {
//        $this->setRequestParams($this->getRequest()->input());
//        $this->setPaginationParams(
//                \array_intersect_key(
//                        $this->getRequestParams(), self::$paginationParams
//                )
//        );
//        $this->setRouteParams($this->getRequest()->route()->parameters());
        return $this;
    }

    /*     * *******************************************************************
     *                                                                   *
     * PRIVATE FUNCTIONS                                                 *
     *                                                                   *
     * ******************************************************************* */
}
