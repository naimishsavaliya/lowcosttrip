<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Session;

class NewsletterController extends ControllerAbstract {

    public function index(Request $request) {
		$rules = [
		   'newsletter_email' => 'required|string|email|max:255',
		];
		$messages = [

		   'newsletter_email.required'      => 'Please enter an email'
		];
		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()) {
		   return redirect()->back()->withErrors($validator)->withInput();
		} else {
			$records = array(
				"email"   => $request->newsletter_email
			);
			$this->getNewsletterLib()->addNewsletter($records);
			return redirect()->route('newsletter-suc');
		}
    }

    public function newsletter_suc() {
		$data = array();
		Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
		});
		Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
		});
		Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
		});
		Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
			return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
		});

		Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
	    });
		$data['domestic_sectors'] = Cache::pull('domestic_sectors');
		$data['global_sectors'] = Cache::pull('global_sectors');
		$data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
		$data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
		$data['CruiseSections'] = Cache::pull('CruiseSections');
		$cities_arr = array();
		$data['cities'] = json_encode($cities_arr);
		return view('client/newsletter-suc', $data);
    }

}
