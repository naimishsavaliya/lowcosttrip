<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Illuminate\Support\Facades\Cache;

class ContactController extends ControllerAbstract {

    public function index() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        return view('client/contact/contact', $data);
    }

}
