<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use RobinCSamuel\LaravelMsg91\Facades\LaravelMsg91;
use App\Helpers\TransactionRequest;
use App\Wallet;
use Mail;
use File;
use Session;

class PaymentController extends ControllerAbstract {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        return view('client/payment/payment-page', $data);
    }

    public function user_form() {
        \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');

        $data['wallet_user_data'] = '';
        if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
             $data['wallet_user_data'] = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
        }
        return view('client/payment/user-form', $data);
    }

    public function form_submit(Request $request) {
        $validator = \Validator::make($request->all(), [
            'customer_name'     => 'required',
            'mobile_number'     => 'required|numeric',
            'email'             => 'required|email',
            'tour_code'         => 'required',
            'amount'            => 'required|numeric',
            'gross_amount'      => 'required|numeric',
            'remarks'           => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->route('payment')
                            ->withErrors($validator)
                            ->withInput();
        }
        // Create the user
        if((isset($request->is_wallet) && $request->is_wallet=='on') && (isset($request->wallet_amount) && $request->wallet_amount!='0')){
            \Session::put(['is_status'=>1,'is_wallet' => $request->is_wallet,'wallet_amount'=>$request->wallet_amount]);
            if($request->gross_amount<=$request->wallet_amount){
                $records=array('customer_name'  =>$request->customer_name,
                            'mobile_number'     =>$request->mobile_number,
                            'email'             =>$request->email,
                            'tour_code'         =>$request->tour_code,
                            'amount'            =>$request->amount,
                            'gross_amount'      =>$request->gross_amount,
                            'refered_by'        =>$request->refered_by,
                            'remarks'           =>$request->remarks,
                            'wallet_amount'     =>round($request->gross_amount),
                            'is_wallet'         =>1
                    );
            }else{
                $records=array('customer_name'  =>$request->customer_name,
                            'mobile_number'     =>$request->mobile_number,
                            'email'             =>$request->email,
                            'tour_code'         =>$request->tour_code,
                            'amount'            =>$request->amount,
                            'gross_amount'      =>$request->gross_amount,
                            'refered_by'        =>$request->refered_by,
                            'remarks'           =>$request->remarks,
                            'wallet_amount'     =>$request->wallet_amount,
                            'is_wallet'         =>1
                );
            }
        }else{
            $records=array('customer_name'  =>$request->customer_name,
                        'mobile_number'     =>$request->mobile_number,
                        'email'             =>$request->email,
                        'tour_code'         =>$request->tour_code,
                        'amount'            =>$request->amount,
                        'gross_amount'      =>$request->gross_amount,
                        'refered_by'        =>$request->refered_by,
                        'remarks'           =>$request->remarks
                );
        }
        if(isset($request->is_wallet) && $request->is_wallet=='on'){
            if($request->gross_amount<=$request->wallet_amount){
                if ($this->getPaymentLib()->addPayment($records)) {
                    $pay_amt = round((((($request->amount) * env('ONLINE_PAYMENT_CHARGE', 1.6)) / 100 ) + $request->amount));
                    $cashback_amt = $this->cashback($pay_amt,1);
                    \Session::put(['cashback_amt'=>round($cashback_amt)]);
                    date_default_timezone_set('Asia/Calcutta');
                    $datenow = date("Y-m-d H:m:s");
                    $transactionId = mt_rand(100000,999999).mt_rand(100000,999999);
                    $merchant_transactionId = rand(1, 1000000);
                    $payment_info = array(
                        "customer_id"       => isset(\Auth::user()->id)?\Auth::user()->id:'',
                        "status"            => 'Ok',
                        "paid_amount"       => (($pay_amt) ? $pay_amt : ''),
                        "paid_date"         => $datenow,
                        "transaction_id"    => (($transactionId) ? $transactionId : ''),
                        "merchant_transaction_id"    => (($merchant_transactionId) ? $merchant_transactionId : ''),
                        "user_name"         => (($request->customer_name) ? $request->customer_name : ''),
                        "user_email"        => (($request->email) ? $request->email : ''),
                        "user_mobile_no"    => (($request->mobile_number) ? $request->mobile_number : ''),
                        "user_address"      => 'India',
                        "wallet_amount"     => (($pay_amt) ? $pay_amt : ''),
                        "is_wallet"         => ($request->session()->has('is_wallet'))?1:0,
                        "is_payment_type"  => 2
                    );
                    if ($payment = $this->getPaymentLib()->addPaymentResponse($payment_info)) {
                        if ($payment_info['status'] == 'Ok') {
                            if($request->session()->has('is_status')){
                                if($request->session()->get('is_status')==1){
                                    if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
                                        $wallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
                                        if(isset($wallet) && $wallet!=''){
                                            $current_balance=$wallet->current_balance-round($pay_amt);
                                            $wallet_data = Wallet::create([
                                                'user_id'     => \Auth::user()->id,
                                                'debit'      => round($pay_amt),
                                                'current_balance'=>$current_balance
                                            ]);
                                        }
                                    }
                                }
                            }
                            if($request->session()->has('cashback_amt')){
                                if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
                                    $lastRecordwallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
                                    if(isset($lastRecordwallet) && $lastRecordwallet!=''){
                                        $lastrecord_current_balance=$lastRecordwallet->current_balance+$request->session()->get('cashback_amt');
                                        $lastrecord_wallet_data = Wallet::create([
                                            'user_id'           => \Auth::user()->id,
                                            'credit'            => $request->session()->get('cashback_amt'),
                                            'current_balance'   =>$lastrecord_current_balance
                                        ]);
                                    }else{
                                        $addnew_record_wallet_data = Wallet::create([
                                            'user_id'     => \Auth::user()->id,
                                            'credit'      => round($request->session()->get('cashback_amt')),
                                            'current_balance'=>round($request->session()->get('cashback_amt'))
                                        ]);
                                    }
                                }
                            }
                            \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
                            \Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format($pay_amt, 2) . ' ) has been paid successfully.');
                            \Session::flash('alert-class', 'alert-success');
                            return redirect()->route('user-payment-response', $transactionId);
                        } else {
                            \Session::flash('message', 'Error..! Your payment (Rs. ' . number_format($pay_amt, 2) . ' ) has been paid unsuccessfully.');
                            \Session::flash('alert-class', 'alert-danger');
                            return redirect()->route('user-form');
                        }
                    } else {
                        \Session::flash('message', 'Error in payment.');
                        \Session::flash('alert-class', 'alert-danger');
                        return redirect()->route('user-form');
                    }
                }else{
                    \Session::flash('message', 'Unable to create payment.');
                    \Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('user-form');
                }
            }else{  
                // dd('Min wallet value Max gross_amount');
                if ($payment = $this->getPaymentLib()->addPayment($records)) {
                    $pay_amt = round((((($request->amount) * env('ONLINE_PAYMENT_CHARGE', 1.6)) / 100 ) + $request->amount));
                    // if(isset($request->is_wallet) && $request->is_wallet=='on'){
                    //     $pay_amt-=$request->wallet_amount;
                    // }

                    // $cashback_amt = $this->cashback($pay_amt,1);

                    if(isset($request->is_wallet) && $request->is_wallet=='on'){
                        $pay_amt-=$request->wallet_amount;
                        $cashback_amt = $this->cashback(round($pay_amt+$request->wallet_amount),1);
                    }else{
                        $cashback_amt = $this->cashback($pay_amt,1);
                    }

                    \Session::put(['cashback_amt'=>round($cashback_amt)]);

                    date_default_timezone_set('Asia/Calcutta');
                    $datenow = date("d/m/Y H:m:s");
                    $transactionDate = str_replace(" ", "%20", $datenow);

                    $transactionId = rand(1, 1000000);
                    $transactionRequest = new TransactionRequest();
                    $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
                    $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
                    $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
                    $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
                    $transactionRequest->setAmount(round($pay_amt));
                    $transactionRequest->setTransactionCurrency("INR");
                    $transactionRequest->setTransactionAmount(round($pay_amt));
                    $transactionRequest->setReturnUrl(route('user-response'));
                    $transactionRequest->setClientCode(123);
                    $transactionRequest->setTransactionId($transactionId);
                    $transactionRequest->setTransactionDate($transactionDate);
                    $transactionRequest->setCustomerName($request->customer_name);
                    $transactionRequest->setCustomerEmailId($request->email);
                    $transactionRequest->setCustomerMobile($request->mobile_number);
                    $transactionRequest->setLeadId($payment);
                    $transactionRequest->setQuotId(0);
                    $transactionRequest->setCustomerBillingAddress("India");
                    $transactionRequest->setCustomerAccount("639827");
                    $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));

                    $url = $transactionRequest->getPGUrl();
                    return redirect($url);
                    header("Location: $url");
                } else {
                    \Session::flash('message', 'Unable to create payment.');
                    \Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('user-form');
                }
            }
        }else{
            //dd('Not use wallet');
            if ($payment = $this->getPaymentLib()->addPayment($records)) {
                $pay_amt = round((((($request->amount) * env('ONLINE_PAYMENT_CHARGE', 1.6)) / 100 ) + $request->amount));
                $cashback_amt = $this->cashback($pay_amt,1);
                \Session::put(['cashback_amt'=>round($cashback_amt)]);
                date_default_timezone_set('Asia/Calcutta');
                $datenow = date("d/m/Y H:m:s");
                $transactionDate = str_replace(" ", "%20", $datenow);
                $transactionId = rand(1, 1000000);
                $transactionRequest = new TransactionRequest();
                $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
                $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
                $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
                $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
                $transactionRequest->setAmount(round($pay_amt));
                $transactionRequest->setTransactionCurrency("INR");
                $transactionRequest->setTransactionAmount(round($pay_amt));
                $transactionRequest->setReturnUrl(route('user-response'));
                $transactionRequest->setClientCode(123);
                $transactionRequest->setTransactionId($transactionId);
                $transactionRequest->setTransactionDate($transactionDate);
                $transactionRequest->setCustomerName($request->customer_name);
                $transactionRequest->setCustomerEmailId($request->email);
                $transactionRequest->setCustomerMobile($request->mobile_number);
                $transactionRequest->setLeadId($payment);
                $transactionRequest->setQuotId(0);
                $transactionRequest->setCustomerBillingAddress("India");
                $transactionRequest->setCustomerAccount("639827");
                $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
                $url = $transactionRequest->getPGUrl();
                return redirect($url);
                header("Location: $url");
            } else {
                \Session::flash('message', 'Unable to create payment.');
                \Session::flash('alert-class', 'alert-danger');
                return redirect()->route('user-form');
            }
        }
    }

    public function user_response(Request $request) {
        if ($request) {
            $payment_info = array(
                //"customer_id" => (($request->udf5) ? $request->udf5 : ''),
                "customer_id" => isset(\Auth::user()->id)?\Auth::user()->id:'',
                "status" => (($request->f_code) ? $request->f_code : ''),
                "paid_amount" => (($request->amt) ? $request->amt : ''),
                "paid_date" => (($request->date) ? date('Y-m-d H:m:s', strtotime($request->date)) : ''),
                "transaction_id" => (($request->mmp_txn) ? $request->mmp_txn : ''),
                "merchant_transaction_id" => (($request->mer_txn) ? $request->mer_txn : ''),
                "bank_transaction_id" => (($request->bank_txn) ? $request->bank_txn : ''),
                "bank_name" => (($request->bank_name) ? $request->bank_name : ''),
                "auth_code" => (($request->auth_code) ? $request->auth_code : ''),
                "description" => (($request->desc) ? $request->desc : ''),
                "user_name" => (($request->udf1) ? $request->udf1 : ''),
                "user_email" => (($request->udf2) ? $request->udf2 : ''),
                "user_mobile_no" => (($request->udf3) ? $request->udf3 : ''),
                "user_address" => (($request->udf4) ? $request->udf4 : ''),
                "all_response" => response()->json($request),
                "wallet_amount" => ($request->session()->has('wallet_amount'))?$request->session()->get('wallet_amount'):'',
                "is_wallet" => ($request->session()->has('is_wallet'))?1:0,
                "is_payment_type"  => ($request->session()->has('is_wallet'))?3:1
            );
        }
        if ($payment = $this->getPaymentLib()->addPaymentResponse($payment_info)) {
            if ($payment_info['status'] == 'Ok') {
                if($request->session()->has('is_status')){
                    if($request->session()->get('is_status')==1){
                        if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
                            $wallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
                            if(isset($wallet) && $wallet!=''){
                                $current_balance=$wallet->current_balance-$request->session()->get('wallet_amount');
                                $wallet_data = Wallet::create([
                                    'user_id'     => \Auth::user()->id,
                                    'debit'      => $request->session()->get('wallet_amount'),
                                    'current_balance'=>$current_balance
                                ]);
                            }
                        }
                    }
                }
                if($request->session()->has('cashback_amt')){
                    if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
                        $lastRecordwallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
                        if(isset($lastRecordwallet) && $lastRecordwallet!=''){
                            $lastrecord_current_balance=$lastRecordwallet->current_balance+$request->session()->get('cashback_amt');
                            $lastrecord_wallet_data = Wallet::create([
                                'user_id'     => \Auth::user()->id,
                                'credit'      => $request->session()->get('cashback_amt'),
                                'current_balance'=>$lastrecord_current_balance
                            ]);
                        }else{
                            $addnew_record_wallet_data = Wallet::create([
                                'user_id'     => \Auth::user()->id,
                                'credit'      => round($request->session()->get('cashback_amt')),
                                'current_balance'=>round($request->session()->get('cashback_amt'))
                            ]);
                        }
                    }
                }
                \Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format(($request->session()->has('is_wallet'))?$request->amt+$request->session()->get('wallet_amount'):$request->amt, 2) . ' ) has been paid successfully.');
                \Session::flash('alert-class', 'alert-success');
                \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
            } else {
                \Session::flash('message', 'Error..! Your payment (Rs. ' . number_format(($request->session()->has('is_wallet'))?$request->amt+$request->session()->get('wallet_amount'):$request->amt, 2) . ' ) has been paid unsuccessfully.');
                \Session::flash('alert-class', 'alert-danger');
                \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
            }
        } else {
            \Session::flash('message', 'Error in payment.');
            \Session::flash('alert-class', 'alert-danger');
            \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
        }
        return redirect()->route('user-payment-response', $request->mmp_txn);
    }

    public function user_payment_response($tra_id) {
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });

        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });

        $transaction_data = $this->getPaymentLib()->getPaymentLeadNo($tra_id);
        //dd($transaction_data);
        if ($transaction_data) {
            $data['payment_info'] = $transaction_data;
            $data['domestic_sectors'] = Cache::pull('domestic_sectors');
            $data['global_sectors'] = Cache::pull('global_sectors');
            $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
            $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
            $data['CruiseSections'] = Cache::pull('CruiseSections');
            return view('client/payment/user-payment-response-info', $data);
        } else {
            return redirect()->route('payment');
        }
    }

    /*

    public function quotation_payment(Request $request){
        $validator = \Validator::make($request->all(), [
            'customer_name' => 'required',
            'mobile_number' => 'required',
            'email'         => 'required|email',
            'quote_number'  => 'required',
            'amount'        => 'required',
            'gross_amount'  => 'required',
            'remarks'       => 'required'
        ]);
        $userData  = $request->all();
        if ($validator->fails()) {
            return redirect()->route('quotation.make.payment', [$userData['lead_id'], $userData['quote_id']])
                            ->withErrors($validator)
                            ->withInput();
        }
        if((isset($request->is_wallet) && $request->is_wallet=='on') && (isset($request->wallet_amount) && $request->wallet_amount!='0')){
            \Session::put(['is_status'=>1,'is_wallet' => $request->is_wallet,'wallet_amount'=>$request->wallet_amount]);
            $profileData = array(
                 'name'          => $userData['customer_name'],
                 'mobile_number' => $userData['mobile_number'],
                 'email_id'      => $userData['email'],
                 'amount'        => $userData['amount'],
                 'gross_amount'  => $userData['gross_amount'],
                 'remarks'       => $userData['remarks'],
                 'refered_by'    => ($userData['refered_by'] ? $userData['refered_by'] :''),
                 'lead_id'       => $userData['lead_id'],
                 'quote_id'      => $userData['quote_id'],
                 'wallet_amount'     =>$request->wallet_amount,
                 'is_wallet'         =>1
             );
        }else{
            $profileData = array(
                 'name'          => $userData['customer_name'],
                 'mobile_number' => $userData['mobile_number'],
                 'email_id'      => $userData['email'],
                 'amount'        => $userData['amount'],
                 'gross_amount'  => $userData['gross_amount'],
                 'remarks'       => $userData['remarks'],
                 'refered_by'    => ($userData['refered_by'] ? $userData['refered_by'] :''),
                 'lead_id'       => $userData['lead_id'],
                 'quote_id'      => $userData['quote_id']
             );
        }
        if ($payment =  $this->getQuotationPaymentLib()->addPayment($profileData) ) {
            $lead_info = $this->getLeadLib()->getLeadById($userData['lead_id']);
            $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($userData['lead_id'], $userData['quote_id']);
            if ($lead_info && $quotation) {
                $amount = $userData['gross_amount'];
                if(isset($request->is_wallet) && $request->is_wallet=='on'){
                    $amount-=$request->wallet_amount;
                }
                $cashback_amt = $this->cashback($amount,1);
                \Session::put(['cashback_amt'=>round($cashback_amt)]);

                date_default_timezone_set('Asia/Calcutta');
                $datenow = date("d/m/Y h:m:s");
                $transactionDate = str_replace(" ", "%20", $datenow);

                $transactionId = rand(1, 1000000);
                $transactionRequest = new TransactionRequest();
                $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
                $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
                $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
                $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
                $transactionRequest->setAmount(round($amount));
                $transactionRequest->setTransactionCurrency("INR");
                $transactionRequest->setTransactionAmount(round($amount));
                $transactionRequest->setReturnUrl(route('atom_response'));
                $transactionRequest->setClientCode(123);
                $transactionRequest->setTransactionId($transactionId);
                $transactionRequest->setTransactionDate($transactionDate);
                $transactionRequest->setCustomerName($userData['customer_name']);
                $transactionRequest->setCustomerEmailId($userData['email']);
                $transactionRequest->setCustomerMobile($userData['mobile_number']);
                $transactionRequest->setCustomerBillingAddress("India");
                $transactionRequest->setLeadId($lead_info->lead_id);
                $transactionRequest->setQuotId($userData['quote_id']);
                $transactionRequest->setCustomerAccount("639827");
                $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
                $url = $transactionRequest->getPGUrl();
//                echo $url;die;
                return redirect($url);
//                header("Location: $url");
            } else {
                \Session::flash('message', 'Invalid Booking ID');
                \Session::flash('alert-class', 'alert-danger');
                return redirect()->route('payment');
            }
        } else {
            \Session::flash('message', 'Unable to create payment.');
            \Session::flash('alert-class', 'alert-danger');
            return redirect()->route('user-form');
        }
    }

    */

    public function quotation_payment(Request $request){
        $validator = \Validator::make($request->all(), [
          'customer_name' => 'required',
          'mobile_number' => 'required',
          'email'         => 'required|email',
          'quote_number'  => 'required',
          'amount'        => 'required',
          'gross_amount'  => 'required',
          'remarks'       => 'required'
        ]);
        $userData  = $request->all();
        if ($validator->fails()) {
            return redirect()->route('quotation.make.payment', [$userData['lead_id'], $userData['quote_id']])
                          ->withErrors($validator)
                          ->withInput();
        }
        if((isset($request->is_wallet) && $request->is_wallet=='on') && (isset($request->wallet_amount) && $request->wallet_amount!='0')){
            \Session::put(['is_status'=>1,'is_wallet' => $request->is_wallet,'wallet_amount'=>$request->wallet_amount]);
            if($request->gross_amount<=$request->wallet_amount){
                $profileData = array(
                   'name'          => $userData['customer_name'],
                   'mobile_number' => $userData['mobile_number'],
                   'email_id'      => $userData['email'],
                   'amount'        => $userData['amount'],
                   'gross_amount'  => $userData['gross_amount'],
                   'remarks'       => $userData['remarks'],
                   'refered_by'    => ($userData['refered_by'] ? $userData['refered_by'] :''),
                   'lead_id'       => $userData['lead_id'],
                   'quote_id'      => $userData['quote_id'],
                   'wallet_amount' => round($userData['gross_amount']),
                   'is_wallet'     =>1
                );
            }else{
                $profileData = array(
                   'name'          => $userData['customer_name'],
                   'mobile_number' => $userData['mobile_number'],
                   'email_id'      => $userData['email'],
                   'amount'        => $userData['amount'],
                   'gross_amount'  => $userData['gross_amount'],
                   'remarks'       => $userData['remarks'],
                   'refered_by'    => ($userData['refered_by'] ? $userData['refered_by'] :''),
                   'lead_id'       => $userData['lead_id'],
                   'quote_id'      => $userData['quote_id'],
                   'wallet_amount' =>$request->wallet_amount,
                   'is_wallet'     =>1
                );
            }
        }else{
            $profileData = array(
               'name'          => $userData['customer_name'],
               'mobile_number' => $userData['mobile_number'],
               'email_id'      => $userData['email'],
               'amount'        => $userData['amount'],
               'gross_amount'  => $userData['gross_amount'],
               'remarks'       => $userData['remarks'],
               'refered_by'    => ($userData['refered_by'] ? $userData['refered_by'] :''),
               'lead_id'       => $userData['lead_id'],
               'quote_id'      => $userData['quote_id']
            );
        }
        // Use wallet
        if(isset($request->is_wallet) && $request->is_wallet=='on'){
            if($request->gross_amount<=$request->wallet_amount){
                if ($payment =  $this->getQuotationPaymentLib()->addPayment($profileData) ) {
                    $lead_info = $this->getLeadLib()->getLeadById($userData['lead_id']);
                    $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($userData['lead_id'], $userData['quote_id']);
                    if ($lead_info && $quotation) {
                        $amount = round($userData['gross_amount']);
                        $cashback_amt = $this->cashback($amount,1);
                        \Session::put(['cashback_amt'=>round($cashback_amt)]);
                        date_default_timezone_set('Asia/Calcutta');
                        $datenow = date("Y-m-d H:m:s");
                        //$transactionDate = str_replace(" ", "%20", $datenow);
                        $transactionId = mt_rand(100000,999999).mt_rand(100000,999999);
                        $merchant_transactionId = rand(1, 1000000);
                        $payment_info = array(
                        "customer_id"               => isset(\Auth::user()->id)?\Auth::user()->id:'',
                        "lead_id"                   => (($lead_info->lead_id) ? $lead_info->lead_id : ''),
                        "status"                    => 'Ok',
                        "paid_amount"               => (($amount) ? $amount : ''),
                        "paid_date"                 => $datenow,
                        "transaction_id"            => (($transactionId) ? $transactionId : ''),
                        "merchant_transaction_id"   => (($merchant_transactionId) ? $merchant_transactionId : ''),    
                        "user_name"                 => (($request->customer_name) ? $request->customer_name : ''),
                        "user_email"                => (($request->email) ? $request->email : ''),
                        "user_mobile_no"            => (($request->mobile_number) ? $request->mobile_number : ''),
                        "user_address"              => 'India',
                        "wallet_amount"             => (($amount) ? $amount : ''),
                        "is_wallet"                 => ($request->session()->has('is_wallet'))?1:0,
                        "is_payment_type"           => 2
                        );
                        if ($payment = $this->getPaymentLib()->addPaymentResponse($payment_info)) {
                            if ($payment_info['status'] == 'Ok') {
                                if($request->session()->has('is_status')){
                                    if($request->session()->get('is_status')==1){
                                        if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
                                            $wallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
                                            if(isset($wallet) && $wallet!=''){
                                                $current_balance=$wallet->current_balance-round($amount);
                                                $wallet_data = Wallet::create([
                                                'user_id'           => \Auth::user()->id,
                                                'debit'             => round($amount),
                                                'current_balance'   =>$current_balance
                                                ]);
                                            }
                                        }
                                    }
                                }
                                if($request->session()->has('cashback_amt')){
                                    if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
                                        $lastRecordwallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
                                        if(isset($lastRecordwallet) && $lastRecordwallet!=''){
                                            $lastrecord_current_balance=$lastRecordwallet->current_balance+$request->session()->get('cashback_amt');
                                            $lastrecord_wallet_data = Wallet::create([
                                            'user_id'           => \Auth::user()->id,
                                            'credit'            => $request->session()->get('cashback_amt'),
                                            'current_balance'   =>$lastrecord_current_balance
                                            ]);
                                        }else{
                                            $addnew_record_wallet_data = Wallet::create([
                                                'user_id'     => \Auth::user()->id,
                                                'credit'      => round($request->session()->get('cashback_amt')),
                                                'current_balance'=>round($request->session()->get('cashback_amt'))
                                            ]);
                                        }
                                    }
                                }
                                \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
                                $lead_info = $this->getLeadLib()->getLeadByNuber($lead_info->lead_id);
                                $receipt_no = $this->getReceipLib()->getReceiptNumber();
                                $customer_payment = array(
                                    "lead_id"           => (($lead_info) ? $lead_info->id : ''),
                                    "paid_amount"       => (($amount) ? $amount : ''),
                                    "paid_date"         => $datenow,
                                    "transaction_id"    => (($transactionId) ? $transactionId : ''),
                                    "merchant_transaction_id" => (($merchant_transactionId) ? $merchant_transactionId : ''),
                                    "quotation_id"  => (($userData['quote_id']) ? $userData['quote_id'] : ''),
                                    "payment_mode"  => "Online",
                                    "type"          => "payment",
                                    "customer_id"   =>((isset($lead_info) && $lead_info->customer_id != NULL) ? $lead_info->customer_id : ''),
                                    "tour_id"       =>((isset($lead_info) && $lead_info->tour_id != NULL) ? $lead_info->tour_id : ''),
                                    "receipt_no"    => $receipt_no,
                                );
                                $lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($lead_info->id);
                                $crbalance=null;
                                if(!empty($lastRecord)){
                                    if(isset($lastRecord->balance) && $lastRecord->balance!=null){
                                        $customer_payment['balance']=$lastRecord->balance-$amount;
                                    }
                                }else{
                                    $customer_payment['balance']=$crbalance-$amount;
                                }
                                $this->getCustomerPaymentLib()->addCustomerPayment($customer_payment);
                                $this->getReceipLib()->updateReceiptNumber();
                                \Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format($amount, 2) . ' ) has been paid successfully.');
                                \Session::flash('alert-class', 'alert-success');
                            } else {
                                \Session::flash('message', 'Error..! Your payment (Rs. ' . number_format($amount, 2) . ' ) has been paid unsuccessfully.');
                                \Session::flash('alert-class', 'alert-danger');
                            }
                        }else{
                            \Session::flash('message', 'Error in payment.');
                            \Session::flash('alert-class', 'alert-danger');
                        }
                        return redirect()->route('payment-response', $transactionId);
                    } else {
                        \Session::flash('message', 'Invalid Booking ID');
                        \Session::flash('alert-class', 'alert-danger');
                        return redirect()->route('payment');
                    }
                } else {
                    \Session::flash('message', 'Unable to create payment.');
                    \Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('user-form');
                }
            }else{
                // dd('Min wallet value Max amount');
                if ($payment =  $this->getQuotationPaymentLib()->addPayment($profileData) ) {
                    $lead_info = $this->getLeadLib()->getLeadById($userData['lead_id']);
                    $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($userData['lead_id'], $userData['quote_id']);
                    if ($lead_info && $quotation) {
                        $amount = $userData['gross_amount'];
                        // if(isset($request->is_wallet) && $request->is_wallet=='on'){
                        //     $amount-=$request->wallet_amount;
                        // }
                        // $cashback_amt = $this->cashback($amount,1);

                        if(isset($request->is_wallet) && $request->is_wallet=='on'){
                            $amount-=$request->wallet_amount;
                            $cashback_amt = $this->cashback(round($amount+$request->wallet_amount),1);
                        }else{
                            $cashback_amt = $this->cashback($amount,1);
                        }
                        
                        \Session::put(['cashback_amt'=>round($cashback_amt)]);

                        date_default_timezone_set('Asia/Calcutta');
                        $datenow = date("d/m/Y H:m:s");
                        $transactionDate = str_replace(" ", "%20", $datenow);

                        $transactionId = rand(1, 1000000);
                        $transactionRequest = new TransactionRequest();
                        $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
                        $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
                        $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
                        $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
                        $transactionRequest->setAmount(round($amount));
                        $transactionRequest->setTransactionCurrency("INR");
                        $transactionRequest->setTransactionAmount(round($amount));
                        $transactionRequest->setReturnUrl(route('atom_response'));
                        $transactionRequest->setClientCode(123);
                        $transactionRequest->setTransactionId($transactionId);
                        $transactionRequest->setTransactionDate($transactionDate);
                        $transactionRequest->setCustomerName($userData['customer_name']);
                        $transactionRequest->setCustomerEmailId($userData['email']);
                        $transactionRequest->setCustomerMobile($userData['mobile_number']);
                        $transactionRequest->setCustomerBillingAddress("India");
                        $transactionRequest->setLeadId($lead_info->lead_id);
                        $transactionRequest->setQuotId($userData['quote_id']);
                        $transactionRequest->setCustomerAccount("639827");
                        $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
                        $url = $transactionRequest->getPGUrl();
                        return redirect($url);
                    } else {
                        \Session::flash('message', 'Invalid Booking ID');
                        \Session::flash('alert-class', 'alert-danger');
                        return redirect()->route('payment');
                    }
                } else {
                    \Session::flash('message', 'Unable to create payment.');
                    \Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('user-form');
                }
            }
        }else{
            //dd('Not Use wallet');
            if ($payment    =  $this->getQuotationPaymentLib()->addPayment($profileData) ) {
                $lead_info  = $this->getLeadLib()->getLeadById($userData['lead_id']);
                $quotation  = $this->getLeadQuatationLib()->getLeadQuatationById($userData['lead_id'], $userData['quote_id']);
                if ($lead_info && $quotation) {
                    $amount         = $userData['gross_amount'];
                    $cashback_amt   = $this->cashback($amount,1);
                    \Session::put(['cashback_amt'=>round($cashback_amt)]);
                    date_default_timezone_set('Asia/Calcutta');
                    $datenow            = date("d/m/Y H:m:s");
                    $transactionDate    = str_replace(" ", "%20", $datenow);
                    $transactionId      = rand(1, 1000000);
                    $transactionRequest = new TransactionRequest();
                    $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
                    $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
                    $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
                    $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
                    $transactionRequest->setAmount(round($amount));
                    $transactionRequest->setTransactionCurrency("INR");
                    $transactionRequest->setTransactionAmount(round($amount));
                    $transactionRequest->setReturnUrl(route('atom_response'));
                    $transactionRequest->setClientCode(123);
                    $transactionRequest->setTransactionId($transactionId);
                    $transactionRequest->setTransactionDate($transactionDate);
                    $transactionRequest->setCustomerName($userData['customer_name']);
                    $transactionRequest->setCustomerEmailId($userData['email']);
                    $transactionRequest->setCustomerMobile($userData['mobile_number']);
                    $transactionRequest->setCustomerBillingAddress("India");
                    $transactionRequest->setLeadId($lead_info->lead_id);
                    $transactionRequest->setQuotId($userData['quote_id']);
                    $transactionRequest->setCustomerAccount("639827");
                    $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
                    $url = $transactionRequest->getPGUrl();
                    return redirect($url);
                } else {
                    \Session::flash('message', 'Invalid Booking ID');
                    \Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('payment');
                }
            } else {
                \Session::flash('message', 'Unable to create payment.');
                \Session::flash('alert-class', 'alert-danger');
                return redirect()->route('user-form');
            }
        }
    }

    public function atomResponse(Request $request) {
        if ($request) {
            $payment_info = array(
                "customer_id" => isset(\Auth::user()->id)?\Auth::user()->id:'',
                "lead_id" => (($request->udf5) ? $request->udf5 : ''),
                "status" => (($request->f_code) ? $request->f_code : ''),
                "paid_amount" => (($request->amt) ? $request->amt : ''),
                "paid_date" => (($request->date) ? date('Y-m-d H:m:s', strtotime($request->date)) : ''),
                "transaction_id" => (($request->mmp_txn) ? $request->mmp_txn : ''),
                "merchant_transaction_id" => (($request->mer_txn) ? $request->mer_txn : ''),
                "bank_transaction_id" => (($request->bank_txn) ? $request->bank_txn : ''),
                "bank_name" => (($request->bank_name) ? $request->bank_name : ''),
                "auth_code" => (($request->auth_code) ? $request->auth_code : ''),
                "description" => (($request->desc) ? $request->desc : ''),
                "user_name" => (($request->udf1) ? $request->udf1 : ''),
                "user_email" => (($request->udf2) ? $request->udf2 : ''),
                "user_mobile_no" => (($request->udf3) ? $request->udf3 : ''),
                "user_address" => (($request->udf4) ? $request->udf4 : ''),
                "all_response" => response()->json($request),
                "wallet_amount" => ($request->session()->has('wallet_amount'))?$request->session()->get('wallet_amount'):'',
                "is_wallet" => ($request->session()->has('is_wallet'))?1:0,
                "is_payment_type"  => ($request->session()->has('is_wallet'))?3:1
            );
        }
        if ($payment = $this->getPaymentLib()->addPaymentResponse($payment_info)) {

            if ($payment_info['status'] == 'Ok') {
                if($request->session()->has('is_status')){
                    if($request->session()->get('is_status')==1){
                        if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
                            $wallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
                            if(isset($wallet) && $wallet!=''){
                                $current_balance=$wallet->current_balance-$request->session()->get('wallet_amount');
                                $wallet_data = Wallet::create([
                                    'user_id'     => \Auth::user()->id,
                                    'debit'      => $request->session()->get('wallet_amount'),
                                    'current_balance'=>$current_balance
                                ]);
                            }
                        }
                    }
                }
                if($request->session()->has('cashback_amt')){
                    if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
                        $lastRecordwallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
                        if(isset($lastRecordwallet) && $lastRecordwallet!=''){
                            $lastrecord_current_balance=$lastRecordwallet->current_balance+$request->session()->get('cashback_amt');
                            $lastrecord_wallet_data = Wallet::create([
                                'user_id'     => \Auth::user()->id,
                                'credit'      => $request->session()->get('cashback_amt'),
                                'current_balance'=>$lastrecord_current_balance
                            ]);
                        }else{
                            $addnew_record_wallet_data = Wallet::create([
                                'user_id'     => \Auth::user()->id,
                                'credit'      => round($request->session()->get('cashback_amt')),
                                'current_balance'=>round($request->session()->get('cashback_amt'))
                            ]);
                        }
                    }
                }
                $lead_info = $this->getLeadLib()->getLeadByNuber($request->udf5);
                $receipt_no = $this->getReceipLib()->getReceiptNumber();
                $customer_payment = array(
                    "lead_id" => (($lead_info) ? $lead_info->id : ''),
                    "paid_amount" => (($request->amt) ? $request->amt : ''),
                    "paid_date" => (($request->date) ? date('Y-m-d H:m:s', strtotime($request->date)) : ''),
                    "transaction_id" => (($request->mmp_txn) ? $request->mmp_txn : ''),
                    "merchant_transaction_id" => (($request->mer_txn) ? $request->mer_txn : ''),
                    "bank_transaction_id" => (($request->bank_txn) ? $request->bank_txn : ''),
                    "bank_name" => (($request->bank_name) ? $request->bank_name : ''),
                    "auth_code" => (($request->auth_code) ? $request->auth_code : ''),
                    "quotation_id" => (($request->udf9) ? $request->udf9 : ''),
                    "payment_mode" => "Online",
                    "type"         => "payment",
                    "customer_id"  =>((isset($lead_info) && $lead_info->customer_id != NULL) ? $lead_info->customer_id : ''),
                    "tour_id"  =>((isset($lead_info) && $lead_info->tour_id != NULL) ? $lead_info->tour_id : ''),
                    "receipt_no" => $receipt_no,
                );

                //$lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord();
                // if(!empty($lastRecord)){
                //  if(isset($lastRecord->balance) && $lastRecord->balance!=null){
                //      $records['balance']=$lastRecord->balance-$request->discount_amount;
                //  }
                // }

                $lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($lead_info->id);
                $crbalance=null;
                if(!empty($lastRecord)){
                    if(isset($lastRecord->balance) && $lastRecord->balance!=null){
                        $customer_payment['balance']=$lastRecord->balance-$request->amt;
                    }
                }else{
                    $customer_payment['balance']=$crbalance-$request->amt;
                }

                $this->getCustomerPaymentLib()->addCustomerPayment($customer_payment);
                $this->getReceipLib()->updateReceiptNumber();


                \Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format(($request->session()->has('is_wallet'))?$request->amt+$request->session()->get('wallet_amount'):$request->amt, 2) . ' ) has been paid successfully.');
                \Session::flash('alert-class', 'alert-success');
                \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
            } else {
                \Session::flash('message', 'Error..! Your payment (Rs. ' . number_format(($request->session()->has('is_wallet'))?$request->amt+$request->session()->get('wallet_amount'):$request->amt, 2) . ' ) has been paid unsuccessfully.');
                \Session::flash('alert-class', 'alert-danger');
                \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
            }
        } else {
            \Session::flash('message', 'Error in payment.');
            \Session::flash('alert-class', 'alert-danger');
            \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
        }
        return redirect()->route('payment-response', $request->mmp_txn);
    }

    public function payment_response($tra_id) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $transaction_data = $this->getPaymentLib()->getPaymentLeadNo($tra_id);
        if ($transaction_data) {
            $paymentData = array();
            $quotation = array();
            $particular = array();
            $itinarary = array();
            if(isset($transaction_data->transaction_id) && $transaction_data->transaction_id!=''){
                $paymentData = $this->getCustomerPaymentLib()->getQuotationId($transaction_data->transaction_id);
                if(!empty($paymentData)){
                    if(isset($paymentData->lead_id) && isset($paymentData->quotation_id)){
                        $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($paymentData->lead_id, $paymentData->quotation_id);
                        $particular = $this->getLeadParticularLib()->getLeadParticularById($paymentData->lead_id, $paymentData->quotation_id);
                        $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($paymentData->lead_id, $paymentData->quotation_id);
                    }  
                }
            }
            $data['payment_data'] = $paymentData;
            $data['transaction_data'] = $transaction_data;
            $data['quotation'] = $quotation;
            $data['particular'] = $particular;
            $data['itinarary'] = $itinarary;
            $lead_number_info = $this->getLeadLib()->getLeadByNuber($transaction_data->lead_id);
            $data['lead_info'] = $lead_info = $this->getLeadLib()->getLeadInfo($lead_number_info->id);
            if ($data['lead_info']) {
                $data['domestic_sectors'] = Cache::pull('domestic_sectors');
                $data['global_sectors'] = Cache::pull('global_sectors');
                $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
                $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
                $data['CruiseSections'] = Cache::pull('CruiseSections');

                $contents = view('client/payment/payment-receipt-template',['payment_data'=>$paymentData,'transaction_data'=>$transaction_data,'quotation'=>$quotation,'particular'=>$particular,'itinarary'=>$itinarary,'lead_info'=>$lead_info])->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($contents);
                $file_name = isset($lead_info->name) ? $lead_info->name . time() . '.pdf' : time() . '.pdf';
                $path = public_path() . '/pdf';
                $file = public_path() . '/pdf/' . $file_name;
                // if (!file_exists($path)) {
                //     File::makeDirectory($path, 0777, true, true);
                // }
                // $path = public_path().'/images';
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                $pdf->save($file);

                $maildata = array('email' => $lead_info->email, 'file' => $file);
                Mail::send([], $maildata, function($message)  use ($maildata )  {
                     $message->to($maildata['email'], 'Low Cost Trip')->subject
                        ('Low Cost Trip');
                     $message->cc(env('CC_MAIL', 'info@lowcosttrip.com'));
                     $message->attach($maildata['file']);
                     $message->from('info@lowcosttrip.com','Low Cost Trip');
                });
                return view('client/payment/payment-response-info', $data);
            } else {
                return redirect()->route('payment');
            }
        } else {
            return redirect()->route('payment');
        }
    }

    public function check_booking_id(Request $request) {
        $lead_info = $this->getLeadLib()->getLeadByNuber($request->booking_id, array('id', 'lead_id', 'email', 'phone'));
        if ($lead_info && trim($request->booking_id) != '') {
            $otp = rand(55555, 99999);
            session(['book_id_otp' => $otp]);
            session(['booking_id' => $request->booking_id]);
            $return['status'] = true;
            $return['message'] = 'OTP Sent in your mobile number';
            $return['otp'] = $otp;
            $rs = LaravelMsg91::message($lead_info->phone, "Thank your interest in low cost trip. Your payment OTP is {$otp}.");
        } else {
            $return['status'] = false;
            $return['message'] = 'Invalid booking ID';
        }
        return response()->json($return);
    }

    public function check_otp(Request $request) {
        $session = (string) session()->get('book_id_otp');
        $otp = (string) $request->otp;
        if (trim($request->otp) != '' && $otp === $session) {
            $return['status'] = true;
        } else {
            $return['status'] = false;
            $return['message'] = 'Invalid OTP';
        }
        return response()->json($return);
    }

    public function payment_info($leadId) {
        $booking_id = (string) session()->get('booking_id');
        if ($booking_id == $leadId) {

            Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
                return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
            });
            Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
                return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
            });
            Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
                return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
            });
            Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
                return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
            });

            Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
                        return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
                    });

            $data['lead_info'] = $this->getLeadLib()->getLeadByNuber($leadId);
            if ($data['lead_info']) {
                $data['domestic_sectors'] = Cache::pull('domestic_sectors');
                $data['global_sectors'] = Cache::pull('global_sectors');
                $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
                $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
                $data['CruiseSections'] = Cache::pull('CruiseSections');
                return view('client/payment/payment-info', $data);
            } else {
                return redirect()->route('payment');
            }
        } else {
            return redirect()->route('payment');
        }
    }

    public function sendToAtom(Request $request) {
        $request->session()->forget('book_id_otp');
        $request->session()->forget('booking_id');
        session(['lead_id' => $request->booking_id]);
        if ($request->lead_id != '' && $request->id > 0) {
            $lead_info = $this->getLeadLib()->getLeadByNuber($request->lead_id);
            if ($lead_info) {
                date_default_timezone_set('Asia/Calcutta');
                $datenow = date("d/m/Y H:m:s");
                $transactionDate = str_replace(" ", "%20", $datenow);

                $transactionId = rand(1, 1000000);
                $transactionRequest = new TransactionRequest();
                $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
                $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
                $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
                $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
                $transactionRequest->setAmount(5000);
                $transactionRequest->setTransactionCurrency("INR");
                $transactionRequest->setTransactionAmount(5000);
                $transactionRequest->setReturnUrl(route('atom_response'));
                $transactionRequest->setClientCode(123);
                $transactionRequest->setTransactionId($transactionId);
                $transactionRequest->setTransactionDate($transactionDate);
                $transactionRequest->setCustomerName($lead_info->name);
                $transactionRequest->setCustomerEmailId($lead_info->email);
                $transactionRequest->setCustomerMobile($lead_info->phone);
                $transactionRequest->setLeadId($lead_info->lead_id);
                $transactionRequest->setCustomerBillingAddress("India");
                $transactionRequest->setCustomerAccount("639827");
                $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
                $url = $transactionRequest->getPGUrl();
                // dd( $url );
                return redirect($url);
                header("Location: $url");
            } else {
                \Session::flash('message', 'Invalid Booking ID');
                \Session::flash('alert-class', 'alert-danger');
                return redirect()->route('payment');
            }
        } else {
            \Session::flash('message', 'Invalid Booking ID');
            \Session::flash('alert-class', 'alert-danger');
            return redirect()->route('payment');
        }
    }

    public function pay_now($lead_id, $quot_id) {
        if ($lead_id != '' && $lead_id > 0 && $quot_id != '' && $quot_id > 0) {
            $lead_info = $this->getLeadLib()->getLeadById($lead_id);
            $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quot_id);
            if ($lead_info && $quotation) {
                $amount = $quotation->total_amount;
                date_default_timezone_set('Asia/Calcutta');
                $datenow = date("d/m/Y H:m:s");
                $transactionDate = str_replace(" ", "%20", $datenow);

                $transactionId = rand(1, 1000000);
                $transactionRequest = new TransactionRequest();
                $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
                $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
                $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
                $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
                $transactionRequest->setAmount($amount);
                $transactionRequest->setTransactionCurrency("INR");
                $transactionRequest->setTransactionAmount($amount);
                $transactionRequest->setReturnUrl(route('atom_response'));
                $transactionRequest->setClientCode(123);
                $transactionRequest->setTransactionId($transactionId);
                $transactionRequest->setTransactionDate($transactionDate);
                $transactionRequest->setCustomerName($lead_info->name);
                $transactionRequest->setCustomerEmailId($lead_info->email);
                $transactionRequest->setCustomerMobile($lead_info->phone);
                $transactionRequest->setCustomerBillingAddress("India");
                $transactionRequest->setLeadId($lead_info->lead_id);
                $transactionRequest->setQuotId($quot_id);
                $transactionRequest->setCustomerAccount("639827");
                $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
                $url = $transactionRequest->getPGUrl();
//                echo $url;die;
                return redirect($url);
//                header("Location: $url");
            } else {
                \Session::flash('message', 'Invalid Booking ID');
                \Session::flash('alert-class', 'alert-danger');
                return redirect()->route('payment');
            }
        } else {
            \Session::flash('message', 'Invalid Booking ID');
            \Session::flash('alert-class', 'alert-danger');
            return redirect()->route('payment');
        }
    }

    public function cashback($pay_amt,$percentage){
        $cashback_amt = ($percentage / 100) * $pay_amt;
        return $cashback_amt;
    }
    
    /*

    public function form_submit(Request $request) {
        //dd(isset(\Auth::user()->id)?\Auth::user()->id:null);
        $validator = \Validator::make($request->all(), [
                    'customer_name' => 'required',
                    'mobile_number' => 'required|numeric',
                    'email' => 'required|email',
                    'tour_code' => 'required',
                    'amount' => 'required|numeric',
                    'gross_amount' => 'required|numeric',
                    'remarks' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->route('payment')
                            ->withErrors($validator)
                            ->withInput();
        }
        // Create the user
        if((isset($request->is_wallet) && $request->is_wallet=='on') && (isset($request->wallet_amount) && $request->wallet_amount!='0')){
            \Session::put(['is_status'=>1,'is_wallet' => $request->is_wallet,'wallet_amount'=>$request->wallet_amount]);
            $records=array('customer_name'  =>$request->customer_name,
                        'mobile_number'     =>$request->mobile_number,
                        'email'             =>$request->email,
                        'tour_code'         =>$request->tour_code,
                        'amount'            =>$request->amount,
                        'gross_amount'      =>$request->gross_amount,
                        'refered_by'        =>$request->refered_by,
                        'remarks'           =>$request->remarks,
                        'wallet_amount'     =>$request->wallet_amount,
                        'is_wallet'         =>1
                );
        }else{
            $records=array('customer_name'  =>$request->customer_name,
                        'mobile_number'     =>$request->mobile_number,
                        'email'             =>$request->email,
                        'tour_code'         =>$request->tour_code,
                        'amount'            =>$request->amount,
                        'gross_amount'      =>$request->gross_amount,
                        'refered_by'        =>$request->refered_by,
                        'remarks'           =>$request->remarks
                );
        }
        //dd(99<=100);
        // if(isset($request->is_wallet) && $request->is_wallet=='on'){
            
        // }else{

        // }
        if ($payment = $this->getPaymentLib()->addPayment($records)) {
            $pay_amt = round((((($request->amount) * env('ONLINE_PAYMENT_CHARGE', 1.6)) / 100 ) + $request->amount));
            if(isset($request->is_wallet) && $request->is_wallet=='on'){
                $pay_amt-=$request->wallet_amount;
            }

            $cashback_amt = $this->cashback($pay_amt,1);

            \Session::put(['cashback_amt'=>round($cashback_amt)]);

            date_default_timezone_set('Asia/Calcutta');
            $datenow = date("d/m/Y h:m:s");
            $transactionDate = str_replace(" ", "%20", $datenow);

            $transactionId = rand(1, 1000000);
            $transactionRequest = new TransactionRequest();
            $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
            $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
            $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
            $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
            $transactionRequest->setAmount(round($pay_amt));
            $transactionRequest->setTransactionCurrency("INR");
            $transactionRequest->setTransactionAmount(round($pay_amt));
            $transactionRequest->setReturnUrl(route('user-response'));
            $transactionRequest->setClientCode(123);
            $transactionRequest->setTransactionId($transactionId);
            $transactionRequest->setTransactionDate($transactionDate);
            $transactionRequest->setCustomerName($request->customer_name);
            $transactionRequest->setCustomerEmailId($request->email);
            $transactionRequest->setCustomerMobile($request->mobile_number);
            $transactionRequest->setLeadId($payment);
            $transactionRequest->setQuotId(0);
            $transactionRequest->setCustomerBillingAddress("India");
            $transactionRequest->setCustomerAccount("639827");
            $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));

            $url = $transactionRequest->getPGUrl();
            return redirect($url);
            header("Location: $url");
        } else {
            \Session::flash('message', 'Unable to create payment.');
            \Session::flash('alert-class', 'alert-danger');
            return redirect()->route('user-form');
        }
    }

    */

    /*
    public function form_submit(Request $request) {
        // $records = array(
        //     'is_status' => $request->session()->get('is_status'),
        //     'is_wallet'=>$request->session()->get('is_wallet'),
        //     'wallet_amount'=>$request->session()->get('wallet_amount'),
        //     'cashback_amt'=>$request->session()->get('cashback_amt')
        // );
        // dd($records);
        //dd($request->session()->get('is_status'));
        // \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
        $validator = \Validator::make($request->all(), [
                    'customer_name' => 'required',
                    'mobile_number' => 'required|numeric',
                    'email' => 'required|email',
                    'tour_code' => 'required',
                    'amount' => 'required|numeric',
                    'gross_amount' => 'required|numeric',
                    'remarks' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->route('payment')
                            ->withErrors($validator)
                            ->withInput();
        }
        // Create the user
        if((isset($request->is_wallet) && $request->is_wallet=='on') && (isset($request->wallet_amount) && $request->wallet_amount!='0')){
            //session(['is_status'=>1,'is_wallet' => $request->is_wallet,'wallet_amount'=>$request->wallet_amount]);
            \Session::put(['is_status'=>1,'is_wallet' => $request->is_wallet,'wallet_amount'=>$request->wallet_amount]);
            $records=array('customer_name'  =>$request->customer_name,
                        'mobile_number'     =>$request->mobile_number,
                        'email'             =>$request->email,
                        'tour_code'         =>$request->tour_code,
                        'amount'            =>$request->amount,
                        'gross_amount'      =>$request->gross_amount,
                        'refered_by'        =>$request->refered_by,
                        'remarks'           =>$request->remarks,
                        'wallet_amount'     =>$request->wallet_amount,
                        'is_wallet'         =>1
                );
        }else{
            $records=array('customer_name'  =>$request->customer_name,
                        'mobile_number'     =>$request->mobile_number,
                        'email'             =>$request->email,
                        'tour_code'         =>$request->tour_code,
                        'amount'            =>$request->amount,
                        'gross_amount'      =>$request->gross_amount,
                        'refered_by'        =>$request->refered_by,
                        'remarks'           =>$request->remarks
                );
        }

        //if ($payment = $this->getPaymentLib()->addPayment($request->except('_token'))) {
        if ($payment = $this->getPaymentLib()->addPayment($records)) {
            $pay_amt = round((((($request->amount) * env('ONLINE_PAYMENT_CHARGE', 1.6)) / 100 ) + $request->amount));
            //dd(isset($request->is_wallet) && $request->is_wallet=='on');
            if(isset($request->is_wallet) && $request->is_wallet=='on'){
                $pay_amt-=$request->wallet_amount;
            }

            $cashback_amt = $this->cashback($pay_amt,1);

            \Session::put(['cashback_amt'=>round($cashback_amt)]);

            date_default_timezone_set('Asia/Calcutta');
            $datenow = date("d/m/Y h:m:s");
            $transactionDate = str_replace(" ", "%20", $datenow);

            $transactionId = rand(1, 1000000);
            $transactionRequest = new TransactionRequest();
            $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
            $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
            $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
            $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
            $transactionRequest->setAmount(round($pay_amt));
            $transactionRequest->setTransactionCurrency("INR");
            $transactionRequest->setTransactionAmount(round($pay_amt));
            $transactionRequest->setReturnUrl(route('user-response'));
            $transactionRequest->setClientCode(123);
            $transactionRequest->setTransactionId($transactionId);
            $transactionRequest->setTransactionDate($transactionDate);
            $transactionRequest->setCustomerName($request->customer_name);
            $transactionRequest->setCustomerEmailId($request->email);
            $transactionRequest->setCustomerMobile($request->mobile_number);
            $transactionRequest->setLeadId($payment);
            $transactionRequest->setQuotId(0);
            $transactionRequest->setCustomerBillingAddress("India");
            $transactionRequest->setCustomerAccount("639827");
            $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));

            $url = $transactionRequest->getPGUrl();
            return redirect($url);
            header("Location: $url");
        } else {
            \Session::flash('message', 'Unable to create payment.');
            \Session::flash('alert-class', 'alert-danger');
            return redirect()->route('user-form');
        }
    }

    */

    /*
    public function quotation_payment(Request $request){
        $validator = \Validator::make($request->all(), [
            'customer_name' => 'required',
            'mobile_number' => 'required',
            'email'         => 'required|email',
            'quote_number'  => 'required',
            'amount'        => 'required',
            'gross_amount'  => 'required',
            'remarks'       => 'required'
        ]);
        $userData  = $request->all();
        if ($validator->fails()) {
            return redirect()->route('quotation.make.payment', [$userData['lead_id'], $userData['quote_id']])
                            ->withErrors($validator)
                            ->withInput();
        }
        if((isset($request->is_wallet) && $request->is_wallet=='on') && (isset($request->wallet_amount) && $request->wallet_amount!='0')){
            \Session::put(['is_status'=>1,'is_wallet' => $request->is_wallet,'wallet_amount'=>$request->wallet_amount]);
            $profileData = array(
                 'name'          => $userData['customer_name'],
                 'mobile_number' => $userData['mobile_number'],
                 'email_id'      => $userData['email'],
                 'amount'        => $userData['amount'],
                 'gross_amount'  => $userData['gross_amount'],
                 'remarks'       => $userData['remarks'],
                 'refered_by'    => ($userData['refered_by'] ? $userData['refered_by'] :''),
                 'lead_id'       => $userData['lead_id'],
                 'quote_id'      => $userData['quote_id'],
                 'wallet_amount'     =>$request->wallet_amount,
                 'is_wallet'         =>1
             );
        }else{
            $profileData = array(
                 'name'          => $userData['customer_name'],
                 'mobile_number' => $userData['mobile_number'],
                 'email_id'      => $userData['email'],
                 'amount'        => $userData['amount'],
                 'gross_amount'  => $userData['gross_amount'],
                 'remarks'       => $userData['remarks'],
                 'refered_by'    => ($userData['refered_by'] ? $userData['refered_by'] :''),
                 'lead_id'       => $userData['lead_id'],
                 'quote_id'      => $userData['quote_id']
             );
        }
        if ($payment =  $this->getQuotationPaymentLib()->addPayment($profileData) ) {
            $lead_info = $this->getLeadLib()->getLeadById($userData['lead_id']);
            $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($userData['lead_id'], $userData['quote_id']);
            if ($lead_info && $quotation) {
                $amount = $userData['gross_amount'];
                if(isset($request->is_wallet) && $request->is_wallet=='on'){
                    $amount-=$request->wallet_amount;
                }
                $cashback_amt = $this->cashback($amount,1);
                \Session::put(['cashback_amt'=>round($cashback_amt)]);

                date_default_timezone_set('Asia/Calcutta');
                $datenow = date("d/m/Y h:m:s");
                $transactionDate = str_replace(" ", "%20", $datenow);

                $transactionId = rand(1, 1000000);
                $transactionRequest = new TransactionRequest();
                $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
                $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
                $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
                $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
                $transactionRequest->setAmount(round($amount));
                $transactionRequest->setTransactionCurrency("INR");
                $transactionRequest->setTransactionAmount(round($amount));
                $transactionRequest->setReturnUrl(route('atom_response'));
                $transactionRequest->setClientCode(123);
                $transactionRequest->setTransactionId($transactionId);
                $transactionRequest->setTransactionDate($transactionDate);
                $transactionRequest->setCustomerName($userData['customer_name']);
                $transactionRequest->setCustomerEmailId($userData['email']);
                $transactionRequest->setCustomerMobile($userData['mobile_number']);
                $transactionRequest->setCustomerBillingAddress("India");
                $transactionRequest->setLeadId($lead_info->lead_id);
                $transactionRequest->setQuotId($userData['quote_id']);
                $transactionRequest->setCustomerAccount("639827");
                $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
                $url = $transactionRequest->getPGUrl();
//                echo $url;die;
                return redirect($url);
//                header("Location: $url");
            } else {
                \Session::flash('message', 'Invalid Booking ID');
                \Session::flash('alert-class', 'alert-danger');
                return redirect()->route('payment');
            }
        } else {
            \Session::flash('message', 'Unable to create payment.');
            \Session::flash('alert-class', 'alert-danger');
            return redirect()->route('user-form');
        }
    }
    */

}
