<?php

namespace App\Http\Controllers\client;

use Mail;
use App\User;
use App\Userprofile;
use App\Wallet;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\ControllerAbstract;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Intervention\Image\Facades\Image;

use Socialite;
use DB;
use Session;

class LoginController extends ControllerAbstract {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = 'admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function index() {
      $data = array();
      Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
      });
      Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
      });
      Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
      });
      Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
          return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
      });
      Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
      });
      $data['domestic_sectors'] = Cache::pull('domestic_sectors');
      $data['global_sectors'] = Cache::pull('global_sectors');
      $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
      $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
      $data['CruiseSections'] = Cache::pull('CruiseSections');
      $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name', 'sortname', 'phonecode'));
      $data['states']   = $this->getStateLib()->getStateByCountry(101, array('state_id', 'state_name'));
      $data['cities'] = $this->getCityLib()->getCityByState(22, array('city_id', 'city_name'));
      return view('auth.login', $data);
    }

    public function login(Request $request) {
      $userdata = array(
        'email'     => $request->email,
        'password'  => $request->password,
        'is_agent'  => 0
      );
      if($res = Auth::attempt($userdata)){
        if((Auth::user()->status=="1")){
          if((Auth::user()->is_new_password=="1")){ //When user generated while add lead 
            $f_token = Auth::user()->forgot_token;
            \Auth::logout();
            return redirect()->route('reset_password',[$f_token]);
          }else{
            if((Auth::user()->role->name=="Customer")){
              return redirect()->route('user_dashboard');
            } else{
              \Auth::logout();
              $request->session()->flash('log_error', 'Login id or password incorrect.');
              return redirect()->route('client_login_form');
            }
          }
        }else{
            \Auth::logout();
            $request->session()->flash('log_error', 'Email not Activated ');
            return redirect()->route('client_login_form');
        }
      }else{
        $request->session()->flash('log_error', 'Login id or password incorrect.');
        return redirect()->route('client_login_form');
      }
    }

    public function getStateByCountry($country_id) {
        $states = $this->getStateLib()->getStateByCountry($country_id, array('state_id', 'state_name'));
        exit(json_encode($states));
    }

    public function getCityByState($state_id) {
        $cities = $this->getCityLib()->getCityByState($state_id, array('city_id', 'city_name'));
        exit(json_encode($cities));
    }

    public function duplicate_email(Request $request){
        $email = $request->email;
        $is_agent = $request->is_agent;
        
        $result = $this->getUserLib()->checkuserdata(['users.email'=>$email, 'is_agent' => $is_agent]);
        if($result>0){
          echo json_encode(FALSE);
        }else{
          echo json_encode(TRUE);
        }
    }

    public function duplicate_phone_no(Request $request){
        $phone_no = $request->phone_no;
        $is_agent = $request->is_agent;
        $result = $this->getUserLib()->checkuserdata(['users_profile.phone_no'=>$phone_no, 'is_agent' => $is_agent]);
        if($result>0){
          echo json_encode(FALSE);
        }else{
          echo json_encode(TRUE);
        }
    }

    public function forgot_password() {
      $data = array();
      Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
      });
      Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
      });
      Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
      });
      Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
          return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
      });
      Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
      });
      $data['domestic_sectors'] = Cache::pull('domestic_sectors');
      $data['global_sectors'] = Cache::pull('global_sectors');
      $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
      $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
      $data['CruiseSections'] = Cache::pull('CruiseSections');
      $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
      return view('client/forgot-password', $data);
    }

    public function forgot_password_request(Request $request) {
      $userdata = $this->getUserLib()->GetUserRecord(['email'=>$request->email,'role_id'=>5,'is_agent' => 0]);
      if($userdata){
        $forgot_token       = str_random(10);
        $forgot_token_date  =  date("Y-m-d H:i:s");
        $data['forgot_token']       = $forgot_token;
        $data['forgot_token_date']       = $forgot_token_date;
        $this->getUserLib()->updateUser($data, $userdata->id);
        Mail::send('client/forgot-password-template', ['forgot_token'=>$forgot_token], function($message) use ($request) {
           $message->to($request->email, 'Low Cost Trip')->subject
              ('Forgot Password');
           $message->from('info@lowcosttrip.com','Low Cost Trip');
        });
        if (Mail::failures()) {
          $request->session()->flash('forgot_error', 'Could not send email, please try again later.');
          return redirect()->route('forgot_password');
        }
        $request->session()->flash('message', 'Send email.');
        return redirect()->route('client_login_form');
      }else{
        $request->session()->flash('forgot_error', 'Please enter correct email.');
        return redirect()->route('forgot_password');
      }
    }

    public function reset_password($forgot_token) {
      $data = array();
      Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
      });
      Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
      });
      Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
          return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
      });
      Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
          return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
      });
      Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
      });
      $data['domestic_sectors'] = Cache::pull('domestic_sectors');
      $data['global_sectors'] = Cache::pull('global_sectors');
      $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
      $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
      $data['CruiseSections'] = Cache::pull('CruiseSections');
      $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
      $data['userdata'] = $this->getUserLib()->verify_token(['forgot_token'=>$forgot_token,'role_id'=>5,'is_agent' => 0]);
      $data['forgot_token'] = $forgot_token;
      return view('client/reset-password', $data);
    }

    public function reset_password_request(Request $request) {
      if($request->user_id){
        $data['password']       = Hash::make($request->password);
        $data['is_new_password']= 0;
        $this->getUserLib()->updateUser($data, $request->user_id);
        $request->session()->flash('message', 'Reset password successfully.');
        return redirect()->route('client_login_form');
      }else{
        $request->session()->flash('reset_error', 'Forgot token does not match.');
        return redirect()->route('reset_password',$request->forgot_token);
      }
    }

    public function activation_user($activation_token,Request $request) {
      $userdata = $this->getUserLib()->verify_token(['activation_token'=>$activation_token]);
      if($userdata){
        $data['status']       = 1;
        $this->getUserLib()->updateUser($data, $userdata->id);
        $request->session()->flash('message', 'Your account is activated. You can login below.');
      }else{
        $request->session()->flash('log_error', 'Activation token does not match.');
      }
      if(isset($userdata) && isset($userdata->is_agent) && $userdata->is_agent == 1){
        return redirect()->route('agent_login_form');
      }else{
        return redirect()->route('client_login_form');
      }
    }

    public function callback($provider_name){
        $provider  = Socialite::driver($provider_name)->user();
        $user_type =  session('user_type');

        if($provider->email != NULL){
          $result = User::where('provider_id', $provider->id)->where('is_agent', $user_type)->orwhere("email", $provider->email)->where('deleted_at', NULL)->first();
        }else{
          $result = User::where('provider_id', $provider->id)->where('is_agent', $user_type)->where('deleted_at', NULL)->first();
        }
        if($result){
          $user_id = $result['id'];
          $user = array(
              'first_name'      => $provider->name,
              'email'           => (($provider->email != NULL) ? $provider->email  : ''),
              'provider_id'     => $provider->id,
              'role_id'         => (($user_type == 1)? 4 : 5),
              'status'          => '1',
              'provider'        => $provider_name,
              'is_agent'        => $user_type, 
          );
          // $imagename = $userId . '_user.jpg';
          $path = str_replace("?sz=50","",$provider->avatar);
          $filename = basename($path);
          $image_name = $user_id . '_user.jpg';
          Image::make($path)->save(public_path('/uploads/user_profile/' . $image_name));

          $users_profile = array(
              'user_id'     => $user_id,
              'image'       => $image_name
          );
          $this->getUserLib()->updateUser($user, $user_id);
          $this->getUserprofileLib()->updateUserprofile($users_profile, $user_id);

          $user = User::where('provider_id', $provider->id)->first();
        }else{
          $user = User::create([
              'first_name'      => $provider->name,
              'email'           => (($provider->email != NULL) ? $provider->email  : ''),
              'provider_id'     => $provider->id,
              'role_id'         => (($user_type == 1)? 4 : 5),
              'status'          => 1,
              'provider'        => $provider_name,
              'is_agent'        => $user_type,
          ]);

          $path = str_replace("?sz=50","",$provider->avatar);
          $filename = basename($path);
          $image_name =  $user->id . '_user.jpg';
          Image::make($path)->save(public_path('/uploads/user_profile/' . $image_name));

          $users_profile = array(
              'user_id'     => $user->id,
              'image'       => $image_name
          );

          $wallet = Wallet::create([
              'user_id'     => $user->id,
              'credit'      => 100,
              'current_balance'=>100
          ]);
          
          $this->getUserprofileLib()->addUserprofile($users_profile);
        }
        Auth::login($user, true);
        Session::forget('user_type');
        return redirect()->route('user_dashboard');
    }
 
    public function redirect($provider, $type){
        session(['user_type' => $type]);
        return Socialite::with($provider)->redirect();
    }
 

}
