<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Illuminate\Support\Facades\Cache;
use DB;

class HolidayController extends ControllerAbstract {

    public function index() {
        $data = array();
		$cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
		$cities_arr = array();
		foreach ($cities as $key => $value) {
			$cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
		}
		$data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    public function domestic_holidays() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $breadcrumbs = array(
            'Home' => route('index'),
            'Indian Holidays' => route('indian-holiday')
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = 'Indian Holidays';
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.domestic_international' => 1,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1];
        // if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
        //     $where_arr['tour.tour_type'] = $_GET['theme_type'];
        // }
        // if(isset($_GET['duration']) && $_GET['duration']!=''){
        //     $where_arr['tour.tour_nights'] = $_GET['duration'];
        // }
        // if(isset($_GET['hotel']) && $_GET['hotel']!=''){
        //     $where_arr['tour.hotel'] = $_GET['hotel'];
        // }
        // if(isset($_GET['budget']) && $_GET['budget']!=''){
        //     $where_arr['tour.budget'] = $_GET['budget'];
        // }
        // if(isset($_GET['short_by']) && $_GET['short_by']!=''){
        //     $where_arr['tour.short_by'] = $_GET['short_by'];
        // }
        // $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);
        $holidays = $this->getTourLib()->get_mega_menu_tour_data(array('tour.*'), $where_arr);
        if(count($holidays)>0){
            foreach ($holidays as $key => $holiday) {
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
                if (!empty($cost)) {
                    $holidays[$key]['cost'] = $cost;
                }
                if (!empty($hotel)) {
                    $holidays[$key]['hotel'] = $hotel;
                }
            }
            $data['holidays'] = $holidays;
        }else{
            $data['holidays'] = array();
        }
        //dd($data);
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    public function global_holidays() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $breadcrumbs = array(
            'Home' => route('index'),
            'Global Holidays' => route('global-holiday'),
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = 'Global Holidays';
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.domestic_international' => 2,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1];
        // if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
        //     $where_arr['tour.tour_type'] = $_GET['theme_type'];
        // }
        // if(isset($_GET['duration']) && $_GET['duration']!=''){
        //     $where_arr['tour.tour_nights'] = $_GET['duration'];
        // }
        // if(isset($_GET['hotel']) && $_GET['hotel']!=''){
        //     $where_arr['tour.hotel'] = $_GET['hotel'];
        // }
        // if(isset($_GET['budget']) && $_GET['budget']!=''){
        //     $where_arr['tour.budget'] = $_GET['budget'];
        // }
        // if(isset($_GET['short_by']) && $_GET['short_by']!=''){
        //     $where_arr['tour.short_by'] = $_GET['short_by'];
        // }
        // $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);
        $holidays = $this->getTourLib()->get_mega_menu_tour_data(array('tour.*'), $where_arr);
        if(count($holidays)>0){
            foreach ($holidays as $key => $holiday) {
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
                if (!empty($cost)) {
                    $holidays[$key]['cost'] = $cost;
                }
                if (!empty($hotel)) {
                    $holidays[$key]['hotel'] = $hotel;
                }
            }
            $data['holidays'] = $holidays;
        }else{
            $data['holidays'] = array();
        }
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    public function sector_holidays($sectorId, $sectorName) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $sector = $this->getSectorLib()->getSectorById($sectorId);
        $breadcrumbs = array(
            'Home' => route('index'),
            'Indian Holidays' => route('indian-holiday'),
            $sector->name => route('indian-holiday.sector',[$sector->id,str_slug($sector->name,'-')])
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = $sector->name;
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.domestic_international' => 1,'tour.sector'=>$sectorId,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1];
        // if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
        //     $where_arr['tour.tour_type'] = $_GET['theme_type'];
        // }
        // if(isset($_GET['duration']) && $_GET['duration']!=''){
        //     $where_arr['tour.tour_nights'] = $_GET['duration'];
        // }
        // if(isset($_GET['hotel']) && $_GET['hotel']!=''){
        //     $where_arr['tour.hotel'] = $_GET['hotel'];
        // }
        // if(isset($_GET['budget']) && $_GET['budget']!=''){
        //     $where_arr['tour.budget'] = $_GET['budget'];
        // }
        // if(isset($_GET['short_by']) && $_GET['short_by']!=''){
        //     $where_arr['tour.short_by'] = $_GET['short_by'];
        // }
        // $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);
        $holidays = $this->getTourLib()->get_mega_menu_tour_data(array('tour.*'), $where_arr);
        if(count($holidays)>0){
            foreach ($holidays as $key => $holiday) {
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
                if (!empty($cost)) {
                    $holidays[$key]['cost'] = $cost;
                }
                if (!empty($hotel)) {
                    $holidays[$key]['hotel'] = $hotel;
                }
            }
            $data['holidays'] = $holidays;
        }else{
            $data['holidays'] = array();
        }
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    public function global_sector_holidays($sectorId, $sectorName) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $sector = $this->getSectorLib()->getSectorById($sectorId);
        $breadcrumbs = array(
            'Home' => route('index'),
            'Global Holidays' => route('global-holiday'),
            $sector->name => route('global-holiday.sector',[$sector->id,str_slug($sector->name,'-')])
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = $sector->name;
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.domestic_international' => 2,'tour.sector'=>$sectorId,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1];
        // if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
        //     $where_arr['tour.tour_type'] = $_GET['theme_type'];
        // }
        // if(isset($_GET['duration']) && $_GET['duration']!=''){
        //     $where_arr['tour.tour_nights'] = $_GET['duration'];
        // }
        // if(isset($_GET['hotel']) && $_GET['hotel']!=''){
        //     $where_arr['tour.hotel'] = $_GET['hotel'];
        // }
        // if(isset($_GET['budget']) && $_GET['budget']!=''){
        //     $where_arr['tour.budget'] = $_GET['budget'];
        // }
        // if(isset($_GET['short_by']) && $_GET['short_by']!=''){
        //     $where_arr['tour.short_by'] = $_GET['short_by'];
        // }
        // $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);
        $holidays = $this->getTourLib()->get_mega_menu_tour_data(array('tour.*'), $where_arr);

        if(count($holidays)>0){
            foreach ($holidays as $key => $holiday) {
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
                if (!empty($cost)) {
                    $holidays[$key]['cost'] = $cost;
                }
                if (!empty($hotel)) {
                    $holidays[$key]['hotel'] = $hotel;
                }
            }
            $data['holidays'] = $holidays;
        }else{
            $data['holidays'] = array();
        }
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    public function specialty_tours() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $breadcrumbs = array(
            'Home' => route('index'),
            'Speciality Tours' => route('speciality-tours'),
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = 'Speciality Tours';
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1];
        // if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
        //     $where_arr['tour.tour_type'] = $_GET['theme_type'];
        // }
        // if(isset($_GET['duration']) && $_GET['duration']!=''){
        //     $where_arr['tour.tour_nights'] = $_GET['duration'];
        // }
        // if(isset($_GET['hotel']) && $_GET['hotel']!=''){
        //     $where_arr['tour.hotel'] = $_GET['hotel'];
        // }
        // if(isset($_GET['budget']) && $_GET['budget']!=''){
        //     $where_arr['tour.budget'] = $_GET['budget'];
        // }
        // if(isset($_GET['short_by']) && $_GET['short_by']!=''){
        //     $where_arr['tour.short_by'] = $_GET['short_by'];
        // }
        // $holidays = $this->getTourLib()->getToursDetailsForSpecialtyTours(array('tour.*'), $where_arr);
        $holidays = $this->getTourLib()->getSpecialtyToursData(array('tour.*'), $where_arr);
        if(count($holidays)>0){
            foreach ($holidays as $key => $holiday) {
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
                if (!empty($cost)) {
                    $holidays[$key]['cost'] = $cost;
                }
                if (!empty($hotel)) {
                    $holidays[$key]['hotel'] = $hotel;
                }
            }
            $data['holidays'] = $holidays;
        }else{
            $data['holidays'] = array();
        }
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    public function specialty_tours_category($typeId, $CategoryName) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $tour_category = $this->getTourtypemasterLib()->getTourtypeById($typeId);
        $breadcrumbs = array(
            'Home' => route('index'),
            'Speciality Tours' => route('speciality-tours'),
            $tour_category->name => route('speciality-tours.category',[$tour_category->typeId,str_slug($tour_category->name,'-')])
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = $tour_category->name;
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        //$where_arr = ['tour.tour_type'=>$typeId,'tour.tour_status'=>1];
        // if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
        //     $where_arr['tour.tour_type'] = $_GET['theme_type'];
        // }
        // if(isset($_GET['duration']) && $_GET['duration']!=''){
        //     $where_arr['tour.tour_nights'] = $_GET['duration'];
        // }
        // if(isset($_GET['hotel']) && $_GET['hotel']!=''){
        //     $where_arr['tour.hotel'] = $_GET['hotel'];
        // }
        // if(isset($_GET['budget']) && $_GET['budget']!=''){
        //     $where_arr['tour.budget'] = $_GET['budget'];
        // }
        // if(isset($_GET['short_by']) && $_GET['short_by']!=''){
        //     $where_arr['tour.short_by'] = $_GET['short_by'];
        // }
        // $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);
        $holidays = $this->getTourLib()->categrowiseSpecialtyTours($typeId);
        if(count($holidays)>0){
            foreach ($holidays as $key => $holiday) {
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
                if (!empty($cost)) {
                    $holidays[$key]['cost'] = $cost;
                }
                if (!empty($hotel)) {
                    $holidays[$key]['hotel'] = $hotel;
                }
            }
            $data['holidays'] = $holidays;
        }else{
            $data['holidays'] = array();
        }
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    public function cruise_tours() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $breadcrumbs = array(
            'Home' => route('index'),
            'Cruise Tours' => route('cruise-tours')
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = 'Cruise Tours';
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $holidays = $this->getTourLib()->getCruiseHolidayslist(12);
        if(count($holidays)>0){
            foreach ($holidays as $key => $holiday) {
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
                if (!empty($cost)) {
                    $holidays[$key]['cost'] = $cost;
                }
                if (!empty($hotel)) {
                    $holidays[$key]['hotel'] = $hotel;
                }
            }
            $data['holidays'] = $holidays;
        }else{
            $data['holidays'] = array();
        }
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    public function holiday_detail($tourId, $tourName) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        // $tour = $this->getTourLib()->getTourById($tourId);
        $tour = $this->getTourLib()->get_tour_data($tourId);
        $tour_image = \DB::table('tour_images')
                    ->where('tour_id', $tourId)
                    ->get();
        $tour_itinerary = $this->getTourItineraryLib()->getTourInerary(array('tour_itinerary.id', 'tour_itinerary.tour_id', 'tour_itinerary.tour_day', 'tour_itinerary.title', 'tour_itinerary.description', 'tour_itinerary.image'), array('tour_itinerary.tour_id' => $tour->id));
        $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $tour->id));
        $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $tour->id));
        if (!empty($tour_itinerary)) {
            $data['tour_itinerary'] = $tour_itinerary;
        }
        if (!empty($tour_image)) {
            $data['tour_image'] = $tour_image;
        }
        if (!empty($cost)) {
            $data['cost'] = $cost;
        }
        if (!empty($hotel)) {
            $data['hotel'] = $hotel;
        }
        $breadcrumbs = array(
            'Home' => route('index'),
            $tour->tour_name => route('holiday-detail',[$tour->id,str_slug($tour->tour_name,'-')])
        );
        if(isset($tour->domestic_international)){
            if($tour->domestic_international==1){
                $breadcrumbs = array(
                    'Home' => route('index'),
                    'Indian Holidays'=>route('indian-holiday'),
                    $tour->tour_name => route('holiday-detail',[$tour->id,str_slug($tour->tour_name,'-')])
                );
            }else if ($tour->domestic_international==2) {
                $breadcrumbs = array(
                    'Home' => route('index'),
                    'Global Holidays'=>route('global-holiday'),
                    $tour->tour_name => route('holiday-detail',[$tour->id,str_slug($tour->tour_name,'-')])
                );
            }
        }
        $data['breadcrumbs']    = $breadcrumbs;
        $data['title']          = $tour->tour_name;
        $data['description']    = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';
        $data['tour'] = $tour;
        // dd($cost);
        return view('client/holiday/detail', $data);
    }

    public function holiday_package_detail(Request $request){
        $tour_id = $request->tour_id;
        $package_id = $request->package_id;
        $hotels = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $tour_id,'tour_hotel.package_type'=>$package_id));
        $data = [];
        foreach ($hotels as $key => $hotel) {
            $data[] = \View::make('client/holiday/package_carousel_view', ['h'=>$key, 'tour_hotel'=>$hotel])->render();
        }
        return response()->json($data);
    }

    public function add_to_compare(Request $request){
        $tourId = $request->tour_id;
        $package_id = $request->package_id;
        $tour_data = array('tour_id' => $tourId,'package_id'=>$package_id);
        $arr = [];
        //return $request->session()->forget('compare_tours');
        if($request->session()->has('compare_tours')){
            $arr = $request->session()->get('compare_tours');
            $tour_ids = array_column($arr,'tour_id');
            if(in_array($tourId, $tour_ids)){
                $arr_key = array_search($tourId, $tour_ids);
                if($arr[$arr_key]['package_id']==$package_id){
                    return response()->json(['status'=>false,'message'=>'Already added this tour.','count'=>count($arr)]);
                }
                else{
                    $arr[$arr_key]['package_id'] = $package_id;
                    session(['compare_tours' => $arr]);
                    return response()->json(['status'=>true,'message'=>'Tour package successfully updated.','count'=>count($arr)]);
                }
            }
            else{
                if(count($arr)>=3){
                    return response()->json(['status'=>false,'message'=>'Already added 3 tours.','count'=>count($arr)]);
                }
                else{
                    $arr[] = $tour_data;
                    session(['compare_tours' => $arr]);
                    return response()->json(['status'=>true,'message'=>'Tour successfully added to compare.','count'=>count($arr)]);
                }
            }
        }
        else{
            $arr[] = $tour_data;
            session(['compare_tours' => $arr]);
            return response()->json(['status'=>true,'message'=>'tour successfully added to compare.','count'=>count($arr)]);
        }
    }

    public function empty_tour(){
        session()->forget('compare_tours');
        return redirect()->route('index');
    }

    public function compare_tour() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $compare_data = [];
        $compare_tours=session()->get('compare_tours');
        if($compare_tours){
            foreach ($compare_tours as $key => $compare) {
                $tourId = $compare['tour_id'];
                $package_id = $compare['package_id'];
                $tour = $this->getTourLib()->get_tour_data($tourId);
                //dd($tour);
                $tour_itinerary = $this->getTourItineraryLib()->getTourInerary(array('tour_itinerary.id', 'tour_itinerary.tour_id', 'tour_itinerary.tour_day', 'tour_itinerary.title', 'tour_itinerary.description', 'tour_itinerary.image'), array('tour_itinerary.tour_id' => $tourId));
                $hotels = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $tourId,'tour_hotel.package_type'=>$package_id));
                $cost = $this->getTourCostAmountLib()->get_totalCost(['tour_cost_amount.tour_id'=>$tourId,'tour_cost_amount.rate_id'=>2,'tour_cost_amount.package_type_id'=>$package_id]);
                $package = $this->getTourpackagetypemasterLib()->getTourpackagetypemasterById($package_id);
                $compare_data[]=['tour'=>$tour,'tour_itinerary'=>$tour_itinerary,'hotels'=>$hotels,'cost'=>$cost,'package'=>$package]; 
            }
        }
        //dd($compare_data);
        $data['compare_data'] = $compare_data;
        return view('client/holiday/compare', $data);
    }
    
    public function tour_filter(Request $request){
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $breadcrumbs = array(
            'Home' => route('index')
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = 'Holidays';
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $where_arr = ['tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1];
        if(isset($request->tour_location) && $request->tour_location!=''){
            $where_arr['raw'] = '(tour.tour_name LIKE "%'.$request->tour_location.'%" OR tour.ending_point LIKE "%'.$request->tour_location.'%" OR tour_itinerary.title LIKE "%'.$request->tour_location.'%" OR tour_itinerary.description LIKE "%'.$request->tour_location.'%")';
        }
        if(isset($request->tour_month) && $request->tour_month!=''){
            if(isset($where_arr['raw'])){
                $where_arr['raw'] .= ' AND DATE_FORMAT(tour_departure_date.departure_date,"%m") = "'.$request->tour_month.'"';
            }
            else{
                $where_arr['raw'] = 'DATE_FORMAT(tour_departure_date.departure_date,"%m") = "'.$request->tour_month.'"';
            }
        }
        if(isset($request->tour_year) && $request->tour_year!=''){
            if(isset($where_arr['raw'])){
                $where_arr['raw'] .= ' AND DATE_FORMAT(tour_departure_date.departure_date,"%Y") = "'.$request->tour_year.'"';
            }
            else{
                $where_arr['raw'] = 'DATE_FORMAT(tour_departure_date.departure_date,"%Y") = "'.$request->tour_year.'"';
            }
        }
        if(isset($request->search) && $request->search!=''){
            if(isset($where_arr['raw'])) {
                $where_arr['raw'] .= ' AND DATE_FORMAT(tour_departure_date.departure_date,"%Y") = "'.$request->tour_year.'"';
            } else {
                $where_arr['raw'] = ' tour.tour_name LIKE "%'.$request->search.'%" OR tour.keywords LIKE "%'.$request->search.'%"  OR tour.short_description LIKE "%'.$request->search.'%"   OR tour.long_description LIKE "%'.$request->search.'%"  OR tour_hotel.location LIKE "%'.$request->search.'%"';
            }
        }

        $holidays = $this->getTourLib()->searchTour(array('tour.*'), $where_arr);
        if(count($holidays)>0){
            foreach ($holidays as $key => $holiday) {
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
                if (!empty($cost)) {
                    $holidays[$key]['cost'] = $cost;
                }
                if (!empty($hotel)) {
                    $holidays[$key]['hotel'] = $hotel;
                }
            }
            $data['holidays'] = $holidays;
        }else{
            $data['holidays'] = array();
        }
        $cities_arr = array();
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/filter', $data);
    }

    /*

    public function hot_domestic_holidays() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $breadcrumbs = array(
            'Home' => route('index'),
            'Hot Domestic Holidays' => route('hot-domestic-holiday')
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = 'Hot Domestic Holidays';
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.domestic_international' => 1,'tour.tour_status'=>1];
        if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
            $where_arr['tour.tour_type'] = $_GET['theme_type'];
        }
        if(isset($_GET['duration']) && $_GET['duration']!=''){
            $where_arr['tour.tour_nights'] = $_GET['duration'];
        }
        if(isset($_GET['hotel']) && $_GET['hotel']!=''){
            $where_arr['tour.hotel'] = $_GET['hotel'];
        }
        if(isset($_GET['budget']) && $_GET['budget']!=''){
            $where_arr['tour.budget'] = $_GET['budget'];
        }
        if(isset($_GET['short_by']) && $_GET['short_by']!=''){
            $where_arr['tour.short_by'] = $_GET['short_by'];
        }
        $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);
        foreach ($holidays as $key => $holiday) {
            $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
            $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
            if (!empty($cost)) {
                $holidays[$key]['cost'] = $cost;
            }
            if (!empty($hotel)) {
                $holidays[$key]['hotel'] = $hotel;
            }
        }
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        // foreach ($cities as $key => $value) {
        //     $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        // }
        $data['cities'] = json_encode($cities_arr);
        $data['holidays'] = $holidays;
        return view('client/holiday/list', $data);
    }

    public function hot_international_holidays() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $breadcrumbs = array(
            'Home' => route('index'),
            'Hot International Holidays' => route('hot-international-holiday'),
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = 'Hot International Holidays';
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.domestic_international' => 2,'tour.tour_status'=>1];
        if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
            $where_arr['tour.tour_type'] = $_GET['theme_type'];
        }
        if(isset($_GET['duration']) && $_GET['duration']!=''){
            $where_arr['tour.tour_nights'] = $_GET['duration'];
        }
        if(isset($_GET['hotel']) && $_GET['hotel']!=''){
            $where_arr['tour.hotel'] = $_GET['hotel'];
        }
        if(isset($_GET['budget']) && $_GET['budget']!=''){
            $where_arr['tour.budget'] = $_GET['budget'];
        }
        if(isset($_GET['short_by']) && $_GET['short_by']!=''){
            $where_arr['tour.short_by'] = $_GET['short_by'];
        }
        $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);

        // $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), array('tour.domestic_international' => 2));
        foreach ($holidays as $key => $holiday) {
            $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
            $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
            if (!empty($cost)) {
                $holidays[$key]['cost'] = $cost;
            }
            if (!empty($hotel)) {
                $holidays[$key]['hotel'] = $hotel;
            }
            //dd(
            //  \DB::getQueryLog()
            //);
        }
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        // foreach ($cities as $key => $value) {
        //     $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        // }
        $data['cities'] = json_encode($cities_arr);
        //echo '<pre>';
        //print_r($holidays);
        //die;
        $data['holidays'] = $holidays;
        return view('client/holiday/list', $data);
    }

    

    public function theme_holidays() {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $breadcrumbs = array(
            'Home' => route('index'),
            'Holidays By Theme' => route('holidays-by-theme'),
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = 'Holidays By Theme';
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.tour_status'=>1];
        if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
            $where_arr['tour.tour_type'] = $_GET['theme_type'];
        }
        if(isset($_GET['duration']) && $_GET['duration']!=''){
            $where_arr['tour.tour_nights'] = $_GET['duration'];
        }
        if(isset($_GET['hotel']) && $_GET['hotel']!=''){
            $where_arr['tour.hotel'] = $_GET['hotel'];
        }
        if(isset($_GET['budget']) && $_GET['budget']!=''){
            $where_arr['tour.budget'] = $_GET['budget'];
        }
        if(isset($_GET['short_by']) && $_GET['short_by']!=''){
            $where_arr['tour.short_by'] = $_GET['short_by'];
        }
        $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);

        //$holidays = $this->getTourLib()->getToursDetails(array('tour.*'));
        foreach ($holidays as $key => $holiday) {
            $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
            $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
            if (!empty($cost)) {
                $holidays[$key]['cost'] = $cost;
            }
            if (!empty($hotel)) {
                $holidays[$key]['hotel'] = $hotel;
            }
        // dd(
        // \DB::getQueryLog()
        // );
        }
        // echo '<pre>';
        //print_r($holidays);
        //die;
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        $data['holidays'] = $holidays;
        return view('client/holiday/list', $data);
    }

    public function theme_holidays_sector($sectorId, $sectorName) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $sector = $this->getTourtypemasterLib()->getTourtypeById($sectorId);
        $breadcrumbs = array(
            'Home' => route('index'),
            'Holidays By Theme' => route('holidays-by-theme'),
            $sector->name => route('holidays-by-theme.sector',[$sector->typeId,str_slug($sector->name,'-')])
        );
        $data['breadcrumbs'] = $breadcrumbs;
        $data['title'] = $sector->name;
        $data['description'] = 'We specialize in customized/tailor-made tours to India as well as international tour packages. ';

        $data['tourBudget'] = $this->getCommonLib()->tourBudget();
        $data['tourDuration'] = $this->getCommonLib()->tourDuration();
        $data['hotelChoice'] = $this->getCommonLib()->hotelChoice();
        $data['toursSortBy'] = $this->getCommonLib()->toursSortBy();

        $where_arr = ['tour.tour_type'=>$sectorId,'tour.tour_status'=>1];
        if(isset($_GET['theme_type']) && $_GET['theme_type']!=''){
            $where_arr['tour.tour_type'] = $_GET['theme_type'];
        }
        if(isset($_GET['duration']) && $_GET['duration']!=''){
            $where_arr['tour.tour_nights'] = $_GET['duration'];
        }
        if(isset($_GET['hotel']) && $_GET['hotel']!=''){
            $where_arr['tour.hotel'] = $_GET['hotel'];
        }
        if(isset($_GET['budget']) && $_GET['budget']!=''){
            $where_arr['tour.budget'] = $_GET['budget'];
        }
        if(isset($_GET['short_by']) && $_GET['short_by']!=''){
            $where_arr['tour.short_by'] = $_GET['short_by'];
        }
        $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), $where_arr);
        // $holidays = $this->getTourLib()->getToursDetails(array('tour.*'), array('tour.tour_type'=>$sectorId));
        //echo '<pre>'; var_dump($holidays); die();
        foreach ($holidays as $key => $holiday) {
            $cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
            $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
            if (!empty($cost)) {
                $holidays[$key]['cost'] = $cost;
            }
            if (!empty($hotel)) {
                $holidays[$key]['hotel'] = $hotel;
            }
        // dd(
        // \DB::getQueryLog()
        //  );
        }
        //echo '<pre>';
        //print_r($holidays);
        //die;
        $data['holidays'] = $holidays;
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        $cities_arr = array();
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/holiday/list', $data);
    }

    */
}
