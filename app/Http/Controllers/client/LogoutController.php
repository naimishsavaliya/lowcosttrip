<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;

class LogoutController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		\Auth::logout();
		return redirect()->route('client_login_form');
	}

}
