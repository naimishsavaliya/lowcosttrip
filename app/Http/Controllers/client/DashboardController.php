<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Illuminate\Support\Facades\Cache;
use App\Customertype;
use Session;
use DB;
use Validator;
use Hash;
use Mail;

class DashboardController extends ControllerAbstract {

   
    public function index() {
    	$data = array();
    	Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
    	});
    	Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
    	});
    	Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
    	});
    	Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
    	    return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
    	});
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });

        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');

        // $data['intrested_ins'] = $this->getCommonLib()->getLeadIntrestedin();
        // $data['lead_sources'] = $this->getCommonLib()->getLeadSources();
        // $data['lead_places_type'] = $this->getCommonLib()->getLeadIsPlaceType();
        // $data['group_customised'] = $this->getCommonLib()->getLeadCustomizedOpt();
        // $data['lead_status'] = $this->getLeadstatusmasterLib()->getLeadstatusmaster(array('typeId', 'name'));
    	
        // $data['flight_leads'] = $this->getLeadLib()->getLead_data(['customer_id'=>\Auth::user()->id,'interested_in'=>2]);
        // $data['hotel_leads'] = $this->getLeadLib()->getLead_data(['customer_id'=>\Auth::user()->id,'interested_in'=>3]);
        // $data['holiday_leads'] = $this->getLeadLib()->getLead_data(['customer_id'=>\Auth::user()->id,'interested_in'=>7]);
        // $data['visa_leads'] = $this->getLeadLib()->getLead_data(['customer_id' => \Auth::user()->id, 'interested_in' => 8]);
        // $data['book_leads'] = $this->getLeadLib()->getLeadForBookTour(['lead.customer_id' => \Auth::user()->id, 'lead.interested_in' => 4]);
        // $data['all_leads'] = $this->getLeadLib()->getLeadForBookTour(['lead.customer_id' => \Auth::user()->id]);
        return view('client/dashboard', $data);
    }

    /*
     * Return Lead data in JSON
     */

    public function data(Request $request) {
        if ($request->ajax()) {
            $query = \App\Lead::select('lead.id', 
                        'lead.lead_id',
                        'lead.created_at',
                        DB::raw("CONCAT(lead.name, '\n', lead.email, '\n', lead.phone) as user_info"), 
                        'lead.interested_in',
                        'lead.assign_to', 
                        'master_lead_status.name as status_name', 
                        'lead.status_id', 
                        DB::raw("CONCAT(users.first_name,' ',users.last_name) as assign_to_user"))
                        ->leftjoin('master_lead_status', 'lead.status_id', '=', 'master_lead_status.typeId')
                        ->leftJoin('users', function($join) {
                        $join->whereRaw(\DB::raw("find_in_set(users.id, lead.assign_to)"));
                        })
                        ->where('lead.deleted_at', null)
                        ->groupBy('lead.id');
            $query->where('lead.customer_id', \Auth::user()->id);
            return datatables()->of($query)
                            ->editColumn('interested_in', function ($model) {
                                if (isset($model->interested_in)) {
                                    return $this->getCommonLib()->getLeadIntrestedin($model->interested_in);
                                } else {
                                    return '';
                                }
                            })
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->filterColumn('assign_to_user', function($query, $keyword) {
                                $query->whereRaw("CONCAT(users.first_name,' ',users.last_name) like ?", ["%{$keyword}%"]);
                            })
                            ->filterColumn('user_info', function($query, $keyword) {
                                $query->whereRaw("CONCAT(lead.name, '\n', lead.email, '\n', lead.phone) like ?", ["%{$keyword}%"]);
                            })
                            ->filterColumn('status_name', function($query, $keyword) {
                                $query->whereRaw("master_lead_status.name like ?", ["%{$keyword}%"]);
                            })
                            ->filterColumn('created_at', function ($query, $keyword) {
                                $query->whereRaw("DATE_FORMAT(lead.created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                            })
                            ->filterColumn('interested_in', function ($query, $keyword) use ($request) {
                                if (isset($request['search']) && $request['search']['value'] != '') {
                                    $keyword = $request['search']['value'];
                                }
                                $interested_in = $this->getCommonLib()->getLeadIntrestedinid($keyword);
                                if(count($interested_in) >0){
                                    $query->where("interested_in",implode(',',$interested_in), 'in',false );
                                }
                            })
                            // ->filterColumn('created_at', function($model) {
                            //     dd($model->created_at);
                            //     exit();
                            //     // $search_date = date('Y-m-d', strtotime($keyword));
                            //     // $query->whereRaw("DATE_FORMAT(lead.created_at, '%Y-%m-%d') = ".$search_date);
                            // })
                            ->addColumn('action', function($Leadquatation) {
                                $action_html = '';
                                $action_html .= '<a href="'.route('view-quotation',$Leadquatation->lead_id).'" target="_blank" title="View Quotation" class="btn btn-block btn-xs btn-info"><b>Quotations</b></a>';
                                if($Leadquatation->interested_in == '2'){
                                    $url_lead = 'update-flight';
                                }elseif ($Leadquatation->interested_in == '3') {
                                    $url_lead = 'update-hotel';
                                }elseif ($Leadquatation->interested_in == '7') {
                                    $url_lead = 'update-holiday';
                                }elseif ($Leadquatation->interested_in == '8') {
                                    $url_lead = 'update-visa';
                                }elseif ($Leadquatation->interested_in == '4') {
                                    $url_lead = 'update-tour';
                                }
                                $action_html .= '<a href="'.route($url_lead,$Leadquatation->id).'" target="_blank" title="View Quotation" class="btn btn-block btn-xs btn-warning"><b>View Lead</b></a>';
                                return $action_html;
                            })
                            ->toJson();

        }
    }


    /*
     * Return Lead data in JSON
     */

    public function conform_lead_data(Request $request) {
        if ($request->ajax()) {
            // $quotation = $this->getLeadQuatationLib()->getLeadQuatation('*', array("status_id" => '3'));
            // dd($quotation);
           
            $query = \App\Lead::select('lead.id', 
                        'lead.lead_id',
                        'lead.created_at',
                        'lead_quatation.quatation_id',
                        DB::raw("CONCAT(lead.name, '\n', lead.email, '\n', lead.phone) as user_info"), 
                        'lead.interested_in',
                        'lead.assign_to', 
                        'master_lead_status.name as status_name', 
                        'lead.status_id', 
                        DB::raw("CONCAT(users.first_name,' ',users.last_name) as assign_to_user"))
                        ->leftjoin('master_lead_status', 'lead.status_id', '=', 'master_lead_status.typeId')
                        ->join('lead_quatation', 'lead.id', '=', 'lead_quatation.lead_id')
                        ->leftJoin('users', function($join) {
                        $join->whereRaw(\DB::raw("find_in_set(users.id, lead.assign_to)"));
                        })
                        ->where('lead.deleted_at', null)
                        ->where('lead_quatation.status_id', 3)
                        ->groupBy('lead.id');
            $query->where('lead.customer_id', \Auth::user()->id);
            return datatables()->of($query)
                            ->editColumn('interested_in', function ($model) {
                                if (isset($model->interested_in)) {
                                    return $this->getCommonLib()->getLeadIntrestedin($model->interested_in);
                                } else {
                                    return '';
                                }
                            })
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->filterColumn('assign_to_user', function($query, $keyword) {
                                $query->whereRaw("CONCAT(users.first_name,' ',users.last_name) like ?", ["%{$keyword}%"]);
                            })
                            ->filterColumn('user_info', function($query, $keyword) {
                                $query->whereRaw("CONCAT(lead.name, '\n', lead.email, '\n', lead.phone) like ?", ["%{$keyword}%"]);
                            })
                            ->filterColumn('status_name', function($query, $keyword) {
                                $query->whereRaw("master_lead_status.name like ?", ["%{$keyword}%"]);
                            })
                            ->filterColumn('created_at', function ($query, $keyword) {
                                $query->whereRaw("DATE_FORMAT(lead.created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                            })
                            ->filterColumn('interested_in', function ($query, $keyword) use ($request) {
                                if (isset($request['search']) && $request['search']['value'] != '') {
                                    $keyword = $request['search']['value'];
                                }
                                $interested_in = $this->getCommonLib()->getLeadIntrestedinid($keyword);
                                if(count($interested_in) >0){
                                    $query->where("interested_in",implode(',',$interested_in), 'in',false );
                                }
                            })
                            // ->filterColumn('created_at', function($model) {
                            //     dd($model->created_at);
                            //     exit();
                            //     // $search_date = date('Y-m-d', strtotime($keyword));
                            //     // $query->whereRaw("DATE_FORMAT(lead.created_at, '%Y-%m-%d') = ".$search_date);
                            // })
                            ->addColumn('action', function($Leadquatation) {
                                $action_html = '';
                                $action_html .= '<a href="'.route('view-quotation',$Leadquatation->lead_id).'" target="_blank" title="View Quotation" class="btn btn-block btn-xs btn-info"><b>Quotations</b></a>';
                                if($Leadquatation->interested_in == '2'){
                                    $url_lead = 'update-flight';
                                }elseif ($Leadquatation->interested_in == '3') {
                                    $url_lead = 'update-hotel';
                                }elseif ($Leadquatation->interested_in == '7') {
                                    $url_lead = 'update-holiday';
                                }elseif ($Leadquatation->interested_in == '8') {
                                    $url_lead = 'update-visa';
                                }elseif ($Leadquatation->interested_in == '4') {
                                    $url_lead = 'update-tour';
                                }
                                $action_html .= '<a href="'.route($url_lead,$Leadquatation->id).'" target="_blank" title="View Quotation" class="btn btn-block btn-xs btn-warning"><b>View Lead</b></a>';
                                return $action_html;
                            })
                            ->toJson();

        }
    }

    public function update_flight($leadId) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');

        $data['lead'] = $this->getLeadLib()->getLeadById($leadId);
        //dd($lead->fromCity->city_name);
        $cities_arr = array();
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/update-flight', $data);
    }

    public function update_hotel($leadId) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $cities_arr = array();
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        $data['lead'] = $this->getLeadLib()->getLeadById($leadId);
        return view('client/update-hotel', $data);
    }

    public function update_holiday($leadId) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $data['group_customised'] = $this->getCommonLib()->getLeadCustomizedOpt();
        $cities_arr = array();
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        $data['lead'] = $this->getLeadLib()->getLeadById($leadId);
        $data['holiday_destination_details'] = $this->getHolidaydestinationdetailsLib()->getHolidaydestinationdetails(array('holiday_destination_details.*'),['lead_id'=>$leadId]);
        $data['holiday_activity_details'] = $this->getHolidayactivitiesLib()->getHolidayactivities(array('holiday_activities.*'),['lead_id'=>$leadId]);
        $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
        return view('client/update-holiday', $data);
    }

    public function update_visa($leadId) {
        $data = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $data['lead'] = $this->getLeadLib()->getLeadById($leadId);
                // dd($data['lead']);
        //dd($lead->fromCity->city_name);
        $cities_arr = array();
        $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        foreach ($cities as $key => $value) {
            $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        }
        $data['cities'] = json_encode($cities_arr);
        return view('client/update-visa', $data);
    }
    
    public function update_tour($leadId) {
        $data = array();
        $tour = array();
        $cost = array();
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $lead_data = $this->getLeadLib()->getLeadById($leadId);
        $cost = array();
        $hotel = array();
        $holiday = array();
        if (isset($lead_data->tour_id) && $lead_data->tour_id != '') {
            // $tour = $this->getTourLib()->get_tour_data($lead_data->tour_id);
            // $cost = $this->getTourCostAmountLib()->get_totalCost(['tour_cost_amount.tour_id' => $lead_data->tour_id, 'tour_cost_amount.rate_id' => 2, 'tour_cost_amount.package_type_id' => 1]);

            $holiday = $this->getTourLib()->get_tour_data($lead_data->tour_id);
            if(!empty($holiday)){
                $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->id));
                $hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 
                    'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
            }
        }
        // $data['tour'] = $tour;
        // $data['cost'] = $cost;
        $data['lead'] = $lead_data;
        $data['cost'] = $cost;
        $data['hotel'] = $hotel;
        $data['holiday'] = $holiday;
        $cities_arr = array();
        // $cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
        // foreach ($cities as $key => $value) {
        //     $cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
        // }
        $data['cities'] = json_encode($cities_arr);
        return view('client/update-book', $data);
    }
    
    public function view_quotation($leadId) {
        $data = array();
    	Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
    	});
    	Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
    	});
    	Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
    	});
    	Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
    	    return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
    	});
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });
    	$data['domestic_sectors'] = Cache::pull('domestic_sectors');
    	$data['global_sectors'] = Cache::pull('global_sectors');
    	$data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
    	$data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
        $data['quotation_status'] = $this->getCommonLib()->getQuotationStatus();
        $data['lead_info']  = $lead_info = $this->getLeadLib()->getLeadByNuber($leadId);
        //dd($data);
        $customer_id = \Auth::user()->id;
        if(isset($lead_info) && isset($lead_info->customer_id ) && $lead_info->customer_id == $customer_id){
        }else{
           return \Redirect::route('user_dashboard');
        } 

         
        $data['quotation']= $this->getLeadQuatationLib()->getLeadQuatation('*', array("lead_id" => $lead_info->id));
        
        return view('client/view-quotations', $data);
    }
    
    public function get_quotation($lead_id, Request $request) {
        if ($request->ajax()) {

             $query = \App\Leadquatation::select('lead_quatation.quaId','lead_quatation.quatation_id', 'lead_quatation.lead_id', 'lead_quatation.subject', DB::raw("CONCAT(lead_quatation.adult,'/',lead_quatation.child,'/',lead_quatation.infant) as members"),'lead_quatation.total_amount', 'lead_quatation.created_at', \DB::raw("CONCAT(users.first_name,' ',users.last_name) as full_name"), 'lead_quatation.status_id')
                ->leftjoin('users', 'lead_quatation.created_by', '=', 'users.id')
                ->leftjoin('lead', 'lead_quatation.lead_id', '=', 'lead.id')
                ->where('lead_quatation.deleted_at', null)
                ->where('lead.customer_id', \Auth::user()->id);
            if($lead_id > 0){
                $query->where('lead_quatation.lead_id', $lead_id);
            }
             return datatables()->of($query)
                            ->editColumn('created_at', function ($model) {
                                return date("d M Y", strtotime($model->created_at));
                            })
                            ->editColumn('subject', function ($model) {
                                return mb_strimwidth($model->subject, 0, 100, "...");
                            })
                            ->editColumn('total_amount', function ($model) {
                                return number_format($model->total_amount, 2);
                            })
                            ->filterColumn('members', function($query, $keyword) {
                                $query->whereRaw("CONCAT(lead_quatation.adult,'/',lead_quatation.child,'/',lead_quatation.infant) like ?", ["%{$keyword}%"]);
                            })
                            // ->editColumn('members', function ($model) {
                            //     $str_member = '';
                            //     if (isset($model->members)) {
                            //         $str_member .= $model->members . '/';
                            //     } else {
                            //         $str_member .= '0/';
                            //     }
                            //     if (isset($model->child)) {
                            //         $str_member .= $model->child . '/';
                            //     } else {
                            //         $str_member .= '0/';
                            //     }
                            //     if (isset($model->infant)) {
                            //         $str_member .= $model->infant;
                            //     } else {
                            //         $str_member .= '0';
                            //     }
                            //     return $str_member;
                            // })
                            // ->filterColumn('lead_quatation.created_at', function($query, $keyword) {
                            //     $search_date = date('Y-m-d', strtotime($keyword));
                            //     $query->whereRaw("DATE_FORMAT(lead_quatation.created_at, '%Y-%m-%d') like ?", ["%{$search_date}%"]);
                            // })
                            ->filterColumn('lead_quatation.created_at', function ($query, $keyword) {
                                $query->whereRaw("DATE_FORMAT(lead_quatation.created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                            })
                            ->filterColumn('full_name', function($query, $keyword) {
                                $query->whereRaw("CONCAT(users.first_name,' ',users.last_name) like ?", ["%{$keyword}%"]);
                            })
                            ->editColumn('status_id', function($Leadquatation) {
                                if ($Leadquatation->status_id == 1) {
                                    return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="'.$Leadquatation->quaId.'" data-id="'.$Leadquatation->status_id.'" data-toggle="modal"  data-target="#quotation_update_status"><span class="badge badge-bg-draft">Draft</span></a>';
                                } elseif ($Leadquatation->status_id == 2) {
                                    return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="'.$Leadquatation->quaId.'" data-id="'.$Leadquatation->status_id.'" data-toggle="modal"  data-target="#quotation_update_status"><span class="badge badge-bg-sent">Quote Sent</span></a>';
                                } elseif ($Leadquatation->status_id == 3) {
                                    return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="'.$Leadquatation->quaId.'" data-id="'.$Leadquatation->status_id.'" data-toggle="modal"  data-target="#quotation_update_status"><span class="badge badge-bg-approved">Quote Approved</span></a>';
                                } elseif ($Leadquatation->status_id == 4) {
                                    return '<a href="javascript:void(0)" class="quotation-change-status-btn" row-id="'.$Leadquatation->quaId. '"data-id="'.$Leadquatation->status_id.'" data-toggle="modal"  data-target="#quotation_update_status"><span class="badge badge-bg-rejected">Quote Rejected</span></a>';
                                } else {
                                    return '-';
                                }
                            })
                            ->addColumn('action', function($Leadquatation) {
                                return '<a href="'.route('quotation_info',[$Leadquatation->lead_id,$Leadquatation->quaId]).'"  title="Info" class=""><i class="fa fa-eye" aria-hidden="true"></i></a></a>';
                            })
                            ->rawColumns(['status_id', 'action'])
                            ->toJson();
        }
    }
    
    public function change_status(Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            try {
                $table = $request->table;
                $id = $request->id;
                $status = $request->status;
                $column_name = $request->column_name;
                
                $update_columns = "status";
                if(isset($request->column_update)){
                    $update_columns = $request->column_update;
                }
                
                //update query fire
                DB::table($table)
                        ->where($column_name, $id)
                        ->update([$update_columns => $status]);
                        if($status==3){
                            $quotation = $this->getLeadQuatationLib()->getLeadQuatationRecordById($id);
                            $customerpayment = $this->getCustomerPaymentLib()->chechQuotationCustomerPayment($id);
                            $records = array(
                                "quotation_id" => $id
                            );
                            // $lastRecord = $this->getCustomerPaymentLib()->getCustomerPaymentlastRecord();
                            // if (!empty($lastRecord)) {
                            //     if (isset($lastRecord->balance) && $lastRecord->balance != null) {
                            //         if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                            //             $records['balance'] = $quotation->total_amount + $lastRecord->balance;
                            //         }
                            //     }
                            // } else {
                            //     $records['balance'] = $quotation->total_amount;
                            // }

                            // if (isset($quotation->lead_id) && $quotation->lead_id != null) {
                            //     $lead = $this->getLeadLib()->getLeadById($quotation->lead_id);
                            //     $records['lead_id'] = $quotation->lead_id;
                            //     if (isset($lead->customer_id) && $lead->customer_id != null) {
                            //         $records['customer_id'] = $lead->customer_id;
                            //     }
                            // }
                            // if (isset($quotation->tour_id) && $quotation->tour_id != null) {
                            //     $records['tour_id'] = $quotation->tour_id;
                            // }
                            // if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                            //     $records['total_amount'] = $quotation->total_amount;
                            // }

                            if(!empty($quotation)){
                                if(isset($quotation->lead_id) && $quotation->lead_id != null){
                                    $lead = $this->getLeadLib()->getLeadById($quotation->lead_id);
                                    $records['lead_id'] = $quotation->lead_id;
                                    if (isset($lead->customer_id) && $lead->customer_id != null) {
                                        $records['customer_id'] = $lead->customer_id;
                                    }
                                    $lastRecord=$this->getCustomerPaymentLib()->getCustomerPaymentlastRecord($quotation->lead_id);
                                    if (!empty($lastRecord)) {
                                        if (isset($lastRecord->balance) && $lastRecord->balance != null) {
                                            if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                                                $records['balance'] = $quotation->total_amount + $lastRecord->balance;
                                            }
                                        }
                                    } else {
                                        $records['balance'] = $quotation->total_amount;
                                    }
                                }

                                if (isset($quotation->tour_id) && $quotation->tour_id != null) {
                                    $records['tour_id'] = $quotation->tour_id;
                                }
                                if (isset($quotation->total_amount) && !is_null($quotation->total_amount)) {
                                    $records['total_amount'] = $quotation->total_amount;
                                } 
                            }

                            if (empty($customerpayment)) {
                                $this->getCustomerPaymentLib()->addCustomerPayment($records);
                            }

                            $lead_id = $quotation->lead_id;
                            $quot_id = $id;
                            $data['lead'] = $lead = $this->getLeadLib()->getLeadById($lead_id);
                            $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quot_id);
                            $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
                            $quotation->quotation_status = $quotation_satus;
                            $data['quotation'] = $quotation;
                            
                            $data['particular'] = $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quot_id);
                            $data['itinarary'] = $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quot_id);
                            // ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary]
                            $receipt_no = '';
                            if($quotation->status_id == 3){
                                $invoice = $this->getInvoiceLib()->getInvoice('proforma', $lead->id, $quot_id );
                                if (isset($invoice) && $invoice != null) {
                                    $receipt_no = $invoice->receipt_number;
                                } else {
                                    $receipt_no = $this->getReceipLib()->getProformaReceiptNumber();
                                    $inset_data = array(
                                        'lead_id' => $lead->id,
                                        'type' => 'proforma',
                                        'customer_id' => ( (isset($lead->customer_id) && $lead->customer_id > 0 ) ? $lead->customer_id : ''),
                                        'receipt_number' => $receipt_no,
                                        'quatation_id' => $quot_id,
                                        'created_at' => date("Y-m-d H:i:s")
                                    );
                                    $invoice = $this->getInvoiceLib()->insertInvoice($inset_data);
                                    $this->getReceipLib()->updateProformaReceiptNumber();
                                }
                            }
                            $data['receipt_no'] =  $receipt_no; 
                            $data['user_info']= $user_info = $this->getUserLib()->getUserFullProfile($lead->customer_id);

                            // $contents = view('admin/quotation/quotation-email-template', ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary, 'receipt_no' => $receipt_no])->render();

                            // echo $contents;die;


                            $d = array('email' => $lead->email, 'quot_id' => $quotation->quatation_id );
                            Mail::send('admin/quotation/quotation-email-template', $data, function($message)  use ($d )  {
                                 $message->to($d['email'], 'Low Cost Trip (#'.$d['quot_id'].')')->subject
                                    ('Low cost trip (#'.$d['quot_id'].')');
                                 $message->from('info@lowcosttrip.com','Low Cost Trip');
                            });
                 
            //                echo $this->show($quotation->lead_id, $quotation->quaId);

//                            $lead_id = $quotation->lead_id;
//                            $quotation_id = $quotation->quaId;
//                            $lead = $this->getLeadLib()->getLeadById($lead_id);
//                            $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quotation_id);
////                            $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
////                            $quotation->quotation_status = $quotation_satus;
//
//                            $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quotation_id);
//                            $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quotation_id);
//                            $invoice = $this->getInvoiceLib()->getInvoice('proforma', $lead->id);  
//                            if (isset($invoice) && $invoice != null) { 
//                                $receipt_no = $invoice->receipt_number;
//                            }else{
//                                $receipt_no = $this->getReceipLib()->getProformaReceiptNumber();
//                                $inset_data = array(
//                                    'lead_id'=> $lead->id,
//                                    'type' => 'proforma',
//                                    'customer_id' =>( (isset($lead->customer_id) && $lead->customer_id > 0 ) ? $lead->customer_id : ''),
//                                    'receipt_number' => $receipt_no,
//                                    'created_at' => date("Y-m-d H:i:s")
//                                );
//                                $invoice = $this->getInvoiceLib()->insertInvoice($inset_data);  
//                                $this->getReceipLib()->updateProformaReceiptNumber();
//                            }
//                          
//                            $contents = view('admin/quotation/quotation-template', ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary,  'receipt_no' => $receipt_no])->render();
//                            $data = array(); 
//                            $pdf = \App::make('dompdf.wrapper');
//                            $pdf->loadHTML($contents);
//                            $file_name = isset($lead->name) ? $lead->name.time().'.pdf' : time().'.pdf';
//                            $file = public_path().'/pdf/'.$file_name;
//                            $pdf->save($file);
//                            $email = $lead->email;
//                            if(isset($lead->email)){
//                                $data = array('email'=>$email );
//                                Mail::send([], $data, function($message) use ($file) {
//                                    $message->subject('Low Cost Trip')
//                                           ->attach($file)
//                                           ->setBody('Your Prefer invoice', 'text/html');
//                                });
//                            }
                        } else {
                            //Start
                            $current_balance = null;
                            $PaymentData = $this->getCustomerPaymentLib()->chechQuotationCustomerPayment($id);
                            if (!empty($PaymentData)) {
                                if (isset($PaymentData->created_at)) {
                                    //$BalnceRecord = $this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at);
                                    $BalnceRecord = $this->getCustomerPaymentLib()->GetUpRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
                                    if (!empty($BalnceRecord)) {
                                        if (isset($BalnceRecord->balance) && $BalnceRecord->balance != null) {
                                            $current_balance = $BalnceRecord->balance - $PaymentData->balance;
                                        }
                                    }

                                    //$AllDownRecord = $this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at);
                                    $AllDownRecord = $this->getCustomerPaymentLib()->GetDownRecordCustomerPayment($PaymentData->created_at,$PaymentData->lead_id);
                                    if (!empty($AllDownRecord)) {
                                        foreach ($AllDownRecord as $key => $value) {
                                            $cur = $value->balance + $current_balance;
                                            $this->getCustomerPaymentLib()->updateCustomerPayment(['balance' => $cur], $value->id);
                                        }
                                    }
                                }
                                $this->getCustomerPaymentLib()->deleteCustomerPaymentQuotation($id);
                            }
                        }

                $response['success'] = true;
                $response['msg_status'] = 'success';
                $response['message'] = "Status has been changed successfully";
            } catch (Exception $e) {
                $response['success'] = false;
                $response['msg_status'] = 'warning';
                $response['message'] = "Something went wrong, please try again";
            }
        }
        exit(json_encode($response));
    }
    
    public function quotation_info($lead_id, $quot_id){ 
        Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
        });
        Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
        });
        Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
            return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
        });
        Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
            return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
        });
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });

        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
         
        $data['lead'] = $lead = $this->getLeadLib()->getLeadInfo($lead_id);
        $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quot_id); 

        if(isset($lead) && isset($lead['customer_id'] ) && $lead['customer_id'] == \Auth::user()->id && $quotation != null){
        }else{
           return \Redirect::route('user_dashboard');
        } 


        $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
        $quotation->quotation_status = $quotation_satus;
        $data['quotation'] = $quotation;
        
        $data['particular'] = $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quot_id);
        $data['itinarary'] = $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quot_id);
        $data['user_info']= $user_info = $this->getUserLib()->getUserFullProfile($lead->customer_id);

        // ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary]
        return view('client/quotation-info', $data);
    }

    public function make_payment($lead_id, $quot_id){
    	Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
    	});
    	Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
    	});
    	Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
    	    return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
    	});
    	Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
    	    return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
    	});
        Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
        });

        $data['domestic_sectors'] = Cache::pull('domestic_sectors');
        $data['global_sectors'] = Cache::pull('global_sectors');
        $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
        $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
        $data['CruiseSections'] = Cache::pull('CruiseSections');
         
        $data['lead'] = $lead = $this->getLeadLib()->getLeadInfo($lead_id);
        $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quot_id); 

        if(isset($lead) && isset($lead['customer_id'] ) && $lead['customer_id'] == \Auth::user()->id && $quotation != null){
        }else{
           return \Redirect::route('user_dashboard');
        } 

        if($lead->assign_to > 0){
            $data['assign_to'] = $this->getUserLib()->getUserById($lead->assign_to);
        }

        $data['wallet_user_data'] = '';
        if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
             $data['wallet_user_data'] = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
        }

        $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
        $quotation->quotation_status = $quotation_satus;
        $data['quotation'] = $quotation;
        
        $data['particular'] = $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quot_id);
        $data['itinarary']  = $itinarary  = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quot_id);
        $data['user_info']  = $user_info  = $this->getUserLib()->getUserFullProfile($lead->customer_id);
        return view('client/payment/make-payment', $data);
    }

    public function mail_quotation($lead_id, $quot_id){ 
        $data['lead'] = $lead = $this->getLeadLib()->getLeadInfo($lead_id);
        $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quot_id);


        if(isset($lead) && isset($lead['customer_id'] ) && $lead['customer_id'] == \Auth::user()->id && $quotation != null){
        }else{
           return \Redirect::route('user_dashboard');
        } 
        
        $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
        $quotation->quotation_status = $quotation_satus;
        $data['quotation'] = $quotation;
        
        $data['particular'] = $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quot_id);
        $data['itinarary'] = $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quot_id);
        // ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary]
        $receipt_no = '';
        if($quotation->status_id == 3){
            $invoice = $this->getInvoiceLib()->getInvoice('proforma', $lead->id, $quot_id);
            if (isset($invoice) && $invoice != null) {
                $receipt_no = $invoice->receipt_number;
            } else {
                $receipt_no = $this->getReceipLib()->getProformaReceiptNumber();
                $inset_data = array(
                    'lead_id' => $lead->id,
                    'type' => 'proforma',
                    'customer_id' => ( (isset($lead->customer_id) && $lead->customer_id > 0 ) ? $lead->customer_id : ''),
                    'receipt_number' => $receipt_no,
                    'quatation_id' => $quot_id,
                    'created_at' => date("Y-m-d H:i:s")
                );
                $invoice = $this->getInvoiceLib()->insertInvoice($inset_data);
                $this->getReceipLib()->updateProformaReceiptNumber();
            }
        }

        $data['user_info']= $user_info = $this->getUserLib()->getUserFullProfile($lead->customer_id);
        // dd($user_info);

        // $contents = view('admin/quotation/quotation-email-template', ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary, 'receipt_no' => $receipt_no, 'user_info' => $user_info])->render();

        // echo $contents;die;



        $data['receipt_no'] =  $receipt_no; 
        $d = array('email' => $lead->email, 'quot_id' => $quotation->quatation_id);
        Mail::send('admin/quotation/quotation-email-template', $data, function($message)  use ($d )  {
             $message->to($d['email'], 'Low Cost Trip (#'.$d['quot_id'].')')->subject
                ('Low cost trip (#'.$d['quot_id'].')');
             // $message->attach($data['file']);
             $message->from('info@lowcosttrip.com','Low Cost Trip');
        });
        if (Mail::failures()) {
            \Session::flash('message', 'Something went wrong please try again later..! ');
            \Session::flash('alert-class', 'alert-danger');
        }else{
            \Session::flash('message', 'Quotation sent in your mail. Please check your inbox..!');
            \Session::flash('alert-class', 'alert-success');
        }
        return \Redirect::route('quotation_info', [$lead_id, $quot_id]);
    }

    public function check_auth($lead_id) {
        $lead_info = $this->getLeadLib()->getLeadById($lead_id);
        $customer_id = \Auth::user()->id;
        if(isset($lead_info) && isset($lead_info->customer_id ) && $lead_info->customer_id == $customer_id){
            return true;
        }else{
          return back();
        }
    }

    public function user_profile() {
        if(\Auth::user() != '' ){
            $data = array();
            Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
                return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
            });
            Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
                return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
            });
            Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
                return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
            });
            Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
                return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
            });
            Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
                return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
            });

            $data['domestic_sectors'] = Cache::pull('domestic_sectors');
            $data['global_sectors'] = Cache::pull('global_sectors');
            $data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
            $data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
            $data['CruiseSections'] = Cache::pull('CruiseSections');
            
            $data['user_data'] = $this->getUserLib()->getUserFullProfile(\Auth::user()->id);
            $data['users'] = $this->getUserLib()->getUserWithProfile(\Auth::user()->id);
            $data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
            if($data['users'][0]->country_id != NULL && $data['users'][0]->country_id > 0){
                $data['states'] = $this->getStateLib()->getStateByCountry($data['users'][0]->country_id, array('state_id', 'state_name'));
            }
            if($data['users'][0]->state_id != NULL && $data['users'][0]->state_id > 0){
                $data['cities'] = $this->getCityLib()->getCityByState($data['users'][0]->state_id, array('city_id', 'city_name'));
            }
            $data['timezones'] = $this->getCommonLib()->getTimezoneData();
            return view('client/profile/user-profile', $data);
        }
    }
    
    public function getStateByCountry($country_id) {
        $states = $this->getStateLib()->getStateByCountry($country_id, array('state_id', 'state_name'));
        exit(json_encode($states));
    }

    public function getCityByState($state_id) {
        $cities = $this->getCityLib()->getCityByState($state_id, array('city_id', 'city_name'));
        exit(json_encode($cities));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        Session::flash('tab', 'edit_profile');
        $userId = "";
        $userData = $request->all();
        $user_id = \Auth::user()->id;
        if (!empty($user_id)) {
            $userId = $user_id;
        }

        if (empty($user_id)) {
            $rules = [
                'first_name'    => 'required|string|max:255',
                'last_name'     => 'required|string|max:255',
                'email'         => 'required|string|email|max:255|unique:users',
                'phone_no'      => 'required|string|max:20|unique:users_profile',
                'password'      =>'required|min:6',
                'confirm_password'=>'required|same:password|min:6',
                'age'           => 'required',
                'gender'        => 'required',
                'address'       => 'required',
                'country_id'    => 'required',
                'state_id'      => 'required',
                'city_id'       => 'required',
                'pin_code'      => 'required',
                // 'status'        => 'required'
            ];
        }else{
            $rules = [
                'first_name'    => 'required|string|max:255',
                'last_name'     => 'required|string|max:255',
                'email'         => 'required|string|email|max:255',
                'phone_no'      => 'required|string|max:20',
                'age'           => 'required',
                'gender'        => 'required',
                'address'       => 'required',
                'country_id'    => 'required',
                'state_id'      => 'required',
                'city_id'       => 'required',
                'pin_code'      => 'required',
                // 'status'        => 'required'
            ];
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            if (isset( $userId )) {
                return redirect()->route('user.profile.page', $userId)->withErrors($validator)->withInput();
            } else {
                return redirect()->route('user.profile.page')->withErrors($validator)->withInput();
            }
        } else {
            $data['first_name']     = $userData['first_name'];
            $data['last_name']      = $userData['last_name'];
            if (empty($userId)) {
                $data['email']      = $userData['email'];
            }

            // if($userData['password']){
            //    $data['password']       = Hash::make($userData['password']);
            // }

            // $data['status']         = $userData['status'];

            // if (empty($userId)) {
            
            // }

            $profileData = array(
                'landline_no'       => $userData['landline_no'],
                'age'               => $userData['age'],
                'gender'            => $userData['gender'],
                'time_zone'         => $userData['time_zone'],
                'address'           => $userData['address'],
                'country_id'        => $userData['country_id'],
                'state_id'          => $userData['state_id'],
                'city_id'           => $userData['city_id'],
                'pin_code'          => $userData['pin_code'],
                'information'       => $userData['information']
            );

            $profileData['phone_no']      = $userData['phone_no'];


            if($userData['birth_date'] != ''){
                $profileData['birth_date'] = date('Y-m-d', strtotime($userData['birth_date']));
            }


            if ($user_id > 0) {
                $this->getUserLib()->updateUser($data, $user_id);
                $this->getUserprofileLib()->updateUserprofile($profileData, $user_id);
                Session::flash('success_msg', 'Your profile has been updated successfully!');
            } else {
                $userId = $this->getUserLib()->addUser($data);
                $profileData['user_id'] = $userId;
                $this->getUserprofileLib()->addUserprofile($profileData);
                Session::flash('success_msg', 'Your profile has been saved successfully!');
            }
            $user = $this->getUserLib()->getUserById($userId);
            //Updoad image and update in db
            $image = $request->file('image');
            // dd($image);
            if (!empty($image)) {
                $imagename = $userId . '_user.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/user_profile');
                $image->move($destinationPath, $imagename);
                $profilepicData['image'] = $imagename;
                $this->getUserprofileLib()->updateUserprofile($profilepicData, $userId);

            }
        }
        return redirect()->route('user.profile.page'); 
    }

    public function change_password(Request $request){
        Session::flash('tab', 'change_password');
        $userData = $request->all();
        $userId = '';
        $user_id = \Auth::user()->id;
        if (!empty($user_id)) {
            $userId = $user_id;
        }
        if (!empty($user_id)) {
            $request_data = $request->All();
            $messages = [
                'current-password.required' => 'Please enter current password',
                'password.required' => 'Please enter password',
            ];

              $validator = Validator::make($request_data, [
                'current-password' => 'required',
                'password' => 'required|same:password',
                'password_confirmation' => 'required|same:password',     
              ], $messages);

            if($validator->fails())
            {
              return redirect()->route('user.profile.page')->withErrors($validator)->withInput();
            }
        else
        {  
          $current_password = \Auth::user()->password;           
          if(Hash::check($request_data['current-password'], $current_password))
          {           
            if($userData['password']){
               $pass_change['password']       = Hash::make($userData['password']);
            }
            $this->getUserLib()->updateUser($pass_change, $user_id);
            Session::flash('success_msg', 'Password has been changed successfully!');
            return redirect()->route('user.profile.page');
          }
          else
          {           
            $request->session()->flash('old_pass_error', 'Please enter old current password');
            return redirect()->route('user.profile.page');
          }
        }        
      }else{
        return redirect()->route('user.profile.page');
      }    
    }

    public function wallet_data(Request $request) {
        if ($request->ajax()) {
            $query = \App\Wallet::select('id','user_id','created_at','credit','debit','current_balance')
                        ->where('user_id', \Auth::user()->id)
                        ->orderBy('id', 'DESC');
            return datatables()->of($query)
                ->editColumn('created_at', function ($model) {
                    return date("d M Y", strtotime($model->created_at));
                })
                ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %b %Y') like ?", ["%$keyword%"]);
                })
                ->toJson();
        }
    }

    /*
    public function mail_quotation($lead_id, $quot_id){
        $data['lead'] = $lead = $this->getLeadLib()->getLeadById($lead_id);
        $quotation = $this->getLeadQuatationLib()->getLeadQuatationById($lead_id, $quot_id);
        $quotation_satus = $this->getCommonLib()->getQuotationStatus($quotation->status_id);
        $quotation->quotation_status = $quotation_satus;
        $data['quotation'] = $quotation;
        
        $data['particular'] = $particular = $this->getLeadParticularLib()->getLeadParticularById($lead_id, $quot_id);
        $data['itinarary'] = $itinarary = $this->getLeadItinararyLib()->getLeadItinararyById($lead_id, $quot_id);
        // ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary]
        if($quotation->status_id == 3){
            $invoice = $this->getInvoiceLib()->getInvoice('proforma', $lead->id);
            if (isset($invoice) && $invoice != null) {
                $receipt_no = $invoice->receipt_number;
            } else {
                $receipt_no = $this->getReceipLib()->getProformaReceiptNumber();
                $inset_data = array(
                    'lead_id' => $lead->id,
                    'type' => 'proforma',
                    'customer_id' => ( (isset($lead->customer_id) && $lead->customer_id > 0 ) ? $lead->customer_id : ''),
                    'receipt_number' => $receipt_no,
                    'created_at' => date("Y-m-d H:i:s")
                );
                $invoice = $this->getInvoiceLib()->insertInvoice($inset_data);
                $this->getReceipLib()->updateProformaReceiptNumber();
            }
        }
        $data['receipt_no'] =  $receipt_no;

        $contents = view('admin/quotation/quotation-email-template', ['lead' => $lead, 'quotation' => $quotation, 'particular' => $particular, 'itinarary' => $itinarary, 'receipt_no' => $receipt_no])->render();

        echo $contents;die;

        $d = array('email' => $lead->email);
        Mail::send('admin/quotation/quotation-email-template', $data, function($message)  use ($d )  {
             $message->to($d['email'], 'Low Cost Trip')->subject
                ('Low Cost Trip');
             // $message->attach($data['file']);
             $message->from('info@lowcosttrip.com','Low Cost Trip');
        });
        return view('client/quotation-info', $data);
    }
    */

}
