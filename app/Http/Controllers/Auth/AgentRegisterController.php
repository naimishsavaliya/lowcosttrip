<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\User;
use App\Userprofile;
use App\Wallet;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class AgentRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'city_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
            'email' => 'required|string|email|max:255',
            'phone_no' => 'required|string|max:20',
            'country_code' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $activation_token       = str_random(10);
        $user = User::create([
            'first_name'      => $data['first_name'],
            'last_name'       => $data['last_name'],
            'email'           => $data['email'],
            'password'        => Hash::make($data['password']),
            'activation_token'=> $activation_token,
            'role_id'         => 4,
            'travel_agency_name' =>$data['travel_agency_name'],
            'is_agent'       => 1,
        ]);

        $users_profile = Userprofile::create([
            'user_id'     => $user->id,
            'country_id'  => $data['country_id'],
            'state_id'    => $data['state_id'],
            'city_id'     => $data['city_id'],
            'phone_no'    => $data['phone_no'],
            'country_code'=> $data['country_code']
            ]);

        $wallet = Wallet::create([
            'user_id'     => $user->id,
            'credit'      => 100,
            'current_balance'=>100
        ]);

  
           Mail::send('client/register-template', ['email'=>$data['email'],'first_name'=>$data['first_name'],'last_name'=>$data['last_name'],'activation_token'=>$activation_token], function($message) use ($data) {
              $message->to($data['email'], 'Low cost trip')->subject
                 ('Thank you for registering low cost trip');
              $message->from('info@lowcosttrip.com','Low cost trip');
           });

        // $body = "<html>
        //             <head></head>
        //             <body>
        //               <div style=\"background:#ecf0f1;width: 500px;font-family: Arial, Helvetica, sans-serif;\">
        //                    <div style=\"width: 500px; background-color:#fefefe;font-family: Arial, Helvetica, sans-serif;\">
        //                         <div >
        //                              <h4>Hello ".$data['first_name'].' '.$data['last_name']."</h4>
        //                              <br/>
        //                              <p style=\"margin-left: 10px;\">Thank you for registering to the Low Cost Trip. 
        //                                   If you need any help please email the info@lowcosttrip.com or call them on  +91 81 82 83 84 85.
        //                              </p>
        //                         </div>
        //                    </div>
        //               </div>
        //             </body>
        //         </html>";

        //dd($body);

        //     $fullname    =$data['first_name'].' '.$data['last_name'];
        //     $to          = $data['email'];
        //     $from        = "info@lowcosttrip.com";
        //     $subject     = "THANK YOU FOR REGISTERING LOW COST TRIP";
        //     $mainMessage = $body;
        //     $headers     = "From: $from";

        //     $semi_rand     = md5(time());
        //     $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
        //     $headers      .= "\nMIME-Version: 1.0\n" .
        //     "Content-Type: multipart/mixed;\n" .
        //     " boundary=\"{$mime_boundary}\"";
        //     $message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
        //     "Content-Transfer-Encoding: 7bit\n\n" . $mainMessage . "\n\n"; 

        //     mail($to, $subject, $message, $headers);

        return $user;

    }

    protected function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        // $activation_token       = str_random(10);
        //    Mail::send('client/register-template', ['email'=>$request->email,'first_name'=>$request->first_name,'last_name'=>$request->last_name,'activation_token'=>$activation_token], function($message) use ($request) {
        //       $message->to($request->email, 'Low cost trip')->subject
        //          ('Thank you for registering low cost trip');
        //       $message->from('info@lowcosttrip.com','Low cost trip');
        //    });

        $request->session()->flash('message', 'Registration successful. We have sent verification mail to your email id. Please check your email to activate your account.');
        return redirect()->route('agent_login_form');
      
    }
}
