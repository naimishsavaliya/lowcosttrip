<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerAbstract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use RobinCSamuel\LaravelMsg91\Facades\LaravelMsg91;
use App\Helpers\TransactionRequest;
use Mail;
use View;
use Redirect;
use DB;
use Session;
use App\Wallet;

class HomeController extends ControllerAbstract {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	//protected $url = 'http://naimish/lowcosttrip-wp/wp-json/wp/v2/';

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		// dd(env('WP_URL'));
		//"http://naimish/lowcosttrip-wp/"
		$data = array();
		Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
		});
		Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
		});
		Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
		});
		Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
			return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
		});
		Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
	    });
		$data['domestic_sectors'] = Cache::pull('domestic_sectors');
		$data['global_sectors'] = Cache::pull('global_sectors');
		$data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
		$data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
		$data['CruiseSections'] = Cache::pull('CruiseSections');

		$data['home_banner'] = $this->getHomebannerLib()->getHomebanner(array('banId','title','tooltip','image_1','redirect_to'),['banner_opt'=>1,'banner_cat'=>1,'status'=>1]);

		$Hotdeal = $this->getHometoursectionLib()->getActiveHometoursection();

		// if(!empty($Hotdeal)){
		// 	foreach ($Hotdeal as $key => $HotValue) {
		// 		if($HotValue->primary_name=='Domestic Holidays'){
		// 			$hot_domestic_holidays = $this->getDomesticHolidayLib()->getHotdomesticholidays(['domestic_holidays.deleted_at'=>null,'domestic_holidays.status'=>1]);
		// 			if(!empty($hot_domestic_holidays)){
		// 				foreach ($hot_domestic_holidays as $key1 => $holiday) {
		// 				    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
		// 				    if (!empty($cost)) {
		// 				        $hot_domestic_holidays[$key1]['cost'] = $cost;
		// 				    }
		// 				}
		// 			}
		// 			$Hotdeal[$key]['hot_tour_data'] = $hot_domestic_holidays;
		// 		}else if ($HotValue->primary_name=='International Holidays') {
		// 			$hot_international_holidays = $this->getInternationalHolidayLib()->getHotInternationalholidays(['international_holidays.deleted_at'=>null,'international_holidays.status'=>1]);
		// 			if(!empty($hot_international_holidays)){
		// 				foreach ($hot_international_holidays as $key2 => $holiday) {
		// 				    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
		// 				    if (!empty($cost)) {
		// 				        $hot_international_holidays[$key2]['cost'] = $cost;
		// 				    }
		// 				}
		// 			}
		// 			$Hotdeal[$key]['hot_tour_data'] = $hot_international_holidays;
		// 		}else if ($HotValue->primary_name=='Featured Tours') {
		// 			$hot_featured_tours = $this->getFeaturedTblTourLib()->getFeaturedTours(['featured_tbl_tours.deleted_at'=>null,'featured_tbl_tours.status'=>1]);
		// 			if(!empty($hot_featured_tours)){
		// 				foreach ($hot_featured_tours as $key3 => $holiday) {
		// 				    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
		// 				    if (!empty($cost)) {
		// 				        $hot_featured_tours[$key3]['cost'] = $cost;
		// 				    }
		// 				}
		// 			}
		// 			$Hotdeal[$key]['hot_tour_data'] = $hot_featured_tours;
		// 		}else if ($HotValue->primary_name=='Hot Deals Tours') {
		// 			$hot_deals_tours = $this->getHotDealTourLib()->getHotDealsTours(['hot_deal_tours.deleted_at'=>null,'hot_deal_tours.status'=>1]);
		// 			if(!empty($hot_deals_tours)){
		// 				foreach ($hot_deals_tours as $key4 => $holiday) {
		// 				    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
		// 				    if (!empty($cost)) {
		// 				        $hot_deals_tours[$key4]['cost'] = $cost;
		// 				    }
		// 				}
		// 			}
		// 			$Hotdeal[$key]['hot_tour_data'] = $hot_deals_tours;
		// 		}else if ($HotValue->primary_name=='New Arrival Tours') {
		// 			$new_arrival_tours = $this->getNewArrivalTourLib()->getNewArrivalTours(['new_arrival_tours.deleted_at'=>null,'new_arrival_tours.status'=>1]);
		// 			if(!empty($new_arrival_tours)){
		// 				foreach ($new_arrival_tours as $key5 => $holiday) {
		// 				    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
		// 				    if (!empty($cost)) {
		// 				        $new_arrival_tours[$key5]['cost'] = $cost;
		// 				    }
		// 				}
		// 			}
		// 			$Hotdeal[$key]['hot_tour_data'] = $new_arrival_tours;
		// 		}else if ($HotValue->primary_name=='Featured Destination Tours') {
		// 			$featured_destination_tours = $this->getFeaturedDestinationTourLib()->getFeaturedDestinationTours(['featured_destination_tours.deleted_at'=>null,'featured_destination_tours.status'=>1]);
		// 			if(!empty($featured_destination_tours)){
		// 				foreach ($featured_destination_tours as $key6 => $holiday) {
		// 				    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
		// 				    if (!empty($cost)) {
		// 				        $featured_destination_tours[$key6]['cost'] = $cost;
		// 				    }
		// 				}
		// 			}
		// 			$Hotdeal[$key]['hot_tour_data'] = $featured_destination_tours;
		// 		}
				
		// 	}
		// }

		if(!empty($Hotdeal)){
			foreach ($Hotdeal as $key => $HotValue) {
				if($HotValue->primary_name=='Domestic Holidays'){
					$hot_domestic_holidays = $this->getDomesticHolidayLib()->getHotdomesticholidays(['domestic_holidays.deleted_at'=>null,'domestic_holidays.status'=>1,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1]);
					 //$where_arr = ['tour.domestic_international' => 1,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1];
					//dd($hot_domestic_holidays);
					// if(!empty($hot_domestic_holidays)){
					// 	foreach ($hot_domestic_holidays as $key1 => $holiday) {
					// 	    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
					// 	    if (!empty($cost)) {
					// 	        $hot_domestic_holidays[$key1]['cost'] = $cost;
					// 	    }
					// 	}
					// }
					$Hotdeal[$key]['hot_tour_data'] = $hot_domestic_holidays;
				}else if ($HotValue->primary_name=='International Holidays') {
					$hot_international_holidays = $this->getInternationalHolidayLib()->getHotInternationalholidays(['international_holidays.deleted_at'=>null,'international_holidays.status'=>1,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1]);
					// if(!empty($hot_international_holidays)){
					// 	foreach ($hot_international_holidays as $key2 => $holiday) {
					// 	    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
					// 	    if (!empty($cost)) {
					// 	        $hot_international_holidays[$key2]['cost'] = $cost;
					// 	    }
					// 	}
					// }
					$Hotdeal[$key]['hot_tour_data'] = $hot_international_holidays;
				}else if ($HotValue->primary_name=='Featured Tours') {
					$hot_featured_tours = $this->getFeaturedTblTourLib()->getFeaturedTours(['featured_tbl_tours.deleted_at'=>null,'featured_tbl_tours.status'=>1,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1]);
					// if(!empty($hot_featured_tours)){
					// 	foreach ($hot_featured_tours as $key3 => $holiday) {
					// 	    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
					// 	    if (!empty($cost)) {
					// 	        $hot_featured_tours[$key3]['cost'] = $cost;
					// 	    }
					// 	}
					// }
					$Hotdeal[$key]['hot_tour_data'] = $hot_featured_tours;
				}else if ($HotValue->primary_name=='Hot Deals Tours') {
					$hot_deals_tours = $this->getHotDealTourLib()->getHotDealsTours(['hot_deal_tours.deleted_at'=>null,'hot_deal_tours.status'=>1,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1]);
					// if(!empty($hot_deals_tours)){
					// 	foreach ($hot_deals_tours as $key4 => $holiday) {
					// 	    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
					// 	    if (!empty($cost)) {
					// 	        $hot_deals_tours[$key4]['cost'] = $cost;
					// 	    }
					// 	}
					// }
					$Hotdeal[$key]['hot_tour_data'] = $hot_deals_tours;
				}else if ($HotValue->primary_name=='New Arrival Tours') {
					$new_arrival_tours = $this->getNewArrivalTourLib()->getNewArrivalTours(['new_arrival_tours.deleted_at'=>null,'new_arrival_tours.status'=>1,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1]);
					// if(!empty($new_arrival_tours)){
					// 	foreach ($new_arrival_tours as $key5 => $holiday) {
					// 	    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
					// 	    if (!empty($cost)) {
					// 	        $new_arrival_tours[$key5]['cost'] = $cost;
					// 	    }
					// 	}
					// }
					$Hotdeal[$key]['hot_tour_data'] = $new_arrival_tours;
				}else if ($HotValue->primary_name=='Featured Destination Tours') {
					$featured_destination_tours = $this->getFeaturedDestinationTourLib()->getFeaturedDestinationTours(['featured_destination_tours.deleted_at'=>null,'featured_destination_tours.status'=>1,'tour.tour_status'=>1,'tour.deleted_at'=>null,'tour.public_private' => 1]);
					// if(!empty($featured_destination_tours)){
					// 	foreach ($featured_destination_tours as $key6 => $holiday) {
					// 	    $cost = $this->getTourCostLib()->getCurrentTourCost(array('id', 'from_date', 'to_date', 'discount'), array('tour_id' => $holiday->tour_id));
					// 	    if (!empty($cost)) {
					// 	        $featured_destination_tours[$key6]['cost'] = $cost;
					// 	    }
					// 	}
					// }
					$Hotdeal[$key]['hot_tour_data'] = $featured_destination_tours;
				}
				
			}
		}
		$data['Hotdeal'] = $Hotdeal;

		//$blog_posts = collect($this->getJson(env('WP_URL') . 'wp-json/wp/v2/posts?per_page=3&orderby=date&order=asc'));
		$blog_posts = collect($this->getJson(env('WP_URL') . 'wp-json/wp/v2/posts?per_page=3&orderby=date&order=desc'));
		if(!empty($blog_posts)){
			$data['source_url'] = array();
			foreach ($blog_posts as $key => $value) {
				$media=collect($this->getJson(env('WP_URL') . 'wp-json/wp/v2/media/'.$value->featured_media));
				$data['source_url'][]=$media['source_url'];
			}
			$data['blog_posts']=$blog_posts;
		}else{
			$data['blog_posts']=array();
		}

		// $data['source_url'] = array();
		// foreach ($blog_posts as $key => $value) {
		// 	$media=collect($this->getJson(env('WP_URL') . 'wp-json/wp/v2/media/'.$value->featured_media));
		// 	$data['source_url'][]=$media['source_url'];
		// }
		// $data['blog_posts']=$blog_posts;
		$cities_arr = array();
		$cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
		foreach ($cities as $key => $value) {
			$cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
		}
		$data['cities'] = json_encode($cities_arr);
		return view('client/index', $data);
	}

	protected function getJson($url){
        $response = file_get_contents($url, false);
        return json_decode( $response );
		// $response = file_get_contents($url);
  //       if ($response === false) {
  //             return json_decode( array() );
  //          } else {
  //          	return json_decode( $response );
  //          }
    }

	public function customise_book() {
		$data = array();
		Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
		});
		Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
		});
		Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
		});
		Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
			return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
		});
		Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
	    });
		$data['domestic_sectors'] = Cache::pull('domestic_sectors');
		$data['global_sectors'] = Cache::pull('global_sectors');
		$data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
		$data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
		$data['CruiseSections'] = Cache::pull('CruiseSections');
		$data['travel_type'] = $this->getCommonLib()->getLeadCustomizedOpt();
		if(isset(\Auth::user()->id) && \Auth::user()->id > 0){
			$data['user_data'] = $this->getUserLib()->getUserFullProfile(\Auth::user()->id);
		}
		
		$cities_arr = array();
		$cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
		foreach ($cities as $key => $value) {
			$cities_arr[] = array('value' => $value->city_id, "label" => $value->city_name);
		}
		$data['cities'] = json_encode($cities_arr);
		return view('client/customise-book', $data);
	}

	public function flight_data_store(Request $request) {
		$rules = [
			'flight_type' => 'required|string|max:255',
			'from_city' => 'required|string|max:255',
			'from_city_id' => 'required|string|max:255',
			'to_city' => 'required|string|max:255',
			'to_city_id' => 'required|string|max:255',
			'flight_class' => 'required|string|max:255',
			'deprature_date' => 'required|string|max:255',
			// 'return_date' => 'required|string|max:255',
			'adults' => 'required|string|max:255',
			'contact_name' => 'required|string|max:255',
			'contact_no' => 'required|string|max:255',
			'email_id' => 'required|string|email|max:255',
		];
		if ($request->flight_type == 'Round Trip') {
			$rules['return_date'] = 'required|string|max:255';
	    }
		$validator = Validator::make($request->all(), $rules);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {
			$user = $this->getUserLib()->getUserByEmailOrPhone($request->email_id, $request->contact_no);
			if (!empty($user->id)) {
				$customer_id = $user->id;
			} else {
				$name_arr = explode(" ", $request->contact_name);
				$last_name = "";
				foreach ($name_arr as $key => $value) {
					if ($key == 0) {
						$first_name = $value;
					} else {
						$last_name .= " " . $value;
					}
				}
				$userData = array(
					"first_name" => $first_name,
					"last_name" => trim($last_name, " "),
					"email"     => $request->email_id,
					"password"  => Hash::make($request->contact_no),
					"role_id" => 5,
					'status'  => 1,
					'is_new_password' => 1,
					'forgot_token' => str_random(10),
				);
				$customer_id = $this->getUserLib()->addUser($userData);
				$profileData = array();
				$profileData['user_id'] = $customer_id;
				$profileData['phone_no'] = $request->contact_no;
				$this->getUserprofileLib()->addUserprofile($profileData);
				$this->send_mail_to_user($customer_id);
			}
			if(\Auth::check()){
				$customer_id=\Auth::user()->id;
			}
			//dd($customer_id);
			$leadData = array(
				"name" => $request->contact_name,
				"email" => $request->email_id,
				"phone" => $request->contact_no,
				"interested_in" => 2, //2 = Flight
				"flight_type" => $request->flight_type,
				"from_city" => isset($request->from_city_id)?$request->from_city_id:'',
				"to_city" => isset($request->to_city_id)?$request->to_city_id:'',
				"flight_class" => $request->flight_class,
				"deprature_date" => date("Y-m-d", strtotime($request->deprature_date)),
				"return_date" => date("Y-m-d", strtotime($request->return_date)),
				"adult" => $request->adults,
				"child" => $request->child,
				"infant" => $request->infants,
				"notes" => $request->notes,
				"status_id" => 1, //1 = New
				"lead_source" => 1, // 1 = Web
				"customer_id" => $customer_id,
				"customer_type_id" => 9
			);
			$lead_id = $this->getLeadLib()->addLead($leadData);
			$lead = $this->getLeadLib()->getLeadById($lead_id, ['lead_id']);
			$rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "Inquiry created successfully. Your inquiry ref. no is ".$lead->lead_id.".";
		}
		return response()->json($return);
	}
	
	public function hotel_data_store(Request $request) {
		$rules = [
			'destination_city' => 'required|string|max:255',
			'destination_city_id' => 'required|string|max:255',
			'deprature_date' => 'required|string|max:255',
			'number_of_nights' => 'required|string|max:255',
			'return_date' => 'required|string|max:255',
			// 'number_of_guests' => 'required|string|max:255',
			'adults' => 'required|string|max:255',
			'contact_name' => 'required|string|max:255',
			'contact_no' => 'required|string|max:255',
			'email_id' => 'required|string|email|max:255',
		];
		$validator = Validator::make($request->all(), $rules);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {
			$user = $this->getUserLib()->getUserByEmailOrPhone($request->email_id, $request->contact_no);
			if (!empty($user->id)) {
				$customer_id = $user->id;
			} else {
				$name_arr = explode(" ", $request->contact_name);
				$last_name = "";
				foreach ($name_arr as $key => $value) {
					if ($key == 0) {
						$first_name = $value;
					} else {
						$last_name .= " " . $value;
					}
				}
				$userData = array(
					"first_name" => $first_name,
					"last_name" => trim($last_name, " "),
					"email" => $request->email_id,
					"password"  => Hash::make($request->contact_no),
					"role_id" => 5,
					'status'  => 1,
					'is_new_password' => 1,
					'forgot_token' => str_random(10),
				);
				$customer_id = $this->getUserLib()->addUser($userData);
				$profileData = array();
				$profileData['user_id'] = $customer_id;
				$profileData['phone_no'] = $request->contact_no;
				$this->getUserprofileLib()->addUserprofile($profileData);
				$this->send_mail_to_user($customer_id);
			}
			if(\Auth::check()){
				$customer_id=\Auth::user()->id;
			}
			$leadData = array(
				"name" => $request->contact_name,
				"email" => $request->email_id,
				"phone" => $request->contact_no,
				"interested_in" => 3, //3 = Hotel
				"destination_city_id" => isset($request->destination_city_id)?$request->destination_city_id:'',
				"deprature_date" => date("Y-m-d", strtotime($request->deprature_date)),
				"number_of_nights" => $request->number_of_nights,
				"return_date" => date("Y-m-d", strtotime($request->return_date)),
				// "adult" => $request->number_of_guests,
				"adult" => $request->adults,
				"child" => $request->child,
				"infant" => $request->infants,
				"notes" => $request->notes,
				"status_id" => 1, //1 = New
				"lead_source" => 1, // 1 = Web
				"customer_id" => $customer_id,
				"customer_type_id" => 9

			);
			$lead_id = $this->getLeadLib()->addLead($leadData);
			$lead = $this->getLeadLib()->getLeadById($lead_id, ['lead_id']);
			$rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "Inquiry created successfully. Your inquiry ref. no is ".$lead->lead_id.".";
		}
		return response()->json($return);
	}
	
	public function visa_data_store(Request $request) {
		$rules = [
			'where_go' => 'required|string|max:255',
			'where_go_id' => 'required|string|max:255',
			'where_live' => 'required|string|max:255',
			'where_live_id' => 'required|string|max:255',
			'travel_date' => 'required|string|max:255',
			'duration_days' => 'required|string|max:255',
			'number_of_entries' => 'required|string|max:255',
			'visa_type' => 'required|string|max:255',
			'adults' => 'required|string|max:255',
			'contact_name' => 'required|string|max:255',
			'contact_no' => 'required|string|max:255',
			'email_id' => 'required|string|email|max:255',
		];
		$validator = Validator::make($request->all(), $rules);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {
			$user = $this->getUserLib()->getUserByEmailOrPhone($request->email_id, $request->contact_no);
			if (!empty($user->id)) {
				$customer_id = $user->id;
			} else {
				$name_arr = explode(" ", $request->contact_name);
				$last_name = "";
				foreach ($name_arr as $key => $value) {
					if ($key == 0) {
						$first_name = $value;
					} else {
						$last_name .= " " . $value;
					}
				}
				$userData = array(
					"first_name" => $first_name,
					"last_name" => trim($last_name, " "),
					"email" => $request->email_id,
					"password"  => Hash::make($request->contact_no),
					"role_id" => 5,
					'status'  => 1,
					'is_new_password' => 1,
					'forgot_token' => str_random(10),
				);
				$customer_id = $this->getUserLib()->addUser($userData);
				$profileData = array();
				$profileData['user_id'] = $customer_id;
				$profileData['phone_no'] = $request->contact_no;
				$this->getUserprofileLib()->addUserprofile($profileData);
				$this->send_mail_to_user($customer_id);
			}
			if(\Auth::check()){
				$customer_id=\Auth::user()->id;
			}
			$leadData = array(
				"name" => $request->contact_name,
				"email" => $request->email_id,
				"phone" => $request->contact_no,
				"interested_in" => 8, //8 = Visa
				"from_city" => isset($request->where_live_id)?$request->where_live_id:'',
				"to_city" => isset($request->where_go_id)?$request->where_go_id:'',
				"deprature_date" => date("Y-m-d", strtotime($request->travel_date)),
				"duration_days" => $request->duration_days,
				"number_of_entries" => $request->number_of_entries,
				"visa_type" => $request->visa_type,
				"adult" => $request->adults,
				"child" => $request->child,
				"infant" => $request->infants,
				"notes" => $request->notes,
				"status_id" => 1, //1 = New
				"lead_source" => 1, // 1 = Web
				"customer_id" => $customer_id,
				"customer_type_id" => 9

			);
			$lead_id = $this->getLeadLib()->addLead($leadData);
			$lead = $this->getLeadLib()->getLeadById($lead_id, ['lead_id']);
			$rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "Inquiry created successfully. Your inquiry ref. no is ".$lead->lead_id.".";
		}
		return response()->json($return);
	}
	
	public function holiday_data_store(Request $request) {
		// $rules = [
		// 	'departure_city' => 'required|string|max:255',
		// 	'departure_city_id' => 'required|string|max:255',
		// 	'check_in_date' => 'required|string|max:255',
		// 	'travel_type' => 'required|string|max:255',
		// 	'adults' => 'required|string|max:255',
		// 	'contact_name' => 'required|string|max:255',
		// 	'contact_no' => 'required|string|max:255',
		// 	'email_id' => 'required|string|email|max:255',
		// ];
		// $validator = Validator::make($request->all(), $rules);

		$rules = [
			'departure_city' 		=> 'required|string|max:255',
		    'departure_city_id'     => 'required',
		    'check_in_date'       	=> 'required',
		    'travel_type'       	=> 'required|string|max:255',
		    'adults'       			=> 'required|string|max:255',
		    'contact_name'       	=> 'required|string|max:255',
		    'contact_no'       		=> 'required|string|max:255',
		    'email_id'       		=> 'required|string|email|max:255',
		    'destination.city_name.*' 		  => 'required',
			'destination.city_id.*' 		  => 'required',
			'destination.number_of_nights.*'  => 'required',
		    "activiti_details.'city'.*"       => 'required',
		    "activiti_details.'activiti'.*"   => 'required',
			
		];

		$messages = [
			'departure_city.required'      		=> 'The departure city field is required.',
		    'departure_city_id.required'      	=> 'The departure city id field is required.',
		    'check_in_date.required'      		=> 'The interested date field is required.',
		    'travel_type.required'      		=> 'The travel type field is required.',
		    'adults.required'      				=> 'The adults field is required.',
		    'contact_name.required'      		=> 'The contact name field is required.',
		    'contact_no.required'      			=> 'The contact no field is required.',
		    'email_id.required'      			=> 'The email field is required.',
		    'destination.city_name.*.required'        => 'The destination city field is required.',
			'destination.city_id.*.required'      	  => 'The destination city id field is required.',
			'destination.number_of_nights.*.required' => 'The destination number of night field is required.',
		    "activiti_details.'city'.*"      		  => 'The Activity country is required.',
		    "activiti_details.'activiti'.*"           => 'The country activity field is required.',
		];

		$validator = Validator::make($request->all(), $rules, $messages);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {
			$user = $this->getUserLib()->getUserByEmailOrPhone($request->email_id, $request->contact_no);
			if (!empty($user->id)) {
				$customer_id = $user->id;
			} else {
				$name_arr = explode(" ", $request->contact_name);
				$last_name = "";
				foreach ($name_arr as $key => $value) {
					if ($key == 0) {
						$first_name = $value;
					} else {
						$last_name .= " " . $value;
					}
				}
				$userData = array(
					"first_name" => $first_name,
					"last_name" => trim($last_name, " "),
					"email" => $request->email_id,
					"password"  => Hash::make($request->contact_no),
					"role_id" => 5,
					'status'  => 1,
					'is_new_password' => 1,
					'forgot_token' => str_random(10),
				);
				$customer_id = $this->getUserLib()->addUser($userData);
				$profileData = array();
				$profileData['user_id'] = $customer_id;
				$profileData['phone_no'] = $request->contact_no;
				$this->getUserprofileLib()->addUserprofile($profileData);
				$this->send_mail_to_user($customer_id);
			}
			if(\Auth::check()){
				$customer_id=\Auth::user()->id;
			}
			$leadData = array(
				"name" => $request->contact_name,
				"email" => $request->email_id,
				"phone" => $request->contact_no,
				"interested_in" => 7, //7 = Holiday
				"departure_city_id" => isset($request->departure_city_id)?$request->departure_city_id:'',
				"travel_type" => $request->travel_type,
				"check_in_date" => date("Y-m-d", strtotime($request->check_in_date)),
				"adult" => $request->adults,
				"child" => $request->child,
				"infant" => $request->infants,
				"notes" => $request->notes,
				"status_id" => 1, //1 = New
				"lead_source" => 1, // 1 = Web
				"customer_id" => $customer_id,
				"customer_type_id" => 9

			);
			$lead_id = $this->getLeadLib()->addLead($leadData);
			if(!empty($request->destination) && count($request->destination) > 0) {
				for ($i=0; $i < count($request->destination['city_name']) ; $i++) { 
						$destinationData = array(
							"lead_id" => isset($lead_id)?$lead_id:'',
							"destination_city_id" => isset($request->destination['city_id'][$i])?$request->destination['city_id'][$i]:'',
							"number_of_nights" => isset($request->destination['number_of_nights'][$i])?$request->destination['number_of_nights'][$i]:''
						);
						$this->getHolidaydestinationdetailsLib()->addHolidaydestinationdetails($destinationData);
				}
			}

			if(!empty($request->activiti_details) && count($request->activiti_details) > 0) {
				for ($i=0; $i < count($request->activiti_details["'city'"]) ; $i++) { 
					if($request->activiti_details["'city'"][$i] != '' &&  $request->activiti_details["'activiti'"][$i] != ''){
						$data_activity = array(
							"lead_id"    => isset($lead_id)?$lead_id:'',
							"country_id"  => isset($request->activiti_details["'city'"][$i])?$request->activiti_details["'city'"][$i]:'',
							"activiti_id" => isset($request->activiti_details["'activiti'"][$i])?$request->activiti_details["'activiti'"][$i]:''
						);
						$this->getHolidayactivitiesLib()->addHolidayactivities($data_activity);
					}
				}
			}
			$lead = $this->getLeadLib()->getLeadById($lead_id, ['lead_id']);
			$rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "Inquiry created successfully. Your inquiry ref. no is ".$lead->lead_id.".";
		}
		return response()->json($return);
	}

	public function flight_data_edit(Request $request) {
		$rules = [
			'flight_type' => 'required|string|max:255',
			'from_city' => 'required|string|max:255',
			'from_city_id' => 'required|string|max:255',
			'to_city' => 'required|string|max:255',
			'to_city_id' => 'required|string|max:255',
			'flight_class' => 'required|string|max:255',
			'deprature_date' => 'required|string|max:255',
			'return_date' => 'required|string|max:255',
			'adults' => 'required|string|max:255',
			'contact_name' => 'required|string|max:255',
			'contact_no' => 'required|string|max:255',
			'email_id' => 'required|string|email|max:255',
		];
		$validator = Validator::make($request->all(), $rules);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {

			$leadData = array(
				"name" 				=> $request->contact_name,
				"email" 			=> $request->email_id,
				"phone" 			=> $request->contact_no,
				"flight_type" 		=> $request->flight_type,
				"from_city" 		=> isset($request->from_city_id)?$request->from_city_id:'',
				"to_city" 			=> isset($request->to_city_id)?$request->to_city_id:'',
				"flight_class" 		=> $request->flight_class,
				"deprature_date" 	=> date("Y-m-d", strtotime($request->deprature_date)),
				"return_date" 		=> date("Y-m-d", strtotime($request->return_date)),
				"adult" 			=> $request->adults,
				"child" 			=> $request->child,
				"infant" 			=> $request->infants,
				"notes" 			=> $request->notes,
				"customer_type_id" => 9
			);
			$lead_id = $this->getLeadLib()->updateLead($leadData,$request->id);
			//$return['success'] = true;
			//$return['msg'] = "Inquiry updated successfully";

			$lead = $this->getLeadLib()->getLeadById($request->id, ['lead_id']);
			// $rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "Inquiry updated successfully";
		}
		return response()->json($return);
	}

	public function hotel_data_edit(Request $request) {
		// dd(array(
		// 		'Destination City'		=>$request->destination_city,
		// 		'Destination City Id'	=>$request->destination_city_id,
		// 		'Deprature Date'		=>$request->deprature_date,
		// 		'Return Date'			=>$request->return_date,
		// 		'Number of guests'		=>$request->number_of_guests,
		// 		'Contact Name'			=>$request->contact_name,
		// 		'Contact No'			=>$request->contact_no,
		// 		'Email'					=>$request->email_id
		// ));
		$rules = [
			'destination_city' => 'required|string|max:255',
			'destination_city_id' => 'required|string|max:255',
			'deprature_date' => 'required|string|max:255',
			'return_date' => 'required|string|max:255',
			'adults' => 'required|string|max:255',
			'contact_name' => 'required|string|max:255',
			'contact_no' => 'required|string|max:255',
			'email_id' => 'required|string|email|max:255',
		];
		$validator = Validator::make($request->all(), $rules);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {
			$leadData = array(
				"name" 					=> $request->contact_name,
				"email" 				=> $request->email_id,
				"phone" 				=> $request->contact_no,
				"destination_city_id" 	=> isset($request->destination_city_id)?$request->destination_city_id:'',
				"deprature_date" 		=> date("Y-m-d", strtotime($request->deprature_date)),
				"return_date" 			=> date("Y-m-d", strtotime($request->return_date)),
				"adult" 			    => $request->adults,
				"child" 			    => $request->child,
				"infant" 			    => $request->infants,
				"notes" 				=> $request->notes
			);
			$lead_id = $this->getLeadLib()->updateLead($leadData,$request->id);
			//$return['success'] = true;
			//$return['msg'] = "Inquiry updated successfully";

			$lead = $this->getLeadLib()->getLeadById($request->id, ['lead_id']);
			// $rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "Inquiry updated successfully";
		}
		return response()->json($return);
	}

	public function holiday_data_edit(Request $request) {
		// dd(array(
		// 		'lead_id'				=>$request->id,
		// 		'Departure City'		=>$request->departure_city,
		// 		'Departure City Id'		=>$request->departure_city_id,
		// 		'Check In Date'			=>$request->check_in_date,
		// 		'Travel Type'			=>$request->travel_type,
		// 		'Adults'				=>$request->adults,
		// 		'Contact Name'			=>$request->contact_name,
		// 		'Contact No'			=>$request->contact_no,
		// 		'Email'					=>$request->email_id
		// ));
		//dd($request->destination);
		// $rules = [
		// 	'departure_city' => 'required|string|max:255',
		// 	'departure_city_id' => 'required|string|max:255',
		// 	'check_in_date' => 'required|string|max:255',
		// 	'travel_type' => 'required|string|max:255',
		// 	'adults' => 'required|string|max:255',
		// 	'contact_name' => 'required|string|max:255',
		// 	'contact_no' => 'required|string|max:255',
		// 	'email_id' => 'required|string|email|max:255',
		// ];
		// $validator = Validator::make($request->all(), $rules);

		$rules = [
			'departure_city' 		=> 'required|string|max:255',
		    'departure_city_id'     => 'required',
		    'check_in_date'       	=> 'required',
		    'travel_type'       	=> 'required|string|max:255',
		    'adults'       			=> 'required|string|max:255',
		    'contact_name'       	=> 'required|string|max:255',
		    'contact_no'       		=> 'required|string|max:255',
		    'email_id'       		=> 'required|string|email|max:255',
		    'destination.city_name.*' 		  => 'required',
			'destination.city_id.*' 		  => 'required',
			'destination.number_of_nights.*'  => 'required',
		    "activiti_details.'city'.*"       => 'required',
		    "activiti_details.'activiti'.*"   => 'required',
		];

		$messages = [
			'departure_city.required'      		=> 'The departure city field is required.',
		    'departure_city_id.required'      	=> 'The departure city id field is required.',
		    'check_in_date.required'      		=> 'The interested date field is required.',
		    'travel_type.required'      		=> 'The travel type field is required.',
		    'adults.required'      				=> 'The adults field is required.',
		    'contact_name.required'      		=> 'The contact name field is required.',
		    'contact_no.required'      			=> 'The contact no field is required.',
		    'email_id.required'      			=> 'The email field is required.',
		    'destination.city_name.*.required'        => 'The destination city field is required.',
			'destination.city_id.*.required'      	  => 'The destination city id field is required.',
			'destination.number_of_nights.*.required' => 'The destination number of night field is required.',
		    "activiti_details.'city'.*"      		  => 'The Activity country is required.',
		    "activiti_details.'activiti'.*"           => 'The country activity field is required.',
		];

		$validator = Validator::make($request->all(), $rules, $messages);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {
			$leadData = array(
				"name" 				=> $request->contact_name,
				"email" 			=> $request->email_id,
				"phone" 			=> $request->contact_no,
				"departure_city_id" => isset($request->departure_city_id)?$request->departure_city_id:'',
				"travel_type" 		=> $request->travel_type,
				"check_in_date" 	=> date("Y-m-d", strtotime($request->check_in_date)),
				"adult" 			=> $request->adults,
				"child" 			=> $request->child,
				"infant" 			=> $request->infants,
				"notes" 			=> $request->notes
			);
			
			$lead_id = $this->getLeadLib()->updateLead($leadData,$request->id);
			$this->getHolidaydestinationdetailsLib()->deleteHolidaydestinationdetails_lead(['lead_id'=>$request->id]);
			if(!empty($request->destination) && count($request->destination) > 0) {

				for ($i=0; $i < count($request->destination['city_name']) ; $i++) { 
					$destinationData = array(
						"lead_id"    => isset($request->id)?$request->id:'',
						"destination_city_id" => isset($request->destination['city_id'][$i])?$request->destination['city_id'][$i]:'',
						"number_of_nights" => isset($request->destination['number_of_nights'][$i])?$request->destination['number_of_nights'][$i]:''
					);
					$this->getHolidaydestinationdetailsLib()->addHolidaydestinationdetails($destinationData);
				}
			}

			$this->getHolidayactivitiesLib()->deleteHolidayactivities_lead(['lead_id'=>$request->id]);
			if(!empty($request->activiti_details) && count($request->activiti_details) > 0) {
				for ($i=0; $i < count($request->activiti_details["'city'"]) ; $i++) { 
					if($request->activiti_details["'city'"][$i] != '' &&  $request->activiti_details["'activiti'"][$i] != ''){
						$data_activity = array(
							"lead_id"    => isset($request->id)?$request->id:'',
							"country_id"  => isset($request->activiti_details["'city'"][$i])?$request->activiti_details["'city'"][$i]:'',
							"activiti_id" => isset($request->activiti_details["'activiti'"][$i])?$request->activiti_details["'activiti'"][$i]:''
						);
						$this->getHolidayactivitiesLib()->addHolidayactivities($data_activity);
					}
				}
			}
			
			$lead = $this->getLeadLib()->getLeadById($request->id, ['lead_id']);
			// $rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "Inquiry updated successfully";
			//$return['msg'] = "Inquiry updated successfully";
		}
		return response()->json($return);
	}

	public function visa_data_edit(Request $request) {
		// dd(array(
		// 	'lead_id'				=>$request->id,
		// 	'where_go'				=>$request->where_go,
		// 	'where_go_id'			=>$request->where_go_id,
		// 	'where_live'			=>$request->where_live,
		// 	'where_live_id'			=>$request->where_live_id,
		// 	'travel_date'			=>$request->travel_date,
		// 	'duration_days'			=>$request->duration_days,
		// 	'number_of_entries'		=>$request->number_of_entries,
		// 	'visa_type'				=>$request->visa_type,
		// 	'adults'				=>$request->adults,
		// 	'Contact Name'			=>$request->contact_name,
		// 	'Contact No'			=>$request->contact_no,
		// 	'Email'					=>$request->email_id
		// ));
		$rules = [
			'where_go' 			=> 'required|string|max:255',
			'where_go_id' 		=> 'required|string|max:255',
			'where_live' 		=> 'required|string|max:255',
			'where_live_id' 	=> 'required|string|max:255',
			'travel_date' 		=> 'required|string|max:255',
			'duration_days' 	=> 'required|string|max:255',
			'number_of_entries' => 'required|string|max:255',
			'visa_type' 		=> 'required|string|max:255',
			'adults' 			=> 'required|string|max:255',
			'contact_name' 		=> 'required|string|max:255',
			'contact_no' 		=> 'required|string|max:255',
			'email_id' 			=> 'required|string|email|max:255',
		];
		$validator = Validator::make($request->all(), $rules);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {
			$leadData = array(
				"name" 				=> $request->contact_name,
				"email" 			=> $request->email_id,
				"phone" 			=> $request->contact_no,
				"from_city" 		=> isset($request->where_live_id)?$request->where_live_id:'',
				"to_city" 			=> isset($request->where_go_id)?$request->where_go_id:'',
				"deprature_date" 	=> date("Y-m-d", strtotime($request->travel_date)),
				"duration_days" 	=> $request->duration_days,
				"number_of_entries" => $request->number_of_entries,
				"visa_type" 		=> $request->visa_type,
				"adult" 			=> $request->adults,
				"child" 			=> $request->child,
				"infant" 			=> $request->infants,
				"notes" 			=> $request->notes
			);
			// $lead_id = $this->getLeadLib()->updateLead($leadData,$request->id);
			// $return['success'] = true;
			// $return['msg'] = "Inquiry updated successfully";
			$lead_id = $this->getLeadLib()->updateLead($leadData,$request->id);
			$lead = $this->getLeadLib()->getLeadById($request->id, ['lead_id']);
			// $rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "Inquiry updated successfully";
		}
		return response()->json($return);
	}

	public function inquiry_now($tourId, $tourName,$packageType=1) {
		// $where_arr = ['tour.id' => $tourId,'tour.tour_status'=>1];
		// $holiday = $this->getTourLib()->getinquiryToursDetails(array('tour.*'), $where_arr);
		$data = array();
		Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
		});
		Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
		});
		Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
		});
		Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
			return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
		});
		Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
            return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
	    });
		$data['domestic_sectors'] = Cache::pull('domestic_sectors');
		$data['global_sectors'] = Cache::pull('global_sectors');
		$data['pilgrimage_sectors'] = Cache::pull('pilgrimage_sectors');
		$data['SpecialtyTours'] = Cache::pull('SpecialtyTours');
		$data['CruiseSections'] = Cache::pull('CruiseSections');
		if(isset(\Auth::user()->id) && \Auth::user()->id > 0){
			$data['user_data'] = $this->getUserLib()->getUserFullProfile(\Auth::user()->id);
		}
		$holiday = $this->getTourLib()->get_tour_data($tourId);
		if(!empty($holiday)){
			$data['holiday'] = $holiday;
			$cost = $this->getTourCostLib()->getCurrentTourCost(array('tour_cost.id', 'tour_cost.from_date', 'tour_cost.to_date', 'tour_cost.discount', 'currency_master.currency_name', 'currency_master.currency_value', 'currency_master.currency_icon'), array('tour_id' => $holiday->id));
			$hotel = $this->getTourHotelLib()->getHotelByTour(array('tour_hotel.location', 'tour_hotel.hotel', 'tour_hotel.hotel_star', 'tour_hotel.night', 'master_tour_package_type.name', 
				'tour_hotel.package_type', 'tour_hotel.id'), array('tour_hotel.tour_id' => $holiday->id));
			if(!empty($cost)){
				$data['cost'] = $cost;
			}else{
				$data['cost'] = array();
			}
			if(!empty($hotel)){
				$data['hotel'] = $hotel;
			}else{
				$data['hotel'] = array();
			}
		}else{
			$data['holiday'] = array();
		}

		$cities_arr = array();
		$data['cities'] = json_encode($cities_arr);

		return view('client/book-now', $data);
    }

    public function book_store(Request $request) {
    	//dd($request->tour_id);
    	$rules = [
			'adults' 		=> 'required|string|max:255',
            'interested_date' => 'required|string|max:255',
            'hotel_category' => 'required|string|max:255',
			'contact_name' 	=> 'required|string|max:255',
			'contact_no' 	=> 'required|string|max:255',
			'email_id' 		=> 'required|string|email|max:255',
		];
		$validator = Validator::make($request->all(), $rules);
		$return = array();
		$return['success'] = true;
		if ($validator->fails()) {
			$errors = $validator->errors(); //here's the magic
			$return['errors'] = array();
			foreach ($rules as $key => $value) {
				if ($errors->has($key)) { //checks whether that input has an error.
					$return['errors'][] = [$key, $errors->first($key)];
				}
			}
			$return['success'] = false;
		} else {
			$user = $this->getUserLib()->getUserByEmailOrPhone($request->email_id, $request->contact_no);
			if (!empty($user->id)) {
				$customer_id = $user->id;
			} else {
				$name_arr = explode(" ", $request->contact_name);
				$last_name = "";
				foreach ($name_arr as $key => $value) {
					if ($key == 0) {
						$first_name = $value;
					} else {
						$last_name .= " " . $value;
					}
				}
				$userData = array(
					"first_name" => $first_name,
					"last_name" => trim($last_name, " "),
					"email" => $request->email_id,
					"password"  => Hash::make($request->contact_no),
					"role_id" => 5,
					'status'  => 1,
					'is_new_password' => 1,
					'forgot_token' => str_random(10),
				);
				$customer_id = $this->getUserLib()->addUser($userData);
				$profileData = array();
				$profileData['user_id'] = $customer_id;
				$profileData['phone_no'] = $request->contact_no;
				$this->getUserprofileLib()->addUserprofile($profileData);
				$this->send_mail_to_user($customer_id);
			}
			if(\Auth::check()){
				$customer_id=\Auth::user()->id;
			}
			$leadData = array(
				"name" 			=> $request->contact_name,
				"email" 		=> $request->email_id,
				"phone" 		=> $request->contact_no,
				"adult" 		=> $request->adults,
				"child" 		=> $request->child,
				"infant" 		=> $request->infants,
				"notes" 		=> $request->notes,
				"tour_id" 		=> $request->tour_id,
                            "interested_date" 		=> date("Y-m-d", strtotime($request->interested_date)),
				"interested_in" => 4, //4 = tour
				"status_id" 	=> 1, //1 = New
				"lead_source" 	=> 1, // 1 = Web
				"hotel_category"=>$request->hotel_category,
				"customer_id" 	=> $customer_id,
			);
			$lead_id = $this->getLeadLib()->addLead($leadData);
			$lead = $this->getLeadLib()->getLeadById($lead_id, ['lead_id']);
			$rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
			$return['success'] = true;
			$return['msg'] = "successfully booked. Your inquiry ref. no is <strong>".$lead->lead_id."</strong>.";
		}
		return response()->json($return);
	}
    
    public function book_data_edit(Request $request) {
        $rules = [
            'adults' => 'required|string|max:255',
            'interested_date' => 'required|string|max:255',
            'hotel_category' => 'required|string|max:255',
            'contact_name' => 'required|string|max:255',
            'contact_no' => 'required|string|max:255',
            'email_id' => 'required|string|email|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        $return = array();
        $return['success'] = true;
        if ($validator->fails()) {
            $errors = $validator->errors(); //here's the magic
            $return['errors'] = array();
            foreach ($rules as $key => $value) {
                if ($errors->has($key)) { //checks whether that input has an error.
                    $return['errors'][] = [$key, $errors->first($key)];
                }
            }
            $return['success'] = false;
        } else {
            $leadData = array(
                "name" => $request->contact_name,
                "email" => $request->email_id,
                "phone" => $request->contact_no,
                "adult" => $request->adults,
                "child" => $request->child,
                "infant" => $request->infants,
                "notes" => $request->notes,
                "hotel_category"=>$request->hotel_category,
                "interested_date" => date("Y-m-d", strtotime($request->interested_date))
            );
            $lead_id = $this->getLeadLib()->updateLead($leadData, $request->id);
            $lead = $this->getLeadLib()->getLeadById($request->id, ['lead_id']);
            $rs = LaravelMsg91::message($request->contact_no, "Thank your interest in Low Cost Trip. Your inquiry ref. no is {$lead->lead_id}. We will get back to you soon.");
            $return['success'] = true;
            $return['msg'] = "Booking updated successfully";
        }
        return response()->json($return);
    }

    public function get_activiti_html() {  
    	$data['countrys'] = $this->getCountryLib()->getCountry(array('country_id', 'country_name'));
		$view = View::make('client/activites-html', $data);
    	$return['html'] = $view->render();
    	$return['success'] = true;
    	return response()->json($return);
    }

    public function get_activiti_by_city(Request $request){
        $return['activities'] = $this->getBookActivitiesLib()->getActivitiesByCity($request->id); 
        $return['success'] = true;
        return response()->json($return);
    }

    public function send_mail_to_user($user_id){
    	$data['user_info'] = $this->getUserLib()->getUserById($user_id);
    	$data['user_profile'] = $this->getUserprofileLib()->getUserprofileById($user_id);
    	$d = array('email' => $data['user_info']->email );
        Mail::send('client/register-template-add-lead', $data, function($message)  use ($d )  {
             $message->to($d['email'], 'Low Cost Trip')->subject
                ('Thank you for choosing low cost trip');
             $message->from('info@lowcosttrip.com','Low Cost Trip');
        });
    }

    public function flight_booking(Request $request) {
    	//dd(route('flight.booking'));
		\Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
		Cache::remember('domestic_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 1, 'status' => 1));
		});
		Cache::remember('global_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 2, 'status' => 1));
		});
		Cache::remember('pilgrimage_sectors', env('CACHE_LIIMIT'), function () {
			return $this->getSectorLib()->getSector(array('id', 'name'), array('category_id' => 3, 'status' => 1));
		});
		Cache::remember('SpecialtyTours', env('CACHE_LIIMIT'), function () {
			return $this->getTourtypemasterLib()->getTourtype(array('typeId', 'name','logo'), array('status' => 1));
		});
		Cache::remember('CruiseSections', env('CACHE_LIIMIT'), function () {
			return $this->getTourLib()->getCruiseHolidays(array('id', 'tour_code','tour_name','image'),array('tour_status' => 1, 'public_private' => 1,'deleted_at'=>null),12);
		});
		$data['domestic_sectors'] 		= Cache::pull('domestic_sectors');
		$data['global_sectors'] 		= Cache::pull('global_sectors');
		$data['pilgrimage_sectors'] 	= Cache::pull('pilgrimage_sectors');
		$data['SpecialtyTours'] 		= Cache::pull('SpecialtyTours');
		$data['CruiseSections'] 		= Cache::pull('CruiseSections');
		$data['travel_type'] 			= $this->getCommonLib()->getLeadCustomizedOpt();
		$data['city'] 					= $this->getCityLib()->getCity();
		if(isset(\Auth::user()->id) && \Auth::user()->id > 0){
			$data['user_data'] 			= $this->getUserLib()->getUserFullProfile(\Auth::user()->id);
		}

		$cities_arr = array();
		$cities = $this->getCityLib()->getCity(['city_id', 'city_name']);
		foreach ($cities as $key => $value) {
			$cities_arr[] 				= array('value' => $value->city_id, "label" => $value->city_name);
		}
		$data['cities'] 				= json_encode($cities_arr);

		$data['wallet_user_data'] 		= '';
		if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
			$data['wallet_user_data'] 	= $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
		}
		
		return view('client/customise-flight-book', $data);
    }

    public function flight_book_submit(Request $request) {
    	$result = array();
	    // Validate parameter
	    if(isset($request->from) && !empty($request->from) &&
	       isset($request->to) && !empty($request->to) &&
	       isset($request->depart_date) && !empty($request->depart_date) &&       
	       isset($request->class) && !empty($request->class) &&
	       isset($request->adults) && isset($request->child) && isset($request->infants)){   
	    	// echo $request->from.'<br>'; echo $request->to.'<br>'; echo $request->depart_date.'<br>'; echo $request->class.'<br>'; echo $request->adults.'<br>'; echo $request->child.'<br>'; echo $request->infants.'<br>';die;
	    	if((isset($request->is_wallet) && $request->is_wallet=='on') && (isset($request->wallet_amount) && $request->wallet_amount!='0')){
	            \Session::put(['is_status'=>1,'is_wallet' => $request->is_wallet,'wallet_amount'=>$request->wallet_amount]);  
	        }

	        $sessionID = $this->getSessionId();        
	        
	        if(empty($sessionID)){
	            $result['error'] 	= '101';
	            $result['message'] 	= 'Something happen bad';            
	            echo json_encode($result);
	            exit;
	        }

	        $departure_city 	= $request->from;
	        $arrival_city 		= $request->to;
	        $departure_date 	= date("d/m/Y",strtotime($request->depart_date));        
	        $class 				= $request->class;
	        $adults 			= $request->adults;
	        $child 				= $request->child;
	        $infants 			= $request->infants;
	        
			// Build the request json string
			// Search flights         
			$requestJsonBody = '{
				"Data": {
					"IsMobileSearchQuery": "N",
					"MaxOptionsCount": "10",
					"TravelType": "D",
					"JourneyType": "O",
					"IsPersonalBooking": "N",
					"AirOriginDestinations": [{
						"DepartureAirport": "'.$departure_city.'",
						"ArrivalAirport": "'.$arrival_city.'",
						"DepartureDate": "'.$departure_date.'"
					}],
					"AirPassengerQuantities": {
						"NumAdults": '.$adults.',
						"NumChildren": '.$child.',
						"NumInfants": '.$infants.'
					},
					"AirSearchPreferences": {
						"BookingClass": "'.$class.'",
						"Carrier": "ANY"
					},
					"SearchLevelReportingParams": {
						"BillingEntityCode": 0
					}
				}
			}';

			$url = "https://".env('FLIGHT_URL')."/restwebsvc/air/searchV1";
	        
	       // Set headers 
	        $headers = array(            
	            "Content-type:application/json",
	            "Accept: application/json",
	            "Content-Encoding:gzip",
	            "Accept-Encoding:zip",
	            "Cookie: JSESSIONID=".$sessionID
	        ); 
	        
	        $search_result = $this->sendRequest($url,$headers,$requestJsonBody);

	        if(isset($search_result->Status) && $search_result->Status=="Success"){                        
	            if(isset($search_result->Data->AirSearchResult)){                
	                $flightsData = $search_result->Data->AirSearchResult;
	                $result['SearchFormData'] = $search_result->Data->SearchFormData;
	                foreach($flightsData as $i=>$row){ 
	                	// print_r(array_keys($row->OptionSegmentsInfo));die; 
	                    $last_option = 0;                       
	               		 // dd($last_option);                  
	                    
	                    $result['AirSearchResult'][$i]['OptionId'] 			= $row->OptionId;
	                    $result['AirSearchResult'][$i]['name'] 				= $this->getOperatingAirline($row->OptionSegmentsInfo);
	                    $result['AirSearchResult'][$i]['FlightNumber'] 		= $row->OptionSegmentsInfo[0]->FlightNumber;
	                    $result['AirSearchResult'][$i]['CarrierCode'] 		= $row->OptionSegmentsInfo[0]->CarrierCode;

	                    // Departure Info
	                    $result['AirSearchResult'][$i]['DepartureAirport'] 	= $row->OptionSegmentsInfo[0]->DepartureAirport;
	                    $result['AirSearchResult'][$i]['DepartureLocation'] = $row->OptionSegmentsInfo[0]->DepartureLocation->Name;                                        
	                    $DepartureTime 										= str_replace("/", "-",$row->OptionSegmentsInfo[0]->DepartureTime);
	                    $result['AirSearchResult'][$i]['DepartureTime'] 	= date('H:i',strtotime($DepartureTime));                    

	                    // Arriaval Info                    
	                    $ArrivalTime 										= str_replace("/", "-",$row->OptionSegmentsInfo[$last_option]->ArrivalTime);
	                    $result['AirSearchResult'][$i]['ArrivalTime'] 		= date('H:i',strtotime($ArrivalTime)); 
	                    $result['AirSearchResult'][$i]['ArrivalAirport'] 	= $row->OptionSegmentsInfo[$last_option]->ArrivalAirport;
	                    $result['AirSearchResult'][$i]['ArrivalLocation'] 	= $row->OptionSegmentsInfo[$last_option]->ArrivalLocation->Name;
	                    
	                    $count_stop 										=( isset($row->OptionSegmentsInfo[0]->StopoverCodes)  ? (count($row->OptionSegmentsInfo[0]->StopoverCodes)) : 0);
	                    $result['AirSearchResult'][$i]['StopoverCodes'] 	= $count_stop == 0 ? "Non" : $count_stop ;
	                    
	                    $minutes 											= $row->OptionSegmentsInfo[0]->JourneyTime;
	                    $hours 												= floor($minutes / 60);
	                    $min 												= $minutes - ($hours * 60);                    
	                    $result['AirSearchResult'][$i]['JourneyTime'] 		= $hours."H ".$min.'m';
	                    
	                    $result['AirSearchResult'][$i]['price'] 			= $row->OptionPriceInfo->TotalPrice;
	                }
	            }
	        }
	        $SearchFormData 		= $result['SearchFormData'];
	        $response['html_data'] 	= View::make('client/customise-flight-serch-result', compact('result', 'SearchFormData'))->render();
	        $response['success'] 	= true;
	        echo json_encode($response);
	    }else{
	        $response['error'] 		= '100';
	        $response['message'] 	= 'Something happen bad';            
	        echo json_encode($response);
	        exit;        
	    }
	    // End IF
    }

    public function flight_detsils(Request $request) {
    	$result = array();
	    $response = array();
	    // Validate parameter
	    if(isset($request->id) && isset($request->fromdata) && !empty($request->fromdata)){        
	        $sessionID 	=  $this->getSessionId();        
	        if(empty($sessionID)){
	            $result['error'] 	= '101';
	            $result['message'] 	= 'Something happen bad';            
	            echo json_encode($result);
	            exit;
	        }
	        $optionId 			= $request->id;
	        $SearchFormData 	= $request->fromdata;
	        // Build the request json string
	        // Reprice
	        $requestJsonBody 	= '{"Data":{"AirOriginDestinationOptions": ['.$optionId.'],"SearchFormData":"'.$SearchFormData.'"}}';
	                   
	        $url = "https://".env('FLIGHT_URL')."/restwebsvc/air/repriceV1";
	        
	       // Set headers 
	        $headers 	= array(            
	            "Content-type:application/json",
	            "Accept: application/json",
	            "Content-Encoding:gzip",
	            "Accept-Encoding:zip",
	            "Cookie: JSESSIONID=".$sessionID
	        ); 
	        
	        $response 	=  $this->sendRequest($url,$headers,$requestJsonBody);

	        $response_data['html_data'] = View::make('client/customise-single-flight-details', compact('response'))->render();
	        $response_data['success'] 	= true;
	        echo json_encode($response_data);
	    }else{
	        $result['error'] 	= '100';
	        $result['message'] 	= 'Something happen bad';            
	        echo json_encode($result);
	        exit;        
	    }
	    // End IF
    }

    public function flight_book_now(Request $request) {
    	$result 	= array();
	    $response 	= array();
	    // Validate parameter
	    if(isset($request->id) && isset($request->fromdata) && !empty($request->fromdata)){        
	        $sessionID = $this->getSessionId();        
	        if(empty($sessionID)){
	            $result['error'] 	= '101';
	            $result['message'] 	= 'Something happen bad';            
	            echo json_encode($result);
	            exit;
	        }
	        $optionId 			= $request->id;
	        $SearchFormData 	= $request->fromdata;
	        // Build the request json string
	        // Reprice
	        $requestJsonBody 	= '{"Data":{"AirOriginDestinationOptions": ['.$optionId.'],"SearchFormData":"'.$SearchFormData.'"}}';
	                   
	        $url = "https://".env('FLIGHT_URL')."/restwebsvc/air/repriceV1";
	        
	       // Set headers 
	        $headers 	= array(            
	            "Content-type:application/json",
	            "Accept: application/json",
	            "Content-Encoding:gzip",
	            "Accept-Encoding:zip",
	            "Cookie: JSESSIONID=".$sessionID
	        ); 
	        $response 	= $this->sendRequest($url,$headers,$requestJsonBody);
	        $result_data['html_data'] 	= View::make('client/customise-flight-details', compact('response'))->render();
	        $result_data['success'] 	= true;
	        echo json_encode($result_data);
	    }else{
	        $result['error'] 	= '100';
	        $result['message'] 	= 'Something happen bad';            
	        echo json_encode($result);
	        exit;        
	    }
	    // End IF
    }

    public function flight_order_now(Request $request) {
    	$result = array();
	    // Validate parameter
	    if( isset($request->SearchFormData) && !empty($request->SearchFormData) &&
	        isset($request->CartData) && !empty($request->CartData) &&
	        isset($request->CartBookingId) && !empty($request->CartBookingId) &&
	        isset($request->email) && !empty($request->email) &&
	        isset($request->mobile) && !empty($request->mobile)){
	        //Use Wallet
	        $pay_amt = str_replace("," , "" , $request->total_price);
	        if($request->session()->has('is_wallet') && $request->session()->has('is_wallet')=='on'){
	        	//dd(Use Wallet);
	        	if($pay_amt<=$request->session()->get('wallet_amount')){
	        		//dd('Max wallet value Min amount');
	        		date_default_timezone_set('Asia/Calcutta');
                    $datenow = date("Y-m-d H:m:s");
                    $transactionId = mt_rand(100000,999999).mt_rand(100000,999999);
                    DB::table('flight_temp_datas')->insertGetId(
					    ['unique_id' => $transactionId, 'value' => json_encode($request->all())]
					);
                    $merchant_transactionId = rand(1, 1000000);
	        		$payment_info = array(
	        		    "customer_id" 				=> isset(\Auth::user()->id)?\Auth::user()->id:'',
	        		    "status" 					=> 'Ok',
	        		    "paid_amount" 				=> (($pay_amt) ? $pay_amt : ''),
	        		    "paid_date" 				=> $datenow,
	        		    "transaction_id" 			=> (($transactionId) ? $transactionId : ''),
	        		    "merchant_transaction_id" 	=> (($merchant_transactionId) ? $merchant_transactionId : ''),
	        		    "user_name" 				=> '-',
	        		    "user_email" 				=> (($request->email) ? $request->email : ''),
	        		    "user_mobile_no" 			=> (($request->mobile) ? $request->mobile : ''),
	        		    "user_address" 				=> 'India',
	        		    "wallet_amount" 			=> (($pay_amt) ? $pay_amt : ''),
	        		    "is_wallet" 				=> ($request->session()->has('is_wallet'))?1:0,
	        		    "is_payment_type"  			=> 2
	        		);
			        if ($payment = $this->getPaymentLib()->addPaymentResponse($payment_info)) {
			        	$flight_temp_datas = DB::table('flight_temp_datas')->where('unique_id', $transactionId)->first();
			            if ($payment_info['status'] == 'Ok') {

			            	if($request->session()->has('is_status')){
			            	    if($request->session()->get('is_status')==1){
			            	        if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
			            	            $wallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
			            	            if(isset($wallet) && $wallet!=''){
			            	                $current_balance=$wallet->current_balance-round($pay_amt);
			            	                $wallet_data = Wallet::create([
			            	                    'user_id'     		=> 	\Auth::user()->id,
			            	                    'debit'      		=> 	round($pay_amt),
			            	                    'current_balance'	=>	$current_balance
			            	                ]);
			            	            }
			            	        }
			            	    }
			            	}

			            	if($this->cashback($pay_amt,1)){
			            		if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
			            		    $lastRecordwallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
			            		    if(isset($lastRecordwallet) && $lastRecordwallet!=''){
			            		        $lastrecord_current_balance=$lastRecordwallet->current_balance+round($this->cashback($pay_amt,1));
			            		        $lastrecord_wallet_data = Wallet::create([
			            		            'user_id'     => \Auth::user()->id,
			            		            'credit'      => round($this->cashback($pay_amt,1)),
			            		            'current_balance'=>$lastrecord_current_balance
			            		        ]);
			            		    }
			            		}
			            	}
			            	
			            	\Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);

							$request_session_obj = json_decode($flight_temp_datas->value);
							$request_session = (array) $request_session_obj;
							$sessionID =  $this->getSessionId();        
						        if(empty($sessionID)){
						            $result['error'] = '101';
						            $result['message'] = 'Something happen bad';            
						            echo json_encode($result);
						            exit;
						        }
						        $SearchFormData = $request_session['SearchFormData'];
						        $CartData = $request_session['CartData'];
						        $CartBookingId = $request_session['CartBookingId'];
						        $email = $request_session['email'];
						        $mobile = $request_session['mobile'];
						        
						        $fistname = (array) $request_session['firstname'];        
						        $lastname = (array) $request_session['lastname'];        
						        $sex 	  = (array) $request_session['sex'];        
						        $dob 	  = (  isset($request_session['dob']) ? (array) $request_session['dob'] : array()) ;   

						        $request_seats 	  = (  isset($request_session['seats']) ? (array) $request_session['seats'] : array()) ;        
						        $request_meals 	  = (  isset($request_session['meals']) ? (array) $request_session['meals'] : array()) ;        
						        $request_buggage 	  = (  isset($request_session['buggage']) ? (array) $request_session['buggage'] : array()) ;

						        $request_set_data['Data']['SearchFormData'] = $SearchFormData;
						        $request_set_data['Data']['CartData'] = $CartData;
						        $request_set_data['Data']['CartBookingId'] = $CartBookingId;
						        
						        $i=0;
						        foreach($fistname as $key => $value){
						            if(strpos($key, 'adults') !== false) {
						                $type='A';
						            }else if(strpos($key, 'child') !== false) {
						                $type='C';
						            }else if(strpos($key, 'infants') !== false) {
						                $type='I';
						            }
						            
						            $request_set_data['Data']['Passengers'][$i]['Type'] = $type;
						            $request_set_data['Data']['Passengers'][$i]['DateOfBirth'] = isset( $dob[$key]) ?  $dob[$key] : "N/A";
						            $request_set_data['Data']['Passengers'][$i]['Title'] = "Mr";
						            $request_set_data['Data']['Passengers'][$i]['FirstName'] = $value;
						            $request_set_data['Data']['Passengers'][$i]['LastName'] = $lastname[$key];            
						            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['SeatPreference'] = isset($request_seats[$key]) ? $request_seats[$key] : "N/A";
						            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['Id'] = "N/A";
						            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['MealPreference'] = isset($request_meals[$key]) ? $request_meals[$key] : "N/A";;
						            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['BuggagePreference'] = isset($request_buggage[$key]) ? $request_buggage[$key] : "N/A";;
						            //$request_set_data['Data']['Passengers'][$i]['Preferences'][0]['SSRPreference'] = "N/A";
						            $request_set_data['Data']['Passengers'][$i]['ReportingParameters'][0]['Id'] = "N/A";
						            $request_set_data['Data']['Passengers'][$i]['ReportingParameters'][0]['Value'] = "N/A";
						            $i++;
						        }
						                
						        $request_set_data['Data']['DeliveryInfo']['Mobile'] = $mobile;
						        $request_set_data['Data']['DeliveryInfo']['Email'] = $email;
						        $request_set_data['Data']['DeliveryInfo']['SendEmail'] = "N";
						        $request_set_data['Data']['DeliveryInfo']['SendSMS'] = "N";
						        
						        $request_set_data['Data']['MetaInfos']['E'] = "0";
						        $request_set_data['Data']['MetaInfos']['M'] = "0";        
						        $request_set_data['Data']['PaymentMethod']['Mode'] = "D";        
						        $requestJsonBody = json_encode($request_set_data); 
						        
						        $url = "https://".env('FLIGHT_URL')."/restwebsvc/air/bookV1";
						        
						       // Set headers 
						        $headers = array(            
						            "Content-type:application/json",
						            "Accept: application/json",
						            "Content-Encoding:gzip",
						            "Accept-Encoding:zip",
						            "Cookie: JSESSIONID=".$sessionID
						        ); 
			        
							    $result_session = $this->sendRequest($url,$headers,$requestJsonBody);
							    
						        //Save Data In DB
						        if( $result_session->Status == 'Success' ){
					        		$books_data= array(
					        			"flight_from" => $request_session['flight_from'], // $request_session['from']
										"flight_to" => $request_session['flight_to'], // $request_session['to']
										"depart_date" => date('Y-m-d', strtotime($request_session['depart_date'])), // $request_session['departdate']
										"passengers_class" => $request_session['passengers_class'], // $request_session['class']
										"adults" =>$request_session['adults'], // $request_session['adults']
										"child" => $request_session['child'], // $request_session['child']
										"infants" => $request_session['infants'], // $request_session['infants']
										"email" => $request_session['email'],
										"mobile" => $request_session['mobile'],
										"base_fare" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->BasePrice),
										"udfcharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->UdfCharge),
										"congestioncharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->CongestionCharge),
										"igstax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->IGSTax),
										"tgsttax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->TGSTTax),
										"airporttax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->AirportTax),
										"fuelSurcharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->FuelSurcharge),
										"grand_total" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->TotalPrice),
										"api_response" =>  json_encode($result_session) ,
										"payment_id" =>  $payment ,
					        		);
					        	  	$flight_book_id = $this->getFlightBooksLib()->addFlightBooks($books_data);

					        	  	foreach ($fistname as $k => $val) {
					        	  		$type = preg_replace('/[0-9]+/', '', $k);
					        	  		$i = (int) filter_var($k, FILTER_SANITIZE_NUMBER_INT);
					        	  		$meals = ''; $meals_price = 0;
					        	  		foreach ($result_session->Data->Cart->OrderItems[0]->Prefrences->MealPrefrences[0]->PrefList as  $PrefList) {
					        	  			if(isset($request_meals[$type.$i])  && $request_meals[$type.$i] == $PrefList->Code ){
					        	  				$meals = $PrefList->Description.' ('.$PrefList->Code.')';
					        	  				$meals_price = $PrefList->Price;
					        	  			}
					        	  		}

					        	  		$seats = ''; $seats_price = 0;
					        	  		foreach ($result_session->Data->Cart->OrderItems[0]->Prefrences->SeatPrefrences[0]->PrefList as  $SeatPre) {
					        	  			if(isset($request_seats[$type.$i])  && $request_seats[$type.$i] == $SeatPre->Code ){
					        	  				$seats = $SeatPre->Description.' ('.$SeatPre->Code.')';
					        	  				$seats_price = $SeatPre->Price;
					        	  			}
					        	  		}
					 
					        	  		$buggage = ''; $buggage_price = 0;
					        	  		foreach ($result_session->Data->Cart->OrderItems[0]->Prefrences->SeatPrefrences[0]->PrefList as  $BuggagePre) {
					        	  			if(isset($request_buggage[$type.$i])  && $request_buggage[$type.$i] == $BuggagePre->Code ){
					        	  				$seats = $BuggagePre->Description.' ('.$BuggagePre->Code.')';
					        	  				$seats_price = $BuggagePre->Price;
					        	  			}
					        	  		}
					 
									 	$passengers = array(
											'flight_books_id' => $flight_book_id ,
										    'passengers_type' => $type,
										    'first_name'      => ( isset($fistname[$type.$i]) ? $fistname[$type.$i] : ''),
										    'last_name'       => ( isset($lastname[$type.$i]) ? $lastname[$type.$i] : ''),
										    'date_of_birth'   => ( isset($dob[$type.$i]) ? date('Y-m-d', strtotime($dob[$type.$i])) : ''),
										    'gender'          => ( isset($sex[$type.$i]) ? $sex[$type.$i] : ''),
										    'meals'           => $meals ,
										    'meals_price'     => $meals_price,
										    'seats'           => $seats,
										    'seats_price'     => $seats_price,
										    'buggage'           => $buggage,
										    'buggage_price'     => $buggage_price 
										);
						        	  	$flight_passengers_id = $this->getFlightPassengersLib()->addFlightPassengers($passengers);
					        	  	}

					    	  		$flights = $result_session->Data->Cart->OrderItems;
					                foreach($flights as $key => $row){                
					                    $item = $row->FlightSegment; 
					                    $item_PriceInfo = $result_session->Data->Cart->OrderItems[0]->PriceInfo;                             
					                    
						                $DepartureTime = str_replace("/", "-",$item->DepartureTime);
						                $ArrivalTime = str_replace("/", "-",$item->ArrivalTime);

						                $minutes = $item->JourneyTime;
						                $hours = floor($minutes / 60);
						                $min = $minutes - ($hours * 60);
						                $count_stop =(  (isset($item->StopoverCodes))  ? count($item->StopoverCodes) : 0);

									 	$passengers = array(
											'flight_books_id' => $flight_book_id ,
										    'flight_name'     =>  $item->OperatingAirline,
										    'flight_number'   => $item->FlightNumber,
										    'carrier_code'    => $item->CarrierCode,
										    'drrival_airport' => $item->DepartureAirport ,
										    'departure_time'  => date("Y-m-d H:i:s",strtotime($DepartureTime)),
										    'aeparture_airport'  => $item->ArrivalAirport,
										    'arrival_time'    => date("Y-m-d H:i:s",strtotime($ArrivalTime)),
										    'time_duration'   => $hours."h ".$min.'m',
										    'flight_type'     => $count_stop == 0 ? "Non stop" : $count_stop.' stop',
										);
						        	  	$flight_passengers_id = $this->getFlightReviewInfoLib()->addFlightReviewInfo($passengers);
						        	}
						        	\Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format($pay_amt, 2) . ' ) has been paid successfully.');
			                		\Session::flash('alert-class', 'alert-success');	
						        }else{
						        	\Session::flash('message', 'Error..! Your payment (Rs. ' . number_format($pay_amt, 2) . ' ) has been paid unsuccessfully.');
			               			\Session::flash('alert-class', 'alert-danger');	
						        }
			                \Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format($pay_amt, 2) . ' ) has been paid successfully.');
			                \Session::flash('alert-class', 'alert-success');
			            }else{
			                \Session::flash('message', 'Error..! Your payment (Rs. ' . number_format($pay_amt, 2) . ' ) has been paid unsuccessfully.');
			                \Session::flash('alert-class', 'alert-danger');
			            }
			        }else{
			            \Session::flash('message', 'Error in payment.');
			            \Session::flash('alert-class', 'alert-danger');
			        }
			        $ajax_data['url'] 		= route('flight.booking');
		            $ajax_data['status'] 	= "Success";
		           	echo json_encode($ajax_data);
		           	exit();
	        	}else{
	        		//dd('Min Wallet value');
	        		if($request->session()->has('is_wallet') && $request->session()->has('is_wallet')=='on'){
	        		    $pay_amt-=$request->session()->get('wallet_amount');
	        		}
		            date_default_timezone_set('Asia/Calcutta');
		            $datenow 			= date("d/m/Y H:m:s");
		            $transactionDate 	= str_replace(" ", "%20", $datenow);

		            $transactionId 		= rand(1, 1000000).time();

		            DB::table('flight_temp_datas')->insertGetId(
					    ['unique_id' => $transactionId, 'value' => json_encode($request->all())]
					);
		            $transactionRequest = new TransactionRequest();
		            $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
		            $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
		            $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
		            $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
		            $transactionRequest->setAmount($pay_amt);
		            $transactionRequest->setTransactionCurrency("INR");
		            $transactionRequest->setTransactionAmount($pay_amt);
		            $transactionRequest->setReturnUrl(route('flight.payment.response'));
		            $transactionRequest->setClientCode(123);
		            $transactionRequest->setTransactionId($transactionId);
		            $transactionRequest->setTransactionDate($transactionDate);
		            $transactionRequest->setCustomerName('-');
		            $transactionRequest->setCustomerEmailId($request->email);
		            $transactionRequest->setCustomerMobile($request->mobile);
		            $transactionRequest->setLeadId(0);
		            $transactionRequest->setQuotId(0);
		            $transactionRequest->setCustomerBillingAddress("India");
		            $transactionRequest->setCustomerAccount("639827");
		            $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
		            
		            $ajax_data['url'] 		= $transactionRequest->getPGUrl();
		            $ajax_data['status'] 	= "Success";
		 
		           	echo json_encode($ajax_data);
				    exit(); 	
	        	}
	        }else{
	        	//dd(Not Use Wallet);
	            date_default_timezone_set('Asia/Calcutta');
	            $datenow 			= date("d/m/Y H:m:s");
	            $transactionDate 	= str_replace(" ", "%20", $datenow);

	            $transactionId 		= rand(1, 1000000).time();

	            DB::table('flight_temp_datas')->insertGetId(
				    ['unique_id' => $transactionId, 'value' => json_encode($request->all())]
				);
	            $transactionRequest = new TransactionRequest();
	            $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
	            $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
	            $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
	            $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
	            $transactionRequest->setAmount($pay_amt);
	            $transactionRequest->setTransactionCurrency("INR");
	            $transactionRequest->setTransactionAmount($pay_amt);
	            $transactionRequest->setReturnUrl(route('flight.payment.response'));
	            $transactionRequest->setClientCode(123);
	            $transactionRequest->setTransactionId($transactionId);
	            $transactionRequest->setTransactionDate($transactionDate);
	            $transactionRequest->setCustomerName('-');
	            $transactionRequest->setCustomerEmailId($request->email);
	            $transactionRequest->setCustomerMobile($request->mobile);
	            $transactionRequest->setLeadId(0);
	            $transactionRequest->setQuotId(0);
	            $transactionRequest->setCustomerBillingAddress("India");
	            $transactionRequest->setCustomerAccount("639827");
	            $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
	            
	            $ajax_data['url'] 		= $transactionRequest->getPGUrl();
	            $ajax_data['status'] 	= "Success";
	 
	           	echo json_encode($ajax_data);
			    exit(); 	
	        }        
	    }else{
	        $flight_response['error'] 	= '100';
	        $flight_response['message'] = 'Something happen bad';            
	        echo json_encode($flight_response);
	        exit;        
	    }
	    // End IF
    }

    /*

    public function flight_order_now(Request $request) {
    	$result = array();
	    // Validate parameter
	    if( isset($request->SearchFormData) && !empty($request->SearchFormData) &&
	        isset($request->CartData) && !empty($request->CartData) &&
	        isset($request->CartBookingId) && !empty($request->CartBookingId) &&
	        isset($request->email) && !empty($request->email) &&
	        isset($request->mobile) && !empty($request->mobile)){        
	        	        
	        $pay_amt = str_replace("," , "" , $request->total_price);

	    	if($request->session()->has('is_wallet') && $request->session()->has('is_wallet')=='on'){
	    	    $pay_amt-=$request->session()->get('wallet_amount');
	    	}

	    	//$cashback_amt = $this->cashback($pay_amt,1);
	    	//cashback_amt
	    	//\Session::put(['cashback_amt'=>round($cashback_amt)]);

            date_default_timezone_set('Asia/Calcutta');
            $datenow 			= date("d/m/Y h:m:s");
            $transactionDate 	= str_replace(" ", "%20", $datenow);

            $transactionId 		= rand(1, 1000000).time();

            DB::table('flight_temp_datas')->insertGetId(
			    ['unique_id' => $transactionId, 'value' => json_encode($request->all())]
			);
            $transactionRequest = new TransactionRequest();
            $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
            $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
            $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
            $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
            $transactionRequest->setAmount($pay_amt);
            $transactionRequest->setTransactionCurrency("INR");
            $transactionRequest->setTransactionAmount($pay_amt);
            $transactionRequest->setReturnUrl(route('flight.payment.response'));
            $transactionRequest->setClientCode(123);
            $transactionRequest->setTransactionId($transactionId);
            $transactionRequest->setTransactionDate($transactionDate);
            $transactionRequest->setCustomerName('-');
            $transactionRequest->setCustomerEmailId($request->email);
            $transactionRequest->setCustomerMobile($request->mobile);
            $transactionRequest->setLeadId(0);
            $transactionRequest->setQuotId(0);
            $transactionRequest->setCustomerBillingAddress("India");
            $transactionRequest->setCustomerAccount("639827");
            $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
            
            $ajax_data['url'] 		= $transactionRequest->getPGUrl();
            $ajax_data['status'] 	= "Success";
 
           	echo json_encode($ajax_data);
		    exit; 	
	    }else{
	        $flight_response['error'] 	= '100';
	        $flight_response['message'] = 'Something happen bad';            
	        echo json_encode($flight_response);
	        exit;        
	    }
	    // End IF
    }

    */

    public function flight_payment_response(Request $request) {
        if ($request) {
            $payment_info = array(
                //"customer_id" => (($request->udf5) ? $request->udf5 : ''),
                "status" => (($request->f_code) ? $request->f_code : ''),
                "paid_amount" => (($request->amt) ? $request->amt : ''),
                "paid_date" => (($request->date) ? date('Y-m-d H:m:s', strtotime($request->date)) : ''),
                "transaction_id" => (($request->mmp_txn) ? $request->mmp_txn : ''),
                "merchant_transaction_id" => (($request->mer_txn) ? $request->mer_txn : ''),
                "bank_transaction_id" => (($request->bank_txn) ? $request->bank_txn : ''),
                "bank_name" => (($request->bank_name) ? $request->bank_name : ''),
                "auth_code" => (($request->auth_code) ? $request->auth_code : ''),
                "description" => (($request->desc) ? $request->desc : ''),
                "user_name" => (($request->udf1) ? $request->udf1 : ''),
                "user_email" => (($request->udf2) ? $request->udf2 : ''),
                "user_mobile_no" => (($request->udf3) ? $request->udf3 : ''),
                "user_address" => (($request->udf4) ? $request->udf4 : ''),
                "customer_id" => isset(\Auth::user()->id)?\Auth::user()->id:'',
                "wallet_amount" => ($request->session()->has('wallet_amount'))?$request->session()->get('wallet_amount'):'',
                "is_wallet" => ($request->session()->has('is_wallet'))?1:0,
                "all_response" => response()->json($request),
                "is_payment_type"  => ($request->session()->has('is_wallet'))?3:1
            );
        }
        if ($payment = $this->getPaymentLib()->addPaymentResponse($payment_info)) {

        	$flight_temp_datas = DB::table('flight_temp_datas')->where('unique_id', $request->mer_txn)->first();
            if ($payment_info['status'] == 'Ok') {

            	if($request->session()->has('is_status')){
            	    if($request->session()->get('is_status')==1){
            	        if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
            	            $wallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
            	            if(isset($wallet) && $wallet!=''){
            	                $current_balance=$wallet->current_balance-$request->session()->get('wallet_amount');
            	                $wallet_data = Wallet::create([
            	                    'user_id'     => \Auth::user()->id,
            	                    'debit'      => $request->session()->get('wallet_amount'),
            	                    'current_balance'=>$current_balance
            	                ]);
            	            }
            	        }
            	    }
            	}

            	if($request->session()->has('is_status')){
            		if($request->session()->get('is_status')==1){
            			if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
            				$totalCashback=$this->cashback(round($request->amt)+round($request->session()->get('wallet_amount')),1);
            				if($totalCashback){
            					$lastRecordwallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
            					if(isset($lastRecordwallet) && $lastRecordwallet!=''){
            						$lastrecord_current_balance=$lastRecordwallet->current_balance+round($totalCashback);
            						$lastrecord_wallet_data = Wallet::create([
            						    'user_id'     => \Auth::user()->id,
            						    'credit'      => round($totalCashback),
            						    'current_balance'=>$lastrecord_current_balance
            						]);
            					}else{
            						$addnew_record_wallet_data = Wallet::create([
            						    'user_id'     => \Auth::user()->id,
            						    'credit'      => round($totalCashback),
            						    'current_balance'=>round($totalCashback)
            						]);
            					}
            				}
            			}
            		}
            	}else{
            		if($this->cashback($request->amt,1)){
            			if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
            			    $lastRecordwallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
            			    if(isset($lastRecordwallet) && $lastRecordwallet!=''){
            			        $lastrecord_current_balance=$lastRecordwallet->current_balance+round($this->cashback($request->amt,1));
            			        $lastrecord_wallet_data = Wallet::create([
            			            'user_id'     => \Auth::user()->id,
            			            'credit'      => round($this->cashback($request->amt,1)),
            			            'current_balance'=>$lastrecord_current_balance
            			        ]);
            			    }
            			}
            		}
            	}
            	
            	// if($this->cashback($request->amt,1)){
            	// 	if(isset(\Auth::user()->id) && \Auth::user()->id!=''){
            	// 	    $lastRecordwallet = $this->getWalletLib()->getWalletUserDataById(\Auth::user()->id);
            	// 	    if(isset($lastRecordwallet) && $lastRecordwallet!=''){
            	// 	        $lastrecord_current_balance=$lastRecordwallet->current_balance+round($this->cashback($request->amt,1));
            	// 	        $lastrecord_wallet_data = Wallet::create([
            	// 	            'user_id'     => \Auth::user()->id,
            	// 	            'credit'      => round($this->cashback($request->amt,1)),
            	// 	            'current_balance'=>$lastrecord_current_balance
            	// 	        ]);
            	// 	    }
            	// 	}
            	// }
            	
				$request_session_obj = json_decode($flight_temp_datas->value) ;
				$request_session = (array) $request_session_obj;
				$sessionID =  $this->getSessionId();        
			        if(empty($sessionID)){
			            $result['error'] = '101';
			            $result['message'] = 'Something happen bad';            
			            echo json_encode($result);
			            exit;
			        }
			        $SearchFormData = $request_session['SearchFormData'];
			        $CartData = $request_session['CartData'];
			        $CartBookingId = $request_session['CartBookingId'];
			        $email = $request_session['email'];
			        $mobile = $request_session['mobile'];
			        
			        $fistname = (array) $request_session['firstname'];        
			        $lastname = (array) $request_session['lastname'];        
			        $sex 	  = (array) $request_session['sex'];        
			        $dob 	  = (  isset($request_session['dob']) ? (array) $request_session['dob'] : array()) ;   

			        $request_seats 	  = (  isset($request_session['seats']) ? (array) $request_session['seats'] : array()) ;        
			        $request_meals 	  = (  isset($request_session['meals']) ? (array) $request_session['meals'] : array()) ;        
			        $request_buggage 	  = (  isset($request_session['buggage']) ? (array) $request_session['buggage'] : array()) ;

			        $request_set_data['Data']['SearchFormData'] = $SearchFormData;
			        $request_set_data['Data']['CartData'] = $CartData;
			        $request_set_data['Data']['CartBookingId'] = $CartBookingId;
			        
			        $i=0;
			        foreach($fistname as $key => $value){
			            if(strpos($key, 'adults') !== false) {
			                $type='A';
			            }else if(strpos($key, 'child') !== false) {
			                $type='C';
			            }else if(strpos($key, 'infants') !== false) {
			                $type='I';
			            }
			            
			            $request_set_data['Data']['Passengers'][$i]['Type'] = $type;
			            $request_set_data['Data']['Passengers'][$i]['DateOfBirth'] = isset( $dob[$key]) ?  $dob[$key] : "N/A";
			            $request_set_data['Data']['Passengers'][$i]['Title'] = "Mr";
			            $request_set_data['Data']['Passengers'][$i]['FirstName'] = $value;
			            $request_set_data['Data']['Passengers'][$i]['LastName'] = $lastname[$key];            
			            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['SeatPreference'] = isset($request_seats[$key]) ? $request_seats[$key] : "N/A";
			            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['Id'] = "N/A";
			            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['MealPreference'] = isset($request_meals[$key]) ? $request_meals[$key] : "N/A";;
			            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['BuggagePreference'] = isset($request_buggage[$key]) ? $request_buggage[$key] : "N/A";;
			            //$request_set_data['Data']['Passengers'][$i]['Preferences'][0]['SSRPreference'] = "N/A";
			            $request_set_data['Data']['Passengers'][$i]['ReportingParameters'][0]['Id'] = "N/A";
			            $request_set_data['Data']['Passengers'][$i]['ReportingParameters'][0]['Value'] = "N/A";
			            $i++;
			        }
			                
			        $request_set_data['Data']['DeliveryInfo']['Mobile'] = $mobile;
			        $request_set_data['Data']['DeliveryInfo']['Email'] = $email;
			        $request_set_data['Data']['DeliveryInfo']['SendEmail'] = "N";
			        $request_set_data['Data']['DeliveryInfo']['SendSMS'] = "N";
			        
			        $request_set_data['Data']['MetaInfos']['E'] = "0";
			        $request_set_data['Data']['MetaInfos']['M'] = "0";        
			        $request_set_data['Data']['PaymentMethod']['Mode'] = "D";        
			        $requestJsonBody = json_encode($request_set_data); 
			        
			        $url = "https://".env('FLIGHT_URL')."/restwebsvc/air/bookV1";
			        
			       // Set headers 
			        $headers = array(            
			            "Content-type:application/json",
			            "Accept: application/json",
			            "Content-Encoding:gzip",
			            "Accept-Encoding:zip",
			            "Cookie: JSESSIONID=".$sessionID
			        ); 
        
				    $result_session = $this->sendRequest($url,$headers,$requestJsonBody);
				    
			        //Save Data In DB
			        if( $result_session->Status == 'Success' ){
		        		$books_data= array(
		        			"flight_from" => $request_session['flight_from'], // $request_session['from']
							"flight_to" => $request_session['flight_to'], // $request_session['to']
							"depart_date" => date('Y-m-d', strtotime($request_session['depart_date'])), // $request_session['departdate']
							"passengers_class" => $request_session['passengers_class'], // $request_session['class']
							"adults" =>$request_session['adults'], // $request_session['adults']
							"child" => $request_session['child'], // $request_session['child']
							"infants" => $request_session['infants'], // $request_session['infants']
							"email" => $request_session['email'],
							"mobile" => $request_session['mobile'],
							"base_fare" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->BasePrice),
							"udfcharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->UdfCharge),
							"congestioncharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->CongestionCharge),
							"igstax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->IGSTax),
							"tgsttax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->TGSTTax),
							"airporttax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->AirportTax),
							"fuelSurcharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->FuelSurcharge),
							"grand_total" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->TotalPrice),
							"api_response" =>  json_encode($result_session) ,
							"payment_id" =>  $payment ,
		        		);
		        	  	$flight_book_id = $this->getFlightBooksLib()->addFlightBooks($books_data);

		        	  	foreach ($fistname as $k => $val) {
		        	  		$type = preg_replace('/[0-9]+/', '', $k);
		        	  		$i = (int) filter_var($k, FILTER_SANITIZE_NUMBER_INT);
		        	  		$meals = ''; $meals_price = 0;
		        	  		foreach ($result_session->Data->Cart->OrderItems[0]->Prefrences->MealPrefrences[0]->PrefList as  $PrefList) {
		        	  			if(isset($request_meals[$type.$i])  && $request_meals[$type.$i] == $PrefList->Code ){
		        	  				$meals = $PrefList->Description.' ('.$PrefList->Code.')';
		        	  				$meals_price = $PrefList->Price;
		        	  			}
		        	  		}

		        	  		$seats = ''; $seats_price = 0;
		        	  		foreach ($result_session->Data->Cart->OrderItems[0]->Prefrences->SeatPrefrences[0]->PrefList as  $SeatPre) {
		        	  			if(isset($request_seats[$type.$i])  && $request_seats[$type.$i] == $SeatPre->Code ){
		        	  				$seats = $SeatPre->Description.' ('.$SeatPre->Code.')';
		        	  				$seats_price = $SeatPre->Price;
		        	  			}
		        	  		}
		 
		        	  		$buggage = ''; $buggage_price = 0;
		        	  		foreach ($result_session->Data->Cart->OrderItems[0]->Prefrences->SeatPrefrences[0]->PrefList as  $BuggagePre) {
		        	  			if(isset($request_buggage[$type.$i])  && $request_buggage[$type.$i] == $BuggagePre->Code ){
		        	  				$seats = $BuggagePre->Description.' ('.$BuggagePre->Code.')';
		        	  				$seats_price = $BuggagePre->Price;
		        	  			}
		        	  		}
		 
						 	$passengers = array(
								'flight_books_id' => $flight_book_id ,
							    'passengers_type' => $type,
							    'first_name'      => ( isset($fistname[$type.$i]) ? $fistname[$type.$i] : ''),
							    'last_name'       => ( isset($lastname[$type.$i]) ? $lastname[$type.$i] : ''),
							    'date_of_birth'   => ( isset($dob[$type.$i]) ? date('Y-m-d', strtotime($dob[$type.$i])) : ''),
							    'gender'          => ( isset($sex[$type.$i]) ? $sex[$type.$i] : ''),
							    'meals'           => $meals ,
							    'meals_price'     => $meals_price,
							    'seats'           => $seats,
							    'seats_price'     => $seats_price,
							    'buggage'           => $buggage,
							    'buggage_price'     => $buggage_price 
							);
			        	  	$flight_passengers_id = $this->getFlightPassengersLib()->addFlightPassengers($passengers);
		        	  	}

		    	  		$flights = $result_session->Data->Cart->OrderItems;
		                foreach($flights as $key => $row){                
		                    $item = $row->FlightSegment; 
		                    $item_PriceInfo = $result_session->Data->Cart->OrderItems[0]->PriceInfo;                             
		                    
			                $DepartureTime = str_replace("/", "-",$item->DepartureTime);
			                $ArrivalTime = str_replace("/", "-",$item->ArrivalTime);

			                $minutes = $item->JourneyTime;
			                $hours = floor($minutes / 60);
			                $min = $minutes - ($hours * 60);
			                $count_stop =(  (isset($item->StopoverCodes))  ? count($item->StopoverCodes) : 0);

						 	$passengers = array(
								'flight_books_id' => $flight_book_id ,
							    'flight_name'     =>  $item->OperatingAirline,
							    'flight_number'   => $item->FlightNumber,
							    'carrier_code'    => $item->CarrierCode,
							    'drrival_airport' => $item->DepartureAirport ,
							    'departure_time'  => date("Y-m-d H:i:s",strtotime($DepartureTime)),
							    'aeparture_airport'  => $item->ArrivalAirport,
							    'arrival_time'    => date("Y-m-d H:i:s",strtotime($ArrivalTime)),
							    'time_duration'   => $hours."h ".$min.'m',
							    'flight_type'     => $count_stop == 0 ? "Non stop" : $count_stop.' stop',
							);
			        	  	$flight_passengers_id = $this->getFlightReviewInfoLib()->addFlightReviewInfo($passengers);
			        	}
			        	\Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format(($request->session()->has('is_wallet'))?$request->amt+$request->session()->get('wallet_amount'):$request->amt, 2) . ' ) has been paid successfully.');
                		\Session::flash('alert-class', 'alert-success');	
			        }else{
			        	\Session::flash('message', 'Error..! Your payment (Rs. ' . number_format(($request->session()->has('is_wallet'))?$request->amt+$request->session()->get('wallet_amount'):$request->amt, 2) . ' ) has been paid unsuccessfully.');
               			\Session::flash('alert-class', 'alert-danger');	
			        }
                \Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format(($request->session()->has('is_wallet'))?$request->amt+$request->session()->get('wallet_amount'):$request->amt, 2) . ' ) has been paid successfully.');
                \Session::flash('alert-class', 'alert-success');
            }else{
                \Session::flash('message', 'Error..! Your payment (Rs. ' . number_format(($request->session()->has('is_wallet'))?$request->amt+$request->session()->get('wallet_amount'):$request->amt, 2) . ' ) has been paid unsuccessfully.');
                \Session::flash('alert-class', 'alert-danger');
            }
        }else{
            \Session::flash('message', 'Error in payment.');
            \Session::flash('alert-class', 'alert-danger');
        }
        \Session::forget(['is_status','is_wallet','wallet_amount','cashback_amt']);
        return redirect()->route('flight.booking');
        //return redirect()->route('flight.booking', $request->mmp_txn);
    }

    public function cashback($pay_amt,$percentage){
        $cashback_amt = ($percentage / 100) * $pay_amt;
        return $cashback_amt;
    }

    public function cashback_session(){
    	return \Session::get('cashback_amt');        
    }

    public function getOperatingAirline($segments){
        $OperatingAirline = $segments[0]->OperatingAirline;
        foreach($segments as $row){            
            if($OperatingAirline != $row->OperatingAirline){
                $OperatingAirline = "Multiple";           
            }
        }
        return $OperatingAirline;
    }
    
    public function getSessionId(){
        // Build the request json string        
        $requestJsonBody = '{"Role":"A","IsMobile":"N","UserId":"'.env('FLIGHT_USERID').'","Password":"'.env('FLIGHT_PASSWORD').'","AuthType":"WBS"}';
        $url = "https://".env('FLIGHT_URL')."/restwebsvc/auth/loginV1";        
        // Set headers 
        $headers = array(            
            "Content-type:application/json",
            "Accept: application/json",
            "Content-Encoding:gzip",
            "Accept-Encoding:zip"
        ); 
        
        // send request 
        $result = $this->sendRequest($url,$headers,$requestJsonBody);
        if(isset($result->Data->SessionId)){
            return $result->Data->SessionId;
        }
        return "";
    }
    
    public function sendRequest($url,$headers,$requestJsonBody){
        //setting the curl parameters.
        $ch = curl_init();        
        curl_setopt($ch, CURLOPT_URL, $url);        
        // Following line is compulsary to add as it is:
        curl_setopt($ch, CURLOPT_POSTFIELDS,$requestJsonBody);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 400); //timeout in seconds
        $response = curl_exec($ch);
        curl_close($ch);        
        return json_decode($response);
    }

    /*
    
    public function flight_order_now(Request $request) {

    	// $request_session = $request->all();

	    //     $sessionID =  $this->getSessionId();        
	        
	    //     if(empty($sessionID)){
	    //         $result['error'] = '101';
	    //         $result['message'] = 'Something happen bad';            
	    //         echo json_encode($result);
	    //         exit;
	    //     }
	    //     $SearchFormData = $request_session['SearchFormData'];
	    //     $CartData = $request_session['CartData'];
	    //     $CartBookingId = $request_session['CartBookingId'];
	    //     $email = $request_session['email'];
	    //     $mobile = $request_session['mobile'];
	        
	    //     $fistname = $request_session['firstname'];        
	    //     $lastname = $request_session['lastname'];        
	    //     $sex = $request_session['sex'];        
	    //     $request_set_data['Data']['SearchFormData'] = $SearchFormData;
	    //     $request_set_data['Data']['CartData'] = $CartData;
	    //     $request_set_data['Data']['CartBookingId'] = $CartBookingId;
	        
	    //     $i=0;
	    //     foreach($fistname as $key => $value){
	    //         if(strpos($key, 'adults') !== false) {
	    //             $type='A';
	    //         }else if(strpos($key, 'child') !== false) {
	    //             $type='C';
	    //         }else if(strpos($key, 'infants') !== false) {
	    //             $type='I';
	    //         }
	            
	    //         $request_set_data['Data']['Passengers'][$i]['Type'] = $type;
	    //         $request_set_data['Data']['Passengers'][$i]['DateOfBirth'] = isset($request_session['dob'][$key]) ? $request_session['dob'][$key] : "N/A";
	    //         $request_set_data['Data']['Passengers'][$i]['Title'] = "Mr";
	    //         $request_set_data['Data']['Passengers'][$i]['FirstName'] = $value;
	    //         $request_set_data['Data']['Passengers'][$i]['LastName'] = $lastname[$key];            
	    //         $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['SeatPreference'] = isset($request_session['seats'][$key]) ? $request_session['seats'][$key] : "N/A";
	    //         $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['Id'] = "N/A";
	    //         $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['MealPreference'] = isset($request_session['meals'][$key]) ? $request_session['meals'][$key] : "N/A";;
	    //         $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['BuggagePreference'] = isset($request_session['buggage'][$key]) ? $request_session['buggage'][$key] : "N/A";;
	    //         //$request_set_data['Data']['Passengers'][$i]['Preferences'][0]['SSRPreference'] = "N/A";
	    //         $request_set_data['Data']['Passengers'][$i]['ReportingParameters'][0]['Id'] = "N/A";
	    //         $request_set_data['Data']['Passengers'][$i]['ReportingParameters'][0]['Value'] = "N/A";
	    //         $i++;
	    //     }
	                
	    //     $request_set_data['Data']['DeliveryInfo']['Mobile'] = $mobile;
	    //     $request_set_data['Data']['DeliveryInfo']['Email'] = $email;
	    //     $request_set_data['Data']['DeliveryInfo']['SendEmail'] = "N";
	    //     $request_set_data['Data']['DeliveryInfo']['SendSMS'] = "N";
	        
	    //     $request_set_data['Data']['MetaInfos']['E'] = "0";
	    //     $request_set_data['Data']['MetaInfos']['M'] = "0";        
	    //     $request_set_data['Data']['PaymentMethod']['Mode'] = "D";        
	    //     $requestJsonBody = json_encode($request_set_data);        
	        
	    //     $url = "https://".env('FLIGHT_URL')."/restwebsvc/air/bookV1";
	        
	    //    // Set headers 
	    //     $headers = array(            
	    //         "Content-type:application/json",
	    //         "Accept: application/json",
	    //         "Content-Encoding:gzip",
	    //         "Accept-Encoding:zip",
	    //         "Cookie: JSESSIONID=".$sessionID
	    //     ); 

	    // $result_session = $this->sendRequest($url,$headers,$requestJsonBody);
	    // dd( $result_session );



    	$result = array();
    	// dd($request->all());
	    // Validate parameter
	    if( isset($request->SearchFormData) && !empty($request->SearchFormData) &&
	        isset($request->CartData) && !empty($request->CartData) &&
	        isset($request->CartBookingId) && !empty($request->CartBookingId) &&
	        isset($request->email) && !empty($request->email) &&
	        isset($request->mobile) && !empty($request->mobile)){        
	        
	        // $request->session()->forget('request_Data');
		    // Session::push('request_Data', $request->all()); 

		    // $request->session()->put('request_Data', $request->all());

	        
	        $pay_amt = str_replace("," , "" , $request->total_price);
	        // $pay_amt = number_format((((($request->amount) * 2.6) / 100 ) + $request->amount), 2);

            date_default_timezone_set('Asia/Calcutta');
            $datenow = date("d/m/Y h:m:s");
            $transactionDate = str_replace(" ", "%20", $datenow);

            $transactionId = rand(1, 1000000).time();

            DB::table('flight_temp_datas')->insertGetId(
			    ['unique_id' => $transactionId, 'value' => json_encode($request->all())]
			);
            $transactionRequest = new TransactionRequest();
            $transactionRequest->setMode(env('PAYMENT_MODE', 'test'));
            $transactionRequest->setLogin(env('PAYMENT_LOGIN', 197));
            $transactionRequest->setPassword(env('PAYMENT_PASSWORD', 'Test@123'));
            $transactionRequest->setProductId(env('PAYMENT_PRODUCTID', 'NSE'));
            $transactionRequest->setAmount($pay_amt);
            $transactionRequest->setTransactionCurrency("INR");
            $transactionRequest->setTransactionAmount($pay_amt);
            $transactionRequest->setReturnUrl(route('flight.payment.response'));
            $transactionRequest->setClientCode(123);
            $transactionRequest->setTransactionId($transactionId);
            $transactionRequest->setTransactionDate($transactionDate);
            $transactionRequest->setCustomerName('-');
            $transactionRequest->setCustomerEmailId($request->email);
            $transactionRequest->setCustomerMobile($request->mobile);
            $transactionRequest->setLeadId(0);
            $transactionRequest->setQuotId(0);
            $transactionRequest->setCustomerBillingAddress("India");
            $transactionRequest->setCustomerAccount("639827");
            $transactionRequest->setReqHashKey(env("REQHASHKEY","1f3854e67ce80ffca1"));
            

            $ajax_data['url'] = $transactionRequest->getPGUrl();
            $ajax_data['status'] = "Success";
 
           	echo json_encode($ajax_data);
		    exit; 	
        
	    }else{
	        $flight_response['error'] = '100';
	        $flight_response['message'] = 'Something happen bad';            
	        echo json_encode($flight_response);
	        exit;        
	    }// End IF
	 
    }

    public function flight_payment_response(Request $request) {

        if ($request) {
            $payment_info = array(
                "customer_id" => (($request->udf5) ? $request->udf5 : ''),
                "status" => (($request->f_code) ? $request->f_code : ''),
                "paid_amount" => (($request->amt) ? $request->amt : ''),
                "paid_date" => (($request->date) ? date('Y-m-d h:m:s', strtotime($request->date)) : ''),
                "transaction_id" => (($request->mmp_txn) ? $request->mmp_txn : ''),
                "merchant_transaction_id" => (($request->mer_txn) ? $request->mer_txn : ''),
                "bank_transaction_id" => (($request->bank_txn) ? $request->bank_txn : ''),
                "bank_name" => (($request->bank_name) ? $request->bank_name : ''),
                "auth_code" => (($request->auth_code) ? $request->auth_code : ''),
                "description" => (($request->desc) ? $request->desc : ''),
                "user_name" => (($request->udf1) ? $request->udf1 : ''),
                "user_email" => (($request->udf2) ? $request->udf2 : ''),
                "user_mobile_no" => (($request->udf3) ? $request->udf3 : ''),
                "user_address" => (($request->udf4) ? $request->udf4 : ''),
                "customer_id" => \Auth::user()->id,
                "all_response" => response()->json($request),
            );
        }
 

        if ($payment = $this->getPaymentLib()->addPaymentResponse($payment_info)) {
        	$flight_temp_datas = DB::table('flight_temp_datas')->where('unique_id', $request->mer_txn)->first();
            if ($payment_info['status'] == 'Ok') {
         		
            		 $request_session_obj = json_decode($flight_temp_datas->value) ;
 					 $request_session = (array) $request_session_obj;

			        $sessionID =  $this->getSessionId();        
			        
			        if(empty($sessionID)){
			            $result['error'] = '101';
			            $result['message'] = 'Something happen bad';            
			            echo json_encode($result);
			            exit;
			        }
			        $SearchFormData = $request_session['SearchFormData'];
			        $CartData = $request_session['CartData'];
			        $CartBookingId = $request_session['CartBookingId'];
			        $email = $request_session['email'];
			        $mobile = $request_session['mobile'];
			        
			        $fistname = (array) $request_session['firstname'];        
			        $lastname = (array) $request_session['lastname'];        
			        $sex 	  = (array) $request_session['sex'];        
			        $dob 	  = (  isset($request_session['dob']) ? (array) $request_session['dob'] : array()) ;   

			        $request_seats 	  = (  isset($request_session['seats']) ? (array) $request_session['seats'] : array()) ;        
			        $request_meals 	  = (  isset($request_session['meals']) ? (array) $request_session['meals'] : array()) ;        
			        $request_buggage 	  = (  isset($request_session['buggage']) ? (array) $request_session['buggage'] : array()) ;

			        $request_set_data['Data']['SearchFormData'] = $SearchFormData;
			        $request_set_data['Data']['CartData'] = $CartData;
			        $request_set_data['Data']['CartBookingId'] = $CartBookingId;
			        
			        $i=0;
			        foreach($fistname as $key => $value){
			            if(strpos($key, 'adults') !== false) {
			                $type='A';
			            }else if(strpos($key, 'child') !== false) {
			                $type='C';
			            }else if(strpos($key, 'infants') !== false) {
			                $type='I';
			            }
			            
			            $request_set_data['Data']['Passengers'][$i]['Type'] = $type;
			            $request_set_data['Data']['Passengers'][$i]['DateOfBirth'] = isset( $dob[$key]) ?  $dob[$key] : "N/A";
			            $request_set_data['Data']['Passengers'][$i]['Title'] = "Mr";
			            $request_set_data['Data']['Passengers'][$i]['FirstName'] = $value;
			            $request_set_data['Data']['Passengers'][$i]['LastName'] = $lastname[$key];            
			            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['SeatPreference'] = isset($request_seats[$key]) ? $request_seats[$key] : "N/A";
			            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['Id'] = "N/A";
			            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['MealPreference'] = isset($request_meals[$key]) ? $request_meals[$key] : "N/A";;
			            $request_set_data['Data']['Passengers'][$i]['Preferences'][0]['BuggagePreference'] = isset($request_buggage[$key]) ? $request_buggage[$key] : "N/A";;
			            //$request_set_data['Data']['Passengers'][$i]['Preferences'][0]['SSRPreference'] = "N/A";
			            $request_set_data['Data']['Passengers'][$i]['ReportingParameters'][0]['Id'] = "N/A";
			            $request_set_data['Data']['Passengers'][$i]['ReportingParameters'][0]['Value'] = "N/A";
			            $i++;
			        }
			                
			        $request_set_data['Data']['DeliveryInfo']['Mobile'] = $mobile;
			        $request_set_data['Data']['DeliveryInfo']['Email'] = $email;
			        $request_set_data['Data']['DeliveryInfo']['SendEmail'] = "N";
			        $request_set_data['Data']['DeliveryInfo']['SendSMS'] = "N";
			        
			        $request_set_data['Data']['MetaInfos']['E'] = "0";
			        $request_set_data['Data']['MetaInfos']['M'] = "0";        
			        $request_set_data['Data']['PaymentMethod']['Mode'] = "D";        
			        $requestJsonBody = json_encode($request_set_data); 
			        
			        $url = "https://".env('FLIGHT_URL')."/restwebsvc/air/bookV1";
			        
			       // Set headers 
			        $headers = array(            
			            "Content-type:application/json",
			            "Accept: application/json",
			            "Content-Encoding:gzip",
			            "Accept-Encoding:zip",
			            "Cookie: JSESSIONID=".$sessionID
			        ); 
        
				    $result_session = $this->sendRequest($url,$headers,$requestJsonBody);
				    
			        //Save Data In DB
			        if( $result_session->Status == 'Success' ){
		        		$books_data= array(
		        			"flight_from" => $request_session['flight_from'], // $request_session['from']
							"flight_to" => $request_session['flight_to'], // $request_session['to']
							"depart_date" => date('Y-m-d', strtotime($request_session['depart_date'])), // $request_session['departdate']
							"passengers_class" => $request_session['passengers_class'], // $request_session['class']
							"adults" =>$request_session['adults'], // $request_session['adults']
							"child" => $request_session['child'], // $request_session['child']
							"infants" => $request_session['infants'], // $request_session['infants']
							"email" => $request_session['email'],
							"mobile" => $request_session['mobile'],
							"base_fare" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->BasePrice),
							"udfcharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->UdfCharge),
							"congestioncharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->CongestionCharge),
							"igstax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->IGSTax),
							"tgsttax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->TGSTTax),
							"airporttax" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->AirportTax),
							"fuelSurcharge" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->CustomTaxDetails->FuelSurcharge),
							"grand_total" => str_replace("," , "" , $result_session->Data->Cart->PriceDetails->TotalPrice),
							"api_response" =>  json_encode($result_session) ,
							"payment_id" =>  $payment ,
		        		);
		        	  	$flight_book_id = $this->getFlightBooksLib()->addFlightBooks($books_data);

		        	  	foreach ($fistname as $k => $val) {
		        	  		$type = preg_replace('/[0-9]+/', '', $k);
		        	  		$i = (int) filter_var($k, FILTER_SANITIZE_NUMBER_INT);
		        	  		$meals = ''; $meals_price = 0;
		        	  		foreach ($result_session->Data->Cart->OrderItems[0]->Prefrences->MealPrefrences[0]->PrefList as  $PrefList) {
		        	  			if(isset($request_meals[$type.$i])  && $request_meals[$type.$i] == $PrefList->Code ){
		        	  				$meals = $PrefList->Description.' ('.$PrefList->Code.')';
		        	  				$meals_price = $PrefList->Price;
		        	  			}
		        	  		}

		        	  		$seats = ''; $seats_price = 0;
		        	  		foreach ($result_session->Data->Cart->OrderItems[0]->Prefrences->SeatPrefrences[0]->PrefList as  $SeatPre) {
		        	  			if(isset($request_seats[$type.$i])  && $request_seats[$type.$i] == $SeatPre->Code ){
		        	  				$seats = $SeatPre->Description.' ('.$SeatPre->Code.')';
		        	  				$seats_price = $SeatPre->Price;
		        	  			}
		        	  		}
		 
						 	$passengers = array(
								'flight_books_id' => $flight_book_id ,
							    'passengers_type' => $type,
							    'first_name'      => ( isset($fistname[$type.$i]) ? $fistname[$type.$i] : ''),
							    'last_name'       => ( isset($lastname[$type.$i]) ? $lastname[$type.$i] : ''),
							    'date_of_birth'   => ( isset($dob[$type.$i]) ? date('Y-m-d', strtotime($dob[$type.$i])) : ''),
							    'gender'          => ( isset($sex[$type.$i]) ? $sex[$type.$i] : ''),
							    'meals'           => $meals ,
							    'meals_price'     => $meals_price,
							    'seats'           => $seats,
							    'seats_price'     => $seats_price 
							);
			        	  	$flight_passengers_id = $this->getFlightPassengersLib()->addFlightPassengers($passengers);
		        	  	}


		    	  		$flights = $result_session->Data->Cart->OrderItems;
		                foreach($flights as $key => $row){                
		                    $item = $row->FlightSegment; 
		                    $item_PriceInfo = $result_session->Data->Cart->OrderItems[0]->PriceInfo;                             
		                    
			                $DepartureTime = str_replace("/", "-",$item->DepartureTime);
			                $ArrivalTime = str_replace("/", "-",$item->ArrivalTime);

			                $minutes = $item->JourneyTime;
			                $hours = floor($minutes / 60);
			                $min = $minutes - ($hours * 60);
			                $count_stop =(  (isset($item->StopoverCodes))  ? count($item->StopoverCodes) : 0);

						 	$passengers = array(
								'flight_books_id' => $flight_book_id ,
							    'flight_name'     =>  $item->OperatingAirline,
							    'flight_number'   => $item->FlightNumber,
							    'carrier_code'    => $item->CarrierCode,
							    'drrival_airport' => $item->DepartureAirport ,
							    'departure_time'  => date("Y-m-d H:i:s",strtotime($DepartureTime)),
							    'aeparture_airport'  => $item->ArrivalAirport,
							    'arrival_time'    => date("Y-m-d H:i:s",strtotime($ArrivalTime)),
							    'time_duration'   => $hours."h ".$min.'m',
							    'flight_type'     => $count_stop == 0 ? "Non stop" : $count_stop.' stop',
							);
			        	  	$flight_passengers_id = $this->getFlightReviewInfoLib()->addFlightReviewInfo($passengers);
			        	}
			        	\Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format($request->amt, 2) . ' ) has been paid successfully.');
                		\Session::flash('alert-class', 'alert-success');	
			        }else{
			        	\Session::flash('message', 'Error..! Your payment (Rs. ' . number_format($request->amt, 2) . ' ) has been paid unsuccessfully.');
               			\Session::flash('alert-class', 'alert-danger');	
			        }
  
                \Session::flash('message', 'Congratulation..! Your payment (Rs. ' . number_format($request->amt, 2) . ' ) has been paid successfully.');
                \Session::flash('alert-class', 'alert-success');
            } else {
                \Session::flash('message', 'Error..! Your payment (Rs. ' . number_format($request->amt, 2) . ' ) has been paid unsuccessfully.');
                \Session::flash('alert-class', 'alert-danger');
            }
        } else {
            \Session::flash('message', 'Error in payment.');
            \Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('flight.booking', $request->mmp_txn);
    }

    */

}
