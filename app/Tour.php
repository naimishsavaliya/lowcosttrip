<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model {

    protected $table = 'tour';

    public function tourCost(){
    	return $this->hasMany(TourCost::class);
    }

    public function tourHotel(){
    	return $this->hasMany(TourHotel::class);
    }

    public function tourCostAmount(){
    	return $this->hasMany(TourCostAmount::class);
    }
}
