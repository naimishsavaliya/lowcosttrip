<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userprofile extends Model {

    protected $table = 'users_profile';
    protected $fillable = [
        'id', 'user_id', 'manager_id', 'subscription_id', 'title', 'address', 'country_id', 'state_id', 'city_id', 'pin_code', 'age', 'time_zone', 'phone_no', 'landline_no', 'gender', 'birth_date', 'information', 'image', 'sector_id', 'company_name', 'website', 'skype_id', 'designation', 'agent_type', 'how_old_agency', 'booking_per_month', 'no_of_employee', 'year_of_registration', 'compnay_type', 'where_hear_about', 'use_any_travel_web', 'destination_ids', 'products_services', 'current_travellers_regions', 'affiliations_id', 'contact_name_1', 'contact_email_1', 'contact_phone_1', 'contact_name_2', 'contact_email_2', 'contact_phone_2', 'contact_name_3', 'contact_email_3', 'contact_phone_3','country_code'
    ];

}
