<?php

namespace App\Libraries;

use App\DomesticHoliday;
use Illuminate\Support\Facades\Auth;

class DomesticHolidayLib {

    protected $_domesticholiday = null;

    public function getDomesticHolidayModel() {
        if (!($this->_domesticholiday instanceof \App\DomesticHoliday)) {
            $this->_domesticholiday = new \App\DomesticHoliday();
        }
        return $this->_domesticholiday;
    }

    public function addDomesticHoliday($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getDomesticHolidayModel()->insertGetId($data);
    }

    public function updateDomesticHoliday($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getDomesticHolidayModel()->where('id', $id)->update($data);
    }

    public function deleteDomesticHoliday($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getDomesticHolidayModel()->where('id', $id)->update($data);
    }

    public function getDomesticHolidayById($id, $fields = "*") {
        $query = $this->getDomesticHolidayModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getHotdomesticholidays($params = array()) {
        $query = $this->getDomesticHolidayModel()
                ->select('domestic_holidays.id','domestic_holidays.tour_id','domestic_holidays.title','domestic_holidays.start_date','domestic_holidays.end_date','tour.tour_name','tour.tour_nights','tour.image')
                ->leftJoin('tour', 'tour.id', '=', 'domestic_holidays.tour_id');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        $query->orderBy('domestic_holidays.id', 'DESC');
        $query->limit(4);
        return $query->get();
    }

}

?>