<?php

namespace App\Libraries;

use App\Course;
use Illuminate\Support\Facades\Auth;

class CourseLib {

	protected $_courseModel = null;

	public function getCourseModel() {
		if (!($this->_courseModel instanceof \App\Course)) {
			$this->_courseModel = new \App\Course();
		}
		return $this->_courseModel;
	}

	public function addCourse($data) {
		$data['created_at'] = date("Y-m-d H:i:s");
		$data['created_by'] = Auth::id();
		return $query = $this->getCourseModel()->insertGetId($data);
	}

	public function updateCourse($data, $id) {
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getCourseModel()->where('course_id', $id)->update($data);
	}

	public function deleteCourse($id) {
		$data = array();
		$data['deleted_at'] = date("Y-m-d H:i:s");
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getCourseModel()->where('course_id', $id)->update($data);
	}

	public function getCourseById($id) {
		$query = $this->getCourseModel()
				->where('course_id', $id);
		return $query->first();
	}

	public function getCourse($fields = "*", $all = false) {
		$query = $this->getCourseModel()
				->select($fields)
				->orderBy('course_name', 'ASC');
		if (!$all) {
			$query->where('deleted_at', null)->where('status',1);
		}
		return $query->get();
	}
	
	public function getCourseType() {
		return array(1=>"Live", 2 => "Pre Recorded");
	}

}

?>