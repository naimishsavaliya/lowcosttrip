<?php

namespace App\Libraries;

use App\BookActivities;
use Illuminate\Support\Facades\Auth;

class BookActivitiesLib {

    protected $_bookActivities = null;

    public function getBookActivities() {
        if (!($this->_bookActivities instanceof \App\BookActivities)) {
            $this->_bookActivities = new \App\BookActivities();
        }
        return $this->_bookActivities;
    }

    public function addBookActivities($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getBookActivities()->insertGetId($data);
    }

    public function updateBookActivities($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getBookActivities()->where('id', $id)->update($data);
    }

    public function deleteBookActivities($id) {
        $data = array();
        return $this->getBookActivities()->where('id', $id)->delete();
    }

    public function getBookActivitiesById($id) {
        $query = $this->getBookActivities()
                ->where('id', $id);
        return $query->first();
    }

    public function getBookActivitiesInfo($fields = "*", $all = false) {
        $query = $this->getBookActivities()
                ->select($fields)
                ->where('deleted_at',null)
                ->orderBy('title', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }


    public function getActivitiesByCity($c_id) {
        $query = $this->getBookActivities()
            ->select('id', 'title', 'country_id')
            ->where('status','1')
            ->where('country_id',$c_id)
            ->orderBy('id', 'ASC');
        return $query->get();
    }

}

?>