<?php

namespace App\Libraries;

use App\Category;
use Illuminate\Support\Facades\Auth;

class CategoryLib {

	protected $_categoryModel = null;

	public function getCategoryModel() {
		if (!($this->_categoryModel instanceof \App\Category)) {
			$this->_categoryModel = new \App\Category();
		}
		return $this->_categoryModel;
	}

	public function addCategory($data) {
		$data['created_at'] = date("Y-m-d H:i:s");
		$data['created_by'] = Auth::id();
		return $query = $this->getCategoryModel()->insertGetId($data);
	}

	public function updateCategory($data, $id) {
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getCategoryModel()->where('id', $id)->update($data);
	}

	public function deleteCategory($id) {
		$data = array();
		$data['deleted_at'] = date("Y-m-d H:i:s");
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getCategoryModel()->where('id', $id)->update($data);
	}

	public function getCategoryById($id) {
		$query = $this->getCategoryModel()
				->where('id', $id);
		return $query->first();
	}

	public function getCategories($fields = "*", $all = false) {
		$query = $this->getCategoryModel()
				->select($fields)
				->orderBy('category_name', 'ASC');
		if (!$all) {
			$query->where('deleted_at', null)->where('status',1);
		}
		return $query->get();
	}

}

?>