<?php

namespace App\Libraries;

use App\Tourpackagetype;
use Illuminate\Support\Facades\Auth;

class TourpackagetypeLib {

    protected $_tourpackagetypemasterModel = null;

    public function getTourpackagetypemasterModel() {
        if (!($this->_tourpackagetypemasterModel instanceof \App\Tourpackagetype)) {
            $this->_tourpackagetypemasterModel = new \App\Tourpackagetype();
        }
        return $this->_tourpackagetypemasterModel;
    }

    public function addTourpackagetypemaster($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourpackagetypemasterModel()->insertGetId($data);
    }

    public function updateTourpackagetypemaster($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourpackagetypemasterModel()->where('typeId', $id)->update($data);
    }

    public function deleteTourpackagetypemaster($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourpackagetypemasterModel()->where('typeId', $id)->update($data);
    }

    public function getTourpackagetypemasterById($id) {
        $query = $this->getTourpackagetypemasterModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getTourpackagetypemaster($fields = "*", $params = array()) {
        $query = $this->getTourpackagetypemasterModel()
                ->select($fields)
                ->orderBy('typeId', 'ASC');
        $query->where('deleted_at', null)->where('status', 1);
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>