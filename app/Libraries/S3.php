<?php

/**
 * AWS S3 library file
 *
 * @package         Aws\S3
 * @version         $Id:$
 */
/**
 * AWS S3 library class
 *
 * @package         Aws\S3
 * @author          sarif
 */

namespace App\Libraries;

use Illuminate\Support\Facades\Storage;

class S3 {
    /*     * *******************************************************************
     *                                                                   *
     * PUBLIC MAIN FUNCTIONS                                             *
     *                                                                   *
     * ******************************************************************* */

    public function __construct() {
        
    }

    /**
     * get the S3 filesystem object
     *
     * @param string $bucket
     * @param string $region
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    public static function getFilesystem($bucket = null, $region = null) {
        return Storage::createS3Driver([
                    'driver' => 's3',
                    'key' => config('filesystems.disks.s3.key'),
                    'secret' => config('filesystems.disks.s3.secret'),
                    'region' => ($region ?: config('filesystems.disks.s3.region')),
                    'bucket' => ($bucket ?: config('filesystems.disks.s3.bucket')),
        ]);
    }

    /*     * *******************************************************************
     *                                                                   *
     * PUBLIC HELPER FUNCTIONS                                           *
     *                                                                   *
     * ******************************************************************* */

    /*     * *******************************************************************
     *                                                                   *
     * PROTECTED FUNCTIONS                                               *
     *                                                                   *
     * ******************************************************************* */

    /*     * *******************************************************************
     *                                                                   *
     * PRIVATE FUNCTIONS                                                 *
     *                                                                   *
     * ******************************************************************* */
}
