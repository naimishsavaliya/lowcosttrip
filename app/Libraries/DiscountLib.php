<?php

namespace App\Libraries;

use App\Discount;
use Illuminate\Support\Facades\Auth;

class DiscountLib {

    protected $_discountModel = null;

    public function getDiscountModel() {
        if (!($this->_discountModel instanceof \App\Discount)) {
            $this->_discountModel = new \App\Discount();
        }
        return $this->_discountModel;
    }

    public function addDiscount($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getDiscountModel()->insertGetId($data);
    }

    public function updateDiscount($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getDiscountModel()->where('desId', $id)->update($data);
    }

    public function deleteDiscount($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getDiscountModel()->where('desId', $id)->update($data);
    }

    public function getDiscountById($id) {
        $query = $this->getDiscountModel()
                ->where('desId', $id);
        return $query->first();
    }

    public function getDiscount($fields = "*", $params = array()) {
        $query = $this->getDiscountModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>