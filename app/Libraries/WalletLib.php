<?php

namespace App\Libraries;

use App\Wallet;
use Illuminate\Support\Facades\Auth;

class WalletLib {

    protected $_wallet = null;

    public function getWallet() {
        if (!($this->_wallet instanceof \App\Wallet)) {
            $this->_wallet = new \App\Wallet();
        }
        return $this->_wallet;
    }

    public function getWalletUserDataById($id, $fields = "*") {
        $query = $this->getWallet()
                ->select($fields)
                ->where('user_id', $id)
                ->orderBy('id', 'desc');
        return $query->first();
    }

}

?>