<?php

namespace App\Libraries;

use App\Leadstatus;
use Illuminate\Support\Facades\Auth;

class LeadstatusLib {

    protected $_leadstatusmasterModel = null;

    public function getLeadstatusmasterModel() {
        if (!($this->_leadstatusmasterModel instanceof \App\Leadstatus)) {
            $this->_leadstatusmasterModel = new \App\Leadstatus();
        }
        return $this->_leadstatusmasterModel;
    }

    public function addLeadstatusmaster($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getLeadstatusmasterModel()->insertGetId($data);
    }

    public function updateLeadstatusmaster($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadstatusmasterModel()->where('typeId', $id)->update($data);
    }

    public function deleteLeadstatusmaster($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadstatusmasterModel()->where('typeId', $id)->update($data);
    }

    public function getLeadstatusmasterById($id) {
        $query = $this->getLeadstatusmasterModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getLeadstatusmaster($fields = "*", $all = false) {
        $query = $this->getLeadstatusmasterModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>