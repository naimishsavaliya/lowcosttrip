<?php

namespace App\Libraries;

use App\Accomodation;
use Illuminate\Support\Facades\Auth;

class AccomodationLib {

    protected $_accomodationModel = null;

    public function getAccomodationModel() {
        if (!($this->_accomodationModel instanceof \App\Accomodation)) {
            $this->_accomodationModel = new \App\Accomodation();
        }
        return $this->_accomodationModel;
    }

    public function addAccomodation($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getAccomodationModel()->insertGetId($data);
    }

    public function updateAccomodation($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAccomodationModel()->where('id', $id)->update($data);
    }

    public function deleteAccomodation($id) {
        return $this->getAccomodationModel()->where('id', $id)->delete();
    }
    public function deleteAccomodationByLeadId($lead_id) {
        return $this->getAccomodationModel()->where('lead_id', $lead_id)->delete();
    }

    public function getAccomodationById($id) {
        $query = $this->getAccomodationModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getAccomodation($fields = "*", $params = array()) {
        $query = $this->getAccomodationModel()
                ->select($fields)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>