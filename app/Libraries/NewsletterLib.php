<?php

namespace App\Libraries;

use App\Newsletter;
use Illuminate\Support\Facades\Auth;

class NewsletterLib {

    protected $_newsletter = null;

    public function getNewsletter() {
        if (!($this->_newsletter instanceof \App\Newsletter)) {
            $this->_newsletter = new \App\Newsletter();
        }
        return $this->_newsletter;
    }

    public function addNewsletter($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getNewsletter()->insertGetId($data);
    }

}

?>