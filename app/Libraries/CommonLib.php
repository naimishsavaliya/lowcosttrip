<?php

use App\Userprofile;

namespace App\Libraries;

class CommonLib {

    protected $_getCommandData = null;

    public function getCommondataModal() {
        return $this->_getCommandData;
    }

    public function getTimezoneData() {
        $timezones = array('(GMT-11:00) Midway Island' => 'Pacific/Midway', '(GMT-11:00) Samoa' => 'Pacific/Samoa', '(GMT-10:00) Hawaii' => 'Pacific/Honolulu', '(GMT-09:00) Alaska' => 'US/Alaska', '(GMT-08:00) Pacific Time (US &amp; Canada)' => 'America/Los_Angeles', '(GMT-08:00) Tijuana' => 'America/Tijuana', '(GMT-07:00) Arizona' => 'US/Arizona', '(GMT-07:00) Chihuahua' => 'America/Chihuahua', '(GMT-07:00) La Paz' => 'America/Chihuahua', '(GMT-07:00) Mazatlan' => 'America/Mazatlan', '(GMT-07:00) Mountain Time (US &amp; Canada)' => 'US/Mountain', '(GMT-06:00) Central America' => 'America/Managua', '(GMT-06:00) Central Time (US &amp; Canada)' => 'US/Central', '(GMT-06:00) Guadalajara' => 'America/Mexico_City', '(GMT-06:00) Mexico City' => 'America/Mexico_City', '(GMT-06:00) Monterrey' => 'America/Monterrey', '(GMT-06:00) Saskatchewan' => 'Canada/Saskatchewan', '(GMT-05:00) Bogota' => 'America/Bogota', '(GMT-05:00) Eastern Time (US &amp; Canada)' => 'US/Eastern', '(GMT-05:00) Indiana (East)' => 'US/East-Indiana', '(GMT-05:00) Lima' => 'America/Lima', '(GMT-05:00) Quito' => 'America/Bogota', '(GMT-04:00) Atlantic Time (Canada)' => 'Canada/Atlantic', '(GMT-04:30) Caracas' => 'America/Caracas', '(GMT-04:00) La Paz' => 'America/La_Paz', '(GMT-04:00) Santiago' => 'America/Santiago', '(GMT-03:30) Newfoundland' => 'Canada/Newfoundland', '(GMT-03:00) Brasilia' => 'America/Sao_Paulo', '(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires', '(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires', '(GMT-03:00) Greenland' => 'America/Godthab', '(GMT-02:00) Mid-Atlantic' => 'America/Noronha', '(GMT-01:00) Azores' => 'Atlantic/Azores', '(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde', '(GMT+00:00) Casablanca' => 'Africa/Casablanca', '(GMT+00:00) Edinburgh' => 'Europe/London', '(GMT+00:00) Greenwich Mean Time : Dublin' => 'Etc/Greenwich', '(GMT+00:00) Lisbon' => 'Europe/Lisbon', '(GMT+00:00) London' => 'Europe/London', '(GMT+00:00) Monrovia' => 'Africa/Monrovia', '(GMT+00:00) UTC' => 'UTC', '(GMT+01:00) Amsterdam' => 'Europe/Amsterdam', '(GMT+01:00) Belgrade' => 'Europe/Belgrade', '(GMT+01:00) Berlin' => 'Europe/Berlin', '(GMT+01:00) Bern' => 'Europe/Berlin', '(GMT+01:00) Bratislava' => 'Europe/Bratislava', '(GMT+01:00) Brussels' => 'Europe/Brussels', '(GMT+01:00) Budapest' => 'Europe/Budapest', '(GMT+01:00) Copenhagen' => 'Europe/Copenhagen', '(GMT+01:00) Ljubljana' => 'Europe/Ljubljana', '(GMT+01:00) Madrid' => 'Europe/Madrid', '(GMT+01:00) Paris' => 'Europe/Paris', '(GMT+01:00) Prague' => 'Europe/Prague', '(GMT+01:00) Rome' => 'Europe/Rome', '(GMT+01:00) Sarajevo' => 'Europe/Sarajevo', '(GMT+01:00) Skopje' => 'Europe/Skopje', '(GMT+01:00) Stockholm' => 'Europe/Stockholm', '(GMT+01:00) Vienna' => 'Europe/Vienna', '(GMT+01:00) Warsaw' => 'Europe/Warsaw', '(GMT+01:00) West Central Africa' => 'Africa/Lagos', '(GMT+01:00) Zagreb' => 'Europe/Zagreb', '(GMT+02:00) Athens' => 'Europe/Athens', '(GMT+02:00) Bucharest' => 'Europe/Bucharest', '(GMT+02:00) Cairo' => 'Africa/Cairo', '(GMT+02:00) Harare' => 'Africa/Harare', '(GMT+02:00) Helsinki' => 'Europe/Helsinki', '(GMT+02:00) Istanbul' => 'Europe/Istanbul', '(GMT+02:00) Jerusalem' => 'Asia/Jerusalem', '(GMT+02:00) Kyiv' => 'Europe/Helsinki', '(GMT+02:00) Pretoria' => 'Africa/Johannesburg', '(GMT+02:00) Riga' => 'Europe/Riga', '(GMT+02:00) Sofia' => 'Europe/Sofia', '(GMT+02:00) Tallinn' => 'Europe/Tallinn', '(GMT+02:00) Vilnius' => 'Europe/Vilnius', '(GMT+03:00) Baghdad' => 'Asia/Baghdad', '(GMT+03:00) Kuwait' => 'Asia/Kuwait', '(GMT+03:00) Minsk' => 'Europe/Minsk', '(GMT+03:00) Nairobi' => 'Africa/Nairobi', '(GMT+03:00) Riyadh' => 'Asia/Riyadh', '(GMT+03:00) Volgograd' => 'Europe/Volgograd', '(GMT+03:30) Tehran' => 'Asia/Tehran', '(GMT+04:00) Abu Dhabi' => 'Asia/Muscat', '(GMT+04:00) Baku' => 'Asia/Baku', '(GMT+04:00) Moscow' => 'Europe/Moscow', '(GMT+04:00) Muscat' => 'Asia/Muscat', '(GMT+04:00) St. Petersburg' => 'Europe/Moscow', '(GMT+04:00) Tbilisi' => 'Asia/Tbilisi', '(GMT+04:00) Yerevan' => 'Asia/Yerevan', '(GMT+04:30) Kabul' => 'Asia/Kabul', '(GMT+05:00) Islamabad' => 'Asia/Karachi', '(GMT+05:00) Karachi' => 'Asia/Karachi', '(GMT+05:00) Tashkent' => 'Asia/Tashkent', '(GMT+05:30) Chennai' => 'Asia/Calcutta', '(GMT+05:30) Kolkata' => 'Asia/Kolkata', '(GMT+05:30) Mumbai' => 'Asia/Calcutta', '(GMT+05:30) New Delhi' => 'Asia/Calcutta', '(GMT+05:30) Sri Jayawardenepura' => 'Asia/Calcutta', '(GMT+05:45) Kathmandu' => 'Asia/Katmandu', '(GMT+06:00) Almaty' => 'Asia/Almaty', '(GMT+06:00) Astana' => 'Asia/Dhaka', '(GMT+06:00) Dhaka' => 'Asia/Dhaka', '(GMT+06:00) Ekaterinburg' => 'Asia/Yekaterinburg', '(GMT+06:30) Rangoon' => 'Asia/Rangoon', '(GMT+07:00) Bangkok' => 'Asia/Bangkok', '(GMT+07:00) Hanoi' => 'Asia/Bangkok', '(GMT+07:00) Jakarta' => 'Asia/Jakarta', '(GMT+07:00) Novosibirsk' => 'Asia/Novosibirsk', '(GMT+08:00) Beijing' => 'Asia/Hong_Kong', '(GMT+08:00) Chongqing' => 'Asia/Chongqing', '(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong', '(GMT+08:00) Krasnoyarsk' => 'Asia/Krasnoyarsk', '(GMT+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur', '(GMT+08:00) Perth' => 'Australia/Perth', '(GMT+08:00) Singapore' => 'Asia/Singapore', '(GMT+08:00) Taipei' => 'Asia/Taipei', '(GMT+08:00) Ulaan Bataar' => 'Asia/Ulan_Bator', '(GMT+08:00) Urumqi' => 'Asia/Urumqi', '(GMT+09:00) Irkutsk' => 'Asia/Irkutsk', '(GMT+09:00) Osaka' => 'Asia/Tokyo', '(GMT+09:00) Sapporo' => 'Asia/Tokyo', '(GMT+09:00) Seoul' => 'Asia/Seoul', '(GMT+09:00) Tokyo' => 'Asia/Tokyo', '(GMT+09:30) Adelaide' => 'Australia/Adelaide', '(GMT+09:30) Darwin' => 'Australia/Darwin', '(GMT+10:00) Brisbane' => 'Australia/Brisbane', '(GMT+10:00) Canberra' => 'Australia/Canberra', '(GMT+10:00) Guam' => 'Pacific/Guam', '(GMT+10:00) Hobart' => 'Australia/Hobart', '(GMT+10:00) Melbourne' => 'Australia/Melbourne', '(GMT+10:00) Port Moresby' => 'Pacific/Port_Moresby', '(GMT+10:00) Sydney' => 'Australia/Sydney', '(GMT+10:00) Yakutsk' => 'Asia/Yakutsk', '(GMT+11:00) Vladivostok' => 'Asia/Vladivostok', '(GMT+12:00) Auckland' => 'Pacific/Auckland', '(GMT+12:00) Fiji' => 'Pacific/Fiji', '(GMT+12:00) International Date Line West' => 'Pacific/Kwajalein', '(GMT+12:00) Kamchatka' => 'Asia/Kamchatka', '(GMT+12:00) Magadan' => 'Asia/Magadan', '(GMT+12:00) Marshall Is.' => 'Pacific/Fiji', '(GMT+12:00) New Caledonia' => 'Asia/Magadan', '(GMT+12:00) Solomon Is.' => 'Asia/Magadan', '(GMT+12:00) Wellington' => 'Pacific/Auckland', '(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu');
        return $timezones;
    }

    public function getHomepagebanneroption() {
        $banneroptArr = array(
            1 => '1 Image',
            2 => '2 Image',
            3 => '3 Image'
        );
        return $banneroptArr;
    }

    public function getHomepagebannercategory() {
        $categoryArr = array(
            1 => 'Holidays',
            2 => 'Flights',
            3 => 'Hotels',
            4 => 'Activity'
        );
        return $categoryArr;
    }

    public function getLocationTourType($key = 0) {
        $categoryArr = array(
            1 => 'Domestic',
            2 => 'International',
        );
        if ($key > 0) {
            return $categoryArr[$key];
        }
        return $categoryArr;
    }

    public function getTourDisplay() {
        $categoryArr = array(
            1 => 'Public',
            2 => 'Private',
        );
        return $categoryArr;
    }

    public function getTourGroup($key = '') {
        $categoryArr = array(
            1 => 'Group',
            2 => 'Customized',
        );
        if ($key > 0) {
            return $categoryArr[$key];
        }
        return $categoryArr;
    }

    /*
     * Retrun lead intrested in
     */

    public function getLeadIntrestedin($key = 0) {
        $intrested_arr = array(
            // 1 => 'Forex',
            2 => 'Flight',
            3 => 'Hotel',
            4 => 'Tour',
            // 5 => 'Transportation',
            // 6 => 'Activity',
            7 => 'Holiday',
            8 => 'Visa',
        );
        if ($key > 0) {
            return $intrested_arr[$key];
        }
        return $intrested_arr;
    }

    public function getLeadIntrestedinid($key = '') {
        $data = array(
            1 => 'Forex',
            2 => 'Flights',
            3 => 'Hotel Room',
            4 => 'Tour',
            5 => 'Transportation',
            6 => 'Activity',
            7 => 'Holiday',
            8 => 'Visa',
        );
        if ($key != '') {
            $input = $key;

            $result = array_filter($data, function ($item) use ($input) {
                if (stripos($item, $input) !== false) {
                    return true;
                }
                return false;
            });
             
            if(isset($result) && count($result) > 0)
                return array_keys($result);
        }
        return array();
    }

    /*
     * Return Lead Source
     */

    public function getLeadSources($key = 0, $search = 0) {
        $lead_source = array(
            1 => 'Web',
            2 => 'Phone',
            3 => 'Walk in',
            4 => 'Agent',
            5 => 'Ref',
        );
        if($search == 0){
            if ($key > 0) {
                return $lead_source[$key];
            }
        } 
        if($search == 1){
            if ($key != '') {
                $input = $key;
                $result = array_filter($lead_source, function ($item) use ($input) {
                    if (stripos($item, $input) !== false) {
                        return true;
                    }
                    return false;
                });
                if(isset($result) && count($result) > 0)
                    return array_keys($result);
            }
            return array();
        }
        return $lead_source;
    }

    /*
     * Return Lead place types
     */

    public function getLeadIsPlaceType() {
        $lead_sources = array(
            1 => 'Domestic',
            2 => 'International'
        );
        return $lead_sources;
    }

    /*
     * Return Lead Customized option
     */

    public function getLeadCustomizedOpt() {
        $lead_customized_opt = array(
            1 => 'Group',
            2 => 'Customised (FIT)'
        );
        return $lead_customized_opt;
    }

    /*
     * Return Quotation Status
     */

    // public function getQuotationStatus($key = 0) {
    //     $quotation_arr = array(
    //         1 => 'Draft',
    //         2 => 'Quote Sent',
    //         3 => 'Quote Approved',
    //         4 => 'Quote Rejected',
    //     );
    //     if ($key > 0) {
    //         return $quotation_arr[$key];
    //     }
    //     return $quotation_arr;
    // }

    public function getQuotationStatus($key = 0,$search = 0) {
        $quotation_arr = array(
            1 => 'Draft',
            2 => 'Quote Sent',
            3 => 'Quote Approved',
            4 => 'Quote Rejected',
        );
        if($search == 0){
            if ($key > 0) {
                return $quotation_arr[$key];
            }
        } 
        if($search == 1){
            if ($key != '') {
                $input = $key;
                $result = array_filter($quotation_arr, function ($item) use ($input) {
                    if (stripos($item, $input) !== false) {
                        return true;
                    }
                    return false;
                });
                if(isset($result) && count($result) > 0)
                    return array_keys($result);
            }
            return array();
        }
        return $quotation_arr;
    }

    /*
     * Return Agent Type
     */

    public function getAgentType($key = 0) {
        $agent_arr = array(
            1 => 'Agent',
            2 => 'Retailer',
            3 => 'Wholesaler',
        );
        if ($key > 0) {
            return $agent_arr[$key];
        }
        return $agent_arr;
    }

    /*
     * Return Booking Per Month
     */

    public function getBookingPerMonth($key = 0) {
        $booking_arr = array(
            1 => '0-5',
            2 => '5-15',
            3 => '15-30',
            4 => '30+',
        );
        if ($key > 0) {
            return $booking_arr[$key];
        }
        return $booking_arr;
    }

    /*
     * Return No Of employees
     */

    public function getTotalEmployees($key = 0) {
        $employees_arr = array(
            1 => '1-5',
            2 => '5-15',
            3 => '15-30',
            4 => '30-45',
            5 => '45+',
        );
        if ($key > 0) {
            return $employees_arr[$key];
        }
        return $employees_arr;
    }

    /*
     * Return Year of Registation
     */

    public function getYearOfRegistation($key = 0) {
        $year_arr = array();
        for ($year = 1990; $year <= date('Y'); $year++) {
            $year_arr[$year] = $year;
        }
        if ($key > 0) {
            return $year_arr[$key];
        }
        return $year_arr;
    }

    /*
     * Return company type
     */

    public function getCompanyType($key = 0) {
        $company_arr = array(
            1 => 'Proprietor',
            2 => 'Partnership',
            3 => 'Pvt Ltd',
            4 => 'Llc',
        );
        if ($key > 0) {
            return $company_arr[$key];
        }
        return $company_arr;
    }

    /*
     * Return Hear About Us
     */

    public function getHearAboutUS($key = 0) {
        $head_about_arr = array(
            1 => "How did you hear about us?",
            2 => "Via Google Search",
            3 => "Via Friend Reference",
            4 => "Via Email",
            5 => "Via Facebook",
            6 => "Via Twitter",
            7 => "Via Linkedin",
            8 => "Via Instagram",
            9 => "Other",
        );
        if ($key > 0) {
            return $head_about_arr[$key];
        }
        return $head_about_arr;
    }

    /*
     * Return Affiliations
     */

    public function getAffiliationsType($key = 0) {
        $affliations_arr = array(
            1 => "Iata",
            2 => "Tafi",
            3 => "Swiss",
            4 => "Others",
        );
        if ($key > 0) {
            return $affliations_arr[$key];
        }
        return $affliations_arr;
    }

    /*
     * Return Product Servies
     */

    public function getProductServices($key = 0) {
        $product_servies_arr = array(
            1 => 'Holiday packages',
            2 => 'Air',
            3 => 'Hotel / weekend getaways',
            4 => 'Visa',
            5 => 'Activities',
        );
        if ($key > 0) {
            return $product_servies_arr[$key];
        }
        return $product_servies_arr;
    }
    
    /*
     * Return Discont for
     */
    public function getDiscountFor($key = 0) {
        $discount_for_arr = array(
            1 => 'Hotel',
            2 => 'Tour',
            3 => 'Flight',
            4 => 'Activities',
        );
        if ($key > 0) {
            return $discount_for_arr[$key];
        }
        return $discount_for_arr;
    }
    
    public function tourBudget($key = 0) {
        $arr = array(
            '0-10000' => 'Upto 10,000',
            '10000-25000' => '10,000-25,000',
            '25000-50000' => '25,000-50,000',
            '50000-75000' => '50,000-75,000',
            '75000' => '75000+',
        );
        if ($key > 0) {
            return $arr[$key];
        }
        return $arr;
    }

    public function tourDuration($key = 0) {
        $arr = array(
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
            6 => '6',
            7 => '7',
            8 => '8',
            9 => '9',
            10 => '10',
            11 => '11',
            12 => '12',
            13 => '13',
            14 => '14',
            15 => '15',
            16 => '16',
            17 => '17',
            18 => '18',
            19 => '19',
            20 => '20',
            21 => '21',
            22 => '22',
            23 => '23',
            24 => '24',
            25 => '25',
            26 => '25+',
        );
        if ($key > 0) {
            return $arr[$key];
        }
        return $arr;
    }

     public function hotelChoice($key = 0) {
        $arr = array(
            1 => '*',
            2 => '**',
            3 => '***',
            4 => '****',
            5 => '*****',
        );
        if ($key > 0) {
            return $arr[$key];
        }
        return $arr;
    }
    
     public function toursSortBy($key = 0) {
        $arr = array(
            1 => 'New & Popular',
            2 => 'Featured',
            3 => 'Price: Low to High',
            4 => 'Price: High to Low',
            5 => 'Avg. Customer Review',
            6 => 'Newest Arrivals',
        );
        if ($key > 0) {
            return $arr[$key];
        }
        return $arr;
    }

    public function type_of_payment($key = 0) {
        $arr = array(
            'Online'   => 'Online',
            'Offline'   => 'Offline'
        );
        if ($key > 0) {
            return $arr[$key];
        }
        return $arr;
    }

    public function paymentType($key = 0) {
        $arr = array(
            'Cash'    => 'Cash',
            'Bank deposit'    => 'Bank deposit',
            'NEFT'    => 'NEFT',
            'Online'    => 'Online',
            'Cheque'    => 'Cheque'
        );
        if ($key > 0) {
            return $arr[$key];
        }
        return $arr;
    }

    public function paymentMode($key = 0) {
        $arr = array(
            'Advance'   => 'Advance',
            'Part'   => 'Part',
            'Full payment'   => 'Full payment'
        );
        if ($key > 0) {
            return $arr[$key];
        }
        return $arr;
    }

    public function getusermoduleroleinid($key = '') {
        $data = array(
          1 => 'Admin',
          2 => 'Manager'
        );
        if ($key != '') {
            $input = $key;

            $result = array_filter($data, function ($item) use ($input) {
                if (stripos($item, $input) !== false) {
                    return true;
                }
                return false;
            });
             
            if(isset($result) && count($result) > 0)
                return array_keys($result);
        }
        return array();
    }

    public function getsectorCategoryId($key = '') {
        $data = array(
          1 => 'Domestic',
          2 => 'International',
          3 => 'Pilgrimage'
        );
        if ($key != '') {
            $input = $key;

            $result = array_filter($data, function ($item) use ($input) {
                if (stripos($item, $input) !== false) {
                    return true;
                }
                return false;
            });
             
            if(isset($result) && count($result) > 0)
                return array_keys($result);
        }
        return array();
    }

    public function getLocationTourTypeForDatatable($key = '') {
        $categoryArr = array(
            1 => 'Domestic',
            2 => 'International',
        );
        if ($key != '') {
            $input = $key;

            $result = array_filter($categoryArr, function ($item) use ($input) {
                if (stripos($item, $input) !== false) {
                    return true;
                }
                return false;
            });
             
            if(isset($result) && count($result) > 0)
                return array_keys($result);
        }
        return array();
    }

}

?>
