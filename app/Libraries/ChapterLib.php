<?php

namespace App\Libraries;

use App\Chapter;
use Illuminate\Support\Facades\Auth;

class ChapterLib {

	protected $_chapterModel = null;

	public function getChapterModel() {
		if (!($this->_chapterModel instanceof \App\Chapter)) {
			$this->_chapterModel = new \App\Chapter();
		}
		return $this->_chapterModel;
	}

	public function addChapter($data) {
		$data['created_at'] = date("Y-m-d H:i:s");
		$data['created_by'] = Auth::id();
		return $query = $this->getChapterModel()->insertGetId($data);
	}

	public function updateChapter($data, $id) {
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getChapterModel()->where('chapter_id', $id)->update($data);
	}

	public function deleteChapter($id) {
		$data = array();
		$data['deleted_at'] = date("Y-m-d H:i:s");
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getChapterModel()->where('chapter_id', $id)->update($data);
	}

	public function getChapterById($id) {
		$query = $this->getChapterModel()
				->where('chapter_id', $id);
		return $query->first();
	}

	public function getChapter($fields = "*", $all = false) {
		$query = $this->getChapterModel()
				->select($fields)
				->orderBy('chapter_name', 'ASC');
		if (!$all) {
			$query->where('deleted_at', null)->where('status',1);
		}
		return $query->get();
	}
	
}

?>