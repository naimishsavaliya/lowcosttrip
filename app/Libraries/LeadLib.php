<?php

namespace App\Libraries;

use App\Lead;
use DB;
use Illuminate\Support\Facades\Auth;

class LeadLib {

    protected $_leadModel = null;

    public function getLeadModel() {
        if (!($this->_leadModel instanceof \App\Lead)) {
            $this->_leadModel = new \App\Lead();
        }
        return $this->_leadModel;
    }

    public function addLead($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        $data['lead_id'] =  "LCT". (10000 + $this->getNextAutoID("lead"));
        return $query = $this->getLeadModel()->insertGetId($data);
    }

	public function getNextAutoID($table_name) {
		$statement = \DB::select("show table status like '{$table_name}'");
		return $statement[0]->Auto_increment;
	}
	
    public function updateLead($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadModel()->where('id', $id)->update($data);
    }

    public function deleteLead($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadModel()->where('id', $id)->update($data);
    }

    public function getLeadById($id, $fields = "*") {
        $query = $this->getLeadModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getLead($fields = "*", $all = false) {
        $query = $this->getLeadModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null);
        }
        return $query->get();
    }

    public function getLeadByNuber($lead_no, $fields = "*") {
        $query = $this->getLeadModel()
                ->select($fields)
                ->where('lead_id', strtoupper($lead_no));
        return $query->first();
    }
    
    public function getLeadHeaderInfoById($id) {
        $query = $this->getLeadModel()
                ->select('lead.*','users.first_name','users.last_name','master_lead_status.name as status')
                ->leftJoin('users', 'lead.created_by', '=', 'users.id')
                ->leftJoin('master_lead_status', 'lead.status_id', '=', 'master_lead_status.typeId')
                ->where('lead.id', $id);
        return $query->first();
    }

    public function getLead_data($where) {
        $query = $this->getLeadModel()
                ->select('*')
                ->where($where);
        return $query->get();
    }

    public function getLeadForBookTour($where) {
        $query = $this->getLeadModel()
                ->select('lead.*','tour.tour_name','tour.tour_code','tour.tour_nights','tour.image')
                ->leftJoin('tour', 'lead.tour_id', '=', 'tour.id')
                ->orderBy('created_at', 'DESC')
                ->where($where);
        return $query->get();
    }

    public function getLeadInfo($id) {
        $query = $this->getLeadModel()
                ->select('lead.*','users.first_name','users.last_name', 'country.country_name', 'state.state_name', 'city.city_name', 'users_profile.address')
                ->leftJoin('users', 'lead.created_by', '=', 'users.id')
                ->leftJoin('users_profile', 'lead.customer_id', '=', 'users_profile.user_id')
                ->leftJoin('country', 'lead.country_id', '=', 'country.country_id')
                ->leftJoin('state', 'lead.state_id', '=', 'state.state_id')
                ->leftJoin('city', 'lead.city_id', '=', 'city.city_id')
                ->where('lead.id', $id);
        return $query->first();
    }

    // public function getLeadQuatationById($lead_id, $quotation_id, $fields = "*") {
    //     $query = $this->getLeadQuatationModel()
    //             ->select('lead_quatation.*', 'country.country_name', 'state.state_name', 'city.city_name', 'users.first_name', 'users.last_name', 'lead.lead_id as lead_no')
    //             ->leftJoin('lead', 'lead_quatation.lead_id', '=', 'lead.id')
    //             ->leftJoin('country', 'lead_quatation.country_id', '=', 'country.country_id')
    //             ->leftJoin('state', 'lead_quatation.state_id', '=', 'state.state_id')
    //             ->leftJoin('city', 'lead_quatation.city_id', '=', 'city.city_id')
    //             ->leftJoin('users', 'lead_quatation.created_by', '=', 'users.id')
    //             ->where('lead_quatation.lead_id', $lead_id)
    //             ->where('quaId', $quotation_id);
    //     return $query->first();
    // }

    // public function get_lead_data_for_cron($lead_id) {
    //     $query = $this->getLeadModel()
    //             ->select('id','lead_id','name','email')
    //             ->where('deleted_at', null)
    //             ->whereIn('id', $lead_id)
    //             ->orderBy('id', 'ASC');
    //     return $query->get();
    // }

    public function get_lead_data_for_cron($lead_id) {
        $query = $this->getLeadModel()
                ->select('lead.id','lead.lead_id','lead.name as lead_title','lead.email',DB::raw('CONCAT(users.first_name, " ", users.last_name) AS full_name'))
                ->leftJoin('users', 'lead.customer_id', '=', 'users.id')
                ->where('lead.id', $lead_id);
        return $query->first();
    }

    public function getLeadCounter( $status_id, $intrest_in, $month) {
        $query = $this->getLeadModel()
                ->select('id', 'status_id', 'interested_in', 'created_at')
                ->where('deleted_at', null)
                // ->whereRaw('year(`created_at`) = ?', array(date('Y')))
                ->orderBy('created_at', 'ASC');
        if($status_id > 0){
            $query->where('status_id', $status_id);
        }
        if($intrest_in > 0){
            $query->where('interested_in', $intrest_in);
        }
        if($month > 0){
            $month_year = explode('-', $month);
            $query->whereMonth('created_at', '=', $month_year[0]);
            $query->whereYear('created_at', '=', $month_year[1]);
        }
        return $query->get();
    }

    public function getleadfilter($start_date,$end_date) {
        $query = $this->getLeadModel()
        ->selectRaw("id,status_id,interested_in,deleted_at,DATE_FORMAT(created_at,'%Y-%m-%d') as created_at");
            $query->where('deleted_at', null);
            $query->whereBetween("created_at", [$start_date, $end_date]);
            //dd($query->toSql());
        return $query->get();
    }


}

?>