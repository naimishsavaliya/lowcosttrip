<?php

namespace App\Libraries;

use App\Albumvideo;
use Illuminate\Support\Facades\Auth;

class AlbumvideoLib {

    protected $_albumvideoModel = null;

    public function getAlbumvideoModel() {
        if (!($this->_albumvideoModel instanceof \App\Albumvideo)) {
            $this->_albumvideoModel = new \App\Albumvideo();
        }
        return $this->_albumvideoModel;
    }

    public function addAlbumvideo($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getAlbumvideoModel()->insertGetId($data);
    }

    public function updateAlbumvideo($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAlbumvideoModel()->where('vidId', $id)->update($data);
    }

    public function deleteAlbumvideo($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAlbumvideoModel()->where('vidId', $id)->update($data);
    }

    public function getAlbumvideoById($id) {
        $query = $this->getAlbumvideoModel()
                ->where('vidId', $id);
        return $query->first();
    }

    public function getAlbumvideo($fields = "*", $all = false) {
        $query = $this->getAlbumvideoModel()
                ->select($fields)
                ->where('deleted_at',null)
                ->orderBy('title', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>