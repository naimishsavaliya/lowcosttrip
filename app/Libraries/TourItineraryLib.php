<?php

namespace App\Libraries;

use App\TourItinerary;
use Illuminate\Support\Facades\Auth;

class TourItineraryLib {

    protected $_tourIneraryModel = null;

    public function getTourIneraryModel() {
        if (!($this->_tourIneraryModel instanceof \App\TourItinerary)) {
            $this->_tourIneraryModel = new \App\TourItinerary();
        }
        return $this->_tourIneraryModel;
    }

    public function addTourInerary($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourIneraryModel()->insertGetId($data);
    }

    public function updateTourInerary($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourIneraryModel()->where('id', $id)->update($data);
    }

    public function deleteTourInerary($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourIneraryModel()->where('id', $id)->update($data);
    }

    public function deleteTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourIneraryModel()->where('tour_id', $id)->update($data);
    }

    public function getTourIneraryById($id) {
        $query = $this->getTourIneraryModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTourInerary($fields = "*", $params = array()) {
        $query = $this->getTourIneraryModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderByRaw('CAST(tour_day as SIGNED INTEGER) ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function getItineraryByTour($tour_id, $fields = "*", $params = array()) {
        $query = $this->getTourIneraryModel()
                ->select($fields)
                ->where('tour_id', $tour_id)
                ->where('deleted_at', null)
                ->orderBy('tour_day', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>