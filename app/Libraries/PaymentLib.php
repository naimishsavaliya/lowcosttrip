<?php

namespace App\Libraries;

use App\Payments;
use App\Payment_response;
use Illuminate\Support\Facades\Auth;

class PaymentLib {

    protected $_paymentModel = null;
    protected $_paymentResponseModel = null;

    public function getPaymentModel() {
        if (!($this->_paymentModel instanceof \App\Payments)) {
            $this->_paymentModel = new \App\Payments();
        }
        return $this->_paymentModel;
    }
    public function getPayment_responseModel() {
        if (!($this->_paymentResponseModel instanceof \App\Payment_response)) {
            $this->_paymentResponseModel = new \App\Payment_response();
        }
        return $this->_paymentResponseModel;
    }

    public function addPayment($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getPaymentModel()->insertGetId($data);
    }

    public function addPaymentResponse($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getPayment_responseModel()->insertGetId($data);
    }
    public function getPaymentLeadNo($tra_id) {
        $query = $this->getPayment_responseModel()
                ->where('transaction_id', $tra_id);
        return $query->first();
    }

    public function updatePayment($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getPaymentModel()->where('id', $id)->update($data);
    }

    public function deletePayment($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getPaymentModel()->where('id', $id)->update($data);
    }

    public function getPaymentById($id) {
        $query = $this->getPaymentModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getPayment($fields = "*", $params = array()) {
        $query = $this->getPaymentModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('customer_name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>