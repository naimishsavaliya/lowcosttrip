<?php

namespace App\Libraries;

use App\Roomfacilitis;
use Illuminate\Support\Facades\Auth;

class RoomfacilitiesLib {

    protected $_roomfacilitiesModel = null;

    public function getRoomfacilitiesModel() {
        if (!($this->_roomfacilitiesModel instanceof \App\Roomfacilitis)) {
            $this->_roomfacilitiesModel = new \App\Roomfacilitis();
        }
        return $this->_roomfacilitiesModel;
    }

    public function addRoomFacilities($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getRoomfacilitiesModel()->insertGetId($data);
    }

    public function updateRoomFacilities($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getRoomfacilitiesModel()->where('facId', $id)->update($data);
    }

    public function deleteRoomFacilities($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getRoomfacilitiesModel()->where('facId', $id)->update($data);
    }

    public function getRoomFacilitiesById($id) {
        $query = $this->getRoomfacilitiesModel()
                ->where('facId', $id);
        return $query->first();
    }

    public function getRoomFacilities($fields = "*", $params = array()) {
        $query = $this->getRoomfacilitiesModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>