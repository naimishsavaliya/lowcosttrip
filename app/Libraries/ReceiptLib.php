<?php

namespace App\Libraries;

use App\Receipt;
use Illuminate\Support\Facades\Auth;

class ReceiptLib {

    protected $_receiptModel = null;

    public function getReceiptModel() {
        if (!($this->_receiptModel instanceof \App\Receipt)) {
            $this->_receiptModel = new \App\Receipt();
        }
        return $this->_receiptModel;
    }

    public function getReceiptNumber() {
        $query = $this->getReceiptModel()->where('type', 'payment');
        $receipt_data = $query->first();
        if (isset($receipt_data) && $receipt_data != null) {
            $receipt = $receipt_data->current_receipt_number;
        } else {
            $receipt = $data['start_receipt_number'] = "10001";
            $data['current_receipt_number'] = "10001";
            $data['type'] = "payment";
            $data['created_at'] = date("Y-m-d H:i:s");
            $this->getReceiptModel()->insertGetId($data);
        }
        return $receipt;
    }

    public function updateReceiptNumber() {
        $recept = $this->getReceiptNumber();
        $data['current_receipt_number'] = ($recept + 1);
        $data['updated_at'] = date("Y-m-d H:i:s");
        return $this->getReceiptModel()->where('type', 'payment')->where('current_receipt_number', $recept)->update($data);
    }

    public function getProformaReceiptNumber() {
        $query = $this->getReceiptModel()->where('type', 'proforma');
        $receipt_data = $query->first();
        if (isset($receipt_data) && $receipt_data != null) {
            $receipt = $receipt_data->current_receipt_number;
        } else {
            $receipt = $data['start_receipt_number'] = "10001";
            $data['current_receipt_number'] = "10001";
            $data['type'] = "proforma";
            $data['created_at'] = date("Y-m-d H:i:s");
            $this->getReceiptModel()->insertGetId($data);
        }
        return $receipt;
    }

    public function updateProformaReceiptNumber() {
        $recept = $this->getProformaReceiptNumber();
        // echo $recept;
        $data['current_receipt_number'] = ($recept + 1);
        $data['updated_at'] = date("Y-m-d H:i:s");
        // echo "<pre>";
        // print_r( $data);
        // die;
        return $this->getReceiptModel()->where('type', 'proforma')->where('current_receipt_number', $recept)->update($data);
    }

}

?>