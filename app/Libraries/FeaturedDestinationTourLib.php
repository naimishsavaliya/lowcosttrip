<?php

namespace App\Libraries;

use App\FeaturedDestinationTour;
use Illuminate\Support\Facades\Auth;

class FeaturedDestinationTourLib {

    protected $_featureddestinationtour = null;

    public function getFeaturedDestinationTourModel() {
        if (!($this->_featureddestinationtour instanceof \App\FeaturedDestinationTour)) {
            $this->_featureddestinationtour = new \App\FeaturedDestinationTour();
        }
        return $this->_featureddestinationtour;
    }

    public function addFeaturedDestinationTour($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getFeaturedDestinationTourModel()->insertGetId($data);
    }

    public function updateFeaturedDestinationTour($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getFeaturedDestinationTourModel()->where('id', $id)->update($data);
    }

    public function deleteFeaturedDestinationTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getFeaturedDestinationTourModel()->where('id', $id)->update($data);
    }

    public function getFeaturedDestinationTourById($id, $fields = "*") {
        $query = $this->getFeaturedDestinationTourModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getFeaturedDestinationTours($params = array()) {
        $query = $this->getFeaturedDestinationTourModel()
                ->select('featured_destination_tours.id','featured_destination_tours.tour_id','featured_destination_tours.title','featured_destination_tours.start_date','featured_destination_tours.end_date','tour.tour_name','tour.tour_nights','tour.image')
                ->leftJoin('tour', 'tour.id', '=', 'featured_destination_tours.tour_id');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        $query->orderBy('featured_destination_tours.id', 'DESC');
        $query->limit(4);
        return $query->get();
    }

}

?>