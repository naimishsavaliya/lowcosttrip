<?php

namespace App\Libraries;

use App\Sectors;
use Illuminate\Support\Facades\Auth;

class SectorLib {

    protected $_sectorModel = null;

    public function getSectorModel() {
        if (!($this->_sectorModel instanceof \App\Sectors)) {
            $this->_sectorModel = new \App\Sectors();
        }
        return $this->_sectorModel;
    }

    public function addSector($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getSectorModel()->insertGetId($data);
    }

    public function updateSector($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getSectorModel()->where('id', $id)->update($data);
    }

    public function deleteSector($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getSectorModel()->where('id', $id)->update($data);
    }

    public function getSectorById($id) {
        $query = $this->getSectorModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getSector($fields = "*", $params = array()) {
        $query = $this->getSectorModel()
                ->select("*")
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>