<?php

namespace App\Libraries;

use App\TourHistory;
use Illuminate\Support\Facades\Auth;

class TourHistoryLib {

    protected $_tourHistoryModel = null;

    public function getTourHistoryModel() {
        if (!($this->_tourHistoryModel instanceof \App\Tourhistory)) {
            $this->_tourHistoryModel = new \App\Tourhistory();
        }
        return $this->_tourHistoryModel;
    }

    public function addTourHistory($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourHistoryModel()->insertGetId($data);
    }

    public function getTourHistoryById($id) {
        $query = $this->getTourHistoryModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTourHistory($fields = "*", $params = array()) {
        $fields[]='users.first_name';
        $fields[]='users.last_name';
        $query = $this->getTourHistoryModel()
                ->select($fields)
                ->leftJoin('users', 'tour_history.created_by', '=', 'users.id')
                ->orderBy('tour_history.id', 'DESC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function deleteTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourHistoryModel()->where('tour_id', $id)->update($data);
    }

}

?>