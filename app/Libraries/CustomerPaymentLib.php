<?php

namespace App\Libraries;

use App\CustomerPaymentlead;
use Illuminate\Support\Facades\Auth;

class CustomerPaymentLib {

    protected $_customerpaymentleadModel = null;

    public function getcustomerpaymentleadModel() {
        if (!($this->_customerpaymentleadModel instanceof \App\CustomerPaymentlead)) {
            $this->_customerpaymentleadModel = new \App\CustomerPaymentlead();
        }
        return $this->_customerpaymentleadModel;
    }

    public function addCustomerPayment($data) {       
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getcustomerpaymentleadModel()->insertGetId($data);
    }

    public function updateCustomerPayment($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getcustomerpaymentleadModel()->where('id', $id)->update($data);
    }

    public function getCustomerPaymentById($id, $fields = "*") {
        $query = $this->getcustomerpaymentleadModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->where('id', $id);
        return $query->first();
    }

    public function chechQuotationCustomerPayment($quotation_id, $fields = "*") {
        $query = $this->getcustomerpaymentleadModel()
                ->select($fields)
                ->where('quotation_id', $quotation_id)
                ->where('deleted_at', null);
        return $query->first();
    }

    public function updateCustomerPaymentQuotation($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getcustomerpaymentleadModel()->where('quotation_id', $id)->update($data);
    }

    public function deleteCustomerPaymentQuotation($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getcustomerpaymentleadModel()->where('quotation_id', $id)->update($data);
    }

    // public function getCustomerPaymentlastRecord() {
    //     $query = $this->getcustomerpaymentleadModel()
    //             ->select('*')
    //             ->where('deleted_at', null)
    //             ->orderBy('created_at', 'DESC');
    //     return $query->first();
    // }

    // public function GetDownRecordCustomerPayment($date, $fields = "*") {
    //     $query = $this->getcustomerpaymentleadModel()
    //             ->select($fields)
    //             ->where('deleted_at', null)
    //             ->where('created_at', '>', $date)
    //             ->orderBy('created_at', 'ASC');
    //     return $query->get();
    // }

    // public function GetUpRecordCustomerPayment($date, $fields = "*") {
    //     $query = $this->getcustomerpaymentleadModel()
    //             ->select($fields)
    //             ->where('deleted_at', null)
    //             ->where('created_at', '<', $date)
    //             ->orderBy('created_at', 'DESC');
    //     return $query->first();
    // }

    public function getCustomerPaymentlastRecord($lead_id) {
        $query = $this->getcustomerpaymentleadModel()
                ->select('*')
                ->where('deleted_at', null)
                ->where('lead_id', $lead_id)
                ->orderBy('created_at', 'DESC');
        return $query->first();
    }

    public function GetDownRecordCustomerPayment($date, $lead_id,$fields = "*") {
        $query = $this->getcustomerpaymentleadModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->where('lead_id', $lead_id)
                ->where('created_at', '>', $date)
                ->orderBy('created_at', 'ASC');
        return $query->get();
    }

    public function GetUpRecordCustomerPayment($date, $lead_id,$fields = "*") {
        $query = $this->getcustomerpaymentleadModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->where('lead_id', $lead_id)
                ->where('created_at', '<', $date)
                ->orderBy('created_at', 'DESC');
        return $query->first();
    }

    public function getQuotationId($transaction_id, $fields = "*") {
        $query = $this->getcustomerpaymentleadModel()
                ->select($fields)
                ->where('transaction_id', $transaction_id)
                ->where('deleted_at', null);
        return $query->first();
    }

}

?>