<?php

namespace App\Libraries;

use App\TourInfo;
use Illuminate\Support\Facades\Auth;

class TourInfoLib {

    protected $_tourinfoModel = null;

    public function getTourInfoModel() {
        if (!($this->_tourinfoModel instanceof \App\TourInfo)) {
            $this->_tourinfoModel = new \App\TourInfo();
        }
        return $this->_tourinfoModel;
    }

    public function addTourInfo($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourInfoModel()->insertGetId($data);
    }

    public function updateTourInfo($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourInfoModel()->where('id', $id)->update($data);
    }

    public function deleteTourInfo($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourInfoModel()->where('id', $id)->update($data);
    }

    public function getTourInfoById($id) {
        $query = $this->getTourInfoModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTourInfo($fields = "*", $params = array()) {
        $query = $this->getTourInfoModel()
                ->select("*")
                ->where('deleted_at', null)
                ->orderBy('title', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>