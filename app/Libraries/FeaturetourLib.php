<?php

namespace App\Libraries;

use App\Featuretour;
use Illuminate\Support\Facades\Auth;

class FeaturetourLib {

    protected $_featuretourmasterModel = null;

    public function getFeaturetourmasterModel() {
        if (!($this->_featuretourmasterModel instanceof \App\Featuretour)) {
            $this->_featuretourmasterModel = new \App\Featuretour();
        }
        return $this->_featuretourmasterModel;
    }

    public function addFeaturetourmaster($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getFeaturetourmasterModel()->insertGetId($data);
    }

    public function updateFeaturetourmaster($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getFeaturetourmasterModel()->where('typeId', $id)->update($data);
    }

    public function deleteFeaturetourmaster($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getFeaturetourmasterModel()->where('typeId', $id)->update($data);
    }

    public function getFeaturetourmasterById($id) {
        $query = $this->getFeaturetourmasterModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getFeaturetourmaster($fields = "*", $all = false) {
        $query = $this->getFeaturetourmasterModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>