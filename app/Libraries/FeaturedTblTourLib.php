<?php

namespace App\Libraries;

use App\FeaturedTblTour;
use Illuminate\Support\Facades\Auth;

class FeaturedTblTourLib {

    protected $_featuredtbltour = null;

    public function getFeaturedTourModel() {
        if (!($this->_featuredtbltour instanceof \App\FeaturedTblTour)) {
            $this->_featuredtbltour = new \App\FeaturedTblTour();
        }
        return $this->_featuredtbltour;
    }

    public function addFeaturedTour($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getFeaturedTourModel()->insertGetId($data);
    }

    public function updateFeaturedTour($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getFeaturedTourModel()->where('id', $id)->update($data);
    }

    public function deleteFeaturedTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getFeaturedTourModel()->where('id', $id)->update($data);
    }

    public function getFeaturedTourById($id, $fields = "*") {
        $query = $this->getFeaturedTourModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getFeaturedTours($params = array()) {
        $query = $this->getFeaturedTourModel()
                ->select('featured_tbl_tours.id','featured_tbl_tours.tour_id','featured_tbl_tours.title','featured_tbl_tours.start_date','featured_tbl_tours.end_date','tour.tour_name','tour.tour_nights','tour.image')
                ->leftJoin('tour', 'tour.id', '=', 'featured_tbl_tours.tour_id');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        $query->orderBy('featured_tbl_tours.id', 'DESC');
        $query->limit(4);
        return $query->get();
    }

}

?>