<?php

namespace App\Libraries;

use App\Albums;
use App\Albumimages;
use Illuminate\Support\Facades\Auth;

class AlbumsLib {

    protected $_albumsModel = null;
    protected $_albumimagesModel = null;
    
    //Albums model load
    public function getAlbumsModel() {
        if (!($this->_albumsModel instanceof \App\Albums)) {
            $this->_albumsModel = new \App\Albums();
        }
        return $this->_albumsModel;
    }
    
    //Album Images model load
    public function getAlbumImagesModel() {
        if (!($this->_albumimagesModel instanceof \App\Albumimages)) {
            $this->_albumimagesModel = new \App\Albumimages();
        }
        return $this->_albumimagesModel;
    }
    
    //Add Albums
    public function addAlbums($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getAlbumsModel()->insertGetId($data);
    }
    
    //Update Albums
    public function updateAlbums($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAlbumsModel()->where('albId', $id)->update($data);
    }

    //Delete Albums
    public function deleteAlbums($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAlbumsModel()->where('albId', $id)->update($data);
    }
    
    //get Sinle Albums
    public function getAlbumsById($id) {
        $query = $this->getAlbumsModel()
                ->where('albId', $id);
        return $query->first();
    }
    
    //Get all Albums
    public function getAlbums($fields = "*", $params = array()) {
        $query = $this->getAlbumsModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>