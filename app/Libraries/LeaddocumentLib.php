<?php

namespace App\Libraries;

use App\Leaddocuments;
use Illuminate\Support\Facades\Auth;

class LeaddocumentLib {

    protected $_leaddocumentModel = null;

    public function getLeaddocumentModel() {
        if (!($this->_leaddocumentModel instanceof \App\Leaddocuments)) {
            $this->_leaddocumentModel = new \App\Leaddocuments();
        }
        return $this->_leaddocumentModel;
    }

    public function addLeadDocument($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getLeaddocumentModel()->insertGetId($data);
    }

    public function updateLeadDocument($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeaddocumentModel()->where('docId', $id)->update($data);
    }

    public function deleteLeadDocument($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeaddocumentModel()->where('docId', $id)->update($data);
    }

    public function getLeadDocumentById($id) {
        $query = $this->getLeaddocumentModel()
                ->where('docId', $id);
        return $query->first();
    }

    public function getLeadDocument($fields = "*", $params = array()) {
        $query = $this->getLeaddocumentModel()
                ->select($fields)
                ->orderBy('names', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>