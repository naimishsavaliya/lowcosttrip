<?php

namespace App\Libraries;

use App\Homebanner;
use Illuminate\Support\Facades\Auth;

class HomebannerLib {

    protected $_homebannerModel = null;

    public function getHomebannerModel() {
        if (!($this->_homebannerModel instanceof \App\Homebanner)) {
            $this->_homebannerModel = new \App\Homebanner();
        }
        return $this->_homebannerModel;
    }

    public function addHomebanner($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getHomebannerModel()->insertGetId($data);
    }

    public function updateHomebanner($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHomebannerModel()->where('banId', $id)->update($data);
    }

    public function deleteHomebanner($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHomebannerModel()->where('banId', $id)->update($data);
    }

    public function getHomebannerById($id) {
        $query = $this->getHomebannerModel()
                ->where('banId', $id);
        return $query->first();
    }

    // public function getHomebanner($fields = "*", $params = array()) {
    //     $query = $this->getHomebannerModel()
    //             ->select($fields)
    //             ->orderBy('name', 'ASC');
    //     foreach ($params as $key => $param) {
    //         $query->where($key, $param);
    //     }
    //     return $query->get();
    // }

    public function getHomebanner($fields = "*", $params = array()) {
        $query = $this->getHomebannerModel()
                ->select($fields)
                ->where('deleted_at', null);
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
            $query->orderBy('sort_ord', 'ASC');
        return $query->get();
    }

}

?>