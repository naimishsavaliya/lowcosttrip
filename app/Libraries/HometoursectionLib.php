<?php

namespace App\Libraries;

use App\Hometoursection;
use Illuminate\Support\Facades\Auth;

class HometoursectionLib {

    protected $_hometoursectionModel = null;

    public function getHometoursectionModel() {
        if (!($this->_hometoursectionModel instanceof \App\Hometoursection)) {
            $this->_hometoursectionModel = new \App\Hometoursection();
        }
        return $this->_hometoursectionModel;
    }

    public function updateHometoursection($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHometoursectionModel()->where('id', $id)->update($data);
    }

    public function getHomeTourSectionNameById($id) {
        $query = $this->getHometoursectionModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getActiveHometoursection() {
        $query = $this->getHometoursectionModel()
                ->where('status', 1);
        return $query->get();
    }

}

?>