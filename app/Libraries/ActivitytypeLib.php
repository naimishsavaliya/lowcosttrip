<?php

namespace App\Libraries;

use App\Activitytype;
use Illuminate\Support\Facades\Auth;

class ActivitytypeLib {

    protected $_activitymasterModel = null;

    public function getActivitytypemasterModel() {
        if (!($this->_activitymasterModel instanceof \App\Activitytype)) {
            $this->_activitymasterModel = new \App\Activitytype();
        }
        return $this->_activitymasterModel;
    }

    public function addActivitytypemaster($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getActivitytypemasterModel()->insertGetId($data);
    }

    public function updateActivitytypemaster($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getActivitytypemasterModel()->where('typeId', $id)->update($data);
    }

    public function deleteActivitytypemaster($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getActivitytypemasterModel()->where('typeId', $id)->update($data);
    }

    public function getActivitytypemasterById($id) {
        $query = $this->getActivitytypemasterModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getActivitytypemaster($fields = "*", $all = false) {
        $query = $this->getActivitytypemasterModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>