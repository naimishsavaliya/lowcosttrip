<?php

namespace App\Libraries;

use App\Certificates;
use Illuminate\Support\Facades\Auth;

class CertificatesLib {

    protected $_certificatesModel = null;

    public function getCertificatesModel() {
        if (!($this->_certificatesModel instanceof \App\Certificates)) {
            $this->_certificatesModel = new \App\Certificates();
        }
        return $this->_certificatesModel;
    }

    public function addCertificates($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getCertificatesModel()->insertGetId($data);
    }

    public function updateCertificates($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCertificatesModel()->where('cerId', $id)->update($data);
    }

    public function deleteCertificates($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCertificatesModel()->where('cerId', $id)->update($data);
    }

    public function getCertificatesById($id) {
        $query = $this->getCertificatesModel()
                ->where('cerId', $id);
        return $query->first();
    }

    public function getCertificates($fields = "*", $params = array()) {
        $query = $this->getCertificatesModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>