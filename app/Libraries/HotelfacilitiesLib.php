<?php

namespace App\Libraries;

use App\Hotelfaclitie;
use Illuminate\Support\Facades\Auth;

class HotelfacilitiesLib {

    protected $_hotelfacilitiesModel = null;

    public function getHotelfacilitiesModel() {
        if (!($this->_hotelfacilitiesModel instanceof \App\Hotelfaclitie)) {
            $this->_hotelfacilitiesModel = new \App\Hotelfaclitie();
        }
        return $this->_hotelfacilitiesModel;
    }

    public function addHotelfacilities($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getHotelfacilitiesModel()->insertGetId($data);
    }

    public function updateHotelfacilities($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotelfacilitiesModel()->where('facId', $id)->update($data);
    }

    public function deleteHotelfacilities($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotelfacilitiesModel()->where('facId', $id)->update($data);
    }

    public function getHotelfacilitiesById($id) {
        $query = $this->getHotelfacilitiesModel()
                ->where('facId', $id);
        return $query->first();
    }

    public function getHotelfacilities($fields = "*", $params = array(), $status = 1) {
        $query = $this->getHotelfacilitiesModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->where('status', $status)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>