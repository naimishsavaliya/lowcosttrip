<?php

namespace App\Libraries;

use App\Hottours;
use Illuminate\Support\Facades\Auth;

class HottoursLib {

    protected $_hottoursModel = null;

    public function getHottoursModel() {
        if (!($this->_hottoursModel instanceof \App\Hottours)) {
            $this->_hottoursModel = new \App\Hottours();
        }
        return $this->_hottoursModel;
    }

    public function addHottours($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getHottoursModel()->insertGetId($data);
    }

    public function updateHottours($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHottoursModel()->where('id', $id)->update($data);
    }

    public function deleteHottours($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHottoursModel()->where('id', $id)->update($data);
    }

    public function getHottoursById($id) {
        $query = $this->getHottoursModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getHottours($fields = "*", $params = array()) {
        $query = $this->getHottoursModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>
