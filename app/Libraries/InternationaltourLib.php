<?php

namespace App\Libraries;

use App\Internationaltour;
use Illuminate\Support\Facades\Auth;

class InternationaltourLib {

    protected $_internationaltourModel = null;

    public function getInternationalTourModel() {
        if (!($this->_internationaltourModel instanceof \App\Internationaltour)) {
            $this->_internationaltourModel = new \App\Internationaltour();
        }
        return $this->_internationaltourModel;
    }

    public function addInternationaltours($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getInternationalTourModel()->insertGetId($data);
    }

    public function updateInternationaltours($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getInternationalTourModel()->where('id', $id)->update($data);
    }

    public function deleteInternationaltours($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getInternationalTourModel()->where('id', $id)->update($data);
    }

    public function getInternationaltoursById($id) {
        $query = $this->getInternationalTourModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getInternationaltours($fields = "*", $params = array()) {
        $query = $this->getInternationalTourModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>
