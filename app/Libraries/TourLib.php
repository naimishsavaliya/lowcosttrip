<?php

namespace App\Libraries;

use App\Tour;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Pagination\Paginator;
// $results = new \Illuminate\Pagination\Paginator($parameters);

class TourLib {

    protected $_tourModel = null;

    public function getTourModel() {
        if (!($this->_tourModel instanceof \App\Tour)) {
            $this->_tourModel = new \App\Tour();
        }
        return $this->_tourModel;
    }

    public function addTour($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourModel()->insertGetId($data);
    }

    public function updateTour($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourModel()->where('id', $id)->update($data);
    }

    public function deleteTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourModel()->where('id', $id)->update($data);
    }

    public function getTourById($id, $fields = "*") {
        $query = $this->getTourModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getTourHeader($id) {
        $query = $this->getTourModel()
                ->select('tour.id', 'tour.tour_name', 'tour.created_by', 'users.first_name', 'users.last_name', \DB::raw('COUNT(tour_departure_date.id) AS totatl_departure'))
                ->leftJoin('users', 'tour.created_by', '=', 'users.id')
                ->leftJoin('tour_departure_date', 'tour.id', '=', 'tour_id')
                ->where('tour.id', $id);
//                ->groupBy('tour.id');
        return $query->first();
    }

    public function getTour($fields = "*", $all = false) {
        $query = $this->getTourModel()
                ->select($fields)
                ->orderBy('tour_name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('tour_status', 1);
        }
        if (\Auth::user()->role_id == 3 || \Auth::user()->role_id == 4) {
            $query->where('created_by', Auth::user()->id);
        }
        if (\Auth::user()->role_id == 2) {
            $query->whereIn('sector', explode(',', \Session::get('sector_id')));
        }
        return $query->get();
    }

    public function getTournameAutocomplate($fields = "*", $keyword) {
        $query = $this->getTourModel()
                ->select($fields)
                ->orderBy('tour_name', 'ASC');
        if (!empty($keyword)) {
            $query->where("tour_name", "like", '%' . $keyword . '%');
        }
        return $query->get();
    }

    public function getToursDetails($fields = "*", $where = array(), $sortBy = "", $page = 1, $limit = 10) {
        $query = $this->getTourModel()
                ->has('tourCost')
                ->select($fields);
                //->orderBy('tour_name', 'ASC');
        foreach ($where as $key => $param) {
            if($key=='tour.hotel'){
                $query->whereHas('tourHotel', function ($query) use ($param) {
                    $query->where('hotel_star', '=', $param);
                });
            }else if($key=='tour.budget'){
                $budget=explode("-",$param);
                if(isset($budget[0]) && isset($budget[1])){
                    $min = $budget[0];
                    $max = $budget[1];
                    $query->whereHas('tourCostAmount', function ($query) use ($min,$max) {
                        $query->where('sell','>=',$min);
                        $query->where('sell','<=',$max);
                        //$query->whereBetween('sell',[$min, $max]);
                    });
                }else{
                    $query->whereHas('tourCostAmount', function ($query) use ($param) {
                        $query->where('sell','>=',$param);
                    });
                }
            }else if ($key=='tour.short_by') {
                if($param=='1'){
                    $query->orderBy('created_at', 'DESC');
                }elseif ($param=='3') {
                    $query->orderByRaw('((SELECT sell from tour_cost_amount WHERE tour_cost_amount.tour_id=tour.id AND tour_cost_amount.sell!="" ORDER BY sell ASC LIMIT 1)- ( (SELECT tca.sell*tour_cost.discount from tour_cost_amount as tca LEFT JOIN tour_cost ON tca.cost_id=tour_cost.id WHERE tca.tour_id=tour.id AND tca.sell!="" ORDER BY sell ASC LIMIT 1) /100 )) ASC');
                    // $query->orderByRaw('(SELECT sell from tour_cost_amount WHERE tour_cost_amount.tour_id=tour.id AND tour_cost_amount.sell!="" ORDER BY sell ASC LIMIT 1) ASC');
                }elseif ($param=='4') {
                    $query->orderByRaw('((SELECT sell from tour_cost_amount WHERE tour_cost_amount.tour_id=tour.id AND tour_cost_amount.sell!="" ORDER BY sell ASC LIMIT 1)- ( (SELECT tca.sell*tour_cost.discount from tour_cost_amount as tca LEFT JOIN tour_cost ON tca.cost_id=tour_cost.id WHERE tca.tour_id=tour.id AND tca.sell!="" ORDER BY sell ASC LIMIT 1) /100 )) DESC');
                    // $query->orderByRaw('(SELECT sell from tour_cost_amount WHERE tour_cost_amount.tour_id=tour.id AND tour_cost_amount.sell!="" ORDER BY sell ASC LIMIT 1) DESC');
                }elseif ($param=='6') {
                    $query->orderBy('created_at', 'DESC');
                }
            }
            else{
                $query->where($key, $param);
            }
        }
        //$query->orderBy('id', 'DESC');
        //dd($query->toSql()); 
        return $query->paginate(10);
    }

    public function get_tour_data($id, $fields = "*") {
        $query = $this->getTourModel()
                ->has('tourCost')
                ->selectRaw('tour.*,(SELECT GROUP_CONCAT(master_feature_tour.name ) FROM master_feature_tour WHERE FIND_IN_SET(master_feature_tour.typeId, tour.feature) > 0) AS feature_data ')
                ->where('id', $id)
                ->where('tour_status', 1)
                ->where('public_private', 1)
                ->where('deleted_at', null);
        return $query->first();
    }

    public function searchTour($fields = '*',$whereArray = []){
        $query = $this->getTourModel()
                ->has('tourCost')
                //->select($fields)
                ->selectRaw('tour.*,(SELECT GROUP_CONCAT(master_feature_tour.name ) FROM master_feature_tour WHERE FIND_IN_SET(master_feature_tour.typeId, tour.feature) > 0) AS feature_data ')
                ->leftJoin('tour_departure_date', 'tour.id', '=', 'tour_departure_date.tour_id')
                ->leftJoin('tour_itinerary', 'tour.id', '=', 'tour_itinerary.tour_id')
                ->leftJoin('tour_hotel', 'tour.id', '=', 'tour_hotel.tour_id');
        if(count($whereArray)>0){
            foreach ($whereArray as $field => $value) {
                if($field=='raw'){
                    $query->whereRaw($value);
                }
                else{
                    $query->where($field,$value);
                }
            }
        }
        //$query->where('tour.deleted_at', null);
        $query->distinct('tour.id');
        // dd($query->toSql());
        // dd($query->get());
        //$candidates = $query->get();
        //$paginator = Paginator::make($candidates, count($candidates), 10);
        return $query->get();
    }

    public function getAllTourData( $fields = "*") {
        $query = $this->getTourModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('id', 'DESC');
        return $query->get();
    }

    public function getAll_DomesticTourData($fields = "*") {
        $query = $this->getTourModel()
                ->select($fields)
                ->where('domestic_international',1)
                ->where('deleted_at', null)
                ->orderBy('id', 'DESC');
        return $query->get();
    }

    public function getAll_InternationalTourData($fields = "*") {
        $query = $this->getTourModel()
                ->select($fields)
                ->where('domestic_international', 2)
                ->where('deleted_at', null)
                ->orderBy('id', 'DESC');
        return $query->get();
    }

    public function checkTour($id, $fields = "*") {
        $query = $this->getTourModel()
                ->select($fields)
                ->where('id', $id);
        return $query->count();
    }

    public function getinquiryToursDetails($fields = "*", $where = array()) {
        $query = $this->getTourModel()
                ->has('tourCost')
                ->select($fields);
        foreach ($where as $key => $param) {
                $query->where($key, $param);
        }
        return $query->first();
    }

    public function getToursDetailsForSpecialtyTours($fields = "*", $where = array(), $sortBy = "", $page = 1, $limit = 10) {
        $query = $this->getTourModel()
                ->has('tourCost')
                ->select($fields);
                $query->where('tour.tour_type', '!=' , null);
        foreach ($where as $key => $param) {
            if($key=='tour.hotel'){
                $query->whereHas('tourHotel', function ($query) use ($param) {
                    $query->where('hotel_star', '=', $param);
                });
            }else if($key=='tour.budget'){
                $budget=explode("-",$param);
                if(isset($budget[0]) && isset($budget[1])){
                    $min = $budget[0];
                    $max = $budget[1];
                    $query->whereHas('tourCostAmount', function ($query) use ($min,$max) {
                        $query->where('sell','>=',$min);
                        $query->where('sell','<=',$max);
                    });
                }else{
                    $query->whereHas('tourCostAmount', function ($query) use ($param) {
                        $query->where('sell','>=',$param);
                    });
                }
            }else if ($key=='tour.short_by') {
                if($param=='1'){
                    $query->orderBy('created_at', 'DESC');
                }elseif ($param=='3') {
                    $query->orderByRaw('((SELECT sell from tour_cost_amount WHERE tour_cost_amount.tour_id=tour.id AND tour_cost_amount.sell!="" ORDER BY sell ASC LIMIT 1)- ( (SELECT tca.sell*tour_cost.discount from tour_cost_amount as tca LEFT JOIN tour_cost ON tca.cost_id=tour_cost.id WHERE tca.tour_id=tour.id AND tca.sell!="" ORDER BY sell ASC LIMIT 1) /100 )) ASC');
                }elseif ($param=='4') {
                    $query->orderByRaw('((SELECT sell from tour_cost_amount WHERE tour_cost_amount.tour_id=tour.id AND tour_cost_amount.sell!="" ORDER BY sell ASC LIMIT 1)- ( (SELECT tca.sell*tour_cost.discount from tour_cost_amount as tca LEFT JOIN tour_cost ON tca.cost_id=tour_cost.id WHERE tca.tour_id=tour.id AND tca.sell!="" ORDER BY sell ASC LIMIT 1) /100 )) DESC');
                }elseif ($param=='6') {
                    $query->orderBy('created_at', 'DESC');
                }
            }
            else{
                $query->where($key, $param);
            }
        }
        //$query->orderBy('id', 'DESC');
        //dd($query->toSql()); 
        return $query->paginate(10);
    }

    //common use function 
    public function get_mega_menu_tour_data($fields = "*", $where = array(), $sortBy = "", $page = 1, $limit = 10){
        $query = $this->getTourModel()
                ->has('tourCost')
                ->selectRaw('tour.*,(SELECT GROUP_CONCAT(master_feature_tour.name ) FROM master_feature_tour WHERE FIND_IN_SET(master_feature_tour.typeId, tour.feature) > 0) AS feature_data ');
                if(!empty($where)){
                    foreach ($where as $key => $param) {
                        $query->where($key, $param);
                    }
                }
        return $query->paginate(10);
    }

    public function getSpecialtyToursData($fields = "*", $where = array(), $sortBy = "", $page = 1, $limit = 10) {
        $query = $this->getTourModel()
                ->has('tourCost')
                ->selectRaw('tour.*,(SELECT GROUP_CONCAT(master_feature_tour.name ) FROM master_feature_tour WHERE FIND_IN_SET(master_feature_tour.typeId, tour.feature) > 0) AS feature_data ');
                $query->where('tour.tour_type', '!=' , null);
                if(!empty($where)){
                    foreach ($where as $key => $param) {
                        $query->where($key, $param);
                    }
                }
        return $query->paginate(10);
    }

    public function categrowiseSpecialtyTours($typeId) {
        $query = $this->getTourModel()
                ->has('tourCost')
                ->selectRaw('tour.*,(SELECT GROUP_CONCAT(master_feature_tour.name ) FROM master_feature_tour WHERE FIND_IN_SET(master_feature_tour.typeId, tour.feature) > 0) AS feature_data ');
                $query->where('tour.tour_type', '!=' , null);
                $query->where('tour.tour_status', 1);
                $query->where('tour.public_private', 1);
                $query->where('tour.deleted_at', null);
                $query->whereRaw('FIND_IN_SET("'.$typeId.'",tour.tour_type)');
        return $query->paginate(10);
    }

    public function getCruiseHolidays($fields = "*", $where = array(),$typeId) {
        $query = $this->getTourModel()
                ->has('tourCost')
                ->select($fields);
                if(!empty($where)){
                    foreach ($where as $key => $param) {
                        $query->where($key, $param);
                    }
                }
                $query->where('tour_type', '!=' , null);
                $query->whereRaw('FIND_IN_SET("'.$typeId.'",tour_type)');
        return $query->get();
    }

    public function getCruiseHolidayslist($typeId) {
        $query = $this->getTourModel()
                ->has('tourCost')
                ->selectRaw('tour.*,(SELECT GROUP_CONCAT(master_feature_tour.name ) FROM master_feature_tour WHERE FIND_IN_SET(master_feature_tour.typeId, tour.feature) > 0) AS feature_data ');
                $query->where('tour.tour_type', '!=' , null);
                $query->where('tour.tour_status', 1);
                $query->where('tour.public_private', 1);
                $query->where('tour.deleted_at', null);
                $query->whereRaw('FIND_IN_SET("'.$typeId.'",tour.tour_type)');
        return $query->paginate(10);
    }

}

?>