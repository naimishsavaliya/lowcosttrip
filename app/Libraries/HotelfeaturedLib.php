<?php

namespace App\Libraries;

use App\Hotelfeatured;
use Illuminate\Support\Facades\Auth;

class HotelfeaturedLib {

    protected $_hotelfeaturedModel = null;

    public function getHotelfeaturedModel() {
        if (!($this->_hotelfeaturedModel instanceof \App\Hotelfeatured)) {
            $this->_hotelfeaturedModel = new \App\Hotelfeatured();
        }
        return $this->_hotelfeaturedModel;
    }

    public function addHotelfeatured($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getHotelfeaturedModel()->insertGetId($data);
    }

    public function updateHotelfeatured($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotelfeaturedModel()->where('id', $id)->update($data);
    }

    public function deleteHotelfeatured($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotelfeaturedModel()->where('id', $id)->update($data);
    }

    public function getHotelfeaturedById($id) {
        $query = $this->getHotelfeaturedModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getHotelfeatured($fields = "*", $params = array()) {
        $query = $this->getHotelfeaturedModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>