<?php

namespace App\Libraries;

use App\LeadPackagesHotels;
use Illuminate\Support\Facades\Auth;

class LeadPackagesHotelsLib {

    protected $_leadpackageshotelsModel = null;

    public function getleadpackageshotelsModel() {
        if (!($this->_leadpackageshotelsModel instanceof \App\LeadPackagesHotels)) {
            $this->_leadpackageshotelsModel = new \App\LeadPackagesHotels();
        }
        return $this->_leadpackageshotelsModel;
    }

    public function add($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getleadpackageshotelsModel()->insertGetId($data);
    }

    public function update($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        return $this->getleadpackageshotelsModel()->where('id', $id)->update($data);
    }

    public function delete($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        return $this->getleadpackageshotelsModel()->where('id', $id)->update($data);
    }

    public function delete_by_package_id($id) {
        return $this->getleadpackageshotelsModel()->where('lead_package_id', $id)->delete();
    }

    public function getLeadPackagesHotelsById($id) {
        $query = $this->getleadpackageshotelsModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getLeadPackagesHotels($fields = "*", $params = array()) {
        $query = $this->getleadpackageshotelsModel()
                ->select($fields)
                ->where('deleted_at', null);
                // ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>