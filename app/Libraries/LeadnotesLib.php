<?php

namespace App\Libraries;

use App\Leadnotes;
use Illuminate\Support\Facades\Auth;

class LeadnotesLib {

    protected $_leadnotesModel = null;

    public function getLeadnotesModel() {
        if (!($this->_leadnotesModel instanceof \App\Leadnotes)) {
            $this->_leadnotesModel = new \App\Leadnotes();
        }
        return $this->_leadnotesModel;
    }

    public function addLeadnote($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getLeadnotesModel()->insertGetId($data);
    }

    public function updateLeadnote($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadnotesModel()->where('notId', $id)->update($data);
    }

    public function deleteLeadnote($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadnotesModel()->where('notId', $id)->update($data);
    }

    public function getLeadnoteById($id) {
        $query = $this->getLeadnotesModel()
                ->where('notId', $id);
        return $query->first();
    }

    public function getLeadnote($fields = "*", $params = array()) {
        $query = $this->getLeadnotesModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('title', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function get_followup_data_for_cron($datetime) {
        $query = $this->getLeadnotesModel()
                ->select('*')
                ->where('deleted_at', null)
                ->where('status', 1)
                //->whereRaw("DATE_FORMAT(followup_date,'%Y-%m-%d %H:%i') = '2019-01-01 12:09'")
                ->whereRaw("DATE_FORMAT(followup_date,'%Y-%m-%d %H:%i') = '".$datetime."'")
                ->orderBy('notId', 'ASC');
        return $query->get();
    }

}

?>