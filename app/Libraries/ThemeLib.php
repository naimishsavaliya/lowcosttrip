<?php

namespace App\Libraries;

use App\Theme;
use Illuminate\Support\Facades\Auth;

class ThemeLib {

	protected $_themeModel = null;

	public function getThemeModel() {
		if (!($this->_themeModel instanceof \App\Theme)) {
			$this->_themeModel = new \App\Theme();
		}
		return $this->_themeModel;
	}

	public function addTheme($data) {
		$data['created_at'] = date("Y-m-d H:i:s");
		$data['created_by'] = Auth::id();
		return $query = $this->getThemeModel()->insertGetId($data);
	}

	public function updateTheme($data, $id) {
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getThemeModel()->where('theme_id', $id)->update($data);
	}

	public function deleteTheme($id) {
		$data = array();
		$data['deleted_at'] = date("Y-m-d H:i:s");
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getThemeModel()->where('theme_id', $id)->update($data);
	}

	public function getThemeById($id) {
		$query = $this->getThemeModel()
				->where('theme_id', $id);
		return $query->first();
	}

	public function getTheme($fields = "*", $all = false) {
		$query = $this->getThemeModel()
				->select($fields)
				->orderBy('theme_name', 'ASC');
		if (!$all) {
			$query->where('deleted_at', null)->where('status',1);
		}
		return $query->get();
	}

}

?>