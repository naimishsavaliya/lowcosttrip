<?php

namespace App\Libraries;

use App\Holidaydestinationdetails;
use Illuminate\Support\Facades\Auth;

class HolidaydestinationdetailsLib {

    protected $_holidaydestinationdetailsModel = null;
        
    public function getHolidaydestinationdetailsModel() {
        if (!($this->_holidaydestinationdetailsModel instanceof \App\Holidaydestinationdetails)) {
            $this->_holidaydestinationdetailsModel = new \App\Holidaydestinationdetails();
        }
        return $this->_holidaydestinationdetailsModel;
    }
    
    public function addHolidaydestinationdetails($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getHolidaydestinationdetailsModel()->insertGetId($data);
    }
    
    public function updateHolidaydestinationdetails($data, $id) {
        return $this->getHolidaydestinationdetailsModel()->where('id', $id)->update($data);
    }

    public function deleteHolidaydestinationdetails($id) {
        $data = array();
        return $this->getHolidaydestinationdetailsModel()->where('id', $id)->delete();
    }
    
    public function getHolidaydestinationdetailsById($id) {
        $query = $this->getHolidaydestinationdetailsModel()
                ->where('id', $id);
        return $query->first();
    }
    
    public function getHolidaydestinationdetails($fields = "*", $params = array()) {
        $query = $this->getHolidaydestinationdetailsModel()
                ->select($fields)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function deleteHolidaydestinationdetails_lead($where) {
        return $this->getHolidaydestinationdetailsModel()->where($where)->delete();
    }

}

?>