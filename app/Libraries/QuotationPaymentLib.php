<?php

namespace App\Libraries;

use App\QuotationPayment;
use App\Payment_response;
use Illuminate\Support\Facades\Auth;

class QuotationPaymentLib {

    protected $_quotationpaymentModel = null;

    public function  GetquotationPaymentModel() {
        if (!($this->_quotationpaymentModel instanceof \App\QuotationPayment)) {
            $this->_quotationpaymentModel = new \App\QuotationPayment();
        }
        return $this->_quotationpaymentModel;
    }
    
    public function addPayment($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->GetquotationPaymentModel()->insertGetId($data);
    }


    public function updatePayment($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->GetquotationPaymentModel()->where('id', $id)->update($data);
    }

    public function deletePayment($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->GetquotationPaymentModel()->where('id', $id)->update($data);
    }

    public function getPaymentById($id) {
        $query = $this->GetquotationPaymentModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getPayment($fields = "*", $params = array()) {
        $query = $this->GetquotationPaymentModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('customer_name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>