<?php

namespace App\Libraries;

use App\Albumcategory;
use Illuminate\Support\Facades\Auth;

class AlbumcategoryLib {

    protected $_albumcategoryModel = null;

    public function getAlbumcategoryModel() {
        if (!($this->_albumcategoryModel instanceof \App\Albumcategory)) {
            $this->_albumcategoryModel = new \App\Albumcategory();
        }
        return $this->_albumcategoryModel;
    }

    public function addAlbumcategory($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getAlbumcategoryModel()->insertGetId($data);
    }

    public function updateAlbumcategory($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAlbumcategoryModel()->where('albId', $id)->update($data);
    }

    public function deleteAlbumcategory($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAlbumcategoryModel()->where('albId', $id)->update($data);
    }

    public function getAlbumcategoryById($id) {
        $query = $this->getAlbumcategoryModel()
                ->where('albId', $id);
        return $query->first();
    }

    public function getAlbumcategory($fields = "*", $params = array()) {
        $query = $this->getAlbumcategoryModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('title', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>