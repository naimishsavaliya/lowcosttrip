<?php

namespace App\Libraries;

use App\TourHotel;
use Illuminate\Support\Facades\Auth;

class TourHotelLib {

    protected $_tourHotelModel = null;

    public function getTourHotelModel() {
        if (!($this->_tourHotelModel instanceof \App\TourHotel)) {
            $this->_tourHotelModel = new \App\TourHotel();
        }
        return $this->_tourHotelModel;
    }

    public function addTourHotel($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourHotelModel()->insertGetId($data);
    }

    public function updateTourHotel($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourHotelModel()->where('id', $id)->update($data);
    }

    public function deleteTourHotel($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourHotelModel()->where('id', $id)->update($data);
    }

    public function deleteTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourHotelModel()->where('tour_id', $id)->update($data);
    }

    public function getTourHotelById($id) {
        $query = $this->getTourHotelModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTourHotel($fields = "*", $params = array()) {
        $query = $this->getTourHotelModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function getHotelByTour($fields = "*", $params = array()) {
        $query = $this->getTourHotelModel()
                ->select($fields)
                ->leftJoin('master_tour_package_type','tour_hotel.package_type','=','master_tour_package_type.typeId')
                ->where('tour_hotel.deleted_at', null)
                ->orderBy('tour_hotel.package_type', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>