<?php

namespace App\Libraries;

use App\TourDeparture;
use Illuminate\Support\Facades\Auth;

class TourDepartureLib {

    protected $_tourDepartureModel = null;

    public function getTourDepartureModel() {
        if (!($this->_tourDepartureModel instanceof \App\TourDeparture)) {
            $this->_tourDepartureModel = new \App\TourDeparture();
        }
        return $this->_tourDepartureModel;
    }

    public function addTourDeparture($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourDepartureModel()->insertGetId($data);
    }

    public function updateTourDeparture($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourDepartureModel()->where('id', $id)->update($data);
    }

    public function deleteTourDeparture($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourDepartureModel()->where('id', $id)->update($data);
    }

    public function deleteTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourDepartureModel()->where('tour_id', $id)->update($data);
    }

    public function getTourDepartureById($id) {
        $query = $this->getTourDepartureModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTourDeparture($fields = "*", $params = array(),$currentYear = false) {
        $query = $this->getTourDepartureModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        if($currentYear){
            $query->whereYear('departure_date', $currentYear);
        }
        return $query->get();
    }

}

?>