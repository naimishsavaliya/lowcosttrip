<?php

namespace App\Libraries;

use App\FlightReviewInfo;
use Illuminate\Support\Facades\Auth;

class FlightReviewInfoLib {

    protected $_flightreviewinfo = null;

    public function getFlightReviewInfo() {
        if (!($this->_flightreviewinfo instanceof \App\FlightReviewInfo)) {
            $this->_flightreviewinfo = new \App\FlightReviewInfo();
        }
        return $this->_flightreviewinfo;
    }

    public function addFlightReviewInfo($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getFlightReviewInfo()->insertGetId($data);
    }

    public function getFlightReviewByFlightId($id) {
        $query = $this->getFlightReviewInfo()
                ->select('flight_review_info.*', 'c1.city_name as drrival_airportcity', 'c2.city_name as aeparture_airportcity')
                ->leftjoin('city as c1', 'flight_review_info.drrival_airport', '=', 'c1.city_code')
                ->leftjoin('city as c2', 'flight_review_info.aeparture_airport', '=', 'c2.city_code')
                ->where('flight_review_info.flight_books_id', $id);
        return $query->get();
    }
    
}

?>