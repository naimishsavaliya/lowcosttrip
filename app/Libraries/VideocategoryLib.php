<?php

namespace App\Libraries;

use App\Videocategory;
use Illuminate\Support\Facades\Auth;

class VideocategoryLib {

    protected $_videocategoryModel = null;

    public function getVideocategoryModel() {
        if (!($this->_videocategoryModel instanceof \App\Videocategory)) {
            $this->_videocategoryModel = new \App\Videocategory();
        }
        return $this->_videocategoryModel;
    }

    public function addVideocategory($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getVideocategoryModel()->insertGetId($data);
    }

    public function updateVideocategory($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getVideocategoryModel()->where('catId', $id)->update($data);
    }

    public function deleteVideocategory($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getVideocategoryModel()->where('catId', $id)->update($data);
    }

    public function getVideocategoryById($id) {
        $query = $this->getVideocategoryModel()
                ->where('catId', $id);
        return $query->first();
    }

    public function getVideocategory($fields = "*", $params = array()) {
        $query = $this->getVideocategoryModel()
                ->select($fields)
                ->where('deleted_at',null)
                ->orderBy('title', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>