<?php

namespace App\Libraries;

use App\Agentdocument;
use Illuminate\Support\Facades\Auth;

class AgentdocumentLib {

    protected $_agentdocumentModel = null;

    public function getAgentdocumentModel() {
        if (!($this->_agentdocumentModel instanceof \App\Agentdocument)) {
            $this->_agentdocumentModel = new \App\Agentdocument();
        }
        return $this->_agentdocumentModel;
    }

    public function addAgentdocument($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getAgentdocumentModel()->insertGetId($data);
    }

    public function updateAgentdocument($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAgentdocumentModel()->where('docId', $id)->update($data);
    }

    public function deleteAgentdocument($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getAgentdocumentModel()->where('docId', $id)->update($data);
    }

    public function getAgentdocumentById($id) {
        $query = $this->getAgentdocumentModel()
                ->where('docId', $id);
        return $query->first();
    }

    public function getAgentdocument($id, $fields = "*", $params = array()) {
        $query = $this->getAgentdocumentModel()
                ->select($fields)
                ->where('agent_id', $id)
                ->orderBy('docId', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>