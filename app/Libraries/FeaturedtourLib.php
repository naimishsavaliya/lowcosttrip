<?php

namespace App\Libraries;

use App\Featuredtour;
use Illuminate\Support\Facades\Auth;

class FeaturedtourLib {

    protected $_featuredtourModel = null;

    public function getFeaturedtourModel() {
        if (!($this->_featuredtourModel instanceof \App\Featuredtour)) {
            $this->_featuredtourModel = new \App\Featuredtour();
        }
        return $this->_featuredtourModel;
    }

    public function addFeaturedtour($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getFeaturedtourModel()->insertGetId($data);
    }

    public function updateFeaturedtour($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getFeaturedtourModel()->where('id', $id)->update($data);
    }

    public function deleteFeaturedtour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getFeaturedtourModel()->where('id', $id)->update($data);
    }

    public function getFeaturedtourById($id) {
        $query = $this->getFeaturedtourModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getFeaturedtour($fields = "*", $params = array()) {
        $query = $this->getFeaturedtourModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>
