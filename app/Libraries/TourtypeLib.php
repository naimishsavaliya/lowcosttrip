<?php

namespace App\Libraries;

use App\Tourtype;
use Illuminate\Support\Facades\Auth;

class TourtypeLib {

    protected $_tourtypeModel = null;

    public function getTourtypeModel() {
        if (!($this->_tourtypeModel instanceof \App\Tourtype)) {
            $this->_tourtypeModel = new \App\Tourtype();
        }
        return $this->_tourtypeModel;
    }

    public function addTourtype($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourtypeModel()->insertGetId($data);
    }

    public function updateTourtype($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourtypeModel()->where('typeId', $id)->update($data);
    }

    public function deleteTourtype($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourtypeModel()->where('typeId', $id)->update($data);
    }

    public function getTourtypeById($id) {
        $query = $this->getTourtypeModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getTourtype($fields = "*", $params = array()) {
        $query = $this->getTourtypeModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        $query->where('deleted_at', null);
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>