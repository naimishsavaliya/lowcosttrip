<?php

namespace App\Libraries;

use App\Level;
use Illuminate\Support\Facades\Auth;

class LevelLib {

	protected $_levelModel = null;

	public function getLevelModel() {
		if (!($this->_levelModel instanceof \App\Level)) {
			$this->_levelModel = new \App\Level();
		}
		return $this->_levelModel;
	}

	public function addLevel($data) {
		$data['created_at'] = date("Y-m-d H:i:s");
		$data['created_by'] = Auth::id();
		return $query = $this->getLevelModel()->insertGetId($data);
	}

	public function updateLevel($data, $id) {
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getLevelModel()->where('level_id', $id)->update($data);
	}

	public function deleteLevel($id) {
		$data = array();
		$data['deleted_at'] = date("Y-m-d H:i:s");
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getLevelModel()->where('level_id', $id)->update($data);
	}

	public function getLevelById($id) {
		$query = $this->getLevelModel()
				->where('level_id', $id);
		return $query->first();
	}

	public function getLevel($fields = "*", $params = array(), $all = false) {
		$query = $this->getLevelModel()
				->select($fields)
				->orderBy('level_name', 'ASC');
		foreach ($params as $key => $param) {
			$query->where($key, $param);
		}
		if (!$all) {
			$query->where('deleted_at', null)->where('status', 1);
		}
		return $query->get();
	}

}

?>