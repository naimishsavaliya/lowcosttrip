<?php

namespace App\Libraries;

use App\Role;
use Illuminate\Support\Facades\Auth;

class RoleLib {

    protected $_roleModel = null;

    public function getRoleModel() {
        if (!($this->_roleModel instanceof \App\Role)) {
            $this->_roleModel = new \App\Role();
        }
        return $this->_roleModel;
    }

    public function addRole($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
//        $data['created_by'] = Auth::id();
        return $query = $this->getRoleModel()->insertGetId($data);
    }

    public function updateRole($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
//        $data['updated_by'] = Auth::id();
        return $this->getRoleModel()->where('id', $id)->update($data);
    }

    public function deleteRole($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
//        $data['updated_by'] = Auth::id();
        return $this->getRoleModel()->where('id', $id)->update($data);
    }

    public function getRoleById($id) {
        $query = $this->getRoleModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getRole($fields = "*", $params = array()) {
        $query = $this->getRoleModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>