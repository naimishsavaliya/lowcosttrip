<?php

namespace App\Libraries;

use App\InternationalHoliday;
use Illuminate\Support\Facades\Auth;

class InternationalHolidayLib {

    protected $_internationalHoliday = null;

    public function getInternationalHolidayModel() {
        if (!($this->_internationalHoliday instanceof \App\InternationalHoliday)) {
            $this->_internationalHoliday = new \App\InternationalHoliday();
        }
        return $this->_internationalHoliday;
    }

    public function addInternationalHoliday($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getInternationalHolidayModel()->insertGetId($data);
    }

    public function updateInternationalHoliday($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getInternationalHolidayModel()->where('id', $id)->update($data);
    }

    public function deleteInternationalHoliday($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getInternationalHolidayModel()->where('id', $id)->update($data);
    }

    public function getInternationalHolidayById($id, $fields = "*") {
        $query = $this->getInternationalHolidayModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getHotInternationalholidays($params = array()) {
        $query = $this->getInternationalHolidayModel()
                ->select('international_holidays.id','international_holidays.tour_id','international_holidays.title','international_holidays.start_date','international_holidays.end_date','tour.tour_name','tour.tour_nights','tour.image')
                ->leftJoin('tour', 'tour.id', '=', 'international_holidays.tour_id');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        $query->orderBy('international_holidays.id', 'DESC');
        $query->limit(4);
        return $query->get();
    }

}

?>