<?php

namespace App\Libraries;

use App\TourCostAmount;
use Illuminate\Support\Facades\Auth;

class TourCostAmountLib {

    protected $_tourCostAmountModel = null;

    public function getTourCostAmountModel() {
        if (!($this->_tourCostAmountModel instanceof \App\TourCostAmount)) {
            $this->_tourCostAmountModel = new \App\TourCostAmount();
        }
        return $this->_tourCostAmountModel;
    }

    public function addTourCostAmount($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourCostAmountModel()->insertGetId($data);
    }

    public function updateTourCostAmount($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourCostAmountModel()->where('id', $id)->update($data);
    }

    public function deleteTourCostAmount($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourCostAmountModel()->where('id', $id)->update($data);
    }

    public function deleteTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourCostAmountModel()->where('tour_id', $id)->update($data);
    }

    
    public function deleteTourCostAmountByCost($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourCostAmountModel()->where('cost_id', $id)->update($data);
    }

    public function getTourCostAmountById($id) {
        $query = $this->getTourCostAmountModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTourCostAmount($fields = "*", $params = array()) {
        $query = $this->getTourCostAmountModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function get_totalCost($where) {
        $query = $this->getTourCostAmountModel()
//                ->selectRaw('(tour_cost_amount.sell-((tour_cost_amount.sell*tour_cost.discount)/100)) as total_cost')
                ->selectRaw("CASE  
                                WHEN tour_cost.discount >=0 THEN (tour_cost_amount.sell-((tour_cost_amount.sell*tour_cost.discount)/100))
                                ELSE tour_cost_amount.sell
                               END as total_cost")
                ->leftJoin('tour_cost', 'tour_cost_amount.cost_id', '=', 'tour_cost.id')
                ->where($where)
                ->where('tour_cost_amount.deleted_at', null);
        return $query->first();
    }

}

?>