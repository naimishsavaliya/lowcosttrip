<?php

namespace App\Libraries;

use App\Discountcode;
use Illuminate\Support\Facades\Auth;

class DiscountcodeLib {

    protected $_discountModel = null;

    public function getDiscountcodeModel() {
        if (!($this->_discountModel instanceof \App\Discountcode)) {
            $this->_discountModel = new \App\Discountcode();
        }
        return $this->_discountModel;
    }

    public function addDiscountcode($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getDiscountcodeModel()->insertGetId($data);
    }

    public function updateDiscountcode($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getDiscountcodeModel()->where('desId', $id)->update($data);
    }

    public function deleteDiscountcode($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getDiscountcodeModel()->where('desId', $id)->update($data);
    }

    public function getDiscountcodeById($id) {
        $query = $this->getDiscountcodeModel()
                ->where('desId', $id);
        return $query->first();
    }

    public function getDiscountcode($fields = "*", $params = array()) {
        $query = $this->getDiscountcodeModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>