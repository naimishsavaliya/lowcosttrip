<?php

namespace App\Libraries;

use App\TourFAQ;
use Illuminate\Support\Facades\Auth;

class TourFAQLib {

    protected $_tourFAQModel = null;

    public function getTourFAQModel() {
        if (!($this->_tourFAQModel instanceof \App\TourFAQ)) {
            $this->_tourFAQModel = new \App\TourFAQ();
        }
        return $this->_tourFAQModel;
    }

    public function addTourFAQ($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourFAQModel()->insertGetId($data);
    }

    public function updateTourFAQ($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourFAQModel()->where('id', $id)->update($data);
    }

    public function deleteTourFAQ($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourFAQModel()->where('id', $id)->update($data);
    }

    public function deleteTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourFAQModel()->where('tour_id', $id)->update($data);
    }

    public function getTourFAQById($id) {
        $query = $this->getTourFAQModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTourFAQ($fields = "*", $params = array()) {
        $query = $this->getTourFAQModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>