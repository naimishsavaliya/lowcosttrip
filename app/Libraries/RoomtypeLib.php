<?php

namespace App\Libraries;

use App\Roomtype;
use Illuminate\Support\Facades\Auth;

class RoomtypeLib {

    protected $_roomtypeModel = null;

    public function getRoomtypeModel() {
        if (!($this->_roomtypeModel instanceof \App\Roomtype)) {
            $this->_roomtypeModel = new \App\Roomtype();
        }
        return $this->_roomtypeModel;
    }

    public function addRoomtype($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getRoomtypeModel()->insertGetId($data);
    }

    public function updateRoomtype($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getRoomtypeModel()->where('typeId', $id)->update($data);
    }

    public function deleteRoomtype($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getRoomtypeModel()->where('typeId', $id)->update($data);
    }

    public function getRoomtypeById($id) {
        $query = $this->getRoomtypeModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getRoomtype($fields = "*", $all = false) {
        $query = $this->getRoomtypeModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>