<?php

namespace App\Libraries;

use App\Suppliertype;
use Illuminate\Support\Facades\Auth;

class SuppliertypeLib {

    protected $_suppliertypeModel = null;

    public function getSuppliertypeModel() {
        if (!($this->_suppliertypeModel instanceof \App\Suppliertype)) {
            $this->_suppliertypeModel = new \App\Suppliertype();
        }
        return $this->_suppliertypeModel;
    }

    public function addSuppliertype($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getSuppliertypeModel()->insertGetId($data);
    }

    public function updateSuppliertype($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getSuppliertypeModel()->where('supTypeId', $id)->update($data);
    }

    public function deleteSuppliertype($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getSuppliertypeModel()->where('supTypeId', $id)->update($data);
    }

    public function getSuppliertypeById($id) {
        $query = $this->getSuppliertypeModel()
                ->where('supTypeId', $id);
        return $query->first();
    }

    public function getSuppliertype($fields = "*", $params = array()) {
        $query = $this->getSuppliertypeModel()
                ->select($fields)
                ->orderBy('type', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>