<?php

namespace App\Libraries;

use App\Webpopup;
use Illuminate\Support\Facades\Auth;

class WebpopupLib {

    protected $_webpopupModel = null;

    public function getWebpopupModel() {
        if (!($this->_webpopupModel instanceof \App\Webpopup)) {
            $this->_webpopupModel = new \App\Webpopup();
        }
        return $this->_webpopupModel;
    }

    public function addWebpopup($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getWebpopupModel()->insertGetId($data);
    }

    public function updateWebpopup($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getWebpopupModel()->where('id', $id)->update($data);
    }

    public function deleteWebpopup($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getWebpopupModel()->where('id', $id)->update($data);
    }

    public function getWebpopupById($id) {
        $query = $this->getWebpopupModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getWebpopup($fields = "*", $params = array()) {
        $query = $this->getWebpopupModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>