<?php

namespace App\Libraries;

use App\Customertype;
use Illuminate\Support\Facades\Auth;

class CustomertypeLib {

    protected $_customertypemasterModel = null;

    public function getCustomertypemasterModel() {
        if (!($this->_customertypemasterModel instanceof \App\Customertype)) {
            $this->_customertypemasterModel = new \App\Customertype();
        }
        return $this->_customertypemasterModel;
    }

    public function addCustomertypemaster($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getCustomertypemasterModel()->insertGetId($data);
    }

    public function updateCustomertypemaster($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCustomertypemasterModel()->where('typeId', $id)->update($data);
    }

    public function deleteCustomertypemaster($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCustomertypemasterModel()->where('typeId', $id)->update($data);
    }

    public function getCustomertypemasterById($id) {
        $query = $this->getCustomertypemasterModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getCustomertypemaster($fields = "*", $all = false) {
        $query = $this->getCustomertypemasterModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>