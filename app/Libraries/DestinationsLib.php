<?php

namespace App\Libraries;

use App\Destinations;
use Illuminate\Support\Facades\Auth;

class DestinationsLib {

    protected $_destinationsModel = null;

    public function getDestinationModel() {
        if (!($this->_destinationsModel instanceof \App\Destinations)) {
            $this->_destinationsModel = new \App\Destinations();
        }
        return $this->_destinationsModel;
    }

    public function addDestination($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getDestinationModel()->insertGetId($data);
    }

    public function updateDestination($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getDestinationModel()->where('desId', $id)->update($data);
    }

    public function deleteDestination($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getDestinationModel()->where('desId', $id)->update($data);
    }

    public function getDestinationById($id) {
        $query = $this->getDestinationModel()
                ->where('desId', $id);
        return $query->first();
    }

    public function getDestination($fields = "*", $params = array()) {
        $query = $this->getDestinationModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>