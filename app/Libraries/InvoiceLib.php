<?php
namespace App\Libraries;
use App\Invoice;
use Illuminate\Support\Facades\Auth;
class InvoiceLib {

    protected $_invoiceModel = null;

    public function getInvoiceModel() {
        if (!($this->_invoiceModel instanceof \App\Invoice)) {
            $this->_invoiceModel = new \App\Invoice();
        }
        return $this->_invoiceModel;
    }

    public function getInvoice($type, $lead_id, $quote_id = 0) {
        if($quote_id > 0 ){
            $query = $this->getInvoiceModel()->where('type', $type)->where('lead_id', $lead_id)->where('quatation_id', $quote_id);
        }else{
            $query = $this->getInvoiceModel()->where('type', $type)->where('lead_id', $lead_id);
        }
        return $query->first();
    }
    public function insertInvoice($data) {
        return $this->getInvoiceModel()->insertGetId($data);
    }

    public function updateReceiptNumber() {
        $recept = $this->getReceiptNumber();
        $data['current_receipt_number'] = ($recept + 1);
        $data['updated_at'] = date("Y-m-d H:i:s");
        return $this->getProformaModel()->where('type', 'payment')->where('current_receipt_number', $recept)->update($data);
    }

    public function getProformaReceiptNumber() {
        $query = $this->getProformaModel()->where('type', 'proforma');
        $receipt_data = $query->first();
        if (isset($receipt_data) && $receipt_data != null) {
            $receipt = $receipt_data->current_receipt_number;
        } else {
            $receipt = $data['start_receipt_number'] = "10001";
            $data['current_receipt_number'] = "10001";
            $data['type'] = "payment";
            $data['created_at'] = date("Y-m-d H:i:s");
            $this->getReceiptModel()->insertGetId($data);
        }
        return $receipt;
    }

    public function updateProformaReceiptNumber() {
        $recept = $this->getReceiptNumber();
        $data['current_receipt_number'] = ($recept + 1);
        $data['updated_at'] = date("Y-m-d H:i:s");
        return $this->getReceiptModel()->where('type', 'proforma')->where('current_receipt_number', $recept)->update($data);
    }

}

?>