<?php

namespace App\Libraries;

use App\City;
use Illuminate\Support\Facades\Auth;

class CityLib {

    protected $_cityModel = null;

    public function getCityModel() {
        if (!($this->_cityModel instanceof \App\City)) {
            $this->_cityModel = new \App\City();
        }
        return $this->_cityModel;
    }

    public function addCity($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getCityModel()->insertGetId($data);
    }

    public function updateCity($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCityModel()->where('city_id', $id)->update($data);
    }

    public function deleteCity($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCityModel()->where('city_id', $id)->update($data);
    }

    public function getCityById($id) {
        $query = $this->getCityModel()
                ->where('city_id', $id);
        return $query->first();
    }

    public function getCity($fields = "*", $params = array()) {
        $query = $this->getCityModel()
                ->select($fields)
                ->orderBy('city_name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
            //$query->limit(50);
        return $query->get();
    }

    public function getCityByState($state_id, $fields = "*", $params = array()) {
        $query = $this->getCityModel()
                ->select($fields)
                ->orderBy('city_name', 'ASC')
                ->where('state_id', $state_id);
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>