<?php

namespace App\Libraries;

use App\TourCost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class TourCostLib {

    protected $_tourCostModel = null;

    public function getTourCostModel() {
        if (!($this->_tourCostModel instanceof \App\TourCost)) {
            $this->_tourCostModel = new \App\TourCost();
        }
        return $this->_tourCostModel;
    }

    public function addTourCost($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTourCostModel()->insertGetId($data);
    }

    public function updateTourCost($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourCostModel()->where('id', $id)->update($data);
    }

    public function deleteTourCost($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourCostModel()->where('id', $id)->update($data);
    }

    public function deleteTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTourCostModel()->where('tour_id', $id)->update($data);
    }

    public function getTourCostById($id) {
        $query = $this->getTourCostModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTourCost($fields = "*", $params = array()) {
        $query = $this->getTourCostModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function getCurrentTourCost($fields = "*", $params = array(), $singleAmount = false) {
        $returnData = array();
        $query = $this->getTourCostModel()
                    ->select($fields)
                    ->leftJoin('currency_master', 'tour_cost.currency_id', '=', 'currency_master.currId')
                    ->where('tour_cost.deleted_at', null);
                    //->whereRaw('CURDATE() BETWEEN from_date AND to_date')
                    if(!empty($params)){
                        foreach ($params as $key => $param) {
                            $query->where($key, $param);
                        }
                    }
        $query->orderBy('tour_cost.id', 'ASC');
        $cost = $query->first();
        if($cost){
            Cache::remember('tour_rate_master', env('CACHE_LIIMIT'), function () {
                $rateMasterObj = new \App\Libraries\RatetypeLib();
                    return $rateMasterObj->getRatetypemaster(array('typeId', 'name', 'description'));
            });
            Cache::remember('tour_package_type', env('CACHE_LIIMIT'), function () {
                $TourpackagetypeObj = new \App\Libraries\TourpackagetypeLib();
                    return $TourpackagetypeObj->getTourpackagetypemaster(array('typeId', 'name'));
            });
            $tour_rate_master = Cache::pull('tour_rate_master');
            $tour_package_type = Cache::pull('tour_package_type');

            $CostAmountObj = new \App\Libraries\TourCostAmountLib();
            $costAmount = $CostAmountObj->getTourCostAmount(array('*'), array('cost_id' => $cost->id, 'tour_id' => $params['tour_id']));

            $returnData['dates'] = array('id' => $cost->id, 'from_date' => $cost->from_date, 'to_date' => $cost->to_date, 'discount' => $cost->discount);
            $rate_type = array();
            foreach ($tour_rate_master as $rate_master) {
                $rate_type[$rate_master->typeId] = array('name' => $rate_master->name, 'description' => $rate_master->description);
            }
            foreach ($tour_package_type as $type) {
                $returnData['amount'][$type->typeId] = array('name' => $type->name, 'rates' => $rate_type);
            }
            foreach ($costAmount as $amount) {
                if (!isset($returnData['amount'][$amount->package_type_id]) || !isset($returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id])) {
                continue;
                }
                $packageName = $returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id];
                $returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id] = array('name' => $packageName, 'cost' => $amount->cost, "sell" => $amount->sell);
            }
            return $returnData;
        }else{
            return $returnData;
        }
    }

    // public function getCurrentTourCost($fields = "*", $params = array(), $singleAmount = false) {
    //    //\DB::enableQueryLog();
    //     $query = $this->getTourCostModel()
    //             ->select($fields)
    //             ->leftJoin('currency_master', 'tour_cost.currency_id', '=', 'currency_master.currId')
    //             ->where('tour_cost.deleted_at', null)
    //             //->whereRaw('CURDATE() BETWEEN from_date AND to_date')
    //             ->orderBy('tour_cost.id', 'ASC');
    //     foreach ($params as $key => $param) {
    //         $query->where($key, $param);
    //     }
    //     $cost = $query->first();
    //     Cache::remember('tour_rate_master', env('CACHE_LIIMIT'), function () {
    //         $rateMasterObj = new \App\Libraries\RatetypeLib();
    //         return $rateMasterObj->getRatetypemaster(array('typeId', 'name', 'description'));
    //     });
    //     Cache::remember('tour_package_type', env('CACHE_LIIMIT'), function () {
    //         $TourpackagetypeObj = new \App\Libraries\TourpackagetypeLib();
    //         return $TourpackagetypeObj->getTourpackagetypemaster(array('typeId', 'name'));
    //     });
    //     $tour_rate_master = Cache::pull('tour_rate_master');
    //     $tour_package_type = Cache::pull('tour_package_type');

    //     $CostAmountObj = new \App\Libraries\TourCostAmountLib();
    //     $costAmount = $CostAmountObj->getTourCostAmount(array('*'), array('cost_id' => $cost->id, 'tour_id' => $params['tour_id']));

    //     $returnData = array();
    //     $returnData['dates'] = array('id' => $cost->id, 'from_date' => $cost->from_date, 'to_date' => $cost->to_date, 'discount' => $cost->discount, 'currency_name' => $cost->currency_name, 'currency_value' => $cost->currency_value, 'currency_icon' => $cost->currency_icon, );
    //     $rate_type = array();
    //     foreach ($tour_rate_master as $rate_master) {
    //         $rate_type[$rate_master->typeId] = array('name' => $rate_master->name, 'description' => $rate_master->description);
    //     }
    //     foreach ($tour_package_type as $type) {
    //         $returnData['amount'][$type->typeId] = array('name' => $type->name, 'rates' => $rate_type);
    //     }
    //     foreach ($costAmount as $amount) {
    //         if (!isset($returnData['amount'][$amount->package_type_id]) || !isset($returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id])) {
    //             continue;
    //         }
    //         $packageName = $returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id];
    //         //            if (!isset($returnData['dates']['amount'])) {
    //         //                $returnData['dates']['amount'] = $amount->sell;
    //         //                $returnData['dates']['name'] = $packageName['name'];
    //         //                $returnData['dates']['description'] = $packageName['description'];
    //         //            }
    //         $returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id] = array('name' => $packageName, 'cost' => $amount->cost, "sell" => $amount->sell);
    //     }
    //     return $returnData;
    // }

    // public function getCurrentTourCost($fields = "*", $params = array(), $singleAmount = false) {
    //     $query = $this->getTourCostModel()
    //             ->select($fields)
    //             ->where('deleted_at', null)
    //             ->orderBy('id', 'ASC');
    //     foreach ($params as $key => $param) {
    //         $query->where($key, $param);
    //     }
    //     $cost = $query->first();
    //     $returnData = array();
    //     if($cost){
    //         Cache::remember('tour_rate_master', env('CACHE_LIIMIT'), function () {
    //             $rateMasterObj = new \App\Libraries\RatetypeLib();
    //             return $rateMasterObj->getRatetypemaster(array('typeId', 'name', 'description'));
    //         });
    //         Cache::remember('tour_package_type', env('CACHE_LIIMIT'), function () {
    //             $TourpackagetypeObj = new \App\Libraries\TourpackagetypeLib();
    //             return $TourpackagetypeObj->getTourpackagetypemaster(array('typeId', 'name'));
    //         });
    //         $tour_rate_master = Cache::pull('tour_rate_master');
    //         $tour_package_type = Cache::pull('tour_package_type');

    //         $CostAmountObj = new \App\Libraries\TourCostAmountLib();
    //         $costAmount = $CostAmountObj->getTourCostAmount(array('*'), array('cost_id' => $cost->id, 'tour_id' => $params['tour_id']));

    //         $returnData = array();
    //         $returnData['dates'] = array('id' => $cost->id, 'from_date' => $cost->from_date, 'to_date' => $cost->to_date, 'discount' => $cost->discount);
    //         $rate_type = array();
    //         foreach ($tour_rate_master as $rate_master) {
    //             $rate_type[$rate_master->typeId] = array('name' => $rate_master->name, 'description' => $rate_master->description);
    //         }
    //         foreach ($tour_package_type as $type) {
    //             $returnData['amount'][$type->typeId] = array('name' => $type->name, 'rates' => $rate_type);
    //         }
    //         foreach ($costAmount as $amount) {
    //             if (!isset($returnData['amount'][$amount->package_type_id]) || !isset($returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id])) {
    //                 continue;
    //             }
    //             $packageName = $returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id];
    //             $returnData['amount'][$amount->package_type_id]['rates'][$amount->rate_id] = array('name' => $packageName, 'cost' => $amount->cost, "sell" => $amount->sell);
    //         }
    //         return $returnData;
    //     }else{
    //         return $returnData;
    //     } 
    // }

}

?>