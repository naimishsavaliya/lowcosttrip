<?php

namespace App\Libraries;

use App\State;
use Illuminate\Support\Facades\Auth;

class StateLib {

    protected $_stateModel = null;

    public function getStateModel() {
        if (!($this->_stateModel instanceof \App\State)) {
            $this->_stateModel = new \App\State();
        }
        return $this->_stateModel;
    }

    public function addState($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getStateModel()->insertGetId($data);
    }

    public function updateState($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getStateModel()->where('state_id', $id)->update($data);
    }

    public function deleteState($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getStateModel()->where('state_id', $id)->update($data);
    }

    public function getStateById($id) {
        $query = $this->getStateModel()
                ->where('state_id', $id);
        return $query->first();
    }

    public function getState($fields = "*", $params = array()) {
        $query = $this->getStateModel()
                ->select($fields)
                ->orderBy('state_name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function getStateByCountry($country_id, $fields = "*", $params = array()) {
        $query = $this->getStateModel()
                ->select($fields)
                ->orderBy('state_name', 'ASC')
                ->where('country_id', $country_id);
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>