<?php

namespace App\Libraries;

use App\Hotelpromote;
use Illuminate\Support\Facades\Auth;

class HotelpromoteLib {

    protected $_hotelpromoteModel = null;

    public function getHotelpromoteModel() {
        if (!($this->_hotelpromoteModel instanceof \App\Hotelpromote)) {
            $this->_hotelpromoteModel = new \App\Hotelpromote();
        }
        return $this->_hotelpromoteModel;
    }

    public function addHotelpromote($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getHotelpromoteModel()->insertGetId($data);
    }

    public function updateHotelpromote($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotelpromoteModel()->where('id', $id)->update($data);
    }

    public function deleteHotelpromote($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotelpromoteModel()->where('id', $id)->update($data);
    }

    public function getHotelpromoteById($id) {
        $query = $this->getHotelpromoteModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getHotelpromote($fields = "*", $params = array()) {
        $query = $this->getHotelpromoteModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>