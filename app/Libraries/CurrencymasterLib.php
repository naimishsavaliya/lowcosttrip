<?php

namespace App\Libraries;

use App\Currencymaster;
use Illuminate\Support\Facades\Auth;

class CurrencymasterLib {

    protected $_currencymasterModel = null;

    public function getCurrencymasterModel() {
        if (!($this->_currencymasterModel instanceof \App\Currencymaster)) {
            $this->_currencymasterModel = new \App\Currencymaster();
        }
        return $this->_currencymasterModel;
    }

    public function addCurrencymaster($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getCurrencymasterModel()->insertGetId($data);
    }

    public function updateCurrencymaster($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCurrencymasterModel()->where('currId', $id)->update($data);
    }

    public function deleteCurrencymaster($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCurrencymasterModel()->where('currId', $id)->update($data);
    }

    public function getCurrencymasterById($id) {
        $query = $this->getCurrencymasterModel()
                ->where('currId', $id);
        return $query->first();
    }

    public function getCurrencymaster($fields = "*", $all = false) {
        $query = $this->getCurrencymasterModel()
                ->select($fields)
                ->orderBy('currency_name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>