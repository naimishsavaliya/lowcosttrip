<?php

namespace App\Libraries;

use App\Leadquatation;
use Illuminate\Support\Facades\Auth;

class LeadquatationLib {

    protected $_leadquatationModel = null;

    public function getLeadQuatationModel() {
        if (!($this->_leadquatationModel instanceof \App\Leadquatation)) {
            $this->_leadquatationModel = new \App\Leadquatation();
        }
        return $this->_leadquatationModel;
    }

    public function addLeadQuatation($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        $data['quatation_id'] =  "LCTQT". (10000 + $this->getNextAutoID("lead_quatation"));
        return $query = $this->getLeadQuatationModel()->insertGetId($data);
    }


    public function getNextAutoID($table_name) {
        $statement = \DB::select("show table status like '{$table_name}'");
        return $statement[0]->Auto_increment;
    }

    public function updateLeadQuatation($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadQuatationModel()->where('quaId', $id)->update($data);
    }

    public function deleteLeadQuatation($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadQuatationModel()->where('quaId', $id)->update($data);
    }

    public function getLeadQuatationById($lead_id, $quotation_id, $fields = "*") {
        $query = $this->getLeadQuatationModel()
                ->select('lead_quatation.*', 'country.country_name', 'state.state_name', 'city.city_name', 'users.first_name', 'users.last_name', 'lead.lead_id as lead_no', 'users_profile.phone_no as quick_contact', 'currency_master.currency_name', 'currency_master.code', 'currency_master.symbol')
                ->leftJoin('lead', 'lead_quatation.lead_id', '=', 'lead.id')
                ->leftJoin('country', 'lead_quatation.country_id', '=', 'country.country_id')
                ->leftJoin('state', 'lead_quatation.state_id', '=', 'state.state_id')
                ->leftJoin('city', 'lead_quatation.city_id', '=', 'city.city_id')
                ->leftJoin('users', 'lead_quatation.created_by', '=', 'users.id')
                ->leftJoin('users_profile', 'lead_quatation.created_by', '=', 'users_profile.user_id')
                ->leftJoin('currency_master', 'lead_quatation.quotation_currency', '=', 'currency_master.currId')
                ->where('lead_quatation.lead_id', $lead_id)
                ->where('quaId', $quotation_id);
        return $query->first();
    }

    public function getLeadQuatation($fields = "*", $params = array()) {
        $query = $this->getLeadQuatationModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('subject', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function getLeadQuatationRecordById($id, $fields = "*") {
        $query = $this->getLeadQuatationModel()
                ->select($fields)
                ->where('quaId', $id);
        return $query->first();
    }


    public function getQuotationByNumber($id, $fields = "*") {
        $query = $this->getLeadQuatationModel()
                ->select($fields)
                ->where('quatation_id', $id);
        return $query->first();
    }

    public function getQuatationBtLeadNo($id) {
        $query = $this->getLeadQuatationModel()
                ->select('*')
                ->where('lead_id', $id);
        return $query->first();
    }

}

?>