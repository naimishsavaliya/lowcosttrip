<?php

namespace App\Libraries;

use App\Userprofile;
use Illuminate\Support\Facades\Auth;

class UserprofileLib {

    protected $_userprofileModel = null;

    public function getUserprofileModel() {
        if (!($this->_userprofileModel instanceof \App\Userprofile)) {
            $this->_userprofileModel = new \App\Userprofile();
        }
        return $this->_userprofileModel;
    }

    public function addUserprofile($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
//        $data['created_by'] = Auth::id();
        return $query = $this->getUserprofileModel()->insertGetId($data);
    }

    public function updateUserprofile($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
//        $data['updated_by'] = Auth::id();
        return $this->getUserprofileModel()->where('user_id', $id)->update($data);
    }

    public function deleteUserprofile($id) {
        $data = array();
//        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
//        $data['updated_by'] = Auth::id();
        return $this->getUserprofileModel()->where('user_id', $id)->update($data);
    }

    public function getUserprofileById($id) {
        $query = $this->getUserprofileModel()
                ->where('user_id', $id);
        return $query->first();
    }

    public function getUserprofile($fields = "*", $params = array()) {
        $query = $this->getUserprofileModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>