<?php

namespace App\Libraries;

use App\Customerinfo;
use Illuminate\Support\Facades\Auth;

class CustomerinfoLib {

    protected $_customerinfoModel = null;

    public function getCustomerinfoModel() {
        if (!($this->_customerinfoModel instanceof \App\Customerinfo)) {
            $this->_customerinfoModel = new \App\Customerinfo();
        }
        return $this->_customerinfoModel;
    }

    public function addCustomerinfo($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getCustomerinfoModel()->insertGetId($data);
    }

    public function updateCustomerinfo($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCustomerinfoModel()->where('id', $id)->update($data);
    }

    public function deleteCustomerinfo($id) {
        return $this->getCustomerinfoModel()->where('id', $id)->delete();
    }
    public function deleteCustomerinfoByLeadId($lead_id) {
        return $this->getCustomerinfoModel()->where('lead_id', $lead_id)->delete();
    }
    public function deleteCustomerinfoByAccId($acc_id) {
        return $this->getCustomerinfoModel()->where('accomodation_id', $acc_id)->delete();
    }

    public function getCustomerinfoById($id) {
        $query = $this->getCustomerinfoModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getCustomerinfo($fields = "*", $params = array()) {
        $query = $this->getCustomerinfoModel()
                ->select($fields)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>