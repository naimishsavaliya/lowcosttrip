<?php

namespace App\Libraries;

use App\Holidayactivities;
use Illuminate\Support\Facades\Auth;

class HolidayactivitiesLib {

    protected $_holidayactivitiesmodel = null;
        
    public function getHolidayactivitiesModel() {
        if (!($this->_holidayactivitiesmodel instanceof \App\Holidayactivities)) {
            $this->_holidayactivitiesmodel = new \App\Holidayactivities();
        }
        return $this->_holidayactivitiesmodel;
    }
    
    public function addHolidayactivities($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getHolidayactivitiesModel()->insertGetId($data);
    }
    
    public function updateHolidayactivities($data, $id) {
        return $this->getHolidayactivitiesModel()->where('id', $id)->update($data);
    }

    public function deleteHolidayactivities($id) {
        $data = array();
        return $this->getHolidayactivitiesModel()->where('id', $id)->delete();
    }
    
    public function getHolidayactivitiesById($id) {
        $query = $this->getHolidayactivitiesModel()
                ->where('id', $id);
        return $query->first();
    }
    
    public function getHolidayactivities($fields = "*", $params = array()) {
        $query = $this->getHolidayactivitiesModel()
                ->select($fields)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function deleteHolidayactivities_lead($where) {
        return $this->getHolidayactivitiesModel()->where($where)->delete();
    }

}

?>