<?php

namespace App\Libraries;

use App\Leadquatationitinerary;
use Illuminate\Support\Facades\Auth;

class LeadquatationitinararyLib {

    protected $_leaditinararyModel = null;

    public function getLeaItinararyModel() {
        if (!($this->_leaditinararyModel instanceof \App\Leadquatationitinerary)) {
            $this->_leaditinararyModel = new \App\Leadquatationitinerary();
        }
        return $this->_leaditinararyModel;
    }

    public function addLeadItinarary($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getLeaItinararyModel()->insertGetId($data);
    }

    public function updateLeadItinarary($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeaItinararyModel()->where('itiId', $id)->update($data);
    }

    public function deleteLeadItinarary($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeaItinararyModel()->where('itiId', $id)->update($data);
    }

    public function getLeadItinararyById($lead_id, $quotation_id, $fields = "*") {
        $query = $this->getLeaItinararyModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->where('lead_id', $lead_id)
                ->where('quotation_id', $quotation_id);
        return $query->get();
    }

    public function getLeadItinarary($fields = "*", $params = array()) {
        $query = $this->getLeaItinararyModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('title', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }
    
    public function deleteMulLeadItinarary($ids) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeaItinararyModel()->whereIn('itiId', $ids)->update($data);
    }

}

?>