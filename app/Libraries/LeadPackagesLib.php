<?php

namespace App\Libraries;

use App\LeadPackages;
use Illuminate\Support\Facades\Auth;

class LeadPackagesLib {

    protected $_leadpackagesModel = null;

    public function getleadpackagesModel() {
        if (!($this->_leadpackagesModel instanceof \App\LeadPackages)) {
            $this->_leadpackagesModel = new \App\LeadPackages();
        }
        return $this->_leadpackagesModel;
    }

    public function add($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getleadpackagesModel()->insertGetId($data);
    }

    public function update($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        return $this->getleadpackagesModel()->where('id', $id)->update($data);
    }

    public function delete($id) {
        return $this->getleadpackagesModel()->where('id', $id)->delete();
    }

    public function getLeadPackagesById($id) {
        $query = $this->getleadpackagesModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getLeadPackages($fields = "*", $params = array()) {
        $query = $this->getleadpackagesModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('title', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>