<?php

namespace App\Libraries;

use App\FlightPassengers;
use Illuminate\Support\Facades\Auth;

class FlightPassengersLib {

    protected $_flightpassengers = null;

    public function getFlightPassengers() {
        if (!($this->_flightpassengers instanceof \App\FlightPassengers)) {
            $this->_flightpassengers = new \App\FlightPassengers();
        }
        return $this->_flightpassengers;
    }

    public function addFlightPassengers($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getFlightPassengers()->insertGetId($data);
    }

    public function getFlightPassengersByFlightId($id) {
        $query = $this->getFlightPassengers()
                ->where('flight_books_id', $id);
        return $query->get();
    }

}

?>