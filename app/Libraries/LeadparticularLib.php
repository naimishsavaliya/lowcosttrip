<?php

namespace App\Libraries;

use App\Leadquatationperticular;
use Illuminate\Support\Facades\Auth;

class LeadparticularLib {

    protected $_leaditinararyModel = null;

    public function getLeadParticularModel() {
        if (!($this->_leaditinararyModel instanceof \App\Leadquatationperticular)) {
            $this->_leaditinararyModel = new \App\Leadquatationperticular();
        }
        return $this->_leaditinararyModel;
    }

    public function addLeadParticular($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getLeadParticularModel()->insertGetId($data);
    }

    public function updateLeadParticular($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadParticularModel()->where('perId', $id)->update($data);
    }

    public function deleteLeadParticular($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadParticularModel()->where('perId', $id)->update($data);
    }

    public function getLeadParticularById($lead_id, $quotation_id, $fields = "*") {
        $query = $this->getLeadParticularModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->where('lead_id', $lead_id)
                ->where('quotation_id', $quotation_id);
        return $query->get();
    }

    public function getLeadParticular($fields = "*", $params = array()) {
        $query = $this->getLeadParticularModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('item_title', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }
    
    public function deleteMulLeadParticular($ids) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getLeadParticularModel()->whereIn('perId', $ids)->update($data);
    }

}

?>