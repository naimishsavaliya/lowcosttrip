<?php

namespace App\Libraries;

use App\Country;
use Illuminate\Support\Facades\Auth;

class CountryLib {

    protected $_countryModel = null;

    public function getCountryModel() {
        if (!($this->_countryModel instanceof \App\Country)) {
            $this->_countryModel = new \App\Country();
        }
        return $this->_countryModel;
    }

    public function addCountry($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getCountryModel()->insertGetId($data);
    }

    public function updateCountry($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCountryModel()->where('country_id', $id)->update($data);
    }

    public function deleteCountry($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getCountryModel()->where('country_id', $id)->update($data);
    }

    public function getCountryById($id) {
        $query = $this->getCountryModel()
                ->where('country_id', $id);
        return $query->first();
    }

    public function getCountry($fields = "*", $params = array()) {
        $query = $this->getCountryModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('country_name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>