<?php

namespace App\Libraries;

use App\HotDealTour;
use Illuminate\Support\Facades\Auth;

class HotDealTourLib {

    protected $_hotdealtour = null;

    public function getHotDealTourModel() {
        if (!($this->_hotdealtour instanceof \App\HotDealTour)) {
            $this->_hotdealtour = new \App\HotDealTour();
        }
        return $this->_hotdealtour;
    }

    public function addHotDealTour($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getHotDealTourModel()->insertGetId($data);
    }

    public function updateHotDealTour($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotDealTourModel()->where('id', $id)->update($data);
    }

    public function deleteHotDealTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotDealTourModel()->where('id', $id)->update($data);
    }

    public function getHotDealTourById($id, $fields = "*") {
        $query = $this->getHotDealTourModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getHotDealsTours($params = array()) {
        $query = $this->getHotDealTourModel()
                ->select('hot_deal_tours.id','hot_deal_tours.tour_id','hot_deal_tours.title','hot_deal_tours.start_date','hot_deal_tours.end_date','tour.tour_name','tour.tour_nights','tour.image')
                ->leftJoin('tour', 'tour.id', '=', 'hot_deal_tours.tour_id');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        $query->orderBy('hot_deal_tours.id', 'DESC');
        $query->limit(4);
        return $query->get();
    }

}

?>