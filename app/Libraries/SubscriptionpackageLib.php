<?php

namespace App\Libraries;

use App\Subscriptionpackage;
use Illuminate\Support\Facades\Auth;

class SubscriptionpackageLib {

    protected $_subscriptionpackageModel = null;

    public function getSubscriptionpackageModel() {
        if (!($this->_subscriptionpackageModel instanceof \App\Subscriptionpackage)) {
            $this->_subscriptionpackageModel = new \App\Subscriptionpackage();
        }
        return $this->_subscriptionpackageModel;
    }

    public function addSubscriptionpackage($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getSubscriptionpackageModel()->insertGetId($data);
    }

    public function updateSubscriptionpackage($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getSubscriptionpackageModel()->where('packId', $id)->update($data);
    }

    public function deleteSubscriptionpackage($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getSubscriptionpackageModel()->where('packId', $id)->update($data);
    }

    public function getSubscriptionpackageById($id) {
        $query = $this->getSubscriptionpackageModel()
                ->where('packId', $id);
        return $query->first();
    }

    public function getSubscriptionpackage($fields = "*", $all = false) {
        $query = $this->getSubscriptionpackageModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>
