<?php

namespace App\Libraries;

use App\Ratetype;
use Illuminate\Support\Facades\Auth;

class RatetypeLib {

    protected $_ratetypemasterModel = null;

    public function getRatetypemasterModel() {
        if (!($this->_ratetypemasterModel instanceof \App\Ratetype)) {
            $this->_ratetypemasterModel = new \App\Ratetype();
        }
        return $this->_ratetypemasterModel;
    }

    public function addRatetypemaster($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getRatetypemasterModel()->insertGetId($data);
    }

    public function updateRatetypemaster($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getRatetypemasterModel()->where('typeId', $id)->update($data);
    }

    public function deleteRatetypemaster($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getRatetypemasterModel()->where('typeId', $id)->update($data);
    }

    public function getRatetypemasterById($id) {
        $query = $this->getRatetypemasterModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getRatetypemaster($fields = "*", $params = array()) {
        $query = $this->getRatetypemasterModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        $query->where('deleted_at', null)->where('status', 1);
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>