<?php

namespace App\Libraries;

use App\Hoteltype;
use Illuminate\Support\Facades\Auth;

class HoteltypeLib {

    protected $_hoteltypeModel = null;

    public function getHoteltypeModel() {
        if (!($this->_hoteltypeModel instanceof \App\Hoteltype)) {
            $this->_hoteltypeModel = new \App\Hoteltype();
        }
        return $this->_hoteltypeModel;
    }

    public function addHoteltype($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getHoteltypeModel()->insertGetId($data);
    }

    public function updateHoteltype($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHoteltypeModel()->where('typeId', $id)->update($data);
    }

    public function deleteHoteltype($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHoteltypeModel()->where('typeId', $id)->update($data);
    }

    public function getHoteltypeById($id) {
        $query = $this->getHoteltypeModel()
                ->where('typeId', $id);
        return $query->first();
    }

    public function getHoteltype($fields = "*", $all = false) {
        $query = $this->getHoteltypeModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        if (!$all) {
            $query->where('deleted_at', null)->where('status', 1);
        }
        return $query->get();
    }

}

?>