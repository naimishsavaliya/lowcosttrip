<?php

namespace App\Libraries;

use App\Location;
use Illuminate\Support\Facades\Auth;

class LocationLib {

	protected $_locationModel = null;

	public function getLocationModel() {
		if (!($this->_locationModel instanceof \App\Location)) {
			$this->_locationModel = new \App\Location();
		}
		return $this->_locationModel;
	}

	public function addLocation($data) {
		$data['created_at'] = date("Y-m-d H:i:s");
		$data['created_by'] = Auth::id();
		return $query = $this->getLocationModel()->insertGetId($data);
	}

	public function updateLocation($data, $id) {
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getLocationModel()->where('location_id', $id)->update($data);
	}

	public function deleteLocation($id) {
		$data = array();
		$data['deleted_at'] = date("Y-m-d H:i:s");
		$data['updated_at'] = date("Y-m-d H:i:s");
		$data['updated_by'] = Auth::id();
		return $this->getLocationModel()->where('location_id', $id)->update($data);
	}

	public function getLocationById($id) {
		$query = $this->getLocationModel()
				->where('location_id', $id);
		return $query->first();
	}

	public function getLocation($fields = "*", $all = false) {
		$query = $this->getLocationModel()
				->select($fields)
				->orderBy('location_name', 'ASC');
		if (!$all) {
			$query->where('deleted_at', null)->where('status',1);
		}
		return $query->get();
	}

}

?>