<?php

namespace App\Libraries;

use App\CnfLeadAutoNo;
use Illuminate\Support\Facades\Auth;

class CnfLeadAutoNoLib {

    protected $_cnfleadautonoModel = null;

    public function getCnfLeadAutoNoModel() {
        if (!($this->_cnfleadautonoModel instanceof \App\CnfLeadAutoNo)) {
            $this->_cnfleadautonoModel = new \App\CnfLeadAutoNo();
        }
        return $this->_cnfleadautonoModel;
    }

    public function addCnfLeadAutoNo($data = array()) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getCnfLeadAutoNoModel()->insertGetId($data);
    }

    public function updateCnfLeadAutoNo($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        return $this->getCnfLeadAutoNoModel()->where('id', $id)->update($data);
    }

    public function deleteCnfLeadAutoNo($id) {
        return $this->getCnfLeadAutoNoModel()->where('id', $id)->delete();
    }
    public function deleteCnfLeadAutoNoByLeadId($lead_id) {
        return $this->getCnfLeadAutoNoModel()->where('lead_id', $lead_id)->delete();
    }

    public function getCnfLeadAutoNoById($id) {
        $query = $this->getCnfLeadAutoNoModel()
                ->where('id', $id);
        return $query->first();
    }

	public function getNextAutoID() {
		$statement = \DB::select("show table status like 'cnf_lead_auto_no'");
		return $statement[0]->Auto_increment;
	}
	
    public function getCnfLeadAutoNo($fields = "*", $params = array()) {
        $query = $this->getCnfLeadAutoNoModel()
                ->select($fields)
                ->orderBy('id', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>