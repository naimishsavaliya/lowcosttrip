<?php

namespace App\Libraries;

use App\Suppliers;
use Illuminate\Support\Facades\Auth;

class SupplierLib {

    protected $_supplierModel = null;

    public function getSupplierModel() {
        if (!($this->_supplierModel instanceof \App\Suppliers)) {
            $this->_supplierModel = new \App\Suppliers();
        }
        return $this->_supplierModel;
    }

    public function addSupplier($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
//        $data['created_by'] = Auth::id();
        return $query = $this->getSupplierModel()->insertGetId($data);
    }

    public function updateSupplier($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
//        $data['updated_by'] = Auth::id();
        return $this->getSupplierModel()->where('supId', $id)->update($data);
    }

    public function deleteSupplier($id) {
        $data = array();
//        $data['deleted_at'] = date("Y-m-d H:i:s");
//        $data['updated_at'] = date("Y-m-d H:i:s");
//        $data['updated_by'] = Auth::id();
        return $this->getSupplierModel()->where('supId', $id)->update($data);
    }

    public function getSupplierById($id) {
        $query = $this->getSupplierModel()
                ->where('supId', $id);
        return $query->first();
    }

    public function getSupplier($fields = "*", $params = array()) {
        $query = $this->getSupplierModel()
                ->select($fields)
                ->orderBy('type', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>