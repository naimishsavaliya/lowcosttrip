<?php

namespace App\Libraries;

use App\NewArrivalTour;
use Illuminate\Support\Facades\Auth;

class NewArrivalTourLib {

    protected $_newarrivaltour = null;

    public function getNewArrivalTourModel() {
        if (!($this->_newarrivaltour instanceof \App\NewArrivalTour)) {
            $this->_newarrivaltour = new \App\NewArrivalTour();
        }
        return $this->_newarrivaltour;
    }

    public function addNewArrivalTour($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getNewArrivalTourModel()->insertGetId($data);
    }

    public function updateNewArrivalTour($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getNewArrivalTourModel()->where('id', $id)->update($data);
    }

    public function deleteNewArrivalTour($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getNewArrivalTourModel()->where('id', $id)->update($data);
    }

    public function getNewArrivalTourById($id, $fields = "*") {
        $query = $this->getNewArrivalTourModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getNewArrivalTours($params = array()) {
        $query = $this->getNewArrivalTourModel()
                ->select('new_arrival_tours.id','new_arrival_tours.tour_id','new_arrival_tours.title','new_arrival_tours.start_date','new_arrival_tours.end_date','tour.tour_name','tour.tour_nights','tour.image')
                ->leftJoin('tour', 'tour.id', '=', 'new_arrival_tours.tour_id');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        $query->orderBy('new_arrival_tours.id', 'DESC');
        $query->limit(4);
        return $query->get();
    }

}

?>