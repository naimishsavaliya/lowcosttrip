<?php

namespace App\Libraries;

use App\Hotel;
use Illuminate\Support\Facades\Auth;

class HotelLib {

    protected $_hotelModel = null;

    public function getHotelModel() {
        if (!($this->_hotelModel instanceof \App\Hotel)) {
            $this->_hotelModel = new \App\Hotel();
        }
        return $this->_hotelModel;
    }

    public function addHotel($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getHotelModel()->insertGetId($data);
    }

    public function updateHotel($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotelModel()->where('id', $id)->update($data);
    }

    public function deleteHotel($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getHotelModel()->where('id', $id)->update($data);
    }

    public function getHotelById($id) {
        $query = $this->getHotelModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getHotel($fields = "*", $params = array()) {
        $query = $this->getHotelModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>