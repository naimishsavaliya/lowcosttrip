<?php

namespace App\Libraries;

use App\Testimonial;
use Illuminate\Support\Facades\Auth;

class TestimonialLib {

    protected $_testimonialModel = null;

    public function getTesttimonialModel() {
        if (!($this->_testimonialModel instanceof \App\Testimonial)) {
            $this->_testimonialModel = new \App\Testimonial();
        }
        return $this->_testimonialModel;
    }

    public function addTestimonial($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getTesttimonialModel()->insertGetId($data);
    }

    public function updateTestimonial($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTesttimonialModel()->where('id', $id)->update($data);
    }

    public function deleteTestimonial($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getTesttimonialModel()->where('id', $id)->update($data);
    }

    public function getTestimonialById($id) {
        $query = $this->getTesttimonialModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getTestimonial($fields = "*", $params = array()) {
        $query = $this->getTesttimonialModel()
                ->select($fields)
                ->where('deleted_at', null)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>