<?php

namespace App\Libraries;

use App\FlightBooks;
use Illuminate\Support\Facades\Auth;
use DB;

class FlightBooksLib {

    protected $_flightbooks = null;

    public function getFlightBooks() {
        if (!($this->_flightbooks instanceof \App\FlightBooks)) {
            $this->_flightbooks = new \App\FlightBooks();
        }
        return $this->_flightbooks;
    }

    public function addFlightBooks($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        return $query = $this->getFlightBooks()->insertGetId($data);
    }

    public function getFlightBooksById($id) {
        $query = $this->getFlightBooks()
                    ->select('flight_books.*', 'c1.city_name as flight_from_city', 'c2.city_name as flight_to_city')
                    ->leftjoin('city as c1', 'flight_books.flight_from', '=', 'c1.city_code')
                    ->leftjoin('city as c2', 'flight_books.flight_to', '=', 'c2.city_code')
                ->where('id', $id);
        return $query->first();
    }

    public function getFlightBooksPayment($id) {
      return DB::table('payment_response')->where('id', $id)->first();
    }

}

?>