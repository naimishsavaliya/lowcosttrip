<?php

namespace App\Libraries;

use App\Roompricetype;
use Illuminate\Support\Facades\Auth;

class RoompricetypeLib {

    protected $_roompricetypeModel = null;

    public function getRoompricetypeModel() {
        if (!($this->_roompricetypeModel instanceof \App\Roompricetype)) {
            $this->_roompricetypeModel = new \App\Roompricetype();
        }
        return $this->_roompricetypeModel;
    }

    public function addRoomPriceType($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getRoompricetypeModel()->insertGetId($data);
    }

    public function updateRoomPriceType($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getRoompricetypeModel()->where('id', $id)->update($data);
    }

    public function deleteRoomPriceType($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getRoompricetypeModel()->where('id', $id)->update($data);
    }

    public function getRoomPriceTypeById($id) {
        $query = $this->getRoompricetypeModel()
                ->where('id', $id);
        return $query->first();
    }

    public function getRoomPriceType($fields = "*", $params = array()) {
        $query = $this->getRoompricetypeModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

}

?>