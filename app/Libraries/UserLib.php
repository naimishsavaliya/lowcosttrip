<?php

namespace App\Libraries;

use App\User;
use Illuminate\Support\Facades\Auth;

class UserLib {

    protected $_userModel = null;

    public function getUserModel() {
        if (!($this->_userModel instanceof \App\User)) {
            $this->_userModel = new \App\User();
        }
        return $this->_userModel;
    }

    public function addUser($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = Auth::id();
        return $query = $this->getUserModel()->insertGetId($data);
    }

    public function updateUser($data, $id) {
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getUserModel()->where('id', $id)->update($data);
    }

    public function deleteUser($id) {
        $data = array();
        $data['deleted_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['updated_by'] = Auth::id();
        return $this->getUserModel()->where('id', $id)->update($data);
    }

    public function getUserById($id, $fields = "*") {
        $query = $this->getUserModel()
                ->select($fields)
                ->where('id', $id);
        return $query->first();
    }

    public function getUser($fields = "*", $params = array()) {
        $query = $this->getUserModel()
                ->select($fields)
                ->orderBy('name', 'ASC');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function getAdminUser() {
        $query = $this->getUserModel()
                ->select('users.id', 'users.first_name', 'users.last_name', 'users.email')
                ->where('users.role_id', '!=' ,5)
                ->where('users.status', 1)
                ->where('users.deleted_at', null);
        return $query->get();
    }

	public function getUserByEmailOrPhone($email, $phone) {
		$query = $this->getUserModel()->select('users.id')
				->leftJoin("users_profile", "users_profile.user_id", "=", "users.id");
		$query->where("users_profile.phone_no", $phone)->orwhere("users.email", $email);
		return $user = $query->first();
	}

    public function getUserWithProfile($id, $fields = "*", $params = array()) {
        $query = $this->getUserModel()
                ->select($fields)
                ->where('users.id', $id)
                ->leftJoin('users_profile', 'users.id', '=', 'users_profile.user_id')
                ->orderBy('first_name', 'ASC');
        if (count($params) > 0) {
            foreach ($params as $key => $param) {
                $query->where($key, $param);
            }
        }
        return $query->get();
    }

    public function getSupplierWithProfile($id, $fields = "*", $params = array()) {
        $query = $this->getUserModel()
                ->select($fields)
                ->where('users.id', $id)
                ->join('suppliers', 'users.id', '=', 'suppliers.supId');
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->first();
    }

    public function getUsersByRole($role_id, $fields = "*", $params = array()) {
        $query = $this->getUserModel()
                ->select($fields)
                ->where('users.role_id', $role_id)
                ->where('status', 1)
                ->where('deleted_at', null);
        foreach ($params as $key => $param) {
            $query->where($key, $param);
        }
        return $query->get();
    }

    public function checkuserdata($params = array(), $fields = "*") {
        $query = $this->getUserModel()
                ->select($fields)
                ->leftJoin('users_profile', 'users.id', '=', 'users_profile.user_id')
                ->orderBy('first_name', 'ASC');
        if (count($params) > 0) {
            foreach ($params as $key => $param) {
                $query->where($key, $param);
            }
        }
        return $query->count();
    }

    public function GetUserRecord($where, $fields = "*") {
        $query = $this->getUserModel()
                ->select($fields)
                ->where($where);
        return $query->first();
    }

    // public function verify_token($forgot_token, $fields = "*") {
    //     $query = $this->getUserModel()
    //             ->select($fields)
    //             ->where('forgot_token', $forgot_token);
    //     return $query->first();
    // }

    public function verify_token($where, $fields = "*") {
        $query = $this->getUserModel()
                ->select($fields)
                ->where($where);
        return $query->first();
    }

    
    public function getUserFullProfile($id) {
        $query = $this->getUserModel()
                ->select('users.id', 'users.first_name', 'users.last_name', 'users.email', 'users_profile.address', 'users_profile.pin_code', 'users_profile.age', 'users_profile.information', 'users_profile.phone_no', 'users_profile.landline_no', 'users_profile.gender', 'users_profile.image', 'users_profile.birth_date', 'users_profile.time_zone', 'country.country_name', 'state.state_name', 'city.city_name')
                ->where('users.id', $id)
                ->join('users_profile', 'users.id', '=', 'users_profile.user_id')
                ->leftJoin('country', 'users_profile.country_id', '=', 'country.country_id')
                ->leftJoin('state', 'users_profile.state_id', '=', 'state.state_id')
                ->leftJoin('city', 'users_profile.city_id', '=', 'city.city_id');
        return $query->first();
    }

}

?>