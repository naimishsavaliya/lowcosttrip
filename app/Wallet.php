<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model {

    protected $table = 'wallets';
    protected $fillable = ['id', 'user_id', 'credit', 'debit','current_balance', 'created_at', 'updated_at','deleted_at'];

}
