<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightPassengers extends Model
{
    protected $table = 'flight_passengers';
}
