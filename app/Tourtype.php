<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tourtype extends Model
{
     protected $table = 'master_tour_type';
}
