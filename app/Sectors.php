<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sectors extends Model
{
    
    protected $table = 'sector';
    
    //fillable fields
    protected $fillable = ['name', 'description'];
    
    
    //custom timestamps name
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
