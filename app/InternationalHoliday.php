<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternationalHoliday extends Model {

    protected $table = 'international_holidays';

}
