<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightReviewInfo extends Model
{
    protected $table = 'flight_review_info';
}
