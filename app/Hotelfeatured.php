<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotelfeatured extends Model
{
    protected $table = "hotel_featured";
}
