<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission {

	public static function defaultPermissions() {
		return [
			'view_category',
			'add_category',
			'edit_category',
			'delete_category',
			'view_role',
			'add_role',
			'edit_role',
			'delete_role',
			'view_level',
			'add_level',
			'edit_level',
			'delete_level',
			'view_course',
			'add_course',
			'edit_course',
			'delete_course',
		];
	}

}
